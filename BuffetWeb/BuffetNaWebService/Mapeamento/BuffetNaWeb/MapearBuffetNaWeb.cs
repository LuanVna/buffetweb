﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Net;
using HtmlAgilityPack;
using BuffetWebData.Utils;

namespace BuffetNaWebService.Mapeamento.BuffetNaWeb
{
    public class MapearBuffetNaWeb
    {
        private const string url_portal = "http://www.buffetnaweb.com.br";

        public void Start()
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<B_SERVICO_MAPEAR_REGIAO> regioes = this.Regioes();
                List<RegiaoBuffetNaWeb> rnw = new List<RegiaoBuffetNaWeb>();
                List<ParceiroBuffetNaWeb> parceirosNaWeb = new List<ParceiroBuffetNaWeb>();

                foreach (var regiao in regioes)
                {
                    RegiaoBuffetNaWeb d = new RegiaoBuffetNaWeb() { html = new WebClient().DownloadString(regiao.url) };
                    regiao.ultima_atualizacao = DateTime.Today;
                    regiao.quantidade_mapeada = d.regiaoParceiroNaWeb.Count;
                    rnw.Add(d);
                }

                db.SaveChanges();

                foreach (var regiao in rnw)
                {
                    foreach (var parceiro in regiao.regiaoParceiroNaWeb)
                    {
                        parceirosNaWeb.Add(new ParceiroBuffetNaWeb()
                        {
                            html = new WebClient().DownloadString(parceiro.url),
                            ParceiroRegiao = parceiro
                        });
                    }
                }

                List<B_SERVICO_PARCEIRO_MAPEADO> mapeados = db.B_SERVICO_PARCEIRO_MAPEADO.Where(e => e.de_concorrente.Equals("BuffetNaWeb")).ToList();

                foreach (var parceiro in parceirosNaWeb)
                {
                    try
                    {
                        var existe = mapeados.FirstOrDefault(e => e.nome.Equals(parceiro.nome) && e.telefone.Equals(parceiro.telefone) && e.status != "Cliente");
                        if (existe == null)
                        {
                            B_SERVICO_PARCEIRO_MAPEADO p = new B_SERVICO_PARCEIRO_MAPEADO()
                            {
                                atualizado_em = DateTime.Now,
                                mapeado_em = DateTime.Now,
                                nome = parceiro.nome,
                                autenticacao = UtilsManager.CriarAutenticacaoEmpresa(),
                                descricao = parceiro.descricao,
                                plano_atual = parceiro.ParceiroRegiao.plano,
                                site_portal = parceiro.ParceiroRegiao.url,
                                site = parceiro.site,
                                de_concorrente = "BuffetNaWeb",
                                video = parceiro.video,
                                rua = parceiro.rua,
                                localidade = parceiro.localidade,
                                status = "Mapeado",
                                telefone = parceiro.telefone,
                            };
                            db.B_SERVICO_PARCEIRO_MAPEADO.Add(p);
                            db.SaveChanges();
                            foreach (var imagem in parceiro.Imagens)
                            {
                                db.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS.Add(new B_SERVICO_PARCEIRO_MAPEADO_IMAGENS()
                                {
                                    id_parceiro_mapeado = p.id,
                                    url = imagem.ToString(),
                                });
                            }
                            db.SaveChanges();
                        }
                        else
                        {
                            existe.atualizado_em = DateTime.Now;
                            existe.nome = parceiro.nome;
                            existe.autenticacao = UtilsManager.CriarAutenticacaoEmpresa();
                            existe.descricao = parceiro.descricao;
                            existe.plano_atual = parceiro.ParceiroRegiao.plano;
                            existe.site_portal = parceiro.ParceiroRegiao.url;
                            existe.site = parceiro.site;
                            existe.de_concorrente = "BuffetNaWeb";
                            existe.rua = parceiro.rua;
                            existe.localidade = parceiro.localidade;
                            existe.telefone = parceiro.telefone;
                            existe.video = parceiro.video;
                            db.SaveChanges();

                            db.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS.RemoveRange(db.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS.Where(e => e.id_parceiro_mapeado == existe.id).ToList());

                            foreach (var imagem in parceiro.Imagens)
                            {
                                db.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS.Add(new B_SERVICO_PARCEIRO_MAPEADO_IMAGENS()
                                {
                                    id_parceiro_mapeado = existe.id,
                                    url = imagem.ToString(),
                                });
                            }
                            db.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
        }

        private List<B_SERVICO_MAPEAR_REGIAO> Regioes()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var regioesDb = db.B_SERVICO_MAPEAR_REGIAO.Where(e => e.concorrente.Equals("BuffetNaWeb")).ToList();

                string RegioesHtml = new WebClient().DownloadString(url_portal);

                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };

                htmlDoc.LoadHtml(RegioesHtml);

                var regioesEncontradas = htmlDoc.DocumentNode.SelectNodes("//a").Select(p => p.GetAttributeValue("href", ""));
                foreach (string regiao in regioesEncontradas)
                {
                    if (regiao.Contains("buffet-infantil") && !regiao.Contains("detalhe"))
                    {
                        if (regioesDb.FirstOrDefault(e => e.url.Equals(regiao)) == null)
                        {
                            db.B_SERVICO_MAPEAR_REGIAO.Add(new B_SERVICO_MAPEAR_REGIAO()
                            {
                                url = regiao,
                                concorrente = "BuffetWeb",
                                ultima_atualizacao = DateTime.Now
                            });
                        }
                    }
                }
                db.SaveChanges();
                return db.B_SERVICO_MAPEAR_REGIAO.Where(e => e.concorrente.Equals("BuffetNaWeb")).ToList();
            }
        }
    }
}
