﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuffetNaWebService.Mapeamento.BuffetNaWeb
{
    public class ParceiroBuffetNaWeb
    {
        public RegiaoParceiroNaWeb ParceiroRegiao { get; set; }
        public string html { get; set; }
        public string nome
        {
            get
            {
                return GetTag("h1", "name");
            }
        }

        public string descricao
        {
            get
            {
                return GetTag("div", "description");
            }
        }

        public string telefone
        {
            get
            {
                return GetTag("span", "telephone");
            }
        }

        public string rua
        {
            get
            {
                return GetTag("span", "streetAddress");
            }
        }
        public string localidade
        {
            get
            {
                return GetTag("span", "addressLocality");
            }
        }

        public string site
        {
            get
            {
                return GetTag("a", "url");
            }
        }

        public List<string> Imagens
        {
            get
            {
                List<string> images = new List<string>();

                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };

                htmlDoc.LoadHtml(this.html);

                foreach (HtmlNode img in htmlDoc.DocumentNode.SelectNodes("//img"))
                {
                    HtmlAttribute att = img.Attributes["src"];
                    if (att.Value.Contains("anunciante"))
                    {
                        images.Add(att.Value);
                    }
                }
                return images;
            }
        }

        public string video
        {
            get
            {
                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };
                htmlDoc.LoadHtml(this.html);
                var link = htmlDoc.DocumentNode.SelectSingleNode("//iframe[@class='embed-responsive-item']");
                return link != null ? link.Attributes["src"].Value : "";
            }
        }

        public string GetTag(string tag, string itemprop)
        {
            var htmlDoc = new HtmlDocument()
            {
                OptionFixNestedTags = true,
                OptionAutoCloseOnEnd = true
            };
            htmlDoc.LoadHtml(this.html);
            var link = htmlDoc.DocumentNode.SelectSingleNode("//" + tag + "[@itemprop='" + itemprop + "']");
            return link == null ? "" : link.InnerHtml;
        }
    }
}
