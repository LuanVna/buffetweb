﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuffetNaWebService.Mapeamento.BuffetNaWeb
{
    public class RegiaoBuffetNaWeb
    {
        public string html { get; set; }

        public List<RegiaoParceiroNaWeb> regiaoParceiroNaWeb
        {
            get
            {
                List<RegiaoParceiroNaWeb> r = new List<RegiaoParceiroNaWeb>();

                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };

                htmlDoc.LoadHtml(this.html);

                for (int i = 0; i < htmlDoc.DocumentNode.SelectNodes("//div[@itemprop='itemListElement']").ToList().Count; i++)
                {
                    try
                    {
                        HtmlNode url = htmlDoc.DocumentNode.SelectNodes("//a[@itemprop='url']")[i];
                        HtmlNode plano = htmlDoc.DocumentNode.SelectNodes("//div[@itemprop='itemListElement']")[i];

                        if (plano != null && url != null && url.Attributes["href"].Value.Contains("detalhe"))
                        {
                            string plano_box = "";
                            switch (plano.Attributes["class"].Value)
                            {
                                case "box bg_green": plano_box = "Platina"; break;
                                case "box bg_blue": plano_box = "Diamante"; break;
                                case "box bg_yellow": plano_box = "Ouro"; break;
                                case "box bg_gray": plano_box = "Prata"; break;
                                case "box-simples bg_white": plano_box = "Normal"; break;
                            }

                            if (plano_box != "")
                            {
                                r.Add(new RegiaoParceiroNaWeb()
                                {
                                    plano = plano_box,
                                    url = url.Attributes["href"].Value
                                });
                            }
                        }
                    }
                    catch (Exception)
                    { }
                }
                return r;
            }
        }

        private string GetTag(string tag, string itemprop)
        {
            var htmlDoc = new HtmlDocument()
            {
                OptionFixNestedTags = true,
                OptionAutoCloseOnEnd = true
            };
            htmlDoc.LoadHtml(this.html);
            var link = htmlDoc.DocumentNode.SelectSingleNode("//" + tag + "[@itemprop='" + itemprop + "']");
            return link.InnerHtml;
        }
    }

    public class RegiaoParceiroNaWeb
    {
        public string plano { get; set; }
        public string url { get; set; }
    }
}
