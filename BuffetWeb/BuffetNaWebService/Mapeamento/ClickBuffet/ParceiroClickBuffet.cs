﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BuffetNaWebService.Mapeamento.ClickBuffet
{
    public class ParceiroClickBuffet
    {
        public string html { get; set; }

        public RegiaoParceiroClickBuffet ParceiroRegiao { get; set; }

        public string nome
        {
            get
            {
                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };
                htmlDoc.LoadHtml(this.html);
                var link = htmlDoc.DocumentNode.SelectSingleNode("//a[@rel='bookmark']");
                return link.InnerHtml;
            }
        }

        public string telefone
        {
            get
            {
                return this.GetTag("Fone: (.*)Site", "Fone: (.*)Site");
            }
        }

        public string descricao
        {
            get
            {
                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };
                htmlDoc.LoadHtml(this.html);
                var link = htmlDoc.DocumentNode.SelectSingleNode("//p");
                return link != null ? link.InnerHtml : "";
            }
        }

        public string site
        {
            get
            {
                return this.GetTag("Fone: (.*)Site", "Fone: (.*)Site");
            }
        }

        public string rua
        {
            get
            {
                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };
                htmlDoc.LoadHtml(this.html);
                var link = htmlDoc.DocumentNode.SelectSingleNode("//p[@style='text-align: justify;']");
                return link != null ? link.InnerHtml : "";
            }
        }

        public string localidade
        {
            get
            {
                return this.GetTag("Endereço: (.*) Fone:", "Endereço: Fone:");
            }
        }

        public List<string> Imagens
        {
            get
            {
                List<string> images = new List<string>();

                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };

                htmlDoc.LoadHtml(this.html);

                foreach (HtmlNode img in htmlDoc.DocumentNode.SelectNodes("//img"))
                {
                    HtmlAttribute att = img.Attributes["src"];
                    if (!att.Value.Contains("orcamento-online") &&
                        !att.Value.Contains("botao-busca-avancada") &&
                        !att.Value.Contains("opo-logo") &&
                        !att.Value.Contains("anuncieaqui") &&
                        att.Value.Contains(".jpg"))
                    {
                        images.Add(att.Value);
                    }
                }
                return images;
            }
        }

        public string video { get; set; }

        private string GetTag(string regex, string replace)
        {
            Regex tagRegex = new Regex(regex, RegexOptions.IgnoreCase);
            MatchCollection valorMatches = tagRegex.Matches(html);
            if (valorMatches.Count != 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Match valorMatch in valorMatches)
                {
                    return valorMatch.Value.Replace(replace, "");
                }
            }
            return "";
        }
    }
}