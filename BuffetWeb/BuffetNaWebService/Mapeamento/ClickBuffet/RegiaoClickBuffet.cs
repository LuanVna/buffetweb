﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuffetNaWebService.Mapeamento.ClickBuffet
{
    public class RegiaoClickBuffet
    {
        public string html { get; set; }
        public List<RegiaoParceiroClickBuffet> parceiroClickBuffet
        {
            get
            {
                List<RegiaoParceiroClickBuffet> parceiros = new List<RegiaoParceiroClickBuffet>();

                var htmlDoc = new HtmlDocument()
                {
                    OptionFixNestedTags = true,
                    OptionAutoCloseOnEnd = true
                };

                htmlDoc.LoadHtml(this.html);

                var parceirosEncontrados = htmlDoc.DocumentNode.SelectNodes("//a").Select(p => p.GetAttributeValue("href", ""));
                foreach (var parceiroEncontrado in parceirosEncontrados)
                {
                    if (parceiroEncontrado.Contains("http://www.clickbuffetssp.com.br/buffet"))
                    {
                        parceiros.Add(new RegiaoParceiroClickBuffet()
                        {
                            url = parceiroEncontrado
                        });
                    }
                }
                return parceiros;
            }
        }
    }

    public class RegiaoParceiroClickBuffet
    {
        public string url { get; set; }
    }
}
