﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetNaWebService.Mapeamento.BuffetNaWeb;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using BuffetNaWebService.Mapeamento.ClickBuffet;

namespace BuffetNaWebService.Mapeamento
{
    public class StatusImportacaoArgs : EventArgs
    {
        public string mensagem { get; set; }
        public int regioes { get; set; }
    }

    public class StatusConcluidoArgs : EventArgs
    {
        public List<B_SERVICO_MAPEAR_REGIAO> regioes { get; set; }
    }
    public class ParceiroAtualizado
    {
        public B_SERVICO_PARCEIRO_MAPEADO parceiro { get; set; }
        public bool isOk { get; set; }
        public string mensagem { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public string logo { get; set; }
    }

    public class MapearConcorrente
    {
        public event EventHandler<StatusImportacaoArgs> OnStatusImportacao;

        public void BuffetNaWeb()
        {
            MapearBuffetNaWeb buffetWeb = new MapearBuffetNaWeb();
            buffetWeb.Start();
        }

        public void ClickBuffet()
        {
            MapearClickBuffet buffetWeb = new MapearClickBuffet();
            buffetWeb.Start();
        }







        public ParceiroAtualizado ForcarMapeamento(int id_parceiro)
        {
            ParceiroAtualizado pAtualizado = new ParceiroAtualizado();
            using (BuffetContext db = new BuffetContext())
            {
                B_SERVICO_PARCEIRO_MAPEADO parceiro = db.B_SERVICO_PARCEIRO_MAPEADO.FirstOrDefault(e => e.id == id_parceiro);
                if (parceiro != null)
                {
                    pAtualizado.parceiro = parceiro;
                    string pagina_parceiro = new WebClient().DownloadString(parceiro.site);
                    if (pagina_parceiro != null)
                    {
                        string logo = this.GetLogo(pagina_parceiro);
                        if (logo != "")
                        {
                            logo = logo.Contains("http") ? logo : parceiro.site + logo;
                            parceiro.logo = logo;

                            pAtualizado.mensagem = "Logo mapeado";
                            pAtualizado.logo = logo;
                        }

                        string email = this.GetRegex(pagina_parceiro, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

                        if (email != "")
                        {
                            parceiro.email = email;
                            pAtualizado.email = email;
                            pAtualizado.mensagem += " Email mapeado";
                        }
                        else
                        {
                            var htmlDoc = new HtmlDocument()
                            {
                                OptionFixNestedTags = true,
                                OptionAutoCloseOnEnd = true
                            };

                            htmlDoc.LoadHtml(pagina_parceiro);

                            var e = htmlDoc.DocumentNode.SelectNodes("//a").Select(p => p.GetAttributeValue("href", "contato"));
                            foreach (string contato in e)
                            {
                                if (contato.Contains("contato") || contato.Contains("Contato"))
                                {
                                    string pagina_contato = new WebClient().DownloadString(contato);
                                    string email_contato = this.GetRegex(pagina_contato, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                                    parceiro.email = email_contato;
                                    pAtualizado.email = email;
                                    pAtualizado.mensagem += " Email mapeado";
                                    break;
                                }
                            }
                            db.SaveChanges();
                        }
                    }
                }
            }
            return pAtualizado;
        }

        private string GetLogo(string html)
        {
            try
            {
                var htmlDoc = new HtmlDocument()
                   {
                       OptionFixNestedTags = true,
                       OptionAutoCloseOnEnd = true
                   };

                htmlDoc.LoadHtml(html);
                var e = htmlDoc.DocumentNode.SelectNodes("//img").Select(p => p.GetAttributeValue("src", ""));
                foreach (string logo in e)
                {
                    if (logo.Contains("logo"))
                    {
                        return logo;
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return "";
        }

        private string GetRegex(string html, string regex)
        {
            Regex emailRegex = new Regex(regex, RegexOptions.IgnoreCase);
            MatchCollection valorMatches = emailRegex.Matches(html);
            if (valorMatches.Count != 0)
            {
                StringBuilder sb = new StringBuilder();

                foreach (Match valorMatch in valorMatches)
                {
                    return valorMatch.Value;
                }
            }
            return "";
        }

        public void CallEvent(string mensagem, int regioes, int total_parceiros, int importando_parceiro)
        {
            if (this.OnStatusImportacao != null)
            {
                this.OnStatusImportacao(this, new StatusImportacaoArgs()
                {
                    mensagem = mensagem,
                    regioes = regioes
                });
            }
        }




    }
}
