﻿<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>

    <style>
        __FONTES_SELECIONADAS__
    </style>
</head>

<body>

    <table>
        <tr>
            <td align="center">
                <a href="__DOMINIO_EMPRESA__">
                    <img src="__LOGO_EMPRESA__" />
                </a>
            </td>
        </tr>

        <tr>
            <td align="center">
                <img src="http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Logo_Convite_Online.png" />
            </td>
        </tr>
		
        <tr>
            <td align="center" width="500">
                <div style="font-size: 20px; font-family: 'Hobo Std'">
                    Olá __NOME_CONVIDADO__
                </div>
                <div style="font-size: 20px; font-family: 'Hobo Std'" >
                    Você recebeu um convite de aniversário de __ANIVERSARIANTES__ <p />
                    Clique no botão para visualizar e confirmar presença
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
               <div style="margin-top:20px">
					<a href="__DOMINIO_BUFFETWEB__/Convidados/ResponderConvite?autenticacao=__AUTENTICACAO__">
						<img src="http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Visualizar_Convite.png" />
					</a>
			   </div>
            </td>
        </tr>

        <tr>
            <td align="center">
                <div style="font-family= 'Hobo Std';margin-top:20px">
                    Caso não esteja visualizando este email corretamente, <a href="__DOMINIO_BUFFETWEB__/Convidados/ResponderConvite?autenticacao=__AUTENTICACAO__">clique aqui</a>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>