﻿function v(obj) {
    return document.getElementById(obj).value;
}

function o(obj) {
    return document.getElementById(obj);
}

function DIBaseValue(obj) {
    return v(obj);
}

function DIBaseObj(obj) {
    return o(obj);
} 

function DISubmit() {
    document.getElementById('form').submit();
}

function DISubmit(id) {
    document.getElementById(id).submit();
}

function DIBaseNextFocus(obj, to, length) {
    if (obj.value.length == length) {
        document.getElementById(to).focus();
    }
}

function DIBaseNextSubmit(obj, length, submit_id) {
    if (obj.value.length == length) {
        document.getElementById(submit_id).submit();
    }
}

function DIBaseNextFunction(obj, length, functionCallBack) {
    if (obj.value.length == length) {
        functionCallBack();
    }
}

function DIBaseIsEmptyAnd() {
    for (var i = 0; i < arguments.length; i++) {
        if (v(arguments[i]) == '') {
            return true;
        }
    }
    return false;
}

function DIMask(src, mask) {
    var i = src.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i)
    if (texto.substring(0, 1) != saida) {
        src.value += texto.substring(0, 1);
    }
}

function DIBaseIsEmptyOr() {
    var args = 0;
    for (var i = 0; i < arguments.length; i++) {
        if (v(arguments[i]) == '') {
            args++
        }
    }
    return args == arguments.length;
}

function DIBaseArguments() {
    var args = '/?&';
    for (var i = 0; i < arguments.length; i++) {
        args += arguments[i] +"="+ v(arguments[i]) +"&";
    }
    return args;
}

function DIBaseEmpty(obj) {
    return v(obj) == '';
}

function DIOnlyNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function DIstartWith(value, startWith) { 
    var start = false;
    for (var i = 0; i < startWith.length; i++) {
        if (i < value.length) {
            if (value[i] == startWith[i]) {
                start = true;
            } else {
                start = false;
            }
            if (value[i] != startWith[i]) {
                return false;
            } 
        } else {
            return false;
        }
    }
    return start; 
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function FiltrarTabelas(evt) {
    //var id = "#" + evt.id + " tr";
    $("#search2 tr").each(function (index) {
        if (index !== 0) {
            $row = $(this);
            var text = $row.find("td").text();
            if (evt.value.length == 0) {
                $row.show();
            } else {
                found = false;
                var regExp = new RegExp(evt.value, 'i');
                if (regExp.test(text)) {
                    found = true;
                }
                found ? $row.show() : $row.hide();
            }
        }
    });
}

function ConvertDate(data) {
    var re = /-?\d+/;
    var m = re.exec(data);
    var d = new Date(parseInt(m[0]));
    return +(d.getUTCDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear());
}