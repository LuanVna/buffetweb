﻿//CONFIGURACAO DA CLASSE UI
var tempo = 1000; //TEMPO


//MENSAGENS DE ALERTA 
var tipoMensagem = {
    SUCESSO: 0,
    ERRO: 1,
    NONE: 2,
    INFO: 3
}

var VALIDAR = {
    VAZIO: 0
}

function UIBaseAlert(titulo, mensagme, tipoMensagem) {
    var tipo = '';
    switch (tipoMensagem) {
        case 0: tipo = 'success'; break;
        case 1: tipo = 'error'; break;
        case 2: tipo = 'none'; break;
        case 3: tipo = 'info'; break;
    }
    $.pnotify({ title: titulo, text: mensagme, type: tipo, opacity: .8 });
}

function UIBaseMensagem(text, idMessage) {
    o(idMessage).innerHTML = text;
    o(idMessage).style.display = "block";
}

function UIBaseMensagem(text, id) {
    o(id).innerHTML = text;
    o(id).style.display = "block";
}

function UIBaseAlertSUCESSO(mensagem) {
    UIBaseAlert('Legal!', mensagem, tipoMensagem.SUCESSO);
}

function UIBaseAlertNONE(mensagem) {
    UIBaseAlert('Oops!', mensagem, tipoMensagem.NONE);
}

function UIBaseAlertERRO() {
    UIBaseAlert('ERRO!', 'Ouve algum erro, atualize a pagina e tente novamente!', tipoMensagem.ERRO);
} 

function UIBaseAlertINFO(mensagem) {
    UIBaseAlert('Informacao!', mensagem, tipoMensagem.INFO);
} 

function UIBaseRefreshShow() {
    var refresh = '<div id="refresh" class="widgetrefresh" style="display: block;"><span><i class="fa fa-spinner fa-spin fa-3x"></i></span></div>';
    document.getElementById('divRefresh').innerHTML += (refresh);
}

function UIBaseRefreshHide() {
    $("#refresh").remove(); 
}

function onSelectSomes(evt, id_component) {
    if (o('data').dataset.multiselect == "True") {
        if (evt.className == "onSelect") {
            o(id_component).value = false;
            evt.className = "ofSelect";
        } else {
            o(id_component).value = true;
            evt.className = "onSelect";
        }
    } else {
        if (evt.className == "onSelect") {
            o(id_component).value = false;
            evt.className = "ofSelect";
            return;
        }
        for (var i = o('data').dataset.total - 1; i != -1; i--) {
            o(i).className = "ofSelect";
            o(i + 'selecionado').value = false;
        }
        o(id_component).value = true;
        evt.className = "onSelect";
    }
}

function onSelectOnlyOne(evt) {
    if (evt.className == "onSelect") { 
        evt.className = "ofSelect";
        return;
    }
    for (var i = o('data').dataset.total - 1; i != -1; i--) {
        o(i).className = "ofSelect"; 
    } 
    evt.className = "onSelect";
}
 

function BuscarOperadora(evt) {
    if (evt.value.length == 13) {
        $.getJSON('/Convidados/BuscarOperadora', { telefone: evt.value }, function (operadora) {
            if (operadora.Item1 == true) {
                if (operadora.Item2 != "invalido") {
                    o('mensagem').style.display = "none";
                    o('operadora').value = operadora.Item2;
                    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + operadora.Item2 + ".png";
                    o("telefone_principal").setAttribute("style", "height: 44px; background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;padding-left: 50px;width: 200px;background-color: white;");

                    o('Celular_Valido').value = true;
                } else {
                    if (o('mensagem') != null) {
                        o('mensagem').style.display = "block";
                        o('textMensagem').innerHTML = "Por favor informe um celular válido ( com DDD ) ";
                    }
                    o('Celular_Valido').value = false;
                }
            }
        }, 'json');
    }
}