﻿window.onload = ConvidadosParametros;
function ConvidadosParametros() {
    var tempo = ["100",
        "200",
        "300",
        "400",
        "500",
        "600",
        "700", 
        "800"];
    for (var i = 0; i < tempo.length; i++) {
        setTimeout(function () {
            if (document.getElementById('revslider') != null && document.getElementById('isMobile') != null) {
                document.getElementById('revslider').style.height = "700px";
            }
        }, tempo[i]);
    }
}

function verificaCampos(count) {
    for (var i = 0; i < count; i++) {
        if (o('[' + i + ']isOn').value == "true") {
            return true;
        }
    }
    return false;
}

function ValidaCampos(evt) {
    if (o('nome').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe o nome do convidado!";
        return;
    }
    if (o('email').value == "" && o('telefone_principal').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Você precisa informar no mínimo o Telefone ou Email do convidado!";
        return;
    }

    if (o('email').value.length != 0) {
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        if (!re.test(o('email').value)) {
            o('mensagem').style.display = "block";
            o('textMensagem').innerHTML = "O e-mail não está em um formato válido!";
            return;
        }
    }

    if (o('Celular_Valido').value == "false") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe um Celular valido ( com DDD ) ";
        return;
    }

    o('adicionarConvidado').submit();
}



function AdicionaFamiliares(evt) {
    o('containerConvidados').innerHTML += linhaFamiliar(evt.dataset.quantidade);
    evt.dataset.quantidade++;
}

function linhaFamiliar(linha) {
    return '<div class="form-group">' +
                '<div class="span4">' +
                    '<label>Nome</label>' +
                    '<input type="text" name="[' + linha + '].nome" class="form-control span12" />' +
                '</div>' +
                '<div class="span2">' +
                    '<label>Tipo Convidado</label>' +
                    '<select class="form-control" name="[' + linha + '].tipo_convidado">' +
                        '<option value="Adulto">Adulto</option>' +
                        '<option value="Criança Ate 5">Criança até 5 anos</option>' +
                        '<option value="Criança Acima 5">Criança acima 5 anos</option>' +
                    '</select>' +
                '</div>' +
            '</div>';
}

function BuscaOperadora(evt) {
    o('operadora').value = "";
    if (evt.value.length == 0) {
        o('Celular_Valido').value = "";
        o('mensagem').style.display = "none";
    } else if (evt.value.length == 13) {
        $.getJSON('/Convidados/BuscarOperadora', { telefone: evt.value }, function (operadora) {
            if (operadora.Item1 == true) {
                if (operadora.Item2 != "invalido") {
                    o('mensagem').style.display = "none";
                    o('operadora').value = operadora.Item2;
                    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + operadora.Item2 + ".png";
                    o("telefone_principal").setAttribute("style", "height: 44px; background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;padding-left: 50px;width: 200px;background-color: white;");

                    o('Celular_Valido').value = true;
                } else {
                    if (o('mensagem') != null) {
                        o('mensagem').style.display = "block";
                        o('textMensagem').innerHTML = "Por favor informe um celular válido ( com DDD ) ";
                    }
                    o('Celular_Valido').value = false;
                }
            }
        }, 'json');
    }
}


function BuscaOperadoraEditar(evt) {
    o('operadora').value = "";
    if (evt.value.length == 0) {
        o('numero_valido').value = "";
        o('editar_mensagem').style.display = "none";
    } else if (evt.value.length == 13) {
        $.getJSON('/Convidados/BuscarOperadora', { telefone: evt.value }, function (operadora) {
            if (operadora.Item1 == true) {
                if (operadora.Item2 != "invalido") {
                    o('editar_mensagem').style.display = "none";
                    o('editar_operadora').value = operadora.Item2;
                    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + operadora.Item2 + ".png";
                    o("editar_telefone").setAttribute("style", "height: 44px; background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;padding-left: 50px");

                    o('numero_valido').value = true;
                } else {
                    if (o('editar_mensagem') != null) {
                        o('editar_mensagem').style.display = "block";
                        o('editar_textMensagem').innerHTML = "Por favor informe um celular válido ( com DDD ) ";
                    }
                    o('numero_valido').value = false;
                }
            }
        }, 'json');
    }
}

function BuscarOperadoraAnony(evt) {
    o('operadora').value = "";
    if (evt.value.length == 0) {
        o('Celular_Valido').value = "";
        o('mensagem_informacoes').style.display = "none";
    } else if (evt.value.length == 13) {
        $.getJSON('/Convidados/BuscarOperadoraAnony', { telefone: evt.value, autenticacao: o('convidado_data').dataset.autenticacao }, function (operadora) {
            if (operadora.Item1 == true) {
                if (operadora.Item2 != "invalido") {
                    o('mensagem').style.display = "none";
                    o('operadora').value = operadora.Item2;
                    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + operadora.Item2 + ".png";
                    o("telefone_principal").setAttribute("style", " background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;padding-left: 50px;width: 400px;background-color: white;");

                    o('Celular_Valido').value = true;
                } else {
                    o('mensagem_informacoes').style.display = "block";
                    o('textMensagem_informacoes').innerHTML = "Por favor informe um celular válido ( com DDD ) ";
                    o('Celular_Valido').value = false;
                }
            }
        }, 'json');
    }
}







//right no-repeat content-box;

function AdicionarFamiliar(evt, id_convidado) {
    var nome = o('nome_' + id_convidado);
    var tipo_convidado = o('tipo_convidado_' + id_convidado);
    var tabela = o('tabela_' + id_convidado);
    if (nome.value == "" || tipo_convidado.value == "") {
        return;
    }
    evt.value = "Aguarde..."
    $.getJSON('/Convidados/AdicionarFamiliar', { nome: nome.value, tipo_convidado: tipo_convidado.value, id_convidado: id_convidado }, function (familiar) {
        if (familiar.isOk) {
            evt.value = "Adicionar";
            var tabela = o('tabela_' + id_convidado);
            tabela.innerHTML += '<tr id="familiar_' + familiar.id + '"><td>' + familiar.nome + '</td><td>' + familiar.tipo_convidado + '</td><td><input type="button" onclick="ApagarFamiliar(this, ' + familiar.id + ',' + id_convidado + ')" class="btn btn-xs btn-danger btn-block" value="Apagar" /></td></tr>';
            nome.value = "";
            tipo_convidado.value = "Adulto";
            o('div_' + id_convidado).style.display = "block";

            var detalhes = o('detalhes_' + id_convidado);
            detalhes.dataset.familiares = parseInt(detalhes.dataset.familiares) + 1;
            detalhes.value = "Convidados ( " + detalhes.dataset.familiares + " )";

            var total_convidados = o('total_convidados');
            total_convidados.dataset.total = parseInt(total_convidados.dataset.total) + 1;
            total_convidados.innerHTML = total_convidados.dataset.total;
        }
    }, 'json');
}

function ApagarFamiliar(evt, id_familiar, id_convidado) {
    evt.value = "Aguarde...";
    $.getJSON('/Convidados/ApagarFamiliar', { id_familiar: id_familiar }, function (resposta) {
        if (resposta.isOk) {
            $('#familiar_' + id_familiar).remove();
            var detalhes = o('detalhes_' + id_convidado);
            detalhes.dataset.familiares = parseInt(detalhes.dataset.familiares) - 1;
            o('detalhes_' + id_convidado).value = "Convidados ( " + detalhes.dataset.familiares + " )";

            var total_convidados = o('total_convidados');
            total_convidados.dataset.total = parseInt(total_convidados.dataset.total) - 1;
            total_convidados.innerHTML = total_convidados.dataset.total;
        }
    }, 'json');
}

function ConfirmaPresencao(evt, codStatus) {
    var data = o('convidado_data').dataset;
    $.getJSON('/Convidados/ConfirmarPresenca', { autenticacao: data.autenticacao, codigo_unico: data.codigo_unico, email: data.email, codStatus: codStatus }, function (resposta) {
        if (resposta) {

            o("presenca1").style.display = "none";
            o("presenca2").style.display = "none";
            o("presenca3").style.display = "none";
            o("frase").style.display = "none";
            o("frase2").innerHTML = o('convidado_data').dataset.nome;

            if (data.email == "" || data.telefone == "" || codStatus != 2) {
                if (codStatus != 3) {
                    o("dadosPessoais").style.display = "block";
                    o('levar').style.display = "block";
                    if (codStatus == 2) {
                        o('talvez_levar').innerHTML = "Se for, vai levar alguem?";
                    }
                } else {
                    o("mensagem").style.display = "block";
                    o('dadosPessoais').style.display = "none";
                }
            } else {
                o("mensagem").style.display = "block";
            }

        } else {

        }
    }, 'json');
}

function EnviarMensagem(evt) {
    var data = o('convidado_data').dataset;
    $.getJSON('/Convidados/EnviarMensagem', { codigo_unico: data.codigo_unico, autenticacao: data.autenticacao, mensagem: o('mensagem_enviada').value }, function (resposta) {
        if (resposta.isOk) {
            o("mensagem").style.display = "none";
            o("gratidao").style.display = "block";
            o('url_espaco').href = resposta.url_espaco;
        } else {

        }
    }, 'json');
}

function SalvarInformacoes(evt) {

    if (o('email_convidado') != null && o('email_convidado').value.length > 0) {
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        if (!re.test(o('email_convidado').value)) {
            o('mensagem_informacoes').style.display = "block";
            o('textMensagem_informacoes').innerHTML = "O e-mail não está em um formato válido!";
            return false;
        }
    }

    if (o('telefone_principal') != null && o('telefone_principal').value.length > 0) {
        if (!o('Celular_Valido').value == "" && o('Celular_Valido').value == false) {
            o('mensagem_informacoes').style.display = "block";
            o('textMensagem_informacoes').innerHTML = "informe um celular válido!";
        }
    }

    var data = o('convidado_data').dataset;
    $.getJSON('/Convidados/AtualizarInformacoes',
        {
            autenticacao: data.autenticacao,
            codigo_unico: data.codigo_unico,
            email: (o('email_convidado') != null ? o('email_convidado').value : null),
            telefone: (o('telefone_principal') != null ? o('telefone_principal').value : null),
            operadora: (o('operadora') != null ? o('operadora').value : null),
            convidados: o('convidados').value
        }, function (resposta) {
            if (resposta.isOk) {
                o("dadosPessoais").style.display = "none";
                o("gratidao").style.display = "none";
                o("mensagem").style.display = "block";
                o('mensagem_informacoes').style.display = "none";
            } else {
                o('mensagem_informacoes').style.display = "block";
                o('textMensagem_informacoes').innerHTML = "Ouve algum erro, atualize a pagina e tente novamente!";
            }
        }, 'json');

}

function checkOrUncheck(evt, id) {
    var data = evt.dataset;
    if (data.checked == "true") {
        evt.innerHTML = '<img src="/Content/Buffet/Imagens/Convite/check.png"  width="50" /> ';
        o(id).value = false;
        data.checked = false;
    } else {
        evt.innerHTML = '<img src="~/Content/Buffet/Imagens/Convite/uncheck.png" width="50" /> ';
        o(id).value = true;
        data.checked = true;
    }
}

function SelecionarTodos(evt, count) {
    for (var i = 0; i < count + 1; i++) {
        if (evt.dataset.checked == "false") {
            if (o('[' + i + ']isOn') != null && o('[' + i + ']checkeds') != null) {
                o('[' + i + ']checkeds').innerHTML = '<img src="/Content/Buffet/Imagens/Convite/check.png" /> ';
                o('[' + i + ']isOn').value = 'true';
                o('[' + i + ']checkeds').dataset.checked = "true";
            }
            evt.innerHTML = '<img src="/Content/Buffet/Imagens/Convite/check.png" /> ';
        } else {
            if (o('[' + i + ']isOn') != null && o('[' + i + ']checkeds') != null) {
                o('[' + i + ']checkeds').innerHTML = '<img src="/Content/Buffet/Imagens/Convite/uncheck.png" /> ';
                o('[' + i + ']isOn').value = 'false';
                o('[' + i + ']checkeds').dataset.checked = "false";
            }
            evt.innerHTML = '<img src="/Content/Buffet/Imagens/Convite/uncheck.png" /> ';
        }
    }
    evt.dataset.checked = evt.dataset.checked == "true" ? "false" : "true";
}


function editarConvidado(evt) {
    var data = evt.dataset;
    o('editar_id').value = data.id;
    o('editar_email').value = data.email;
    o('editar_telefone').value = data.telefone;
    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + data.operadora + ".png";
    o("editar_telefone").setAttribute("style", "height: 44px; background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;padding-left: 50px;");
    if (data.telefone.length > 0) {
        o('numero_valido').value = true;
    }

    o('editar_nome').value = data.nome;
    if (data.isfacebook == "True") {
        o('imagem_facebook').scr = ('http://graph.facebook.com/' + data.id_facebook + '/picture?width=140&height=140');
        o('div_imagem_facebook').style.display = "block";
    } else {
        o('div_imagem_facebook').style.display = "none";
    }

    o('convidados').value = data.convidados;

    if (data.nao_confirmado != "True") {
        o('editar_email').disabled = true;
        o('editar_telefone').disabled = true;
        o('convidados').disabled = true;
    } else {
        o('editar_email').disabled = false;
        o('editar_telefone').disabled = false;
        o('editar_nome').disabled = false;
        o('convidados').disabled = false;
    }

    $('#EditarConvidado').modal('show');
}


function ValidaCamposEditar() {
    var email = o('editar_email').value;
    if (o('editar_email').disabled != true && o('editar_email').value.length != 0) {
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
        if (!re.test(o('editar_email').value)) {
            o('editar_mensagem').style.display = "block";
            o('editar_textMensagem').innerHTML = "O e-mail não está em um formato válido!";
            return false;
        }
    }

    if (o('editar_telefone').disabled != true && o('editar_telefone').value.length > 0 && !o('numero_valido').value) {
        o('editar_mensagem').style.display = "block";
        o('editar_textMensagem').innerHTML = "Informe um Celular valido ( com DDD ) ";
        return false;
    }

    o('confirmarEditar').submit();
}

function SalvarMensagem(evt, id_mensagem, autenticacao, id_convidado) {
    evt.innerHTML = "Aguarde...";
    $.getJSON('/Convidados/SalvarMensagem', { mensagem: o(id_mensagem).value, autenticacao: autenticacao }, function (resposta) {
        //if (resposta.isOk) {
        //    evt.innerHTML = "Salvar Mensagem";
        //    if (id_convidado != "") {
        //        o('convidado_' + id_convidado).dataset.mensagem_padrao = o(id_mensagem).value;
        //    }
        //    //$('#EditarConvidado').modal('hide')
        //}
    }, 'json');
}

function ApagarConvidado(evt, id_convidado) {
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_lixo_load.gif" width="40" />';
    $.getJSON('/Convidados/ApagarConvidado', { id_convidado: id_convidado }, function (resposta) {
        if (resposta.isOk) {
            o('tr_' + id_convidado).remove();
        } else {
            alert(resposta.mensagem);
        }
    }, 'json');
}