﻿isFriends = false;
amigo_de = "";
amigo_de_id_facebook = "";
reloadModalFacebook = false;

window.fbAsyncInit = function () {
    FB.init({
        appId: '527135120709685', //<- Produção,
        //appId: '263049853734550', //<- Desenvolvimento,
        status: true,
        xfbml: true,
        version: 'v1.0'
    }), { scope: 'publish_actions, user_birthday, user_friends' };

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

    //$("#search").on("keyup", function () {
    //    var value = $(this).val();

    //    $("#tableFacebook tr").each(function (index) {
    //        if (index !== 0) {
    //            $row = $(this);
    //            var text = $row.find("td").text();
    //            var contains = false;
    //            for (var i = 0; i < text.length; i++) {
    //                if (text[i] == value) {
    //                    contains = true;
    //                    break;
    //                }
    //            }

    //            if (contains) {
    //                $row.show();
    //            }
    //            else {
    //                $row.hide();
    //            }
    //        }
    //    });
    //});

    $("#search2").on("keyup", function () {

    });

    $('#friendsFacebook').on('show.bs.modal', function () {
        reloadModalFacebook = false;
    });
    $('#friendsFacebook').on('hidden.bs.modal', function () {
        if (reloadModalFacebook) {
            location.reload();
        }
    });
};

function BuscarAmigosTabela(evt) { 
    $("#convidados tr").each(function (index) {
        if (index !== 0) {
            $row = $(this);
            var text = $row.find("td").text();
            if (evt.value.length == 0) {
                $row.show();
            } else {
                found = false;
                var regExp = new RegExp(evt.value, 'i');
                if (regExp.test(text)) {
                    found = true;
                }
                found ? $row.show() : $row.hide();
            }
        }
    });
}


function DefineAbasTab(userID) {
    var perfis = o('data_facebook').dataset.perfis.split(',');
    for (var i = 0; i < perfis.length; i++) {
        if (perfis[i] != "" && perfis[i] == userID) {
            o('atualizar_' + perfis[i]).style.display = "block";
            o('conectar_' + perfis[i]).style.display = "none";
            o('desconectar_' + perfis[i]).style.display = "block";
            o('remover_' + perfis[i]).style.display = "block";
        } else {
            if (o('atualizar_' + perfis[i]) != null) {
                o('atualizar_' + perfis[i]).style.display = "none";
                o('conectar_' + perfis[i]).style.display = "block";
                o('desconectar_' + perfis[i]).style.display = "none";
                o('remover_' + perfis[i]).style.display = "block";
            }
        }
    }
}

function Desconectar(evt) {
    FB.logout(function (response) {
        $('#friendsFacebook').modal('hide');
        location.reload();
    });
}

function Conectar(evt) {
    FB.logout(function (response) {
        FB.login(function (response) {
            if (response.authResponse) {
                location.reload();
            } else {
                console.log('Usuario nao permite!');
            }
        });
    });
}

function Atualizar(evt) {
    $.getJSON('/Convidados/AtualizarAmigos', { id_facebook: evt.dataset.id_facebook }, function (data) {
        if (data.isOk) {
            FB.login(function (response) {
                if (response.authResponse) {
                    $('#friendsFacebook').modal('hide');
                    FB.api('/me', function (me) {
                        amigo_de_id_facebook = me.id;
                        amigo_de = me.name;
                        FB.api('/me/friends', function (response) {
                            if (response && !response.error) {
                                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_facebook_load.gif" width="50" />';
                                for (var i = 0; i < response.data.length; i++) {
                                    uploadFriends(response.data[i], response.data.length, evt);
                                }
                            }
                        });
                    });
                } else {
                    console.log('Usuario nao permite!');
                }
            });
        }
    }, 'json');
};

function Remover(evt) {
    $.getJSON('/Convidados/RemoverConta', { id_facebook: evt.dataset.id_facebook }, function (data) {
        if (data.isOk) {
            location.reload();
        } else {
            alert('Ouve algum erro, atualize a pagina!');
        }
    }, 'json');
}


//<a href="javascript:void(o)" onclick="Atualizar(this)" id="@string.Format("atualizar_{0}", perfil.id_facebook)">
//                                                   <img src="~/Content/Buffet/Convidados/Imagens/icone_atualizar.png" width="40" height="40" />
//                                               </a>
//                                               <a href="javascript:void(o)" onclick="Conectar(this)" id="@string.Format("conectar_{0}", perfil.id_facebook)">
//                                                   <img src="~/Content/Buffet/Convidados/Imagens/icone_conectar.png" width="40" height="40" />
//                                               </a>
//                                               <a href="javascript:void(o)" onclick="Desconectar(this)" id="@string.Format("desconectar_{0}", perfil.id_facebook)">
//                                                   <img src="~/Content/Buffet/Convidados/Imagens/icone_desconectar.png" width="40" height="40" />
//                                               </a>
//                                               <a href="javascript:void(o)" onclick="Remover(this)" id="@string.Format("remover_{0}", perfil.id_facebook)">
//                                                   <img src="~/Content/Buffet/Convidados/Imagens/icone_remover_conta.png" width="40" height="40" />
//                                               </a>


adicionados = 0;
function uploadFriends(friend, total, evt) {
    FB.api('/' + friend.id, function (response) {
        if (response && !response.error) {
            var request = new Object();
            request.name = response.name;
            request.first_name = response.first_name;
            request.gender = response.gender;
            request.id_facebook = response.id;
            request.last_name = response.last_name;
            request.link = response.link;
            request.locale = response.locale;
            request.amigo_de_nome = amigo_de;
            request.amigo_de_id_facebook = amigo_de_id_facebook;

            $.getJSON('/Convidados/SalvaAmigosFacebook', request, function (data) {
                if (data.isOk) {
                    addFriendsInTable(request);
                    if (adicionados >= total) {
                        location.reload();
                    }
                    adicionados++;
                }
            }, 'json');
        }
    });
}


function searchFriendsInBuffetWeb() {
    $.getJSON('/Convidados/AmigosFacebook', function (f) {
        isFriends = f.isOk;
        if (f.isOk) {
            for (var i = 0; i < f.friends.length; i++) {
                addFriendsInTable(f.friends[i]);
            }
        }
    }, 'json');
}

function addFriendsInTable(friend) {
    $('#tableFacebook > tbody:last').append('<tr id="row_friend_' + friend.id_facebook + '"> <td width="50"><img src="http://graph.facebook.com/' + friend.id_facebook + '/picture"style=" border-radius: 50px; " width="50"/></td> <td><h2>' + friend.name + '</h2></td><td align="right"><a href="javascript:void(o)" onclick="adicionarAmigo(this)" data-id="' + friend.id + '" data-id_facebook="' + friend.id_facebook + '" data-nome="' + friend.name + '"  class="btn btn-primary btn-lg">Adicionar</a> </td> </tr>');
}


function adicionarAmigo(evt) {
    if (o('acompanhantes_' + evt.dataset.id_facebook).value != "") {
        $.getJSON('/Convidados/AdicionarAmigoFacebookALista', { id_facebook: evt.dataset.id_facebook, acompanhantes: o('acompanhantes_' + evt.dataset.id_facebook).value }, function (data) {
            evt.innerHTML = "Aguarde...";
            if (data.isOk) {
                o('row_friend_' + evt.dataset.id_facebook).remove();
                reloadModalFacebook = true;
            }
        }, 'json');
    }
}

function listaDeAmigos(evt) {
    if (evt.innerHTML != '<img src="/Content/Buffet/Convidados/Imagens/icone_facebook_load.gif" width="50">') {
        FB.api('/me', function (me) {
            if (typeof me.error !== "undefined" && me.error.code == 2500) {
                FB.login(function (response) {
                    if (response.authResponse) {
                        FB.api('/me', function (me) {
                            verificaAtualizar(me, evt);
                        });
                    } else {
                        console.log('Usuario nao permite!');
                    }
                });
            } else {
                verificaAtualizar(me, evt);
            }
        });
    } else {

    }
}

function verificaAtualizar(me, evt) {
    amigo_de_id_facebook = me.id;
    amigo_de = me.name;

    var perfis = o('data_facebook').dataset.perfis.split(',');

    var contains = false;
    for (var i = 0; i < perfis.length; i++) {
        var id_facebook = perfis[i];
        if (id_facebook == me.id) {
            contains = true;
            break;
        }
    }

    if (!contains) {
        FB.api('/me/friends', function (response) {
            if (response && !response.error) {
                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_facebook_load.gif" width="50" />';
                for (var i = 0; i < response.data.length; i++) {
                    uploadFriends(response.data[i], response.data.length, evt);
                }
            }
        });
    } else {
        DefineAbasTab(me.id);
        o('nome_logado_' + amigo_de_id_facebook).innerHTML = amigo_de + " ( Conectado )";
        $('#tab_friends a[href="#' + amigo_de.split(' ')[0] + '"]').tab('show');
        $('#friendsFacebook').modal('show');
    }
}


// Verifica o status de login do usuario
function statusChangeCallback(response) {
    //Conectado
    if (response.status === 'connected') {
        DefineAbasTab(response.authResponse.userID);
    } else if (response.status === 'not_authorized') {
    } else {
        //Nao utiliza o aplicativo 
    }
}

function EnviarMensagemFacebook(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_load.gif" width="40" />';
    $.getJSON('/Convidados/EnviarConviteFacebook', { id_facebook: data.id_facebook, autenticacao: data.autenticacao, mensagem: data.mensagem }, function (resposta) {
        FB.login(function (response) {
            if (response.authResponse) {
                FB.ui({
                    app_id: '527135120709685',
                    method: 'send',
                    name: resposta.titulo,
                    to: data.id_facebook,
                    link: resposta.url,
                    //message: evt.mensagem_padrao,
                    description: resposta.titulo
                }, function callback(response) {
                    if (response.success) {
                        $.getJSON('/convidados/ConfirmarConviteFacebook', { id_facebook: data.id_facebook, autenticacao: data.autenticacao }, function (resposta) {
                            if (resposta.isOk) {
                                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_send.png" width="40" />';
                                RemoverBotaoApagar(data.autenticacao);
                            } else {
                                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_error.png" width="40" />';
                            }
                        }, 'json');
                    } else {
                        evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_error.png" width="40" />';
                    }
                });
            } else {
                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_error.png" width="40" />';
                alert('Usuario nao permite!');
            }
        });
    }, 'json');
}

function EnviarEmail(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_load.gif" width="40" />';
    $.getJSON('/Convidados/EnviarConviteEmail', { email: data.email, autenticacao: data.autenticacao, mensagem: data.mensagem }, function (resposta) {
        if (resposta.isOk) {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_send.png" width="40" />';
            RemoverBotaoApagar(data.autenticacao);
        } else {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_error.png" width="40" />';
        }
    }, 'json');
}

function EnviarSMS(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_load.gif" width="40" />';
    $.getJSON('/Convidados/EnviarConviteSMS', { telefone: data.telefone, autenticacao: data.autenticacao, mensagem: data.mensagem }, function (resposta) {
        if (resposta.isOk) {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_send.png" width="40" />';
            RemoverBotaoApagar(data.autenticacao);
        } else {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_error.png" width="40" />';
        }
    }, 'json');
}

function RemoverBotaoApagar(autenticacao) {
    if (o('apagar_' + autenticacao) != null) {
        o('apagar_' + autenticacao).style.display = "none";
    }
}