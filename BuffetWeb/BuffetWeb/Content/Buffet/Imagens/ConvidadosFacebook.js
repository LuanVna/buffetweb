﻿isFriends = false;
amigo_de = "";
amigo_de_id_facebook = "";

window.fbAsyncInit = function () {
    FB.init({
        appId: '527135120709685',
        status: true,
        xfbml: true,
        version: 'v1.0'
    }), { scope: 'publish_actions, user_birthday, user_friends' };

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

    this.searchFriendsInBuffetWeb();

    $("#search").on("keyup", function () {
        var value = $(this).val();

        $("#tableFacebook tr").each(function (index) {
            if (index !== 0) {
                $row = $(this);
                var text = $row.find("td").text();
                var contains = false;
                for (var i = 0; i < text.length; i++) {
                    if (text[i] == value) {
                        contains = true;
                        break;
                    }
                }

                if (contains) {
                    $row.show();
                }
                else {
                    $row.hide();
                }
            }
        });
    });
};

function uploadFriends(friend) {
    FB.api('/' + friend.id, function (response) {
        if (response && !response.error) {
            var request = new Object();
            request.name = response.name;
            request.first_name = response.first_name;
            request.gender = response.gender;
            request.id_facebook = response.id;
            request.last_name = response.last_name;
            request.link = response.link;
            request.locale = response.locale;
            request.amigo_de = amigo_de;
            request.amigo_de_id_facebook = amigo_de_id_facebook;

            $.getJSON('/Convidados/SalvaAmigosFacebook', request, function (data) {
                if (data.isOk) {
                    addFriendsInTable(request);
                }
            }, 'json');
        }
    });
}


function searchFriendsInBuffetWeb() {
    $.getJSON('/Convidados/AmigosFacebook', function (f) {
        isFriends = f.isOk;
        if (f.isOk) {
            for (var i = 0; i < f.friends.length; i++) {
                addFriendsInTable(f.friends[i]);
            }
        }
    }, 'json');
}

function addFriendsInTable(friend) {
    $('#tableFacebook > tbody:last').append('<tr id="row_friend_' + friend.id_facebook + '"> <td width="50"><img src="http://graph.facebook.com/' + friend.id_facebook + '/picture"style=" border-radius: 50px; " width="50"/></td> <td><h2>' + friend.name + '</h2></td><td align="right"><a href="javascript:void(o)" onclick="adicionarAmigo(this)" data-id="' + friend.id + '" data-id_facebook="' + friend.id_facebook + '" data-nome="' + friend.name + '"  class="btn btn-primary btn-lg">Adicionar</a> </td> </tr>');
}


function adicionarAmigo(evt) {
    if (evt.innerHTML == "Adicionar") {
        $.getJSON('/Convidado/AdicionarAmigoFacebookALista', { id_facebook: evt.dataset.id_facebook }, function (data) {
            evt.innerHTML = "Aguarde...";
            if (data.isOk) {
                o('row_friend_' + evt.dataset.id_facebook).remove();
            }
        }, 'json');


    } else {
        evt.innerHTML = "Adicionar"
    }
}

function listaDeAmigos(evt) {
    FB.api('/me', function (me) {
        amigo_de_id_facebook = me.id;
        amigo_de_ = me.name;

        if (!isFriends) {
            FB.api('/me/friends', function (response) {
                if (response && !response.error) {
                    for (var i = 0; i < response.data.length; i++) {
                        uploadFriends(response.data[i]);
                    }
                }
            });
        } else {
            $('#friendsFacebook').modal('show')
        }
    });
}

// Verifica o status de login do usuario
function statusChangeCallback(response) {
    //Conectado
    if (response.status === 'connected') {
        //document.getElementById('amigosDoFacebook').innerHTML = 'Meus Amigos <i class="ifc-facebook"></i>';
        //nao autorizado
    } else if (response.status === 'not_authorized') {
        //document.getElementById('amigosDoFacebook').innerHTML = 'Buscar Amigos do Facebook <i class="ifc-facebook"></i>';
    } else {
        //Nao utiliza o aplicativo
        //document.getElementById('amigosDoFacebook').innerHTML = 'Buscar Amigos do Facebook <i class="ifc-facebook"></i>';
    }
}

function EnviarMensagemFacebook(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/iicone_mensagem_fb_load.png" width="40" />';
    $.getJSON('/Convidados/EnviarConviteFacebook', { id_facebook: '100007787860500', autenticacao: data.autenticacao }, function (resposta) {
        FB.ui({
            app_id: '527135120709685',
            method: 'send',
            name: resposta.titulo,
            to: '100007787860500',
            link: resposta.url,
            description: resposta.titulo
        }, function callback(response) {
            if (response.success) {
                $.getJSON('/convidados/ConfirmarConviteFacebook', { id_facebook: '100007787860500', autenticacao: data.autenticacao }, function (resposta) {
                    if (resposta.isOk) {
                        evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_send.png" width="40" />';
                        RemoverBotaoApagar(data.autenticacao);
                    } else {
                        evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_error.png" width="40" />';
                    }
                }, 'json');
            } else {
                evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_mensagem_fb_error.png" width="40" />';
            }
        });
    }, 'json');
}

function EnviarEmail(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_load.png" width="40" />';
    $.getJSON('/Convidados/EnviarConviteEmail', { email: data.email, autenticacao: data.autenticacao }, function (resposta) {
        if (resposta.isOk) {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_send.png" width="40" />';
            RemoverBotaoApagar(data.autenticacao);
        } else {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_email_error.png" width="40" />';
        }
    }, 'json');
}

function EnviarSMS(evt) {
    var data = evt.dataset;
    evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_load.png" width="40" />';
    $.getJSON('/Convidados/EnviarConviteSMS', { telefone: data.telefone, autenticacao: data.autenticacao }, function (resposta) {
        if (resposta.isOk) {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_send.png" width="40" />';
            RemoverBotaoApagar(data.autenticacao);
        } else {
            evt.innerHTML = '<img src="/Content/Buffet/Convidados/Imagens/icone_sms_error.png" width="40" />';
        }
    }, 'json');
}

function RemoverBotaoApagar(autenticacao) {
    if (o('apagar_' + autenticacao) != null) {
        o('apagar_' + autenticacao).style.display = "none";
    }
}