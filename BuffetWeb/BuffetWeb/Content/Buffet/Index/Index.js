﻿function selecionarEvento(id_evento) {
    o('id_tipo_evento').value = id_evento;
}

function verificarEventoSelecionado(count) {
    for (var i = 0; i < count; i++) {
        if (o(i).class == 'onSelect') {
            return true;
        }
    }
    return false;
}

function BuscarOrcamento(evt) {
    if (o('codigo_unico').value == "" || o('senha').value == "") { 
        o('mensagem').style.display = "none";
        o('textMensagem2').innerHTML = "Informe os dois campos"
        o('mensagemOrcamento').style.display = "block";
        return;
    }
    var oldValue = evt.value;
    evt.value = "Aguarde, Consultando Orcamento..."
    evt.disabled = "disabled";
    $.getJSON('/Home/BuscarOrcamento', { codigo_unico: v('codigo_unico'), senha: v('senha') }, function (data) {
        evt.value = oldValue;
        evt.disabled = "";
        switch (data) {
            case 0: {
                o('mensagem').style.display = "none";
                o('textMensagem2').innerHTML = "Nenhum orçamento encontrado"
                o('mensagemOrcamento').style.display = "block";
            }; break;
            case 1: {
                o('mensagemOrcamento').style.display = "none";
                o('mensagem').style.display = "block";
                o('textMensagem').innerHTML = "Um orçamento foi encontrado, mas sua senha esta inválida!";
            }; break;
            case 2: o('buscarForm').submit(); break;
        }
    }, 'json');
}

function EsqueciMinhaSenha() {
    if (o('codigo_unico').value == "") {
        o('mensagem').style.display = "none";
        o('textMensagem2').innerHTML = "Informe o código do orçamento"
        o('mensagemOrcamento').style.display = "block";
        return;
    }
    $.getJSON('/Home/EsqueciMinhaSenha', { codigo_unico: v('codigo_unico') }, function (data) {
        if (data == 1) {
            o('mensagem').style.display = "none"; 
            o('textMensagem2').innerHTML = "Sua senha foi renovada e enviada no e-mail do cadastro!";
            o('mensagemOrcamento').style.display = "block";
        } else {
            o('mensagem').style.display = "none";
            o('textMensagem2').innerHTML = "Nenhum orçamento encontrado"
            o('mensagemOrcamento').style.display = "block";
        }
    }, 'json');
}