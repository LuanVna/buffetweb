﻿function BuscarHorariosDisponiveis(evt) {
    evt.value = "Aguarde..."
    o('alert_agendar').style.display = "none";
    $.getJSON('/Visita/BuscarHorariosDisponiveis', { tipo_visita: v('tipo_visita'), data: v('data_visita') }, function (resposta) {
        evt.value = "Buscar";
        if (resposta) {
            if (resposta.length != 0) {
                o('id_horario_div').style.display = "block";
                o('id_horario').innerHTML = "";
                var opt = document.createElement('option');
                opt.innerHTML = "Selecione";
                o('id_horario').appendChild(opt);
                for (var i = 0; i < resposta.length; i++) {
                    var opt = document.createElement('option');
                    opt.value = resposta[i].id;
                    opt.innerHTML = resposta[i].horario.Hours + "h" + resposta[i].horario.Minutes;
                    o('id_horario').appendChild(opt);
                }

                o('buscarVisita').style.display = "none";
                o('confirmarVisita').style.display = "block"; 
            } else { 
                o('id_horario_div').style.display = "none";
                o('alert_agendar').style.display = "block";
                evt.value = "Buscar";
            } 
        }
    }, 'json');
}

function LimparAgendamento(evt) { 
    o('buscarVisita').style.display = "block";
    o('confirmarVisita').style.display = "none";
    o('alert_agendar').style.display = "none";
    o('id_horario_div').style.display = "none";
}

function ConfirmarHorario(evt) {

}

//public JsonResult BuscarHorariosDisponiveis(string dia_visita, string tipo_degustacao, DateTime data)




//<div id="calendar" ></div>
//$(document).ready(function () {
//    var date = new Date();
//    var d = date.getDate();
//    var m = date.getMonth();
//    var y = date.getFullYear(); 

//    var calendar = $('#calendar').fullCalendar(
//    {
//        /*
//            header option will define our calendar header.
//            left define what will be at left position in calendar
//            center define what will be at center position in calendar
//            right define what will be at right position in calendar
//        */
//        header:
//        {
//            left: 'prev,next today',
//            center: 'title',
//            right: 'month,agendaWeek,agendaDay'
//        },
//        /*
//            defaultView option used to define which view to show by default,
//            for example we have used agendaWeek.
//        */
//        defaultView: 'agendaWeek',
//        contentHeight: 500,
//        /*
//            selectable:true will enable user to select datetime slot
//            selectHelper will add helpers for selectable.
//        */
//        selectable: true,
//        selectHelper: true,
//        /*
//            when user select timeslot this option code will execute.
//            It has three arguments. Start,end and allDay.
//            Start means starting time of event.
//            End means ending time of event.
//            allDay means if events is for entire day or not.
//        */
//        select: function (start, end, allDay) {
//            /*
//                after selection user will be promted for enter title for event.
//            */
//            var title = prompt('Event Title:');
//            /*
//                if title is enterd calendar will add title and event into fullCalendar.
//            */
//            if (title) {
//                calendar.fullCalendar('renderEvent',
//                    {
//                        title: title,
//                        start: start,
//                        end: end,
//                        allDay: allDay
//                    },
//                    true // make the event "stick"
//                );
//            }
//            calendar.fullCalendar('unselect');
//        },
//        /*
//            editable: true allow user to edit events.
//        */
//        editable: true,
//        /*
//            events is the main option for calendar.
//            for demo we have added predefined events in json object.
//        */
//        events: [
//            {
//                title: 'All Day Event',
//                start: new Date(y, m, 1)
//            },
//            {
//                title: 'Long Event',
//                start: new Date(y, m, d - 5),
//                end: new Date(y, m, d - 2)
//            },
//            {
//                id: 999,
//                title: 'Repeating Event',
//                start: new Date(y, m, d - 3, 16, 0),
//                allDay: false
//            },
//            {
//                id: 999,
//                title: 'Repeating Event',
//                start: new Date(y, m, d + 4, 16, 0),
//                allDay: false
//            },
//            {
//                title: 'Meeting',
//                start: new Date(y, m, d, 10, 30),
//                allDay: false
//            },
//            {
//                title: 'Lunch',
//                start: new Date(y, m, d, 12, 0),
//                end: new Date(y, m, d, 14, 0),
//                allDay: false
//            },
//            {
//                title: 'Birthday Party',
//                start: new Date(y, m, d + 1, 19, 0),
//                end: new Date(y, m, d + 1, 22, 30),
//                allDay: false
//            },
//            {
//                title: 'Click for Google',
//                start: new Date(y, m, 28),
//                end: new Date(y, m, 29),
//                url: 'http://google.com/'
//            }
//        ]
//    });

//});
