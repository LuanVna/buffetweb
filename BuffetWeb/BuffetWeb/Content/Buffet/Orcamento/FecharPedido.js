﻿function PagamentoForma(tipo) {
    Esconder("BoletoBancario");
    Esconder("TransferenciaBancaria");
    Esconder("CartaoCredito");
    Mostrar(tipo);
}






var atual = "InformacoesPessoais"
 
function SalvarInformacoesPessoais(evt) {
    evt.value = "Aguarde..."
    Desabilitar("Prosseguir");
    $.getJSON('/Pedido/SalvarInformacoesPessoais?' + $("#INFORMACOES").serialize(), function (resposta) {
        if (resposta == true) {
            Mostrar("Voltar");
            Mostrar("Imprimir");
            Desabilitar('Prosseguir');
            Contrato();
        }
        evt.value = "Continuar"
    }, 'json');
}

function AceitarContrato(evt) {
    evt.value = "Aguarde...";
    Mostrar("Voltar");
    Esconder("Imprimir");
    Desabilitar("Prosseguir");
    $.getJSON('/Pedido/AceitarContrato', function (resposta) {
        if (resposta == true) { 
            Mostrar("Voltar");
            FormaDePagamento();
        }
        evt.value = "Continuar"
    }, 'json');
}


function Prosseguir(evt) {
    ResetaFormularios();
    switch (atual) {
        case "InformacoesPessoais": SalvarInformacoesPessoais(evt);break;
        case "Contrato": AceitarContrato(evt); break;
        case "FormaDePagamento": {
            Mostrar("Voltar");
            FormaDePagamento();
        } break;
    } 
}

function ResetaFormularios() {
    Esconder("Voltar");
    Esconder("Imprimir");
    Abilitar('Prosseguir');
    o('confirmarContrato').checked = "";
    Titulo("Fechar Pedido");
}


function Voltar() {
    switch (atual) { 
        case "Contrato": { 
            ResetaFormularios();
            InformacoesPessoais();
        } break;
        case "FormaDePagamento": Mostrar('Imprimir'); Contrato(); break;
    }
}

function PermiteContinuar(evt) {
    if (evt.checked == true) {
        Abilitar('Prosseguir');
    } else {
        Desabilitar('Prosseguir');
    }
}

function FormaDePagamento() {
    Titulo("Forma de Pagamento");
    Esconder("InformacoesPessoais");
    Esconder("Contrato");
    Mostrar("FormaDePagamento");
    Abilitar("Prosseguir");
    atual = "FormaDePagamento";
}

function Contrato() {
    Titulo("Contrato");
    Esconder("FormaDePagamento");
    Esconder("InformacoesPessoais");
    Mostrar("Contrato");
    atual = "Contrato";
}

function InformacoesPessoais() {
    Titulo("Fechar Pedido");
    Esconder("FormaDePagamento");
    Esconder("Contrato");
    Mostrar("InformacoesPessoais");
    atual = "InformacoesPessoais";
}

function Imprimir() {

}


function Mostrar(id) {
    o(id).style.display = "block";
    //o(id).style.transform = translate("159px", "430px"); 
}

function Esconder(id) {
    o(id).style.display = "none";
}

function Desabilitar(id) {
    o(id).disabled = "disabled";
}

function Abilitar(id) {
    o(id).disabled = "";
}

function Titulo(text) {
    o('fecharPedidoTitle').innerHTML = text;
}