﻿
function verificaExistente(evt) {
    $.getJSON("/Orcamento/VerificaExistente", { seu_email: evt.value }, function (data) {
        if (data.existe == 1) {
            o('_seu_nome').innerHTML = data.seu_nome + "?";
            $('#myModal').modal('show')
        }
    }, 'json');
}

function buscaCadastrado() {
    $.getJSON("/Orcamento/BuscarCadastrado", { seu_email: v('seu_email') }, function (data) {
        if (data) {
            o('seu_nome').value = data.seu_nome;
            o('seu_telefone2').value = data.seu_telefone2;
            o('seu_telefone').value = data.seu_telefone;
            o('seu_email').value = data.seu_email;
            o('id_onde_conheceu').value = data.id_onde_conheceu;
            $('#myModal').modal('hide');
        }
    }, 'json');
}

function fechaMensagem() {
    o('seu_email').value = "";
    o('seu_email').focus();

    o('seu_nome').value = "";
    o('seu_telefone2').value = "";
    o('seu_telefone').value = "";
    o('seu_email').value = "";
    o('id_onde_conheceu').value = "";
    $('#myModal').modal('hide');
}