﻿function AgendarVisita(evt) {
    if ((o('email') != null && v('email') == "") || (o('celular') != null && v('celular') == "") || v('data') == "" || v('horario') == "") {
        o('textMensagem').innerHTML = 'Preencha todos os campos!';
        document.getElementById("mensagem").style.display = "block";
        return;
    }

    document.getElementById("mensagem").style.display = "none";
    evt.innerHTML = "Aguarde...";

    $.getJSON('/Orcamento/AgendarVisitaSimplificada', {
        email: o('email') != null ? v('email') : null,
        celular: o('celular') != null ? v('celular') : null,
        data: v('data'),
        horario: v('horario'),
        proposta: o('configuracao').dataset.proposta
    }, function (data) {
        o('textMensagem').innerHTML = data.mensagem;
        document.getElementById("mensagem").style.display = "block";
        evt.innerHTML = "Agendar Visita";
    }, 'json');
}



function EnviarEmail(evt) {
    if (v('assunto_email') == "" || v('mensagem_email') == "") {
        o('textMensagem').innerHTML = 'Não esqueça do assunto ou da mensagem!';
        document.getElementById("mensagem_email").style.display = "block";
        return;
    }

    document.getElementById("mensagem_email").style.display = "none";
    evt.innerHTML = "Aguarde...";

    $.getJSON('/Orcamento/EnviarEmailSimplificada', {
        email: o('email_email') != null ? v('email_email') : null,
        celular: o('celular_email') != null ? v('celular_email') : null,
        assunto: v('assunto_email'),
        mensagem: v('conteudo_email'),
        proposta: o('configuracao').dataset.proposta
    }, function (data) {
        o('textMensagem_email').innerHTML = data.mensagem;
        document.getElementById("mensagem_email").style.display = "block";
        evt.innerHTML = "Enviar";
    }, 'json');
} 

function TabAtual(evt, tab) {
    HideTab('detalhes');
    HideTab('precos');
    HideTab('brindes');
    HideTab('opcionais');
    ShowTab(tab);
}

function ShowTab(tab) {
    document.getElementById(tab).style.display = "block";
    document.getElementById('tab' + tab).setAttribute("class", "active");
}

function HideTab(tab) {
    document.getElementById(tab).style.display = "none";
    document.getElementById('tab' + tab).setAttribute("class", "");
}