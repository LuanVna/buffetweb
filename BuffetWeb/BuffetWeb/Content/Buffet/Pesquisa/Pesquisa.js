﻿function SorrisoSelecionado(evt, sorriso, id_pergunta) {
    var sorrisos = ["muito_insatisfeito_transparente",
                    "insatisfeito_transparente",
                    "indiferente_transparente",
                    "satisfeito_transparente",
                    "muito_satisfeito_transparente"];

    for (var i = 0; i < sorrisos.length; i++) {
        o(id_pergunta + "_img_" + sorrisos[i]).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/" + sorrisos[i] + ".png");
    }
    o(evt.dataset.img).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/" + sorriso.replace('_transparente', '') + ".png");
    SalvarResposta(id_pergunta, sorriso.replace('_transparente', ''));
}

function EstrelaSelecionada(evt, estrela, id_pergunta) {
    SalvarResposta(id_pergunta, estrela);

    for (var i = 0; i < 5; i++) {
        if (i >= (estrela + 1)) {
            o(id_pergunta + '_estrela_' + i).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_estrela_cinza.png");
        } else {
            o(id_pergunta + '_estrela_' + i).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png");
        }
    }
}

function GosteiSelecionado(evt, id_pergunta) {
    if (evt.dataset.like == "like") {
        SalvarResposta(id_pergunta, "GOSTEI");
        o('img_like_' + id_pergunta).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_like.png");
        o('img_dislike_' + id_pergunta).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_dislike_cinza.png");
    } else {
        SalvarResposta(id_pergunta, "NAO GOSTEI");
        o('img_like_' + id_pergunta).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_like_cinza.png");
        o('img_dislike_' + id_pergunta).setAttribute("src", "/Content/Buffet/Pesquisa/Imagens/icone_dislike.png");
    }
}

function SalvaComentario(evt, id_pergunta) {
    SalvarResposta(id_pergunta, evt.value);
}


function OpcaoAvaliacao(evt, id_opcao, index) {
    for (var i = 0; i < 5; i++) {
        o(id_opcao + '_avaliacao_' + i).innerHTML = '<img class="satisfacao spanHover" src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30">';
    }
    o(id_opcao + '_avaliacao_' + index).innerHTML = '<img class="satisfacao spanHover" src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_verde.png" width="30">';

    SalvarOpcao(id_opcao, evt.dataset.avaliacao);
}

function SalvarResposta(id_pergunta, resposta) {
    $.getJSON('/Convidados/ResponderPesquisa',
        {
            id_pergunta: id_pergunta,
            resposta: resposta,
            autenticacao: o('configuracao').dataset.autenticacao
        }, function (data) {
            if (data.isOk) {

            }
        }, 'json');
}

function SalvarOpcao(id_opcao, resposta) {
    $.getJSON('/Convidados/ResponderOpcao',
        {
            id_opcao: id_opcao,
            resposta: resposta,
            autenticacao: o('configuracao').dataset.autenticacao
        }, function (data) {
            if (data.isOk) {

            }
        }, 'json');
}

function IniciarPesquisa(evt) {
    o('0_pergunta').style.display = 'block';
    o('descricao_pergunta').style.display = 'none';
}

function ProximaPergunta(evt, contador, total) {
    for (var i = 0; i < total; i++) {
        if ((contador + 1) == i) {
            o(i + '_pergunta').style.display = "block";
            if ((contador + 2) == total) {
                PesquisaConcluida();
            }
        } else {
            if (o(i + '_pergunta') != null) {
                o(i + '_pergunta').style.display = "none";
            }
        }
    }
}


function PesquisaConcluida() {
    $.getJSON('/Convidados/PesquisaConcluida',
     {
         autenticacao: o('configuracao').dataset.autenticacao
     }, function (data) {
     }, 'json');
}

