﻿window.onload = function () {
    if (o('ConfirmarDados') != null) {
        $("#ConfirmarDados").modal("show");
    }
};

function AgendarVisita(evt) {
    if ((o('email') != null && v('email') == "") || (o('celular') != null && v('celular') == "") || v('data') == "" || v('horario') == "") {
        o('textMensagem').innerHTML = 'Preencha todos os campos!';
        document.getElementById("mensagem").style.display = "block";
        return;
    }

    document.getElementById("mensagem").style.display = "none";
    evt.innerHTML = "Aguarde...";

    $.getJSON('/Proposta/AgendarVisitaSimplificada', {
        email: o('email') != null ? v('email') : null,
        celular: o('celular') != null ? v('celular') : null,
        data: v('data'),
        horario: v('horario'),
        mensagem: v('mensagem_visita'),
        autenticacao: o('configuracao').dataset.autenticacao
    }, function (data) {
        o('textMensagem').innerHTML = data.mensagem;
        document.getElementById("mensagem").style.display = "block";
        evt.innerHTML = "Agendar Visita";
    }, 'json');
}

function EnviarEmail(evt) {
    if (v('assunto_email') == "" || v('mensagem_email') == "") {
        o('textMensagem').innerHTML = 'Não esqueça do assunto ou da mensagem!';
        document.getElementById("mensagem_email").style.display = "block";
        return;
    }

    document.getElementById("mensagem_email").style.display = "none";
    evt.innerHTML = "Aguarde...";

    $.getJSON('/Proposta/EnviarEmailSimplificada', {
        email: o('email_email') != null ? v('email_email') : null,
        celular: o('celular_email') != null ? v('celular_email') : null,
        assunto: v('assunto_email'),
        mensagem: v('conteudo_email'),
        autenticacao: o('configuracao').dataset.autenticacao
    }, function (data) {
        o('textMensagem_email').innerHTML = data.mensagem;
        document.getElementById("mensagem_email").style.display = "block";
        evt.innerHTML = "Enviar";
    }, 'json');
}

function TabAtual(evt, tab) {
    for (var i = 0; i < o('abas_ids').value.split(',').length; i++) {
        if (o('abas_ids').value.split(',')[i] != "") {
            HideTab(o('abas_ids').value.split(',')[i]);
        }
        HideTab('Valores');
    }
    ShowTab(tab);
}

function ShowTab(tab) {
    document.getElementById(tab).setAttribute("class", "active");
    document.getElementById('tab_' + tab).style.display = "block";
}

function HideTab(tab) {
    document.getElementById(tab).setAttribute("class", "");
    document.getElementById('tab_' + tab).style.display = "none";
}