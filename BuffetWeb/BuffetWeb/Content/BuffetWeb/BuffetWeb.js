﻿$(document).ready(function () {
     //INICIO
    if (o('ModalBuffetWeb') != null) {
        o('owl-demo').dataset.key_empresa = getCookie('KEY_EMPRESA');
        o('ModalBuffetWeb').innerHTML = addModalBuffetWeb();
        o('autenticacao').value = getCookie('KEY_EMPRESA');
        $("#owl-demo").owlCarousel({
            items: 5,
            lazyLoad: true,
            navigation: true,
            jsonPath: 'http://servico.buffetweb.com/BuffetWebService.asmx/Eventos?KEY_EMPRESA=' + keyEmpresa(),
            jsonSuccess: requestBuffetWebService
        });

        function requestBuffetWebService(data) {
            var thumbnail = "";
            for (var i = 0; i < data.length; i++) {
                thumbnail += generateThumbnail(data[i].nome, data[i].url_imagem, data[i].id);
            }
            $("#owl-demo").html(thumbnail);
        }
    }

    //GERENCIADOR
    $("#owl-gerenciar").owlCarousel({
        items: 5,
        lazyLoad: true,
        navigation: true 
    }); 
});

function generateThumbnail(name, url_image, id) {
    var thumbnail = '<div class="item">' +
                        '<div class="thumbnail col-md-11">' +
                            '<img src="' + url_image + '" style=" height: 150px; height: 153px; ">' +
                            '<div class="caption">' +
                                '<h3 class="text-center">' + name.split(' ')[0] + '</h3>' +
                                '<p>' + generateForm(id) + '</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
    return thumbnail;
}

function generateForm(id_evento) {
    var form = '<form action="/Orcamento/InformacoesDoEvento" method="post">' +
                    '<input type="hidden" name="key_empresa" value="' + keyEmpresa() + '" />' +
                    '<input type="hidden" name="id_tipo_evento" value="' + id_evento + '" />' +
                    '<input type="submit" class="btn btn-success btn-block" value="Selecionar" />' +
                '</form>';
    return form;
}


function addModalBuffetWeb() {
    return '<div class="modal fade bs-example-modal-sm" id="ModalOrcamento" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">' +
              '<div class="modal-dialog modal-sm">' +
                  '<div class="modal-content">' +
                  '<form action="/Orcamento/EscolherOrcamento" id="buscarForm" method="post">' +
                      '<input type="hidden" name="autenticacao" id="autenticacao" value="" />' +
                      '<div class="modal-body" style=" height: 215px; ">' +
                          '<div class="page-title">' +
                              '<h2><i class="fa fc-bgevent-container color"></i> Orçamento Online </h2>' +
                          '</div>' +
                          '<div class="col-md-12">' +
                              '<div id="mensagemOrcamento" style="display:none;" class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                  '<strong id="textMensagem2">Nenhum Orçamento Encontrado</strong>' +
                              '</div>' +
                              '<div id="mensagem" style="display:none; " class="alert alert-danger alert-dismissible fade in" role="alert">' +
                                  '<strong id="textMensagem"></strong>' +
                                  '<button type="button" onclick="EsqueciMinhaSenha()" class="btn btn-info">Esqueci a senha do Convite</button>' +
                              '</div>' +
                              '<input type="text" required title="Não esqueca este campo"  maxlength="8" id="codigo_unico" name="codigo_unico" class="form-control" placeholder="Código do evento">' +
                              '<input type="text" style="margin-top: 15px" required  maxlength="4" title="Não esqueca este campo" id="senha" name="senha" class="form-control" placeholder="Senha">' +
                          '<input type="submit" style="margin-top: 15px" value="Consultar" class="col-md-12 btn btn-success btn-block">' +
                      '</div>' +
                  '</div> ' +
                  '</form> ' +
              '</div>' +
          '</div>' +
      '</div>';
}
  
function keyEmpresa() {
    var id_empresa = document.getElementById('owl-demo').dataset.key_empresa;
    if (id_empresa != null && id_empresa != "") {
        return id_empresa;
    } else {
        console.log("Voce precisa informar o 'KEY DA EMPRESA' no dataset no componente!");
    } 
}
 












function LoadEventos() {
    var eventosCarvas = document.getElementById("OrcamentoEventos");
    var KEY_EMPRESA = document.getElementById("OrcamentoEventos").dataset.key_empresa;
    if (eventosCarvas != null) {
        $.getJSON('http://servico.buffetweb.com/BuffetWebService.asmx/Eventos?KEY_EMPRESA=' + KEY_EMPRESA, function (eventos) {
            if (eventos != null) {
                var div = '<div class="container-fluid clearfix About" style=""height: ' + (eventos.length * 50) + '">' +
                     '<div class="container clearfix">' +
                     '<form action="/Orcamento/InformacoesDoEvento" method="post">' +
                     '<div class="container clearfix TitleSection">' +
                     '<header class="page-head">' +
                     '<h1>Monte sua festa<small>// </small></h1>' +
                     '<input oncancel="return verificarEventoSelecionado(' + eventos.length + ')" style="float: right; width: 130px;margin-top: -50px;" type="submit" value="Continuar" class="btn btn-success btn-large span5">' +
                     '<input type="button" class="btn btn-info btn-large span5" onClick="window.location.href = \'/Orcamento/ConsultarOrcamento\'" style="float: right; width: 200px;margin-top: -50px;margin-right: 140px;" value="Consultar Orçamento" />' +
                     '</header></div><input type="hidden" id="data" data-total="' + eventos.length + '" />' +
                     '<input type="hidden" name="id_tipo_evento" id="id_tipo_evento" value="" /><div class="container clearfix" ><div class="row">';
                for (var i = 0; i < eventos.length; i++) {
                    div += '<div id="' + i + '" onclick="onSelectOnlyOne(this); selecionarEvento(' + eventos[i].id + ')" class="ofSelect span2" style=" margin-top: 30px; ">' +
                    '<div class="thumbnail produto">' +
                    '<img data-src="holder.js/100%x200" alt="100%x200" src="' + eventos[i].url + '")" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;">' +
                    '<div class="caption">' +
                    '<h3>' + eventos[i].nome + '</h3>' +
                    '<p></p> </div></div></div>';
                }
                div += '</div> <div> </form></div>';
                eventosCarvas.innerHTML = div;
            }
        }, 'json');
    }
}

function LoadImagensEventos() {
    var eventos = document.getElementById("ImagensEventos");
    if (eventos != null) {
        $.getJSON('/BuffetWeb/ImagensEventos', {}, function (imagens) {


        }, 'json');
    }
}


function selecionarEvento(id_evento) {
    o('id_tipo_evento').value = id_evento;
}

function verificarEventoSelecionado(count) {
    for (var i = 0; i < count; i++) {
        if (o(i).class == 'onSelect') {
            return true;
        }
    }
    return false;
}

function BuscarOrcamento(evt) {
    if (o('codigo_unico').value == "" || o('senha').value == "") {
        o('mensagem').style.display = "none";
        o('textMensagem2').innerHTML = "Informe os dois campos"
        o('mensagemOrcamento').style.display = "block";
        return;
    }
    var oldValue = evt.value;
    evt.value = "Aguarde, Consultando Orcamento..."
    evt.disabled = "disabled";
    $.getJSON('/Home/BuscarOrcamento', { codigo_unico: v('codigo_unico'), senha: v('senha') }, function (data) {
        evt.value = oldValue;
        evt.disabled = "";
        switch (data) {
            case 0: {
                o('mensagem').style.display = "none";
                o('textMensagem2').innerHTML = "Nenhum orçamento encontrado"
                o('mensagemOrcamento').style.display = "block";
            }; break;
            case 1: {
                o('mensagemOrcamento').style.display = "none";
                o('mensagem').style.display = "block";
                o('textMensagem').innerHTML = "Um orçamento foi encontrado, mas sua senha esta inválida!";
            }; break;
            case 2: o('buscarForm').submit(); break;
        }
    }, 'json');
}

function EsqueciMinhaSenha() {
    if (o('codigo_unico').value == "") {
        o('mensagem').style.display = "none";
        o('textMensagem2').innerHTML = "Informe o código do orçamento"
        o('mensagemOrcamento').style.display = "block";
        return;
    }
    $.getJSON('/Home/EsqueciMinhaSenha', { codigo_unico: v('codigo_unico') }, function (data) {
        if (data == 1) {
            o('mensagem').style.display = "none";
            o('textMensagem2').innerHTML = "Sua senha foi renovada e enviada no e-mail do cadastro!";
            o('mensagemOrcamento').style.display = "block";
        } else {
            o('mensagem').style.display = "none";
            o('textMensagem2').innerHTML = "Nenhum orçamento encontrado"
            o('mensagemOrcamento').style.display = "block";
        }
    }, 'json');
}