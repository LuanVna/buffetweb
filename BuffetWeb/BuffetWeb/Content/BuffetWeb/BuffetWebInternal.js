﻿$(document).ready(function () {
    $("#owl-demo").owlCarousel({
        items: 5,
        lazyLoad: true,
        navigation: true,
        itemsCustom: [[450, 4]]
    });

    if (o('servicos-carrocel') != null) {
        for (var i = 0; i < o('servicos-carrocel').value.split(',').length; i++) {
            if (o('servicos-carrocel').value.split(',')[i] != "") {
                //var quantidade = o('servicos-quantidade').value.split(',')[i];
                $("#owl-demo-" + o('servicos-carrocel').value.split(',')[i]).owlCarousel({
                    items:2,
                    lazyLoad: true,
                    navigation: true,
                    itemsDesktop: [1000, 2],
                    itemsDesktopSmall: [900, 2],
                    itemsTablet: [600, 2],
                    itemsMobile: false
                });
            }
        }
    }

    if (o('categorias_disponiveis') != null) {
        //ITENS DE ALUGUEL
        for (var i = 0; i < o('categorias_disponiveis').value.split(',').length ; i++) {
            var categoria = o('categorias_disponiveis').value.split(',')[i];
            if (categoria != "") {
                var quantidade = o('categorias_quantidade').value.split(',')[i];
                $("#" + categoria).owlCarousel({
                    items: quantidade == 1 ? 1 : 2,
                    lazyLoad: true,
                    navigation: true,
                    itemsDesktop: [1000, 2],
                    itemsDesktopSmall: [900, 2],
                    itemsTablet: [600, 2],
                    itemsMobile: false
                });
            }
        }
    }


    if (o('categorias_cardapio') != null) {
        //ITENS DE ALUGUEL
        for (var i = 0; i < o('categorias_cardapio').value.split(',').length ; i++) {
            var categoria = o('categorias_cardapio').value.split(',')[i]; 
            if (categoria != "") {
                var categorias_quantidade = o('categorias_quantidade').value.split(',')[i];
                $("#cardapio" + categoria).owlCarousel({
                    items: categorias_quantidade == 1 ? 1 : 2,
                    lazyLoad: true,
                    navigation: true,
                    itemsDesktop: [1000, 2], 
                    itemsDesktopSmall: [900, 2], 
                    itemsTablet: [600, 2],  
                    itemsMobile: false 
                });
            }
        }
    }


    $('.link').on('click', function (event) {
        var $this = $(this);

        var total = this.dataset.total;
        for (var i = 0; i < total; i++) {
            document.getElementById('idCardapio' + i).innerHTML = "";
        }
        document.getElementById('idCardapio' + this.dataset.id).innerHTML = '<div class="ribbon-inner shadow-pulse bg-warning"style=" font-size: 10px; ">Selecionado</div>';
        document.getElementById('id_cardapio').value = this.dataset.id_pacote;
    });
});


function SelecionaServico(evt, categoria, id_item) {
    var valores = o(id_item).dataset;
    if (o(id_item).value == "true") {
        if (valores.brinde == "True") {
            o('confirmaRemocao').dataset.id_item = id_item;
            o('confirmaRemocao').dataset.categoria = categoria;
            $('#confirmaRemocao').modal('show');
        } else {
            o(categoria + id_item).innerHTML = "";
            o(id_item).value = "false";
        }
    } else {
        if (valores.brinde == "True") {
            o(categoria + id_item).innerHTML = addBrinde();
        } else {
            o(categoria + id_item).innerHTML = addSelecionado();
        }
        o(id_item).value = "true";
    }
}

function SelecionaCardapio(evt, categoria, id_item) { 
    if (o(id_item).value == "true") {
        o(categoria + id_item).innerHTML = "";
        o(id_item).value = "false";
    } else { 
        o(categoria + id_item).innerHTML = addSelecionadoCardapio();
        o(id_item).value = "true";
    }
}

function addSelecionadoCardapio() {
    return '<div class="ribbon-inner shadow-pulse bg-warning" style=" font-size: 10px; margin-top: 7px; margin-left: 15px;">Selecionado</div>';
}

function addSelecionado() {
    return '<div class="ribbon-inner shadow-pulse bg-warning" style=" font-size: 10px; margin-top: 7px; margin-left: 15px;">Selecionado</div>';
}

function addBrinde() {
    return '<div class="ribbon-inner shadow-pulse bg-danger" style=" font-size: 10px; margin-top: 7px; margin-left: 15px;">Brinde</div>';
}

function ConfirmarRemocao() {
    var valores = o('confirmaRemocao').dataset;
    o(valores.categoria + valores.id_item).innerHTML = "";
    o(valores.id_item).value = "false";
    $('#confirmaRemocao').modal('hide');
}