﻿$(document).ready(function () { 
    $("#owl-demo").owlCarousel({  
        itemsDesktop: [1400, 6], 
        itemsDesktop: [1000, 5], 
        itemsDesktopSmall: [900, 3], 
        itemsTablet: [600, 2], 
        itemsMobile: false 
    }); 
});