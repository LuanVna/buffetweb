﻿using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

using BuffetWebData.Models;
using BuffetWeb.Models;
using BuffetWebData.Utils;

namespace BuffetWeb.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnAuthorization(AuthorizationContext context)
        {
            //if (!context.HttpContext.User.Identity.IsAuthenticated)
            //{
            //    context.Result = RedirectToAction("Index", "Home");
            //}
        }

        public EMPRESA empresa
        {
            get
            {
                HttpCookie keyEmpresa = Request.Cookies["KEY_EMPRESA"];
                if (keyEmpresa != null)
                {
                    try
                    {
                        using (BuffetContext db = new BuffetContext())
                        {
                            return db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                        }
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        public ActionResult viewOrcamento
        {
            get
            {
                return RedirectToAction("Orcamento", "Orcamento");
            }
        }

        public ActionResult viewBuffetWeb
        {
            get
            {
                return Redirect("http://www.torceretorce.com");
            }
        }

        public enum tipoMensagem
        {
            tipoSucesso,
            tipoErro,
            tipoAtencao
        }

        public static string mensagem;
        private static string corMensagem;

        public ActionResult viewConsultarOrcamento
        {
            get
            {
                return RedirectToAction("ConsultarOrcamento", "Orcamento");
            }
        }

        public ActionResult viewIndex
        {
            get
            {
                return Redirect("");
            }
        }

        public ORCAMENTO orcamento
        {
            get
            {
                return new SingleTone().orcamento;
            }
        }

        public CLIENTE cliente
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO orcamento = this.orcamento;
                    return db.CLIENTEs.Include(e => e.ENDERECO).Where(c => c.id == orcamento.id_cliente && c.id_empresa == this.ID_EMPRESA).FirstOrDefault();
                }
            }
        }

        public bool isLogin()
        {
            return BWCliente.isLogin();
        }

        public bool Entrar(string email, string senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ADMINISTRADOR adm = db.ADMINISTRADORs.Where(a => a.email.Equals(email) && a.senha.Equals(senha)).FirstOrDefault();
                if (adm != null)
                {
                    FormsAuthentication.SetAuthCookie(adm.id.ToString(), false);
                    return true;
                }
                return false;
            }
        }

        public static void Sair()
        {
            FormsAuthentication.SignOut();
        }

        public void getMensagem()
        {
            if (mensagem != null)
            {
                ViewBag.mensagem = mensagem;
                mensagem = null;
            }
        }

        public void limparMensagem()
        {
            ViewBag.mensagem = null;
        }


        public void setMensagem(string texto, tipoMensagem tipoMensagem)
        {
            switch (tipoMensagem)
            {
                case tipoMensagem.tipoSucesso: corMensagem = "Sucesso"; break;
                case tipoMensagem.tipoErro: corMensagem = "Erro"; break;
                case tipoMensagem.tipoAtencao: corMensagem = "Atencao"; break;
            }
            mensagem = texto;
        }

        public int ID_EMPRESA
        {
            get
            {
                EMPRESA empr = this.empresa;
                if (empr != null)
                {
                    return this.empresa.id;
                }
                return -1;
            }
        }

        public int ID_ORCAMENTO
        {
            get
            {
                ORCAMENTO o = this.orcamento;
                if (o != null)
                {
                    return o.id;
                }
                return -1;
            }
        }

        public string NOME_EMPRESA
        {
            get
            {
                return this.empresa.nome;
            }
        }
    }
}
