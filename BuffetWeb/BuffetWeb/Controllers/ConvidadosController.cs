﻿using BuffetWeb.Models;
using BuffetWeb.Models.Convidados;
using BuffetWeb.Models.Orcamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BuffetWebData.Models;
using System.Data.Entity;
using System.Net;
using BuffetWebData.Utils;
using BuffetWeb.Models.Pesquisa;

namespace BuffetWeb.Controllers
{
    public class ConvidadosController : BaseController
    {

        public ActionResult ListaDeConvidados()
        { 
            using (BuffetContext db = new BuffetContext())
            {
                Convidados convidados = new Convidados();
                convidados.id_empresa = base.ID_EMPRESA;
                convidados.id_orcamento = base.ID_ORCAMENTO;

                //List<CONVITE_CONVIDADOS> convites = db.CONVITE_CONVIDADOS.Include(e => e.CONVITE_CONVIDADOS_FAMILIARES).Where(i => i.id_orcamento == this.orcamento.id && i.id_empresa == ID_EMPRESA).OrderBy(n => n.nome_completo).ToList();


                //convidados.convidados_email = convites.Where(i => i.email != null && i.facebook == null).OrderBy(e => e.nome_completo).ToList();
                //convidados.convidados_telefone = convites.Where(i => i.email == null && i.facebook == null).OrderBy(e => e.nome_completo).ToList();
                //convidados.convidados_facebook = convites.Where(i => i.facebook == true).OrderBy(e => e.nome_completo).ToList();
                //convidados.TotalConvidados = convidados.convidados_email.Count + convidados.convidados_telefone.Count + db.CONVITE_CONVIDADOS_FAMILIARES.Where(e => e.id_empresa == ID_EMPRESA && e.id_orcamento == this.orcamento.id).ToList().Count;

                return View(convidados);
            }
        }

        public ActionResult SalvarConvidados(CONVITE_CONVIDADOS convidado, List<CONVITE_CONVIDADOS_FAMILIARES> familiares)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO oO = new SingleTone().orcamento;
                //if (db.CONVITE_CONVIDADOS.FirstOrDefault(c => c.email.Equals(convidado.email) && c.id_orcamento == oO.id && c.id_empresa == ID_EMPRESA) != null)
                //{
                //    base.setMensagem("Este email já está na lista", tipoMensagem.tipoAtencao);
                //    return RedirectToAction("ListaDeConvidados");
                //}
                //ORCAMENTO evento = db.ORCAMENTOes.Where(o => o.codigo_unico == o.codigo_unico && o.status.Equals("PAGAMENTO EFETUADO")).FirstOrDefault();

                if (convidado.email == null && convidado.telefone == null)
                {
                    base.setMensagem("Você precisa informar no mínimo o Telefone ou Email do convidado!", tipoMensagem.tipoAtencao);
                    return RedirectToAction("ListaDeConvidados");
                }

                convidado.id_orcamento = oO.id;
                convidado.ira = "Não Confirmado";
                convidado.autenticacao = UtilsManager.CriarAutenticacao();
                convidado.id_empresa = oO.id_empresa;
                convidado.sms_enviado = false;
                convidado.email_enviado = false;
                convidado.mensagem = oO.mensagem_padrao;

                db.CONVITE_CONVIDADOS.Add(convidado);

                familiares = familiares.Where(f => f.nome != null).ToList();

                if (familiares.Count != 0)
                {
                    familiares.ForEach(e => e.id_convidado = convidado.id);
                    familiares.ForEach(e => e.id_empresa = base.ID_EMPRESA);
                    familiares.ForEach(e => e.id_orcamento = oO.id);
                    convidado.CONVITE_CONVIDADOS_FAMILIARES = familiares;
                }

                db.SaveChanges();
                string aniversariantes = "";
                foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                {
                    aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                }
                string url = UtilsManager.EncurtarURL("http://www.convite.buffetweb.com/Convidados/ResponderConvite?autenticacao=" + convidado.autenticacao);

                if (convidado.email != null)
                {
                    EmailContent d = new EmailContent();
                    d.emailContentButton = EmailContentButton.Visualizar;
                    d.emailContentHeader = EmailContentHeader.Convite;
                    d.emailTemplate = TemplateEmail.TemplateEmpresa;
                    d.nomeOla = convidado.nome_completo;
                    d.url = url;

                    string m = string.Format("Olá {0} <br />Você recebeu um convite de aniversário de {1} <br />Clique no botão para visualizar e confirmar presença", convidado.nome_completo, aniversariantes);

                    EmailStatusSend statusEmail = new ContactManager(ID_EMPRESA).SendEmail(orcamento.CLIENTE.email, string.Format("Aniversário de {0} ", aniversariantes), m, TipoContato.Convite, convidado.autenticacao, d);

                    convidado.email_enviado = true;
                    convidado.enviado = true;
                    db.SaveChanges();
                }
                else if (convidado.telefone != null)
                {
                    string m = string.Format("{0} - Ola {1} dia {2} vai ser aniversario de {3} confirme em: {4}", empresa.nome, convidado.nome_completo, oO.data_evento.ToShortDateString(), aniversariantes, url);

                    new ContactManager(ID_EMPRESA).SendSMS(convidado.telefone, convidado.nome_completo, m, TipoContato.Convite, convidado.autenticacao);

                    convidado.sms_enviado = true;
                    convidado.enviado = true;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("ListaDeConvidados");
        }

        public JsonResult ApagarConvidado(int id_convidado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO oO = new SingleTone().orcamento;
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Where(c => c.id == id_convidado && c.id_orcamento == oO.id && c.id_empresa == ID_EMPRESA).FirstOrDefault();
                if (convidado != null)
                {
                    if (convidado.enviado != true)
                    {
                        var familiares = db.CONVITE_CONVIDADOS_FAMILIARES.Where(f => f.id_convidado == convidado.id).ToList();
                        if (familiares.Count != 0)
                        {
                            db.CONVITE_CONVIDADOS_FAMILIARES.RemoveRange(familiares);
                        }
                        db.CONVITE_CONVIDADOS.Remove(convidado);
                        db.SaveChanges();
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { isOk = false, mensagem = "Você não pode apagar um convidado que já recebeu convite!" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false, mensagem = "Convidado não encontrado" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditarConvidado(CONVITE_CONVIDADOS convidado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS c = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.id == convidado.id);
                if (c.ira.Equals("Não Confirmado"))
                {
                    c.nome_completo = convidado.nome_completo;
                    c.convidados = convidado.convidados;
                    c.telefone = convidado.telefone;
                    c.operadora = convidado.operadora;
                    c.email = convidado.email;
                }
                else
                {
                    c.nome_completo = convidado.nome_completo;
                }
                db.SaveChanges();
            }
            return RedirectToAction("ListaDeConvidados");
        }

        public ActionResult AdicionarAmigoFacebookALista(string id_facebook, int acompanhantes)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    CONVITE_CONVIDADOS_FACEBOOK convidado = db.CONVITE_CONVIDADOS_FACEBOOK.FirstOrDefault(e => e.id_facebook == id_facebook);
                    if (convidado != null)
                    {
                        ORCAMENTO o = orcamento;
                        CONVITE_CONVIDADOS novoConvidado = new CONVITE_CONVIDADOS()
                         {
                             id_facebook = id_facebook,
                             id_empresa = base.ID_EMPRESA,
                             ira = "Não Confirmado",
                             nome_completo = convidado.name,
                             convidados = acompanhantes > o.max_acompanhantes ? o.max_acompanhantes : acompanhantes,
                             facebook = true,
                             mensagem = o.mensagem_padrao,
                             autenticacao = RLSecurity.Security.CriptographyOn(new Random().Next(1, 99999999).ToString()).Replace("+", ""),
                             id_orcamento = o.id,
                             nome_facebook = db.CONVITE_CONVIDADOS_FACEBOOK.FirstOrDefault(e => e.amigo_de_id_facebook.Equals(id_facebook)).amigo_de_nome
                         };
                        db.CONVITE_CONVIDADOS.Add(novoConvidado);
                        db.SaveChanges();
                        return Json(new { isOk = true, nome = novoConvidado.nome_completo, id_acebook = convidado.id_facebook }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult RemoverConta(string id_facebook)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.amigo_de_id_facebook.Equals(id_facebook) && e.id_orcamento == ID_ORCAMENTO).ToList().ForEach(s => s.ativo = false);
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AtualizarAmigos(string id_facebook)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var amigos = db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.amigo_de_id_facebook.Equals(id_facebook) && e.id_orcamento == ID_ORCAMENTO).ToList();
                    db.CONVITE_CONVIDADOS_FACEBOOK.RemoveRange(amigos);
                    db.Database.CommandTimeout = 60 * 3;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }





        //public ActionResult EnviarEmail(ICollection<EmailsSelecionados> id_convidados)
        //{
        //    ORCAMENTO evento = new SingleTone().orcamento;
        //    if (evento != null && id_convidados != null)
        //    {
        //        id_convidados = id_convidados.Where(i => i.isOn != null).ToList();
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            List<CONVITE_CONVIDADOS> convidados = db.CONVITE_CONVIDADOS.Where(c => c.id_orcamento == evento.id && c.ira.Equals("Não Confirmado")).ToList();
        //            foreach (EmailsSelecionados id_convidado in id_convidados)
        //            {
        //                CONVITE_CONVIDADOS convidado = convidados.Where(c => c.id == id_convidado.id_convidado).FirstOrDefault();
        //                if (convidado != null)
        //                {

        //                    UtilsManager email = new UtilsManager(base.ID_EMPRESA);

        //                    CONVITE_HTML conviteHTML = db.CONVITE_HTML.FirstOrDefault(e => e.id_orcamento == evento.id);

        //                    CONVITE_HTML enviar = new CONVITE_HTML();
        //                    enviar.html = conviteHTML.html;

        //                    enviar.html = enviar.html.Replace("EMAIL_REPLACE", convidado.email);
        //                    enviar.html = enviar.html.Replace("SITE_REPLACE", empresa.dominio_buffet);
        //                    enviar.html = enviar.html.Replace("CODIGO_UNICO_REPLACE", evento.codigo_unico);
        //                    enviar.html = enviar.html.Replace("AUTENTICACAO_EMPRESA", empresa.codigo_empresa);
        //                    enviar.html = enviar.html.Replace("NOME_EMPRESA_REPLACE", base.NOME_EMPRESA.Replace(" ", ""));
        //                    //enviar.html = enviar.html.Replace("FAPAGE_REPLACE", base.getConfig("FAPAGE"));

        //                    bool resposta = email.sendEmail("Aniversário - Ashley 1 ano!", enviar.html, convidado.email);

        //                    if (resposta)
        //                    {
        //                        convidado.email_enviado = true;
        //                        convidado.ira = "Não Confirmado";
        //                        db.SaveChanges();
        //                    }

        //                    //if (Convert.ToBoolean(this.getConfig("CONVITE_QRCODE")))
        //                    //{
        //                    //    string data = "{id_evento:" + evento.id + ",id_convidado:" + convidado.id + ",autenticacao:'" + convidado.autenticacao + "'}";
        //                    //    email.enviarEmailConvite("Convite de e-mail", string.Format("{0}, {1} esta te convidando", convidado.nome_completo, evento.nome), convidado.email, data);
        //                    //}
        //                    //else
        //                    //{
        //                    //    email.enviarEmail("Convite de e-mail", string.Format("{0}, {1} esta te convidando", convidado.nome_completo, evento.nome), convidado.email);
        //                    //}  
        //                }
        //                base.setMensagem(convidados.Count == 1 ? "Email enviado com sucesso!" : "Emails enviados com sucesso!", tipoMensagem.tipoSucesso);
        //            }
        //        }
        //    }
        //    return RedirectToAction("ListaDeConvidados");
        //}


        public ActionResult EnviarConvite(ICollection<EmailsSelecionados> id_convidados)
        {
            ORCAMENTO orcamento = new SingleTone().orcamento;
            if (orcamento != null && id_convidados != null)
            {
                id_convidados = id_convidados.Where(i => i.isOn != false).ToList();
                using (BuffetContext db = new BuffetContext())
                {
                    List<CONVITE_CONVIDADOS> convidados = db.CONVITE_CONVIDADOS.Where(c => c.id_orcamento == orcamento.id && c.ira.Equals("Não Confirmado")).ToList();
                    foreach (EmailsSelecionados id_convidado in id_convidados)
                    {
                        CONVITE_CONVIDADOS convidado = convidados.Where(c => c.id == id_convidado.id_convidado).FirstOrDefault();
                        if (convidado != null)
                        {
                            if (convidado.email != null)
                            {
                                String convite = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/App_Data/Convite/CONVITE_EMAIL.txt"));

                                UtilsManager utils = new UtilsManager(base.ID_EMPRESA);
                                string template = utils.createConviteEmail(convite, orcamento.id, convidado);
                                string aniversariantes = "";
                                foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                                {
                                    aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                                }

                                bool resposta = utils.sendEmail(string.Format("{0} - {1}", empresa.nome, string.Format("Aniversário de {0} ", aniversariantes)), template, convidado.email);

                                if (resposta)
                                {
                                    convidado.email_enviado = true;
                                    db.SaveChanges();
                                }
                            }
                            else if (convidado.telefone != null && convidado.sms_enviado == true)
                            {
                                EMPRESA_PACOTE_SMS sms = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0 && e.id_empresa == ID_EMPRESA);
                                if (sms != null && sms.quantidade_disponivel > 0)
                                {
                                    string aniversariantes = "";
                                    foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                                    {
                                        aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                                    }

                                    string mensagem = string.Format("{0} - Ola {1} dia {2} vai ser aniversario de {3} confirme em: {4}", empresa.nome, convidado.nome_completo, orcamento.data_evento.ToShortDateString(), aniversariantes, UtilsManager.urlConvite(convidado.autenticacao));

                                    if (UtilsManager.sendSMS(convidado.telefone, mensagem))
                                    {
                                        sms.quantidade_disponivel--;
                                        convidado.sms_enviado = true;
                                        db.SaveChanges();
                                    }
                                }
                            }




                            //if (Convert.ToBoolean(this.getConfig("CONVITE_QRCODE")))
                            //{
                            //    string data = "{id_evento:" + orcamento.id + ",id_convidado:" + convidado.id + ",autenticacao:'" + convidado.autenticacao + "'}";
                            //    utils.enviarEmailConvite("Convite de e-mail", string.Format("{0}, {1} esta te convidando", convidado.nome_completo, evento.nome), convidado.email, data);
                            //}
                            //else
                            //{
                            //    utils.enviarEmail("Convite de e-mail", string.Format("{0}, {1} esta te convidando", convidado.nome_completo, evento.nome), convidado.email);
                            //}  
                        }
                        base.setMensagem(convidados.Count == 1 ? "Email enviado com sucesso!" : "Emails enviados com sucesso!", tipoMensagem.tipoSucesso);
                    }
                }
            }
            return RedirectToAction("ListaDeConvidados");
        }

        private bool Confirmar(string autenticacao, string codigo_unico, string email, int codStatus)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Include(em => em.EMPRESA).FirstOrDefault(c => c.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    HttpCookie cookie = new HttpCookie("KEY_EMPRESA", convidado.EMPRESA.codigo_empresa);
                    cookie.Expires = DateTime.Now.AddMonths(1);
                    HttpContext.Response.SetCookie(cookie);

                    convidado.ira = this.codigoStatus(codStatus);
                    db.SaveChanges();
                    ViewBag.nome = convidado.nome_completo;
                }
                else
                {
                    ViewBag.mensagem = "Oops! Infelizmente este evento não esta mais disponível";
                }
            }
            return true;
        }

        public ActionResult RespostaPresenca(string autenticacao, string codigo_unico, string email, int codStatus)
        {
            if (this.Confirmar(autenticacao, codigo_unico, email, codStatus))
            {
                return View();
            }
            return View();
        }

        //public ActionResult MensagemPresenca(string email, string codigo_unico, string mensagem)
        //{
        //    using (BuffetContext db = new BuffetContext())
        //    {
        //        ORCAMENTO orcamento = db.ORCAMENTOes.Where(o => o.codigo_unico.Equals(codigo_unico)).FirstOrDefault();
        //        if (orcamento != null)
        //        {
        //            CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Where(c => c.id_orcamento == orcamento.id && c.email.Equals(email)).FirstOrDefault();
        //            if (convidado != null)
        //            {
        //                convidado.mensagem = mensagem;
        //                db.SaveChanges();
        //            }
        //        }
        //    } 
        //    return View();
        //}

        public ActionResult ComoChegar(string autenticacao, string codigo_unico, string email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamento = db.ORCAMENTOes.Include(c => c.LOCAL_EVENTO.ENDERECO).Include(co => co.CONVITE_CONVIDADOS).FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico) && e.CONVITE_CONVIDADOS.FirstOrDefault(con => con.email.Equals(email)) != null);
                if (orcamento != null)
                {
                    CONVITE_CONVIDADOS convidado = orcamento.CONVITE_CONVIDADOS.FirstOrDefault();

                }
            }
            return View();
        }

        public ActionResult ImprimirListaDeConvidados(string tipoLista)
        {
            ORCAMENTO o = base.orcamento;
            using (BuffetContext db = new BuffetContext())
            {
                List<CONVITE_CONVIDADOS> convidados = new List<CONVITE_CONVIDADOS>();

                if (tipoLista.Equals("COMPLETA"))
                {
                    convidados = db.CONVITE_CONVIDADOS.Include(c => c.ORCAMENTO).Where(c => c.id_orcamento == o.id).OrderBy(od => od.nome_completo).ToList();
                }
                else if (tipoLista.Equals("CONFIRMADOS"))
                {
                    convidados = db.CONVITE_CONVIDADOS.Include(c => c.ORCAMENTO).Where(c => c.id_orcamento == o.id && c.ira.Equals("EU VOU")).OrderBy(od => od.nome_completo).ToList();
                }
                return View(convidados);
            }
        }

        public ActionResult ConvitePartial(string codigo_unico, int? id_orcamento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO o = db.ORCAMENTOes.Include(co => co.CONVITE_CONVIDADOS).FirstOrDefault(c => c.codigo_unico.Equals(codigo_unico) || c.id == id_orcamento);
                if (o != null)
                {
                    RespostaNavegador resposta = new RespostaNavegador() { id_empresa = ID_EMPRESA, convidado = o.CONVITE_CONVIDADOS.FirstOrDefault(), id_orcamento = o.id };
                    resposta.isMobile = Request.UserAgent.Contains("Mobile");
                    return View(resposta);
                }
            }
            return View();
        }

        public ActionResult ResponderConvite(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Include(em => em.EMPRESA).Include(o => o.ORCAMENTO).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    HttpCookie cookie = new HttpCookie("KEY_EMPRESA", convidado.EMPRESA.codigo_empresa);
                    cookie.Expires = DateTime.Now.AddMonths(1);
                    HttpContext.Response.SetCookie(cookie);
                    ORCAMENTO o = convidado.ORCAMENTO;
                    RespostaNavegador resposta = new RespostaNavegador() { id_empresa = ID_EMPRESA, convidado = convidado, id_orcamento = o.id };
                    resposta.isMobile = Request.UserAgent.Contains("Mobile");
                    ViewBag.autenticacao = autenticacao;

                    new ContactManager(ID_EMPRESA).ReadContact(Request, autenticacao, TipoContato.Convite);

                    return View(resposta);
                }
            }
            return Redirect("http://buffetweb.com");
        }


        private string codigoStatus(int codigo)
        {
            switch (codigo)
            {
                case 1: return "EU VOU";
                case 2: return "TALVEZ";
                case 3: return "NAO VOU";
            }
            return "";
        }


        //------------------------------------- JSON -------------------------------------
        public JsonResult BuscarOperadora(string telefone)
        {
            ORCAMENTO orcamento = new SingleTone().orcamento;
            if (orcamento != null)
            {
                Tuple<bool, string> resposta = UtilsManager.GetOperadora(telefone);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult BuscarOperadoraAnony(string telefone, string autenticacao)
        {
            if (autenticacao != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                    if (convidado != null)
                    {
                        Tuple<bool, string> resposta = UtilsManager.GetOperadora(telefone);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }

        public JsonResult AdicionarFamiliar(CONVITE_CONVIDADOS_FAMILIARES familiar)
        {
            ORCAMENTO orcamento = new SingleTone().orcamento;
            if (orcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    familiar.id_empresa = base.ID_EMPRESA;
                    familiar.id_orcamento = orcamento.id;
                    db.CONVITE_CONVIDADOS_FAMILIARES.Add(familiar);
                    db.SaveChanges();
                    return Json(new { isOk = true, id = familiar.id, nome = familiar.nome, tipo_convidado = familiar.tipo_convidado }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApagarFamiliar(int id_familiar)
        {
            ORCAMENTO orcamento = new SingleTone().orcamento;
            if (orcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS_FAMILIARES familiar = db.CONVITE_CONVIDADOS_FAMILIARES.FirstOrDefault(f => f.id == id_familiar && f.id_orcamento == orcamento.id && f.id_empresa == ID_EMPRESA);
                    if (familiar != null)
                    {
                        db.CONVITE_CONVIDADOS_FAMILIARES.Remove(familiar);
                        db.SaveChanges();
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }




        //resposta
        public JsonResult ConfirmarPresenca(string autenticacao, string codigo_unico, string email, int codStatus)
        {
            return Json(this.Confirmar(autenticacao, codigo_unico, email, codStatus), JsonRequestBehavior.AllowGet);
        }

        public JsonResult EnviarMensagem(string autenticacao, string codigo_unico, string mensagem)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO orcamento = db.ORCAMENTOes.Where(o => o.codigo_unico.Equals(codigo_unico)).FirstOrDefault();
                    if (orcamento != null)
                    {
                        CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Where(c => c.id_orcamento == orcamento.id && c.autenticacao.Equals(autenticacao)).FirstOrDefault();
                        if (convidado != null)
                        {
                            convidado.resposta = mensagem;
                            db.SaveChanges();
                        }
                    }
                    return Json(new { isOk = true, url_espaco = new UtilsManager(base.empresa.id).DominioEmpresa() }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = false, url_espaco = new UtilsManager(base.empresa.id).DominioEmpresa() }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult AtualizarInformacoes(string autenticacao, string codigo_unico, string email, string telefone, string operadora, int convidados)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.Include(o => o.ORCAMENTO).FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.ORCAMENTO.codigo_unico.Equals(codigo_unico));
                    if (convidado != null)
                    {
                        convidado.convidados = convidados;
                        if (email != "")
                        {
                            convidado.email = email;
                        }
                        if (telefone != "")
                        {
                            convidado.telefone = telefone;
                            convidado.operadora = operadora;
                        }
                        db.SaveChanges();
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SalvaAmigosFacebook(CONVITE_CONVIDADOS_FACEBOOK convidados)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    convidados.id_orcamento = orcamento.id;
                    convidados.ativo = true;
                    db.CONVITE_CONVIDADOS_FACEBOOK.Add(convidados);
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AmigosFacebook()
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    int id_orcamento = orcamento.id;
                    var amigos = db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.id_orcamento == id_orcamento)
                                                                .Select(f => new { f.id, f.id_facebook, f.name }).OrderBy(n => n.name).ToList();
                    return Json(new { isOk = true, friends = amigos }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult AtualizarAmigosFacebook(string id_facebook)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    int id_orcamento = orcamento.id;
                    db.CONVITE_CONVIDADOS_FACEBOOK.RemoveRange(db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.id_orcamento == id_orcamento && e.id_facebook == id_facebook).ToList());
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        //ENVIAR CONVITES
        public JsonResult EnviarConviteEmail(string email, string autenticacao)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.email.Equals(email) && e.id_orcamento == ID_ORCAMENTO);
                    if (convidado != null)
                    {
                        String convite = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/App_Data/Convite/CONVITE_EMAIL.txt"));

                        UtilsManager utils = new UtilsManager(base.ID_EMPRESA);
                        string template = utils.createConviteEmail(convite, orcamento.id, convidado);

                        string aniversariantes = "";
                        foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                        {
                            aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                        }

                        bool resposta = utils.sendEmail(string.Format("Aniversário de {0} ", aniversariantes), template, convidado.email);

                        if (resposta)
                        {
                            convidado.email_enviado = true;
                            convidado.enviado = true;
                            db.SaveChanges();
                            return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EnviarConviteSMS(string telefone, string autenticacao)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.telefone.Equals(telefone) && e.id_orcamento == ID_ORCAMENTO);
                    if (convidado != null)
                    {
                        EMPRESA_PACOTE_SMS sms = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0 && e.id_empresa == ID_EMPRESA);
                        if (sms != null && sms.quantidade_disponivel > 0)
                        {
                            string aniversariantes = "";
                            foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                            {
                                aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                            }

                            string mensagem = string.Format("{0} - Ola {1} dia {2} vai ser aniversario de {3} confirme em: {4}", empresa.nome, convidado.nome_completo, orcamento.data_evento.ToShortDateString(), aniversariantes, UtilsManager.urlConvite(convidado.autenticacao));

                            if (UtilsManager.sendSMS(convidado.telefone, mensagem))
                            {
                                sms.quantidade_disponivel--;
                                convidado.sms_enviado = true;
                                convidado.enviado = true;
                                db.SaveChanges();
                            }
                        }
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GerarConviteFacebook()
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO o = base.orcamento;
                string titulo = string.Format("{0} - {1}", base.NOME_EMPRESA, o.CONVITE_TEMPLATE.FirstOrDefault().titulo_convite);
                string autenticacao = UtilsManager.CriarAutenticacao();
                return Json(new { isOk = true, url = UtilsManager.urlConvite(autenticacao), autenticacao = autenticacao, titulo = titulo }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarConviteFacebook(string html, string nome)
        {


            using (BuffetContext db = new BuffetContext())
            {
                db.CONVITE_CONVIDADOS.Add(new CONVITE_CONVIDADOS()
                {
                    //id_facebook = id_facebook,
                    nome_completo = nome,
                    ira = "Não Confirmado",
                    //nome_facebook = nome,
                    id_empresa = base.ID_EMPRESA,
                    id_orcamento = base.ID_ORCAMENTO,
                    mensagem = base.orcamento.mensagem_padrao,
                    enviado = true,
                    sms_enviado = false,
                    email_enviado = false,
                    autenticacao = autenticacao
                });
                db.SaveChanges();
                return Json(new { isOk = true, JsonRequestBehavior.AllowGet });
            }
        }

        public JsonResult EnviarConviteFacebook(string id_facebook, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.id_orcamento == ID_ORCAMENTO);
                if (convidado != null)
                {
                    string m = string.Format("{0} - {1}", empresa.nome, orcamento.CONVITE_TEMPLATE.FirstOrDefault().titulo_convite);
                    return Json(new { isOk = true, url = UtilsManager.urlConvite(convidado.autenticacao), titulo = m }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ConfirmarConviteFacebook(string id_facebook, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.id_orcamento == ID_ORCAMENTO);
                if (convidado != null)
                {
                    convidado.facebook_enviado = true;
                    convidado.enviado = true;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarMensagem(string mensagem, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.id_orcamento == ID_ORCAMENTO);
                if (convidado != null)
                {
                    convidado.mensagem = mensagem;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AjustesConvites(int? max_acompanhantes, string mensagem_padrao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orc = db.ORCAMENTOes.FirstOrDefault(e => e.id == ID_ORCAMENTO && e.id_empresa == ID_EMPRESA);
                if (orc != null)
                {
                    orc.max_acompanhantes = max_acompanhantes;
                    orc.mensagem_padrao = mensagem_padrao;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("ListaDeConvidados");
        }

        public ActionResult Pesquisa(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    ViewBag.autenticacao = autenticacao;

                    HttpCookie cookie = new HttpCookie("KEY_EMPRESA", db.EMPRESAs.FirstOrDefault(f => f.id == convidado.id_empresa).codigo_empresa);
                    cookie.Expires = DateTime.Now.AddMonths(1);
                    HttpContext.Response.SetCookie(cookie);


                    DateTime depoisDoEvento = DateTime.Now.Date;
                    ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(f => f.id == convidado.id_orcamento && f.id_empresa == convidado.id_empresa);
                    if (orcamento != null)
                    {
                        if (depoisDoEvento > orcamento.data_evento)
                        {
                            if (convidado.compareceu == true)
                            {
                                convidado.status_pequisa = "INICIADA";
                                db.SaveChanges();
                                PesquisaModel pesquisa = new PesquisaModel()
                                {
                                    id_convidado = convidado.id,
                                    id_empresa = ID_EMPRESA,
                                    id_orcamento = convidado.id_orcamento
                                };
                                return View(pesquisa);
                            }
                            else
                            {
                                //Foi convidado mas nao compareceu
                                ViewBag.mensagem = "Me parece que você não compareceu ao evento!";
                                return View();
                            }
                        }
                        else
                        {
                            //pesquisa nao disponivel
                            ViewBag.mensagem = "Essa pesquisa estara disponivel a partir de " + orcamento.data_evento.ToShortDateString();
                            return View();
                        }
                    }
                    else
                    {
                        //nenhum orcamento
                        ViewBag.mensagem = "Nenhum orçamento encontrado!";
                        return View();
                    }
                }
                //voce nao foi covidado
                ViewBag.mensagem = "Você não foi convidado para esse evento!";
                return View();
            }
        }

        [AllowAnonymous]
        public JsonResult ResponderPesquisa(string resposta, int id_pergunta, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    PESQUISA_RESPOSTA pergunta = db.PESQUISA_RESPOSTA.FirstOrDefault(e => e.id_convidado == convidado.id && e.id_pergunta == id_pergunta);
                    if (pergunta == null)
                    {
                        db.PESQUISA_RESPOSTA.Add(new PESQUISA_RESPOSTA()
                        {
                            id_convidado = convidado.id,
                            id_empresa = base.ID_EMPRESA,
                            id_pergunta = id_pergunta,
                            resposta = resposta
                        });
                    }
                    else
                    {
                        pergunta.resposta = resposta;
                    }
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public JsonResult ResponderOpcao(string resposta, int id_opcao, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    PESQUISA_RESPOSTA_OPCOES pergunta = db.PESQUISA_RESPOSTA_OPCOES.FirstOrDefault(e => e.id_convidado == convidado.id && e.id_opcao == id_opcao);
                    if (pergunta == null)
                    {
                        db.PESQUISA_RESPOSTA_OPCOES.Add(new PESQUISA_RESPOSTA_OPCOES()
                        {
                            id_convidado = convidado.id,
                            id_empresa = base.ID_EMPRESA,
                            id_opcao = id_opcao,
                            resposta = resposta
                        });
                    }
                    else
                    {
                        pergunta.resposta = resposta;
                    }
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public JsonResult PesquisaConcluida(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(f => f.autenticacao.Equals(autenticacao));
                if (convidado != null)
                {
                    convidado.status_pequisa = "CONCLUIDA";
                    db.SaveChanges();

                    UtilsManager notificacao = new UtilsManager(base.ID_EMPRESA);
                    notificacao.mensagemNotificacao = "O convidado " + convidado.nome_completo + " Acaba de responder a pesquisa de satisfação!";
                    notificacao.sendNotificacao(Notificar.notificarPesquisaConcluida);
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public string autenticacao { get; set; }
    }
}
