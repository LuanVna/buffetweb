﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWeb.Models;
using BuffetWebData.Models;
using BuffetWeb.Models.Index;
using BuffetWebData.Utils;

namespace BuffetWeb.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        public ActionResult Index(string autenticacao, string am)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (autenticacao != null)
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(autenticacao));
                    if (empresa != null)
                    {
                        HttpCookie cookie = new HttpCookie("KEY_EMPRESA", empresa.codigo_empresa);
                        cookie.Expires = DateTime.Now.AddMonths(1);
                        HttpContext.Response.SetCookie(cookie);
                    }
                }

                ORCAMENTO o = base.orcamento;
                if (o != null)
                {
                    if (o.apenas_convite)
                    {
                        return RedirectToAction("ListaDeConvidados", "Convidados");
                    }
                    return RedirectToAction("Orcamento", "Orcamento");
                }
            }
            return View(new Index());
        }

        public ActionResult Contato(POTENCIAL_CLIENTE clientePotencial)
        {
            using (BuffetContext db = new BuffetContext())
            {
                clientePotencial.id_empresa = base.ID_EMPRESA;
                db.POTENCIAL_CLIENTE.Add(clientePotencial);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public JsonResult EsqueciMinhaSenha(string codigo_unico)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<ORCAMENTO> orcamentos = db.ORCAMENTOes.Where(o => o.codigo_unico.Equals(codigo_unico)).ToList();
                ORCAMENTO orcamento = orcamentos.Where(e => e.codigo_unico.Equals(codigo_unico)).FirstOrDefault();
                if (orcamento != null)
                {
                    string senha = new Random().Next(0, 100000).ToString();
                    orcamento.senha = RLSecurity.Security.CriptographyOn(senha);
                    db.SaveChanges();
                    CLIENTE cliente = db.CLIENTEs.Where(c => c.id == orcamento.id_cliente).FirstOrDefault();

                    UtilsManager email = new UtilsManager(orcamento.id_empresa);
                    if (email.sendEmail("Sua Senha de acesso", senha, cliente.email))
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarOrcamento(string codigo_unico, string senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                senha = RLSecurity.Security.CriptographyOn(senha);
                ORCAMENTO orcamento = db.ORCAMENTOes.Where(e => e.codigo_unico.Equals(codigo_unico)).FirstOrDefault();
                if (orcamento != null)
                {
                    if (orcamento.senha == senha)
                    {
                        return Json(2, JsonRequestBehavior.AllowGet);
                    }
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
