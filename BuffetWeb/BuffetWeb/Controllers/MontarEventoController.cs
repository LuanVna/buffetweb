﻿using System.Linq;
using System.Web.Mvc;
using BuffetWeb.Models.Orcamento;
using BuffetWeb.Models;
using BuffetWebData.Models;

namespace BuffetWeb.Controllers
{
    public class MontarEventoController : BaseController
    {

        private static string CATEGORIA;
        //
        // GET: /Orcamento/

        //passo 1
        public ActionResult IniciarOrcamento()
        {
            using (BuffetContext db = new BuffetContext())
            {
                base.getMensagem();
                return View(new IniciarOrcamento() { id_empresa = base.ID_EMPRESA });
            }
        }  

        public ActionResult CardapioSelecionado(int id_cardapio)
        {
            if (id_cardapio == 0)
            {

            }
            else
            {

            }
            return View();
        }

        public ActionResult ConfirmarTipoEvento(int id_tipo_evento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO o = new SingleTone().orcamento;
                if (o != null)
                {
                    ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(or => or.id == o.id && o.id_empresa == ID_EMPRESA);
                    orcamento.id_tipo_evento = id_tipo_evento;
                    db.SaveChanges();
                    return RedirectToAction("OrcamentoCriado");
                }
            }
            return base.viewConsultarOrcamento;
        } 
    }
}
