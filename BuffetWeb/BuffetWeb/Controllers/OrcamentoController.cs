﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWeb.Models.Orcamento;
using System.Dynamic;
using BuffetWeb.Models;
using System.Data.Entity;
using BuffetWeb.Models.Visitas;
using BuffetWebData.Utils;

namespace BuffetWeb.Controllers
{
    [Authorize]
    public class OrcamentoController : BaseController
    {
        private static int id_evento_cardapio;

        private static bool concluido;

        public ActionResult Orcamento()
        {
            ORCAMENTO o = base.orcamento;
            if (concluido)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO or = db.ORCAMENTOes.Where(oc => oc.id == o.id && o.id_empresa == ID_EMPRESA).FirstOrDefault();
                    if (or != null)
                    {
                        or.status = "CONCLUIDO";
                        db.SaveChanges();
                    }
                    base.getMensagem();
                }
                ViewBag.concluido = concluido;
                concluido = false;
            }

            else if (o.apenas_convite)
            {
                return RedirectToAction("ListaDeConvidados", "Convidados");
            }
            return View(o);
        }

        [AllowAnonymous]
        public ActionResult Proposta(string proposta)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamen = db.ORCAMENTOes.Include(em => em.EMPRESA.ENDERECO).Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(proposta));
                if (orcamen != null)
                {
                    UtilsManager.AutenticarEmpresa(HttpContext, orcamen.EMPRESA.codigo_empresa);

                    ViewBag.proposta = proposta;

                    new ContactManager(ID_EMPRESA, orcamen.id_cliente).ReadContact(Request, proposta, TipoContato.Proposta);

                    return View(orcamen);
                }
                return RedirectToAction("");
            }
        }
        //PASSO 1
        public ActionResult InformacoesDoEvento(int? id_tipo_evento, string key_empresa)
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA em = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(key_empresa));
                if (em != null)
                {
                    HttpCookie cookie = new HttpCookie("KEY_EMPRESA", em.codigo_empresa);
                    cookie.Expires = DateTime.Now.AddMonths(1);
                    HttpContext.Response.SetCookie(cookie);
                }
            }
            if (id_tipo_evento != null)
            {
                InformacoesEvento informacoes = new InformacoesEvento() { id_empresa = base.empresa.id };
                informacoes.setIdEvento((int)id_tipo_evento);
                id_evento_cardapio = Convert.ToInt32(id_tipo_evento);
                ViewBag.id_evento = id_evento_cardapio;
                return View(informacoes);
            }
            return base.viewConsultarOrcamento;
        }


        //PASSO 3
        public ActionResult OrcamentoCriado()
        {
            ORCAMENTO o = new SingleTone().orcamento;
            if (o != null)
            {
                ViewBag.CODIGO_UNICO = o.codigo_unico;
                ViewBag.NOME_CLIENTE = o.nome;

                ViewBag.evento = o.EVENTO.nome;
                ViewBag.imagem = o.EVENTO.url;
                ViewBag.id_evento = id_evento_cardapio;

                using (BuffetContext db = new BuffetContext())
                {
                    List<EVENTOS_PACOTES> pacote_evento = db.EVENTOS_PACOTES.Include(p => p.PACOTE.IMAGENS_PACOTES).Include(e => e.EVENTO).Where(e => e.id_evento == o.id_tipo_evento && e.id_empresa == ID_EMPRESA && (e.PACOTE.status == true)).ToList();
                    return View(pacote_evento);
                }
            }
            return base.viewConsultarOrcamento;
        }

        //PASSO 2
        public ActionResult MontarFesta(ORCAMENTO orcamento, CLIENTE cliente, int id_evento)
        {
            id_evento_cardapio = id_evento;
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    int id_cliente = 0;
                    string email = "";
                    CLIENTE clien = db.CLIENTEs.Where(c => c.email.Equals(cliente.email)).FirstOrDefault();
                    if (clien == null)
                    {
                        cliente.desde = DateTime.Now.Date;
                        clien.id_empresa = base.ID_EMPRESA;
                        clien.cliente_de = "SITE";
                        db.CLIENTEs.Add(cliente);
                        db.SaveChanges();
                        id_cliente = cliente.id;
                        email = cliente.email;
                    }
                    else
                    {
                        id_cliente = clien.id;
                        email = clien.email;
                    }

                    List<ORCAMENTO> o = db.ORCAMENTOes.ToList();
                    bool repeat = true;
                    do
                    {
                        string codigo_unico = new Random().Next(0, 100000000).ToString();
                        if (o.Where(i => i.codigo_unico.Equals(codigo_unico)).FirstOrDefault() == null)
                        {
                            orcamento.id_cliente = id_cliente;
                            orcamento.id_tipo_evento = id_evento;
                            orcamento.codigo_unico = codigo_unico;
                            orcamento.status = "INICIADO";
                            orcamento.data_criacao = DateTime.Now.Date;

                            orcamento.id_empresa = base.ID_EMPRESA;

                            string senha = new Random().Next(0, 100000).ToString();
                            orcamento.senha = RLSecurity.Security.CriptographyOn(senha);

                            db.ORCAMENTOes.Add(orcamento);
                            db.SaveChanges();

                            UtilsManager emails = new UtilsManager(base.ID_EMPRESA);
                            if (emails.sendEmail(string.Format("{0} - {1}", base.NOME_EMPRESA, "Suas Informações do Evento"), String.Format("Codigo do Orcamento: {0}\n Senha: {1}", orcamento.codigo_unico, senha), email))
                            {
                                bool orcamentoSalvo = BWCliente.AutenticaCliente(codigo_unico, senha);
                                if (orcamentoSalvo)
                                {
                                    if (orcamento.id_tipo_servico == 3)
                                    {
                                        return RedirectToAction("Servicos");
                                    }
                                    return RedirectToAction("OrcamentoCriado");
                                }
                                else
                                {
                                    base.setMensagem("Ouve algum erro ao tentar gerar seu orcamento, tente novamente", tipoMensagem.tipoErro);
                                    return RedirectToAction("ConsultarOrcamento");
                                }
                            };
                            base.setMensagem("Ouve algum erro ao tentar enviar a senha para seu e-mail, tente novamente", tipoMensagem.tipoErro);
                            return RedirectToAction("ConsultarOrcamento", "MontarEvento");
                        }
                    } while (repeat);

                    UtilsManager notificacao = new UtilsManager(base.ID_EMPRESA) { codigo_unico = orcamento.codigo_unico };
                    notificacao.sendNotificacao(Notificar.notificarNovoOrcamento);

                    return RedirectToAction("Index", "Home");
                }
                catch (Exception e)
                {
                    base.setMensagem("Ouve algum erro ao tentar gerar seu orcamento, tente novamente", tipoMensagem.tipoErro);
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        public ActionResult FazerPagamento()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento.aceita_contrato == true)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return View(db.ORCAMENTOes.Include(c => c.CLIENTE).Include(e => e.EVENTO).Where(c => c.id == orcamento.id && c.id_empresa == ID_EMPRESA).FirstOrDefault());
                }
            }
            return View();
        }

        public ActionResult ConfirmarPagamento(CartaoCredito cartaoCredito)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO or = db.ORCAMENTOes.Where(oc => oc.codigo_unico == oc.codigo_unico && oc.id_empresa == ID_EMPRESA).FirstOrDefault();
                if (or != null)
                {
                    //TODO: EFETUAR PAGAMENTO CARTAO DE CREDITO

                    or.status = "PAGAMENTO EFETUADO";
                    db.SaveChanges();
                    base.setMensagem("Pagamento efetuado com sucesso!", tipoMensagem.tipoSucesso);
                    return RedirectToAction("OrcamentoConcluido");
                }
                return base.viewConsultarOrcamento;
            }
        }

        public ActionResult ConcluirOrcamento()
        {
            if (base.empresa != null)
            {
                ORCAMENTO o = base.orcamento;
                if (o != null)
                {
                    concluido = true;
                    return RedirectToAction("Orcamento");
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }


        [AllowAnonymous]
        public ActionResult ConsultarOrcamento(string codigo_unico, string senha, string autenticacao)
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                if (orcamento.apenas_convite)
                {
                    return RedirectToAction("ListaDeConvidados", "Convidados");
                }
                return View(orcamento);
            }
            else
            {
                if (codigo_unico != null && senha != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        senha = RLSecurity.Security.CriptographyOn(senha);
                        ORCAMENTO o = db.ORCAMENTOes.Where(oc => oc.codigo_unico.Equals(codigo_unico) && oc.senha.Equals(senha)).FirstOrDefault();
                        if (o != null)
                        {
                            UtilsManager.AutenticarEmpresa(HttpContext, o.EMPRESA.codigo_empresa);

                            if (o.apenas_convite)
                            {
                                return RedirectToAction("ListaDeConvidados", "Convidados");
                            }
                            return View(o);
                        }
                        else
                        {
                            base.setMensagem("Orcamento não encontrado", tipoMensagem.tipoAtencao);
                        }
                    }
                }
                else
                {
                    ViewBag.codigo_unico = codigo_unico;
                    ViewBag.autenticacao = autenticacao;
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult EscolherOrcamento(string codigo_unico, string senha, string autenticacao)
        {
            if (codigo_unico != null || senha != null)
            {
                new ContactManager(ID_EMPRESA).ReadContact(Request, autenticacao, TipoContato.InformacoesDeLogin);

                using (BuffetContext db = new BuffetContext())
                {
                    senha = RLSecurity.Security.CriptographyOn(senha);
                    ORCAMENTO o = db.ORCAMENTOes.Include(em => em.EMPRESA).FirstOrDefault(oc => oc.codigo_unico.Equals(codigo_unico) && oc.senha.Equals(senha));
                    if (o != null)
                    {
                        UtilsManager.AutenticarEmpresa(HttpContext, o.EMPRESA.codigo_empresa);

                        if (o.apenas_convite)
                        {
                            return RedirectToAction("ListaDeConvidados", "Convidados");
                        }
                        base.limparMensagem();
                        return base.viewOrcamento;
                    }
                }
            }
            base.setMensagem("Orcamento Não encontrado", tipoMensagem.tipoAtencao);
            return base.viewConsultarOrcamento;
        }

        public ActionResult InformacoesPessoais()
        {
            return View(base.cliente);
        }

        public ActionResult SalvarPessoais(CLIENTE cliente)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE client = db.CLIENTEs.Include(e => e.ENDERECO).FirstOrDefault(i => i.id == this.cliente.id && i.id_empresa == ID_EMPRESA);
                if (client != null)
                {
                    client.nome = cliente.nome;
                    client.rg = cliente.rg;
                    client.cpf = cliente.cpf;
                    client.email = cliente.email;
                    client.celular = cliente.celular;
                    client.telefone = cliente.telefone;
                    client.cliente_de = "SITE";
                    if (client.ENDERECO != null)
                    {
                        client.ENDERECO.cep = cliente.ENDERECO.cep;
                        client.ENDERECO.endereco1 = cliente.ENDERECO.endereco1;
                        client.ENDERECO.bairro = cliente.ENDERECO.bairro;
                        client.ENDERECO.estado = cliente.ENDERECO.estado;
                        client.ENDERECO.cidade = cliente.ENDERECO.cidade;
                        client.ENDERECO.complemento = cliente.ENDERECO.complemento;
                        client.ENDERECO.id_empresa = base.ID_EMPRESA;
                    }
                    else
                    {
                        ENDERECO oEndereco = new ENDERECO();
                        oEndereco.cep = cliente.ENDERECO.cep;
                        oEndereco.endereco1 = cliente.ENDERECO.endereco1;
                        oEndereco.bairro = cliente.ENDERECO.bairro;
                        oEndereco.estado = cliente.ENDERECO.estado;
                        oEndereco.cidade = cliente.ENDERECO.cidade;
                        oEndereco.complemento = cliente.ENDERECO.complemento;
                        oEndereco.id_empresa = base.ID_EMPRESA;
                        client.id_endereco = oEndereco.id;
                        db.ENDERECOes.Add(oEndereco);
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("Contrato");
            }
        }

        public ActionResult VisualizarOrcamento()
        {
            return View(base.orcamento);
        }

        public ActionResult Contrato()
        {
            return View();
        }

        public ActionResult ConfirmarContrato(string confirmado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO oOrcamento = db.ORCAMENTOes.FirstOrDefault(o => o.id == orcamento.id && o.id_empresa == orcamento.id_empresa);
                if (oOrcamento != null)
                {
                    oOrcamento.aceita_contrato = confirmado.Equals("on");
                    db.SaveChanges();
                }
                return RedirectToAction("FormasDePagamento", "Pagamento");
            }
        }


        public ActionResult ConfirmarCardapio(int id_cardapio)
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    //using (BuffetContext db = new BuffetContext())
                    //{
                    //    List<PACOTES_ITENS> cardapios = db.PACOTES_ITENS.Include(es => es.ITENS_CARDAPIO).Where(e => e.id_itens_cardapio == id_cardapio && e.id_empresa == this.ID_EMPRESA).ToList();

                    //    foreach (var cardapio in cardapios)
                    //    {
                    //        db.ORCAMENTO_ITENS.Add(new ORCAMENTO_ITENS()
                    //        {
                    //            data_objeto = DateTime.Now.Date,
                    //            id_orcamento = orcamento.id,
                    //            valor_atual = (Decimal)cardapio.ITENS_CARDAPIO.venda_pessoa,
                    //            id_cardapio = cardapio.ITENS_CARDAPIO.id,
                    //            id_empresa = this.ID_EMPRESA
                    //        });
                    //    }

                    //    db.SaveChanges();

                    //PACOTE pacote = db.PACOTES.Include(e => e.EVENTOS_PACOTES.FirstOrDefault(p => p.id == id_cardapio && p.id_empresa == this.ID_EMPRESA);
                    //if (pacote != null)
                    //{

                    //}
                    return RedirectToAction("Servicos");
                    //}
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        public ActionResult Servicos()
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    ListarServicos ListaServicos = new ListarServicos();
                    using (BuffetContext db = new BuffetContext())
                    {
                        ListaServicos.servicos = db.EVENTO_SERVICOS.Include(it => it.ITENS_SERVICOS.CATEGORIA_SERVICOS).Where(e => e.id_tipo_evento == orcamento.id_tipo_evento && e.id_empresa == ID_EMPRESA).ToList();

                        //REGRA DE NEGOCIO: CASO O BRINDE SEJA COBRADO O VALOR E CAMUFLADO ENTRE OS ITENS
                        Decimal valor_brindes = ListaServicos.servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).Sum(e => e.ITENS_SERVICOS.valor_venda);
                        if (ListaServicos.servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).ToList().Count != 0)
                        {
                            ListaServicos.servicos.Where(b => b.cobrar_brinde != true && b.brinde != true).ToList().ForEach(e => e.ITENS_SERVICOS.valor_venda += (valor_brindes / ListaServicos.servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).ToList().Count));
                        }

                        ListaServicos.categorias = ListaServicos.servicos.Select(e => e.ITENS_SERVICOS.CATEGORIA_SERVICOS).Distinct().ToList();
                    }

                    if (ListaServicos.servicos.Count > 0)
                    {
                        return View(ListaServicos);
                    }
                    //REDIRECIONA PARA O PROXIMO CASO NAO EXISTA NENHUM SERVICO ASSOCIADO PARA O CARDAPIO ESPECIFICADO
                    return RedirectToAction("ItensDeAluguel");

                    //using (BuffetContext db = new BuffetContext())
                    //{
                    //    List<EVENTO_SERVICOS> servicos = db.EVENTO_SERVICOS.Include(it => it.ITENS_SERVICOS).Where(e => e.id_tipo_evento == orcamento.id_tipo_evento && e.id_empresa == this.ID_EMPRESA).ToList();

                    //    //REGRA DE NEGOCIO: CASO O BRINDE SEJA COBRADO O VALOR E CAMUFLADO ENTRE OS ITENS
                    //    Decimal valor_brindes = servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).Sum(e => e.ITENS_SERVICOS.valor_venda);
                    //    if (servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).ToList().Count != 0)
                    //    {
                    //        servicos.Where(b => b.cobrar_brinde != true && b.brinde != true).ToList().ForEach(e => e.ITENS_SERVICOS.valor_venda += (valor_brindes / servicos.Where(b => b.cobrar_brinde == true && b.brinde == true).ToList().Count));
                    //    }

                    //    if (servicos.Count != 0)
                    //    { 
                    //        return View(servicos);
                    //    }
                    //    //REDIRECIONA PARA O PROXIMO CASO NAO EXISTA NENHUM SERVICO ASSOCIADO PARA O CARDAPIO ESPECIFICADO
                    //    return RedirectToAction("ItensDeAluguel");
                    //}
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        public ActionResult ItensDeAluguel()
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        List<EVENTO_ALUGUEL> aluguel = db.EVENTO_ALUGUEL.Include(it => it.ITENS_ALUGUEL.CATEGORIA_ALUGUEL).Where(e => e.id_tipo_evento == orcamento.id_tipo_evento && e.id_empresa == this.ID_EMPRESA).ToList();
                        if (aluguel.Count != 0)
                        {
                            return View(aluguel);
                        }
                        //REDIRECIONA PARA O PROXIMO CASO NAO EXISTA NENHUM ITEM DE ALUGUEL ASSOCIADO PARA O CARDAPIO ESPECIFICADO
                        concluido = true;
                        return RedirectToAction("Orcamento");
                    }
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        public ActionResult DetalhesCardapio(int? id_cardapio)
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    ViewBag.id_cardapio = id_cardapio;
                    using (BuffetContext db = new BuffetContext())
                    {
                        EVENTOS_PACOTES evento = db.EVENTOS_PACOTES.FirstOrDefault(e => e.id_pacote == id_cardapio);

                        List<PACOTE_CATEGORIA_ITENS> itens = db.PACOTE_CATEGORIA_ITENS.Include(ca => ca.CATEGORIA_ITENS).Include(es => es.PACOTES_ITENS.Select(ee => ee.ITENS_CARDAPIO)).Where(e => e.id_pacote_evento == evento.id_pacote && e.CATEGORIA_ITENS.descricao.Equals("INCLUSA") && e.id_empresa == this.ID_EMPRESA).ToList();

                        return View(itens);
                    }
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        public ActionResult DetalhesCardapioExtra(int? id_cardapio)
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        EVENTOS_PACOTES evento = db.EVENTOS_PACOTES.FirstOrDefault(e => e.id_pacote == id_cardapio);

                        List<PACOTE_CATEGORIA_ITENS> itens = db.PACOTE_CATEGORIA_ITENS.Include(ca => ca.CATEGORIA_ITENS).Include(es => es.PACOTES_ITENS.Select(ee => ee.ITENS_CARDAPIO)).Where(e => e.id_pacote_evento == evento.id_pacote && e.CATEGORIA_ITENS.descricao.Equals("EXTRA") && e.id_empresa == this.ID_EMPRESA).ToList();

                        return View(itens);
                    }
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        public ActionResult Concluido(bool? concluido)
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        ORCAMENTO orc = db.ORCAMENTOes.FirstOrDefault(e => e.id == orcamento.id);
                        orc.status = "CONCLUIDO";
                        db.SaveChanges();
                        //TODO: ENVIAR UMA NOTIFICACAO OU EMAIL PARA O ADMINISTRADOR DOS EVENTOS.
                        if (concluido != null)
                        {
                            return View();
                        }
                        else
                        {
                            return base.viewOrcamento;
                        }
                    }
                }
                return base.viewConsultarOrcamento;
            }
            return base.viewBuffetWeb;
        }

        //---------------- JSON -------------------
        [HttpGet]
        public JsonResult VerificaExistente(string seu_email)
        {
            if (base.empresa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CLIENTE cliente = db.CLIENTEs.Where(e => e.email.Equals(seu_email)).FirstOrDefault();
                    OrcamentoVerificaExistente orca = new OrcamentoVerificaExistente();
                    if (cliente != null)
                    {
                        orca.seu_nome = cliente.nome;
                        orca.existe = true;
                        return Json(orca, JsonRequestBehavior.AllowGet);
                    }
                    orca.existe = false;
                    return Json(orca, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult BuscarCadastrado(string seu_email)
        {
            if (base.empresa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CLIENTE cliente = db.CLIENTEs.Where(e => e.email.Equals(seu_email)).FirstOrDefault();
                    if (cliente != null)
                    {
                        BuscarCadastrado orca = new BuscarCadastrado();
                        orca.seu_email = cliente.email;
                        orca.seu_nome = cliente.nome;
                        orca.seu_telefone = cliente.telefone;
                        orca.seu_telefone2 = cliente.celular;


                        return Json(orca, JsonRequestBehavior.AllowGet);
                    }
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult BuscarAgenda()
        {
            if (base.empresa != null)
            {
                ORCAMENTO oOrcamento = base.orcamento;
                if (oOrcamento != null)
                {
                    Visitas visitas = new Visitas();
                    return Json(visitas, JsonRequestBehavior.AllowGet);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult OrcamentoCriadoCardapios(int id_evento)
        {
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
