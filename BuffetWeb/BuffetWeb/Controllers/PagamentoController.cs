﻿using BuffetWebData.Models;
using BuffetWeb.Models.Pagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuffetWeb.Controllers
{
    public class PagamentoController : BaseController
    {
        //
        // GET: /Pagamento/
        private static bool concluido;

        public ActionResult FormasDePagamento()
        {
            if (base.isLogin())
            {
                base.getMensagem();
                return View(new FormaDePagamento());
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult PagamentoEfetuado()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                if (new FormaDePagamento().cielo.Equals("true"))
                {
                    if (concluido)
                    {
                        concluido = false;
                        return View(orcamento);
                    }
                }
                base.setMensagem("Esta forma de pagamento não esta dísponivel", tipoMensagem.tipoAtencao);
                return RedirectToAction("FormasDePamento");
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult CartaoDeCredito()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                if (new FormaDePagamento().cielo.Equals("true"))
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        return View(orcamento);
                    }
                }
                base.setMensagem("Esta forma de pagamento não esta dísponivel", tipoMensagem.tipoAtencao);
                return RedirectToAction("FormasDePamento");
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult PagamentoCartaoDeCredito(CartaoCredito cartaoCredito)
        {
            return RedirectToAction("PagamentoEfetuado");
        } 

        public ActionResult TransferenciaBancaria(string codigoBanco)
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                //EFETUAR TRANSACAO AQUI
            }
            return RedirectToAction("PagamentoEfetuado");
        }

        public ActionResult BoletoBancario()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                //EFETUAR TRANSACAO AQUI
            }
            return RedirectToAction("PagamentoEfetuado");
        }
    }
}
