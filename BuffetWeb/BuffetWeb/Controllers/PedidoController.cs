﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace BuffetWeb.Controllers
{
    public class PedidoController : BaseController
    {



        public JsonResult AceitarContrato()
        {
            if (base.empresa != null)
            {
                ORCAMENTO o = base.orcamento;
                if (o != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.id == o.id);
                        if (orcamento != null)
                        {
                            orcamento.aceita_contrato = true;
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }



        public JsonResult SalvarInformacoesPessoais(CLIENTE cliente)
        {
            if (base.empresa != null)
            {
                ORCAMENTO orcamento = base.orcamento;
                if (orcamento != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        CLIENTE client = db.CLIENTEs.Include(e => e.ENDERECO).FirstOrDefault(i => i.id == orcamento.CLIENTE.id && i.id_empresa == ID_EMPRESA);
                        if (client != null)
                        {
                            client.nome = cliente.nome;
                            client.rg = cliente.rg;
                            client.cpf = cliente.cpf;
                            client.email = cliente.email;
                            client.celular = cliente.celular;
                            client.telefone = cliente.telefone; 
                            if (client.ENDERECO != null)
                            {
                                client.ENDERECO.cep = cliente.ENDERECO.cep; 
                                client.ENDERECO.endereco1 = cliente.ENDERECO.endereco1;
                                client.ENDERECO.bairro = cliente.ENDERECO.bairro;
                                client.ENDERECO.estado = cliente.ENDERECO.estado;
                                client.ENDERECO.cidade = cliente.ENDERECO.cidade;
                                client.ENDERECO.complemento = cliente.ENDERECO.complemento;
                                client.ENDERECO.id_empresa = base.ID_EMPRESA;
                            }
                            else
                            {
                                ENDERECO oEndereco = new ENDERECO();
                                oEndereco.cep = cliente.ENDERECO.cep; 
                                oEndereco.endereco1 = cliente.ENDERECO.endereco1;
                                oEndereco.bairro = cliente.ENDERECO.bairro;
                                oEndereco.estado = cliente.ENDERECO.estado;
                                oEndereco.cidade = cliente.ENDERECO.cidade;
                                oEndereco.complemento = cliente.ENDERECO.complemento;
                                oEndereco.id_empresa = base.ID_EMPRESA;
                                client.id_endereco = oEndereco.id;
                                db.ENDERECOes.Add(oEndereco);
                            } 
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        } 
                    }
                } 
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }
    }
}
