﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using System.Web.Routing;
using BuffetWebData.Utils;
using BuffetWeb.Models.Proposta;

namespace BuffetWeb.Controllers
{
    public class PropostaController : BaseController
    {
        public ActionResult VisualizarProposta(string autenticacao)
        {

            PropostaModel proposta = new PropostaModel()
            {
                autenticacao = autenticacao,
                httpContext = HttpContext
            };

            if (proposta.Proposta != null)
            {
                new ContactManager(proposta.Proposta.id_empresa, (int)proposta.Proposta.id_cliente).ReadContact(Request, autenticacao, TipoContato.Proposta);

                if (DateTime.Today > proposta.Proposta.disponivel_ate)
                    return Redirect(string.Format("http://www.buffetweb.com/Parceiros/Parceiro?autenticacao={0}&m=404", proposta.Proposta.ORCAMENTO_PROPOSTA.LOCAL_EVENTO.autenticacao));
                else if (!proposta.Proposta.disponivel)
                    return Redirect(string.Format("http://www.buffetweb.com/Parceiros/Parceiro?autenticacao={0}&m=403", proposta.Proposta.ORCAMENTO_PROPOSTA.LOCAL_EVENTO.autenticacao));
                return View(proposta);
            }
            else
                return Redirect("http://www.buffetweb.com");
        }

        public ActionResult AtualizarProposta(string autenticacao, int? convidados, DateTime? data_evento, string periodo)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA proposta = db.CLIENTE_PROPOSTA.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (proposta != null)
                {
                    UtilsManager.SaveCookie(HttpContext, "KEY_EMPRESA", proposta.EMPRESA.codigo_empresa);

                    if (convidados != null)
                        proposta.convidados = convidados;
                    if (data_evento != null)
                        proposta.data_evento = data_evento;
                    if (periodo != null)
                        proposta.periodo = periodo;

                    db.SaveChanges();
                    return RedirectToAction("VisualizarProposta", new RouteValueDictionary(new { autenticacao = autenticacao }));
                }
                return Redirect("http://www.buffetweb.com");
            }
        }

        [AllowAnonymous]
        public JsonResult AgendarVisitaSimplificada(string autenticacao, DateTime data, string horario, string email, string celular, string mensagem)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA p = db.CLIENTE_PROPOSTA.Include(em => em.EMPRESA).Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (p != null)
                {
                    CLIENTE c = db.CLIENTEs.FirstOrDefault(e => e.id == p.CLIENTE.id);
                    if (c.email == null)
                        c.email = email;
                    if (c.celular == null)
                    {
                        c.celular = celular;
                        c.operadora_celular = UtilsManager.GetOperadora(celular).Item2;
                    }
                    db.SaveChanges();


                    string m = string.Format("O cliente {0} deseja agendar uma visita!", c.nome);
                    string autenticacao_visita = UtilsManager.CriarAutenticacao();
                    bool resposta = new ContactManager(p.EMPRESA.id).SendNotificationFromSystem("Agendamento de visita", m, new ContentNotification()
                                                                     {
                                                                         action = "VisitasAgendadas",
                                                                         controller = "Agenda",
                                                                         parameters = string.Format("autenticacao={0}&autenticacao_visita={1}", autenticacao, autenticacao_visita),
                                                                     }, NotificationRule.orcamento_agendar_visita);
                    if (resposta)
                        return Json(new { isOk = true, mensagem = "Em breve entraremos em contato para verificar a disponibilidade da visita!" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { isOk = false, mensagem = string.Format(" Não foi possivel agendar uma visita, entre em contato por telefone: {0}", c.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);


                    //string mensagem = string.Format("O cliente {0} deseja agendar uma visita dia {1} as {2} <br />Email: {3} <br />Celular:{4}", c.nome, data.ToShortDateString(), horario, c.email, c.celular);

                    //EmailContent d = new EmailContent();
                    //d.emailContentButton = EmailContentButton.Visualizar;
                    //d.emailContentHeader = EmailContentHeader.Proposta;
                    //d.emailTemplate = TemplateEmail.TemplateEmpresa;
                    //d.nomeOla = c.nome;

                    //EmailStatusSend status = new ContactManager(p.id_empresa, c.id).SendEmail("contato@torceretorce.com", "Agendamento de Visita", autenticacao, TipoContato.AgendamentoDeVisita, mensagem, d);
                    //if (status == EmailStatusSend.Send)
                    //{
                    //    //if (c.email != null)
                    //    //{
                    //    //    new ContactManager(c.id_empresa, cliente.id).SendEmail(c.email, "Agendamento de Visita", "Você acaba de agendar uma visita, aguarde que entraremos em contato o mais breve possível!", TipoContato.AgendamentoDeVisita, autenticacao, d);
                    //    //}
                    //    //new ContactManager(c.id_empresa).SendNotificationFromSystem("Agendamento de Visita", mensagem, NotificationRule.AgendarVisita);
                    //    return Json(new { isOk = true, mensagem = "Em breve entraremos em contato para verificar a disponibilidade da visita!" }, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    return Json(new { isOk = false, mensagem = string.Format(" Não foi possivel agendar uma visita, entre em contato por telefone: {0}", c.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);
                    //}
                }
                return Json(new { isOk = false, mensagem = "Não foi possivel agendar uma visita!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public JsonResult EnviarEmailSimplificada(string autenticacao, string assunto, string mensagem, string email, string celular)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA p = db.CLIENTE_PROPOSTA.Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (p != null)
                {
                    CLIENTE c = db.CLIENTEs.FirstOrDefault(e => e.id == p.CLIENTE.id);
                    if (c.email == null)
                        c.email = email;
                    if (c.celular == null)
                    {
                        c.celular = celular;
                        c.operadora_celular = UtilsManager.GetOperadora(celular).Item2;
                    }
                    db.SaveChanges();

                    string mensagem2 = string.Format("O cliente {0} acaba de responder sua proposta do evento {1} <br/>{2} <br />Email: {3} <br />Celular:{4}", p.CLIENTE.nome, p.ORCAMENTO_PROPOSTA.nome_proposta, mensagem, p.CLIENTE.email, p.CLIENTE.celular);


                    EmailContent d = new EmailContent();
                    d.emailContentButton = EmailContentButton.Visualizar;
                    d.emailContentHeader = EmailContentHeader.Proposta;
                    d.emailTemplate = TemplateEmail.TemplateEmpresa;
                    d.nomeOla = c.nome;

                    EmailStatusSend status = new ContactManager(p.id_empresa, c.id).SendEmail("contato@torceretorce.com", "Resposta de proposta " + p.ORCAMENTO_PROPOSTA.nome_proposta, mensagem2, TipoContato.RespostaProposta, autenticacao, d);
                    if (status == EmailStatusSend.Send)
                    {
                        //new ContactManager(p.id_empresa).SendNotificationFromSystem("Resposta de proposta", mensagem, NotificationRule.EntrarContato);

                        return Json(new { isOk = true, mensagem = "Mensagem enviada com sucesso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { isOk = false, mensagem = string.Format("Não foi possivel enviar um contato!, entre em contato por telefone: {0}", p.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { isOk = false, mensagem = "Não foi possivel enviar um contato!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult ImprimirProposta(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamen = db.ORCAMENTOes.Include(em => em.EMPRESA.ENDERECO).Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));

                if (orcamen.EMPRESA != null)
                {
                    UtilsManager.SaveCookie(HttpContext, "KEY_EMPRESA", orcamen.EMPRESA.codigo_empresa);
                }

                if (orcamen != null)
                {
                    ViewBag.proposta = autenticacao;

                    return View(orcamen);
                }
                return RedirectToAction("");
            }
        }
    }
}
