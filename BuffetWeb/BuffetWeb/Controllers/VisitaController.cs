﻿using BuffetWebData.Models;
using BuffetWeb.Models.Visitas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Globalization;


namespace BuffetWeb.Controllers
{
    public class VisitaController : BaseController
    {
        //
        // GET: /Visita/

        public JsonResult BuscarHorariosDisponiveis(string tipo_visita, DateTime data)
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CultureInfo ptBR = new CultureInfo("pt-BR"); 
                    string dia_visita = "";
                    switch (data.Date.DayOfWeek.ToString("d", ptBR))
                    {
                        case "0": dia_visita = "Domingo"; break;
                        case "1": dia_visita = "Segunda"; break;
                        case "2": dia_visita = "Terça"; break;
                        case "3": dia_visita = "Quarta"; break;
                        case "4": dia_visita = "Quinta"; break;
                        case "5": dia_visita = "Sexta"; break;
                        case "6": dia_visita = "Sabado"; break; 
                    }

                    var visitas = db.VISITAS_DISPONIVEIS.Where(e => e.tipo_visita.Equals(tipo_visita) 
                                                            && e.dia_visita.Equals(dia_visita)
                                                            && e.id_empresa == orcamento.id_empresa
                                                            && e.id_local_evento == orcamento.id_local_evento)
                                                            .Select(e => new { e.horario, e.tipo_visita, e.dia_visita, e.id }).ToList();

                    List<VISITAS_AGENDADAS> agendadas = db.VISITAS_AGENDADAS.Where(e => e.id_empresa == orcamento.id_empresa && e.dia.Equals(dia_visita) && e.data.Equals(data.Date)).ToList();
                    foreach (var agenda in agendadas)
                    {
                        visitas.Remove(visitas.FirstOrDefault(e => e.id == agenda.id_visitas_disponiveis));
                    } 

                    return Json(visitas, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }











        public ActionResult AgendarVisita()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                if (orcamento.ORCAMENTO_VISITAS.Where(v => v.status_visita.Equals("AGENDADO")).FirstOrDefault() == null)
                {
                    return View(new Visitas());
                }
                else
                {
                    return RedirectToAction("DetalhesDaVisita");
                }
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult DetalhesDaVisita()
        {
            ORCAMENTO orcamento = base.orcamento;
            if (orcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return View(db.ORCAMENTO_VISITAS.Include(o => o.ORCAMENTO).Include(l => l.LOCAL_EVENTO).Where(e => e.id_empresa == ID_EMPRESA && e.id_orcamento == orcamento.id).ToList());
                }
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult ConfirmarVisita(ORCAMENTO_VISITAS visita)
        {
            ORCAMENTO oOrcamento = base.orcamento;
            if (oOrcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {

                    visita.id_empresa = base.ID_EMPRESA;
                    visita.status_visita = "AGENDADO";
                    visita.id_orcamento = orcamento.id;

                    ORCAMENTO_VISITAS visi = db.ORCAMENTO_VISITAS.Where(v => v.id_orcamento == oOrcamento.id && v.status_visita.Equals("AGENDADO")).FirstOrDefault();
                    if (visita == null)
                    {
                        db.ORCAMENTO_VISITAS.Add(visita);
                    }
                    else
                    {
                        visi.id_horario = visita.id_horario;
                        visi.id_local_evento = visita.id_local_evento;
                        visi.data = visita.data;
                        visi.id_empresa = base.ID_EMPRESA;
                        visi.status_visita = "AGENDADO";
                        visi.id_orcamento = orcamento.id;
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("DetalhesDaVisita");
            }
            return base.viewConsultarOrcamento;
        }

        public ActionResult ConfirmarCancelarVistia(string mensagem, int id_visita)
        {
            ORCAMENTO oOrcamento = base.orcamento;
            if (oOrcamento != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO_VISITAS visita = db.ORCAMENTO_VISITAS.Where(v => v.id == id_visita && v.id_orcamento == oOrcamento.id && v.status_visita != "CANCELADO").FirstOrDefault();
                    if (visita != null)
                    {
                        visita.status_visita = "CANCELADO";
                        db.SaveChanges();
                    }
                    return RedirectToAction("AgendarVisita");
                }
            }
            return base.viewConsultarOrcamento;
        }
    }
}
