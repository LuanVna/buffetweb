﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity; 

namespace BuffetWeb.Models
{
    public class BWCliente
    {
        private static string idOrcamento()
        {
            try
            {
                return HttpContext.Current.User.Identity.Name;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static bool isLogin()
        {
            if (getOrcamento() != null)
                return true;
            return false;
        }

        public static string isLoginID()
        {
            return getOrcamento().codigo_unico;
        }

        public static ORCAMENTO getOrcamento()
        {
            string _idOrcamento = idOrcamento();
            if (_idOrcamento == null)
                return null;
            else
            {
                using (BuffetContext db = new BuffetContext())
                {
                    try
                    {
                        ORCAMENTO orcamento = db.ORCAMENTOes.Include(l => l.EVENTO)
                                             .Include(i => i.ORCAMENTO_ITENS)
                                             .Include(v => v.ORCAMENTO_VISITAS) 
                                             .Include(c => c.CLIENTE.ENDERECO)
                                             .Include(an => an.CONVITE_ANIVERSARIANTES.Select(f => f.CLIENTE_ANIVERSARIANTES))
                                             .Include(con => con.CONVITE_CONVIDADOS)
                                             .Include(e => e.LOCAL_EVENTO)
                                             .Include(t => t.CONVITE_TEMPLATE).FirstOrDefault(i => i.codigo_unico == _idOrcamento);
                        return orcamento;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
        }

        public static bool AutenticaCliente(string codigo_unico, string senha, bool isSecurity = false)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    if (!isSecurity)
                    { 
                        senha = RLSecurity.Security.CriptographyOn(senha);
                    }
                    ORCAMENTO orcamento = db.ORCAMENTOes.Where(i => i.codigo_unico == codigo_unico && i.senha.Equals(senha)).FirstOrDefault();
                    if (orcamento != null)
                    {
                        FormsAuthentication.SetAuthCookie(orcamento.codigo_unico.ToString(), false); 
                        return true;
                    }
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static void Deslogar()
        {
            FormsAuthentication.SignOut();
        }
    }
}