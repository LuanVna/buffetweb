﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWeb.Models.Convidados
{
    public class Convidados
    {
        public int id_empresa { get; set; }
        public int id_orcamento { get; set; }
        public bool isMobile { get; set; }

        public List<CONVITE_CONVIDADOS> convidados
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CONVITE_CONVIDADOS.Include(e => e.CONVITE_CONVIDADOS_FAMILIARES).Where(i => i.id_orcamento == this.id_orcamento && i.id_empresa == this.id_empresa).OrderBy(n => n.nome_completo).ToList();
                }
            }
        }

        public List<ContasFacebook> contasFacebook
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    //var perfis_logados = db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.id_orcamento == this.id_orcamento && e.ativo == true).Select(f => new { f.amigo_de_id_facebook, f.amigo_de_nome }).Distinct().ToList();
                    List<ContasFacebook> contas = new List<ContasFacebook>();
                    //foreach (var conta in perfis_logados)
                    //{
                    //    contas.Add(new ContasFacebook()
                    //    {
                    //        id_facebook = conta.amigo_de_id_facebook,
                    //        nome_facebook = conta.amigo_de_nome.Split(' ')[0]
                    //    });
                    //}
                    return contas;
                }
            }
        }

        //public List<CONVITE_CONVIDADOS_FACEBOOK> amigosFacebook
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            List<CONVITE_CONVIDADOS_FACEBOOK> amigos = db.CONVITE_CONVIDADOS_FACEBOOK.Where(e => e.id_orcamento == this.id_orcamento && e.ativo == true).Distinct().ToList();

        //            List<CONVITE_CONVIDADOS> c = this.convidados;
        //            foreach (var con in c)
        //            { 
        //                amigos.Remove(amigos.FirstOrDefault(e => e.id_facebook == con.id_facebook));
        //            }
        //            return amigos.OrderBy(e => e.name).ToList();
        //        }
        //    }
        //}

        public bool sendSMS
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var send = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0 && e.id_empresa == this.id_empresa); /*&& permitirSMS*/
                    if (send != null)
                    {
                        return true;
                    }
                    return false;
                }
            }
        }

    }

    public class ContasFacebook
    {
        public string id_facebook { get; set; }
        public string nome_facebook { get; set; }
    }
}