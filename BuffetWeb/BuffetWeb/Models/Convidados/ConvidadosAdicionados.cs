﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Convidados
{
    public class ConvidadosAdicionados
    {
        public string nome { get; set; }
        public string email { get; set; }
        public string documento { get; set; }
        public bool adulto { get; set; }
    }
}