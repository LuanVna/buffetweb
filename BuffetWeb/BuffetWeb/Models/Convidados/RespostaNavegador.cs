﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebData.Utils;

namespace BuffetWeb.Models.Convidados
{
    public class RespostaNavegador
    {
        public int id_empresa { get; set; }
        public int id_orcamento { get; set; }
        public CONVITE_CONVIDADOS convidado { get; set; }
        public bool isMobile { get; set; }

        public string mensagem
        {
            get
            {
                if (this.orcamento == null)
                {
                    return "Oops! Infelizmente este evento não esta mais disponível";
                }
                if (this.convidado == null)
                {
                    return "Oops, parece que você não foi convidado!";
                }
                return "";
            }
        }
         

        public string CSSFontes
        {
            get
            {
                return new UtilsManager().CSSFontes();
            }
        }

        public EMPRESA empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                }
            }
        }

        public ORCAMENTO orcamento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTOes.Include(c => c.CONVITE_TEMPLATE).Include(c => c.CLIENTE.CLIENTE_ANIVERSARIANTES).Include(l => l.LOCAL_EVENTO.ENDERECO).FirstOrDefault(e => e.id == this.id_orcamento && e.id_empresa == this.id_empresa);
                }
            }
        }

        public B_CONVITES_DISPONIVEIS disponivel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    int id_convite = this.orcamento.CONVITE_TEMPLATE.FirstOrDefault().id_convite;
                    return db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == id_convite);
                }
            }
        } 
    }
}