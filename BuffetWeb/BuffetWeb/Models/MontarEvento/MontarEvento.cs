﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models; 

namespace BuffetWeb.Models.MontarEvento
{
    public class MontarEvento
    {
        public List<ORCAMENTO_PACOTES> pacotes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTO_PACOTES.ToList();
                }
            }
        }

        //public string FTP_HOST_PASTA_PACOTE { get { return PerfilEmpresa.FTP_HOST_PASTA_PACOTE; } }
        //public bool MOSTRAR_DESCRICAO { get { return Convert.ToBoolean(PerfilEmpresa.ORCAMENTO_MOSTRAR_DESCRICAO); } } 
    }
}