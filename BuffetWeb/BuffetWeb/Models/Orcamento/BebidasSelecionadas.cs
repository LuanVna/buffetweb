﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    public class BebidasSelecionadas
    {
        public int id_bebida { get; set; }
        public string selecionado { get; set; }
    }
}