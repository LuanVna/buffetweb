﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWeb.Models.Orcamento
{
    public class DetalhesOrcamento
    { 
        public int id_empesa { get; set; }
        public List<ORCAMENTO_VISITAS> visitas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    DateTime now = DateTime.Now.Date;
                    return db.ORCAMENTO_VISITAS.Where(d => d.data.Month == now.Month && d.data.Year == now.Year && d.id_empresa == this.id_empesa).ToList();
                }
            } 
        }
        public ORCAMENTO orcamento
        {
            get
            {
                return new SingleTone().orcamento;
            } 
        }
    }
}