﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    public class DocesSelecionados
    { 
        public int id_doce { get; set; }
        public string selecionado { get; set; }
    }
}