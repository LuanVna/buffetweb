﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity; 

namespace BuffetWeb.Models.Orcamento
{
    public class InformacoesEvento
    {
        public int id_empresa { get; set; }
        public List<TIPO_SERVICO> tipo_servico
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.TIPO_SERVICO.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<HORARIO> horario_evento
        {
            get
            {

                using (BuffetContext db = new BuffetContext())
                {
                    return db.HORARIOS.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }
        public List<ONDE_CONHECEU> como_conheceu
        {
            get
            {

                using (BuffetContext db = new BuffetContext())
                {
                    return db.ONDE_CONHECEU.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<LOCAL_EVENTO> local_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.ToList();
                }
            }
        }

        public List<CAMPOS_DESCRICAO> campos_descricao { get; set; }

        public void setIdEvento(int id_tipo_evento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<CAMPOS_DISPONIVEIS> c = db.CAMPOS_DISPONIVEIS.Where(d => d.id_tipo_evento == id_tipo_evento).ToList();

                campos_descricao = new List<CAMPOS_DESCRICAO>();
                foreach (var disponivel in c)
                {
                    campos_descricao.Add(db.CAMPOS_DESCRICAO.Include(i => i.CAMPOS_COMPONENTES).Where(cc => cc.id == disponivel.id_campos_descricao).FirstOrDefault());
                }
            }
        }
    }
}