﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using BuffetWebData.Models;

namespace BuffetWeb.Models.Orcamento
{
    public class IniciarOrcamento
    {
        public int id_empresa { get; set; }
        //public IniciarOrcamento()
        //{
        //    this.tipo_evento = new List<EVENTO>();
        //    this.horarios = new List<HORARIO>();
        //}

        //public List<EVENTO> tipo_evento
        //{
        //    get
        //    {

        //    }
        //}
        public List<HORARIO> horarios
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                   return db.HORARIOS.Where(c => c.id_empresa == this.id_empresa).ToList();
                }

            }
        } 

    }
}