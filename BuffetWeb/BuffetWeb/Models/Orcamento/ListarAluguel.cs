﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;  
using BuffetWebData.Models;

namespace BuffetWeb.Models.Orcamento
{
    public class ListarAluguel
    {
        public List<ITENS_ALUGUEL> alugueis
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_ALUGUEL.Where(i => i.status == true).ToList();
                }
            }
        }
        //public List<ORCAMENTO_ALUGUEIS> aluguels_selecionados
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            ORCAMENTO o = new SingleTone().orcamento;
        //            ORCAMENTO_ALUGUEL orcamento = db.ORCAMENTO_ALUGUEL.Where(i => i.id_orcamento == o.id).FirstOrDefault();
        //            if (orcamento != null)
        //            {
        //                List<ORCAMENTO_ALUGUEIS> beb = db.ORCAMENTO_ALUGUEIS.Where(oc => oc.id_orcamento_aluguel == orcamento.id).ToList();
        //                if (beb != null)
        //                {
        //                    return beb;
        //                }
        //            }
        //            return new List<ORCAMENTO_ALUGUEIS>();
        //        }
        //    }
        //}

        //public string FTP_HOST_PASTA_ALUGUEL { get { return PerfilEmpresa.FTP_HOST_PASTA_ALUGUEL; } }
        //public bool MOSTRAR_DESCRICAO { get { return Convert.ToBoolean(PerfilEmpresa.ORCAMENTO_MOSTRAR_DESCRICAO); } }
        //public bool MULTIPLA_SELECAO { get { return Convert.ToBoolean(PerfilEmpresa.ORDEM_PRATOS_ALUGUEL.multipla_selecao); } }
    }
}