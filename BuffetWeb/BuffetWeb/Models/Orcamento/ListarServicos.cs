﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using System.Data.Entity; 
using BuffetWebData.Models;

namespace BuffetWeb.Models.Orcamento
{ 
    public class ListarServicos
    { 

        public List<CATEGORIA_SERVICOS> categorias
        {
            get;
            set;
        }

        public List<EVENTO_SERVICOS> servicos { get; set; }
    }
}