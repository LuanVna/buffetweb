﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    public class OpcionalSelecionado
    {
        public int id_opcional { get; set; }
        public string selecionado { get; set; }
    }
}