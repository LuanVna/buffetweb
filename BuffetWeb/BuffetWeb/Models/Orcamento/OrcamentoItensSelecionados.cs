﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    public class OrcamentoItensSelecionados
    {
        public int id_item { get; set; }
        public bool selecionado { get; set; }
    }
}