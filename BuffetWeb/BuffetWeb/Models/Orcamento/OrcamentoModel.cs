﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    struct OrcamentoVerificaExistente
    {
        public string seu_nome { get; set; }
        public bool existe { get; set; }
    }

    struct BuscarCadastrado
    { 
        public string seu_nome { get; set; }
        public string seu_email { get; set; }
        public string seu_telefone { get; set; }
        public string seu_telefone2 { get; set; } 
        public int id_onde_conheceu { get; set; }
    }
}