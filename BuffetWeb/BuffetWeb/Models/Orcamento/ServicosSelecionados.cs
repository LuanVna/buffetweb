﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Orcamento
{
    public class ServicosSelecionados
    {
        public int id_servico { get; set; }
        public string selecionado { get; set; }
    }
}