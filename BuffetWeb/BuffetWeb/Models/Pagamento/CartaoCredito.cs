﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWeb.Models.Pagamento
{
    public class CartaoCredito
    {
        public string nome_titular { get; set; }
        public string numero_cartao { get; set; }
        public string parcelas { get; set; }
        public string ano { get; set; }
        public string mes { get; set; }
        public string codigo { get; set; }
    }
}