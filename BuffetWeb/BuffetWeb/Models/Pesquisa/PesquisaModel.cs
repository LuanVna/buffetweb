﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWeb.Models.Pesquisa
{
    public class PesquisaModel
    {
        public int id_empresa { get; set; }
        public int id_orcamento { get; set; }
        public int id_convidado { get; set; } 
        
        public PESQUISA_PESQUISAS pesquisa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PESQUISA_PESQUISAS.Include(e => e.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_PERGUNTA_OPCOES)).FirstOrDefault(p => p.id_orcamento == this.id_orcamento && p.id_empresa == this.id_empresa);
                }
            }
        }

        public List<PESQUISA_RESPOSTA> respostas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PESQUISA_RESPOSTA.Where(e => e.id_convidado == this.id_convidado).ToList();
                }
            }
        }

        //public List<EMPRESA_SOCIAL> social
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.EMPRESA_SOCIAL.Where(e => e.id_empresa == this.id_empresa).ToList();
        //        }
        //    }
        //}

        public string mensagem
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<CONVITE_ANIVERSARIANTES> aniversariantes = db.CONVITE_ANIVERSARIANTES.Include(f => f.CLIENTE_ANIVERSARIANTES).Where(e => e.id_orcamento == this.id_orcamento).ToList();
                    string aniversariant = "";
                    foreach (var an in aniversariantes)
                    {
                        aniversariant = string.Format("<p>{0}</p> ", an.CLIENTE_ANIVERSARIANTES.nome);
                    }
                    return string.Format("<h2 class=\"text-center\">Agradecemos por você ter comparecido a festa de {0} Queremos saber sua opnião sobre nosso espaço,<p /> são apenas {1} perguntas, não vai levar mais que 1 minuto.</h2>", aniversariant, pesquisa.PESQUISA_PERGUNTA.Count + 1);
                }
            }
        }

    }
}