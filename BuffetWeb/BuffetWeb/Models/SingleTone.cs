﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWeb.Models.Orcamento;
 

namespace BuffetWeb.Models
{
    public class SingleTone
    {
        private static int index = 0;

        public void zerarIndex()
        {
            index = 0;
        }

        public void decrementaIndex()
        {
            if (index != 0) index--;
        }

        //public static ICollection<OrcamentoItens> itensOrcamento
        //{
        //    get
        //    {
        //        string categoria = "";
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            ORCAMENTO oOrcamento = BWCliente.getOrcamento();
        //            if (oOrcamento != null)
        //            {
        //                List<ORDEM_EVENTO> od = db.ORDEM_EVENTO.Include(c => c.CATEGORIA_ITENS).Where(os => os.id_evento == oOrcamento.EVENTO.id && os.id_empresa == base.ID_EMPRESA).OrderBy(i => i.ordem).ToList();
        //                if (od != null)
        //                {
        //                    if (od != null && od.Count != index)
        //                    {
        //                        if ((1 + index) <= od.Count)
        //                        {
        //                            CATEGORIA_ITENS ordem = od[index].CATEGORIA_ITENS;
        //                            index += 1;
        //                            int indexAux = index - 1;
        //                            categoria = ordem.nome;
        //                        }
        //                        else
        //                        {
        //                            CATEGORIA_ITENS ordem = od[index].CATEGORIA_ITENS;
        //                            int indexAux = index - 1;
        //                            categoria = ordem.nome;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        index = 0;
        //                    }
        //                }
        //            }
        //            CATEGORIA_ITENS id_categoria = db.CATEGORIA_ITENS.Include(c => c.ITENS_CARDAPIO).Where(c => c.nome.ToUpper().Equals(categoria.ToUpper()) && c.id_empresa == base.ID_EMPRESA).FirstOrDefault();
        //            if (id_categoria != null)
        //            {
        //                ICollection<ITENS_CARDAPIO> cards = id_categoria.ITENS_CARDAPIO;
        //                List<ORCAMENTO_ITENS> itensOrcamento = db.ORCAMENTO_ITENS.Where(i => i.id_orcamento == oOrcamento.id && i.id_categoria == id_categoria.id && i.id_empresa == base.ID_EMPRESA).ToList();

        //                List<OrcamentoItens> itens = new List<OrcamentoItens>();
        //                foreach (var card in cards)
        //                {
        //                    itens.Add(new OrcamentoItens()
        //                    {
        //                        cardapio = card,
        //                        selecionado = itensOrcamento.Where(c => c.id_cardapio == card.id).FirstOrDefault() != null ? true : false
        //                    });
        //                }  
        //                return itens;
        //            }
        //            return new List<OrcamentoItens>();
        //        }
        //    }
        //}

        public ORCAMENTO orcamento
        {
            get
            {
                return BWCliente.getOrcamento();
            }
        }

        public static void Sair()
        {
            FormsAuthentication.SignOut();
        }


        public static bool SetAuthCookie(string codigo_unico)
        {
            return SetCookie(codigo_unico, "codigo_unico");
        }

        public static bool SetCookie(string value, string key)
        {
            HttpCookie encodedCookie = new HttpCookie(key, value);

            if (HttpContext.Current.Request.Cookies[key] != null)
            {
                var cookieOld = HttpContext.Current.Request.Cookies[key];
                encodedCookie.Expires = (DateTime.Now.AddMonths(1));
                cookieOld.Value = encodedCookie.Value;
                HttpContext.Current.Response.Cookies.Add(cookieOld);
            }
            else
            {
                encodedCookie.Expires = (DateTime.Now.AddMonths(1));
                HttpContext.Current.Response.Cookies.Add(encodedCookie);
            }
            return true;
        }

        public static void remove(string key)
        {
            var c = new HttpCookie(key);
            c.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(c);
        }

        public static string GetCookie(string key)
        {
            var value = HttpContext.Current.Request.Cookies[key];
            if (value != null)
            {
                return value.Value;
            }
            return "";
        }
    }
}