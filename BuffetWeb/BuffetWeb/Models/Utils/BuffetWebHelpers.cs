﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Web.Mvc;
using BuffetWebData.Utils;
using BuffetWeb.Models;

namespace BuffetWeb.Helpers
{
    public static class BuffetWebHelpers
    {
        private static HttpCookie keyEmpresa
        {
            get { return System.Web.HttpContext.Current.Request.Cookies["KEY_EMPRESA"]; }
        }

        public static bool isMobile(this HtmlHelper helper)
        {
            return System.Web.HttpContext.Current.Request.UserAgent.Contains("Mobile");
        }

        public static bool ApenasConvite(this HtmlHelper helper)
        {
            ORCAMENTO o = new SingleTone().orcamento;
            return o.apenas_convite;
        }


        private static EMPRESA empresa()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
            }
        }

        //public static List<EMPRESA_SOCIAL> redeSocial(this HtmlHelper helper)
        //{
        //    using (BuffetContext db = new BuffetContext())
        //    {
        //        return db.EMPRESA_SOCIAL.Include(em => em.EMPRESA).Where(e => e.EMPRESA.codigo_empresa == keyEmpresa.Value).ToList();
        //    }
        //}

        public static string LogoEmpresa(this HtmlHelper helper)
        {
            return new FTPManager(empresa().id).LogoEmpresa();
        }

        public static string DominioEmpresa(this HtmlHelper helper)
        {
            return new UtilsManager(empresa().id).DominioEmpresa();
        }

        public static MvcHtmlString FontesCSS(this HtmlHelper helper)
        {
            return new MvcHtmlString(new UtilsManager().CSSFontes());
        }

        public static string ArquivoCSS(this HtmlHelper help)
        {
            string pathCSS = ArquivoEmpresa("CSS");
            if (pathCSS != null)
            {
                return pathCSS;
            }
            return "/Content/Gerenciador/css/style.css";
        }

        public static string ArquivoBOOTSTRAP(this HtmlHelper help)
        {
            string pathBootStrap = ArquivoEmpresa("BOOTSTRAP");
            if (pathBootStrap != null)
            {
                return pathBootStrap;
            }
            return "/Content/Gerenciador/css/bootstrap.min.css";
        }

        public static bool Send_SMS(this HtmlHelper help)
        {
            using (BuffetContext db = new BuffetContext())
            {
                int id_empresa = empresa().id;
                var send = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0 && e.id_empresa == id_empresa); /*&& permitirSMS*/
                if (send != null)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool AllowSomesSMS(this HtmlHelper help)
        {
            return true;
        }

        public static string ArquivoLOGO(this HtmlHelper help)
        {
            return ArquivoEmpresa("LOGO");
        }

        private static string ArquivoEmpresa(string key)
        {
            if (keyEmpresa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                    if (empresa != null)
                    {
                        List<EMPRESA_ARQUIVOS> arquivos = db.EMPRESA_ARQUIVOS.Where(i => i.id_empresa == empresa.id).ToList();
                        return arquivos.FirstOrDefault(e => e.chave.Equals(key)).valor;
                        //ViewBag.logo = arquivos.FirstOrDefault(e => e.chave.Equals("LOGO")).valor;
                        //ViewBag.bootstrap = arquivos.FirstOrDefault(e => e.chave.Equals("BOOTSTRAP")).valor;
                        //ViewBag.nome_empresa = empresa.nome;
                    }
                }
            }
            return null;
        }

        public static ORCAMENTO orcamento(this HtmlHelper help)
        {
            return BWCliente.getOrcamento();
        }

        public static EMPRESA empresa(this HtmlHelper help)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (keyEmpresa != null)
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                    return empresa;
                }
                return null;
            }
        }
    }
}