﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
 

namespace BuffetWeb.Models.Visitas
{
    public class VisitaAgendada
    {

        public int id_empresa { get; set; }
        public ORCAMENTO orcamento { get { return new SingleTone().orcamento; } }

        public List<ORCAMENTO_VISITAS> visita
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTO_VISITAS.Where(e => e.id_empresa == this.id_empresa && e.id_orcamento == this.orcamento.id).ToList();
                }
            }
        }
    }
}