﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWeb.Models.Visitas
{
    public class Visitas
    {
        public int id_empesa { get; set; }
        public List<VISITAS_AGENDADAS> visitas_marcadas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    DateTime now = DateTime.Now.Date;
                    List<VISITAS_AGENDADAS> ef = db.VISITAS_AGENDADAS.Include(o => o.VISITAS_DISPONIVEIS).Where(e => e.data.Month == now.Month && e.id_empresa == this.id_empesa).ToList();
                    return ef;
                }
            }
        }

        public ORCAMENTO orcamento
        {
            get
            {
                return new SingleTone().orcamento;
            }
        }

        //public List<HORARIO_DISPONIVEIS> horarios_disponiveis
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.HORARIO_DISPONIVEIS.Where(e => e.disponivel == true && e.id_empresa == this.id_empesa).ToList();
        //        }
        //    }
        //}

        public List<LOCAL_EVENTO> local_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(e => e.id_empresa == this.id_empesa).ToList();
                }
            }
        }
    }

    public class Visita
    {
        public int id { get; set; }
        public string title { get; set; }
        public string data { get; set; }
    }
}