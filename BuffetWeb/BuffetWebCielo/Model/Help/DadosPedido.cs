﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebCielo.Model.Help
{
    public class DadosPedido
    {
        public string numero { get; set; }
        public Decimal valor { get; set; }
        public string moeda { get; set; }
        public DateTime dataHora { get; set; }
        public string idioma { get; set; }
    }
}
