﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebCielo.Model.Help
{
    public class FormaPagamento
    {
        public string bandeira { get; set; }
        public string produto { get; set; }
        public int parcelas { get; set; }
    }
}
