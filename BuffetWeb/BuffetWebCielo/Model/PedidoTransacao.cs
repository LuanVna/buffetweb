﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebCielo.Pedido
{

    public enum TransacaoStatus
    {
        Criada = 0,
        Andamento = 1,
        Autenticada = 2, 
        NaoAutenticada = 3,
        Autorizada = 4,
        NaoAutorizada = 5,
        Capturada = 6,
        Cancelada = 9,
        Autenteicacao = 10, 
        Cancelamento = 12,
        SemStatus = 13
    }


    public class PedidoTransacao
    {
        public string tid { get; set; }
        public string codigoPedido { get; set; }
        public string codigoInternoPedido { get; set; }
        public TransacaoStatus transacaoStatus { get; set; }
        public string data { get; set; }
        public string redirect { get; set; }
    }
}
