﻿using BuffetWebCielo.Model.Help;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebCielo.Pedido
{
    public class TransacaoConsulta
    {
        public TransacaoConsulta()
        {
            this.dadosPedido = new DadosPedido();
            this.formaPagamento = new FormaPagamento();
        }

        public string tid { get; set; }
        public TransacaoStatus status { get; set; }
        public DadosPedido dadosPedido { get; set; }
        public FormaPagamento formaPagamento { get; set; }
    }
}
