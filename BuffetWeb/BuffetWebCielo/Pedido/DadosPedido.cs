﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebCielo.Pedido
{
    public class DadosPedido
    {
        public string NomeTitular { get; set; }
        public string CPJouCNPJ { get; set; }
        public string NumeroCartao { get; set; }
        public string MesValidade { get; set; }
        public string AnoValidade { get; set; }
        public decimal ValorTotal { get; set; }
        public int Parcelas { get; set; }
        public string Bandeira { get; set; }
        public string CodigoSeguranca { get; set; }
        public bool CallToken { get; set; }
        public string Descricao { get; set; }
        public string CodigoInterno { get; set; }
    }
}
