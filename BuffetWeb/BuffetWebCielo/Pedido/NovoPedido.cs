﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuffetWebData.Models;
using BuffetWebCielo.Request;
using BuffetWebCielo.Model;
using System.Xml.Linq;

namespace BuffetWebCielo.Pedido
{
    public class NovoPedido
    {
        public int id_empresa { get; set; }
        private string codigoInterno { get; set; }
        public NovoPedido(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public PedidoTransacao EfetuarTransacao(DadosPedido pedido)
        {
            return new PedidoTransacao()
             {
                 tid = "100699306937914F1001",
                 codigoPedido = "100699306937914F1001",
                 codigoInternoPedido = "100699306937914F1001",
                 transacaoStatus = TransacaoStatus.Capturada,
                 redirect = "/Planos/MeuCarrinho?autenticacao=100699306937914F1001"
             };
            //RequestPedido request = new RequestPedido(this.id_empresa);

            //string response = request.RequisicaoTransacao(pedido, true);
            //this.codigoInterno = request.dadosPedidoNumero;

            //return this.convertToObject(response);
        }

        private PedidoTransacao convertToObject(string xml)
        {
            PedidoTransacao p = new PedidoTransacao();
            XDocument doc = XDocument.Parse(xml.Replace("versao=\"1.2.1\" id=\"a97ab62a-7956-41ea-b03f-c2e9f612c293\" xmlns=\"http://ecommerce.cbmp.com.br\"", ""), LoadOptions.PreserveWhitespace);
            var pedido = (from field in doc.Elements("transacao")
                          select new
                          {
                              tid = field.Element("tid").Value,
                              pan = field.Element("pan").Value,
                              status = field.Element("status").Value,
                              urlautenticacao = field.Element("url-autenticacao").Value,

                              //captura = from cap in field.Elements("captura")
                              //          select new
                              //          {
                              //              captura = cap.Element("codigo").Value,
                              //              data = cap.Element("data-hora").Value
                              //          },
                              dadosPedido = from cap in field.Elements("dados-pedido")
                                            select new
                                            {
                                                numero = cap.Element("numero").Value,
                                                valor = cap.Element("valor").Value,
                                                moeda = cap.Element("moeda").Value,
                                                datahora = cap.Element("data-hora").Value,
                                                idioma = cap.Element("idioma").Value,
                                                taxaembarque = cap.Element("taxa-embarque").Value
                                            },
                              formaPagamento = from cap in field.Elements("forma-pagamento")
                                               select new
                                               {
                                                   bandeira = cap.Element("bandeira").Value,
                                                   produto = cap.Element("produto").Value,
                                                   parcelas = cap.Element("parcelas").Value,
                                               }
                          }).FirstOrDefault();

            p.tid = pedido.tid;
            p.codigoPedido = pedido.dadosPedido.FirstOrDefault().numero;
            p.redirect = pedido.urlautenticacao;

            //var captura = pedido.captura.FirstOrDefault();
            //p.transacaoStatus = (TransacaoStatus)Convert.ToInt32(captura.captura);
            //p.data = captura.data;
            p.codigoInternoPedido = this.codigoInterno;
            return p;
        }
    }
}

//<transacao>
//  <tid>1006993069378D361001</tid>
//  <pan>IqVz7P9zaIgTYdU41HaW/OB/d7Idwttqwb2vaTt8MT0=</pan>
//  <dados-pedido>
//    <numero>4139609</numero>
//    <valor>68960</valor>
//    <moeda>986</moeda>
//    <data-hora>2015-08-05T16:09:18.612-03:00</data-hora>
//    <idioma>PT</idioma>
//    <taxa-embarque>0</taxa-embarque>
//  </dados-pedido>
//  <forma-pagamento>
//    <bandeira>visa</bandeira>
//    <produto>1</produto>
//    <parcelas>1</parcelas>
//  </forma-pagamento>
//  <status>5</status>
//  <autenticacao>
//    <codigo>5</codigo>
//    <mensagem>Transacao sem autenticacao</mensagem>
//    <data-hora>2015-08-05T16:09:18.649-03:00</data-hora>
//    <valor>68960</valor>
//    <eci>7</eci>
//  </autenticacao>
//  <autorizacao>
//    <codigo>5</codigo>
//    <mensagem>Autoriza��o negada</mensagem>
//    <data-hora>2015-08-05T16:09:18.681-03:00</data-hora>
//    <valor>68960</valor>
//    <lr>60</lr>
//    <nsu>640630</nsu>
//  </autorizacao>
//</transacao>
