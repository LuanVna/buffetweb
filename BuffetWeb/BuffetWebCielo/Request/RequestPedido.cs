﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuffetWebData.Models;
using System.Net;
using System.IO;
using BuffetWebCielo.Pedido;

namespace BuffetWebCielo.Request
{

    public class RequestPedido
    {
        private const string versao = "1.2.1";
        private const string enderecoBase = "https://qasecommerce.cielo.com.br";
        private const string endereco = enderecoBase + "/servicos/ecommwsec.do";

        public string dadosEcNumero { get; set; }
        public string dadosEcChave { get; set; }

        public string dadosPortadorNumero { get; set; }
        public string dadosPortadorVal { get; set; }
        public string dadosPortadorInd { get; set; }
        public string dadosPortadorCodSeg { get; set; }

        public string dadosPedidoNumero { get; set; }
        public string dadosPedidoMoeda = "986";
        public string dadosPedidoData { get; set; }
        public string dadosPedidoDescricao { get; set; }
        public string dadosPedidoIdioma = "PT";

        public string formaPagamentoProduto { get; set; }
        public string formaPagamentoParcelas { get; set; }

        public string urlRetorno { get; set; }
        public string autorizar { get; set; }
        public string capturar { get; set; }

        public string tid { get; set; }
        public string status { get; set; }
        public string urlAutenticacao { get; set; }

        public string loja
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (this.id_empresa != -1)
                    {
                        return "";
                    }
                    return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("CIELO_LOJA")).valor;
                }
            }
        }

        public string lojaChave
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (this.id_empresa != -1)
                    {
                        return "";
                    }
                    return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("CIELO_CHAVE")).valor;
                }
            }
        }


        private int? id_empresa { get; set; }
        private DadosPedido dadosPedido { get; set; }
        private string dataAtual { get; set; }

        public RequestPedido(int? id_empresa)
        {
            this.id_empresa = id_empresa;
            //CONFIGURACOES
            this.formaPagamentoProduto = "2";
            this.capturar = "false";
            this.autorizar = "3";

            //ENVIAR CODIGO DE SEGURANCA
            this.dadosPortadorInd = "1";
            //0 – não informado 
            //1 – informado
            //2 – ilegível
            //9 – inexistente
        }

        public string RequisicaoTransacao(DadosPedido dadosPedido, bool incluirPortador = false)
        {
            this.dadosPedido = dadosPedido;

            this.dadosEcNumero = this.loja;
            this.dadosEcChave = this.lojaChave;
            this.dadosPedidoNumero = new Random().Next(1000000, 9999999).ToString();
            this.dadosPedido.NumeroCartao.Replace(".", "");

            DateTime now = DateTime.Now;
            this.dataAtual = string.Format("{0}-{1}-{2}T{3}:{4}:{5}", now.Year, this.ToZero(now.Month), this.ToZero(now.Day), this.ToZero(now.Hour), this.ToZero(now.Minute), this.ToZero(now.Second)); //"2011-12-21T11:32:45";//DateTime.Today.ToString();
            this.urlRetorno = string.Format("/Planos/MeuCarrinho?autenticacao={0}", this.dadosPedidoNumero);

            this.formaPagamentoProduto = "1"; //FORMA DE PAGAMENTO
            this.formaPagamentoParcelas = dadosPedido.Parcelas.ToString(); //QUANTIDADE DE PARCELAS

            string msg = this.XMLHeader() + "<requisicao-transacao id=\"a97ab62a-7956-41ea-b03f-c2e9f612c293\" versao='" + versao + "'>" + this.XMLDadosEc();
            if (incluirPortador == true)
            {
                msg += this.XMLDadosPortador();
            }
            msg += this.XMLDadosPedido() +
                          this.XMLFormaPagamento() +
                          this.XMLUrlRetorno() +
                          this.XMLAutorizar() +
                          this.XMLCapturar();
            msg += "<campo-livre>" + this.dadosPedido.Descricao + "</campo-livre>" +
                   "<bin>" + this.dadosPedido.NumeroCartao.Replace(".", "").Replace("-", "").Substring(0, 6) + "</bin></requisicao-transacao>";

            return this.Enviar(msg, "Transacao");
        }


        public string Enviar(string xml, string nome)
        {
            try
            {
                string data = "mensagem=" + xml;
                byte[] dataStream = Encoding.UTF8.GetBytes(data);
                WebRequest webRequest = WebRequest.Create(endereco);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = dataStream.Length;
                Stream newStream = webRequest.GetRequestStream();
                newStream.Write(dataStream, 0, dataStream.Length);
                newStream.Close();

                using (WebResponse response = webRequest.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string transacao = rd.ReadToEnd();
                        return transacao;
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }
         
        private string ToZero(int value)
        {
            return (value.ToString().Length == 1 ? "0" + value.ToString() : value.ToString());
        }
        public object RequisicaoTid()
        {
            string msg = this.XMLHeader() + "<requisicao-tid id='" + this.dataAtual + "' versao ='" + versao + "'>" +
                         this.XMLDadosEc() +
                         this.XMLFormaPagamento() + "</requisicao-tid>";

            var objResposta = this.Enviar(msg, "Requisicao Tid");
            return objResposta;
        }

        public string RequisicaoAutorizacaoPortador()
        {
            string msg = this.XMLHeader() + "<requisicao-autorizacao-portador id='" + this.dataAtual + "' versao ='" + versao + "'>" +
                         "<tid>" + this.tid + "</tid>" +
                          this.XMLDadosEc() +
                          this.XMLDadosCartao() +
                          this.XMLDadosPedido() +
                          this.XMLFormaPagamento() +
                          "<capturar-automaticamente>" + this.capturar + " </capturar-automaticamente>" +
                          "</requisicao-autorizacao-portador>";

            var objResposta = this.Enviar(msg, "Autorizacao Portador");
            return objResposta;
        }


        public object RequisicaoCaptura(string PercentualCaptura = "", string anexo = null)
        {
            string teste = "<?xml version='1.0' encoding='ISO-8859-1'?>" +
             "<requisicao-consulta id=\"6fcf758e-bc60-4d6a-acf4-496593a40441\" versao='1.2.1'>" +
                "<tid>" + this.tid + "</tid>" +
                "<dados-ec'>" +
                       "<numero>" + this.dadosEcNumero + "</numero>" +
                       "<chave>" + this.dadosEcChave + "" +
                       "</chave>" +
                "</dados-ec>" +
              "</requisicao-consulta>";

            //string msg = this.XMLHeader() + "<requisicao-consulta id='" + this.dataAtual + "' versao ='" + versao + "'>" +
            //             "<tid>"+ this.tid + "</tid>" +
            //             this.XMLDadosEc()+
            //            "<valor>"+PercentualCaptura +"</valor>";
            //if(anexo != null && anexo != "")
            //{
            //    msg +=	"<anexo>"+anexo+"</anexo>";
            //}
            //msg += "</requisicao-consulta>";

            object objResposta = this.Enviar(teste, "Captura");
            return objResposta;
        }


        public string RequisicaoConsulta()
        {
            string msg = "<?xml version='1.0' encoding='ISO-8859-1'?>" +
                   "<requisicao-consulta id='6fcf758e-bc60-4d6a-acf4-496593a40441' versao='1.2.1'>" +
                     "<tid>" + this.tid + "</tid>" +
                     "<dados-ec>" +
                            "<numero>" + this.loja + "</numero>" +
                            "<chave>" + this.lojaChave + "</chave>" +
                     "</dados-ec>" +
                   "</requisicao-consulta>";
            return this.Enviar(msg, "Consulta");
        } 

        private string XMLHeader()
        {
            return @"<?xml version='1.0' encoding='ISO-8859-1'?>";
        }

        private string XMLDadosEc()
        {
            string msg = "<dados-ec><numero>" + this.dadosEcNumero + "</numero>" +
                         "<chave>" + this.dadosEcChave + "</chave></dados-ec>";
            return msg;
        }

        private string XMLDadosPortador()
        {
            string msg = "<dados-portador><numero>" + this.dadosPedido.NumeroCartao.Replace(",", "").Replace(".", "") + "</numero>" +
                        "<validade>" + this.dadosPedido.AnoValidade + this.dadosPedido.MesValidade + "</validade>" +
                        "<indicador>" + this.dadosPortadorInd + "</indicador>" +
                        "<codigo-seguranca>" + this.dadosPedido.CodigoSeguranca + "</codigo-seguranca>";
            if (!string.IsNullOrEmpty(this.dadosPedido.NomeTitular))
            {
                msg += "<nome-portador>" + this.dadosPedido.NomeTitular + "</nome-portador>";
            }
            msg += "</dados-portador>";
            return msg;
        }


        private string XMLDadosCartao()
        {
            string msg = "<dados-cartao> <numero>" + this.dadosPedido.NumeroCartao + "</numero>" +
                        "<validade>" + this.dadosPortadorVal + "</validade>" +
                        "<indicador>" + this.dadosPortadorInd + "</indicador>" +
                        "<codigo-seguranca>" + this.dadosPortadorCodSeg + "</codigo-seguranca>";
            if (!string.IsNullOrEmpty(this.dadosPedido.NomeTitular))
            {
                msg += "<nome-portador>" + this.dadosPedido.NomeTitular + "</nome-portador>";
            }
            msg += "</dados-cartao>";
            return msg;
        }

        private string XMLDadosPedido()
        {
            string msg = "<dados-pedido>" +
                        "<numero>" + this.dadosPedidoNumero + "</numero>" +
                        "<valor>" + this.dadosPedido.ValorTotal.ToString().Replace(",", "").Replace(".", "") + "</valor>" +
                        "<moeda>" + this.dadosPedidoMoeda + "</moeda>" +
                        "<data-hora>" + this.dataAtual + "</data-hora>";

            if (!string.IsNullOrEmpty(this.dadosPedido.Descricao))
            {
                msg += "<descricao>" + this.dadosPedido.Descricao + "</descricao>";
            }
            msg += "<idioma>" + this.dadosPedidoIdioma + "</idioma></dados-pedido>";
            return msg;
        }

        private string XMLFormaPagamento()
        {
            string msg = "<forma-pagamento><bandeira>" + this.dadosPedido.Bandeira + "</bandeira>" +
                        "<produto>" + this.formaPagamentoProduto + "</produto>" +
                        "<parcelas>" + this.formaPagamentoParcelas + "</parcelas></forma-pagamento>";

            return msg;
        }

        private string XMLUrlRetorno()
        {
            return "<url-retorno>" + this.urlRetorno + "</url-retorno>";
        }

        private string XMLAutorizar()
        {
            return "<autorizar>" + this.autorizar + "</autorizar>";
        }

        private string XMLCapturar()
        {
            return "<capturar>" + this.capturar + "</capturar>";
        }
    }
}
