﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuffetWebCielo.Model;
using BuffetWebCielo.Request;
using System.Xml.Linq;
using BuffetWebCielo.Pedido;

namespace BuffetWebCielo.Transacao
{
    public class TransacoesEfetuadas
    {
        public int id_empresa { get; set; }
        public TransacoesEfetuadas(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public TransacaoConsulta Consultar(string tid)
        {
            RequestPedido request = new RequestPedido(-1);
            request.tid = tid;
            string response = request.RequisicaoConsulta();
            return this.convertToObject(response);
        }

        private TransacaoConsulta convertToObject(string xml)
        {
            XDocument doc = XDocument.Parse(xml.Replace("versao=\"1.2.1\" id=\"6fcf758e-bc60-4d6a-acf4-496593a40441\" xmlns=\"http://ecommerce.cbmp.com.br\"", ""), LoadOptions.None);
            var transacao = (from field in doc.Elements("transacao")
                             select new
                             {
                                 tid = field.Element("tid").Value,
                                 pan = field.Element("pan").Value,
                                 status = field.Element("status").Value
                             }).FirstOrDefault();
            TransacaoConsulta t = new TransacaoConsulta();
            if (transacao != null)
            {
                t.tid = transacao.tid;
                t.status = (TransacaoStatus)Convert.ToInt32(transacao.status);
            }
            else
                t.status = TransacaoStatus.SemStatus;
            return t;

            //var transacao = from field in doc.Elements("transacao")
            //                select new
            //                {
            //                    tid = field.Element("tid").Value,
            //                    status = field.Element("status").Value,
            //                    dadosPedidos = from d in field.Elements("dados-pedido")
            //                                   select new
            //                                   {
            //                                       numero = d.Element("numero").Value,
            //                                       moeda = d.Element("moeda").Value,
            //                                       valor = d.Element("valor").Value,
            //                                       dataHora = d.Element("data-hora").Value,
            //                                       idioma = d.Element("idioma").Value,
            //                                       taxaEmbarque = d.Element("taxa-embarque").Value
            //                                   },
            //                    formaPagamento = from f in field.Elements("forma-pagamento")
            //                                     select new
            //                                     {
            //                                         bandeira = f.Element("bandeira").Value,
            //                                         produto = f.Element("produto").Value,
            //                                         parcelas = f.Element("parcelas").Value,
            //                                     }
            //                };


            //foreach (var tran in transacao)
            //{
            //    t.tid = tran.tid;
            //    if (tran.status.Equals(""))
            //    {
            //        t.status = TransacaoStatus.SemStatus;
            //    }
            //    else
            //    {
            //        t.status = (TransacaoStatus)Convert.ToInt32(tran.status);
            //    }

            //    //foreach (var pedido in tran.dadosPedidos)
            //    //{
            //    //    t.dadosPedido.dataHora = Convert.ToDateTime(pedido.dataHora);
            //    //    t.dadosPedido.idioma = pedido.idioma;
            //    //    t.dadosPedido.moeda = pedido.moeda;
            //    //    t.dadosPedido.numero = pedido.numero;
            //    //    t.dadosPedido.valor = Convert.ToDecimal(pedido.valor);
            //    //}
            //    foreach (var formas in tran.formaPagamento)
            //    {
            //        t.formaPagamento.bandeira = formas.bandeira;
            //        t.formaPagamento.parcelas = Convert.ToInt32(formas.parcelas);
            //        t.formaPagamento.produto = formas.produto;
            //    }
            //}
        }
    }
}
