namespace BuffetWebData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ADMINISTRADORs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        email = c.String(unicode: false),
                        senha = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_CONVITES_DISPONIVEIS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tema = c.String(unicode: false),
                        url_image = c.String(unicode: false),
                        disponivel = c.Boolean(nullable: false),
                        titulo = c.String(unicode: false),
                        titulo_cor = c.String(unicode: false),
                        titulo_fonte = c.String(unicode: false),
                        titulo_tamanho = c.String(unicode: false),
                        titulo_top = c.String(unicode: false),
                        titulo_left = c.String(unicode: false),
                        datahora_cor = c.String(unicode: false),
                        datahora_fonte = c.String(unicode: false),
                        datahora_tamanho = c.String(unicode: false),
                        datahora_top = c.String(unicode: false),
                        datahora_left = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        descricao_cor = c.String(unicode: false),
                        descricao_fonte = c.String(unicode: false),
                        descricao_tamanho = c.String(unicode: false),
                        descricao_top = c.String(unicode: false),
                        descricao_left = c.String(unicode: false),
                        aniversariantes_cor = c.String(unicode: false),
                        aniversariantes_fonte = c.String(unicode: false),
                        aniversariantes_tamanho = c.String(unicode: false),
                        aniversariantes_top = c.String(unicode: false),
                        aniversariantes_left = c.String(unicode: false),
                        tipo_evento = c.String(unicode: false),
                        aniversariantes_titulo = c.String(unicode: false),
                        datahora_titulo = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_CONVITES_FONTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome_fonte = c.String(unicode: false),
                        nome_visivel = c.String(unicode: false),
                        caminho_fonte = c.String(unicode: false),
                        css = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_CONVITES_FRASES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titulo = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_EMPRESA_AJUSTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        chave = c.String(unicode: false),
                        valor = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_EMPRESA_USUARIOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        email = c.String(unicode: false),
                        senha = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        celular = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_MODULOS_DISPONIVEIS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        meses_plano = c.Int(nullable: false),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_PLANOS_DISPONIVEIS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        img_miniatura = c.String(unicode: false),
                        img_normal = c.String(unicode: false),
                        valor_mensal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_trimestral = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_semestral = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_anual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        disponivel_de = c.DateTime(nullable: false, precision: 0),
                        disponivel_ate = c.DateTime(precision: 0),
                        id_plano_portal = c.Int(),
                        B_PLANOS_PORTAL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.B_PLANOS_PORTAL", t => t.B_PLANOS_PORTAL_id)
                .Index(t => t.B_PLANOS_PORTAL_id);
            
            CreateTable(
                "dbo.B_PLANOS_PORTAL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        envio_proposta_quantidade_mes = c.Int(nullable: false),
                        nome = c.String(unicode: false),
                        anuncio = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.EMPRESA_CARRINHO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        adicionado_em = c.DateTime(nullable: false, precision: 0),
                        periodo = c.String(unicode: false),
                        id_plano_disponivel = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        comprado = c.Boolean(nullable: false),
                        B_PLANOS_DISPONIVEIS_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.B_PLANOS_DISPONIVEIS", t => t.B_PLANOS_DISPONIVEIS_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.B_PLANOS_DISPONIVEIS_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESAs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        codigo_empresa = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        celular = c.String(unicode: false),
                        cnpj = c.String(unicode: false),
                        razao_social = c.String(unicode: false),
                        data_da_situacao = c.DateTime(precision: 0),
                        data_da_situacao_especial = c.DateTime(precision: 0),
                        id_endereco = c.Int(),
                        codigo_atividade_economica = c.String(unicode: false),
                        codigo_atividade_economica_descricao = c.String(unicode: false),
                        codigo_natureza_juridica = c.String(unicode: false),
                        codigo_natureza_juridica_descricao = c.String(unicode: false),
                        data_consultaRFB = c.DateTime(precision: 0),
                        data_fundacao = c.DateTime(precision: 0),
                        data_motivo_especial_situacaoRFB = c.DateTime(precision: 0),
                        data_situacao_RFB = c.DateTime(precision: 0),
                        documento = c.String(unicode: false),
                        motivo_especial_situacaoRFB = c.String(unicode: false),
                        situacao_RFB = c.String(unicode: false),
                        status = c.String(unicode: false),
                        bloqueio_cnae = c.Boolean(nullable: false),
                        motivo_situacaoRFB = c.String(unicode: false),
                        contato = c.String(unicode: false),
                        aguardando = c.Boolean(nullable: false),
                        ENDERECO_id = c.Int(),
                        ENDERECO_id1 = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ENDERECOes", t => t.ENDERECO_id)
                .ForeignKey("dbo.ENDERECOes", t => t.ENDERECO_id1)
                .Index(t => t.ENDERECO_id)
                .Index(t => t.ENDERECO_id1);
            
            CreateTable(
                "dbo.CAMPANHA_CONTATOS_ENVIADOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titulo = c.String(unicode: false),
                        mensagem = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        id_cliente = c.Int(),
                        data_envio = c.DateTime(nullable: false, precision: 0),
                        data_lido = c.DateTime(precision: 0),
                        id_empresa = c.Int(nullable: false),
                        lido_email = c.Boolean(nullable: false),
                        lido_sms = c.Boolean(nullable: false),
                        endereco_ip = c.String(unicode: false),
                        hostname = c.String(unicode: false),
                        estado = c.String(unicode: false),
                        longitude = c.String(unicode: false),
                        latitude = c.String(unicode: false),
                        provedor = c.String(unicode: false),
                        cidade = c.String(unicode: false),
                        navegador_nome = c.String(unicode: false),
                        navegador_versao = c.String(unicode: false),
                        sistema_operacional = c.String(unicode: false),
                        lido_com_device = c.String(unicode: false),
                        enviado_email = c.Boolean(nullable: false),
                        enviado_sms = c.Boolean(nullable: false),
                        tipo_contato = c.String(unicode: false),
                        quantidade_cliques = c.Int(nullable: false),
                        enviado_facebook = c.Boolean(),
                        lido_facebook = c.Boolean(),
                        CLIENTE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CLIENTE_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CAMPANHA_LINHA_TEMPO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        acontecimento = c.String(unicode: false),
                        data = c.DateTime(nullable: false, precision: 0),
                        id_campanha_contatos_enviados = c.Int(nullable: false),
                        CAMPANHA_CONTATOS_ENVIADOS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CAMPANHA_CONTATOS_ENVIADOS", t => t.CAMPANHA_CONTATOS_ENVIADOS_id)
                .Index(t => t.CAMPANHA_CONTATOS_ENVIADOS_id);
            
            CreateTable(
                "dbo.CLIENTEs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        celular = c.String(unicode: false),
                        desde = c.DateTime(precision: 0),
                        email = c.String(unicode: false),
                        cpf = c.String(unicode: false),
                        rg = c.String(unicode: false),
                        id_endereco = c.Int(),
                        cliente_de = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        operadora_celular = c.String(unicode: false),
                        onde_conheceu = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        email_marketing = c.Boolean(nullable: false),
                        regiao = c.String(unicode: false),
                        ENDERECO_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ENDERECOes", t => t.ENDERECO_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.ENDERECO_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CLIENTE_ANIVERSARIANTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        nascimento = c.DateTime(nullable: false, precision: 0),
                        parentesco = c.String(unicode: false),
                        desde = c.DateTime(nullable: false, precision: 0),
                        id_cliente = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        CLIENTE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CLIENTE_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CONVITE_ANIVERSARIANTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_aniversariante = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        CLIENTE_ANIVERSARIANTES_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CLIENTE_ANIVERSARIANTES", t => t.CLIENTE_ANIVERSARIANTES_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.CLIENTE_ANIVERSARIANTES_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.ORCAMENTOes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        codigo_unico = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        email = c.String(unicode: false),
                        data_evento = c.DateTime(nullable: false, precision: 0),
                        status = c.String(unicode: false),
                        evento_para = c.DateTime(precision: 0),
                        id_tipo_servico = c.Int(),
                        senha = c.String(unicode: false),
                        data_nascimento = c.DateTime(precision: 0),
                        email2 = c.String(unicode: false),
                        instituicao = c.String(unicode: false),
                        n_convidados = c.Int(),
                        n_convidados_c = c.Int(),
                        nome2 = c.String(unicode: false),
                        razao = c.String(unicode: false),
                        nome = c.String(unicode: false),
                        bodas_de_bodas = c.String(unicode: false),
                        telefone2 = c.String(unicode: false),
                        data_criacao = c.DateTime(nullable: false, precision: 0),
                        id_cliente = c.Int(nullable: false),
                        id_tipo_evento = c.Int(),
                        aceita_contrato = c.Boolean(),
                        nome_noivo = c.String(unicode: false),
                        nome_noiva = c.String(unicode: false),
                        telefone_noivo = c.String(unicode: false),
                        telefone_noiva = c.String(unicode: false),
                        email_noivo = c.String(unicode: false),
                        email_noiva = c.String(unicode: false),
                        apenas_convite = c.Boolean(nullable: false),
                        horario_inicio = c.Time(precision: 0),
                        horario_fim = c.Time(precision: 0),
                        max_acompanhantes = c.Int(),
                        mensagem_padrao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(),
                        id_evento = c.Int(),
                        autenticacao = c.String(unicode: false),
                        CLIENTE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                        EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .Index(t => t.CLIENTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.EVENTO_id);
            
            CreateTable(
                "dbo.CONVITE_CONVIDADOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_orcamento = c.Int(nullable: false),
                        nome_completo = c.String(unicode: false),
                        email = c.String(unicode: false),
                        ira = c.String(unicode: false),
                        mensagem = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        telefone = c.String(unicode: false),
                        compareceu = c.Boolean(),
                        operadora = c.String(unicode: false),
                        sms_enviado = c.Boolean(),
                        email_enviado = c.Boolean(),
                        convidados = c.Int(),
                        facebook = c.Boolean(),
                        resposta = c.String(unicode: false),
                        id_facebook = c.String(unicode: false),
                        facebook_enviado = c.Boolean(),
                        enviado = c.Boolean(),
                        nome_facebook = c.String(unicode: false),
                        status_pequisa = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.CONVITE_CONVIDADOS_FAMILIARES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_convidado = c.Int(nullable: false),
                        nome = c.String(unicode: false),
                        tipo_convidado = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        email = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        CONVITE_CONVIDADOS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CONVITE_CONVIDADOS", t => t.CONVITE_CONVIDADOS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.CONVITE_CONVIDADOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.PESQUISA_RESPOSTA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pergunta = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        resposta = c.String(unicode: false),
                        id_convidado = c.Int(),
                        CONVITE_CONVIDADOS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        PESQUISA_PERGUNTA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CONVITE_CONVIDADOS", t => t.CONVITE_CONVIDADOS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PESQUISA_PERGUNTA", t => t.PESQUISA_PERGUNTA_id)
                .Index(t => t.CONVITE_CONVIDADOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PESQUISA_PERGUNTA_id);
            
            CreateTable(
                "dbo.PESQUISA_PERGUNTA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pesquisa = c.Int(nullable: false),
                        titulo = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        tipo_pergunta = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        PESQUISA_PESQUISAS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PESQUISA_PESQUISAS", t => t.PESQUISA_PESQUISAS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PESQUISA_PESQUISAS_id);
            
            CreateTable(
                "dbo.PESQUISA_PERGUNTA_OPCOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titulo = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_pergunta = c.Int(),
                        EMPRESA_id = c.Int(),
                        PESQUISA_PERGUNTA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PESQUISA_PERGUNTA", t => t.PESQUISA_PERGUNTA_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PESQUISA_PERGUNTA_id);
            
            CreateTable(
                "dbo.PESQUISA_RESPOSTA_OPCOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_opcao = c.Int(nullable: false),
                        resposta = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_convidado = c.Int(),
                        CONVITE_CONVIDADOS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        PESQUISA_PERGUNTA_OPCOES_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CONVITE_CONVIDADOS", t => t.CONVITE_CONVIDADOS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PESQUISA_PERGUNTA_OPCOES", t => t.PESQUISA_PERGUNTA_OPCOES_id)
                .Index(t => t.CONVITE_CONVIDADOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PESQUISA_PERGUNTA_OPCOES_id);
            
            CreateTable(
                "dbo.PESQUISA_PESQUISAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_orcamento = c.Int(nullable: false),
                        total_respostas = c.Int(nullable: false),
                        total_enviadas = c.Int(nullable: false),
                        data_envio = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        enviada = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.CONVITE_CONVIDADOS_FACEBOOK",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_orcamento = c.Int(nullable: false),
                        id_facebook = c.String(unicode: false),
                        name = c.String(unicode: false),
                        first_name = c.String(unicode: false),
                        last_name = c.String(unicode: false),
                        gender = c.String(unicode: false),
                        link = c.String(unicode: false),
                        locale = c.String(unicode: false),
                        amigo_de_nome = c.String(unicode: false),
                        amigo_de_id_facebook = c.String(unicode: false),
                        ativo = c.Boolean(nullable: false),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.CONVITE_TEMPLATE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titulo_convite = c.String(unicode: false),
                        titulo_tamanho = c.String(unicode: false),
                        titulo_top = c.String(unicode: false),
                        titulo_left = c.String(unicode: false),
                        titulo_descricao = c.String(unicode: false),
                        descricao_top = c.String(unicode: false),
                        descricao_left = c.String(unicode: false),
                        tamanho_descricao = c.String(unicode: false),
                        aniversariante_tamanho = c.String(unicode: false),
                        aniversariantes_top = c.String(unicode: false),
                        aniversariantes_left = c.String(unicode: false),
                        datahora_top = c.String(unicode: false),
                        datahora_left = c.String(unicode: false),
                        datahora_tamanho = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        id_convite = c.Int(nullable: false),
                        titulo_datahora = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.EVENTOes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        evento_de = c.DateTime(precision: 0),
                        evento_ate = c.DateTime(precision: 0),
                        descricao = c.String(unicode: false),
                        de_epoca = c.Boolean(),
                        quantidade_disponivel = c.Int(),
                        id_empresa = c.Int(nullable: false),
                        dividir_valor_pessoa = c.Boolean(nullable: false),
                        status = c.Boolean(nullable: false),
                        url = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CAMPOS_DISPONIVEIS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_tipo_evento = c.Int(nullable: false),
                        id_campos_descricao = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        CAMPOS_DESCRICAO_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CAMPOS_DESCRICAO", t => t.CAMPOS_DESCRICAO_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .Index(t => t.CAMPOS_DESCRICAO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id);
            
            CreateTable(
                "dbo.CAMPOS_DESCRICAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CAMPOS_COMPONENTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_campo_descricao = c.Int(nullable: false),
                        label = c.String(unicode: false),
                        name = c.String(unicode: false),
                        id_component = c.String(unicode: false),
                        type = c.String(unicode: false),
                        id_values_select = c.Int(),
                        required = c.Boolean(nullable: false),
                        regex = c.String(unicode: false),
                        max_length = c.Int(nullable: false),
                        css = c.String(unicode: false),
                        function = c.String(unicode: false),
                        ordem = c.Int(nullable: false),
                        CAMPOS_DESCRICAO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CAMPOS_DESCRICAO", t => t.CAMPOS_DESCRICAO_id)
                .Index(t => t.CAMPOS_DESCRICAO_id);
            
            CreateTable(
                "dbo.EVENTO_ALUGUEL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_itens_aluguel = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_tipo_evento = c.Int(nullable: false),
                        cobrar_brinde = c.Boolean(nullable: false),
                        brinde = c.Boolean(nullable: false),
                        quantidade = c.Int(nullable: false),
                        mostrar_aluguel = c.Boolean(),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                        ITENS_ALUGUEL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .ForeignKey("dbo.ITENS_ALUGUEL", t => t.ITENS_ALUGUEL_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id)
                .Index(t => t.ITENS_ALUGUEL_id);
            
            CreateTable(
                "dbo.ITENS_ALUGUEL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_venda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        status = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_categoria = c.Int(nullable: false),
                        valor_reembolso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_atraso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        quantidade_disponivel = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        CATEGORIA_ALUGUEL_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_ALUGUEL", t => t.CATEGORIA_ALUGUEL_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CATEGORIA_ALUGUEL_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CATEGORIA_ALUGUEL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.FORNECEDOR_ALUGUEL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_fornecedor = c.Int(nullable: false),
                        id_aluguel = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ITENS_ALUGUEL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_ALUGUEL", t => t.ITENS_ALUGUEL_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_ALUGUEL_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PACOTE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pacotes = c.Int(nullable: false),
                        id_cardapio = c.Int(),
                        id_aluguel = c.Int(),
                        id_diversos = c.Int(),
                        id_servico = c.Int(),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ITENS_ALUGUEL_id = c.Int(),
                        ITENS_SERVICOS_id = c.Int(),
                        ITENS_DIVERSOS_id = c.Int(),
                        ITENS_CARDAPIO_id = c.Int(),
                        ORCAMENTO_PACOTE2_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_ALUGUEL", t => t.ITENS_ALUGUEL_id)
                .ForeignKey("dbo.ITENS_SERVICOS", t => t.ITENS_SERVICOS_id)
                .ForeignKey("dbo.ITENS_DIVERSOS", t => t.ITENS_DIVERSOS_id)
                .ForeignKey("dbo.ITENS_CARDAPIO", t => t.ITENS_CARDAPIO_id)
                .ForeignKey("dbo.ORCAMENTO_PACOTE", t => t.ORCAMENTO_PACOTE2_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_ALUGUEL_id)
                .Index(t => t.ITENS_SERVICOS_id)
                .Index(t => t.ITENS_DIVERSOS_id)
                .Index(t => t.ITENS_CARDAPIO_id)
                .Index(t => t.ORCAMENTO_PACOTE2_id);
            
            CreateTable(
                "dbo.ITENS_CARDAPIO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        id_categoria = c.Int(nullable: false),
                        status = c.Boolean(),
                        descricao = c.String(unicode: false),
                        valor_compra = c.Decimal(precision: 18, scale: 2),
                        id_unidade_medida = c.Int(),
                        rendimento = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_empresa = c.Int(nullable: false),
                        receita = c.String(unicode: false),
                        id_sub_categoria = c.Int(),
                        quantidade_embalagem = c.Decimal(precision: 18, scale: 2),
                        custo_pessoa = c.Decimal(precision: 18, scale: 2),
                        margem_lucro = c.Decimal(precision: 18, scale: 2),
                        venda_pessoa = c.Decimal(precision: 18, scale: 2),
                        url_image = c.String(unicode: false),
                        CATEGORIA_ITENS_id = c.Int(),
                        SUB_CATEGORIA_ITENS_id = c.Int(),
                        UNIDADE_MEDIDA_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_ITENS", t => t.CATEGORIA_ITENS_id)
                .ForeignKey("dbo.SUB_CATEGORIA_ITENS", t => t.SUB_CATEGORIA_ITENS_id)
                .ForeignKey("dbo.UNIDADE_MEDIDA", t => t.UNIDADE_MEDIDA_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CATEGORIA_ITENS_id)
                .Index(t => t.SUB_CATEGORIA_ITENS_id)
                .Index(t => t.UNIDADE_MEDIDA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CATEGORIA_ITENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.PACOTE_CATEGORIA_ITENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        editavel = c.Boolean(),
                        maximo_itens = c.Int(nullable: false),
                        id_pacote_evento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_categoria = c.Int(nullable: false),
                        CATEGORIA_ITENS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        PACOTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_ITENS", t => t.CATEGORIA_ITENS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PACOTEs", t => t.PACOTE_id)
                .Index(t => t.CATEGORIA_ITENS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PACOTE_id);
            
            CreateTable(
                "dbo.PACOTEs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        data_criacao = c.DateTime(nullable: false, precision: 0),
                        mostrar_valor = c.Boolean(nullable: false),
                        status = c.Boolean(),
                        id_empresa = c.Int(nullable: false),
                        margem_lucro = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_venda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        url_image = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EVENTOS_PACOTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pacote = c.Int(nullable: false),
                        id_evento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                        PACOTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .ForeignKey("dbo.PACOTEs", t => t.PACOTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id)
                .Index(t => t.PACOTE_id);
            
            CreateTable(
                "dbo.IMAGENS_PACOTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pacote = c.Int(),
                        nome = c.String(unicode: false),
                        url = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        PACOTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PACOTEs", t => t.PACOTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PACOTE_id);
            
            CreateTable(
                "dbo.PACOTES_ITENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_categoria_pacote = c.Int(nullable: false),
                        id_categoria = c.Int(nullable: false),
                        id_itens_cardapio = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        CATEGORIA_ITENS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ITENS_CARDAPIO_id = c.Int(),
                        PACOTE_CATEGORIA_ITENS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_ITENS", t => t.CATEGORIA_ITENS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_CARDAPIO", t => t.ITENS_CARDAPIO_id)
                .ForeignKey("dbo.PACOTE_CATEGORIA_ITENS", t => t.PACOTE_CATEGORIA_ITENS_id)
                .Index(t => t.CATEGORIA_ITENS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_CARDAPIO_id)
                .Index(t => t.PACOTE_CATEGORIA_ITENS_id);
            
            CreateTable(
                "dbo.SUB_CATEGORIA_ITENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        id_categoria_itens = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_unidade_medida = c.Int(nullable: false),
                        rendimento_pessoa = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CATEGORIA_ITENS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        UNIDADE_MEDIDA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_ITENS", t => t.CATEGORIA_ITENS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.UNIDADE_MEDIDA", t => t.UNIDADE_MEDIDA_id)
                .Index(t => t.CATEGORIA_ITENS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.UNIDADE_MEDIDA_id);
            
            CreateTable(
                "dbo.UNIDADE_MEDIDA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        sigla = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.INGREDIENTEs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        marca = c.String(unicode: false),
                        quantidade_embalagem = c.String(unicode: false),
                        id_unidade_medida = c.Int(nullable: false),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        UNIDADE_MEDIDA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.UNIDADE_MEDIDA", t => t.UNIDADE_MEDIDA_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.UNIDADE_MEDIDA_id);
            
            CreateTable(
                "dbo.FORNECEDOR_INGREDIENTE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(nullable: false),
                        id_ingrediente = c.Int(nullable: false),
                        id_fornecedor = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        FORNECEDOR_id = c.Int(),
                        INGREDIENTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.FORNECEDORs", t => t.FORNECEDOR_id)
                .ForeignKey("dbo.INGREDIENTEs", t => t.INGREDIENTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.FORNECEDOR_id)
                .Index(t => t.INGREDIENTE_id);
            
            CreateTable(
                "dbo.FORNECEDORs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        razao_social = c.String(unicode: false),
                        cnpj = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        contato = c.String(unicode: false),
                        telefone_contato = c.String(unicode: false),
                        celular_contato = c.String(unicode: false),
                        email = c.String(unicode: false),
                        id_endereco = c.Int(),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ENDERECO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ENDERECOes", t => t.ENDERECO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ENDERECO_id);
            
            CreateTable(
                "dbo.ENDERECOes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cep = c.String(unicode: false),
                        endereco1 = c.String(unicode: false),
                        bairro = c.String(unicode: false),
                        cidade = c.String(unicode: false),
                        estado = c.String(unicode: false),
                        complemento = c.String(unicode: false),
                        numero = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        longitude = c.Decimal(precision: 18, scale: 2),
                        latitude = c.Decimal(precision: 18, scale: 2),
                        EMPRESA_id = c.Int(),
                        EMPRESA_id1 = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id1)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id1);
            
            CreateTable(
                "dbo.LOCAL_EVENTO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        id_endereco = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        capacidade = c.Int(nullable: false),
                        porEspaco = c.Boolean(nullable: false),
                        valorMinimo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        telefone = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        regiao = c.String(unicode: false),
                        idade_minima_pagante = c.Int(),
                        horario_inicio_almoco = c.Time(precision: 0),
                        horario_fim_almoco = c.Time(precision: 0),
                        horario_inicio_jantar = c.Time(precision: 0),
                        horario_fim_jantar = c.Time(precision: 0),
                        palavras_chaves = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        video = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        ENDERECO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ENDERECOes", t => t.ENDERECO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ENDERECO_id);
            
            CreateTable(
                "dbo.B_PORTAL_DENUNCIAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        comentario = c.String(unicode: false),
                        desde = c.DateTime(nullable: false, precision: 0),
                        id_local_evento = c.Int(nullable: false),
                        resolvido = c.Boolean(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.EMPRESA_CARRINHO_CUPOM",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        valor_desconto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        disponivel_ate = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        descricao = c.String(unicode: false),
                        usado = c.Boolean(nullable: false),
                        id_planos_disponiveis = c.Int(nullable: false),
                        periodo = c.String(unicode: false),
                        B_PLANOS_DISPONIVEIS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.B_PLANOS_DISPONIVEIS", t => t.B_PLANOS_DISPONIVEIS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.B_PLANOS_DISPONIVEIS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.EMPRESA_HISTORICO_PAGAMENTO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(),
                        produto = c.String(unicode: false),
                        comprado_em = c.DateTime(nullable: false, precision: 0),
                        cartao = c.String(unicode: false),
                        autenticacao_externa = c.String(unicode: false),
                        autenticacao_interna = c.String(unicode: false),
                        id_cupom_promocional = c.Int(),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        forma_pagamento = c.String(unicode: false),
                        status = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        EMPRESA_CARRINHO_CUPOM_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EMPRESA_CARRINHO_CUPOM", t => t.EMPRESA_CARRINHO_CUPOM_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_CARRINHO_CUPOM_id);
            
            CreateTable(
                "dbo.IMAGENS_ESPACO_LOCAL_EVENTOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        url = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.LOCAL_EVENTO_COMENTARIOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        comentario = c.String(unicode: false),
                        desde = c.DateTime(nullable: false, precision: 0),
                        avaliacao = c.Int(nullable: false),
                        id_cliente_perfil = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        analisado = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                        PORTAL_CLIENTE_PERFIL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.PORTAL_CLIENTE_PERFIL", t => t.PORTAL_CLIENTE_PERFIL_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.PORTAL_CLIENTE_PERFIL_id);
            
            CreateTable(
                "dbo.PORTAL_CLIENTE_PERFIL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        email = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        celular = c.String(unicode: false),
                        operadora_celular = c.String(unicode: false),
                        perfil = c.String(unicode: false),
                        url = c.String(unicode: false),
                        regiao = c.String(unicode: false),
                        senha = c.String(unicode: false),
                        id_facebook = c.String(unicode: false),
                        cep = c.String(unicode: false),
                        logradouro = c.String(unicode: false),
                        numero = c.String(unicode: false),
                        bairro = c.String(unicode: false),
                        cidade = c.String(unicode: false),
                        estado = c.String(unicode: false),
                        renovarSenha = c.String(unicode: false),
                        signedRequest = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PORTAL_CLIENTE_FAVORITOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        desde = c.DateTime(nullable: false, precision: 0),
                        id_cliente_perfil = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                        PORTAL_CLIENTE_PERFIL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.PORTAL_CLIENTE_PERFIL", t => t.PORTAL_CLIENTE_PERFIL_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.PORTAL_CLIENTE_PERFIL_id);
            
            CreateTable(
                "dbo.PORTAL_CLIENTE_PESQUISAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        termo = c.String(unicode: false),
                        url = c.String(unicode: false),
                        data = c.DateTime(nullable: false, precision: 0),
                        id_perfil_cliente = c.Int(),
                        mensagem = c.String(unicode: false),
                        PORTAL_CLIENTE_PERFIL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.PORTAL_CLIENTE_PERFIL", t => t.PORTAL_CLIENTE_PERFIL_id)
                .Index(t => t.PORTAL_CLIENTE_PERFIL_id);
            
            CreateTable(
                "dbo.PORTAL_VISUALIZACOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        id_cliente_portal = c.Int(),
                        id_local_evento = c.Int(nullable: false),
                        tipo = c.String(unicode: false),
                        facebook = c.String(unicode: false),
                        LOCAL_EVENTO_id = c.Int(),
                        PORTAL_CLIENTE_PERFIL_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.PORTAL_CLIENTE_PERFIL", t => t.PORTAL_CLIENTE_PERFIL_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.PORTAL_CLIENTE_PERFIL_id);
            
            CreateTable(
                "dbo.LOCAL_EVENTO_IMAGENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        sequencia = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.LOCAL_EVENTO_SOCIAL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        plataforma = c.String(unicode: false),
                        url = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PROPOSTA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome_proposta = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(),
                        autenticacao = c.String(unicode: false),
                        porcento_desconto_avista = c.Int(nullable: false),
                        quantidade_parcelas = c.Int(nullable: false),
                        validade_dias = c.Int(nullable: false),
                        permitir_palavra_chave = c.Boolean(nullable: false),
                        categoria_proposta = c.String(unicode: false),
                        boleto = c.Boolean(),
                        cartao = c.Boolean(),
                        idade_minima = c.Int(),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.CLIENTE_PROPOSTA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_cliente = c.Int(),
                        data_envio = c.DateTime(nullable: false, precision: 0),
                        proposta_de = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        convidados = c.Int(),
                        data_evento = c.DateTime(precision: 0),
                        periodo = c.String(unicode: false),
                        id_proposta = c.Int(nullable: false),
                        autenticacao = c.String(unicode: false),
                        disponivel = c.Boolean(nullable: false),
                        disponivel_ate = c.DateTime(nullable: false, precision: 0),
                        CLIENTE_id = c.Int(),
                        CLIENTE1_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PROPOSTA_id = c.Int(),
                        CLIENTE_id1 = c.Int(),
                        CLIENTE_id2 = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE1_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PROPOSTA", t => t.ORCAMENTO_PROPOSTA_id)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id1)
                .ForeignKey("dbo.CLIENTEs", t => t.CLIENTE_id2)
                .Index(t => t.CLIENTE_id)
                .Index(t => t.CLIENTE1_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PROPOSTA_id)
                .Index(t => t.CLIENTE_id1)
                .Index(t => t.CLIENTE_id2);
            
            CreateTable(
                "dbo.ORCAMENTO_PROPOSTA_ABAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome_aba = c.String(unicode: false),
                        id_orcamento_proposta = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        disponivel = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PROPOSTA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PROPOSTA", t => t.ORCAMENTO_PROPOSTA_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PROPOSTA_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PROPOSTA_DESCRICAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_orcamento_proposta_aba = c.Int(nullable: false),
                        disponivel = c.Boolean(nullable: false),
                        coluna = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PROPOSTA_ABAS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PROPOSTA_ABAS", t => t.ORCAMENTO_PROPOSTA_ABAS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PROPOSTA_ABAS_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PROPOSTA_PACOTE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        quantidade_pessoas = c.Int(nullable: false),
                        id_proposta = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        ativo = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PROPOSTA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PROPOSTA", t => t.ORCAMENTO_PROPOSTA_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PROPOSTA_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        valor_almoco = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_jantar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        segunda = c.Boolean(nullable: false),
                        terca = c.Boolean(nullable: false),
                        quarta = c.Boolean(nullable: false),
                        quinta = c.Boolean(nullable: false),
                        sexta = c.Boolean(nullable: false),
                        sabado = c.Boolean(nullable: false),
                        id_proposta_pacote = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        domingo = c.Boolean(),
                        valor_adicional_pessoa = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PROPOSTA_PACOTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PROPOSTA_PACOTE", t => t.ORCAMENTO_PROPOSTA_PACOTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PROPOSTA_PACOTE_id);
            
            CreateTable(
                "dbo.ORCAMENTO_VISITAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        mensagem = c.String(unicode: false),
                        status_visita = c.String(unicode: false),
                        id_local_evento = c.Int(nullable: false),
                        id_horario = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        tipo_visita = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                        VISITAS_AGENDADAS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .ForeignKey("dbo.VISITAS_AGENDADAS", t => t.VISITAS_AGENDADAS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id)
                .Index(t => t.ORCAMENTO_id)
                .Index(t => t.VISITAS_AGENDADAS_id);
            
            CreateTable(
                "dbo.VISITAS_AGENDADAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(nullable: false),
                        dia = c.String(unicode: false),
                        data = c.DateTime(nullable: false, precision: 0),
                        id_visitas_disponiveis = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                        VISITAS_DISPONIVEIS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .ForeignKey("dbo.VISITAS_DISPONIVEIS", t => t.VISITAS_DISPONIVEIS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id)
                .Index(t => t.VISITAS_DISPONIVEIS_id);
            
            CreateTable(
                "dbo.VISITAS_DISPONIVEIS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        dia_visita = c.String(unicode: false),
                        horario = c.Time(nullable: false, precision: 0),
                        tipo_visita = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.PORTAL_ANUNCIANTE_DESTAQUE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_local_evento = c.Int(nullable: false),
                        de = c.DateTime(nullable: false, precision: 0),
                        ate = c.DateTime(nullable: false, precision: 0),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.PORTAL_ANUNCIANTE_HEADER",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_local_evento = c.Int(nullable: false),
                        de = c.DateTime(nullable: false, precision: 0),
                        ate = c.DateTime(nullable: false, precision: 0),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.PORTAL_ANUNCIANTE_HOME",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_local_evento = c.Int(nullable: false),
                        de = c.DateTime(nullable: false, precision: 0),
                        ate = c.DateTime(nullable: false, precision: 0),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.PORTAL_ANUNCIANTE_PROMOCAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_portal_promocao = c.Int(nullable: false),
                        id_local_evento = c.Int(nullable: false),
                        disponivel = c.Boolean(nullable: false),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        B_PORTAL_PROMOCAO_id = c.Int(),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.B_PORTAL_PROMOCAO", t => t.B_PORTAL_PROMOCAO_id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.B_PORTAL_PROMOCAO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.B_PORTAL_PROMOCAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        disponivel_de = c.DateTime(nullable: false, precision: 0),
                        disponivel_ate = c.DateTime(nullable: false, precision: 0),
                        desconto_de = c.Decimal(nullable: false, precision: 18, scale: 2),
                        url_imagem = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PORTAL_ANUNCIANTE_TOPMENU",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_local_evento = c.Int(nullable: false),
                        de = c.DateTime(nullable: false, precision: 0),
                        ate = c.DateTime(nullable: false, precision: 0),
                        regiao = c.String(unicode: false),
                        destaque_pesquisa_zona = c.Boolean(nullable: false),
                        destaque_mapa = c.Boolean(nullable: false),
                        envio_proposta = c.Boolean(nullable: false),
                        LOCAL_EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.LOCAL_EVENTO", t => t.LOCAL_EVENTO_id)
                .Index(t => t.LOCAL_EVENTO_id);
            
            CreateTable(
                "dbo.ITENS_INGREDIENTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_cardapio = c.Int(nullable: false),
                        id_ingrediente = c.Int(nullable: false),
                        valor_atual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_empresa = c.Int(nullable: false),
                        quantidade = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        INGREDIENTE_id = c.Int(),
                        ITENS_CARDAPIO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.INGREDIENTEs", t => t.INGREDIENTE_id)
                .ForeignKey("dbo.ITENS_CARDAPIO", t => t.ITENS_CARDAPIO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.INGREDIENTE_id)
                .Index(t => t.ITENS_CARDAPIO_id);
            
            CreateTable(
                "dbo.ITENS_DIVERSOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_venda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        status = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_categoria = c.Int(nullable: false),
                        id_unidade_medida = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        CATEGORIA_SERVICOS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        UNIDADE_MEDIDA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_SERVICOS", t => t.CATEGORIA_SERVICOS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.UNIDADE_MEDIDA", t => t.UNIDADE_MEDIDA_id)
                .Index(t => t.CATEGORIA_SERVICOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.UNIDADE_MEDIDA_id);
            
            CreateTable(
                "dbo.CATEGORIA_SERVICOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.ITENS_SERVICOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_venda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        status = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_categoria = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        CATEGORIA_SERVICOS_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CATEGORIA_SERVICOS", t => t.CATEGORIA_SERVICOS_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CATEGORIA_SERVICOS_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EVENTO_SERVICOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_itens_servicos = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_tipo_evento = c.Int(nullable: false),
                        cobrar_brinde = c.Boolean(nullable: false),
                        brinde = c.Boolean(nullable: false),
                        quantidade = c.Int(nullable: false),
                        mostrar_servico = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                        ITENS_SERVICOS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .ForeignKey("dbo.ITENS_SERVICOS", t => t.ITENS_SERVICOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id)
                .Index(t => t.ITENS_SERVICOS_id);
            
            CreateTable(
                "dbo.PRESTADOR_SERVICO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_servico = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_prestador_funcao = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ITENS_SERVICOS_id = c.Int(),
                        PRESTADOR_FUNCAO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_SERVICOS", t => t.ITENS_SERVICOS_id)
                .ForeignKey("dbo.PRESTADOR_FUNCAO", t => t.PRESTADOR_FUNCAO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_SERVICOS_id)
                .Index(t => t.PRESTADOR_FUNCAO_id);
            
            CreateTable(
                "dbo.PRESTADOR_FUNCAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.PRESTADORs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        sexo = c.Boolean(nullable: false),
                        valor_custo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        valor_venda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_funcao = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        segunda = c.String(unicode: false),
                        terca = c.String(unicode: false),
                        quarta = c.String(unicode: false),
                        quinta = c.String(unicode: false),
                        sexta = c.String(unicode: false),
                        sabado = c.String(unicode: false),
                        domingo = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        PRESTADOR_FUNCAO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PRESTADOR_FUNCAO", t => t.PRESTADOR_FUNCAO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PRESTADOR_FUNCAO_id);
            
            CreateTable(
                "dbo.EVENTO_DIVERSOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_itens_diversos = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        id_tipo_evento = c.Int(nullable: false),
                        cobrar_brinde = c.Boolean(nullable: false),
                        brinde = c.Boolean(nullable: false),
                        quantidade = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                        ITENS_DIVERSOS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .ForeignKey("dbo.ITENS_DIVERSOS", t => t.ITENS_DIVERSOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id)
                .Index(t => t.ITENS_DIVERSOS_id);
            
            CreateTable(
                "dbo.FORNECEDOR_CARDAPIO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_fornecedor = c.Int(nullable: false),
                        id_cardapio = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ITENS_CARDAPIO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_CARDAPIO", t => t.ITENS_CARDAPIO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_CARDAPIO_id);
            
            CreateTable(
                "dbo.ORCAMENTO_ITENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        valor_atual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_cardapio = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        data_objeto = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ITENS_CARDAPIO_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ITENS_CARDAPIO", t => t.ITENS_CARDAPIO_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ITENS_CARDAPIO_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.ORCAMENTO_PACOTES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_pacote = c.Int(nullable: false),
                        nome_pacote = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        disponivel = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_PACOTE_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTO_PACOTE", t => t.ORCAMENTO_PACOTE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_PACOTE_id);
            
            CreateTable(
                "dbo.EVENTO_PRESTADORES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        quantidade = c.Int(nullable: false),
                        id_evento = c.Int(nullable: false),
                        tipo = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id);
            
            CreateTable(
                "dbo.POTENCIAL_CLIENTE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        email = c.String(unicode: false),
                        id_festa = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        EVENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.EVENTOes", t => t.EVENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.EVENTO_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_BOLETO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        banco = c.String(unicode: false),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.COLABORADOREs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        email = c.String(unicode: false),
                        senha = c.String(unicode: false),
                        sessao = c.String(unicode: false),
                        id_perfil = c.Int(nullable: false),
                        mostrar_dica = c.Boolean(),
                        id_empresa = c.Int(nullable: false),
                        url = c.String(unicode: false),
                        id_notificacao = c.Int(),
                        administrador = c.Boolean(),
                        celular = c.String(unicode: false),
                        COLABORADOR_NOTIFICACAO_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        PERFIL_COLABORADOR_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOR_NOTIFICACAO", t => t.COLABORADOR_NOTIFICACAO_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.PERFIL_COLABORADOR", t => t.PERFIL_COLABORADOR_id)
                .Index(t => t.COLABORADOR_NOTIFICACAO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.PERFIL_COLABORADOR_id);
            
            CreateTable(
                "dbo.COLABORADOR_DEVICES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                        status = c.Boolean(nullable: false),
                        desde = c.DateTime(nullable: false, precision: 0),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        device = c.String(unicode: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.COLABORADOR_NOTIFICACAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        orcamento_novo_pedido = c.Boolean(nullable: false),
                        orcamento_gendar_visita = c.Boolean(nullable: false),
                        orcamento_fecha_pedido = c.Boolean(nullable: false),
                        convite = c.Boolean(nullable: false),
                        financeiro_pagamento_efetuado = c.Boolean(nullable: false),
                        estoque = c.Boolean(nullable: false),
                        notificacao_email = c.Boolean(nullable: false),
                        pesquisa_concluida = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CONTATO_PLATAFORMA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        email = c.String(unicode: false),
                        titulo = c.String(unicode: false),
                        mensagem_envio = c.String(unicode: false),
                        data_envio = c.DateTime(nullable: false, precision: 0),
                        mensagem_resposta = c.String(unicode: false),
                        data_resposta = c.DateTime(precision: 0),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        autenticacao = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        tipo_contato = c.String(unicode: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_CHEQUE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        banco = c.String(unicode: false),
                        agencia = c.String(unicode: false),
                        conta = c.String(unicode: false),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        n_cheque = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_CREDITO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        n_parcelas = c.Int(nullable: false),
                        n_autorizacao = c.Int(nullable: false),
                        id_bandeira = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        FINANCEIRO_BANDEIRAS_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.FINANCEIRO_BANDEIRAS", t => t.FINANCEIRO_BANDEIRAS_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.FINANCEIRO_BANDEIRAS_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.FINANCEIRO_BANDEIRAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        bandeira = c.String(unicode: false),
                        ativo = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_DEBITO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        n_autorizacao = c.Int(nullable: false),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_DEPOSITO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        banco = c.String(unicode: false),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.FINANCEIRO_P_ESPECIE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false, precision: 0),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        id_orcamento = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.NOTIFICACOes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        mensagem = c.String(unicode: false),
                        data = c.DateTime(nullable: false, precision: 0),
                        lido = c.Boolean(nullable: false),
                        deBuffetWeb = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        para_id_colaborador = c.Int(),
                        de_id_colaborador = c.Int(),
                        paraBuffetWeb = c.Boolean(),
                        COLABORADORE_id = c.Int(),
                        COLABORADORE1_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        COLABORADORE_id1 = c.Int(),
                        COLABORADORE_id2 = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE1_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id1)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id2)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.COLABORADORE1_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.COLABORADORE_id1)
                .Index(t => t.COLABORADORE_id2);
            
            CreateTable(
                "dbo.ORCAMENTO_ANOTACOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_orcamento = c.Int(nullable: false),
                        titulo = c.String(unicode: false),
                        anotacao = c.String(unicode: false),
                        data_horario = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        id_colaborador = c.Int(nullable: false),
                        COLABORADORE_id = c.Int(),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.COLABORADOREs", t => t.COLABORADORE_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.COLABORADORE_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.PERFIL_COLABORADOR",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        c_itens = c.Boolean(nullable: false),
                        v_itens = c.Boolean(nullable: false),
                        e_ajustes = c.Boolean(nullable: false),
                        c_clientes = c.Boolean(nullable: false),
                        v_clientes = c.Boolean(nullable: false),
                        c_colaborador = c.Boolean(nullable: false),
                        v_colaborador = c.Boolean(nullable: false),
                        c_fornecedores = c.Boolean(nullable: false),
                        v_fornecedores = c.Boolean(nullable: false),
                        c_ingredientes = c.Boolean(nullable: false),
                        v_ingredientes = c.Boolean(nullable: false),
                        c_eventos = c.Boolean(nullable: false),
                        v_eventos = c.Boolean(nullable: false),
                        c_tipo_eventos = c.Boolean(nullable: false),
                        v_tipo_eventos = c.Boolean(nullable: false),
                        c_forma_pagamento = c.Boolean(nullable: false),
                        v_forma_pagamento = c.Boolean(nullable: false),
                        c_galeria = c.Boolean(nullable: false),
                        v_galeria = c.Boolean(nullable: false),
                        c_orcamentos = c.Boolean(nullable: false),
                        v_orcamentos = c.Boolean(nullable: false),
                        c_pacotes = c.Boolean(nullable: false),
                        v_pacotes = c.Boolean(nullable: false),
                        c_visitas = c.Boolean(nullable: false),
                        v_visitas = c.Boolean(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        c_financeiro = c.Boolean(nullable: false),
                        v_financeiro = c.Boolean(nullable: false),
                        v_configuracao = c.Boolean(nullable: false),
                        c_configuracao = c.Boolean(nullable: false),
                        c_estoque = c.Boolean(nullable: false),
                        v_estoque = c.Boolean(nullable: false),
                        v_convite = c.Boolean(nullable: false),
                        c_convite = c.Boolean(nullable: false),
                        c_ajustes = c.Boolean(nullable: false),
                        v_ajustes = c.Boolean(nullable: false),
                        device = c.Boolean(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.IMAGENS_GALERIA_EVENTOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(nullable: false),
                        nome = c.String(unicode: false),
                        id_orcamento = c.Int(nullable: false),
                        descricao = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                        ORCAMENTO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.ORCAMENTOes", t => t.ORCAMENTO_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.ORCAMENTO_id);
            
            CreateTable(
                "dbo.IMAGENS_DOS_EVENTOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        url = c.String(unicode: false),
                        id_imagens_evento = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                        IMAGENS_GALERIA_EVENTOS_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .ForeignKey("dbo.IMAGENS_GALERIA_EVENTOS", t => t.IMAGENS_GALERIA_EVENTOS_id)
                .Index(t => t.EMPRESA_id)
                .Index(t => t.IMAGENS_GALERIA_EVENTOS_id);
            
            CreateTable(
                "dbo.CAMPANHA_LISTA",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        data_inclusao = c.DateTime(nullable: false, precision: 0),
                        regras_idade_minima = c.Int(),
                        regras_idade_maxima = c.Int(),
                        regras_reigao = c.String(unicode: false),
                        com_filhos = c.Boolean(),
                        sexo = c.String(unicode: false),
                        com_orcamentos = c.Boolean(),
                        id_empresa = c.Int(nullable: false),
                        confirmados = c.Boolean(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CAMPANHAs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        assunto = c.String(unicode: false),
                        data_inclusao = c.DateTime(nullable: false, precision: 0),
                        id_lista = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        CAMPANHA_LISTA_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CAMPANHA_LISTA", t => t.CAMPANHA_LISTA_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.CAMPANHA_LISTA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CATEGORIA_DIVERSOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        url_image = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CLIENTE_TIMELINE",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        email = c.String(unicode: false),
                        titulo = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        icone = c.String(unicode: false),
                        data = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CONVITE_PERGUNTA_OPCOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        pergunta = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.CONVITE_RESPOSTAS_OPCOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_opcao = c.Int(nullable: false),
                        id_pergunta_opcao = c.Int(nullable: false),
                        id_empresa = c.Int(nullable: false),
                        C_CONVITE_OPCOES_id = c.Int(),
                        CONVITE_PERGUNTA_OPCOES_id = c.Int(),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.C_CONVITE_OPCOES", t => t.C_CONVITE_OPCOES_id)
                .ForeignKey("dbo.CONVITE_PERGUNTA_OPCOES", t => t.CONVITE_PERGUNTA_OPCOES_id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.C_CONVITE_OPCOES_id)
                .Index(t => t.CONVITE_PERGUNTA_OPCOES_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.C_CONVITE_OPCOES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.EMPRESA_ARQUIVOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        chave = c.String(unicode: false),
                        valor = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESA_CNAES",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cnae = c.String(unicode: false),
                        cnae_descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESA_CONFIGURACAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(nullable: false),
                        email_porta = c.String(unicode: false),
                        email_login = c.String(unicode: false),
                        email_host = c.String(unicode: false),
                        email_senha = c.String(unicode: false),
                        permitirSMS = c.Boolean(nullable: false),
                        pesquisa_enviar_automaticamente = c.Boolean(),
                        pesquisa_dias_apos_evento = c.Int(),
                        quantidade_usuarios = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESA_MODULOS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(nullable: false),
                        modulo = c.String(unicode: false),
                        adquirido_em = c.DateTime(nullable: false, precision: 0),
                        expira_em = c.DateTime(nullable: false, precision: 0),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESA_PACOTE_SMS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        quantidade_pacote = c.Int(nullable: false),
                        quantidade_disponivel = c.Int(),
                        valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        autenticacao = c.String(unicode: false),
                        data_compra = c.DateTime(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.EMPRESA_PAGAMENTO_CARTAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_empresa = c.Int(),
                        bandeira = c.String(unicode: false),
                        nome_titular = c.String(unicode: false),
                        ultimos_digitos = c.String(unicode: false),
                        token_autorizacao = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.IMAGENS_ITEM",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        url = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        pasta = c.String(unicode: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.ONDE_CONHECEU",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.TIPO_SERVICO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        id_empresa = c.Int(nullable: false),
                        EMPRESA_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.EMPRESAs", t => t.EMPRESA_id)
                .Index(t => t.EMPRESA_id);
            
            CreateTable(
                "dbo.B_PORTAL_BANNER_PROMOCIONAL",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titulo = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        tipo_banner = c.String(unicode: false),
                        url = c.String(unicode: false),
                        url_imagem = c.String(unicode: false),
                        duracao = c.Int(nullable: false),
                        indeterminado = c.Boolean(nullable: false),
                        disponivel_de = c.DateTime(precision: 0),
                        disponivel_ate = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_POSSIVEL_PARCEIRO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        email = c.String(unicode: false),
                        celular = c.String(unicode: false),
                        operadora = c.String(unicode: false),
                        senha = c.String(unicode: false),
                        desde = c.DateTime(nullable: false, precision: 0),
                        status = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_SERVICO_MAPEAR_REGIAO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        url = c.String(unicode: false),
                        quantidade_mapeada = c.Int(nullable: false),
                        ultima_atualizacao = c.DateTime(precision: 0),
                        concorrente = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_SERVICO_PARCEIRO_MAPEADO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        descricao = c.String(unicode: false),
                        telefone = c.String(unicode: false),
                        rua = c.String(unicode: false),
                        localidade = c.String(unicode: false),
                        status = c.String(unicode: false),
                        plano_atual = c.String(unicode: false),
                        mapeado_em = c.DateTime(precision: 0),
                        atualizado_em = c.DateTime(precision: 0),
                        de_concorrente = c.String(unicode: false),
                        site_portal = c.String(unicode: false),
                        site = c.String(unicode: false),
                        video = c.String(unicode: false),
                        email = c.String(unicode: false),
                        logo = c.String(unicode: false),
                        autenticacao = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        url = c.String(unicode: false),
                        id_parceiro_mapeado = c.Int(nullable: false),
                        B_SERVICO_PARCEIRO_MAPEADO_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.B_SERVICO_PARCEIRO_MAPEADO", t => t.B_SERVICO_PARCEIRO_MAPEADO_id)
                .Index(t => t.B_SERVICO_PARCEIRO_MAPEADO_id);
            
            CreateTable(
                "dbo.C_FORMA_PAGAMENTO",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        identitifcador = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.HORARIOs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(unicode: false),
                        horario_de = c.Time(nullable: false, precision: 0),
                        horario_ate = c.Time(nullable: false, precision: 0),
                        id_empresa = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS", "B_SERVICO_PARCEIRO_MAPEADO_id", "dbo.B_SERVICO_PARCEIRO_MAPEADO");
            DropForeignKey("dbo.TIPO_SERVICO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ONDE_CONHECEU", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.IMAGENS_ITEM", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ENDERECOes", "EMPRESA_id1", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESAs", "ENDERECO_id1", "dbo.ENDERECOes");
            DropForeignKey("dbo.EMPRESA_PAGAMENTO_CARTAO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_PACOTE_SMS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_MODULOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_CONFIGURACAO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_CNAES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_CARRINHO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_ARQUIVOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_PERGUNTA_OPCOES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_RESPOSTAS_OPCOES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_RESPOSTAS_OPCOES", "CONVITE_PERGUNTA_OPCOES_id", "dbo.CONVITE_PERGUNTA_OPCOES");
            DropForeignKey("dbo.CONVITE_RESPOSTAS_OPCOES", "C_CONVITE_OPCOES_id", "dbo.C_CONVITE_OPCOES");
            DropForeignKey("dbo.CLIENTE_TIMELINE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CATEGORIA_DIVERSOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CAMPANHA_LISTA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CAMPANHAs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CAMPANHAs", "CAMPANHA_LISTA_id", "dbo.CAMPANHA_LISTA");
            DropForeignKey("dbo.CAMPANHA_CONTATOS_ENVIADOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CLIENTEs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "CLIENTE_id2", "dbo.CLIENTEs");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "CLIENTE_id1", "dbo.CLIENTEs");
            DropForeignKey("dbo.CLIENTE_ANIVERSARIANTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.IMAGENS_GALERIA_EVENTOS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.IMAGENS_DOS_EVENTOS", "IMAGENS_GALERIA_EVENTOS_id", "dbo.IMAGENS_GALERIA_EVENTOS");
            DropForeignKey("dbo.IMAGENS_DOS_EVENTOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.IMAGENS_GALERIA_EVENTOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_BOLETO", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_BOLETO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PERFIL_COLABORADOR", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.COLABORADOREs", "PERFIL_COLABORADOR_id", "dbo.PERFIL_COLABORADOR");
            DropForeignKey("dbo.ORCAMENTO_ANOTACOES", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.ORCAMENTO_ANOTACOES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_ANOTACOES", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.NOTIFICACOes", "COLABORADORE_id2", "dbo.COLABORADOREs");
            DropForeignKey("dbo.NOTIFICACOes", "COLABORADORE_id1", "dbo.COLABORADOREs");
            DropForeignKey("dbo.NOTIFICACOes", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.NOTIFICACOes", "COLABORADORE1_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.NOTIFICACOes", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_ESPECIE", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_ESPECIE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_ESPECIE", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_DEPOSITO", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_DEPOSITO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_DEPOSITO", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_DEBITO", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_DEBITO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_DEBITO", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_CREDITO", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_CREDITO", "FINANCEIRO_BANDEIRAS_id", "dbo.FINANCEIRO_BANDEIRAS");
            DropForeignKey("dbo.FINANCEIRO_BANDEIRAS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_CREDITO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_CREDITO", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_CHEQUE", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.FINANCEIRO_P_CHEQUE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FINANCEIRO_P_CHEQUE", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.FINANCEIRO_P_BOLETO", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.COLABORADOREs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONTATO_PLATAFORMA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONTATO_PLATAFORMA", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.COLABORADOREs", "COLABORADOR_NOTIFICACAO_id", "dbo.COLABORADOR_NOTIFICACAO");
            DropForeignKey("dbo.COLABORADOR_DEVICES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.COLABORADOR_DEVICES", "COLABORADORE_id", "dbo.COLABORADOREs");
            DropForeignKey("dbo.POTENCIAL_CLIENTE", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.POTENCIAL_CLIENTE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTOes", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTO_PRESTADORES", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTO_PRESTADORES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PACOTES", "ORCAMENTO_PACOTE_id", "dbo.ORCAMENTO_PACOTE");
            DropForeignKey("dbo.ORCAMENTO_PACOTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "ORCAMENTO_PACOTE2_id", "dbo.ORCAMENTO_PACOTE");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "ITENS_CARDAPIO_id", "dbo.ITENS_CARDAPIO");
            DropForeignKey("dbo.ORCAMENTO_ITENS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.ORCAMENTO_ITENS", "ITENS_CARDAPIO_id", "dbo.ITENS_CARDAPIO");
            DropForeignKey("dbo.ORCAMENTO_ITENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FORNECEDOR_CARDAPIO", "ITENS_CARDAPIO_id", "dbo.ITENS_CARDAPIO");
            DropForeignKey("dbo.FORNECEDOR_CARDAPIO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_CARDAPIO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.SUB_CATEGORIA_ITENS", "UNIDADE_MEDIDA_id", "dbo.UNIDADE_MEDIDA");
            DropForeignKey("dbo.ITENS_DIVERSOS", "UNIDADE_MEDIDA_id", "dbo.UNIDADE_MEDIDA");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "ITENS_DIVERSOS_id", "dbo.ITENS_DIVERSOS");
            DropForeignKey("dbo.EVENTO_DIVERSOS", "ITENS_DIVERSOS_id", "dbo.ITENS_DIVERSOS");
            DropForeignKey("dbo.EVENTO_DIVERSOS", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTO_DIVERSOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_DIVERSOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PRESTADORs", "PRESTADOR_FUNCAO_id", "dbo.PRESTADOR_FUNCAO");
            DropForeignKey("dbo.PRESTADORs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PRESTADOR_SERVICO", "PRESTADOR_FUNCAO_id", "dbo.PRESTADOR_FUNCAO");
            DropForeignKey("dbo.PRESTADOR_FUNCAO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PRESTADOR_SERVICO", "ITENS_SERVICOS_id", "dbo.ITENS_SERVICOS");
            DropForeignKey("dbo.PRESTADOR_SERVICO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "ITENS_SERVICOS_id", "dbo.ITENS_SERVICOS");
            DropForeignKey("dbo.EVENTO_SERVICOS", "ITENS_SERVICOS_id", "dbo.ITENS_SERVICOS");
            DropForeignKey("dbo.EVENTO_SERVICOS", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTO_SERVICOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_SERVICOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_SERVICOS", "CATEGORIA_SERVICOS_id", "dbo.CATEGORIA_SERVICOS");
            DropForeignKey("dbo.ITENS_DIVERSOS", "CATEGORIA_SERVICOS_id", "dbo.CATEGORIA_SERVICOS");
            DropForeignKey("dbo.CATEGORIA_SERVICOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_CARDAPIO", "UNIDADE_MEDIDA_id", "dbo.UNIDADE_MEDIDA");
            DropForeignKey("dbo.INGREDIENTEs", "UNIDADE_MEDIDA_id", "dbo.UNIDADE_MEDIDA");
            DropForeignKey("dbo.ITENS_INGREDIENTES", "ITENS_CARDAPIO_id", "dbo.ITENS_CARDAPIO");
            DropForeignKey("dbo.ITENS_INGREDIENTES", "INGREDIENTE_id", "dbo.INGREDIENTEs");
            DropForeignKey("dbo.ITENS_INGREDIENTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FORNECEDOR_INGREDIENTE", "INGREDIENTE_id", "dbo.INGREDIENTEs");
            DropForeignKey("dbo.FORNECEDOR_INGREDIENTE", "FORNECEDOR_id", "dbo.FORNECEDORs");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_TOPMENU", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_PROMOCAO", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_PROMOCAO", "B_PORTAL_PROMOCAO_id", "dbo.B_PORTAL_PROMOCAO");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_HOME", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_HEADER", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.PORTAL_ANUNCIANTE_DESTAQUE", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.ORCAMENTOes", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.VISITAS_AGENDADAS", "VISITAS_DISPONIVEIS_id", "dbo.VISITAS_DISPONIVEIS");
            DropForeignKey("dbo.VISITAS_DISPONIVEIS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.VISITAS_DISPONIVEIS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_VISITAS", "VISITAS_AGENDADAS_id", "dbo.VISITAS_AGENDADAS");
            DropForeignKey("dbo.VISITAS_AGENDADAS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.VISITAS_AGENDADAS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_VISITAS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.ORCAMENTO_VISITAS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.ORCAMENTO_VISITAS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES", "ORCAMENTO_PROPOSTA_PACOTE_id", "dbo.ORCAMENTO_PROPOSTA_PACOTE");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_PACOTE", "ORCAMENTO_PROPOSTA_id", "dbo.ORCAMENTO_PROPOSTA");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_PACOTE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_DESCRICAO", "ORCAMENTO_PROPOSTA_ABAS_id", "dbo.ORCAMENTO_PROPOSTA_ABAS");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_DESCRICAO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_ABAS", "ORCAMENTO_PROPOSTA_id", "dbo.ORCAMENTO_PROPOSTA");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA_ABAS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.ORCAMENTO_PROPOSTA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "ORCAMENTO_PROPOSTA_id", "dbo.ORCAMENTO_PROPOSTA");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "CLIENTE1_id", "dbo.CLIENTEs");
            DropForeignKey("dbo.CLIENTE_PROPOSTA", "CLIENTE_id", "dbo.CLIENTEs");
            DropForeignKey("dbo.LOCAL_EVENTO_SOCIAL", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.LOCAL_EVENTO_SOCIAL", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.LOCAL_EVENTO_IMAGENS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.LOCAL_EVENTO_IMAGENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PORTAL_VISUALIZACOES", "PORTAL_CLIENTE_PERFIL_id", "dbo.PORTAL_CLIENTE_PERFIL");
            DropForeignKey("dbo.PORTAL_VISUALIZACOES", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.PORTAL_CLIENTE_PESQUISAS", "PORTAL_CLIENTE_PERFIL_id", "dbo.PORTAL_CLIENTE_PERFIL");
            DropForeignKey("dbo.PORTAL_CLIENTE_FAVORITOS", "PORTAL_CLIENTE_PERFIL_id", "dbo.PORTAL_CLIENTE_PERFIL");
            DropForeignKey("dbo.PORTAL_CLIENTE_FAVORITOS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.LOCAL_EVENTO_COMENTARIOS", "PORTAL_CLIENTE_PERFIL_id", "dbo.PORTAL_CLIENTE_PERFIL");
            DropForeignKey("dbo.LOCAL_EVENTO_COMENTARIOS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.LOCAL_EVENTO_COMENTARIOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.IMAGENS_ESPACO_LOCAL_EVENTOS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.IMAGENS_ESPACO_LOCAL_EVENTOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.LOCAL_EVENTO", "ENDERECO_id", "dbo.ENDERECOes");
            DropForeignKey("dbo.EMPRESA_CARRINHO_CUPOM", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.EMPRESA_HISTORICO_PAGAMENTO", "EMPRESA_CARRINHO_CUPOM_id", "dbo.EMPRESA_CARRINHO_CUPOM");
            DropForeignKey("dbo.EMPRESA_HISTORICO_PAGAMENTO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_CARRINHO_CUPOM", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EMPRESA_CARRINHO_CUPOM", "B_PLANOS_DISPONIVEIS_id", "dbo.B_PLANOS_DISPONIVEIS");
            DropForeignKey("dbo.EMPRESA_CARRINHO", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.LOCAL_EVENTO", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.B_PORTAL_DENUNCIAS", "LOCAL_EVENTO_id", "dbo.LOCAL_EVENTO");
            DropForeignKey("dbo.FORNECEDORs", "ENDERECO_id", "dbo.ENDERECOes");
            DropForeignKey("dbo.EMPRESAs", "ENDERECO_id", "dbo.ENDERECOes");
            DropForeignKey("dbo.ENDERECOes", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CLIENTEs", "ENDERECO_id", "dbo.ENDERECOes");
            DropForeignKey("dbo.FORNECEDORs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FORNECEDOR_INGREDIENTE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.INGREDIENTEs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_CARDAPIO", "SUB_CATEGORIA_ITENS_id", "dbo.SUB_CATEGORIA_ITENS");
            DropForeignKey("dbo.SUB_CATEGORIA_ITENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.SUB_CATEGORIA_ITENS", "CATEGORIA_ITENS_id", "dbo.CATEGORIA_ITENS");
            DropForeignKey("dbo.PACOTES_ITENS", "PACOTE_CATEGORIA_ITENS_id", "dbo.PACOTE_CATEGORIA_ITENS");
            DropForeignKey("dbo.PACOTES_ITENS", "ITENS_CARDAPIO_id", "dbo.ITENS_CARDAPIO");
            DropForeignKey("dbo.PACOTES_ITENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PACOTES_ITENS", "CATEGORIA_ITENS_id", "dbo.CATEGORIA_ITENS");
            DropForeignKey("dbo.PACOTE_CATEGORIA_ITENS", "PACOTE_id", "dbo.PACOTEs");
            DropForeignKey("dbo.IMAGENS_PACOTES", "PACOTE_id", "dbo.PACOTEs");
            DropForeignKey("dbo.IMAGENS_PACOTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EVENTOS_PACOTES", "PACOTE_id", "dbo.PACOTEs");
            DropForeignKey("dbo.EVENTOS_PACOTES", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTOS_PACOTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PACOTEs", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PACOTE_CATEGORIA_ITENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PACOTE_CATEGORIA_ITENS", "CATEGORIA_ITENS_id", "dbo.CATEGORIA_ITENS");
            DropForeignKey("dbo.ITENS_CARDAPIO", "CATEGORIA_ITENS_id", "dbo.CATEGORIA_ITENS");
            DropForeignKey("dbo.CATEGORIA_ITENS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "ITENS_ALUGUEL_id", "dbo.ITENS_ALUGUEL");
            DropForeignKey("dbo.ORCAMENTO_PACOTE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.FORNECEDOR_ALUGUEL", "ITENS_ALUGUEL_id", "dbo.ITENS_ALUGUEL");
            DropForeignKey("dbo.FORNECEDOR_ALUGUEL", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EVENTO_ALUGUEL", "ITENS_ALUGUEL_id", "dbo.ITENS_ALUGUEL");
            DropForeignKey("dbo.ITENS_ALUGUEL", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.ITENS_ALUGUEL", "CATEGORIA_ALUGUEL_id", "dbo.CATEGORIA_ALUGUEL");
            DropForeignKey("dbo.CATEGORIA_ALUGUEL", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EVENTO_ALUGUEL", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.EVENTO_ALUGUEL", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.EVENTOes", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CAMPOS_DISPONIVEIS", "EVENTO_id", "dbo.EVENTOes");
            DropForeignKey("dbo.CAMPOS_DISPONIVEIS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CAMPOS_DISPONIVEIS", "CAMPOS_DESCRICAO_id", "dbo.CAMPOS_DESCRICAO");
            DropForeignKey("dbo.CAMPOS_COMPONENTES", "CAMPOS_DESCRICAO_id", "dbo.CAMPOS_DESCRICAO");
            DropForeignKey("dbo.ORCAMENTOes", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_TEMPLATE", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.CONVITE_TEMPLATE", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_CONVIDADOS_FACEBOOK", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.PESQUISA_RESPOSTA", "PESQUISA_PERGUNTA_id", "dbo.PESQUISA_PERGUNTA");
            DropForeignKey("dbo.PESQUISA_PERGUNTA", "PESQUISA_PESQUISAS_id", "dbo.PESQUISA_PESQUISAS");
            DropForeignKey("dbo.PESQUISA_PESQUISAS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.PESQUISA_PESQUISAS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PESQUISA_RESPOSTA_OPCOES", "PESQUISA_PERGUNTA_OPCOES_id", "dbo.PESQUISA_PERGUNTA_OPCOES");
            DropForeignKey("dbo.PESQUISA_RESPOSTA_OPCOES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PESQUISA_RESPOSTA_OPCOES", "CONVITE_CONVIDADOS_id", "dbo.CONVITE_CONVIDADOS");
            DropForeignKey("dbo.PESQUISA_PERGUNTA_OPCOES", "PESQUISA_PERGUNTA_id", "dbo.PESQUISA_PERGUNTA");
            DropForeignKey("dbo.PESQUISA_PERGUNTA_OPCOES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PESQUISA_PERGUNTA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PESQUISA_RESPOSTA", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.PESQUISA_RESPOSTA", "CONVITE_CONVIDADOS_id", "dbo.CONVITE_CONVIDADOS");
            DropForeignKey("dbo.CONVITE_CONVIDADOS", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.CONVITE_CONVIDADOS", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_CONVIDADOS_FAMILIARES", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.CONVITE_CONVIDADOS_FAMILIARES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_CONVIDADOS_FAMILIARES", "CONVITE_CONVIDADOS_id", "dbo.CONVITE_CONVIDADOS");
            DropForeignKey("dbo.CONVITE_ANIVERSARIANTES", "ORCAMENTO_id", "dbo.ORCAMENTOes");
            DropForeignKey("dbo.ORCAMENTOes", "CLIENTE_id", "dbo.CLIENTEs");
            DropForeignKey("dbo.CONVITE_ANIVERSARIANTES", "EMPRESA_id", "dbo.EMPRESAs");
            DropForeignKey("dbo.CONVITE_ANIVERSARIANTES", "CLIENTE_ANIVERSARIANTES_id", "dbo.CLIENTE_ANIVERSARIANTES");
            DropForeignKey("dbo.CLIENTE_ANIVERSARIANTES", "CLIENTE_id", "dbo.CLIENTEs");
            DropForeignKey("dbo.CAMPANHA_CONTATOS_ENVIADOS", "CLIENTE_id", "dbo.CLIENTEs");
            DropForeignKey("dbo.CAMPANHA_LINHA_TEMPO", "CAMPANHA_CONTATOS_ENVIADOS_id", "dbo.CAMPANHA_CONTATOS_ENVIADOS");
            DropForeignKey("dbo.EMPRESA_CARRINHO", "B_PLANOS_DISPONIVEIS_id", "dbo.B_PLANOS_DISPONIVEIS");
            DropForeignKey("dbo.B_PLANOS_DISPONIVEIS", "B_PLANOS_PORTAL_id", "dbo.B_PLANOS_PORTAL");
            DropIndex("dbo.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS", new[] { "B_SERVICO_PARCEIRO_MAPEADO_id" });
            DropIndex("dbo.TIPO_SERVICO", new[] { "EMPRESA_id" });
            DropIndex("dbo.ONDE_CONHECEU", new[] { "EMPRESA_id" });
            DropIndex("dbo.IMAGENS_ITEM", new[] { "EMPRESA_id" });
            DropIndex("dbo.ENDERECOes", new[] { "EMPRESA_id1" });
            DropIndex("dbo.EMPRESAs", new[] { "ENDERECO_id1" });
            DropIndex("dbo.EMPRESA_PAGAMENTO_CARTAO", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_PACOTE_SMS", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_MODULOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_CONFIGURACAO", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_CNAES", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_CARRINHO", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_ARQUIVOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_PERGUNTA_OPCOES", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_RESPOSTAS_OPCOES", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_RESPOSTAS_OPCOES", new[] { "CONVITE_PERGUNTA_OPCOES_id" });
            DropIndex("dbo.CONVITE_RESPOSTAS_OPCOES", new[] { "C_CONVITE_OPCOES_id" });
            DropIndex("dbo.CLIENTE_TIMELINE", new[] { "EMPRESA_id" });
            DropIndex("dbo.CATEGORIA_DIVERSOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.CAMPANHA_LISTA", new[] { "EMPRESA_id" });
            DropIndex("dbo.CAMPANHAs", new[] { "EMPRESA_id" });
            DropIndex("dbo.CAMPANHAs", new[] { "CAMPANHA_LISTA_id" });
            DropIndex("dbo.CAMPANHA_CONTATOS_ENVIADOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.CLIENTEs", new[] { "EMPRESA_id" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "CLIENTE_id2" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "CLIENTE_id1" });
            DropIndex("dbo.CLIENTE_ANIVERSARIANTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.IMAGENS_GALERIA_EVENTOS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.IMAGENS_DOS_EVENTOS", new[] { "IMAGENS_GALERIA_EVENTOS_id" });
            DropIndex("dbo.IMAGENS_DOS_EVENTOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.IMAGENS_GALERIA_EVENTOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_BOLETO", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_BOLETO", new[] { "EMPRESA_id" });
            DropIndex("dbo.PERFIL_COLABORADOR", new[] { "EMPRESA_id" });
            DropIndex("dbo.COLABORADOREs", new[] { "PERFIL_COLABORADOR_id" });
            DropIndex("dbo.ORCAMENTO_ANOTACOES", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.ORCAMENTO_ANOTACOES", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_ANOTACOES", new[] { "COLABORADORE_id" });
            DropIndex("dbo.NOTIFICACOes", new[] { "COLABORADORE_id2" });
            DropIndex("dbo.NOTIFICACOes", new[] { "COLABORADORE_id1" });
            DropIndex("dbo.NOTIFICACOes", new[] { "EMPRESA_id" });
            DropIndex("dbo.NOTIFICACOes", new[] { "COLABORADORE1_id" });
            DropIndex("dbo.NOTIFICACOes", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_ESPECIE", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_ESPECIE", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_ESPECIE", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_DEPOSITO", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_DEPOSITO", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_DEPOSITO", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_DEBITO", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_DEBITO", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_DEBITO", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_CREDITO", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_CREDITO", new[] { "FINANCEIRO_BANDEIRAS_id" });
            DropIndex("dbo.FINANCEIRO_BANDEIRAS", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_CREDITO", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_CREDITO", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_CHEQUE", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.FINANCEIRO_P_CHEQUE", new[] { "EMPRESA_id" });
            DropIndex("dbo.FINANCEIRO_P_CHEQUE", new[] { "COLABORADORE_id" });
            DropIndex("dbo.FINANCEIRO_P_BOLETO", new[] { "COLABORADORE_id" });
            DropIndex("dbo.COLABORADOREs", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONTATO_PLATAFORMA", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONTATO_PLATAFORMA", new[] { "COLABORADORE_id" });
            DropIndex("dbo.COLABORADOREs", new[] { "COLABORADOR_NOTIFICACAO_id" });
            DropIndex("dbo.COLABORADOR_DEVICES", new[] { "EMPRESA_id" });
            DropIndex("dbo.COLABORADOR_DEVICES", new[] { "COLABORADORE_id" });
            DropIndex("dbo.POTENCIAL_CLIENTE", new[] { "EVENTO_id" });
            DropIndex("dbo.POTENCIAL_CLIENTE", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTOes", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTO_PRESTADORES", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTO_PRESTADORES", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PACOTES", new[] { "ORCAMENTO_PACOTE_id" });
            DropIndex("dbo.ORCAMENTO_PACOTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "ORCAMENTO_PACOTE2_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "ITENS_CARDAPIO_id" });
            DropIndex("dbo.ORCAMENTO_ITENS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.ORCAMENTO_ITENS", new[] { "ITENS_CARDAPIO_id" });
            DropIndex("dbo.ORCAMENTO_ITENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.FORNECEDOR_CARDAPIO", new[] { "ITENS_CARDAPIO_id" });
            DropIndex("dbo.FORNECEDOR_CARDAPIO", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_CARDAPIO", new[] { "EMPRESA_id" });
            DropIndex("dbo.SUB_CATEGORIA_ITENS", new[] { "UNIDADE_MEDIDA_id" });
            DropIndex("dbo.ITENS_DIVERSOS", new[] { "UNIDADE_MEDIDA_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "ITENS_DIVERSOS_id" });
            DropIndex("dbo.EVENTO_DIVERSOS", new[] { "ITENS_DIVERSOS_id" });
            DropIndex("dbo.EVENTO_DIVERSOS", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTO_DIVERSOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_DIVERSOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.PRESTADORs", new[] { "PRESTADOR_FUNCAO_id" });
            DropIndex("dbo.PRESTADORs", new[] { "EMPRESA_id" });
            DropIndex("dbo.PRESTADOR_SERVICO", new[] { "PRESTADOR_FUNCAO_id" });
            DropIndex("dbo.PRESTADOR_FUNCAO", new[] { "EMPRESA_id" });
            DropIndex("dbo.PRESTADOR_SERVICO", new[] { "ITENS_SERVICOS_id" });
            DropIndex("dbo.PRESTADOR_SERVICO", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "ITENS_SERVICOS_id" });
            DropIndex("dbo.EVENTO_SERVICOS", new[] { "ITENS_SERVICOS_id" });
            DropIndex("dbo.EVENTO_SERVICOS", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTO_SERVICOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_SERVICOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_SERVICOS", new[] { "CATEGORIA_SERVICOS_id" });
            DropIndex("dbo.ITENS_DIVERSOS", new[] { "CATEGORIA_SERVICOS_id" });
            DropIndex("dbo.CATEGORIA_SERVICOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_CARDAPIO", new[] { "UNIDADE_MEDIDA_id" });
            DropIndex("dbo.INGREDIENTEs", new[] { "UNIDADE_MEDIDA_id" });
            DropIndex("dbo.ITENS_INGREDIENTES", new[] { "ITENS_CARDAPIO_id" });
            DropIndex("dbo.ITENS_INGREDIENTES", new[] { "INGREDIENTE_id" });
            DropIndex("dbo.ITENS_INGREDIENTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.FORNECEDOR_INGREDIENTE", new[] { "INGREDIENTE_id" });
            DropIndex("dbo.FORNECEDOR_INGREDIENTE", new[] { "FORNECEDOR_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_TOPMENU", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_PROMOCAO", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_PROMOCAO", new[] { "B_PORTAL_PROMOCAO_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_HOME", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_HEADER", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.PORTAL_ANUNCIANTE_DESTAQUE", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.ORCAMENTOes", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.VISITAS_AGENDADAS", new[] { "VISITAS_DISPONIVEIS_id" });
            DropIndex("dbo.VISITAS_DISPONIVEIS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.VISITAS_DISPONIVEIS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_VISITAS", new[] { "VISITAS_AGENDADAS_id" });
            DropIndex("dbo.VISITAS_AGENDADAS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.VISITAS_AGENDADAS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_VISITAS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.ORCAMENTO_VISITAS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.ORCAMENTO_VISITAS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES", new[] { "ORCAMENTO_PROPOSTA_PACOTE_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_PACOTE", new[] { "ORCAMENTO_PROPOSTA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_PACOTE", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_DESCRICAO", new[] { "ORCAMENTO_PROPOSTA_ABAS_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_DESCRICAO", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_ABAS", new[] { "ORCAMENTO_PROPOSTA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA_ABAS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.ORCAMENTO_PROPOSTA", new[] { "EMPRESA_id" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "ORCAMENTO_PROPOSTA_id" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "EMPRESA_id" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "CLIENTE1_id" });
            DropIndex("dbo.CLIENTE_PROPOSTA", new[] { "CLIENTE_id" });
            DropIndex("dbo.LOCAL_EVENTO_SOCIAL", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.LOCAL_EVENTO_SOCIAL", new[] { "EMPRESA_id" });
            DropIndex("dbo.LOCAL_EVENTO_IMAGENS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.LOCAL_EVENTO_IMAGENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.PORTAL_VISUALIZACOES", new[] { "PORTAL_CLIENTE_PERFIL_id" });
            DropIndex("dbo.PORTAL_VISUALIZACOES", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.PORTAL_CLIENTE_PESQUISAS", new[] { "PORTAL_CLIENTE_PERFIL_id" });
            DropIndex("dbo.PORTAL_CLIENTE_FAVORITOS", new[] { "PORTAL_CLIENTE_PERFIL_id" });
            DropIndex("dbo.PORTAL_CLIENTE_FAVORITOS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.LOCAL_EVENTO_COMENTARIOS", new[] { "PORTAL_CLIENTE_PERFIL_id" });
            DropIndex("dbo.LOCAL_EVENTO_COMENTARIOS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.LOCAL_EVENTO_COMENTARIOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.IMAGENS_ESPACO_LOCAL_EVENTOS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.IMAGENS_ESPACO_LOCAL_EVENTOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.LOCAL_EVENTO", new[] { "ENDERECO_id" });
            DropIndex("dbo.EMPRESA_CARRINHO_CUPOM", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.EMPRESA_HISTORICO_PAGAMENTO", new[] { "EMPRESA_CARRINHO_CUPOM_id" });
            DropIndex("dbo.EMPRESA_HISTORICO_PAGAMENTO", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_CARRINHO_CUPOM", new[] { "EMPRESA_id" });
            DropIndex("dbo.EMPRESA_CARRINHO_CUPOM", new[] { "B_PLANOS_DISPONIVEIS_id" });
            DropIndex("dbo.EMPRESA_CARRINHO", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.LOCAL_EVENTO", new[] { "EMPRESA_id" });
            DropIndex("dbo.B_PORTAL_DENUNCIAS", new[] { "LOCAL_EVENTO_id" });
            DropIndex("dbo.FORNECEDORs", new[] { "ENDERECO_id" });
            DropIndex("dbo.EMPRESAs", new[] { "ENDERECO_id" });
            DropIndex("dbo.ENDERECOes", new[] { "EMPRESA_id" });
            DropIndex("dbo.CLIENTEs", new[] { "ENDERECO_id" });
            DropIndex("dbo.FORNECEDORs", new[] { "EMPRESA_id" });
            DropIndex("dbo.FORNECEDOR_INGREDIENTE", new[] { "EMPRESA_id" });
            DropIndex("dbo.INGREDIENTEs", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_CARDAPIO", new[] { "SUB_CATEGORIA_ITENS_id" });
            DropIndex("dbo.SUB_CATEGORIA_ITENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.SUB_CATEGORIA_ITENS", new[] { "CATEGORIA_ITENS_id" });
            DropIndex("dbo.PACOTES_ITENS", new[] { "PACOTE_CATEGORIA_ITENS_id" });
            DropIndex("dbo.PACOTES_ITENS", new[] { "ITENS_CARDAPIO_id" });
            DropIndex("dbo.PACOTES_ITENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.PACOTES_ITENS", new[] { "CATEGORIA_ITENS_id" });
            DropIndex("dbo.PACOTE_CATEGORIA_ITENS", new[] { "PACOTE_id" });
            DropIndex("dbo.IMAGENS_PACOTES", new[] { "PACOTE_id" });
            DropIndex("dbo.IMAGENS_PACOTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.EVENTOS_PACOTES", new[] { "PACOTE_id" });
            DropIndex("dbo.EVENTOS_PACOTES", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTOS_PACOTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.PACOTEs", new[] { "EMPRESA_id" });
            DropIndex("dbo.PACOTE_CATEGORIA_ITENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.PACOTE_CATEGORIA_ITENS", new[] { "CATEGORIA_ITENS_id" });
            DropIndex("dbo.ITENS_CARDAPIO", new[] { "CATEGORIA_ITENS_id" });
            DropIndex("dbo.CATEGORIA_ITENS", new[] { "EMPRESA_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "ITENS_ALUGUEL_id" });
            DropIndex("dbo.ORCAMENTO_PACOTE", new[] { "EMPRESA_id" });
            DropIndex("dbo.FORNECEDOR_ALUGUEL", new[] { "ITENS_ALUGUEL_id" });
            DropIndex("dbo.FORNECEDOR_ALUGUEL", new[] { "EMPRESA_id" });
            DropIndex("dbo.EVENTO_ALUGUEL", new[] { "ITENS_ALUGUEL_id" });
            DropIndex("dbo.ITENS_ALUGUEL", new[] { "EMPRESA_id" });
            DropIndex("dbo.ITENS_ALUGUEL", new[] { "CATEGORIA_ALUGUEL_id" });
            DropIndex("dbo.CATEGORIA_ALUGUEL", new[] { "EMPRESA_id" });
            DropIndex("dbo.EVENTO_ALUGUEL", new[] { "EVENTO_id" });
            DropIndex("dbo.EVENTO_ALUGUEL", new[] { "EMPRESA_id" });
            DropIndex("dbo.EVENTOes", new[] { "EMPRESA_id" });
            DropIndex("dbo.CAMPOS_DISPONIVEIS", new[] { "EVENTO_id" });
            DropIndex("dbo.CAMPOS_DISPONIVEIS", new[] { "EMPRESA_id" });
            DropIndex("dbo.CAMPOS_DISPONIVEIS", new[] { "CAMPOS_DESCRICAO_id" });
            DropIndex("dbo.CAMPOS_COMPONENTES", new[] { "CAMPOS_DESCRICAO_id" });
            DropIndex("dbo.ORCAMENTOes", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_TEMPLATE", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.CONVITE_TEMPLATE", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS_FACEBOOK", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA", new[] { "PESQUISA_PERGUNTA_id" });
            DropIndex("dbo.PESQUISA_PERGUNTA", new[] { "PESQUISA_PESQUISAS_id" });
            DropIndex("dbo.PESQUISA_PESQUISAS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.PESQUISA_PESQUISAS", new[] { "EMPRESA_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA_OPCOES", new[] { "PESQUISA_PERGUNTA_OPCOES_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA_OPCOES", new[] { "EMPRESA_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA_OPCOES", new[] { "CONVITE_CONVIDADOS_id" });
            DropIndex("dbo.PESQUISA_PERGUNTA_OPCOES", new[] { "PESQUISA_PERGUNTA_id" });
            DropIndex("dbo.PESQUISA_PERGUNTA_OPCOES", new[] { "EMPRESA_id" });
            DropIndex("dbo.PESQUISA_PERGUNTA", new[] { "EMPRESA_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA", new[] { "EMPRESA_id" });
            DropIndex("dbo.PESQUISA_RESPOSTA", new[] { "CONVITE_CONVIDADOS_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS_FAMILIARES", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS_FAMILIARES", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_CONVIDADOS_FAMILIARES", new[] { "CONVITE_CONVIDADOS_id" });
            DropIndex("dbo.CONVITE_ANIVERSARIANTES", new[] { "ORCAMENTO_id" });
            DropIndex("dbo.ORCAMENTOes", new[] { "CLIENTE_id" });
            DropIndex("dbo.CONVITE_ANIVERSARIANTES", new[] { "EMPRESA_id" });
            DropIndex("dbo.CONVITE_ANIVERSARIANTES", new[] { "CLIENTE_ANIVERSARIANTES_id" });
            DropIndex("dbo.CLIENTE_ANIVERSARIANTES", new[] { "CLIENTE_id" });
            DropIndex("dbo.CAMPANHA_CONTATOS_ENVIADOS", new[] { "CLIENTE_id" });
            DropIndex("dbo.CAMPANHA_LINHA_TEMPO", new[] { "CAMPANHA_CONTATOS_ENVIADOS_id" });
            DropIndex("dbo.EMPRESA_CARRINHO", new[] { "B_PLANOS_DISPONIVEIS_id" });
            DropIndex("dbo.B_PLANOS_DISPONIVEIS", new[] { "B_PLANOS_PORTAL_id" });
            DropTable("dbo.HORARIOs");
            DropTable("dbo.C_FORMA_PAGAMENTO");
            DropTable("dbo.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS");
            DropTable("dbo.B_SERVICO_PARCEIRO_MAPEADO");
            DropTable("dbo.B_SERVICO_MAPEAR_REGIAO");
            DropTable("dbo.B_POSSIVEL_PARCEIRO");
            DropTable("dbo.B_PORTAL_BANNER_PROMOCIONAL");
            DropTable("dbo.TIPO_SERVICO");
            DropTable("dbo.ONDE_CONHECEU");
            DropTable("dbo.IMAGENS_ITEM");
            DropTable("dbo.EMPRESA_PAGAMENTO_CARTAO");
            DropTable("dbo.EMPRESA_PACOTE_SMS");
            DropTable("dbo.EMPRESA_MODULOS");
            DropTable("dbo.EMPRESA_CONFIGURACAO");
            DropTable("dbo.EMPRESA_CNAES");
            DropTable("dbo.EMPRESA_ARQUIVOS");
            DropTable("dbo.C_CONVITE_OPCOES");
            DropTable("dbo.CONVITE_RESPOSTAS_OPCOES");
            DropTable("dbo.CONVITE_PERGUNTA_OPCOES");
            DropTable("dbo.CLIENTE_TIMELINE");
            DropTable("dbo.CATEGORIA_DIVERSOS");
            DropTable("dbo.CAMPANHAs");
            DropTable("dbo.CAMPANHA_LISTA");
            DropTable("dbo.IMAGENS_DOS_EVENTOS");
            DropTable("dbo.IMAGENS_GALERIA_EVENTOS");
            DropTable("dbo.PERFIL_COLABORADOR");
            DropTable("dbo.ORCAMENTO_ANOTACOES");
            DropTable("dbo.NOTIFICACOes");
            DropTable("dbo.FINANCEIRO_P_ESPECIE");
            DropTable("dbo.FINANCEIRO_P_DEPOSITO");
            DropTable("dbo.FINANCEIRO_P_DEBITO");
            DropTable("dbo.FINANCEIRO_BANDEIRAS");
            DropTable("dbo.FINANCEIRO_P_CREDITO");
            DropTable("dbo.FINANCEIRO_P_CHEQUE");
            DropTable("dbo.CONTATO_PLATAFORMA");
            DropTable("dbo.COLABORADOR_NOTIFICACAO");
            DropTable("dbo.COLABORADOR_DEVICES");
            DropTable("dbo.COLABORADOREs");
            DropTable("dbo.FINANCEIRO_P_BOLETO");
            DropTable("dbo.POTENCIAL_CLIENTE");
            DropTable("dbo.EVENTO_PRESTADORES");
            DropTable("dbo.ORCAMENTO_PACOTES");
            DropTable("dbo.ORCAMENTO_ITENS");
            DropTable("dbo.FORNECEDOR_CARDAPIO");
            DropTable("dbo.EVENTO_DIVERSOS");
            DropTable("dbo.PRESTADORs");
            DropTable("dbo.PRESTADOR_FUNCAO");
            DropTable("dbo.PRESTADOR_SERVICO");
            DropTable("dbo.EVENTO_SERVICOS");
            DropTable("dbo.ITENS_SERVICOS");
            DropTable("dbo.CATEGORIA_SERVICOS");
            DropTable("dbo.ITENS_DIVERSOS");
            DropTable("dbo.ITENS_INGREDIENTES");
            DropTable("dbo.PORTAL_ANUNCIANTE_TOPMENU");
            DropTable("dbo.B_PORTAL_PROMOCAO");
            DropTable("dbo.PORTAL_ANUNCIANTE_PROMOCAO");
            DropTable("dbo.PORTAL_ANUNCIANTE_HOME");
            DropTable("dbo.PORTAL_ANUNCIANTE_HEADER");
            DropTable("dbo.PORTAL_ANUNCIANTE_DESTAQUE");
            DropTable("dbo.VISITAS_DISPONIVEIS");
            DropTable("dbo.VISITAS_AGENDADAS");
            DropTable("dbo.ORCAMENTO_VISITAS");
            DropTable("dbo.ORCAMENTO_PROPOSTA_PACOTE_VALORES");
            DropTable("dbo.ORCAMENTO_PROPOSTA_PACOTE");
            DropTable("dbo.ORCAMENTO_PROPOSTA_DESCRICAO");
            DropTable("dbo.ORCAMENTO_PROPOSTA_ABAS");
            DropTable("dbo.CLIENTE_PROPOSTA");
            DropTable("dbo.ORCAMENTO_PROPOSTA");
            DropTable("dbo.LOCAL_EVENTO_SOCIAL");
            DropTable("dbo.LOCAL_EVENTO_IMAGENS");
            DropTable("dbo.PORTAL_VISUALIZACOES");
            DropTable("dbo.PORTAL_CLIENTE_PESQUISAS");
            DropTable("dbo.PORTAL_CLIENTE_FAVORITOS");
            DropTable("dbo.PORTAL_CLIENTE_PERFIL");
            DropTable("dbo.LOCAL_EVENTO_COMENTARIOS");
            DropTable("dbo.IMAGENS_ESPACO_LOCAL_EVENTOS");
            DropTable("dbo.EMPRESA_HISTORICO_PAGAMENTO");
            DropTable("dbo.EMPRESA_CARRINHO_CUPOM");
            DropTable("dbo.B_PORTAL_DENUNCIAS");
            DropTable("dbo.LOCAL_EVENTO");
            DropTable("dbo.ENDERECOes");
            DropTable("dbo.FORNECEDORs");
            DropTable("dbo.FORNECEDOR_INGREDIENTE");
            DropTable("dbo.INGREDIENTEs");
            DropTable("dbo.UNIDADE_MEDIDA");
            DropTable("dbo.SUB_CATEGORIA_ITENS");
            DropTable("dbo.PACOTES_ITENS");
            DropTable("dbo.IMAGENS_PACOTES");
            DropTable("dbo.EVENTOS_PACOTES");
            DropTable("dbo.PACOTEs");
            DropTable("dbo.PACOTE_CATEGORIA_ITENS");
            DropTable("dbo.CATEGORIA_ITENS");
            DropTable("dbo.ITENS_CARDAPIO");
            DropTable("dbo.ORCAMENTO_PACOTE");
            DropTable("dbo.FORNECEDOR_ALUGUEL");
            DropTable("dbo.CATEGORIA_ALUGUEL");
            DropTable("dbo.ITENS_ALUGUEL");
            DropTable("dbo.EVENTO_ALUGUEL");
            DropTable("dbo.CAMPOS_COMPONENTES");
            DropTable("dbo.CAMPOS_DESCRICAO");
            DropTable("dbo.CAMPOS_DISPONIVEIS");
            DropTable("dbo.EVENTOes");
            DropTable("dbo.CONVITE_TEMPLATE");
            DropTable("dbo.CONVITE_CONVIDADOS_FACEBOOK");
            DropTable("dbo.PESQUISA_PESQUISAS");
            DropTable("dbo.PESQUISA_RESPOSTA_OPCOES");
            DropTable("dbo.PESQUISA_PERGUNTA_OPCOES");
            DropTable("dbo.PESQUISA_PERGUNTA");
            DropTable("dbo.PESQUISA_RESPOSTA");
            DropTable("dbo.CONVITE_CONVIDADOS_FAMILIARES");
            DropTable("dbo.CONVITE_CONVIDADOS");
            DropTable("dbo.ORCAMENTOes");
            DropTable("dbo.CONVITE_ANIVERSARIANTES");
            DropTable("dbo.CLIENTE_ANIVERSARIANTES");
            DropTable("dbo.CLIENTEs");
            DropTable("dbo.CAMPANHA_LINHA_TEMPO");
            DropTable("dbo.CAMPANHA_CONTATOS_ENVIADOS");
            DropTable("dbo.EMPRESAs");
            DropTable("dbo.EMPRESA_CARRINHO");
            DropTable("dbo.B_PLANOS_PORTAL");
            DropTable("dbo.B_PLANOS_DISPONIVEIS");
            DropTable("dbo.B_MODULOS_DISPONIVEIS");
            DropTable("dbo.B_EMPRESA_USUARIOS");
            DropTable("dbo.B_EMPRESA_AJUSTES");
            DropTable("dbo.B_CONVITES_FRASES");
            DropTable("dbo.B_CONVITES_FONTES");
            DropTable("dbo.B_CONVITES_DISPONIVEIS");
            DropTable("dbo.ADMINISTRADORs");
        }
    }
}
