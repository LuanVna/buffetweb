using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_CONVITES_DISPONIVEIS
    {
        public int id { get; set; }
        public string tema { get; set; }
        public string url_image { get; set; }
        public bool disponivel { get; set; }
        public string titulo { get; set; }
        public string titulo_cor { get; set; }
        public string titulo_fonte { get; set; }
        public string titulo_tamanho { get; set; }
        public string titulo_top { get; set; }
        public string titulo_left { get; set; }
        public string datahora_cor { get; set; }
        public string datahora_fonte { get; set; }
        public string datahora_tamanho { get; set; }
        public string datahora_top { get; set; }
        public string datahora_left { get; set; }
        public string descricao { get; set; }
        public string descricao_cor { get; set; }
        public string descricao_fonte { get; set; }
        public string descricao_tamanho { get; set; }
        public string descricao_top { get; set; }
        public string descricao_left { get; set; }
        public string aniversariantes_cor { get; set; }
        public string aniversariantes_fonte { get; set; }
        public string aniversariantes_tamanho { get; set; }
        public string aniversariantes_top { get; set; }
        public string aniversariantes_left { get; set; }
        public string tipo_evento { get; set; }
        public string aniversariantes_titulo { get; set; }
        public string datahora_titulo { get; set; }
    }
}
