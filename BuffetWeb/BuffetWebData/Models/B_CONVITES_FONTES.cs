using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_CONVITES_FONTES
    {
        public int id { get; set; }
        public string nome_fonte { get; set; }
        public string nome_visivel { get; set; }
        public string caminho_fonte { get; set; }
        public string css { get; set; }
    }
}
