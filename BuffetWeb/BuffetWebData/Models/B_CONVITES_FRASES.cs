using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_CONVITES_FRASES
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
    }
}
