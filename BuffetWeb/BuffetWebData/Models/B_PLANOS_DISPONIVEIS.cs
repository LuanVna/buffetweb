using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_PLANOS_DISPONIVEIS
    {
        public B_PLANOS_DISPONIVEIS()
        {
            this.EMPRESA_CARRINHO = new List<EMPRESA_CARRINHO>();
            this.EMPRESA_CARRINHO_CUPOM = new List<EMPRESA_CARRINHO_CUPOM>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public string img_miniatura { get; set; }
        public string img_normal { get; set; }
        public decimal valor_mensal { get; set; }
        public decimal valor_trimestral { get; set; }
        public decimal valor_semestral { get; set; }
        public decimal valor_anual { get; set; }
        public System.DateTime disponivel_de { get; set; }
        public Nullable<System.DateTime> disponivel_ate { get; set; }
        public Nullable<int> id_plano_portal { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO> EMPRESA_CARRINHO { get; set; }
        public virtual B_PLANOS_PORTAL B_PLANOS_PORTAL { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO_CUPOM> EMPRESA_CARRINHO_CUPOM { get; set; }
    }
}
