using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_PLANOS_PORTAL
    {
        public B_PLANOS_PORTAL()
        {
            this.B_PLANOS_DISPONIVEIS = new List<B_PLANOS_DISPONIVEIS>();
        }

        public int id { get; set; }
        public bool destaque_pesquisa_zona { get; set; }
        public bool destaque_mapa { get; set; }
        public bool envio_proposta { get; set; }
        public int envio_proposta_quantidade_mes { get; set; }
        public string nome { get; set; }
        public string anuncio { get; set; }
        public virtual ICollection<B_PLANOS_DISPONIVEIS> B_PLANOS_DISPONIVEIS { get; set; }
    }
}
