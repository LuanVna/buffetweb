using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class B_SERVICO_MAPEAR_REGIAO
    {
        public int id { get; set; }
        public string url { get; set; }
        public int quantidade_mapeada { get; set; }
        public Nullable<System.DateTime> ultima_atualizacao { get; set; }
        public string concorrente { get; set; }
    }
}
