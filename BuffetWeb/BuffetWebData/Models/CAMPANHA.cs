using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CAMPANHA
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string assunto { get; set; }
        public System.DateTime data_inclusao { get; set; }
        public int id_lista { get; set; }
        public int id_empresa { get; set; }
        public virtual CAMPANHA_LISTA CAMPANHA_LISTA { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
