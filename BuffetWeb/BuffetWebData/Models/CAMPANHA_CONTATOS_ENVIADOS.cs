using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CAMPANHA_CONTATOS_ENVIADOS
    {
        public CAMPANHA_CONTATOS_ENVIADOS()
        {
            this.CAMPANHA_LINHA_TEMPO = new List<CAMPANHA_LINHA_TEMPO>();
        }

        public int id { get; set; }
        public string titulo { get; set; }
        public string mensagem { get; set; }
        public string autenticacao { get; set; }
        public Nullable<int> id_cliente { get; set; }
        public System.DateTime data_envio { get; set; }
        public Nullable<System.DateTime> data_lido { get; set; }
        public int id_empresa { get; set; }
        public bool lido_email { get; set; }
        public bool lido_sms { get; set; }
        public string endereco_ip { get; set; }
        public string hostname { get; set; }
        public string estado { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string provedor { get; set; }
        public string cidade { get; set; }
        public string navegador_nome { get; set; }
        public string navegador_versao { get; set; }
        public string sistema_operacional { get; set; }
        public string lido_com_device { get; set; }
        public bool enviado_email { get; set; }
        public bool enviado_sms { get; set; }
        public string tipo_contato { get; set; }
        public int quantidade_cliques { get; set; }
        public Nullable<bool> enviado_facebook { get; set; }
        public Nullable<bool> lido_facebook { get; set; }
        public virtual ICollection<CAMPANHA_LINHA_TEMPO> CAMPANHA_LINHA_TEMPO { get; set; }
        public virtual CLIENTE CLIENTE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
