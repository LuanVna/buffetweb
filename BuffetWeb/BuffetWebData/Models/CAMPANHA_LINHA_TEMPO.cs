using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CAMPANHA_LINHA_TEMPO
    {
        public string acontecimento { get; set; }
        public System.DateTime data { get; set; }
        public int id_campanha_contatos_enviados { get; set; }
        public int id { get; set; }
        public virtual CAMPANHA_CONTATOS_ENVIADOS CAMPANHA_CONTATOS_ENVIADOS { get; set; }
    }
}
