using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CAMPOS_DISPONIVEIS
    {
        public int id { get; set; }
        public int id_tipo_evento { get; set; }
        public int id_campos_descricao { get; set; }
        public int id_empresa { get; set; }
        public virtual CAMPOS_DESCRICAO CAMPOS_DESCRICAO { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EVENTO EVENTO { get; set; }
    }
}
