using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CATEGORIA_DIVERSOS
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int id_empresa { get; set; }
        public string url_image { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
