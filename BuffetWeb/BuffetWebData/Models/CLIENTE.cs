using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CLIENTE
    {
        public CLIENTE()
        {
            this.CAMPANHA_CONTATOS_ENVIADOS = new List<CAMPANHA_CONTATOS_ENVIADOS>();
            this.CLIENTE_PROPOSTA = new List<CLIENTE_PROPOSTA>();
            this.CLIENTE_ANIVERSARIANTES = new List<CLIENTE_ANIVERSARIANTES>();
            this.CLIENTE_PROPOSTA1 = new List<CLIENTE_PROPOSTA>();
            this.ORCAMENTOes = new List<ORCAMENTO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public Nullable<System.DateTime> desde { get; set; }
        public string email { get; set; }
        public string cpf { get; set; }
        public string rg { get; set; }
        public Nullable<int> id_endereco { get; set; }
        public string cliente_de { get; set; }
        public int id_empresa { get; set; }
        public string operadora_celular { get; set; }
        public string onde_conheceu { get; set; }
        public string autenticacao { get; set; }
        public bool email_marketing { get; set; }
        public string regiao { get; set; }
        public virtual ICollection<CAMPANHA_CONTATOS_ENVIADOS> CAMPANHA_CONTATOS_ENVIADOS { get; set; }
        public virtual ICollection<CLIENTE_PROPOSTA> CLIENTE_PROPOSTA { get; set; }
        public virtual ENDERECO ENDERECO { get; set; }
        public virtual ICollection<CLIENTE_ANIVERSARIANTES> CLIENTE_ANIVERSARIANTES { get; set; }
        public virtual ICollection<CLIENTE_PROPOSTA> CLIENTE_PROPOSTA1 { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<ORCAMENTO> ORCAMENTOes { get; set; }
    }
}
