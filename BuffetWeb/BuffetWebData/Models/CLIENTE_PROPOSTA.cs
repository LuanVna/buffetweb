using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CLIENTE_PROPOSTA
    {
        public int id { get; set; }
        public Nullable<int> id_cliente { get; set; }
        public System.DateTime data_envio { get; set; }
        public string proposta_de { get; set; }
        public int id_empresa { get; set; }
        public Nullable<int> convidados { get; set; }
        public Nullable<System.DateTime> data_evento { get; set; }
        public string periodo { get; set; }
        public int id_proposta { get; set; }
        public string autenticacao { get; set; }
        public bool disponivel { get; set; }
        public System.DateTime disponivel_ate { get; set; }
        public virtual CLIENTE CLIENTE { get; set; }
        public virtual CLIENTE CLIENTE1 { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PROPOSTA ORCAMENTO_PROPOSTA { get; set; }
    }
}
