using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CLIENTE_TIMELINE
    {
        public int id { get; set; }
        public string email { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string icone { get; set; }
        public System.DateTime data { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
