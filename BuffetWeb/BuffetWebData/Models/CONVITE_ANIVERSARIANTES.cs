using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CONVITE_ANIVERSARIANTES
    {
        public int id { get; set; }
        public int id_aniversariante { get; set; }
        public int id_orcamento { get; set; }
        public int id_empresa { get; set; }
        public virtual CLIENTE_ANIVERSARIANTES CLIENTE_ANIVERSARIANTES { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
