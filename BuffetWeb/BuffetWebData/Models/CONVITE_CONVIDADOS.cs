using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CONVITE_CONVIDADOS
    {
        public CONVITE_CONVIDADOS()
        {
            this.CONVITE_CONVIDADOS_FAMILIARES = new List<CONVITE_CONVIDADOS_FAMILIARES>();
            this.PESQUISA_RESPOSTA = new List<PESQUISA_RESPOSTA>();
            this.PESQUISA_RESPOSTA_OPCOES = new List<PESQUISA_RESPOSTA_OPCOES>();
        }

        public int id { get; set; }
        public int id_orcamento { get; set; }
        public string nome_completo { get; set; }
        public string email { get; set; }
        public string ira { get; set; }
        public string mensagem { get; set; }
        public string autenticacao { get; set; }
        public int id_empresa { get; set; }
        public string telefone { get; set; }
        public Nullable<bool> compareceu { get; set; }
        public string operadora { get; set; }
        public Nullable<bool> sms_enviado { get; set; }
        public Nullable<bool> email_enviado { get; set; }
        public Nullable<int> convidados { get; set; }
        public Nullable<bool> facebook { get; set; }
        public string resposta { get; set; }
        public string id_facebook { get; set; }
        public Nullable<bool> facebook_enviado { get; set; }
        public Nullable<bool> enviado { get; set; }
        public string nome_facebook { get; set; }
        public string status_pequisa { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS_FAMILIARES> CONVITE_CONVIDADOS_FAMILIARES { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA> PESQUISA_RESPOSTA { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA_OPCOES> PESQUISA_RESPOSTA_OPCOES { get; set; }
    }
}
