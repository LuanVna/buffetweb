using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class CONVITE_RESPOSTAS_OPCOES
    {
        public int id { get; set; }
        public int id_opcao { get; set; }
        public int id_pergunta_opcao { get; set; }
        public int id_empresa { get; set; }
        public virtual C_CONVITE_OPCOES C_CONVITE_OPCOES { get; set; }
        public virtual CONVITE_PERGUNTA_OPCOES CONVITE_PERGUNTA_OPCOES { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
