using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA
    {
        public EMPRESA()
        {
            this.CAMPANHAs = new List<CAMPANHA>();
            this.CAMPANHA_CONTATOS_ENVIADOS = new List<CAMPANHA_CONTATOS_ENVIADOS>();
            this.CAMPANHA_LISTA = new List<CAMPANHA_LISTA>();
            this.CAMPOS_DISPONIVEIS = new List<CAMPOS_DISPONIVEIS>();
            this.CATEGORIA_ALUGUEL = new List<CATEGORIA_ALUGUEL>();
            this.CATEGORIA_DIVERSOS = new List<CATEGORIA_DIVERSOS>();
            this.CATEGORIA_ITENS = new List<CATEGORIA_ITENS>();
            this.CATEGORIA_SERVICOS = new List<CATEGORIA_SERVICOS>();
            this.CLIENTEs = new List<CLIENTE>();
            this.CLIENTE_ANIVERSARIANTES = new List<CLIENTE_ANIVERSARIANTES>();
            this.CLIENTE_PROPOSTA = new List<CLIENTE_PROPOSTA>();
            this.CLIENTE_TIMELINE = new List<CLIENTE_TIMELINE>();
            this.COLABORADOR_DEVICES = new List<COLABORADOR_DEVICES>();
            this.COLABORADORES = new List<COLABORADORE>();
            this.CONTATO_PLATAFORMA = new List<CONTATO_PLATAFORMA>();
            this.CONVITE_ANIVERSARIANTES = new List<CONVITE_ANIVERSARIANTES>();
            this.CONVITE_CONVIDADOS = new List<CONVITE_CONVIDADOS>();
            this.CONVITE_CONVIDADOS_FAMILIARES = new List<CONVITE_CONVIDADOS_FAMILIARES>();
            this.CONVITE_PERGUNTA_OPCOES = new List<CONVITE_PERGUNTA_OPCOES>();
            this.CONVITE_RESPOSTAS_OPCOES = new List<CONVITE_RESPOSTAS_OPCOES>();
            this.CONVITE_TEMPLATE = new List<CONVITE_TEMPLATE>();
            this.LOCAL_EVENTO_SOCIAL = new List<LOCAL_EVENTO_SOCIAL>();
            this.FINANCEIRO_BANDEIRAS = new List<FINANCEIRO_BANDEIRAS>();
            this.FINANCEIRO_P_ESPECIE = new List<FINANCEIRO_P_ESPECIE>();
            this.FINANCEIRO_P_BOLETO = new List<FINANCEIRO_P_BOLETO>();
            this.FINANCEIRO_P_CHEQUE = new List<FINANCEIRO_P_CHEQUE>();
            this.FINANCEIRO_P_CREDITO = new List<FINANCEIRO_P_CREDITO>();
            this.FINANCEIRO_P_DEBITO = new List<FINANCEIRO_P_DEBITO>();
            this.FINANCEIRO_P_DEPOSITO = new List<FINANCEIRO_P_DEPOSITO>();
            this.ORCAMENTO_ANOTACOES = new List<ORCAMENTO_ANOTACOES>();
            this.EMPRESA_CARRINHO_CUPOM = new List<EMPRESA_CARRINHO_CUPOM>();
            this.EMPRESA_CARRINHO = new List<EMPRESA_CARRINHO>();
            this.EMPRESA_ARQUIVOS = new List<EMPRESA_ARQUIVOS>();
            this.EMPRESA_CNAES = new List<EMPRESA_CNAES>();
            this.EMPRESA_CONFIGURACAO = new List<EMPRESA_CONFIGURACAO>();
            this.EMPRESA_HISTORICO_PAGAMENTO = new List<EMPRESA_HISTORICO_PAGAMENTO>();
            this.EMPRESA_MODULOS = new List<EMPRESA_MODULOS>();
            this.EMPRESA_PACOTE_SMS = new List<EMPRESA_PACOTE_SMS>();
            this.EMPRESA_PAGAMENTO_CARTAO = new List<EMPRESA_PAGAMENTO_CARTAO>();
            this.VISITAS_DISPONIVEIS = new List<VISITAS_DISPONIVEIS>();
            this.ENDERECOes = new List<ENDERECO>();
            this.EVENTO_PRESTADORES = new List<EVENTO_PRESTADORES>();
            this.FORNECEDORs = new List<FORNECEDOR>();
            this.FORNECEDOR_ALUGUEL = new List<FORNECEDOR_ALUGUEL>();
            this.FORNECEDOR_CARDAPIO = new List<FORNECEDOR_CARDAPIO>();
            this.FORNECEDOR_INGREDIENTE = new List<FORNECEDOR_INGREDIENTE>();
            this.VISITAS_AGENDADAS = new List<VISITAS_AGENDADAS>();
            this.IMAGENS_GALERIA_EVENTOS = new List<IMAGENS_GALERIA_EVENTOS>();
            this.IMAGENS_ITEM = new List<IMAGENS_ITEM>();
            this.IMAGENS_PACOTES = new List<IMAGENS_PACOTES>();
            this.IMAGENS_ESPACO_LOCAL_EVENTOS = new List<IMAGENS_ESPACO_LOCAL_EVENTOS>();
            this.INGREDIENTES = new List<INGREDIENTE>();
            this.ITENS_ALUGUEL = new List<ITENS_ALUGUEL>();
            this.ITENS_CARDAPIO = new List<ITENS_CARDAPIO>();
            this.ITENS_DIVERSOS = new List<ITENS_DIVERSOS>();
            this.ITENS_INGREDIENTES = new List<ITENS_INGREDIENTES>();
            this.ITENS_SERVICOS = new List<ITENS_SERVICOS>();
            this.LOCAL_EVENTO = new List<LOCAL_EVENTO>();
            this.LOCAL_EVENTO_IMAGENS = new List<LOCAL_EVENTO_IMAGENS>();
            this.IMAGENS_DOS_EVENTOS = new List<IMAGENS_DOS_EVENTOS>();
            this.NOTIFICACOES = new List<NOTIFICACO>();
            this.ONDE_CONHECEU = new List<ONDE_CONHECEU>();
            this.ORCAMENTOes = new List<ORCAMENTO>();
            this.ORCAMENTO_ITENS = new List<ORCAMENTO_ITENS>();
            this.ORCAMENTO_PACOTE = new List<ORCAMENTO_PACOTE>();
            this.ORCAMENTO_PACOTES = new List<ORCAMENTO_PACOTES>();
            this.ORCAMENTO_PROPOSTA_PACOTE = new List<ORCAMENTO_PROPOSTA_PACOTE>();
            this.ORCAMENTO_PROPOSTA_PACOTE_VALORES = new List<ORCAMENTO_PROPOSTA_PACOTE_VALORES>();
            this.ORCAMENTO_VISITAS = new List<ORCAMENTO_VISITAS>();
            this.PACOTES = new List<PACOTE>();
            this.PACOTE_CATEGORIA_ITENS = new List<PACOTE_CATEGORIA_ITENS>();
            this.EVENTOS_PACOTES = new List<EVENTOS_PACOTES>();
            this.EVENTO_ALUGUEL = new List<EVENTO_ALUGUEL>();
            this.EVENTO_DIVERSOS = new List<EVENTO_DIVERSOS>();
            this.PACOTES_ITENS = new List<PACOTES_ITENS>();
            this.EVENTO_SERVICOS = new List<EVENTO_SERVICOS>();
            this.PERFIL_COLABORADOR = new List<PERFIL_COLABORADOR>();
            this.PESQUISA_PERGUNTA_OPCOES = new List<PESQUISA_PERGUNTA_OPCOES>();
            this.PESQUISA_PERGUNTA = new List<PESQUISA_PERGUNTA>();
            this.PESQUISA_PESQUISAS = new List<PESQUISA_PESQUISAS>();
            this.PESQUISA_RESPOSTA = new List<PESQUISA_RESPOSTA>();
            this.PESQUISA_RESPOSTA_OPCOES = new List<PESQUISA_RESPOSTA_OPCOES>();
            this.POTENCIAL_CLIENTE = new List<POTENCIAL_CLIENTE>();
            this.PRESTADOR_FUNCAO = new List<PRESTADOR_FUNCAO>();
            this.PRESTADORs = new List<PRESTADOR>();
            this.PRESTADOR_SERVICO = new List<PRESTADOR_SERVICO>();
            this.SUB_CATEGORIA_ITENS = new List<SUB_CATEGORIA_ITENS>();
            this.EVENTOes = new List<EVENTO>();
            this.TIPO_SERVICO = new List<TIPO_SERVICO>();
            this.LOCAL_EVENTO_COMENTARIOS = new List<LOCAL_EVENTO_COMENTARIOS>();
            this.ORCAMENTO_PROPOSTA = new List<ORCAMENTO_PROPOSTA>();
            this.ORCAMENTO_PROPOSTA_ABAS = new List<ORCAMENTO_PROPOSTA_ABAS>();
            this.ORCAMENTO_PROPOSTA_DESCRICAO = new List<ORCAMENTO_PROPOSTA_DESCRICAO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string codigo_empresa { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string cnpj { get; set; }
        public string razao_social { get; set; }
        public Nullable<System.DateTime> data_da_situacao { get; set; }
        public Nullable<System.DateTime> data_da_situacao_especial { get; set; }
        public Nullable<int> id_endereco { get; set; }
        public string codigo_atividade_economica { get; set; }
        public string codigo_atividade_economica_descricao { get; set; }
        public string codigo_natureza_juridica { get; set; }
        public string codigo_natureza_juridica_descricao { get; set; }
        public Nullable<System.DateTime> data_consultaRFB { get; set; }
        public Nullable<System.DateTime> data_fundacao { get; set; }
        public Nullable<System.DateTime> data_motivo_especial_situacaoRFB { get; set; }
        public Nullable<System.DateTime> data_situacao_RFB { get; set; }
        public string documento { get; set; }
        public string motivo_especial_situacaoRFB { get; set; }
        public string situacao_RFB { get; set; }
        public string status { get; set; }
        public bool bloqueio_cnae { get; set; }
        public string motivo_situacaoRFB { get; set; }
        public string contato { get; set; }
        public bool aguardando { get; set; }
        public virtual ICollection<CAMPANHA> CAMPANHAs { get; set; }
        public virtual ICollection<CAMPANHA_CONTATOS_ENVIADOS> CAMPANHA_CONTATOS_ENVIADOS { get; set; }
        public virtual ICollection<CAMPANHA_LISTA> CAMPANHA_LISTA { get; set; }
        public virtual ICollection<CAMPOS_DISPONIVEIS> CAMPOS_DISPONIVEIS { get; set; }
        public virtual ICollection<CATEGORIA_ALUGUEL> CATEGORIA_ALUGUEL { get; set; }
        public virtual ICollection<CATEGORIA_DIVERSOS> CATEGORIA_DIVERSOS { get; set; }
        public virtual ICollection<CATEGORIA_ITENS> CATEGORIA_ITENS { get; set; }
        public virtual ICollection<CATEGORIA_SERVICOS> CATEGORIA_SERVICOS { get; set; }
        public virtual ICollection<CLIENTE> CLIENTEs { get; set; }
        public virtual ICollection<CLIENTE_ANIVERSARIANTES> CLIENTE_ANIVERSARIANTES { get; set; }
        public virtual ICollection<CLIENTE_PROPOSTA> CLIENTE_PROPOSTA { get; set; }
        public virtual ICollection<CLIENTE_TIMELINE> CLIENTE_TIMELINE { get; set; }
        public virtual ICollection<COLABORADOR_DEVICES> COLABORADOR_DEVICES { get; set; }
        public virtual ICollection<COLABORADORE> COLABORADORES { get; set; }
        public virtual ICollection<CONTATO_PLATAFORMA> CONTATO_PLATAFORMA { get; set; }
        public virtual ICollection<CONVITE_ANIVERSARIANTES> CONVITE_ANIVERSARIANTES { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS> CONVITE_CONVIDADOS { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS_FAMILIARES> CONVITE_CONVIDADOS_FAMILIARES { get; set; }
        public virtual ICollection<CONVITE_PERGUNTA_OPCOES> CONVITE_PERGUNTA_OPCOES { get; set; }
        public virtual ICollection<CONVITE_RESPOSTAS_OPCOES> CONVITE_RESPOSTAS_OPCOES { get; set; }
        public virtual ICollection<CONVITE_TEMPLATE> CONVITE_TEMPLATE { get; set; }
        public virtual ICollection<LOCAL_EVENTO_SOCIAL> LOCAL_EVENTO_SOCIAL { get; set; }
        public virtual ICollection<FINANCEIRO_BANDEIRAS> FINANCEIRO_BANDEIRAS { get; set; }
        public virtual ICollection<FINANCEIRO_P_ESPECIE> FINANCEIRO_P_ESPECIE { get; set; }
        public virtual ICollection<FINANCEIRO_P_BOLETO> FINANCEIRO_P_BOLETO { get; set; }
        public virtual ICollection<FINANCEIRO_P_CHEQUE> FINANCEIRO_P_CHEQUE { get; set; }
        public virtual ICollection<FINANCEIRO_P_CREDITO> FINANCEIRO_P_CREDITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEBITO> FINANCEIRO_P_DEBITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEPOSITO> FINANCEIRO_P_DEPOSITO { get; set; }
        public virtual ICollection<ORCAMENTO_ANOTACOES> ORCAMENTO_ANOTACOES { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO_CUPOM> EMPRESA_CARRINHO_CUPOM { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO> EMPRESA_CARRINHO { get; set; }
        public virtual ENDERECO ENDERECO { get; set; }
        public virtual ICollection<EMPRESA_ARQUIVOS> EMPRESA_ARQUIVOS { get; set; }
        public virtual ICollection<EMPRESA_CNAES> EMPRESA_CNAES { get; set; }
        public virtual ICollection<EMPRESA_CONFIGURACAO> EMPRESA_CONFIGURACAO { get; set; }
        public virtual ICollection<EMPRESA_HISTORICO_PAGAMENTO> EMPRESA_HISTORICO_PAGAMENTO { get; set; }
        public virtual ICollection<EMPRESA_MODULOS> EMPRESA_MODULOS { get; set; }
        public virtual ICollection<EMPRESA_PACOTE_SMS> EMPRESA_PACOTE_SMS { get; set; }
        public virtual ICollection<EMPRESA_PAGAMENTO_CARTAO> EMPRESA_PAGAMENTO_CARTAO { get; set; }
        public virtual ICollection<VISITAS_DISPONIVEIS> VISITAS_DISPONIVEIS { get; set; }
        public virtual ICollection<ENDERECO> ENDERECOes { get; set; }
        public virtual ICollection<EVENTO_PRESTADORES> EVENTO_PRESTADORES { get; set; }
        public virtual ICollection<FORNECEDOR> FORNECEDORs { get; set; }
        public virtual ICollection<FORNECEDOR_ALUGUEL> FORNECEDOR_ALUGUEL { get; set; }
        public virtual ICollection<FORNECEDOR_CARDAPIO> FORNECEDOR_CARDAPIO { get; set; }
        public virtual ICollection<FORNECEDOR_INGREDIENTE> FORNECEDOR_INGREDIENTE { get; set; }
        public virtual ICollection<VISITAS_AGENDADAS> VISITAS_AGENDADAS { get; set; }
        public virtual ICollection<IMAGENS_GALERIA_EVENTOS> IMAGENS_GALERIA_EVENTOS { get; set; }
        public virtual ICollection<IMAGENS_ITEM> IMAGENS_ITEM { get; set; }
        public virtual ICollection<IMAGENS_PACOTES> IMAGENS_PACOTES { get; set; }
        public virtual ICollection<IMAGENS_ESPACO_LOCAL_EVENTOS> IMAGENS_ESPACO_LOCAL_EVENTOS { get; set; }
        public virtual ICollection<INGREDIENTE> INGREDIENTES { get; set; }
        public virtual ICollection<ITENS_ALUGUEL> ITENS_ALUGUEL { get; set; }
        public virtual ICollection<ITENS_CARDAPIO> ITENS_CARDAPIO { get; set; }
        public virtual ICollection<ITENS_DIVERSOS> ITENS_DIVERSOS { get; set; }
        public virtual ICollection<ITENS_INGREDIENTES> ITENS_INGREDIENTES { get; set; }
        public virtual ICollection<ITENS_SERVICOS> ITENS_SERVICOS { get; set; }
        public virtual ICollection<LOCAL_EVENTO> LOCAL_EVENTO { get; set; }
        public virtual ICollection<LOCAL_EVENTO_IMAGENS> LOCAL_EVENTO_IMAGENS { get; set; }
        public virtual ICollection<IMAGENS_DOS_EVENTOS> IMAGENS_DOS_EVENTOS { get; set; }
        public virtual ICollection<NOTIFICACO> NOTIFICACOES { get; set; }
        public virtual ICollection<ONDE_CONHECEU> ONDE_CONHECEU { get; set; }
        public virtual ICollection<ORCAMENTO> ORCAMENTOes { get; set; }
        public virtual ICollection<ORCAMENTO_ITENS> ORCAMENTO_ITENS { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTES> ORCAMENTO_PACOTES { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_PACOTE> ORCAMENTO_PROPOSTA_PACOTE { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_PACOTE_VALORES> ORCAMENTO_PROPOSTA_PACOTE_VALORES { get; set; }
        public virtual ICollection<ORCAMENTO_VISITAS> ORCAMENTO_VISITAS { get; set; }
        public virtual ICollection<PACOTE> PACOTES { get; set; }
        public virtual ICollection<PACOTE_CATEGORIA_ITENS> PACOTE_CATEGORIA_ITENS { get; set; }
        public virtual ICollection<EVENTOS_PACOTES> EVENTOS_PACOTES { get; set; }
        public virtual ICollection<EVENTO_ALUGUEL> EVENTO_ALUGUEL { get; set; }
        public virtual ICollection<EVENTO_DIVERSOS> EVENTO_DIVERSOS { get; set; }
        public virtual ICollection<PACOTES_ITENS> PACOTES_ITENS { get; set; }
        public virtual ICollection<EVENTO_SERVICOS> EVENTO_SERVICOS { get; set; }
        public virtual ICollection<PERFIL_COLABORADOR> PERFIL_COLABORADOR { get; set; }
        public virtual ICollection<PESQUISA_PERGUNTA_OPCOES> PESQUISA_PERGUNTA_OPCOES { get; set; }
        public virtual ICollection<PESQUISA_PERGUNTA> PESQUISA_PERGUNTA { get; set; }
        public virtual ICollection<PESQUISA_PESQUISAS> PESQUISA_PESQUISAS { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA> PESQUISA_RESPOSTA { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA_OPCOES> PESQUISA_RESPOSTA_OPCOES { get; set; }
        public virtual ICollection<POTENCIAL_CLIENTE> POTENCIAL_CLIENTE { get; set; }
        public virtual ICollection<PRESTADOR_FUNCAO> PRESTADOR_FUNCAO { get; set; }
        public virtual ICollection<PRESTADOR> PRESTADORs { get; set; }
        public virtual ICollection<PRESTADOR_SERVICO> PRESTADOR_SERVICO { get; set; }
        public virtual ICollection<SUB_CATEGORIA_ITENS> SUB_CATEGORIA_ITENS { get; set; }
        public virtual ICollection<EVENTO> EVENTOes { get; set; }
        public virtual ICollection<TIPO_SERVICO> TIPO_SERVICO { get; set; }
        public virtual ICollection<LOCAL_EVENTO_COMENTARIOS> LOCAL_EVENTO_COMENTARIOS { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA> ORCAMENTO_PROPOSTA { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_ABAS> ORCAMENTO_PROPOSTA_ABAS { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_DESCRICAO> ORCAMENTO_PROPOSTA_DESCRICAO { get; set; }
    }
}
