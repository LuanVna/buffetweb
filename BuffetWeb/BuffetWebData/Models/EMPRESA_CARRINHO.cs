using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_CARRINHO
    {
        public int id { get; set; }
        public System.DateTime adicionado_em { get; set; }
        public string periodo { get; set; }
        public int id_plano_disponivel { get; set; }
        public int id_local_evento { get; set; }
        public int id_empresa { get; set; }
        public bool comprado { get; set; }
        public virtual B_PLANOS_DISPONIVEIS B_PLANOS_DISPONIVEIS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
