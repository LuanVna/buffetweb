using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_CARRINHO_CUPOM
    {
        public EMPRESA_CARRINHO_CUPOM()
        {
            this.EMPRESA_HISTORICO_PAGAMENTO = new List<EMPRESA_HISTORICO_PAGAMENTO>();
        }

        public int id { get; set; }
        public decimal valor_desconto { get; set; }
        public System.DateTime disponivel_ate { get; set; }
        public int id_empresa { get; set; }
        public int id_local_evento { get; set; }
        public string descricao { get; set; }
        public bool usado { get; set; }
        public int id_planos_disponiveis { get; set; }
        public string periodo { get; set; }
        public virtual B_PLANOS_DISPONIVEIS B_PLANOS_DISPONIVEIS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EMPRESA_HISTORICO_PAGAMENTO> EMPRESA_HISTORICO_PAGAMENTO { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
