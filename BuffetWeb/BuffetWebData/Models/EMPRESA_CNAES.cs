using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_CNAES
    {
        public int id { get; set; }
        public string cnae { get; set; }
        public string cnae_descricao { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
