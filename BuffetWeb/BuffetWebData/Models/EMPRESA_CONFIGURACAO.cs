using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_CONFIGURACAO
    {
        public int id { get; set; }
        public int id_empresa { get; set; }
        public string email_porta { get; set; }
        public string email_login { get; set; }
        public string email_host { get; set; }
        public string email_senha { get; set; }
        public bool permitirSMS { get; set; }
        public Nullable<bool> pesquisa_enviar_automaticamente { get; set; }
        public Nullable<int> pesquisa_dias_apos_evento { get; set; }
        public int quantidade_usuarios { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
