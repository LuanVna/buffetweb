using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_MODULOS
    {
        public int id { get; set; }
        public int id_empresa { get; set; }
        public string modulo { get; set; }
        public System.DateTime adquirido_em { get; set; }
        public System.DateTime expira_em { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
