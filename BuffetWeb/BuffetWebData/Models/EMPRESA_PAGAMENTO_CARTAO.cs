using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EMPRESA_PAGAMENTO_CARTAO
    {
        public int id { get; set; }
        public Nullable<int> id_empresa { get; set; }
        public string bandeira { get; set; }
        public string nome_titular { get; set; }
        public string ultimos_digitos { get; set; }
        public string token_autorizacao { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
