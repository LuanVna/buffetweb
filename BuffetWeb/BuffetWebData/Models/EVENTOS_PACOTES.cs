using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EVENTOS_PACOTES
    {
        public int id { get; set; }
        public int id_pacote { get; set; }
        public int id_evento { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EVENTO EVENTO { get; set; }
        public virtual PACOTE PACOTE { get; set; }
    }
}
