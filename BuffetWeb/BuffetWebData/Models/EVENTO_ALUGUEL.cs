using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class EVENTO_ALUGUEL
    {
        public int id { get; set; }
        public int id_itens_aluguel { get; set; }
        public int id_empresa { get; set; }
        public int id_tipo_evento { get; set; }
        public bool cobrar_brinde { get; set; }
        public bool brinde { get; set; }
        public int quantidade { get; set; }
        public Nullable<bool> mostrar_aluguel { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EVENTO EVENTO { get; set; }
        public virtual ITENS_ALUGUEL ITENS_ALUGUEL { get; set; }
    }
}
