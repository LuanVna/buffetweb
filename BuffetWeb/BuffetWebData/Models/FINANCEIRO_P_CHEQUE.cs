using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class FINANCEIRO_P_CHEQUE
    {
        public int id { get; set; }
        public System.DateTime data { get; set; }
        public string banco { get; set; }
        public string agencia { get; set; }
        public string conta { get; set; }
        public decimal valor { get; set; }
        public int n_cheque { get; set; }
        public int id_orcamento { get; set; }
        public int id_colaborador { get; set; }
        public int id_empresa { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
