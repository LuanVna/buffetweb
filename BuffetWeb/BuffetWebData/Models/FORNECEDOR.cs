using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class FORNECEDOR
    {
        public FORNECEDOR()
        {
            this.FORNECEDOR_INGREDIENTE = new List<FORNECEDOR_INGREDIENTE>();
        }

        public int id { get; set; }
        public string razao_social { get; set; }
        public string cnpj { get; set; }
        public string telefone { get; set; }
        public string contato { get; set; }
        public string telefone_contato { get; set; }
        public string celular_contato { get; set; }
        public string email { get; set; }
        public Nullable<int> id_endereco { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ENDERECO ENDERECO { get; set; }
        public virtual ICollection<FORNECEDOR_INGREDIENTE> FORNECEDOR_INGREDIENTE { get; set; }
    }
}
