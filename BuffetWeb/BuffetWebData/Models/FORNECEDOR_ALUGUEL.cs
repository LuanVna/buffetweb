using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class FORNECEDOR_ALUGUEL
    {
        public int id { get; set; }
        public int id_fornecedor { get; set; }
        public int id_aluguel { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_ALUGUEL ITENS_ALUGUEL { get; set; }
    }
}
