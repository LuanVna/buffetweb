using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class FORNECEDOR_INGREDIENTE
    {
        public int id { get; set; }
        public int id_empresa { get; set; }
        public int id_ingrediente { get; set; }
        public int id_fornecedor { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual FORNECEDOR FORNECEDOR { get; set; }
        public virtual INGREDIENTE INGREDIENTE { get; set; }
    }
}
