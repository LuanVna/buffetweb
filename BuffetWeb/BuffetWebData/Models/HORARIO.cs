using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class HORARIO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public System.TimeSpan horario_de { get; set; }
        public System.TimeSpan horario_ate { get; set; }
        public int id_empresa { get; set; }
    }
}
