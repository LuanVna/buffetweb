using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class IMAGENS_DOS_EVENTOS
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string url { get; set; }
        public int id_imagens_evento { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual IMAGENS_GALERIA_EVENTOS IMAGENS_GALERIA_EVENTOS { get; set; }
    }
}
