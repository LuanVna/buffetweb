using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class IMAGENS_ESPACO_LOCAL_EVENTOS
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string url { get; set; }
        public int id_empresa { get; set; }
        public int id_local_evento { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
