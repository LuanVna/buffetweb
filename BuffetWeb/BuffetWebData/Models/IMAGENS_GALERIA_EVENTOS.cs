using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class IMAGENS_GALERIA_EVENTOS
    {
        public IMAGENS_GALERIA_EVENTOS()
        {
            this.IMAGENS_DOS_EVENTOS = new List<IMAGENS_DOS_EVENTOS>();
        }

        public int id { get; set; }
        public int id_empresa { get; set; }
        public string nome { get; set; }
        public int id_orcamento { get; set; }
        public string descricao { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<IMAGENS_DOS_EVENTOS> IMAGENS_DOS_EVENTOS { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
