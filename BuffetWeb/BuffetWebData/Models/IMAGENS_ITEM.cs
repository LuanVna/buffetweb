using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class IMAGENS_ITEM
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string url { get; set; }
        public int id_empresa { get; set; }
        public string pasta { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
