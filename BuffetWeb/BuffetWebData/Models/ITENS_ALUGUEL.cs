using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ITENS_ALUGUEL
    {
        public ITENS_ALUGUEL()
        {
            this.EVENTO_ALUGUEL = new List<EVENTO_ALUGUEL>();
            this.FORNECEDOR_ALUGUEL = new List<FORNECEDOR_ALUGUEL>();
            this.ORCAMENTO_PACOTE = new List<ORCAMENTO_PACOTE>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public decimal valor_custo { get; set; }
        public decimal valor_venda { get; set; }
        public bool status { get; set; }
        public int id_empresa { get; set; }
        public int id_categoria { get; set; }
        public decimal valor_reembolso { get; set; }
        public decimal valor_atraso { get; set; }
        public int quantidade_disponivel { get; set; }
        public string url_image { get; set; }
        public virtual CATEGORIA_ALUGUEL CATEGORIA_ALUGUEL { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EVENTO_ALUGUEL> EVENTO_ALUGUEL { get; set; }
        public virtual ICollection<FORNECEDOR_ALUGUEL> FORNECEDOR_ALUGUEL { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
    }
}
