using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ITENS_DIVERSOS
    {
        public ITENS_DIVERSOS()
        {
            this.EVENTO_DIVERSOS = new List<EVENTO_DIVERSOS>();
            this.ORCAMENTO_PACOTE = new List<ORCAMENTO_PACOTE>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public decimal valor_custo { get; set; }
        public decimal valor_venda { get; set; }
        public bool status { get; set; }
        public int id_empresa { get; set; }
        public int id_categoria { get; set; }
        public int id_unidade_medida { get; set; }
        public string url_image { get; set; }
        public virtual CATEGORIA_SERVICOS CATEGORIA_SERVICOS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EVENTO_DIVERSOS> EVENTO_DIVERSOS { get; set; }
        public virtual UNIDADE_MEDIDA UNIDADE_MEDIDA { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
    }
}
