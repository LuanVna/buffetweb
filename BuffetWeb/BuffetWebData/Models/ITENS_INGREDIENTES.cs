using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ITENS_INGREDIENTES
    {
        public int id { get; set; }
        public int id_cardapio { get; set; }
        public int id_ingrediente { get; set; }
        public decimal valor_atual { get; set; }
        public int id_empresa { get; set; }
        public int quantidade { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual INGREDIENTE INGREDIENTE { get; set; }
        public virtual ITENS_CARDAPIO ITENS_CARDAPIO { get; set; }
    }
}
