using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ITENS_SERVICOS
    {
        public ITENS_SERVICOS()
        {
            this.EVENTO_SERVICOS = new List<EVENTO_SERVICOS>();
            this.ORCAMENTO_PACOTE = new List<ORCAMENTO_PACOTE>();
            this.PRESTADOR_SERVICO = new List<PRESTADOR_SERVICO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public decimal valor_custo { get; set; }
        public decimal valor_venda { get; set; }
        public bool status { get; set; }
        public int id_empresa { get; set; }
        public int id_categoria { get; set; }
        public string url_image { get; set; }
        public virtual CATEGORIA_SERVICOS CATEGORIA_SERVICOS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EVENTO_SERVICOS> EVENTO_SERVICOS { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
        public virtual ICollection<PRESTADOR_SERVICO> PRESTADOR_SERVICO { get; set; }
    }
}
