using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class LOCAL_EVENTO
    {
        public LOCAL_EVENTO()
        {
            this.B_PORTAL_DENUNCIAS = new List<B_PORTAL_DENUNCIAS>();
            this.EMPRESA_CARRINHO = new List<EMPRESA_CARRINHO>();
            this.EMPRESA_CARRINHO_CUPOM = new List<EMPRESA_CARRINHO_CUPOM>();
            this.IMAGENS_ESPACO_LOCAL_EVENTOS = new List<IMAGENS_ESPACO_LOCAL_EVENTOS>();
            this.PORTAL_CLIENTE_FAVORITOS = new List<PORTAL_CLIENTE_FAVORITOS>();
            this.VISITAS_DISPONIVEIS = new List<VISITAS_DISPONIVEIS>();
            this.LOCAL_EVENTO_IMAGENS = new List<LOCAL_EVENTO_IMAGENS>();
            this.LOCAL_EVENTO_SOCIAL = new List<LOCAL_EVENTO_SOCIAL>();
            this.ORCAMENTOes = new List<ORCAMENTO>();
            this.PORTAL_ANUNCIANTE_PROMOCAO = new List<PORTAL_ANUNCIANTE_PROMOCAO>();
            this.PORTAL_ANUNCIANTE_TOPMENU = new List<PORTAL_ANUNCIANTE_TOPMENU>();
            this.PORTAL_VISUALIZACOES = new List<PORTAL_VISUALIZACOES>();
            this.ORCAMENTO_VISITAS = new List<ORCAMENTO_VISITAS>();
            this.LOCAL_EVENTO_COMENTARIOS = new List<LOCAL_EVENTO_COMENTARIOS>();
            this.ORCAMENTO_PROPOSTA = new List<ORCAMENTO_PROPOSTA>();
            this.PORTAL_ANUNCIANTE_DESTAQUE = new List<PORTAL_ANUNCIANTE_DESTAQUE>();
            this.PORTAL_ANUNCIANTE_HEADER = new List<PORTAL_ANUNCIANTE_HEADER>();
            this.PORTAL_ANUNCIANTE_HOME = new List<PORTAL_ANUNCIANTE_HOME>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public int id_endereco { get; set; }
        public int id_empresa { get; set; }
        public int capacidade { get; set; }
        public bool porEspaco { get; set; }
        public decimal valorMinimo { get; set; }
        public decimal valor { get; set; }
        public string telefone { get; set; }
        public string autenticacao { get; set; }
        public string regiao { get; set; }
        public Nullable<int> idade_minima_pagante { get; set; }
        public Nullable<System.TimeSpan> horario_inicio_almoco { get; set; }
        public Nullable<System.TimeSpan> horario_fim_almoco { get; set; }
        public Nullable<System.TimeSpan> horario_inicio_jantar { get; set; }
        public Nullable<System.TimeSpan> horario_fim_jantar { get; set; }
        public string palavras_chaves { get; set; }
        public string descricao { get; set; }
        public string video { get; set; }
        public virtual ICollection<B_PORTAL_DENUNCIAS> B_PORTAL_DENUNCIAS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO> EMPRESA_CARRINHO { get; set; }
        public virtual ICollection<EMPRESA_CARRINHO_CUPOM> EMPRESA_CARRINHO_CUPOM { get; set; }
        public virtual ENDERECO ENDERECO { get; set; }
        public virtual ICollection<IMAGENS_ESPACO_LOCAL_EVENTOS> IMAGENS_ESPACO_LOCAL_EVENTOS { get; set; }
        public virtual ICollection<PORTAL_CLIENTE_FAVORITOS> PORTAL_CLIENTE_FAVORITOS { get; set; }
        public virtual ICollection<VISITAS_DISPONIVEIS> VISITAS_DISPONIVEIS { get; set; }
        public virtual ICollection<LOCAL_EVENTO_IMAGENS> LOCAL_EVENTO_IMAGENS { get; set; }
        public virtual ICollection<LOCAL_EVENTO_SOCIAL> LOCAL_EVENTO_SOCIAL { get; set; }
        public virtual ICollection<ORCAMENTO> ORCAMENTOes { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_PROMOCAO> PORTAL_ANUNCIANTE_PROMOCAO { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_TOPMENU> PORTAL_ANUNCIANTE_TOPMENU { get; set; }
        public virtual ICollection<PORTAL_VISUALIZACOES> PORTAL_VISUALIZACOES { get; set; }
        public virtual ICollection<ORCAMENTO_VISITAS> ORCAMENTO_VISITAS { get; set; }
        public virtual ICollection<LOCAL_EVENTO_COMENTARIOS> LOCAL_EVENTO_COMENTARIOS { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA> ORCAMENTO_PROPOSTA { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_DESTAQUE> PORTAL_ANUNCIANTE_DESTAQUE { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_HEADER> PORTAL_ANUNCIANTE_HEADER { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_HOME> PORTAL_ANUNCIANTE_HOME { get; set; }
    }
}
