using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class LOCAL_EVENTO_COMENTARIOS
    {
        public int id { get; set; }
        public string comentario { get; set; }
        public System.DateTime desde { get; set; }
        public int avaliacao { get; set; }
        public int id_cliente_perfil { get; set; }
        public int id_empresa { get; set; }
        public int id_local_evento { get; set; }
        public bool analisado { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual PORTAL_CLIENTE_PERFIL PORTAL_CLIENTE_PERFIL { get; set; }
    }
}
