using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class LOCAL_EVENTO_IMAGENS
    {
        public int id { get; set; }
        public int sequencia { get; set; }
        public int id_local_evento { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
