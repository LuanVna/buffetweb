using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ORCAMENTO
    {
        public ORCAMENTO()
        {
            this.CONVITE_ANIVERSARIANTES = new List<CONVITE_ANIVERSARIANTES>();
            this.CONVITE_CONVIDADOS = new List<CONVITE_CONVIDADOS>();
            this.CONVITE_CONVIDADOS_FACEBOOK = new List<CONVITE_CONVIDADOS_FACEBOOK>();
            this.CONVITE_CONVIDADOS_FAMILIARES = new List<CONVITE_CONVIDADOS_FAMILIARES>();
            this.CONVITE_TEMPLATE = new List<CONVITE_TEMPLATE>();
            this.FINANCEIRO_P_BOLETO = new List<FINANCEIRO_P_BOLETO>();
            this.FINANCEIRO_P_CHEQUE = new List<FINANCEIRO_P_CHEQUE>();
            this.FINANCEIRO_P_CREDITO = new List<FINANCEIRO_P_CREDITO>();
            this.FINANCEIRO_P_DEBITO = new List<FINANCEIRO_P_DEBITO>();
            this.FINANCEIRO_P_DEPOSITO = new List<FINANCEIRO_P_DEPOSITO>();
            this.FINANCEIRO_P_ESPECIE = new List<FINANCEIRO_P_ESPECIE>();
            this.IMAGENS_GALERIA_EVENTOS = new List<IMAGENS_GALERIA_EVENTOS>();
            this.ORCAMENTO_ANOTACOES = new List<ORCAMENTO_ANOTACOES>();
            this.ORCAMENTO_ITENS = new List<ORCAMENTO_ITENS>();
            this.PESQUISA_PESQUISAS = new List<PESQUISA_PESQUISAS>();
            this.ORCAMENTO_VISITAS = new List<ORCAMENTO_VISITAS>();
            this.VISITAS_AGENDADAS = new List<VISITAS_AGENDADAS>();
        }

        public int id { get; set; }
        public string codigo_unico { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public System.DateTime data_evento { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> evento_para { get; set; }
        public Nullable<int> id_tipo_servico { get; set; }
        public string senha { get; set; }
        public Nullable<System.DateTime> data_nascimento { get; set; }
        public string email2 { get; set; }
        public string instituicao { get; set; }
        public Nullable<int> n_convidados { get; set; }
        public Nullable<int> n_convidados_c { get; set; }
        public string nome2 { get; set; }
        public string razao { get; set; }
        public string nome { get; set; }
        public string bodas_de_bodas { get; set; }
        public string telefone2 { get; set; }
        public System.DateTime data_criacao { get; set; }
        public int id_cliente { get; set; }
        public Nullable<int> id_tipo_evento { get; set; }
        public Nullable<bool> aceita_contrato { get; set; }
        public string nome_noivo { get; set; }
        public string nome_noiva { get; set; }
        public string telefone_noivo { get; set; }
        public string telefone_noiva { get; set; }
        public string email_noivo { get; set; }
        public string email_noiva { get; set; }
        public bool apenas_convite { get; set; }
        public Nullable<System.TimeSpan> horario_inicio { get; set; }
        public Nullable<System.TimeSpan> horario_fim { get; set; }
        public Nullable<int> max_acompanhantes { get; set; }
        public string mensagem_padrao { get; set; }
        public int id_empresa { get; set; }
        public Nullable<int> id_local_evento { get; set; }
        public Nullable<int> id_evento { get; set; }
        public string autenticacao { get; set; }
        public virtual CLIENTE CLIENTE { get; set; }
        public virtual ICollection<CONVITE_ANIVERSARIANTES> CONVITE_ANIVERSARIANTES { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS> CONVITE_CONVIDADOS { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS_FACEBOOK> CONVITE_CONVIDADOS_FACEBOOK { get; set; }
        public virtual ICollection<CONVITE_CONVIDADOS_FAMILIARES> CONVITE_CONVIDADOS_FAMILIARES { get; set; }
        public virtual ICollection<CONVITE_TEMPLATE> CONVITE_TEMPLATE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EVENTO EVENTO { get; set; }
        public virtual ICollection<FINANCEIRO_P_BOLETO> FINANCEIRO_P_BOLETO { get; set; }
        public virtual ICollection<FINANCEIRO_P_CHEQUE> FINANCEIRO_P_CHEQUE { get; set; }
        public virtual ICollection<FINANCEIRO_P_CREDITO> FINANCEIRO_P_CREDITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEBITO> FINANCEIRO_P_DEBITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEPOSITO> FINANCEIRO_P_DEPOSITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_ESPECIE> FINANCEIRO_P_ESPECIE { get; set; }
        public virtual ICollection<IMAGENS_GALERIA_EVENTOS> IMAGENS_GALERIA_EVENTOS { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual ICollection<ORCAMENTO_ANOTACOES> ORCAMENTO_ANOTACOES { get; set; }
        public virtual ICollection<ORCAMENTO_ITENS> ORCAMENTO_ITENS { get; set; }
        public virtual ICollection<PESQUISA_PESQUISAS> PESQUISA_PESQUISAS { get; set; }
        public virtual ICollection<ORCAMENTO_VISITAS> ORCAMENTO_VISITAS { get; set; }
        public virtual ICollection<VISITAS_AGENDADAS> VISITAS_AGENDADAS { get; set; }
    }
}
