using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ORCAMENTO_ITENS
    {
        public int id { get; set; }
        public decimal valor_atual { get; set; }
        public int id_cardapio { get; set; }
        public int id_orcamento { get; set; }
        public System.DateTime data_objeto { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_CARDAPIO ITENS_CARDAPIO { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
