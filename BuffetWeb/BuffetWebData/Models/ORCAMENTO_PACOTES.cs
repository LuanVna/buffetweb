using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ORCAMENTO_PACOTES
    {
        public int id { get; set; }
        public int id_pacote { get; set; }
        public string nome_pacote { get; set; }
        public string descricao { get; set; }
        public bool disponivel { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PACOTE ORCAMENTO_PACOTE { get; set; }
    }
}
