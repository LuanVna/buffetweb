using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ORCAMENTO_PROPOSTA_PACOTE
    {
        public ORCAMENTO_PROPOSTA_PACOTE()
        {
            this.ORCAMENTO_PROPOSTA_PACOTE_VALORES = new List<ORCAMENTO_PROPOSTA_PACOTE_VALORES>();
        }

        public int id { get; set; }
        public int quantidade_pessoas { get; set; }
        public int id_proposta { get; set; }
        public int id_empresa { get; set; }
        public bool ativo { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PROPOSTA ORCAMENTO_PROPOSTA { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_PACOTE_VALORES> ORCAMENTO_PROPOSTA_PACOTE_VALORES { get; set; }
    }
}
