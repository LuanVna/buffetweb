using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class ORCAMENTO_PROPOSTA_PACOTE_VALORES
    {
        public int id { get; set; }
        public decimal valor_almoco { get; set; }
        public decimal valor_jantar { get; set; }
        public bool segunda { get; set; }
        public bool terca { get; set; }
        public bool quarta { get; set; }
        public bool quinta { get; set; }
        public bool sexta { get; set; }
        public bool sabado { get; set; }
        public int id_proposta_pacote { get; set; }
        public int id_empresa { get; set; }
        public Nullable<bool> domingo { get; set; }
        public decimal valor_adicional_pessoa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PROPOSTA_PACOTE ORCAMENTO_PROPOSTA_PACOTE { get; set; }
    }
}
