using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PACOTE
    {
        public PACOTE()
        {
            this.EVENTOS_PACOTES = new List<EVENTOS_PACOTES>();
            this.IMAGENS_PACOTES = new List<IMAGENS_PACOTES>();
            this.PACOTE_CATEGORIA_ITENS = new List<PACOTE_CATEGORIA_ITENS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public System.DateTime data_criacao { get; set; }
        public bool mostrar_valor { get; set; }
        public Nullable<bool> status { get; set; }
        public int id_empresa { get; set; }
        public decimal margem_lucro { get; set; }
        public decimal valor_custo { get; set; }
        public decimal valor_venda { get; set; }
        public string url_image { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EVENTOS_PACOTES> EVENTOS_PACOTES { get; set; }
        public virtual ICollection<IMAGENS_PACOTES> IMAGENS_PACOTES { get; set; }
        public virtual ICollection<PACOTE_CATEGORIA_ITENS> PACOTE_CATEGORIA_ITENS { get; set; }
    }
}
