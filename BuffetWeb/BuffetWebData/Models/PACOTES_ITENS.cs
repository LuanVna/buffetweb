using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PACOTES_ITENS
    {
        public int id { get; set; }
        public int id_categoria_pacote { get; set; }
        public int id_categoria { get; set; }
        public int id_itens_cardapio { get; set; }
        public int id_empresa { get; set; }
        public virtual CATEGORIA_ITENS CATEGORIA_ITENS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_CARDAPIO ITENS_CARDAPIO { get; set; }
        public virtual PACOTE_CATEGORIA_ITENS PACOTE_CATEGORIA_ITENS { get; set; }
    }
}
