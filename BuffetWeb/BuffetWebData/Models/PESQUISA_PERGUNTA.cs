using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PESQUISA_PERGUNTA
    {
        public PESQUISA_PERGUNTA()
        {
            this.PESQUISA_PERGUNTA_OPCOES = new List<PESQUISA_PERGUNTA_OPCOES>();
            this.PESQUISA_RESPOSTA = new List<PESQUISA_RESPOSTA>();
        }

        public int id { get; set; }
        public int id_pesquisa { get; set; }
        public string titulo { get; set; }
        public int id_empresa { get; set; }
        public string tipo_pergunta { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PESQUISA_PESQUISAS PESQUISA_PESQUISAS { get; set; }
        public virtual ICollection<PESQUISA_PERGUNTA_OPCOES> PESQUISA_PERGUNTA_OPCOES { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA> PESQUISA_RESPOSTA { get; set; }
    }
}
