using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PESQUISA_PERGUNTA_OPCOES
    {
        public PESQUISA_PERGUNTA_OPCOES()
        {
            this.PESQUISA_RESPOSTA_OPCOES = new List<PESQUISA_RESPOSTA_OPCOES>();
        }

        public int id { get; set; }
        public string titulo { get; set; }
        public int id_empresa { get; set; }
        public Nullable<int> id_pergunta { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PESQUISA_PERGUNTA PESQUISA_PERGUNTA { get; set; }
        public virtual ICollection<PESQUISA_RESPOSTA_OPCOES> PESQUISA_RESPOSTA_OPCOES { get; set; }
    }
}
