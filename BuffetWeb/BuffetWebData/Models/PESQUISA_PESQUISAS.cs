using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PESQUISA_PESQUISAS
    {
        public PESQUISA_PESQUISAS()
        {
            this.PESQUISA_PERGUNTA = new List<PESQUISA_PERGUNTA>();
        }

        public int id { get; set; }
        public int id_orcamento { get; set; }
        public int total_respostas { get; set; }
        public int total_enviadas { get; set; }
        public System.DateTime data_envio { get; set; }
        public int id_empresa { get; set; }
        public bool enviada { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
        public virtual ICollection<PESQUISA_PERGUNTA> PESQUISA_PERGUNTA { get; set; }
    }
}
