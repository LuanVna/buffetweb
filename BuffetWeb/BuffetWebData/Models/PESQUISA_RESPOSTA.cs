using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PESQUISA_RESPOSTA
    {
        public int id { get; set; }
        public int id_pergunta { get; set; }
        public int id_empresa { get; set; }
        public string resposta { get; set; }
        public Nullable<int> id_convidado { get; set; }
        public virtual CONVITE_CONVIDADOS CONVITE_CONVIDADOS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PESQUISA_PERGUNTA PESQUISA_PERGUNTA { get; set; }
    }
}
