using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PORTAL_ANUNCIANTE_PROMOCAO
    {
        public int id { get; set; }
        public int id_portal_promocao { get; set; }
        public int id_local_evento { get; set; }
        public bool disponivel { get; set; }
        public bool destaque_pesquisa_zona { get; set; }
        public bool destaque_mapa { get; set; }
        public bool envio_proposta { get; set; }
        public virtual B_PORTAL_PROMOCAO B_PORTAL_PROMOCAO { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
