using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PORTAL_VISUALIZACOES
    {
        public int id { get; set; }
        public System.DateTime data { get; set; }
        public Nullable<int> id_cliente_portal { get; set; }
        public int id_local_evento { get; set; }
        public string tipo { get; set; }
        public string facebook { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual PORTAL_CLIENTE_PERFIL PORTAL_CLIENTE_PERFIL { get; set; }
    }
}
