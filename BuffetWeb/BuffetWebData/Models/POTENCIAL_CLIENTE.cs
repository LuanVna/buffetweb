using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class POTENCIAL_CLIENTE
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public int id_festa { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EVENTO EVENTO { get; set; }
    }
}
