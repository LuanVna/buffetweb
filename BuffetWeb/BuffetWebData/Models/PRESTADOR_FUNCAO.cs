using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PRESTADOR_FUNCAO
    {
        public PRESTADOR_FUNCAO()
        {
            this.PRESTADORs = new List<PRESTADOR>();
            this.PRESTADOR_SERVICO = new List<PRESTADOR_SERVICO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<PRESTADOR> PRESTADORs { get; set; }
        public virtual ICollection<PRESTADOR_SERVICO> PRESTADOR_SERVICO { get; set; }
    }
}
