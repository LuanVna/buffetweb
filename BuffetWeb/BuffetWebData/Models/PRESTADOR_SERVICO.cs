using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class PRESTADOR_SERVICO
    {
        public int id { get; set; }
        public int id_servico { get; set; }
        public int id_empresa { get; set; }
        public int id_prestador_funcao { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_SERVICOS ITENS_SERVICOS { get; set; }
        public virtual PRESTADOR_FUNCAO PRESTADOR_FUNCAO { get; set; }
    }
}
