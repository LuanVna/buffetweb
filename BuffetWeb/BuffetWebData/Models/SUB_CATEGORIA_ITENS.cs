using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class SUB_CATEGORIA_ITENS
    {
        public SUB_CATEGORIA_ITENS()
        {
            this.ITENS_CARDAPIO = new List<ITENS_CARDAPIO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public int id_categoria_itens { get; set; }
        public int id_empresa { get; set; }
        public int id_unidade_medida { get; set; }
        public decimal rendimento_pessoa { get; set; }
        public virtual CATEGORIA_ITENS CATEGORIA_ITENS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<ITENS_CARDAPIO> ITENS_CARDAPIO { get; set; }
        public virtual UNIDADE_MEDIDA UNIDADE_MEDIDA { get; set; }
    }
}
