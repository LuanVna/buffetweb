using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class UNIDADE_MEDIDA
    {
        public UNIDADE_MEDIDA()
        {
            this.INGREDIENTES = new List<INGREDIENTE>();
            this.ITENS_CARDAPIO = new List<ITENS_CARDAPIO>();
            this.ITENS_DIVERSOS = new List<ITENS_DIVERSOS>();
            this.SUB_CATEGORIA_ITENS = new List<SUB_CATEGORIA_ITENS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string sigla { get; set; }
        public virtual ICollection<INGREDIENTE> INGREDIENTES { get; set; }
        public virtual ICollection<ITENS_CARDAPIO> ITENS_CARDAPIO { get; set; }
        public virtual ICollection<ITENS_DIVERSOS> ITENS_DIVERSOS { get; set; }
        public virtual ICollection<SUB_CATEGORIA_ITENS> SUB_CATEGORIA_ITENS { get; set; }
    }
}
