using System;
using System.Collections.Generic;

namespace BuffetWebData.Models
{
    public partial class VISITAS_AGENDADAS
    {
        public VISITAS_AGENDADAS()
        {
            this.ORCAMENTO_VISITAS = new List<ORCAMENTO_VISITAS>();
        }

        public int id { get; set; }
        public int id_empresa { get; set; }
        public string dia { get; set; }
        public System.DateTime data { get; set; }
        public int id_visitas_disponiveis { get; set; }
        public int id_orcamento { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
        public virtual ICollection<ORCAMENTO_VISITAS> ORCAMENTO_VISITAS { get; set; }
        public virtual VISITAS_DISPONIVEIS VISITAS_DISPONIVEIS { get; set; }
    }
}
