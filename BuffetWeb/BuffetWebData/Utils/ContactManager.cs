﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Net.Mail;
using System.Net;
using HumanAPIClient.Service;
using HumanAPIClient.Model;
using System.Web;
using Newtonsoft.Json;
using System.Dynamic;
using Facebook;

namespace BuffetWebData.Utils
{
    public class ContentNotification
    {
        public string action { get; set; }
        public string controller { get; set; }
        public string parameters { get; set; }
    }


    internal class EmailSettings
    {
        public string email { get; set; }
        public string senha { get; set; }
        public string servidor { get; set; }
        public int porta { get; set; }
    }

    public enum EmailContentButton
    {
        Acessar,
        Verificar,
        Visualizar
    }

    public enum TipoContato
    {
        InformacoesDeLogin,
        Proposta,
        Convite,
        AgendamentoDeVisita,
        RespostaProposta,
        ConfirmacaoCadastro,
        PortalPromocao
    }

    public enum EmailContentHeader
    {
        Proposta,
        Convite,
        Confirmar,
        AgendamentoDeVisita,
        RenovarSenha
    }

    public class EmailContent
    {
        public EmailContentButton emailContentButton { get; set; }
        public EmailContentHeader emailContentHeader { get; set; }
        public TemplateEmail emailTemplate { get; set; }
        public string nomeOla { get; set; }
        public string url { get; set; }
    }

    public enum TemplateEmail
    {
        TemplateEmpresa,
        TemplateBuffetWeb,
        TemplatePortal
    }

    public enum EmailStatusSend
    {
        Send,
        Error
    }

    public enum FacebookStatusSend
    {
        Send,
        Error
    }

    public enum ReadEmailStatus
    {
        ExistAndSave,
        NotExist,
        Error,
    }

    public enum SMSStatusSend
    {
        Send,
        Error
    }

    public enum TimeLineIcon
    {
        Info,
        Danger,
        Warning
    }

    public enum NotificationRule
    {
        orcamento_agendar_visita,
        entrar_em_contato,
        orcamento_novo_pedido,
        orcamento_fechar_pedido,
        pesquisa_concluida,
        notificarNovoOrcamento,
        notificarAgendarVisita,
        notificarNotificacaoEmail,
        notificarNovoPedido,
        notificarPesquisaConcluida
    }

    public class ContactManager
    {
        private int id_empresa { get; set; }
        public int? id_cliente { get; set; }
        public string auxNomeHeader { get; set; }

        private EMPRESA empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                }
            }
        }

        public ContactManager()
        {

        }
        public ContactManager(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }
        public ContactManager(int id_empresa, int id_cliente)
        {
            this.id_empresa = id_empresa;
            this.id_cliente = id_cliente;
        }

        #region SMS
        public SMSStatusSend SendSMS(string phone, string title, string message, TipoContato tipoContato, string autenticacao = "")
        {
            try
            {
                if (autenticacao.Equals(""))
                {
                    autenticacao = UtilsManager.CriarAutenticacao();
                }
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA empresa = db.EMPRESAs.Include(f => f.EMPRESA_PACOTE_SMS).Include(c => c.EMPRESA_CONFIGURACAO).FirstOrDefault(e => e.id == this.id_empresa && e.EMPRESA_CONFIGURACAO.FirstOrDefault().permitirSMS);
                    if (empresa != null)
                    {
                        EMPRESA_PACOTE_SMS sms = empresa.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0);
                        if (sms != null)
                        {
                            byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(message);
                            message = System.Text.Encoding.UTF8.GetString(bytes);

                            SimpleSending cliente = new SimpleSending("recargalucrativa", "7rNEE227EQ");
                            SimpleMessage smsS = new SimpleMessage();
                            smsS.To = string.Format("55{0}", phone.Replace("-", "").Replace("(", "").Replace(")", ""));
                            smsS.Message = string.Format("{0} - {1}", this.empresa.nome, message);
                            List<string> respostaSMS = cliente.send(smsS);
                            if (respostaSMS.Count != 0 && respostaSMS[0].Equals("000 - Message Sent"))
                            {
                                sms.quantidade_disponivel--;
                            }
                        }


                        var campanha = db.CAMPANHA_CONTATOS_ENVIADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.tipo_contato.Equals(tipoContato.ToString()) && e.id_empresa == this.id_empresa);
                        if (campanha == null)
                        {
                            db.CAMPANHA_CONTATOS_ENVIADOS.Add(new CAMPANHA_CONTATOS_ENVIADOS()
                            {
                                autenticacao = autenticacao,
                                data_envio = DateTime.Today,
                                id_empresa = this.id_empresa,
                                mensagem = message,
                                titulo = title,
                                id_cliente = this.id_cliente,
                                enviado_sms = true,
                                tipo_contato = tipoContato.ToString()
                            });
                        }
                        else
                        {
                            campanha.lido_sms = false;
                            campanha.enviado_sms = true;
                        }
                        db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                        {
                            acontecimento = "SMS enviado",
                            data = DateTime.Now,
                            id_campanha_contatos_enviados = campanha.id
                        });
                        db.SaveChanges();
                        return SMSStatusSend.Send;
                    }
                }
            }
            catch (Exception e)
            {
                return SMSStatusSend.Error;
            }
            return SMSStatusSend.Error;
        }

        #endregion
        #region EMAIL
        public EmailStatusSend SendEmail(string email, string title, string message, TipoContato tipoContato, string autenticacao, EmailContent emailContent)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    String template = "";
                    if (emailContent == null)
                    {
                        new Exception("Error Email Content");
                    }
                    if (emailContent.emailTemplate == TemplateEmail.TemplateEmpresa)
                        template = this.TemplateEmailEmpresa(autenticacao, message, emailContent.nomeOla, emailContent);
                    else
                        template = this.TemplateEmailBuffetWeb(autenticacao, title, message, emailContent.nomeOla, emailContent);

                    MailMessage mensagemEmail = new MailMessage(email, email, string.Format("{0} - {1}", this.empresa.nome, title), template);
                    mensagemEmail.IsBodyHtml = true;

                    EmailSettings settings = this.emailSettings;

                    SmtpClient client = new SmtpClient(settings.servidor, settings.porta);
                    client.EnableSsl = true;
                    client.Credentials = new NetworkCredential(settings.email, settings.senha);
                    client.Send(mensagemEmail);

                    var campanha = db.CAMPANHA_CONTATOS_ENVIADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.tipo_contato.Equals(tipoContato.ToString()) && e.id_empresa == this.id_empresa);
                    if (campanha == null)
                    {
                        var c = new CAMPANHA_CONTATOS_ENVIADOS()
                        {
                            autenticacao = autenticacao,
                            data_envio = DateTime.Now,
                            id_empresa = this.id_empresa,
                            mensagem = message,
                            titulo = title,
                            id_cliente = this.id_cliente,
                            enviado_email = true,
                            tipo_contato = tipoContato.ToString()
                        };
                        db.CAMPANHA_CONTATOS_ENVIADOS.Add(c);
                        db.SaveChanges();

                        db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                        {
                            acontecimento = "Email enviado",
                            data = DateTime.Now,
                            id_campanha_contatos_enviados = c.id
                        });
                        db.SaveChanges();
                    }
                    else
                    {
                        campanha.lido_email = false;
                        campanha.enviado_email = true;
                        db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                        {
                            acontecimento = "Email enviado",
                            data = DateTime.Now,
                            id_campanha_contatos_enviados = campanha.id
                        });
                        db.SaveChanges();
                    }

                    return EmailStatusSend.Send;
                }
            }
            catch (Exception e)
            {
                return EmailStatusSend.Error;
            }
        }

        public EmailStatusSend SendEmailPortal(string email, string title, string mensagem, EmailContent emailContent)
        {
            try
            {
                MailMessage mensagemEmail = new MailMessage(email, email, title, mensagem);
                mensagemEmail.IsBodyHtml = true;

                EmailSettings settings = this.emailSettings;

                SmtpClient client = new SmtpClient(settings.servidor, settings.porta);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(settings.email, settings.senha);
                client.Send(mensagemEmail);
                return EmailStatusSend.Send;
            }
            catch (Exception e)
            {
                return EmailStatusSend.Error;
            }
        }

        public EmailStatusSend SendEmail(string email, string title, string mensagem)
        {
            try
            {
                MailMessage mensagemEmail = new MailMessage(email, email, title, mensagem);
                mensagemEmail.IsBodyHtml = true;

                EmailSettings settings = this.emailSettings;

                SmtpClient client = new SmtpClient(settings.servidor, settings.porta);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(settings.email, settings.senha);
                client.Send(mensagemEmail);
                return EmailStatusSend.Send;
            }
            catch (Exception e)
            {
                return EmailStatusSend.Error;
            }
        }


        public ReadEmailStatus ReadContact(HttpRequestBase request, string autenticacao, TipoContato tipoContato)
        {
            using (BuffetContext db = new BuffetContext())
            {

                CAMPANHA_CONTATOS_ENVIADOS campanha = db.CAMPANHA_CONTATOS_ENVIADOS
                                                                .FirstOrDefault(e => e.autenticacao.Equals(autenticacao) &&
                                                                 e.tipo_contato.Equals(tipoContato.ToString()));
                if (campanha != null)
                {
                    try
                    {
                        dynamic o = new WebClient().DownloadString(string.Format("http://ipinfo.io/{0}/json", request.UserHostAddress));

                        IPInfo json = JsonConvert.DeserializeObject<IPInfo>(o);

                        if (json != null)
                        {
                            campanha.endereco_ip = json.ip;
                            campanha.hostname = json.hostname;
                            campanha.cidade = json.city;
                            campanha.estado = json.region;
                            if (json.loc != null)
                            {
                                campanha.longitude = ((string)json.loc).Split(',')[0];
                                campanha.latitude = ((string)json.loc).Split(',')[1];
                            }
                            campanha.provedor = json.org;
                        }

                        string titulo = "";

                        if (request.UserAgent.Contains("Mobile"))
                        {
                            campanha.lido_sms = true;
                            titulo = "Email lido com celular";

                            db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                            {
                                acontecimento = titulo,
                                data = DateTime.Now,
                                id_campanha_contatos_enviados = campanha.id
                            });
                        }
                        else
                        {
                            string linhaTempo = campanha.enviado_facebook == true ? "Notificação lida" : "Email lido";
                            titulo = linhaTempo;

                            if (campanha.enviado_facebook == true)
                                campanha.lido_facebook = true;
                            else
                                campanha.lido_email = true;

                            db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                            {
                                acontecimento = linhaTempo,
                                data = DateTime.Now,
                                id_campanha_contatos_enviados = campanha.id
                            });
                        }

                        if (campanha.CLIENTE != null)
                            new ContactManager(id_empresa).AddTimeLine(campanha.CLIENTE.email, "Proposta lida", string.Format("{0} em {1}", titulo, DateTime.Today), TimeLineIcon.Info);



                        campanha.data_lido = DateTime.Now;
                        campanha.id_cliente = this.id_cliente;
                        campanha.lido_com_device = request.UserAgent;
                        campanha.navegador_nome = request.Browser.Browser;
                        campanha.navegador_versao = request.Browser.Version;
                        campanha.sistema_operacional = request.Browser.Platform;
                        campanha.quantidade_cliques++;

                        db.SaveChanges();

                        return ReadEmailStatus.ExistAndSave;
                    }
                    catch (Exception e)
                    {
                        return ReadEmailStatus.Error;
                    }
                }
                return ReadEmailStatus.NotExist;
            }
        }

        public class IPInfo
        {
            [JsonProperty("ip")]
            public string ip { get; set; }


            [JsonProperty("loc")]
            public string loc { get; set; }


            [JsonProperty("hostname")]
            public string hostname { get; set; }


            [JsonProperty("username")]
            public string username { get; set; }


            [JsonProperty("city")]
            public string city { get; set; }


            [JsonProperty("region")]
            public string region { get; set; }


            [JsonProperty("org")]
            public string org { get; set; }
        }

        private String TemplateEmailEmpresa(string autenticacao_email, string mensagem, string nome_cliente, EmailContent emailContent)
        {
            EMPRESA e = this.empresa;
            String template = new WebClient().DownloadString("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Templates/Email_Empresa.html");

            template = template.Replace("__LOGO_EMPRESA__", e.nome.Replace(" ", ""));
            template = template.Replace("__NOME_EMPRESA__", e.nome);
            //if (e.local.FirstOrDefault(f => f.plataforma.Equals("Site")) != null)
            //    template = template.Replace("__URL_EMPRESA__", e.EMPRESA_SOCIAL.FirstOrDefault(f => f.plataforma.Equals("Site")).url ?? "");
            //else
            //    template = template.Replace("__URL_EMPRESA__", "");

            //template = template.Replace("__TITULO__", titulo);
            template = template.Replace("__HEADER__", getHeaderEMail(emailContent.emailContentHeader));
            template = template.Replace("__MENSAGEM__", mensagem);

            if (emailContent != null)
            {
                template = template.Replace("__CONTENT__", string.Format("<a href=\"{0}\">{1}</a>", UtilsManager.EncurtarURL(emailContent.url), this.getButtonEmail(emailContent.emailContentButton)));
            }
            return template;
        }

        private string getHeaderEMail(EmailContentHeader header)
        {
            if (this.auxNomeHeader != null)
                return string.Format("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Header" + header.ToString() + this.auxNomeHeader + ".png");
            else
                return string.Format("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Header" + header.ToString() + ".png");
        }

        private string getButtonEmail(EmailContentButton button)
        {
            return string.Format("<img src=\"{0}\" style=\" width: 500px; \">", "http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Button" + button.ToString() + ".png");
        }

        private String TemplateEmailBuffetWeb(string autenticacao_email, string titulo, string mensagem, string nome_cliente, EmailContent emailContent)
        {
            EMPRESA e = this.empresa;
            String template = new WebClient().DownloadString("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Templates/BuffetWeb.html");

            //template = template.Replace("__TELEFONE_EMPRESA__", e.telefone); 
            template = template.Replace("__AUTENTICACAO_EMAIL__", autenticacao_email);
            template = template.Replace("__AUTENTICACAO_EMPRESA__", e.codigo_empresa);
            template = template.Replace("__NOME_CLIENTE__", nome_cliente);
            template = template.Replace("__TITULO__", titulo);
            template = template.Replace("__MENSAGEM__", mensagem);
            return template;
        }

        private EmailSettings emailSettings
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_CONFIGURACAO config = db.EMPRESA_CONFIGURACAO.FirstOrDefault(f => f.id_empresa == this.id_empresa);
                    if (config != null)
                    {
                        if (config.email_host != null && config.email_login != null && config.email_porta != null && config.email_senha != null)
                        {
                            return new EmailSettings()
                            {
                                email = config.email_login,
                                senha = config.email_senha,
                                servidor = config.email_host,
                                porta = Convert.ToInt32(config.email_porta),
                            };
                        }
                    }
                    List<B_EMPRESA_AJUSTES> ajustes = db.B_EMPRESA_AJUSTES.ToList();
                    return new EmailSettings()
                    {
                        email = ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_LOGIN")).valor,
                        senha = ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_SENHA")).valor,
                        servidor = ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_HOST")).valor,
                        porta = Convert.ToInt32(ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_PORTA")).valor),
                    };
                }
            }
        }
        #endregion
        #region FACEBOOK
        public bool SendFacebookNotificacao(string autenticacao, string id_facebook, string mensagem, string url, TipoContato tipoContato)
        {
            try
            {
                var fb = new FacebookClient();
                dynamic result = fb.Get("oauth/access_token", new
                {
                    client_id = "527135120709685",
                    client_secret = "b8a86a3e74a0b976a9d9e4413a13c11a",
                    grant_type = "client_credentials"
                });
                fb.AccessToken = result.access_token;

                dynamic nparams = new ExpandoObject();
                nparams.template = mensagem;

                var postResult = fb.Post(id_facebook + "/notifications", nparams);

                using (BuffetContext db = new BuffetContext())
                {
                    var campanha = db.CAMPANHA_CONTATOS_ENVIADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.tipo_contato.Equals(tipoContato.ToString()) && e.id_empresa == this.id_empresa);
                    if (campanha == null)
                    {
                        var c = new CAMPANHA_CONTATOS_ENVIADOS()
                        {
                            autenticacao = autenticacao,
                            data_envio = DateTime.Now,
                            id_empresa = this.id_empresa,
                            mensagem = mensagem,
                            titulo = "Facebook",
                            id_cliente = this.id_cliente,
                            enviado_facebook = true,
                            lido_facebook = false,
                            tipo_contato = tipoContato.ToString()
                        };
                        db.CAMPANHA_CONTATOS_ENVIADOS.Add(c);
                        db.SaveChanges();

                        db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                        {
                            acontecimento = "Notificação enviada",
                            data = DateTime.Now,
                            id_campanha_contatos_enviados = c.id
                        });
                        db.SaveChanges();
                    }
                    else
                    {
                        campanha.lido_facebook = false;
                        campanha.enviado_facebook = true;
                        db.CAMPANHA_LINHA_TEMPO.Add(new CAMPANHA_LINHA_TEMPO()
                        {
                            acontecimento = "Notificação enviado",
                            data = DateTime.Now,
                            id_campanha_contatos_enviados = campanha.id
                        });
                        db.SaveChanges();
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        #endregion
        #region NOTIFICATIONS
        public bool SendNotificationFromSystem(string title, string message, ContentNotification content, NotificationRule notificationRule)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<COLABORADORE> colaboradores = this.colaboradores(notificationRule);
                    foreach (COLABORADORE colaborador in colaboradores)
                    {
                        NOTIFICACO n = new NOTIFICACO();
                        n.id_empresa = this.id_empresa;
                        n.mensagem = message;
                        n.data = DateTime.Now;
                        n.para_id_colaborador = colaborador.id;
                        n.deBuffetWeb = true;
                        db.NOTIFICACOES.Add(n);
                        db.SaveChanges();

                        if (colaborador.COLABORADOR_NOTIFICACAO.notificacao_email)
                        {
                            this.SendEmail(colaborador.email, title, message, TipoContato.AgendamentoDeVisita, "", new EmailContent()
                            {
                                emailContentButton = EmailContentButton.Visualizar,
                                emailContentHeader = EmailContentHeader.AgendamentoDeVisita,
                                emailTemplate = TemplateEmail.TemplateBuffetWeb,
                                nomeOla = colaborador.nome,
                                url = string.Format("http://www.web.buffetweb.com/{0}/{1}?{2}", content.controller, content.action, content.parameters)
                            });
                        }
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private List<COLABORADORE> colaboradores(NotificationRule rule)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<COLABORADORE> colaboradores = db.COLABORADORES.Include(e => e.COLABORADOR_NOTIFICACAO).Where(e => e.id_empresa == this.id_empresa).ToList();

                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.GetType().GetProperty(rule.ToString()).GetValue(colaboradores, null).ToString() == "true").ToList();


                switch (rule)
                {
                    case NotificationRule.orcamento_agendar_visita:
                        {
                            if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_gendar_visita == true).ToList();
                            }
                        }
                        break;
                    case NotificationRule.entrar_em_contato:
                        {
                            return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.notificacao_email == true).ToList();
                        }
                        break;
                    case NotificationRule.notificarNovoOrcamento:
                        {
                            if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_novo_pedido == true).ToList();
                            }
                        }
                        break;
                    case NotificationRule.notificarAgendarVisita:
                        {
                            if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_gendar_visita == true).ToList();
                            }
                        }
                        break;
                    case NotificationRule.notificarNotificacaoEmail:
                        {
                            return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.notificacao_email == true).ToList();
                        };
                    case NotificationRule.notificarNovoPedido:
                        {
                            if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_novo_pedido == true).ToList();
                            }
                        }
                        break;
                    case NotificationRule.notificarPesquisaConcluida:
                        {
                            if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.pesquisa_concluida == true).ToList();
                            }
                        }
                        break;
                }
            }
            return new List<COLABORADORE>();
        }
        #endregion

        #region TIMELINE
        public bool AddTimeLine(string email, string titulo, string descricao, TimeLineIcon icone)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.CLIENTE_TIMELINE.Add(new CLIENTE_TIMELINE()
                    {
                        data = DateTime.Now,
                        descricao = descricao,
                        email = email,
                        titulo = titulo,
                        icone = icone.ToString(),
                        id_empresa = this.id_empresa,
                    });
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
                throw;
            }
        }

        #endregion

        public class SolicitarCotacao
        {
            public string autenticacao { get; set; }
            public PORTAL_CLIENTE_PERFIL cliente { get; set; }
            public string nome { get; set; }
            public string email { get; set; }
            public string telefone { get; set; }
            public string tipoProposta { get; set; }
            public int quantidadeConvidados { get; set; }
            public DateTime dataFesta { get; set; }
        }
    }
}
