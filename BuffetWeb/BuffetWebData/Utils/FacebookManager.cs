﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using BuffetWebData.Models;
using Facebook;
using System.Dynamic;

namespace BuffetWebData.Utils
{
    public class FacebookManager
    {
        public LOCAL_EVENTO localEvento { get; set; }

        private List<PORTAL_CLIENTE_PERFIL> ClientesRegiao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PORTAL_CLIENTE_PERFIL.Where(e => e.regiao.Equals(localEvento.regiao)).ToList();
                }
            }
        }

        public FacebookManager(LOCAL_EVENTO local_evento)
        {
            this.localEvento = local_evento;
        }

        public void NotificarPromocao(B_PORTAL_PROMOCAO promocao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var favoritos = this.ClientesRegiao;
                foreach (var cliente in favoritos)
                {
                    string mensagem = string.Format("Olá {0} você acaba de receber R$ {1} reais de desconto do Buffet {2}! Clique para saber mais!", cliente.nome.Split(' ')[0], promocao.desconto_de, localEvento.nome);
                    string autenticacao = UtilsManager.CriarAutenticacao();
                    string url = string.Format("/Parceiros/Parceiros?parceiro={0}&autenticacao={1}", localEvento.autenticacao, autenticacao);

                    new ContactManager(localEvento.id_empresa).SendFacebookNotificacao(autenticacao, cliente.id_facebook, mensagem, url, TipoContato.PortalPromocao);
                }
            }
        }
    }
}
