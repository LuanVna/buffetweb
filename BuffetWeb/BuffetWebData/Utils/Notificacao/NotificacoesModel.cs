﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuffetWebData.Utils
{
    public class NotificacoesModel
    {
        public string icone { get; set; }
        public string titulo { get; set; }
        public string mensagem { get; set; }
        public string url { get; set; }
        public string modulo { get; set; }
        public bool isOk { get; set; }
        public string cor { get; set; }
        public bool onlyAdm { get; set; }
    }
}
