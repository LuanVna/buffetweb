﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebData.Utils
{
    public enum NotificacaoEnum
    {
        Aleatoria,
        SMS,
        EnderecoLocalEvento,
        PortalComentarios,
        //Proposta,
        Pesquisa,
        Convite,
        Portal,
        CupomPlanos,
        PortalPromocao,
        CadastrarLocalEvento
    }

    public class NotificacoesSistema
    {
        public int id_empresa { get; set; }

        public NotificacoesSistema(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public NotificacoesModel VerificarDisponiveis()
        {
            UtilsEmpresa empresa = new UtilsEmpresa(this.id_empresa);
            do
            {
                NotificacaoEnum not = this.BuscarAleatoriamente();
                switch (not)
                {
                    case NotificacaoEnum.SMS:
                        {
                            Tuple<bool, string, string> tuple = this.VerificaSMS();
                            return new NotificacoesModel()
                            {
                                isOk = tuple.Item1,
                                icone = "SMS",
                                titulo = tuple.Item2,
                                mensagem = tuple.Item3,
                                url = "/Planos/SMS",
                                cor = "#65cea7",
                                onlyAdm = true,
                                modulo = "Orcamento;Convite;"
                            };
                        }
                    case NotificacaoEnum.EnderecoLocalEvento:
                        {
                            if (empresa.MODULO_PORTAL.Item1)
                            {
                                Tuple<bool, string, string> tuple = this.VerificarPesquisa();
                                return new NotificacoesModel()
                                {
                                    isOk = tuple.Item1,
                                    icone = "DadosIncompletos",
                                    titulo = tuple.Item2,
                                    mensagem = tuple.Item3,
                                    url = "/Ajustes/LocalEvento",
                                    cor = "rgb(25, 89, 95)",
                                    onlyAdm = true,
                                    modulo = "Portal"
                                };
                            }
                        }; break;

                    case NotificacaoEnum.PortalComentarios:
                        {
                            if (empresa.MODULO_PORTAL.Item1)
                            {
                                Tuple<bool, string, string, LOCAL_EVENTO> tuple = this.VerificaPortalComentarios();
                                return new NotificacoesModel()
                                {
                                    isOk = tuple.Item1,
                                    icone = "Comentarios",
                                    titulo = tuple.Item2,
                                    mensagem = tuple.Item3,
                                    url = "/Portal/LocalDoEvento?autenticacao=" + tuple.Item4.autenticacao,
                                    cor = "#E2E2E4",
                                    modulo = "Portal"
                                };
                            }
                        }; break;
                    case NotificacaoEnum.Pesquisa:
                        {
                            if (empresa.MODULO_CONVITES.Item1)
                            {
                                Tuple<bool, string, string> tuple = this.VerificarPesquisa();
                                return new NotificacoesModel()
                                {
                                    isOk = tuple.Item1,
                                    icone = "Pesquisa",
                                    titulo = tuple.Item2,
                                    mensagem = tuple.Item3,
                                    url = "/Pesquisa/Pesquisa",
                                    cor = "rgb(189, 155, 75)",
                                    modulo = ""
                                };
                            }
                        }; break;
                    case NotificacaoEnum.Convite:
                        break;
                    case NotificacaoEnum.Portal:
                        break;

                    case NotificacaoEnum.PortalPromocao:
                        {
                            Tuple<bool, string, string, int> tuple = this.VerificarPortalPromocao();
                            return new NotificacoesModel()
                            {
                                isOk = tuple.Item1,
                                icone = "PortalPromocao",
                                titulo = tuple.Item2,
                                mensagem = tuple.Item3,
                                url = "/Planos/PortalPromocao?id=" + tuple.Item4.ToString(),
                                cor = "#7057A2",
                                modulo = ";",
                                onlyAdm = true
                            };
                        };
                    case NotificacaoEnum.CupomPlanos:
                        {
                            Tuple<bool, string, string> tuple = this.VerificarCupomCarrinho();
                            return new NotificacoesModel()
                            {
                                isOk = tuple.Item1,
                                icone = "CupomCarrinho",
                                titulo = tuple.Item2,
                                mensagem = tuple.Item3,
                                url = "/Planos/MeuCarrinho",
                                cor = "rgb(31, 222, 0)",
                                modulo = ";",
                                onlyAdm = true
                            };
                        };
                    case NotificacaoEnum.CadastrarLocalEvento:
                        {
                            Tuple<bool, string, string> tuple = this.VerificaCadastrarLocalEvento();
                            return new NotificacoesModel()
                            {
                                isOk = tuple.Item1,
                                icone = "DadosIncompletos",
                                titulo = tuple.Item2,
                                mensagem = tuple.Item3,
                                url = "/Ajustes/LocalEvento",
                                cor = "rgb(146, 5, 5)",
                                modulo = ";",
                                onlyAdm = true
                            };
                        };
                    default:
                        break;
                }
            } while (true);
        }

        private Tuple<bool, string, string, int> VerificarPortalPromocao()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var promocao = db.B_PORTAL_PROMOCAO.FirstOrDefault(e => DateTime.Today >= e.disponivel_de && DateTime.Today <= e.disponivel_ate);
                if (promocao != null)
                {
                    return new Tuple<bool, string, string, int>(true, promocao.nome, "Participe desta promoção e aumente seu rendimento mensal!", promocao.id);
                }
                return new Tuple<bool, string, string, int>(false, "", "", -1);
            }
        }

        private Tuple<bool, string, string> VerificarCupomCarrinho()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var cupom = db.EMPRESA_CARRINHO_CUPOM.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.usado == false && DateTime.Today <= e.disponivel_ate);
                if (cupom != null)
                {
                    return new Tuple<bool, string, string>(true, "Cupom de Desconto", string.Format("Você tem um cupom de desconto no valor de R$ {0} disponível {1}, use agora e aproveite todos os benefícios do BuffetWeb!", cupom.valor_desconto.ToString().Replace(".", ","), string.Format("{0}", (cupom.disponivel_ate - DateTime.Today).Days == 0 ? "que expira Hoje" : "até " + cupom.disponivel_ate.ToShortDateString())));
                }
                return new Tuple<bool, string, string>(false, "", "");
            }
        }

        private Tuple<bool, string, string, LOCAL_EVENTO> VerificaPortalComentarios()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var comentarios = db.LOCAL_EVENTO_COMENTARIOS.Include(l => l.LOCAL_EVENTO).Where(e => e.id_empresa == this.id_empresa && e.analisado == false).ToList();
                if (comentarios.Count > 0)
                {
                    return new Tuple<bool, string, string, LOCAL_EVENTO>(true, "Comentários", string.Format("Existe{3} {0} comentário{1} para ser aprovado{1} no {2}", comentarios.Count, comentarios.Count > 1 ? "s" : "", comentarios.FirstOrDefault().LOCAL_EVENTO.nome, comentarios.Count > 1 ? "m" : ""), comentarios.FirstOrDefault().LOCAL_EVENTO);
                }
                return new Tuple<bool, string, string, LOCAL_EVENTO>(false, "", "", null);
            }
        }

        public Tuple<bool, string, string> VerificarPesquisa()
        {
            using (BuffetContext db = new BuffetContext())
            {
                DateTime today = DateTime.Today.AddDays(5);
                ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.data_evento < today);
                if (orcamento != null)
                {
                    return new Tuple<bool, string, string>(true, "Pesquisa de Satisfação", "Crie pesquisas de satisfação e consiga os dados dos convidados do seus próximos eventos!");
                }
            }
            return new Tuple<bool, string, string>(false, "", "");
        }

        public Tuple<bool, string, string> VerificarDadosEnderecoLocal()
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<LOCAL_EVENTO> locais = db.LOCAL_EVENTO.Include(d => d.ENDERECO).Where(e => e.id_empresa == this.id_empresa).ToList();
                foreach (var local in locais)
                {
                    var ende = local.ENDERECO;
                    if (ende != null)
                    {
                        if (string.IsNullOrEmpty(ende.cep) ||
                        string.IsNullOrEmpty(ende.endereco1) ||
                        string.IsNullOrEmpty(ende.bairro) ||
                        string.IsNullOrEmpty(ende.cidade) ||
                        string.IsNullOrEmpty(ende.estado) ||
                        string.IsNullOrEmpty(ende.numero))
                        {
                            return new Tuple<bool, string, string>(true, "Alguem pode se perder", string.Format("Verifique se o endereço do local de evento {0} esta completo!", local.nome));
                        }
                    }
                    return new Tuple<bool, string, string>(true, "Ninguém encontra o local da festa", string.Format("O local de evento {0} não tem nenhum endereço cadastrado!", local.nome));
                }
            }
            return new Tuple<bool, string, string>(false, "", "");
        }

        private Tuple<bool, string, string> VerificaSMS()
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA_PACOTE_SMS pacote = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.quantidade_disponivel > 0);
                if (pacote == null)
                {
                    return new Tuple<bool, string, string>(true, "Envie SMS", "Você sabia que 90% dos SMS enviados são lidos? <br />Vejá nossos pacotes promocionais de SMS e aumente o engajamento de seus clientes.");
                }
                else if (pacote.quantidade_disponivel < 50)
                {
                    return new Tuple<bool, string, string>(true, "Envie SMS", string.Format("Você só tem {0} sms disponível para envio, que tal dar uma olhada em nossos pacotes promocionais?", pacote.quantidade_disponivel));
                }
                return new Tuple<bool, string, string>(false, "", "");
            }
        }

        private Tuple<bool, string, string> VerificaCadastrarLocalEvento()
        {
            using (BuffetContext db = new BuffetContext())
            {
                int quantidadeLocais = db.LOCAL_EVENTO.Where(e => e.id_empresa == id_empresa).ToList().Count;
                if (quantidadeLocais != 0)
                {
                    return new Tuple<bool, string, string>(true, "Local do evento", "Você precisa cadastrar um local de evento para utilizar a maioria das funcionalidades!");
                }
            }
            return new Tuple<bool, string, string>(false, "", "");
        }


        private NotificacaoEnum BuscarAleatoriamente()
        {
            NotificacaoEnum[] NotificacaoAleatoria = new NotificacaoEnum[]{
                NotificacaoEnum.Pesquisa,
                //NotificacaoEnum.PortalComentarios,
                //NotificacaoEnum.Proposta,
                NotificacaoEnum.SMS,
                NotificacaoEnum.EnderecoLocalEvento,
                NotificacaoEnum.CupomPlanos,
                NotificacaoEnum.PortalPromocao,
                NotificacaoEnum.CadastrarLocalEvento
            };

            int random = new Random().Next(0, NotificacaoAleatoria.Length);
            return NotificacaoAleatoria[random];
        }
    }
}
