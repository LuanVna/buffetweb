﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Net;

namespace BuffetWebData.Utils
{
    public class UtilsEmpresa
    {
        public int id_empresa { get; set; }
        public UtilsEmpresa(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public UtilsEmpresa() { }

        public EMPRESA CriarEmpresa(string cnpj)
        {
            var jsonResponse = new WebClient().DownloadString(string.Format("http://www.graph.buffetweb.com.br/BuffetWebGraph.asmx?AddEmpresa={0}", cnpj));

            return null;
        }

        private EMPRESA empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.Include(c => c.EMPRESA_CONFIGURACAO).FirstOrDefault(e => e.id == this.id_empresa);
                }
            }
        }

        public string NOME_EMPRESA { get { return empresa.nome; } }
        public string NOME_EMPRESA_FTP { get { return string.Format("{0}_{1}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa); } }

        public Tuple<bool, int> MODULO_CONVITES
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.modulo.Equals("CONVITE"));
                    return new Tuple<bool, int>((DateTime.Now.Date <= orcamento.expira_em.Date), (DateTime.Now.Date - orcamento.expira_em.Date).Days);
                }
            }
        }

        public Tuple<bool, int> MODULO_ORCAMENTO
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.modulo.Equals("ORCAMENTO"));
                    return new Tuple<bool, int>((DateTime.Now.Date <= orcamento.expira_em.Date), (DateTime.Now.Date - orcamento.expira_em.Date).Days);
                }
            }
        }

        public Tuple<bool, int> MODULO_FINANCEIRO
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS financeiro = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.modulo.Equals("FINANCEIRO"));
                    return new Tuple<bool, int>((DateTime.Now.Date <= financeiro.expira_em.Date), (DateTime.Now.Date - financeiro.expira_em.Date).Days);
                }
            }
        }

        public Tuple<bool, int> MODULO_ESTOQUE
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS estoque = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.modulo.Equals("ESTOQUE"));
                    return new Tuple<bool, int>((DateTime.Now.Date <= estoque.expira_em.Date), (DateTime.Now.Date - estoque.expira_em.Date).Days);
                }
            }
        }

        public Tuple<bool, int> MODULO_PORTAL
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS portal = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.modulo.Equals("PORTAL"));
                    return new Tuple<bool, int>((DateTime.Now.Date <= portal.expira_em.Date), (DateTime.Now.Date - portal.expira_em.Date).Days);
                }
            }
        }
    }
}