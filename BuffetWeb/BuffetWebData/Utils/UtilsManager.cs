﻿using HumanAPIClient.Model;
using HumanAPIClient.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuffetWebData.Models;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Configuration;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web;

namespace BuffetWebData.Utils
{
    public enum Notificar
    {
        notificarNovoOrcamento,
        notificarAgendarVisita,
        notificarNovoPedido,
        notificarNotificacaoEmail,
        notificarPesquisaConcluida,
        notificarBuffetWeb
    }

    public class UtilsManager
    {
        public const string URL_PORTAL = "http://www.portal2.buffetweb.com";
        public const string URL_WEB = "http://www.web.buffetweb.com";
        public const string URL_MASTER = "http://www.master.buffetweb.com";

        public string mensagemNotificacao { get; set; }

        public UtilsManager(int? id_empresa = null)
        {
            if (id_empresa != null)
            {
                this.id_empresa = (int)id_empresa;
            }
        }

        public static void SaveCookie(HttpContextBase context, string key, string value)
        {
            HttpCookie cookie = new HttpCookie(key, value);
            cookie.Expires = DateTime.Now.AddMonths(1);
            context.Response.SetCookie(cookie);
        }

        public static string GetCookie(HttpContextBase context, string key)
        {
            return context.Request.Cookies.Get(key).Value;
        }

        public static bool AutenticarEmpresa(HttpContextBase context, string codigo_empresa)
        {
            try
            {
                HttpCookie cookie = new HttpCookie("KEY_EMPRESA", codigo_empresa);
                cookie.Expires = DateTime.Now.AddMonths(1);
                context.Response.SetCookie(cookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public EMPRESA empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                }
            }
        }

        public EMPRESA_CONFIGURACAO configuracao()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.EMPRESA_CONFIGURACAO.FirstOrDefault(e => e.id_empresa == this.id_empresa);
            }
        }

        public int id_empresa { get; set; }
        public string codigo_unico { get; set; }
        public static Tuple<bool, string> GetOperadora(string telefone)
        {
            string url = string.Format("http://portabilidadecelular.com/painel/consulta_numero.php?user=recargalucrativa&pass=6BdIpMBP&seache_number={0}", telefone.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", ""));
            string contents = new System.Net.WebClient().DownloadString(url);
            int codigo = 0;
            if (int.TryParse(contents, out codigo))
            {
                string nome_operadora = "";
                switch (codigo)
                {
                    case 55351: nome_operadora = "nextel"; break;
                    case 55341: nome_operadora = "tim"; break;
                    case 55331: nome_operadora = "oi"; break;
                    case 55323: nome_operadora = "vivo"; break;
                    case 55321: nome_operadora = "claro"; break;
                    case 55320: nome_operadora = "vivo"; break;
                    case 55314: nome_operadora = "oi"; break;
                    default: return new Tuple<bool, string>(true, "invalido");
                }
                return new Tuple<bool, string>(true, nome_operadora);
            }
            return new Tuple<bool, string>(false, "");
        }

        public string GerarCodigoUnico()
        {
            using (BuffetContext db = new BuffetContext())
            {
                do
                {
                    string codigo_unico = new Random().Next(0, 100000000).ToString();
                    if (codigo_unico.Length == 8)
                    {
                        if (db.ORCAMENTOes.FirstOrDefault(i => i.codigo_unico.Equals(codigo_unico)) == null)
                        {
                            return codigo_unico;
                        }
                    }
                } while (true);
            }
        }

        public static string EncurtarURL(string url)
        {
            WebClient request = new WebClient();
            string encurtar = string.Format("https://api-ssl.bitly.com/v3/shorten?access_token=19ca38596f4baeefb12a9fbdfe7f8321da023765&longUrl={0}&format=json", url);
            dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(request.DownloadString(encurtar));
            if (json.status_code == 200)
            {
                return json.data.url;
            }
            return url;
        }

        public static string CriarAutenticacao()
        {
            string autenticacao = "";
            for (int i = 0; i < 3; i++)
            {
                autenticacao += RLSecurity.Security.CriptographyOn(new Random().Next(1, 99999999).ToString()).Replace("+", "").Replace("==", "").Replace("/", "");
            }
            return autenticacao;
        }

        public static string CriarAutenticacaoEmpresa()
        {
            using (BuffetContext db = new BuffetContext())
            {
                do
                {
                    string codigo = "";
                    for (int i = 0; i < 3; i++)
                    {
                        codigo += CriarAutenticacao();
                    }
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(f => f.codigo_empresa.Equals(codigo));
                    if (empresa == null)
                    {
                        return codigo;
                    }
                } while (true);
            }
        }

        public static string gerarCodigoOrcamento()
        {
            using (BuffetContext db = new BuffetContext())
            {
                do
                {
                    string codigo = new Random().Next(0, 99999999).ToString();
                    if (codigo.Length == 8)
                    {
                        ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo));
                        if (orcamento == null)
                        {
                            return codigo;
                        }
                    }
                } while (true);
            }
        }

        public static bool sendSMS(string telefone, string mensagem)
        {
            try
            {
                SimpleSending cliente = new SimpleSending("recargalucrativa", "7rNEE227EQ");
                SimpleMessage sms = new SimpleMessage();
                sms.To = string.Format("55{0}", telefone.Replace("-", "").Replace("(", "").Replace(")", ""));
                sms.Message = mensagem;
                List<string> respostaSMS = cliente.send(sms);
                return respostaSMS.Count != 0 && respostaSMS[0].Equals("000 - Message Sent");
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool SendEmailBuffetweb(COLABORADORE colaborador, string titulo, string mensagem, string email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                db.CONTATO_PLATAFORMA.Add(new CONTATO_PLATAFORMA()
                {
                    autenticacao = UtilsManager.CriarAutenticacao(),
                    email = email,
                    data_envio = DateTime.Now,
                    id_colaborador = colaborador.id,
                    id_empresa = this.id_empresa,
                    mensagem_envio = mensagem,
                    titulo = titulo,
                    tipo_contato = "EMAIL"
                });
                db.SaveChanges();

            }
            return true;
        }

        public bool SendSMSBuffetweb(COLABORADORE colaborador, string titulo, string mensagem, string telefone)
        {
            using (BuffetContext db = new BuffetContext())
            {
                db.CONTATO_PLATAFORMA.Add(new CONTATO_PLATAFORMA()
                {
                    autenticacao = UtilsManager.CriarAutenticacao(),
                    telefone = telefone,
                    data_envio = DateTime.Now,
                    id_colaborador = colaborador.id,
                    id_empresa = this.id_empresa,
                    mensagem_envio = mensagem,
                    titulo = titulo,
                    tipo_contato = "SMS"
                });
                db.SaveChanges();

            }
            return true;
        }

        public bool sendEmail(string titulo, string html, string email)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                    if (empresa != null)
                    {
                        MailMessage mensagemEmail = new MailMessage(email, email, string.Format("{0} - {1}", empresa.nome, titulo), html);
                        mensagemEmail.IsBodyHtml = true;

                        Tuple<string, string, string, string> c_email = this.config_email;

                        SmtpClient client = new SmtpClient(c_email.Item1, Convert.ToInt32(c_email.Item3));
                        client.EnableSsl = true;
                        client.Credentials = new NetworkCredential(c_email.Item2, c_email.Item4);
                        client.Send(mensagemEmail);
                        return true;
                    }
                    new Exception("Você precisa informar o ID_EMPRESA");
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Tuple<string, string, string, string> config_email
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_CONFIGURACAO config = db.EMPRESA_CONFIGURACAO.FirstOrDefault(f => f.id_empresa == this.id_empresa);
                    if (config != null)
                    {
                        if (config.email_host != null && config.email_login != null && config.email_porta != null && config.email_senha != null)
                        {
                            return new Tuple<string, string, string, string>(config.email_host, config.email_login, config.email_porta, config.email_senha);
                            //config.email_host;
                        }
                        else
                        {
                            List<B_EMPRESA_AJUSTES> ajustes = db.B_EMPRESA_AJUSTES.ToList();
                            return new Tuple<string, string, string, string>(
                                    ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_HOST")).valor,
                                    ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_LOGIN")).valor,
                                    ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_PORTA")).valor,
                                    ajustes.FirstOrDefault(e => e.chave.Equals("EMAIL_SENHA")).valor);
                        }
                    }
                    return new Tuple<string, string, string, string>("", "", "", "");
                }
            }
        }

        public static string urlConvite(string autenticacao)
        {
            return EncurtarURL("http://www.convite.buffetweb.com/Convidados/ResponderConvite?autenticacao=" + autenticacao);
        }

        public static string urlOrcamentoSimplificado(string codigo_unico)
        {
            return EncurtarURL("http://www.convite.buffetweb.com/OrcamentoSimplificado/VisualizarOrcamento?codigo_unico=" + codigo_unico);
        }

        public string CSSFontes()
        {
            string css = "";
            const string quote = "\"";
            using (BuffetContext db = new BuffetContext())
            {
                List<B_CONVITES_FONTES> fontes = db.B_CONVITES_FONTES.ToList();
                foreach (var fonte in fontes)
                {
                    css += "@font-face {font-family:" + quote + fonte.nome_fonte + quote + ";" +
                                           " font-style: normal; font-weight: normal;" +
                                           " src: url(" + quote + fonte.caminho_fonte + quote + "); " +
                                           " format(" + quote + "truetype" + quote + ");}";
                }
                return css;
            }
        }

        public void sendNotificacao(Notificar notificar, int? de_colaborador = null)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<COLABORADORE> colaboradores = this.colaboradores(notificar);
                foreach (COLABORADORE colaborador in colaboradores)
                {
                    NOTIFICACO n = new NOTIFICACO();
                    n.id_empresa = this.id_empresa;
                    n.mensagem = this.mensagem(notificar);
                    n.data = DateTime.Now;
                    n.para_id_colaborador = colaborador.id;
                    if (de_colaborador == null)
                    {
                        n.deBuffetWeb = true;
                    }
                    else
                    {
                        n.de_id_colaborador = de_colaborador;
                    }
                    db.NOTIFICACOES.Add(n);
                    db.SaveChanges();

                    if (colaborador.COLABORADOR_NOTIFICACAO.notificacao_email)
                    {
                        COLABORADORE col = db.COLABORADORES.FirstOrDefault(e => e.id == de_colaborador && e.id_empresa == this.id_empresa);
                        string usuario = "";
                        string imagem = "";
                        if (de_colaborador == null)
                            usuario = "BuffetWeb";
                        else
                            usuario = col.nome;

                        if (de_colaborador == null)
                            imagem = "http://buffetweb.com/Empresas/Arquivos/Logos/Logo_Small.png";
                        else
                            imagem = col.url == null ? "" : col.url;

                        this.sendEmail("BuffetWeb - Você tem uma nova mensagem", this.CreateMensagem(imagem, usuario, n.mensagem), colaborador.email);
                    }
                }
            }
        }

        public bool sendNotificacao(NOTIFICACO notificacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (notificacao.paraBuffetWeb == true)
                {
                    notificacao.id_empresa = this.id_empresa;
                    db.NOTIFICACOES.Add(notificacao);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    COLABORADORE colaborador = db.COLABORADORES.Include(n => n.COLABORADOR_NOTIFICACAO).FirstOrDefault(c => c.id == notificacao.para_id_colaborador && c.id_empresa == this.id_empresa);
                    if (colaborador != null)
                    {
                        notificacao.id_empresa = this.id_empresa;
                        db.NOTIFICACOES.Add(notificacao);
                        db.SaveChanges();

                        if (colaborador.COLABORADOR_NOTIFICACAO.notificacao_email)
                        {
                            string usuario = "";
                            string imagem = "";
                            if (notificacao.deBuffetWeb)
                                usuario = "BuffetWeb";
                            else
                                usuario = colaborador.nome;

                            if (notificacao.deBuffetWeb)
                                imagem = "http://buffetweb.com/Empresas/Arquivos/Logos/Logo_Small.png";
                            else
                                imagem = colaborador.url == null ? "" : colaborador.url;

                            this.sendEmail("BuffetWeb - Você tem uma nova mensagem", this.CreateMensagem(imagem, usuario, notificacao.mensagem), colaborador.email);
                        }
                    }
                    return true;
                }
            }
        }

        private List<COLABORADORE> colaboradores(Notificar notificar)
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA empresa = db.EMPRESAs.Include(m => m.EMPRESA_MODULOS).FirstOrDefault(e => e.id == this.id_empresa);
                if (empresa != null)
                {
                    List<COLABORADORE> colaboradores = db.COLABORADORES.Include(e => e.COLABORADOR_NOTIFICACAO).Where(e => e.id_empresa == id_empresa).ToList();
                    switch (notificar)
                    {
                        case Notificar.notificarNovoOrcamento:
                            {
                                if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                                {
                                    return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_novo_pedido == true).ToList();
                                }
                            } break;
                        case Notificar.notificarAgendarVisita:
                            {
                                if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                                {
                                    return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_gendar_visita == true).ToList();
                                }
                            } break;
                        case Notificar.notificarNotificacaoEmail:
                            {
                                return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.notificacao_email == true).ToList();
                            };
                        case Notificar.notificarNovoPedido:
                            {
                                if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                                {
                                    return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.orcamento_novo_pedido == true).ToList();
                                }
                            } break;
                        case Notificar.notificarPesquisaConcluida:
                            {
                                if (empresa.EMPRESA_MODULOS.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO")).expira_em.Date >= DateTime.Now.Date)
                                {
                                    return colaboradores.Where(e => e.COLABORADOR_NOTIFICACAO.pesquisa_concluida == true).ToList();
                                }
                            } break;
                    }
                }
            }
            return new List<COLABORADORE>();
        }

        private string mensagem(Notificar notificar)
        {
            switch (notificar)
            {
                case Notificar.notificarNovoOrcamento:
                    {
                        return "Novo Orçamento " + this.codigo_unico;
                    };
                case Notificar.notificarPesquisaConcluida:
                    {
                        if (this.mensagemNotificacao != null)
                        {
                            return this.mensagemNotificacao;
                        }
                        return "Um convidado acaba de responder mais uma pesquisa!";
                    };
            }
            return "";
        }
        private string getConfig(string chave)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals(chave)).valor;
            }
        }

        private string CreateMensagem(string imagem_usuario, string nome_usuario, string mensagem_usuario)
        {
            WebClient request = new WebClient();
            string htmlNotificacao = request.DownloadString("http://buffetweb.com/Empresas/Arquivos/Notificacao/Notificacao1.html");
            htmlNotificacao = htmlNotificacao.Replace("IMAGEM_USUARIO", imagem_usuario);
            htmlNotificacao = htmlNotificacao.Replace("NOME_USUARIO", nome_usuario);
            htmlNotificacao = htmlNotificacao.Replace("MENSAGEM_USUARIO", mensagem_usuario);
            htmlNotificacao = htmlNotificacao.Replace("DOMINIO_BUFFET", "http://www.web.buffetweb.com");

            return htmlNotificacao;
        }

        public string createTemplateEmail(String convite)
        {
            convite = convite.Replace("__FONTES_SELECIONADAS__", this.CSSFontes());
            convite = convite.Replace("__LOGO_EMPRESA__", new FTPManager(this.empresa.id).LogoEmpresa());
            convite = convite.Replace("__DOMINIO_EMPRESA__", DominioEmpresa());
            return convite;
        }


        public string createConviteEmail(String convite, int id_orcamento, CONVITE_CONVIDADOS convidado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamento = db.ORCAMENTOes.Include(tem => tem.CONVITE_TEMPLATE).Include(c => c.CLIENTE.CLIENTE_ANIVERSARIANTES).FirstOrDefault(e => e.id == id_orcamento);
                CONVITE_TEMPLATE template = orcamento.CONVITE_TEMPLATE.FirstOrDefault();
                B_CONVITES_DISPONIVEIS disponivel = db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == template.id_convite);
                List<CONVITE_ANIVERSARIANTES> aniversariantes = db.CONVITE_ANIVERSARIANTES.Include(c => c.CLIENTE_ANIVERSARIANTES).Where(e => e.id_orcamento == orcamento.id).ToList();

                //ANIVERSARIANTE
                string aniversariants = "";
                foreach (var aniversariante in aniversariantes)
                {
                    aniversariants += "<p>" + aniversariante.CLIENTE_ANIVERSARIANTES.nome + "</p>";
                }
                convite = convite.Replace("__TITULO_CONVITE_FONTE__", disponivel.titulo_fonte);
                convite = convite.Replace("__ANIVERSARIANTES__", aniversariants);
                convite = convite.Replace("__FONTES_SELECIONADAS__", this.CSSFontes());
                convite = convite.Replace("__LOGO_EMPRESA__", new FTPManager(this.empresa.id).LogoEmpresa());
                convite = convite.Replace("__DOMINIO_EMPRESA__", DominioEmpresa());
                //string dominio_buffetweb = ConfigurationManager.AppSettings["DOMINIO_BUFFETWEB"].ToString();
                //convite = convite.Replace("__DOMINIO_BUFFETWEB__", dominio_buffetweb);
                convite = convite.Replace("__NOME_CONVIDADO__", convidado.nome_completo);
                convite = convite.Replace("__AUTENTICACAO__", convidado.autenticacao);
                return convite;
            }
        }

        private string createPesquisa(String convite, CONVITE_CONVIDADOS convidado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamento = db.ORCAMENTOes.Include(tem => tem.CONVITE_TEMPLATE).Include(c => c.CLIENTE.CLIENTE_ANIVERSARIANTES).FirstOrDefault(e => e.id == convidado.id_orcamento);
                CONVITE_TEMPLATE template = orcamento.CONVITE_TEMPLATE.FirstOrDefault();
                B_CONVITES_DISPONIVEIS disponivel = db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == template.id_convite);
                List<CONVITE_ANIVERSARIANTES> aniversariantes = db.CONVITE_ANIVERSARIANTES.Include(c => c.CLIENTE_ANIVERSARIANTES).Where(e => e.id_orcamento == convidado.id_orcamento).ToList();

                //ANIVERSARIANTE
                string aniversariants = "";
                foreach (var aniversariante in aniversariantes)
                {
                    aniversariants += "<p>" + aniversariante.CLIENTE_ANIVERSARIANTES.nome + "</p>";
                }
                convite = convite.Replace("__TITULO_CONVITE_FONTE__", disponivel.titulo_fonte);
                convite = convite.Replace("__ANIVERSARIANTES__", aniversariants);
                convite = convite.Replace("__FONTES_SELECIONADAS__", this.CSSFontes());
                convite = convite.Replace("__LOGO_EMPRESA__", new FTPManager(this.empresa.id).LogoEmpresa());
                convite = convite.Replace("__DOMINIO_EMPRESA__", DominioEmpresa());
                convite = convite.Replace("__DOMINIO_BUFFETWEB__", "http://convite.buffetweb.com");// ConfigurationManager.AppSettings["DOMINIO_BUFFETWEB_CONVITE"].ToString());
                convite = convite.Replace("__NOME_CONVIDADO__", convidado.nome_completo);
                convite = convite.Replace("__AUTENTICACAO__", convidado.autenticacao);
                return convite;
            }
        }

        public string DominioEmpresa()
        {
            //using (BuffetContext db = new BuffetContext())
            //{
            //    EMPRESA_SOCIAL social = db.EMPRESA_SOCIAL.FirstOrDefault(e => e.id_empresa == this.id_empresa && e.plataforma.Equals("Site"));
            //    if (social != null)
            //    {
            //        return social.url;
            //    }
            //    return "#";
            //}
            return "https://www.facebook.com/torceretorcefestas?ref=ts&fref=ts";
        }

        public string createConvite(String convite, int id_orcamento, CONVITE_CONVIDADOS convidado)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamento = db.ORCAMENTOes.Include(tem => tem.CONVITE_TEMPLATE).Include(c => c.CLIENTE.CLIENTE_ANIVERSARIANTES).FirstOrDefault(e => e.id == id_orcamento);
                CONVITE_TEMPLATE template = orcamento.CONVITE_TEMPLATE.FirstOrDefault();
                B_CONVITES_DISPONIVEIS disponivel = db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == template.id_convite);
                List<CONVITE_ANIVERSARIANTES> aniversariantes = db.CONVITE_ANIVERSARIANTES.Include(c => c.CLIENTE_ANIVERSARIANTES).Where(e => e.id_orcamento == orcamento.id).ToList();

                if (template != null)
                {

                    //TITULO
                    convite = convite.Replace("__TITULO_CONVITE_FONTE__", disponivel.titulo_fonte);
                    convite = convite.Replace("__TITULO_CONVITE_SIZE__", template.titulo_tamanho + "px");
                    convite = convite.Replace("__TITULO_CONVITE_TOP__", template.titulo_top);
                    convite = convite.Replace("__TITULO_CONVITE_LEFT__", template.titulo_left);
                    convite = convite.Replace("__TITULO_CONVITE_COR__", "#" + disponivel.titulo_cor);
                    convite = convite.Replace("__TITULO__CONVITE__", template.titulo_convite);

                    //DESCRICAO
                    convite = convite.Replace("__DESCRICAO_CONVITE_FONTE__", disponivel.descricao_fonte);
                    convite = convite.Replace("__DESCRICAO_CONVITE_SIZE__", template.tamanho_descricao + "px");
                    convite = convite.Replace("__DESCRICAO_CONVITE_TOP__", template.descricao_top);
                    convite = convite.Replace("__DESCRICAO_CONVITE_LEFT__", template.descricao_left);
                    convite = convite.Replace("__DESCRICAO_CONVITE_COR__", "#" + disponivel.descricao_cor);
                    convite = convite.Replace("__DESCRICAO__CONVITE__", template.titulo_descricao);

                    //ANIVERSARIANTE
                    string aniversariants = "";
                    foreach (var aniversariante in aniversariantes)
                    {
                        aniversariants += "<p>" + aniversariante.CLIENTE_ANIVERSARIANTES.nome + "</p>";
                    }
                    convite = convite.Replace("__ANIVERSARIANTE_CONVITE_FONTE__", disponivel.aniversariantes_fonte);
                    convite = convite.Replace("__ANIVERSARIANTE_CONVITE_SIZE__", template.aniversariante_tamanho + "px");
                    convite = convite.Replace("__ANIVERSARIANTE_CONVITE_TOP__", template.aniversariantes_top);
                    convite = convite.Replace("__ANIVERSARIANTE_CONVITE_LEFT__", template.aniversariantes_left);
                    convite = convite.Replace("__ANIVERSARIANTE_CONVITE_COR__", "#" + disponivel.aniversariantes_cor);
                    convite = convite.Replace("__ANIVERSARIANTE__CONVITE__", aniversariants);

                    //DATAHORA
                    convite = convite.Replace("__DATAHORA_CONVITE_FONTE__", disponivel.datahora_fonte);
                    convite = convite.Replace("__DATAHORA_CONVITE_SIZE__", template.datahora_tamanho + "px");
                    convite = convite.Replace("__DATAHORA_CONVITE_COR__", "#" + disponivel.datahora_cor);
                    convite = convite.Replace("__DATAHORA_CONVITE_TOP__", template.datahora_top);
                    convite = convite.Replace("__DATAHORA_CONVITE_LEFT__", template.datahora_left);
                    convite = convite.Replace("__DATA_HORA_CONVITE__", string.Format("De {0} até {1}", "12h00", "18h00"));


                    //CONFIGURACOES

                    //ENDERECOS
                    LOCAL_EVENTO local = db.LOCAL_EVENTO.Include(en => en.ENDERECO).FirstOrDefault(e => e.id == orcamento.id_local_evento && e.id_empresa == this.id_empresa);
                    if (local != null)
                    {
                        string local_evento = string.Format("{1}, {2}, {3} \n{4}. \nContato {5}", local.ENDERECO.endereco1, local.ENDERECO.numero, local.ENDERECO.bairro, local.ENDERECO.complemento, local.telefone);
                        convite = convite.Replace("__ENDERECO_TEMPLATE_URL__", local_evento);
                    }

                    //ACOES
                    //string dominio_buffetweb = ConfigurationManager.AppSettings["DOMINIO_BUFFETWEB"].ToString();
                    //convite = convite.Replace("_DOMINIO_BUFFETWEB_", dominio_buffetweb);

                    convite = convite.Replace("AUTENTICACAO_EMPRESA", db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa).codigo_empresa);
                    convite = convite.Replace("CODIGO_UNICO_REPLACE", orcamento.codigo_unico);
                    convite = convite.Replace("EMAIL_REPLACE", convidado.email);

                    convite = convite.Replace("__FONTES_SELECIONADAS__", this.CSSFontes());

                    convite = convite.Replace("__TEMPLATE_CONVITE__", template.url_image);
                    convite = convite.Replace("__LOGO_EMPRESA__", new FTPManager(this.empresa.id).LogoEmpresa());
                }
            }

            return convite;
        }


        public bool SendPesquisa(CONVITE_CONVIDADOS convidado, String convite)
        {
            if (convidado.email != null)
            {
                String pesquisa = this.createPesquisa(convite, convidado);
                return this.sendEmail("Diga-nos o que achou da festa", pesquisa, convidado.email);
            }
            else if (convidado.telefone != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA empresa = db.EMPRESAs.Include(f => f.EMPRESA_PACOTE_SMS).Include(c => c.EMPRESA_CONFIGURACAO).FirstOrDefault(e => e.id == this.id_empresa && e.EMPRESA_CONFIGURACAO.FirstOrDefault().permitirSMS);
                    if (empresa != null)
                    {
                        EMPRESA_PACOTE_SMS sms = empresa.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0);
                        if (sms != null)
                        {
                            string url = "http://www.convite.buffetweb.com/Convidados/Pesquisa?autenteicacao=" + convidado.autenticacao;
                            if (sendSMS(convidado.telefone, "Diga-nos o que achou da festa " + EncurtarURL(url)))
                            {
                                sms.quantidade_disponivel--;
                                db.SaveChanges();
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}