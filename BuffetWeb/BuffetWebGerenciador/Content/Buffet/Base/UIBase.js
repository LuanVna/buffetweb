﻿


function LeftAll(evt, from, to) {

}
//var elem = document.querySelector("select");

//console.log(elem.selectedOptions);

var vals = [];
var textvals = [];

function Left(evt, from, to) {
    Transfer(from, to);
}

function Right(evt, from, to) {
    Transfer(from, to);
}

function Transfer(from, to) {
    $('#' + from + ' :selected').each(function (i, selected) {
        vals[i] = $(selected).val();
        textvals[i] = $(selected).text();
    });

    for (var i = 0; i < vals.length; i++) {
        o(to).options[o(to).options.length] = new Option(textvals[i], vals[i]);
        $("#" + from + " option[value=" + vals[i] + "]").remove();
    }
    vals = [];
    textvals = [];
}

function RemoveAll(evt, from, to) {
    var total = o(from).length - 1;
    for (var i = total; i != -1; i--) {
        o(to).options[o(to).options.length] = new Option(o(from).options[i].innerHTML, o(from).options[i].value);
        $("#" + from + " option[value=" + o(from).options[i].value + "]").remove();
    }
}


function MenuState(evt) {
    var menu = o('wrapper');
    if (menu.className == "") {
        setCookie('menu', 'sidebar-hide');
    } else {
        setCookie('menu', "");
    }
}

//function ClassMenu() {
//    var menu = getCookie('menu');
//    if (menu) {
//        return 'sidebar-hide';
//    }
//    return '';
//}

function CheckOrUncheck(evt, id_check) {
    if (evt.dataset.check == "true") {
        evt.innerHTML = '<img src="/Content/Imagens/icones/icone_uncheck.png" width="30">';
        o(id_check).value = false;
        evt.dataset.check = false;
    } else {
        evt.innerHTML = '<img src="/Content/Imagens/icones/icone_check.png" width="30">';
        evt.dataset.check = true;
        o(id_check).value = true;
    }
} 

//CONFIGURACAO DA CLASSE UI
var tempo = 1000; //TEMPO

//MENSAGENS DE ALERTA 
var tipoMensagem = {
    SUCESSO: 0,
    ERRO: 1,
    NONE: 2,
    INFO: 3
}

var VALIDAR = {
    VAZIO: 0
}

function UIBaseAlert(titulo, mensagme, tipoMensagem) {
    var tipo = '';
    switch (tipoMensagem) {
        case 0: tipo = 'success'; break;
        case 1: tipo = 'error'; break;
        case 2: tipo = 'none'; break;
        case 3: tipo = 'info'; break;
    }
    $.pnotify({ title: titulo, text: mensagme, type: tipo, opacity: .8 });
}

function UIBaseMensagem(text, idMessage) {
    o(idMessage).innerHTML = text;
    o(idMessage).style.display = "block";
}

function UIBaseMensagem(text, id) {
    o(id).innerHTML = text;
    o(id).style.display = "block";
}

function UIBaseAlertSUCESSO(mensagem) {
    UIBaseAlert('Legal!', mensagem, tipoMensagem.SUCESSO);
}

function UIBaseAlertNONE(mensagem) {
    UIBaseAlert('Oops!', mensagem, tipoMensagem.NONE);
}

function UIBaseAlertERRO() {
    UIBaseAlert('ERRO!', 'Ouve algum erro, atualize a pagina e tente novamente!', tipoMensagem.ERRO);
}

function UIBaseAlertINFO(mensagem) {
    UIBaseAlert('Informacao!', mensagem, tipoMensagem.INFO);
}

function UIBaseRefreshShow() {
    var refresh = '<div id="refresh" class="widgetrefresh" style="display: block;"><span><i class="fa fa-spinner fa-spin fa-3x"></i></span></div>';
    document.getElementById('divRefresh').innerHTML += (refresh);
}

function UIBaseRefreshHide() {
    $("#refresh").remove();
}

function ShowMensagem(titulo, mensagem) {
    o('tituloModal').innerHTML = titulo;
    o('mensagemModal').innerHTML = mensagem;
    $('#ModalMensagem').modal('show');
}


function ShowMensagemError(titulo, mensagem) {
    o('tituloModal').innerHTML = titulo;
    o('mensagemModal').innerHTML = mensagem;
    var delay = 2000;
    setTimeout(function () {
        CloseMensagem();
    }, delay);

}




function CloseMensagem() {
    $('#ModalMensagem').modal('hide');
}