﻿$(document).ready(function () {
    $("#titulo_t").draggable();
    $("#descricao_t").draggable();
    $("#aniversariantes_t").draggable();
    $("#datahora_t").draggable();

    $("#owl-demo").owlCarousel({
        items: 4,
        lazyLoad: true,
        navigation: true,
        navigationText: [
          "<i class='fa fa-chevron-left icon-white'></i>",
          "<i class='fa fa-chevron-right icon-white'></i>"
        ],
        jsonPath: '/Convite/BuscarConvitesPorTema?tema=semFiltro',
        jsonSuccess: SelecionarTema
    });

    $("#owl-fontes").owlCarousel({
        items: 3,
        lazyLoad: true,
        navigation: true,
        navigationText: [
          "<i class='fa fa-chevron-left icon-white'></i>",
          "<i class='fa fa-chevron-right icon-white'></i>"
        ],
        jsonPath: '/Convite/BuscarFonteBuffetWeb',
        jsonSuccess: FonteSelecionada
    });

    BuscarFrases();
});

function FonteSelecionada(resposta) {
    var html = "";
    for (var i = 0; i < resposta.length; i++) {
        html += thumbnail(resposta[i]);
    }
    $("#owl-fontes").html(html);
}

function SelecionaArquivo(evt) {
    o('nome_fonte').value = evt.files[0].name.replace('.ttf', '');
}

function thumbnail(r) {
    return '<div class="item lazyOwl" >' +
                '<div class="thumbnail col-md-11" style=" height: 90px; "> ' +
                '<button type="button" class="close" onclick="ApagarFonteBuffetWeb(this)" data-id_fonte="' + r.id + '" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>' +
                    '<p class="text-center">' + r.nome_visivel + '</p>' +
                    '<p class="text-center">' + r.nome_fonte + '</p>' +
                '</div>' +
            '</div>';
}


function ApagarFonteBuffetWeb(evt) {
    $.getJSON('/Convite/ApagarFonte', { id_fonte: evt.dataset.id_fonte }, function (data) {
        if (data.isOk == true) {
            location.reload();
        } else {
            console.log("Ërro");
        }
    }, 'json');
}

function BuscarListaDeConvidados(evt) {
    if (evt.value != "") {
        $('#table').empty();
        $.getJSON('/Convite/BuscarListaDeConvidados', { id_orcamento: evt.value }, function (data) {
            if (data.length > 0) {
                $('#table').append(cabecalhoTabela());
                for (var i = 0; i < data.length; i++) {
                    $('#table').append('<tr><td>' + data[i].nome_completo +
                                       '</td><td>' + (data[i].email == null ? "" : data[i].email) +
                                       '</td><td>' + (data[i].telefone == null ? "" : data[i].telefone) +
                                       '</td><td>' + (data[i].email == null ? "Telefone" : data[i].ira) + '</td></tr>');
                }
                ShowMensagemError("Sucesso!", "Convidados Localizados.");
            } else {
                ShowMensagemError("Informação!", "Nenhum Convidado Localizados.");
            }
        }, 'json');
    } else {
        $('#table').empty();
    }
}


function cabecalhoTabela() {
    return '<thead><tr><th>Nome</th><th>E-mail</th><th>Telefone</th><th>Presença</th></tr></thead>';
}

function ConfirmarPresenca(evt, id_orcamento, id, status) {
    evt.value = "Aguarde...";
    $.getJSON('/Convite/ConfirmarPresenca', { id: id, id_orcamento_convite: id_orcamento, email: o('email_' + id).value, telefone: o('telefone_' + id).value, status: status }, function (resposta) {
        evt.value = status == "CONFIRMAR" ? "Confirmado" : "Cancelado";
        evt.disabled = true;
        $("botao_" + id).removeClass("btn-success").addClass("btn-default");
    }, 'json');
}


function SalvarConvidado(evt) {
    if (o('nome_completo').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe o nome do convidado!";
        return false;
    }
    if (o('email').value == "" && o('telefone').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Você precisa informar no mínimo o Telefone ou Email do convidado!";
        return false;
    }
    if (o('telefone').value.length < 9) {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe um telefone valido com DDD!";
        return false;
    }

    evt.value = "Aguarde...";
    $.getJSON('/Convite/SalvarConvidado?' + $("#NOVOCONVIDADO").serialize(), function (resposta) {
        if (resposta == true) {
            location.reload();
        }
    }, 'json');
}

function AtualizarTitulo() {
    o('titulo_t').style.fontFamily = o('fonte_titulo').value;
    o('titulo_t').style.fontSize = o('tamanho_titulo').value + "px";
    o('titulo_t').style.color = "#" + o('cor_titulo').value;
}

function LocalidadeTitulo(evt) {
    o('titulo_top').value = evt.style.top;
    o('titulo_left').value = evt.style.left;
}



function AtualizarDescricao() {
    o('descricao_t').style.fontFamily = o('fonte_descricao').value;
    o('descricao_t').style.fontSize = o('tamanho_descricao').value + "px";
    o('descricao_t').style.color = "#" + o('cor_descricao').value;
}

function LocalidadeDescricao(evt) {
    o('descricao_top').value = evt.style.top;
    o('descricao_left').value = evt.style.left;
}

function AtualizarAniversariantes() {
    o('aniversariantes_t').style.fontFamily = o('fonte_aniversariante').value;
    o('aniversariantes_t').style.fontSize = o('tamanho_aniversariante').value + "px";
    o('aniversariantes_t').style.color = "#" + o('cor_aniversariante').value;
}

function LocalidadeAniversariantes(evt) {
    o('aniversariantes_top').value = evt.style.top;
    o('aniversariantes_left').value = evt.style.left;
}


function AtualizarDataHora() {
    o('datahora_t').style.fontFamily = o('fonte_datahora').value;
    o('datahora_t').style.fontSize = o('tamanho_datahora').value + "px";
    o('datahora_t').style.color = "#" + o('cor_datahora').value;
}

function LocalidadeDataHora(evt) {
    o('datahora_top').value = evt.style.top;
    o('datahora_left').value = evt.style.left;
}

function EditarConvite(evt) {
    var editar = evt.dataset;
    o('editando').value = true;
    o('id').value = editar.id;
    o('id_convite_excluir').value = editar.id;

    o('tema_valor').value = editar.tema;
    o('tipo_evento').value = editar.tipo_evento;
    if (editar.url_image != "") {
        o('Imagem').src = editar.url_image;
        o('url_image').value = editar.url_image;
    }

    o('disponivel').value = editar.disponivel == "True";
    o('tema_div').setAttribute("class", "col-md-9");
    o('disponivel_div').style.display = "block";
    o('cancelarEdicao').style.display = "block";
    o('excluirEdicao').style.display = "block";

    //TITULO
    o('titulo_convite').value = editar.titulo;
    o('titulo_t').innerHTML = editar.titulo
    o('fonte_titulo').value = editar.titulo_fonte
    o('tamanho_titulo').value = editar.titulo_tamanho
    o('cor_titulo').value = editar.titulo_cor;
    o('titulo_top').value = editar.titulo_top
    o('titulo_left').value = editar.titulo_left

    o('cor_titulo').style.backgroundColor = "#" + editar.titulo_cor;
    o('cor_titulo').style.color = "black";
    o('titulo_t').style.top = editar.titulo_top;
    o('titulo_t').style.left = editar.titulo_left;
    o('titulo_t').style.fontFamily = editar.titulo_fonte;
    o('titulo_t').style.fontSize = editar.titulo_tamanho + "px";
    o('titulo_t').style.color = "#" + editar.titulo_cor;

    //DESCRICAO
    o('descric').value = editar.descricao
    o('descricao_t').innerHTML = editar.descricao
    o('fonte_descricao').value = editar.descricao_fonte
    o('tamanho_descricao').value = editar.descricao_tamanho
    o('cor_descricao').value = editar.descricao_cor
    o('descricao_top').value = editar.descricao_top
    o('descricao_left').value = editar.descricao_left

    o('cor_descricao').style.backgroundColor = "#" + editar.descricao_cor;
    o('cor_descricao').style.color = "black";
    o('descricao_t').style.top = editar.descricao_top;
    o('descricao_t').style.left = editar.descricao_left;
    o('descricao_t').style.fontFamily = editar.descricao_fonte;
    o('descricao_t').style.fontSize = editar.descricao_tamanho + "px";
    o('descricao_t').style.color = "#" + editar.descricao_cor;

    //ANIVERSARIANTE
    o('aniversariantes_titulo').value = editar.aniversariantes_titulo;
    o('aniversariantes_t').innerHTML = editar.aniversariantes_titulo;
    o('cor_aniversariante').value = editar.aniversariantes_cor
    o('fonte_aniversariante').value = editar.aniversariantes_fonte
    o('tamanho_aniversariante').value = editar.aniversariantes_tamanho
    o('aniversariantes_top').value = editar.aniversariantes_top
    o('aniversariantes_left').value = editar.aniversariantes_left

    o('cor_aniversariante').style.backgroundColor = "#" + editar.aniversariantes_cor;
    o('cor_aniversariante').style.color = "black";
    o('aniversariantes_t').style.top = editar.aniversariantes_top;
    o('aniversariantes_t').style.left = editar.aniversariantes_left;
    o('aniversariantes_t').style.fontFamily = editar.aniversariantes_fonte;
    o('aniversariantes_t').style.fontSize = editar.aniversariantes_tamanho + "px";
    o('aniversariantes_t').style.color = "#" + editar.aniversariantes_cor;

    //DATAHORA
    o('datahora_titulo').value = editar.datahora_titulo
    o('datahora_t').innerHTML = editar.datahora_titulo;
    o('fonte_datahora').value = editar.datahora_fonte
    o('tamanho_datahora').value = editar.datahora_tamanho
    o('cor_datahora').value = editar.datahora_cor
    o('datahora_top').value = editar.datahora_top
    o('datahora_left').value = editar.datahora_left

    o('cor_datahora').style.backgroundColor = "#" + editar.datahora_cor;
    o('cor_datahora').style.color = "black";
    o('datahora_t').style.top = editar.datahora_top;
    o('datahora_t').style.left = editar.datahora_left;
    o('datahora_t').style.fontFamily = editar.datahora_fonte;
    o('datahora_t').style.fontSize = editar.datahora_tamanho + "px";
    o('datahora_t').style.color = "#" + editar.datahora_cor;
}

function TabShow(nome) {
    $('#ConviteTabs a[href="#' + nome + '"]').tab('show')
}

//FONTES DISPONIVEIS
function BuscarFrases() {
    $.getJSON('/Convite/BuscarFrases', function (resposta) {
        var cabecalho = "<thead> <tr> <td colspan='3'>Frase</td> <td></td> </tr> </thead>";
        for (var i = 0; i < resposta.frases.length; i++) {
            cabecalho += LinhaFrase(resposta.frases[i]);
        }
        o('tabela_Frases').innerHTML = cabecalho;
    }, 'json');
}

function LinhaFrase(frase) {
    return '<tr id="tr_tabela_frases' + frase.id + '">' +
                '<td>' + (frase.titulo == null ? "" : frase.titulo) + '</td>' +
                '<td>' + frase.descricao + '</td>' +
                '<td>' +
                    '<input type="button" data-dismiss="modal" class="btn btn-xs btn-success" data-titulo="' + frase.titulo + '" data-descricao="' + frase.descricao + '" onclick="SelecionarFrase(this)" value="Selecionar" />' +
                '</td>' +
                '<td>' +
                    '<input type="button" class="btn btn-xs btn-danger" data-id="' + frase.id + '" onclick="ApagarFrase(this)" value="Apagar" />' +
                '</td>' +
            '</tr>';
}

function SalvarFrase(evt) {
    if (o('descricao_frase').value == "") {
        return;
    }
    evt.value = "Aguarde...";
    $.getJSON('/Convite/SalvarFrase', { titulo: o('titulo_frase').value, descricao: o('descricao_frase').value }, function (resposta) {
        if (resposta.isOk) {
            $("#tabela_Frases tr:first").after(LinhaFrase(resposta));
            o('titulo_frase').value = "";
            o('descricao_frase').value = "";
        }
        evt.value = "Salvar Frase";
    }, 'json');
}

function ApagarFrase(evt) {
    evt.value = "Aguarde...";
    $.getJSON('/Convite/ApagarFrase', { id_frase: evt.dataset.id }, function (resposta) {
        if (resposta.isOk) {
            $("#tr_tabela_frases" + evt.dataset.id).remove();
        } else {
            evt.value = "Apagar";
        }
    }, 'json');
}

function SelecionarFrase(evt) {
    if (evt.dataset.titulo != "null") {
        o('titulo_convite').value = evt.dataset.titulo;
        o('titulo_t').innerHTML = evt.dataset.titulo;
    } else {
        o('titulo_convite').value = "";
        o('titulo_t').innerHTML = "";
    }

    o('descric').value = evt.dataset.descricao;
    o('descricao_t').innerHTML = evt.dataset.descricao;
}

var index = 0;
function BuscarConvitesPorTema(evt) {
    $.getJSON('/Convite/BuscarConvitesPorTema', { tema: evt.value }, function (resposta) {
        for (var i = 0; i < index; i++) {
            $("#owl-demo").data('owlCarousel').removeItem();
        }

        for (var i = 0; i < resposta.length; i++) {
            $("#owl-demo").data('owlCarousel').addItem(convitesThumbnail(resposta[i], i));
        }
    }, 'json');
}


function SelecionarTema(resposta) {
    index = resposta.length;
    $("#owl-demo").owlCarousel();
    var html = "";
    for (var i = 0; i < resposta.length; i++) {
        html += convitesThumbnail(resposta[i], i);
    }
    $("#owl-demo").html(html);
}

function convitesThumbnail(r) {
    return '<div class="item lazyOwl" style=" cursor: pointer; ">' +
                '<a href="javascript:void(o)" onclick="EditarConvite(this)" data-id="' + r.id + '" data-tema="' + r.tema + '" data-url_image="' + r.url_image + '" data-disponivel="' + r.disponivel + '" data-titulo="' + (r.titulo == null ? "" : r.titulo) + '" data-titulo_cor="' + r.titulo_cor + '" data-titulo_fonte="' + r.titulo_fonte + '" data-titulo_tamanho="' + r.titulo_tamanho + '" data-titulo_top="' + r.titulo_top + '" data-titulo_left="' + r.titulo_left + '" data-datahora_titulo="' + r.datahora_titulo + '" data-datahora_cor="' + r.datahora_cor + '" data-datahora_fonte="' + r.datahora_fonte + '" data-datahora_tamanho="' + r.datahora_tamanho + '" data-datahora_top="' + r.datahora_top + '" data-datahora_left="' + r.datahora_left + '" data-descricao="' + r.descricao + '" data-descricao_cor="' + r.descricao_cor + '" data-descricao_fonte="' + r.descricao_fonte + '" data-descricao_tamanho="' + r.descricao_tamanho + '" data-descricao_top="' + r.descricao_top + '" data-descricao_left="' + r.descricao_left + '" data-aniversariantes_titulo="' + r.aniversariantes_titulo + '" data-aniversariantes_cor="' + r.aniversariantes_cor + '" data-aniversariantes_fonte="' + r.aniversariantes_fonte + '" data-aniversariantes_tamanho="' + r.aniversariantes_tamanho + '" data-aniversariantes_top="' + r.aniversariantes_top + '" data-aniversariantes_left="' + r.aniversariantes_left + '" data-tipo_evento="' + r.tipo_evento + '" >' +
                '<div class="thumbnail col-md-11">' +
                    '<img style=" height: 200px; " src=' + r.url_image + '>' +
                '<div class="caption"> <h4 class="text-center">' + r.tema + '</h4></div>' +
                '</div>' +
            '</a>' +
            '</div>';
}