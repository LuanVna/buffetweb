﻿var index = 0;
var indexTabela = 0;

function adicionarFornecedor(evt, total) {
    if (index < total) {
        o(index + 'id_fornecedor').style.display = "block";
        index == 0 ? index++ : "";
    }
}

function removerFornecedor(evt, total) {
    if (index < total) {
        o(index + 'id_fornecedor').style.display = "none";
        index != 0 ? index-- : "";
    }
}

function BuscaUnidadeMedida(evt) {
    var select = evt.options[evt.selectedIndex];
    var unidade = select.dataset.unidade_medida;
    if (select != "") {
        o('tipo_quantidade').placeholder = unidade;
        o('tipo_quantidade').value = "";
        o('valor_ingrediente').value = select.dataset.valor;
    }
}

function AdicionaComEnter(evt) {
    if (event.keyCode == 13) {
        AdicionarIngrediente();
        o('ingrediente_m').focus();
        evt.value = "";
        evt.placeholder = "";
    }
}

function AdicionarIngrediente(evt) {
    var index = o('tabelaIngredientes').getElementsByTagName("tr").length - 1;
    if (v('ingrediente_m') != "" && v('tipo_quantidade') != "" && v('tipo_quantidade') != '0') {
        var valor_total = 0;
        var quantidade = parseFloat(v('tipo_quantidade').replace(',', '.'));

        var select = o('ingrediente_m').options[o('ingrediente_m').selectedIndex];
        var valor_ingrediente = parseFloat(select.dataset.valor.replace(',', '.'));
        var quantiade_embalagem = parseFloat(select.dataset.quantiade_embalagem.replace(',', '.'));

        var valor_item = ((valor_ingrediente / quantiade_embalagem) * quantidade).toFixed(2);

        if (o('valor_compra_m').value != "") {
            o('valor_compra_m').value = (parseFloat(o('valor_compra_m').value) + parseFloat(valor_item)).toFixed(2);
        } else {
            o('valor_compra_m').value = valor_item;
        }

        var hiddens = "<input type='hidden' id='[" + index + "]id_ingrediente' value=" + o('ingrediente_m').options[o('ingrediente_m').selectedIndex].dataset.id_ingrediente + ">";
        hiddens += "<input type='hidden' id='[" + index + "]valor_total' value=" + valor_item + ">";
        hiddens += "<input type='hidden' id='[" + index + "]quantidade' value=" + quantidade + ">";

        var id_botao = (index - 1) + "_botao";
        if (o(id_botao) != null) {
            o(id_botao).style.display = "none";
        }

        $('#tabelaIngredientes').append('<tr id="' + o('tabelaIngredientes').getElementsByTagName("tr").length + '"><td>' + v('ingrediente_m') + '</td><td>' + v('tipo_quantidade') + " " + o('tipo_quantidade').placeholder + '</td><td><input type="button" id="' + index + '_botao" class="btn btn-xs btn-danger pull-right" onclick="apagarIngrediente(this,' + o('tabelaIngredientes').getElementsByTagName("tr").length + ',' + valor_item + ' )" value="remover" />' + hiddens + '</td></tr>');

        o('itens_descricao').style.display = "none";
        o('itens_tabela').style.display = "block";

        CalcularCustoPessoaMontar(o('rendimento_m'));
    }
}

function CalcularCustoPessoa(evt) {
    var total = o('quantidade_embalagem') / evt.value;

    if (evt.value != "") {
        o('custo_pessoa_p').value = (v('valor_compra_p_item') / evt.value).toFixed(2);
    } else {
        o('custo_pessoa_p').value = "";
    }
}

function CalcularCustoPessoaMontar(evt) {
    if (evt.value != "") {
        o('custo_pessoa_m').value = (v('valor_compra_m') / evt.value).toFixed(2);
    } else {
        o('custo_pessoa_m').value = "";
    }
    CalculaMargemLuucro(o('margem_lucro_m'), 'custo_pessoa_m', 'venda_pessoa_m');
}

function apagarIngrediente(evt, id_row, valor_linha) {
    var index = o('tabelaIngredientes').getElementsByTagName("tr").length - 1;

    var id_botao = (index - 2) + "_botao";
    if (o(id_botao) != null) {
        o(id_botao).style.display = "block";
    }
    if (index == 1) {
        o('itens_descricao').style.display = "block";
        o('itens_tabela').style.display = "none";
        o('tabelaIngredientes').deleteRow(id_row);
        o('valor_compra_m').value = (parseFloat(o('valor_compra_m').value) - parseFloat(valor_linha)).toFixed(2);
    } else {
        o('tabelaIngredientes').deleteRow(id_row);
        o('valor_compra_m').value = (parseFloat(o('valor_compra_m').value) - parseFloat(valor_linha)).toFixed(2);
    }
}

function calcularPercentual(evt) {
    if (o('valor_compra').value != "") {
        var valor_custo = parseFloat(o('valor_compra').value);
    }
}

function SalvaFornecedor(evt) {
    ShowMensagem('Aguarde...', 'Salvando Fornecedor');
    $.getJSON('/Estoque/SalvarFornecedorJSON?' + $("#FORNECEDOR").serialize(), function (data) {
        if (data.Item1) {
            PreencheSelect('montarItemUnSelect', data.Item2, data.Item3);
            PreencheSelect('ifornecedoresUnSelect', data.Item2, data.Item3);
            ShowMensagemError("Sucesso!", "Fornecedor Cadastrado com sucesso");
            o("FORNECEDOR").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function SalvarSubCategoria(evt) {
    ShowMensagem('Aguarde...', 'Salvando Sub Categoria');
    $.getJSON('/Estoque/SalvarSubCategoriaJSON?' + $("#SUBCATEGORIAS").serialize(), function (data) {
        if (data.Item1) {
            o("SUBCATEGORIAS").reset();
            if (o('id_subcategoria').value != "") {
                ShowMensagemError("Atualize a pagina!!", "Sub Categoria Atualizada com sucesso.");
            } else {
                ShowMensagemError("Sucesso!", "Sub Categoria Cadastrada com sucesso");
            }
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function BuscarSubCategorias(evt) {
    $("#id_sub_categoria_p").empty();
    PreencheSelect('id_sub_categoria_p', "Aguarde...", "");
    $.getJSON('/Estoque/BuscarSubCategoria', { id_categoria: evt.value }, function (data) {
        if (data != null) {
            $("#id_sub_categoria_p").empty();
            if (data.length != 0) {
                PreencheSelect('id_sub_categoria_p', "Selecione", "");
                for (var i = 0; i < data.length; i++) {
                    var select = document.getElementById('id_sub_categoria_p');
                    var option = document.createElement("option");

                    option.dataset.rendimento_pessoa = data[i].rendimento_pessoa;
                    option.dataset.id_unidade_medida = data[i].id_unidade_medida;

                    option.value = data[i].id_sub_categoria;
                    option.innerHTML = data[i].nome_subcategoria;
                    select.appendChild(option);

                    o('rendimento_p').readOnly = false;
                    o('rendimento_p').value = "";
                    o('id_unidade_medida_hidden_p').value = data[i].id_unidade_medida;
                }
            } else {
                PreencheSelect('id_sub_categoria_p', "Sem Sub-Categoria", "");
                o('rendimento_p').readOnly = false;
                o('id_unidade_medida_p').disabled = false;
                o('id_unidade_medida_hidden_p').value = "";
            }
        } else {
            window.location.reload();
        }
    }, 'json');
}

function SubCategoriaSelecionada(evt) {
    if (evt.options[evt.selectedIndex].innerHTML != "Selecione" && evt.options[evt.selectedIndex].innerHTML != "Sem Sub-Categoria") {
        var option = evt.options[evt.selectedIndex].dataset;

        o('id_unidade_medida_p').value = option.id_unidade_medida;
        o('rendimento_hidden_p').value = option.rendimento_pessoa;

        o('rendimento_p').readOnly = true;
        o('id_unidade_medida_p').disabled = true;
    } else {
        o('rendimento_p').value = "";
        o('rendimento_p').readOnly = false;
        o('id_unidade_medida_p').disabled = false;
        o('id_unidade_medida_hidden_p').value = "";
    }
}

function CalculaValores(evt) {
    if (o('quantidade_embalagem_p').value != "") {
        o('rendimento_p').value = (o('quantidade_embalagem_p').value / o('rendimento_hidden_p').value).toFixed(2);
    } else {
        o('rendimento_p').value = "";
    }
    if (o('valor_compra_p_item').value != "" && o('quantidade_embalagem_p').value != "") {
        o('custo_pessoa_p').value = (o('valor_compra_p_item').value / o('rendimento_p').value).toFixed(2);
    } else {
        o('custo_pessoa_p').value = "";
    }
}


function ValorCustoP(evt) {
    if (evt.value != "") {
        o('custo_pessoa_p').value = (evt.value / o('rendimento_p').value).toFixed(2);
    } else {
        o('custo_pessoa_p').value = "";
    }
}

function SelecionarUnidade(evt) {
    o('unidade_selecionada').value = evt.options[evt.selectedIndex].innerHTML;
}

function SalvaIngrediente(evt) {
    ShowMensagem('Aguarde...', 'Salvando Ingrediente');
    $.getJSON('/Estoque/SalvarIngredienteJSON?' + $("#INGREDIENTES").serialize(), function (data) {
        if (data.Item1) {
            var id_ingrediente = data.Item2;
            $("#ifornecedoresSelect option").each(function (i) {
                var id_fornecedor = $(this).val();
                $.getJSON('/Estoque/SalvarFornecedorIngredienteJSON',
                    { id_fornecedor: id_fornecedor, id_ingrediente: id_ingrediente }, function (data) { });
            });

            var select = document.getElementById('ingrediente_m');
            var option = document.createElement("option");

            option.dataset.valor = v('valor_custo');
            option.dataset.unidade_medida = o('unidade_selecionada').value;
            option.dataset.quantiade_embalagem = o('quantidade_embalagem').value;
            option.dataset.id_ingrediente = id_ingrediente;

            option.value = v('marca');
            option.innerHTML = v('marca');
            select.appendChild(option);

            ShowMensagemError("Sucesso!", "Ingrediente Cadastrado com sucesso");
            RemoveAll(this, 'ifornecedoresSelect', 'ifornecedoresUnSelect');
            o("INGREDIENTES").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function SalvarItemServico(evt) {
    ShowMensagem('Aguarde...', 'Salvando Item');
    $.getJSON('/Itens/SalvarItemServicoJSON?' + $("#ITEMSERVICO").serialize(), function (data) {
        if (data.Item1) {
            var id_servico = data.Item2;
            $("#prestadoresSelect option").each(function (i) {
                var id_prestador_funcao = $(this).val();
                $.getJSON('/Itens/SalvarPrestadorItemServicoJSON',
                    { id_prestador_funcao: id_prestador_funcao, id_servico: id_servico },
                    function (data) { });
            });


            ShowMensagemError("Sucesso!", "Item Cadastrado com sucesso");
            RemoveAll(this, 'prestadoresSelect', 'prestadoresUnSelect');

            o("ITEMSERVICO").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}




function SalvarItemPronto(evt) {
    if (o('id_item_p').value != "") {
        ShowMensagem('Aguarde...', 'Atualizando Item');
    } else {
        ShowMensagem('Aguarde...', 'Salvando Item');
    }
    $.getJSON('/Estoque/SalvarItemJSON?' + $("#ITEMPRONTO").serialize(), function (data) {
        if (data.Item1) {
            var id_cardapio = data.Item2;
            UpLoadImagemItemCardapio("imagem_item_p_file", id_cardapio);
            $("#fornecedoresSelect_p option").each(function (i) {
                var id_fornecedor = $(this).val();
                $.getJSON('/Estoque/SalvarFornecedorItemProntoJSON',
                    { id_fornecedor: id_fornecedor, id_cardapio: id_cardapio },
                    function (data) { });
            });
            if (o('id_item_p').value != "") {
                ShowMensagemError("Atualize a pagina!!", "Item Atualizada com sucesso.");
            } else {
                ShowMensagemError("Sucesso!", "Item Cadastrado com sucesso");
            }
            RemoveAll(this, 'fornecedoresSelect_p', 'fornecedoresUnSelect_p');

            o('rendimento_p').readOnly = false;
            o('id_unidade_medida_p').disabled = false;

            o('alerta_cardapio').style.display = "block";
            if (o('id_item_p').value != "") {
                o('mensagem_cardapio').innerHTML = "Atualize a pagina para listar o ultimo item atualizado!";
            } else {
                o('mensagem_cardapio').innerHTML = "Atualize a pagina para listar o ultimo item cadastrado!";
            }

            //LIMPA ATUALIZACAO
            o('id_item_p').value = "";
            o('botao_confirmar_p').value = "Confirmar";

            o("ITEMPRONTO").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function SalvarMontarItem(evt) {

    var rowCount = $('#tabelaIngredientes tr').length - 1;
    if (rowCount == 0) {
        o("alerta_item_pronto").style.display = "block";
        return;
    }
    o("alerta_item_pronto").style.display = "none";
    ShowMensagem('Aguarde...', 'Salvando Item');
    $.getJSON('/Estoque/SalvarItemJSON?' + $("#MONTARITEM").serialize(), function (data) {
        if (data.Item1) {
            UpLoadImagemItemCardapio("image_item_m_file", id_cardapio);
            for (var i = 0; i != o('tabelaIngredientes').rows.length - 1; i++) {
                var jsonPost = { id_item: data.Item2, id_ingrediente: o('[' + i + ']id_ingrediente').value, quantidade: o('[' + i + ']quantidade').value };
                $.getJSON('/Estoque/SalvarIngredienteMontarItemJSON', jsonPost, function (data) { });
            }
            if (o('id_item_m').value == "") {
                ShowMensagemError("Sucesso!", "Item Cadastrado com sucesso");
                o('mensagem_cardapio').innerHTML = "Atualize a pagina para listar o ultimo item cadastrado!";
            } else {
                ShowMensagemError("Atualize a pagina!", "Item Atualizado com sucesso!");
                o('mensagem_cardapio').innerHTML = "Atualize a pagina para listar o ultimo item atualizado!";
            }

            $('#tabelaIngredientes tbody').html('');
            o('itens_descricao').style.display = "block";
            o('itens_tabela').style.display = "none";

            o('alerta_cardapio').style.display = "block";
            o('alerta_ingrediente').style.display = "block";
            evt.value = "Confirmar";

            o("MONTARITEM").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function SalvarCategoria(evt) {
    ShowMensagem('Aguarde...', 'Salvando Categoria');
    $.getJSON('/Estoque/SalvarCategoriaJSON?' + $("#CATEGORIAS").serialize(), function (data) {
        if (data.Item1) {

            PreencheSelect('id_categoria_m', v('nome_categoria'), data.Item2);
            PreencheSelect('id_categoria_p', v('nome_categoria'), data.Item2);
            PreencheSelect('id_categoria_subcategoria', v('nome_categoria'), data.Item2);

            if (o('id_categoria_categoria').value != "") {
                ShowMensagemError("Atualize a pagina!!", "Categoria Atualizada com sucesso.");
                o('id_categoria_categoria').value = "";
            } else {
                ShowMensagemError("Sucesso!", "Categoria com sucesso");
            }
            o('alerta_cardapio').style.display = "block";
            o('mensagem_cardapio').innerHTML = "Atualize a pagina para listar a ultima categoria criada!";

            o("CATEGORIAS").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}


function SalvarCardapio(evt, categorias) {
    ShowMensagem('Aguarde...', 'Salvando Cardapio');
    $.ajax({
        url: '/Estoque/SalvarCardapioJSON?',
        dataType: 'json',
        async: true,
        data: $("#CARDAPIO").serialize(),
        success: function (data) {
            if (data.Item1) {
                ShowMensagemError("Sucesso!", "Cardapio Salvo com sucesso!");
                UpLoadImagemCardapio(data.Item2);
                for (var i = 0; i < categorias.split(",").length; i++) {
                    if (categorias.split(",")[i] != "") {
                        var editavel = o("editavel" + categorias.split(",")[i]).value;
                        var maximo_itens = o("maximo_itens" + categorias.split(",")[i]).value;
                        var id_categoria = o("id_categoria" + categorias.split(",")[i]).value;
                        $.ajax({
                            url: '/Estoque/SalvarPacoteCategoriaJSON?',
                            dataType: 'json',
                            async: false,
                            data: { editavel: editavel, id_categoria: id_categoria, maximo_itens: maximo_itens, id_pacotes: data.Item2 },
                            success: function (dataCategoria) {
                                if (dataCategoria.Item1) {
                                    if (categorias.split(",")[i] != "") {
                                        var dataset = o('categoria' + categorias.split(",")[i]).dataset;

                                        for (var x = 0; x < dataset.total; x++) {
                                            var isSelect = o(dataset.cardapios_id.split(",")[x] + "isselect");
                                            if (isSelect.value == "true") {
                                                //var id_cardapio = o(dataset.id + dataset.cardapios_id.split(",")[x].toString() + "id_cardapio");
                                                $.ajax({
                                                    url: '/Estoque/SalvarPacoteItemJSON?',
                                                    dataType: 'json',
                                                    async: false,
                                                    data: {
                                                        id_itens_cardapio: isSelect.dataset.id_item,
                                                        id_categoria: dataset.id,
                                                        id_categoria_pacote: dataCategoria.Item2
                                                    }, success: function (dataCategoria) {
                                                        console.log('Ok' + dataCategoria);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            } else {
                ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
            }
        }
    });
}

function SelecionarItem(evt, id, valor_compra) {
    if (o(id).value == "false") {
        if (o('valor_custo_cardapio').value != "") {
            o('valor_custo_cardapio').value = (parseFloat(o('valor_custo_cardapio').value) + parseFloat(valor_compra)).toFixed(2);
        } else {
            o('valor_custo_cardapio').value = parseFloat(valor_compra).toFixed(2);
        }
        o(id).value = "true";
    } else {
        if (o('valor_custo_cardapio').value != "") {
            o('valor_custo_cardapio').value = (parseFloat(o('valor_custo_cardapio').value) - parseFloat(valor_compra)).toFixed(2);
        }
        o(id).value = "false";
    }
    PercentualLucroCardapio(o('margem_lucro'));
}

function PercentualLucroCardapio(evt) {
    if (o('valor_custo_cardapio').value != "" && evt.value != "") {
        o('valor_venda_cardapio').value = (parseFloat(o('valor_custo_cardapio').value) + (parseFloat(o('valor_custo_cardapio').value) * (evt.value / 100))).toFixed(2);
    } else {
        o('valor_venda_cardapio').value = o('valor_custo_cardapio').value;
    }
}




function CalculaMargemLuucro(evt, custo, venda) {
    if (v(custo) != "") {
        o(venda).value = (parseFloat(v(custo)) + (parseFloat(v(custo)) * (evt.value / 100))).toFixed(2);
    }
}












function PreencheSelect(id, text, value) {
    var select = document.getElementById(id);
    var option = document.createElement("option");
    option.value = value;
    option.innerHTML = text;
    select.appendChild(option);
}

var tabAtual;

function NovoIngrediente(evt) {
    $('#Itens a[href="#tabIngredientes"]').tab('show');
}

$(document).ready(function () {
    $('#Itens a').click(function (e) {
        tabAtual = this.dataset.id;

        if (tabAtual == 'cardapio') {
            o('tabHeading').innerHTML = "Manutenção de Cardápio";
        } else {

            o('tabHeading').innerHTML = "Itens de Cardápio";
        }

        $(this).tab('show')
    })
});

function AtualizarValor(evt, categorias, categoria_id, todos) {
    var arrTodos = todos.split(",");

    var maiorValor = 0;
    var custo = 0;
    var selecionados = 0;
    for (var i = 0; i != arrTodos.length; i++) {
        if (arrTodos[i] != "") {
            var produtos = o(arrTodos[i] + 'isselect').dataset;

            if (o(arrTodos[i] + 'isselect').value == "true") {

                selecionados++;
                var id_item = produtos.id_item;
                var venda_pessoa = parseFloat(produtos.venda_pessoa.replace(',', '.')).toFixed(2);

                if (parseFloat(venda_pessoa) > parseFloat(maiorValor)) {
                    maiorValor = venda_pessoa;
                    custo = parseFloat(produtos.custo_pessoa.replace(',', '.')).toFixed(2);
                }
            }
        }
    }

    //var media = maiorValor / selecionados;

    if (selecionados == 0) {
        maiorValor = 0;
    }

    o('venda' + categoria_id).value = maiorValor;
    o('custo' + categoria_id).value = custo;

    var total_venda = 0;
    var total_custo = 0;
    for (var i = 0; i < categorias.split(",").length; i++) {
        if (categorias.split(",")[i] != "") {
            total_venda = parseFloat(total_venda) + parseFloat(o('venda' + categorias.split(",")[i]).value);
            total_custo = parseFloat(total_custo) + parseFloat(o('custo' + categorias.split(",")[i]).value);


        }
    }

    o('valor_custo_cardapio').value = total_custo.toFixed(2);
    o('valor_venda_cardapio').value = total_venda.toFixed(2);

    if (total_custo != 0 || total_venda != 0) {

        var lucro = total_venda - total_custo;

        lucro = (lucro * 100) / total_custo
        o('margem_lucro').value = lucro.toFixed(2);

    } else {
        o('margem_lucro').value = "";
        o('margem_lucro').placeholder = "Em (%)"
    }
}

function UpLoadImagemItemCardapio(idComponent, id_item) {
    UPLoadImage(idComponent, "UpLoadImagemItemCardapio", id_item);
}


function UpLoadImagemCardapio(id_item) {
    UPLoadImage("image_cardapio_file", "UpLoadImagemCardapio", id_item);
}

function UpLoadImagemAluguel(id_item) {
    UPLoadImage("imagem_selecionada", "UpLoadImagemAluguel", id_item);
}

function UPLoadImage(idComponent, URL, id_item) {
    var data = new FormData();
    var files = $("#" + idComponent + "").get(0).files;
    if (files.length > 0) {
        data.append("Image", files[0]);
    }
    $.ajax({
        url: '/Estoque/' + URL + '?id_item=' + id_item,
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            console.log("Upload" + response)
        },
        error: function (er) {
            console.log("Upload Error")
        }
    });
}