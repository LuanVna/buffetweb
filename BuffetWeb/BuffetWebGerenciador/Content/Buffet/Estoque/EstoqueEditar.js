﻿function EditarIngrediente(evt, id_ingrediente) {
    evt.value = "Aguarde...";
    $.getJSON('/Estoque/BuscarIngrediente', { id_ingrediente: id_ingrediente }, function (data) {
        if (data.isOk) {

            o('id_ingrediente').value = id_ingrediente;
            o('id_unidade_medida').value = data.id_unidade_medida;
            o('marca').value = data.marca;
            o('quantidade_embalagem_ingrediente').value = data.quantidade_embalagem;
            o('valor_custo').value = data.valor_custo;

            for (var i = 0; i < data.fornecedores.length; i++) {
                adicionarSelect('ifornecedoresSelect', 'ifornecedoresUnSelect', data.fornecedores[i].id_fornecedor, data.fornecedores[i].razao_social);
            }
            $('#modal_ingredientes').modal('hide');
            evt.value = "Editar";
        } else {
            mensagemError();
        }
    }, 'json');
}

function CancelarEditarIngrediente(evt) {
    evt.style.display = "none";
    o('INGREDIENTES').reset();
    RemoveAll(this, 'ifornecedoresSelect', 'ifornecedoresUnSelect');
    o('id_ingrediente').value = "";
}

function EditarSubCategoria(evt, id_subcategoria) {
    evt.value = "Aguarde...";
    $.getJSON('/Estoque/BuscarSubCategoriaEditar', { id_subcategoria: id_subcategoria }, function (data) {
        if (data.isOk) {

            o('id_subcategoria').value = id_subcategoria;
            o('id_categoria_subcategoria').value = data.id_categoria;
            o('id_unidade_medida_subcategoria').value = data.id_unidade_medida;
            o('nome_sub_categoria').value = data.nome;
            o('rendimento_pessoa').value = data.rendimento_pessoa;
            $('#modal_subcategorias').modal('hide');
            evt.value = "Editar";
        } else {
            mensagemError();
        }
    }, 'json');
}

function EditarCategoria(evt, id_categoria) {
    evt.value = "Aguarde...";
    $.getJSON('/Estoque/BuscarCategoriaEditar', { id_categoria: id_categoria }, function (categoria) {
        if (categoria.isOk) {

            o('id_categoria_categoria').value = id_categoria;
            o('descricao_categoria').value = categoria.opcao;
            o('nome_categoria').value = categoria.nome;

            //o('image_item_m_file').src = categoria.url_image;

            $('#modal_categorias').modal('hide');
            evt.value = "Editar";
        } else {
            mensagemError();
        }
    }, 'json');
}

function EditarItem(evt, id_item) {
    evt.value = "Aguarde...";
    $.getJSON('/Estoque/BuscarItemEditar', { id_item: id_item }, function (item) {
        if (item.isOk) {
            if (item.isItemPronto) {
                o('id_item_p').value = id_item;
                o('botao_confirmar_p').value = "Atualizar";

                //SUBCATEGORIA
                $("#id_sub_categoria_p").empty();
                PreencheSelect('id_sub_categoria_p', "Selecione", "");
                for (var i = 0; i < item.sub_categorias.length; i++) {
                    PreencheSelect('id_sub_categoria_p', item.sub_categorias[i].nome, item.sub_categorias[i].id_sub_categoria);
                }

                //FORNECEDORES
                RemoveAll(this, 'fornecedoresSelect_p', 'fornecedoresUnSelect_p');
                for (var i = 0; i < item.fornecedores.length; i++) {
                    adicionarSelect('fornecedoresSelect_p', 'fornecedoresUnSelect_p', item.fornecedores[i].id_fornecedor, item.fornecedores[i].razao_social);
                }

                o('nome_p').value = item.nome;
                o('id_categoria_p').value = item.id_categoria;
                o('id_sub_categoria_p').value = item.id_sub_categoria;

                o('id_unidade_medida_p').value = item.id_unidade_medida;
                o('id_unidade_medida_hidden_p').value = item.id_unidade_medida;

                o('quantidade_embalagem_p').value = item.quantidade_embalagem;
                o('valor_compra_p').value = item.valor_compra;
                o('rendimento_p').value = item.rendimento;

                o('rendimento_p').readOnly = true;
                o('id_unidade_medida_p').disabled = true;

                o('rendimento_hidden_p').value = item.rendimento;
                o('venda_pessoa_p').value = item.venda_pessoa;
                o('custo_pessoa_p').value = item.custo_pessoa;
                o('descricao_p').value = item.descricao;
                o('margem_lucro_p').value = item.margem_lucro;

                o('imagem_item_p').src = item.url_image;
                 
                $('#Itens a[href="#tabItemPronto"]').tab('show');
            } else {

                o('id_item_m').value = id_item;
                o('botao_confirmar_m').value = "Atualizar";
                o("alerta_item_pronto").style.display = "none";

                o('nome_m').value = item.nome;
                o('id_categoria_m').value = item.id_categoria;
                o('id_unidade_medida_m').value = item.id_unidade_medida;
                o('quantidade_embalagem_m').value = item.quantidade_embalagem;
                o('valor_compra_m').value = item.valor_compra;
                o('rendimento_m').value = item.rendimento;
                o('venda_pessoa_m').value = item.venda_pessoa;
                o('custo_pessoa_m').value = item.custo_pessoa;
                o('descricao_m').value = item.descricao;
                o('receita_m').value = item.receita;
                o('margem_lucro_m').value = item.margem_lucro;
                 
                $("#tabelaIngredientes").empty(); 
                o('tabelaIngredientes').innerHTML = "<thead> <tr>  <th width='200'>Nome</th> <th>Quantidade</th> <th></th> </tr> </thead>";
                  
                o('itens_tabela').style.display = "none";
                o('itens_descricao').style.display = "block";

                for (var i = 0; i < item.ingredientes.length; i++) {
                    o('ingrediente_m').value = item.ingredientes[i].marca;
                    o('tipo_quantidade').value = item.ingredientes[i].quantidade;
                    AdicionarIngrediente(null);
                }
                o('ingrediente_m').value = "";
                o('tipo_quantidade').value = "";

                o('image_item_m').src = item.url_image;

                $('#Itens a[href="#tabMontarItem"]').tab('show'); 
            }
            $('#modal_itens').modal('hide');
            evt.value = "Editar";
        } else {
            mensagemError();
        }
    }, 'json');
}

function AtualizaStatusItem(evt, status, index, id_item) {
    if (o('item_status' + index).innerHTML != status) {
        o('item_status' + index).innerHTML = "Aguarde...";
        $.getJSON('/Estoque/AtualizarStatusItemEditar', { id_item: id_item, status: status }, function (data) {
            if (data) {
                o('item_status' + index).innerHTML = status;
            }
        });
    }
}
























function limparSelect(id) {

}

function adicionarSelect(id_para, id_de, value, text) {
    var selectobject = document.getElementById(id_de);
    for (var i = 0; i < selectobject.length; i++) {
        if (selectobject.options[i].value == value) {
            selectobject.remove(i);
            break;
        }
    }

    var select = document.getElementById(id_para);
    var option = document.createElement("option");
    option.value = value;
    option.innerHTML = text;
    select.appendChild(option);
}

function TabAtual(tab) {
    $('#Itens a[href="#' + tab + '"]').tab('show');
}

function mensagemError() {
    ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
}

function limpar(id_form) {
    o(id_form).reset();
}

function showOrHideEditar(id_button) {
    var button = o(id_button);
    if (button.style.display == "block") {
        button.style.display = "none";
    } else {
        button.style.display = "block";
    }
}