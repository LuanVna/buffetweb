﻿function myfunction() { 
    var currentUser = $('#userCount').text();
    $({ numberValue: 0 }).animate({ numberValue: currentUser }, {
        duration: 2500,
        easing: 'linear',
        step: function () {
            $('#userCount').text(Math.ceil(this.numberValue));
        }
    });
}