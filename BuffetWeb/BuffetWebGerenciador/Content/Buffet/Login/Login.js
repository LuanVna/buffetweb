﻿function VerificaUsuario(evt) {
    if (o('email').value == "" || o('senha').value == "") {
        return;
    }
    evt.value = "Aguarde...";
    $.getJSON('/Home/EntrarAdministrativo', { email: o('email').value, senha: o('senha').value, codigo: o('codigo').value }, function (resposta) {
        if (resposta.isOK) {
            evt.value = "Confirmar";
            if (o('codigo').value == "") {
                o('ShowCodigo').style.display = "block";
                Mensagem(resposta.mensagem);
            } else {
                if (resposta.codigo) {
                    window.location.reload();
                } else {
                    Mensagem("Código Inválido");
                }
            }
        } else {
            Mensagem("Verifique seu login");
            evt.value = "Entrar";
        }
    }, 'json');
}

function Mensagem(texto) {
    o('AlertCodigo').style.display = "block";
    o('AlertCodigo').innerHTML = "<strong>Informação</strong> " + texto;
}