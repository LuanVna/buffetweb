﻿function NovoCupom(evt, id_empresa) {
    $('#id_local_evento').append($('<option>', {
        value: "",
        text: "Carregando..."
    }));
    $.getJSON('/Marketing/BuscarInformacoes', { id_empresa: id_empresa }, function (data) {
        if (data.isOk) {
            o('id_empresa').value = id_empresa;
            o('disponivel_ate').value = "";
            o('descricao').value = "";
            o('valor_desconto').value = "";
            $('#id_local_evento').empty();
            if (data.local_evento.length > 1) {
                $('#id_local_evento').append($('<option>', {
                    value: "",
                    text: "Selecione"
                }));
            }
            $.each(data.local_evento, function (i, item) {
                $('#id_local_evento').append($('<option>', {
                    value: item.id,
                    text: item.nome
                }));
            });
        }
    }, 'json');
}