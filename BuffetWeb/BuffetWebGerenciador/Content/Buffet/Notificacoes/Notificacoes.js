﻿function PreviewMensagem(evt) {
    o('mesagem').innerHTML = evt.value
}

function SelecionarColaboradores(evt, index_empresa, qtd_colaboradores) {
    o('empresa' + index_empresa).value = evt.checked; 
    for (var i = 0; i < qtd_colaboradores; i++) {
        o('empresa_' + index_empresa + '_colaborador_' + i).checked = evt.checked;
        o('colaborador' + index_empresa + '' + i).value = evt.checked;
        o('empresa_' + index_empresa + '_tr_' + i).className = evt.checked ? "selected" : "";
    }
}

