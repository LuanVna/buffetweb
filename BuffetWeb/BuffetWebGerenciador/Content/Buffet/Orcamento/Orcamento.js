﻿function SelecionarTipoEvento(id_tipo_evento, evt_id, total) {
    o('id_tipo_evento').value = id_tipo_evento;
    for (var i = 0; i != total; i++) {
        o('evento' + i).innerHTML = "";
    }
    o('evento' + evt_id).innerHTML = '<div class="ribbon-inner shadow-pulse bg-warning"style=" font-size: 10px; ">Selecionado</div>'
}




function CarregarPacotes(evt, id_evento) {
    $.getJSON('/Orcamento/CarregarPacotes', { id_evento: id_evento }, function (data) {
        if (data) {
            MontarPacotetes(data);
        }
    }, 'json');
}


function MontarPacotetes(rows) {
    var pacotes = '';
    for (var row in rows) {
        pacotes += thumbnailPacotes(row.id_pacote, row.nome_pacote);
    }
    if (rows[0].permitir_personalizar) {
        pacotes += thumbnailPersonalizar();
    }
    o('thumbnailPacotes').innerHTML = pacotes;
}


function thumbnailPacotes(id_pacote, nome_pacote) {
    return '<div class="col-md-2 text-center"><a href="#"' +
            '<span>' + nome_pacote + '</span>' +
            '<img class="img-thumbnail" width="200" src="' + id_pacote + '" /></a></div>';
}

function thumbnailPersonalizar() {
    return '<div class="col-md-2 text-center"><a href="#"' +
            '<span>Personalizar</span>' +
            '<img class="img-thumbnail" width="200" src="" /></a></div>';
}

function VerificaForm(evt) {
    if (o('id_cliente').value == "") {
        o('texto_mensagem').innerHTML = "Selecione um Cliente";
        o('mensagem').style.display = "block";
        return false;
    } else if (o('id_tipo_evento').value == "") {
        o('texto_mensagem').innerHTML = "Selecione um Evento";
        o('mensagem').style.display = "block";
        return false;
    }
    return true;
}