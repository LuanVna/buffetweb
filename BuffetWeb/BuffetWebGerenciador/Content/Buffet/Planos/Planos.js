﻿function EditarPlano(evt, id_plano) {
    $.getJSON('/Planos/EditarPlano', { id_plano: id_plano }, function (data) {
        if (data.isOk) {
            data = data.plano;
            o('id_plano').value = data.id;
            o('id_plano_portal_').value = data.id_plano_portal;

            o('anuncio_portal').value = data.anuncio_portal;
            o('envio_proposta_portal').value = data.envio_proposta_portal;
            o('destaque_pesquisa_zona_portal').value = data.destaque_pesquisa_zona_portal;
            o('destaque_mapa_portal').value = data.destaque_mapa_portal;
            if (data.img_miniatura != "")
                o('_img_miniatura').src = data.img_miniatura;
            if (data.img_normal != "")
                o('_img_normal').src = data.img_normal;
             
            o('img_normal_').value = data.img_normal;
            o('img_miniatura_').value = data.img_miniatura;
            
            o('disponivel_de').value = data.disponivel_de;
            o('disponivel_ate').value = data.disponivel_ate;
            o('valor_anual').value = data.valor_anual;
            o('valor_trimestral').value = data.valor_trimestral;
            o('valor_mensal').value = data.valor_mensal;
            o('valor_semestral').value = data.valor_semestral;
            o('nome').value = data.nome;
            o('descricao').value = data.descricao;
        }
    }, 'json');
}

function AdicionarNovoPlanoPortal(evt) {
    $.getJSON('/Planos/AdicionarPlanosPortal', {
        id: o('id_plano_portal').value,
        nome: o('nome_plano_portal').value,
        destaque_pesquisa_zona: o('destaque_pesquisa_zona').value,
        destaque_mapa: o('destaque_mapa').value,
        envio_proposta: o('envio_proposta').value,
        envio_proposta_quantidade_mes: o('envio_proposta_quantidade_mes').value
    }, function (data) {
        o('planos_portal').insertRow = LinhaTabelaPlanosPortal(data);
        $('#NovoPlanoPortal').modal('hide');
    }, 'json');
}

function LinhaTabelaPlanosPortal(data) {
    return '<tr>' +
                '<td>' + data.nome + '</td>' +
                '<td>' +
                    '<input type="button" class="btn btn-sm btn-success" onclick="EditarNovoPlano(this, ' + data.id + ',' + data.nome + ',' + data.destaque_pesquisa_zona + ',' + data.destaque_mapa + ',' + data.envio_proposta + ',' + data.envio_proposta_quantidade_mes + ')" value="Editar">' +
                '</td>' +
            '</tr>';
}

function LimparNovoPlano() {
    o('id_plano_portal').value = "";
    o('nome_plano_portal').value = "";
    o('envio_proposta_quantidade_mes').value = "";
    o('destaque_pesquisa_zona').value = "False";
    o('destaque_mapa').value = "False";
    o('envio_proposta').value = "False";
    o('propostaMensal').style.display = "none";
    o('Adicionar_Portal').value = "Adicionar";
}

function EditarNovoPlano(evt, id, nome, destaque_pesquisa_zona, destaque_mapa, envio_proposta, envio_proposta_quantidade_mes) {
    o('id_plano_portal').value = id;
    o('nome_plano_portal').value = nome;
    o('destaque_pesquisa_zona').value = destaque_pesquisa_zona;
    o('destaque_mapa').value = destaque_mapa;
    o('envio_proposta').value = envio_proposta;
    if (envio_proposta == "True")
        o('propostaMensal').style.display = "block";
    else
        o('propostaMensal').style.display = "none";
    o('envio_proposta_quantidade_mes').value = envio_proposta_quantidade_mes;
    o('Adicionar_Portal').value = "Atualizar";
    $('#NovoPlanoPortal').modal('show');
}