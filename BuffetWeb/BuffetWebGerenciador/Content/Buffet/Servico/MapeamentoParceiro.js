﻿//$(document).ready(function () {
//    $.getJSON('/Servico/IdPareceiros', function (data) {
//        for (var i = 0; i < data.ids.length; i++) {
//            var id_parceiro = "#carousel_" + data.ids[i];
//            $(id_parceiro).owlCarousel({
//                items: 4,
//                lazyLoad: true,
//                navigation: true
//            });
//        }
//    }, 'json');
//});

function MapearSiteParceiro(evt, id_parceiro) {
    $.getJSON('/Servico/ForcarMapeamentoParceiro', { id_parceiro: id_parceiro }, function (data) {
        if (data.isOk) {
            o('email_' + id_parceiro).value = data.parceiro.email;
            o('telefone_' + id_parceiro).value = data.parceiro.telefone;
        }
    }, 'json');
}