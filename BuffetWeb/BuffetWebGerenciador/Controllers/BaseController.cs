﻿using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebGerenciador.Models;
using System.Data.Entity;

namespace BuffetWebGerenciador.Controllers
{
    public class BaseController : Controller
    { 
        public enum tipoMensagem
        {
            tipoSucesso,
            tipoErro,
            tipoAtencao,
            tipoInfo
        }

        private static string mensagem;
        private static string tipo; 

        public EMPRESA EMPRESA
        {
            get
            {
                HttpCookie keyEmpresa = Request.Cookies["KEY_EMPRESA_BUSINESS"];
                if (keyEmpresa != null)
                {
                    try
                    {
                        using (BuffetContext db = new BuffetContext())
                        {
                            return db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                        }
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
                return null;
            }
        }

        public int ID_EMPRESA
        {
            get
            {
                var em = this.EMPRESA;
                if (em != null)
                {
                    return em.id;
                }
                return -1;
            }
        }

        public RedirectToRouteResult LoginView
        {
            get
            {
                return RedirectToAction("", "");
            }
        }

        public RedirectToRouteResult IndexView
        {
            get
            {
                return RedirectToAction("", "");
            }
        }
        public RedirectToRouteResult NotModule
        {
            get
            {
                return RedirectToAction("ModuloNaoPermitido", "Modulos");
            }
        }

        public RedirectToRouteResult NotAcess
        {
            get
            {
                return RedirectToAction("AcessoNegado", "Modulos");
            }
        }

        public RedirectToRouteResult redirect(string action, string controller = "")
        {
            this.setMensagem("Você  não tem permissão para fazer isto!");
            return RedirectToAction(action, controller);
        }

        public B_EMPRESA_USUARIOS administrador
        {
            get { return BWColaborador.getAdmistrador(); }
        }

        public bool isLogin()
        { 
            return this.administrador != null;
        } 

        public bool Entrar(string email, string senha)
        {
            return BWColaborador.AutenticaAdministrador(email, senha);
        } 
        public void Sair()
        {
            BWColaborador.Deslogar();
        }

        public void getMensagem()
        {
            if (mensagem != null)
            {
                ViewBag.mensagem = mensagem;
                ViewBag.tipo = tipo;
                mensagem = null;
            }
        }

        public void setMensagem(string texto, tipoMensagem tipoMensagem = tipoMensagem.tipoInfo)
        {
            switch (tipoMensagem)
            {
                case tipoMensagem.tipoSucesso: tipo = "alert-sucess"; break;
                case tipoMensagem.tipoErro: tipo = "alert-danger"; break;
                case tipoMensagem.tipoAtencao: tipo = "alert-warning"; break;
                case tipoMensagem.tipoInfo: tipo = "alert-info"; break;
            }
            mensagem = string.Format(texto);
        }
    }
}
