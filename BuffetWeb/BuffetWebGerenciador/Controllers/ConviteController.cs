﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Controllers
{
    public class ConviteController : BaseController
    {
        public ActionResult Gerenciar()
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    base.getMensagem();
                    return View(db.B_CONVITES_DISPONIVEIS.ToList());
                }
            }
            return base.IndexView;
        }

        public ActionResult SalvarNovoConvite(HttpPostedFileBase image, B_CONVITES_DISPONIVEIS convite, bool editando)
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    try
                    {
                        if (editando)
                        {
                            //entities.Students.Attach(entities.Students.Single(c => c.ID == student.ID));
                            //entities.Students.ApplyCurrentValues(student);
                            //entities.Savechanges(); 
                            db.Entry(db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == convite.id)).CurrentValues.SetValues(convite);

                            //db.Entry(convite).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            convite.disponivel = true;
                            db.B_CONVITES_DISPONIVEIS.Add(convite);
                        }
                        if (convite.url_image == null)
                        {
                            convite.url_image = "";
                        }

                        db.SaveChanges();
                        if (image != null)
                        {
                            Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadBusiness("Convites", convite.id.ToString(), image.InputStream);
                            if (resposta.Item1)
                            {
                                convite.url_image = resposta.Item2;
                                db.SaveChanges();
                            }
                            else
                            {
                                base.setMensagem("Ouve algum erro ao tentar subir a imagem para o servidor!");
                            }
                        }
                        if (!editando)
                            base.setMensagem("Convite adicionadom com sucesso!");
                        else
                            base.setMensagem("Convite editado com sucesso!");
                    }
                    catch (Exception e)
                    {
                        base.setMensagem("Ouve algum erro ao tentar subir a imagem para o servidor!");
                    }
                    return RedirectToAction("Gerenciar");
                }
            }
            return base.IndexView;
        }

        public ActionResult AtualizarCaminhoFonte(string caminhoFonte)
        {
            if (base.isLogin())
            {
                if (!caminhoFonte.Equals("") && caminhoFonte.Contains("http://fonts.googleapis.com/css?family="))
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        B_EMPRESA_AJUSTES a = db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("FONTES"));
                        a.valor = caminhoFonte;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Gerenciar");
            }
            return base.IndexView;
        }

        public ActionResult AdicionaFonteBuffetWeb(HttpPostedFileBase fonte, string nome_visivel, string nome_fonte)
        {
            if (base.isLogin())
            {
                Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadFonte(nome_fonte, fonte.InputStream);
                using (BuffetContext db = new BuffetContext())
                {
                    string css = "@font-face{ font-family:\"" + nome_fonte + "\";font-style: normal;}font-weight: normal;src: url(\"" + resposta.Item2 + "\"); format(\"truetype\");";
                    db.B_CONVITES_FONTES.Add(new B_CONVITES_FONTES()
                     {
                         caminho_fonte = resposta.Item2,
                         nome_fonte = nome_fonte,
                         nome_visivel = nome_visivel,
                         css = css
                     });
                    db.SaveChanges();
                }
                return RedirectToAction("Gerenciar");
            }
            return base.IndexView;
        }

        public JsonResult BuscarFonteBuffetWeb()
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return Json(db.B_CONVITES_FONTES.ToList(), JsonRequestBehavior.AllowGet);
                }
            }
            return Json("load", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApagarFonte(int id_fonte)
        {
            if (base.isLogin())
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        db.B_CONVITES_FONTES.Remove(db.B_CONVITES_FONTES.FirstOrDefault(i => i.id == id_fonte));
                        db.SaveChanges();
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("load", JsonRequestBehavior.AllowGet);
        }

        //DESCRICAO FRASES
        public JsonResult SalvarFrase(B_CONVITES_FRASES frase)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.B_CONVITES_FRASES.Add(frase);
                    db.SaveChanges();
                    return Json(new { isOk = true, id = frase.id, titulo = frase.titulo, descricao = frase.descricao }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ApagarFrase(int id_frase)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.B_CONVITES_FRASES.Remove(db.B_CONVITES_FRASES.FirstOrDefault(e => e.id == id_frase));
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarFrases()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return Json(new { frases = db.B_CONVITES_FRASES.ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExcluirConvite(int? id_convite_excluir)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    if (id_convite_excluir != null)
                    {
                        db.B_CONVITES_DISPONIVEIS.Remove(db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == (int)id_convite_excluir));
                        db.SaveChanges();
                    }
                    base.setMensagem("Convite excluido com sucesso!");
                }
                catch (Exception e)
                {
                    base.setMensagem("Ouve um erro ao tentar excluir o convite!");
                }
            }
            return RedirectToAction("Gerenciar");
        }

        public JsonResult BuscarConvitesPorTema(string tema)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (tema.Equals("semFiltro"))
                { 
                    return Json(db.B_CONVITES_DISPONIVEIS.ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(db.B_CONVITES_DISPONIVEIS.Where(e => e.tema.Equals(tema)).ToList(), JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
