﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebData.Utils;
using BuffetWebSerasa.Methods;

namespace BuffetWebGerenciador.Controllers
{
    public class EmpresasController : BaseController
    {
        //
        // GET: /Empresas/

        public ActionResult Empresas(string acao, string controle)
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (acao != null)
                    {
                        ViewBag.acao = acao;
                        ViewBag.controle = controle;
                    }
                    return View(db.EMPRESAs.ToList());
                }
            }
            return base.IndexView;
        }

        public ActionResult SelecionarEmpresa(string codigo_empresa, string acao, string controle)
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(codigo_empresa));
                    if (empresa != null)
                    {
                        HttpCookie cookie = new HttpCookie("KEY_EMPRESA_BUSINESS", empresa.codigo_empresa);
                        cookie.Expires = DateTime.Now.AddMonths(1);
                        HttpContext.Response.SetCookie(cookie);
                    }
                }
                if (acao != null)
                {
                    return RedirectToAction(acao, controle);
                }
                return RedirectToAction("Empresas");
            }
            return base.IndexView;
        }

        public ActionResult NovaEmpresa()
        {
            return View();
        }

        public ActionResult SalvarEmpresa(EMPRESA empresa, bool salvarComoLocalDeEvento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                empresa.codigo_empresa = UtilsManager.CriarAutenticacaoEmpresa();
                db.EMPRESAs.Add(empresa);
                db.SaveChanges();
            }
            base.setMensagem("Nova empresa adicionada", tipoMensagem.tipoSucesso);
            return RedirectToAction("NovaEmpresa", "Empresas");
        }

        public ActionResult NovoLocalDeEvento()
        {
            if (EMPRESA == null)
            {
                base.setMensagem("Primeiro você precisa selecionar uma empresa");
                return RedirectToAction("Empresas", "Empresas");
            }
            return View();
        }

        public ActionResult SalvarLocalDeEvento(LOCAL_EVENTO localEvento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                localEvento.autenticacao = UtilsManager.CriarAutenticacaoEmpresa();

                db.LOCAL_EVENTO.Add(localEvento);
                db.SaveChanges();
                base.setMensagem("Nova empresa adicionada", tipoMensagem.tipoSucesso);
                return RedirectToAction("NovoLocalDeEvento", "Empresas");
            }
        }

        public JsonResult BuscarCNPJ(string cnpj)
        {
            using (BuffetContext db = new BuffetContext())
            {
                SerasaEmpresa r = new SerasaEmpresa(cnpj);
                return Json(new { isOk = true, empresa = r.informacoesEmpresa() });
            }
        }
    }
}
