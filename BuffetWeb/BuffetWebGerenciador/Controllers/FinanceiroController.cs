﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Controllers
{
    public class FinanceiroController : Controller
    {
        public ActionResult Financeiro()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var historico = db.EMPRESA_HISTORICO_PAGAMENTO.Include(e => e.EMPRESA).ToList();
                return View(historico);
            }
        }
    }
}
