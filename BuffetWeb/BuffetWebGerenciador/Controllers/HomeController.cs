﻿using BuffetWebData.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace BuffetWebGerenciador.Controllers
{
    public class HomeController : BaseController
    { 
        public ActionResult Index()
        {
            if (base.isLogin())
            {
                return View();
            }
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            if (base.isLogin())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public JsonResult EntrarAdministrativo(string email, string senha, string codigo)
        {
            using (BuffetContext db = new BuffetContext())
            {
                senha =  RLSecurity.Security.CriptographyOn(senha);
                if (codigo == null || codigo.Equals(""))
                {
                    B_EMPRESA_USUARIOS usuario = db.B_EMPRESA_USUARIOS.FirstOrDefault(e => e.email == email && e.senha == senha);
                    if (usuario != null)
                    {
                        string cod = new Random().Next(0, 9999).ToString();
                        //bool resposta = UtilsManager.sendSMS(usuario.celular, string.Format("BuffetWeb: {0}, seu codigo de acesso: {1}", usuario.nome, cod));
                        //if (resposta)
                        if (true)
                        {
                            usuario.autenticacao =  RLSecurity.Security.CriptographyOn(cod);
                            db.SaveChanges();
                            return Json(new { isOK = true, mensagem = string.Format("Código de acesso: {0}", cod) }, JsonRequestBehavior.AllowGet);
                            //return Json(new { isOK = true, mensagem = string.Format("Enviamos o código de acesso para o número: xxxx-x{0}", usuario.celular.Substring(usuario.celular.Length - 3, 3)) }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    B_EMPRESA_USUARIOS usuario = db.B_EMPRESA_USUARIOS.FirstOrDefault(e => e.email == email && e.senha == senha);
                    if (usuario != null)
                    {
                        if (usuario.autenticacao.Equals(RLSecurity.Security.CriptographyOn(codigo)))
                        {
                            base.Entrar(usuario.email, usuario.senha);
                            return Json(new { isOK = true, codigo = true }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { isOK = true, codigo = false }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { isOK = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Deslogar()
        {
            return RedirectToAction("Login");
        }
    }
}
