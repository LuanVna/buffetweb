﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebGerenciador.Models.Marketing;

namespace BuffetWebGerenciador.Controllers
{
    public class MarketingController : BaseController
    {
        public ActionResult CupomDeDesconto()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(new CupomDeDescontoModel());
            }
        }

        public ActionResult BannerPromocional()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.B_PORTAL_BANNER_PROMOCIONAL.ToList());
            }
        }

        public ActionResult AdicionarBanner(B_PORTAL_BANNER_PROMOCIONAL banner, HttpPostedFileBase imagem)
        {
            using (BuffetContext db = new BuffetContext())
            {
                banner.url_imagem = "";
                db.B_PORTAL_BANNER_PROMOCIONAL.Add(banner);
                db.SaveChanges();
                if (imagem != null)
                {
                    var resposta = FTPManager.UploadImageSistema("PortalBanner", banner.id.ToString(), imagem.InputStream);
                    if (resposta.Item1)
                    {
                        banner.url_imagem = resposta.Item2;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("BannerPromocional");
            }
        }

        public ActionResult ApagarBanner(int id_banner)
        {
            using (BuffetContext db = new BuffetContext())
            {
                db.B_PORTAL_BANNER_PROMOCIONAL.Remove(db.B_PORTAL_BANNER_PROMOCIONAL.FirstOrDefault(e => e.id == id_banner));
                db.SaveChanges();
                return RedirectToAction("BannerPromocional");
            }
        }

        public ActionResult PromocaoPortal()
        {
            return View();
        }

        public ActionResult AdicionarCupom(EMPRESA_CARRINHO_CUPOM cupom, List<LocaisSelecionados> locais, bool enviar_email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                cupom.usado = false;
                foreach (var local in locais.Where(s => s.selecionado == true).ToList())
                {
                    cupom.id_local_evento = local.id_local;
                    db.EMPRESA_CARRINHO_CUPOM.Add(cupom);
                }
                db.SaveChanges();
                base.setMensagem("Cupom adicionado");
                return RedirectToAction("CupomDeDesconto");
            }
        }
    }
}
