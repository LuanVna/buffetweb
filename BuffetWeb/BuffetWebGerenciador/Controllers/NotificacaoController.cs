﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebGerenciador.Models.Notificacao;
using BuffetWebData.Utils;

namespace BuffetWebGerenciador.Controllers
{
    public class NotificacaoController : BaseController
    {
        public static NotificacaoModel notificacoes { get; set; }
        public ActionResult Notificacoes()
        {
            if (base.isLogin())
            {
                notificacoes = new NotificacaoModel() { id_empresa = base.ID_EMPRESA };
                return View(notificacoes);
            }
            return base.IndexView;
        }

        public ActionResult SalvarNotificacoes(NOTIFICACO notificacao, List<ColaboradorSelecionado> colaboradoresSelecionados)
        {
            if (base.isLogin())
            {
                try
                {

                    using (BuffetContext db = new BuffetContext())
                    {
                        notificacao.deBuffetWeb = true;
                        notificacao.data = DateTime.Now;
                        foreach (var colaborador in colaboradoresSelecionados.Where(e => e.colaborador_selecionado == true).ToList())
                        {
                            COLABORADORE cola = notificacoes.colaboradorresESelecionada.FirstOrDefault(e => e.id == colaborador.id_colaborador);
                            notificacao.para_id_colaborador = cola.id;
                            notificacao.id_empresa = cola.id_empresa;

                            new UtilsManager(base.ID_EMPRESA).sendNotificacao(notificacao);

                            //db.NOTIFICACOES.Add(notificacao);
                        }
                        //db.SaveChanges();

                        base.setMensagem("Notificações enviadas com sucesso");
                    }
                }
                catch (Exception e)
                {
                    base.setMensagem("Ouve algum erro");
                }
                return RedirectToAction("Notificacoes");
            }
            return base.IndexView;
        }
        public ActionResult SalvarNotificacoesEmpresas(NOTIFICACO notificacao, List<EmpresaSelecionada> empresas, List<ColaboradorSelecionado> colaboradores)
        {
            if (base.isLogin())
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        notificacao.deBuffetWeb = true;
                        notificacao.data = DateTime.Now;
                        foreach (var empresa in empresas.Where(e => e.empresa_selecionada == true).ToList())
                        {
                            foreach (var colaborador in colaboradores.Where(e => e.colaborador_selecionado == true && e.id_empresa == empresa.id_empresa).ToList())
                            {
                                COLABORADORE cola = notificacoes.colaboradorresESelecionada.FirstOrDefault(e => e.id == colaborador.id_colaborador);
                                notificacao.para_id_colaborador = colaborador.id_colaborador; 
                                notificacao.id_empresa = cola.id_empresa;
                                db.NOTIFICACOES.Add(notificacao); 
                            }
                        }
                        db.SaveChanges();
                        base.setMensagem("Notificações enviadas com sucesso");
                    }
                }
                catch (Exception e)
                {
                    base.setMensagem("Ouve algum erro");
                }
                return RedirectToAction("Notificacoes");
            }
            return base.IndexView;
        }
    }
}



