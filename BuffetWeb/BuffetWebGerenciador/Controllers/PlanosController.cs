﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebGerenciador.Models.Planos;

namespace BuffetWebGerenciador.Controllers
{
    public class PlanosController : Controller
    {
        public ActionResult Planos()
        {
            return View(new PlanosModel());
        }

        [ValidateInput(false)]
        public ActionResult AdicionarPlano(B_PLANOS_DISPONIVEIS disponivel, HttpPostedFileBase img_miniatura_base, HttpPostedFileBase img_normal_base, int? id_plano)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id_plano == null)
                    {
                        disponivel.img_normal = "";
                        disponivel.img_miniatura = "";
                        db.B_PLANOS_DISPONIVEIS.Add(disponivel);
                        db.SaveChanges();
                        if (img_miniatura_base != null)
                            disponivel.img_miniatura = FTPManager.UploadImageSistema("Planos", string.Format("{0}_img_miniatura.png", disponivel.id), img_miniatura_base.InputStream).Item2;

                        if (img_normal_base != null)
                            disponivel.img_normal = FTPManager.UploadImageSistema("Planos", string.Format("{0}_img_normal.png", disponivel.id), img_normal_base.InputStream).Item2;
                        db.SaveChanges();
                    }
                    else
                    {
                        var d = db.B_PLANOS_DISPONIVEIS.FirstOrDefault(e => e.id == id_plano);
                        d.nome = disponivel.nome;
                        d.descricao = disponivel.descricao;
                        d.disponivel_ate = disponivel.disponivel_ate;
                        d.disponivel_de = disponivel.disponivel_de;
                        d.id_plano_portal = disponivel.id_plano_portal;
                        d.valor_anual = disponivel.valor_anual;
                        d.valor_mensal = disponivel.valor_mensal;
                        d.valor_trimestral = disponivel.valor_trimestral;
                        d.valor_semestral = disponivel.valor_semestral;
                        db.SaveChanges();
                        if (img_miniatura_base != null)
                            d.img_miniatura = FTPManager.UploadImageSistema("Planos", string.Format("{0}_img_miniatura.png", d.id), img_miniatura_base.InputStream).Item2;

                        if (img_normal_base != null)
                            d.img_normal = FTPManager.UploadImageSistema("Planos", string.Format("{0}_img_normal.png", d.id), img_normal_base.InputStream).Item2;
                        db.SaveChanges();
                    } 
                    return RedirectToAction("Planos");
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Planos");
            }
        }

        public JsonResult AdicionarPlanosPortal(B_PLANOS_PORTAL portal)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (portal.id == 0)
                {
                    db.B_PLANOS_PORTAL.Add(portal);
                }
                else
                {
                    db.B_PLANOS_PORTAL.Attach(portal);
                    db.Entry(portal).State = EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new
                {
                    isOk = true,
                    id = portal.id,
                    nome = portal.nome,
                    destaque_pesquisa_zona = portal.destaque_pesquisa_zona,
                    destaque_mapa = portal.destaque_mapa,
                    envio_proposta = portal.envio_proposta,
                    envio_proposta_quantidade_mes = portal.envio_proposta_quantidade_mes,
                    anuncio = portal.anuncio
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditarPlano(int id_plano)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var plano = db.B_PLANOS_DISPONIVEIS.Include(p => p.B_PLANOS_PORTAL)
                                                   .Where(e => e.id == id_plano)
                                                   .Select(e => new
                                                   {
                                                       e.id,
                                                       e.id_plano_portal,
                                                       anuncio_portal = e.B_PLANOS_PORTAL.anuncio,
                                                       envio_proposta_portal = e.B_PLANOS_PORTAL.envio_proposta,
                                                       destaque_pesquisa_zona_portal = e.B_PLANOS_PORTAL.destaque_pesquisa_zona,
                                                       destaque_mapa_portal = e.B_PLANOS_PORTAL.destaque_mapa,
                                                       img_miniatura = e.img_miniatura,
                                                       img_normal = e.img_normal,
                                                       disponivel_de = e.disponivel_ate,
                                                       disponivel_ate = e.disponivel_ate,
                                                       valor_anual = e.valor_anual,
                                                       valor_trimestral = e.valor_trimestral,
                                                       valor_mensal = e.valor_mensal,
                                                       valor_semestral = e.valor_semestral,
                                                       nome = e.nome,
                                                       descricao = e.descricao
                                                   }).FirstOrDefault();
                return Json(new { isOk = true, plano = plano }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Promocao()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.B_PORTAL_PROMOCAO.ToList());
            }
        }

        [ValidateInput(false)]
        public ActionResult AdicionarPromocao(B_PORTAL_PROMOCAO promocao, HttpPostedFileBase imagem)
        {
            using (BuffetContext db = new BuffetContext())
            {
                promocao.url_imagem = "";
                db.B_PORTAL_PROMOCAO.Add(promocao);
                db.SaveChanges();

                promocao.url_imagem = FTPManager.UploadImageSistema("PortalPromocoes", string.Format("{0}_imagem.png", promocao.id), imagem != null ? imagem.InputStream : null).Item2;

                db.SaveChanges();

                return RedirectToAction("Promocao");
            }
        }
    }
}
