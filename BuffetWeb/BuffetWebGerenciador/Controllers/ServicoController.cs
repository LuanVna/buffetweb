﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebGerenciador.Models.Servico;
using BuffetNaWebService.Mapeamento;

namespace BuffetWebGerenciador.Controllers
{
    public class ServicoController : Controller
    {
        public ActionResult MapeamentoParceiro()
        {
            return View(new MapeamentoParceiro());
        }

        public JsonResult IdPareceiros()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var ids = db.B_SERVICO_PARCEIRO_MAPEADO.Select(e => new { e.id }).ToList();
                return Json(new { ids = ids }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ForcarMapeamentoParceiro(int id_parceiro)
        {
            MapearConcorrente mapear = new MapearConcorrente();
            ParceiroAtualizado parceiro = mapear.ForcarMapeamento(id_parceiro);
            if (parceiro != null)
            {
                return Json(new { isOk = true, email = parceiro.email, telefone = parceiro.telefone, logo = parceiro.logo }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { isOk = true });
        }
    }
}