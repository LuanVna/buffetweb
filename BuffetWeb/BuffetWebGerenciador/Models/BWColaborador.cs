﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BuffetWebGerenciador.Models
{
    public class BWColaborador
    {
        private static int? idColaborador()
        {
            try
            {
                return Convert.ToInt32(HttpContext.Current.User.Identity.Name);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool isLogin()
        {
            if (getAdmistrador() != null)
                return true;
            return false;
        }

        public static int isLoginID()
        {
            return getAdmistrador().id;
        }

        public static B_EMPRESA_USUARIOS getAdmistrador()
        { 
            int? _idAdministrador = idColaborador();
            if (_idAdministrador == null)
            {
                return null;
            }
            else
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        B_EMPRESA_USUARIOS administrador = db.B_EMPRESA_USUARIOS.FirstOrDefault(a => a.id == _idAdministrador);
                        if (administrador != null)
                        {
                            //if (sessao != null)
                            //{
                            //    if (administrador.sessao.Equals(sessao))
                            //    {
                            //        sessao_alterada = false;
                            return administrador;
                            //    }
                            //    else
                            //    {
                            //        sessao_alterada = true;
                            //        return null;
                            //    }
                            //}
                            //sessao_atual = null;
                            //return null;
                        }
                        return administrador;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public static bool sessao_alterada { get; set; }

        private static string sessao
        {
            get
            {
                if (sessao_atual == null)
                {
                    sessao_atual = HttpContext.Current.Session.SessionID;
                }
                return sessao_atual;
            }
        }

        private static string sessao_atual { get; set; }
        public static bool AutenticaAdministrador(string email, string Senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    B_EMPRESA_USUARIOS usuario = db.B_EMPRESA_USUARIOS.FirstOrDefault(u => u.email.ToLower() == email.ToLower() && u.senha == Senha);
                    if (usuario == null)
                        return false;

                    FormsAuthentication.SetAuthCookie(usuario.id.ToString(), true);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public static void Deslogar()
        {
            FormsAuthentication.SignOut();
        }
    }
}