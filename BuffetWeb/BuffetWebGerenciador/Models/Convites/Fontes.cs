﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebGerenciador.Models.Convites
{
    public class Fontes
    {
        public string familia { get; set; }
        public string visivel { get; set; } 
        public string css { get; set; } 
        public int id { get; set; }
    }
}