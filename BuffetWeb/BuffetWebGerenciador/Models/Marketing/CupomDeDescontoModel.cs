﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Models.Marketing
{
    public class CupomDeDescontoModel
    {
        public List<EMPRESA> Empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.Include(l => l.LOCAL_EVENTO)
                                         .Include(c => c.EMPRESA_CARRINHO_CUPOM.Select(f => f.B_PLANOS_DISPONIVEIS))
                                         .Include(d => d.EMPRESA_CARRINHO.Select(f => f.B_PLANOS_DISPONIVEIS))
                                         .Include(f => f.EMPRESA_CARRINHO.Select(d => d.LOCAL_EVENTO))
                                         .ToList();
                }
            }
        }
        public List<B_PLANOS_DISPONIVEIS> Planos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_PLANOS_DISPONIVEIS.ToList();
                }
            }
        }
    }
}