﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebGerenciador.Models.Marketing
{
    public class LocaisSelecionados
    {
        public int id_local { get; set; }
        public bool selecionado { get; set; }
    }
}