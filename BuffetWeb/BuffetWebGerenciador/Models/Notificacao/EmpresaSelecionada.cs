﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebGerenciador.Models.Notificacao
{
    public class EmpresaSelecionada
    { 
        public int id_empresa { get; set; }
        public bool empresa_selecionada { get; set; } 
    }
}