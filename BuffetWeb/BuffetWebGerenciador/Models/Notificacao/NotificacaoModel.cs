﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebGerenciador.Models.Notificacao
{
    public class NotificacaoModel
    {
        public int id_empresa { get; set; }
        public List<COLABORADORE> colaboradorresESelecionada
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.COLABORADORES.Include(e => e.PERFIL_COLABORADOR).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<EMPRESA> empresas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.Include(c => c.COLABORADORES.Select(p => p.PERFIL_COLABORADOR)).ToList();
                }
            }
        }
    }
}