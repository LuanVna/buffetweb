﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Models
{
    public class PerfilEmpresa
    {
        public static EMPRESA empresaSelecionada(HttpRequestBase request)
        {
            HttpCookie keyEmpresa = request.Cookies["KEY_EMPRESA_BUSINESS"];
            if (keyEmpresa != null)
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                        if (empresa != null)
                        {
                            return empresa;
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return null;
        }
    }
}