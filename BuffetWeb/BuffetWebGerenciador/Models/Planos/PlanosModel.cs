﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Models.Planos
{
    public class PlanosModel
    {
        public List<B_PLANOS_DISPONIVEIS> planosDisponiveis
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_PLANOS_DISPONIVEIS.Include(e => e.B_PLANOS_PORTAL).ToList();
                }
            }
        }

        public List<B_PLANOS_PORTAL> planosPortal
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_PLANOS_PORTAL.ToList();
                }
            }
        }
    }
}