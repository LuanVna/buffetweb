﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebGerenciador.Models.Servico
{
    public class MapeamentoParceiro
    {
        public List<B_SERVICO_MAPEAR_REGIAO> Regiao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_SERVICO_MAPEAR_REGIAO.ToList();
                }
            }
        }

        public List<B_SERVICO_PARCEIRO_MAPEADO> Parceiro
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_SERVICO_PARCEIRO_MAPEADO.Include(e => e.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS).ToList();
                }
            }
        }
    }
}