﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Web.Mvc;
using System.Web.Routing;
using BuffetWebGerenciador.Models;
using BuffetWebData.Utils;
using BuffetWebGerenciador.Models.Convites;

namespace BuffetWebGerenciador.Helpers
{
    public enum Modulo
    {
        Financeiro,
        Estoque,
        Orcamento,
        Galeria,
        Convite
    }

    public enum Permitir
    {
        Criar,
        Visualizar
    }

    public enum Conteudo
    {
        Ajustes,
        Clientes,
        Convites,
        Colaborador,
        Configuracoes,
        Eventos,
        Estoque,
        Fornecedor,
        Financeiro,
        Galeria,
        Ingredientes,
        Itens,
        Orcamentos,
        TipoEvento,
        Pacotes,
        Visitas
    }

    public enum Perfil
    {
        Nome,
        Imagem
    }


    public enum MensagemDe
    {
        Sucesso,
        Erro,
        Atencao
    }

    public static class BuffetWebHelper
    {
        public static MvcHtmlString NumeroParcelas(this HtmlHelper helper, string name, int maxLength, bool required = false)
        {
            using (BuffetContext db = new BuffetContext())
            {
                TagBuilder select = createSelect(name, required);
                for (int i = 1; i < maxLength; i++)
                {
                    createOption(select, i.ToString(), i.ToString());
                }
                return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
            }
        }

        public static MvcHtmlString BancosDisponiveis(this HtmlHelper helper, string name, bool required = false)
        {
            TagBuilder select = createSelect(name, required);
            createOption(select, "itau", "Itau");
            createOption(select, "Bradesco", "Bradesco");
            createOption(select, "Santander", "Santander");
            createOption(select, "HSBC", "HSBC");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Estados(this HtmlHelper helper, string name, bool required = false)
        {

            TagBuilder select = createSelect(name, required);
            createOption(select, "SP", "SP");
            createOption(select, "RJ", "RJ");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Cidade(this HtmlHelper helper, string name, bool required = false)
        {
            TagBuilder select = createSelect(name, required);
            createOption(select, "São Paulo", "São Paulo");
            createOption(select, "Rio de Janeiro", "Rio de Janeiro");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString FontesCSS(this HtmlHelper helper)
        {
            return new MvcHtmlString(new UtilsManager().CSSFontes());
        }

        public static MvcHtmlString SelecionarImagem(this HtmlHelper helper, string nomeImagem, string nomeFile, string CallBack = "", int heigth = 100, int width = 100)
        {
            string imageFile = @"<input type='file' name='" + nomeFile + "' style='display:none' id='" + nomeFile + "'  onchange=\"arquivoSelecionado(this, '" + nomeImagem + "'" + (CallBack != "" ? "," : "") + CallBack + ")\" />" +
                                "<a href='#' onclick=\"o('" + nomeFile + "').click()\">" +
                               "<img style=\"width: " + width + "px; height: " + heigth + "px;\" id=\"" + nomeImagem + "\" class=\"img-thumbnail\" alt=" + string.Format("{0}x{1}", heigth, width) + " src=\"http://placehold.it/" + width + "x" + heigth + "/EFEFEF/EBBB5E\"></a>";
            return new MvcHtmlString(imageFile);
        }


        public static MvcHtmlString URLImagem(this HtmlHelper helper, string url)
        {
            return new MvcHtmlString(string.Format("{0}", url != null || url != "" ? url : ""));
        }

        public static MvcHtmlString EmpresaSelecionada(this HtmlHelper helper, HttpRequestBase request)
        {
            EMPRESA empres = Empresa(request);
            if (empres != null)
            {
                string alert = @"<div class='alert alert-default' role='alert' style=' height: 63px; '>" +
                                "<h4 class='col-md-6' style=' margin-top: 8px; '>" + string.Format("Empresa selecionada <strong>{0}</strong> ", empres.nome) +
                                "</h2><a href='/Empresas/Empresas' style=' margin-top: 4px; ' class='btn btn-success col-md-2 pull-right'>Selecionar outra</a></div>";
                return new MvcHtmlString(alert);
            }
            return null;
        }

        public static MvcHtmlString BuffetCheck(this HtmlHelper helper, string titulo, string nameAndID = "", bool isCkeck = true, string callBack = "")
        {
            return new MvcHtmlString("<div style='padding:5px'>" +
                                      "<input type='hidden' name='" + nameAndID + "' id='" + nameAndID + "' value='" + (isCkeck ? "true" : "false") + "' />" +
                                      "<a id='check_" + nameAndID + "' style='margin-right: 5px;' href='javascript:void(o)' onclick='CheckOrUncheck(this, \"" + nameAndID + "\");" + callBack + "' data-check='" + (isCkeck ? "true" : "false") + "'>" +
                                            "<img src='/Content/Imagens/icones/icone_" + (isCkeck ? "check" : "uncheck") + ".png' width='30' />" +
                                       "</a>" +
                                      "<label style='cursor:pointer' onclick='CheckOrUncheck(o('check_" + nameAndID + "'), \"" + nameAndID + "\"); " + callBack + "'>" + titulo + " </label>" +
                                     "</div>");
        }

        public static EMPRESA Empresa(HttpRequestBase request)
        {
            HttpCookie keyEmpresa = request.Cookies["KEY_EMPRESA_BUSINESS"];
            if (keyEmpresa != null)
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                        if (empresa != null)
                        {
                            return empresa;
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return null;
        }

        public static List<Fontes> fontesDisponiveis(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<Fontes> font = new List<Fontes>();
                List<B_CONVITES_FONTES> fontesBuffetWeb = db.B_CONVITES_FONTES.ToList();
                foreach (var fontB in fontesBuffetWeb)
                {
                    font.Add(new Fontes() { familia = fontB.nome_fonte, visivel = fontB.nome_visivel, css = fontB.css, id = fontB.id });
                }

                //string fontes = db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("FONTES")).valor;
                //fontes = fontes.Replace("http://fonts.googleapis.com/css?family=", "").Replace("+", " ");
                //string[] fonte = fontes.Split('|');
                //for (int i = 0; i < fonte.Length; i++)
                //{
                //    font.Add(new Fontes() { familia = fonte[i], visivel = string.Format("G - {0}", fonte[i])});
                //} 
                return font;
            }
        }

        public static string CaminhoFontes(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("FONTES")).valor;
            }
        }

        public static MvcHtmlString AddMensagem(this HtmlHelper helper, string mensagem, MensagemDe mensagemDe)
        {
            if (mensagem != null)
            {
                string alert = @"<div class='alert alert-" + alerta(mensagemDe) + "' role='alert'>" +
                                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>" +
                                "<strong>" + textoAlerta(mensagemDe) + "!</strong> " + mensagem + " </div>";
                return new MvcHtmlString(alert);
            }
            return new MvcHtmlString("");
        }

        private static string alerta(MensagemDe mensagemde)
        {
            switch (mensagemde)
            {
                case MensagemDe.Sucesso: return "success";
                case MensagemDe.Erro: return "danger";
                case MensagemDe.Atencao: return "warning";
            }
            return "";
        }

        private static string textoAlerta(MensagemDe mensagemde)
        {
            switch (mensagemde)
            {
                case MensagemDe.Sucesso: return "Sucesso";
                case MensagemDe.Erro: return "Erro";
                case MensagemDe.Atencao: return "Atenção";
            }
            return "";
        }

        #region PERFIL
        public static string PerfilLogado(this HtmlHelper helper, Perfil modulo)
        {
            B_EMPRESA_USUARIOS colaborador = BWColaborador.getAdmistrador();
            if (colaborador != null)
            {
                switch (modulo)
                {
                    case Perfil.Nome: return colaborador.nome;
                    case Perfil.Imagem: return ""; // colaborador.url;
                }
            }
            return "";
        }

        public static bool Permissoes(this HtmlHelper helper, Permitir permitir, Conteudo conteudo)
        {
            return true;
            //B_EMPRESA_USUARIOS colaborador = BWColaborador.getAdmistrador();
            //if (perfil != null)
            //{
            //    switch (permitir)
            //    {
            //        case Permitir.Criar:
            //            {
            //                switch (conteudo)
            //                {
            //                    case Conteudo.Itens: return perfil.c_itens;
            //                    case Conteudo.Ajustes: return true;
            //                    case Conteudo.Clientes: return perfil.c_clientes;
            //                    case Conteudo.Convites: return perfil.c_convite;
            //                    case Conteudo.Colaborador: return perfil.c_colaborador;
            //                    case Conteudo.Fornecedor: return perfil.c_fornecedores;
            //                    case Conteudo.Ingredientes: return perfil.c_itens;
            //                    case Conteudo.Eventos: return perfil.c_eventos;
            //                    case Conteudo.TipoEvento: return perfil.c_tipo_eventos;
            //                    case Conteudo.Financeiro: return perfil.c_financeiro;
            //                    case Conteudo.Galeria: return perfil.c_galeria;
            //                    case Conteudo.Orcamentos: return perfil.c_orcamentos;
            //                    case Conteudo.Pacotes: return perfil.c_pacotes;
            //                    case Conteudo.Visitas: return perfil.c_visitas; 
            //                    case Conteudo.Configuracoes: return perfil.c_configuracao;
            //                    case Conteudo.Estoque: return perfil.c_estoque;
            //                }
            //            }; break;
            //        case Permitir.Visualizar:
            //            {
            //                switch (conteudo)
            //                {
            //                    case Conteudo.Itens: return perfil.v_itens;
            //                    case Conteudo.Ajustes: return true;
            //                    case Conteudo.Clientes: return perfil.v_clientes;
            //                    case Conteudo.Convites: return perfil.v_convite;
            //                    case Conteudo.Colaborador: return perfil.v_colaborador;
            //                    case Conteudo.Fornecedor: return perfil.v_fornecedores;
            //                    case Conteudo.Ingredientes: return perfil.v_itens;
            //                    case Conteudo.Eventos: return perfil.v_eventos;
            //                    case Conteudo.TipoEvento: return perfil.v_tipo_eventos;
            //                    case Conteudo.Financeiro: return perfil.v_financeiro;
            //                    case Conteudo.Galeria: return perfil.v_galeria;
            //                    case Conteudo.Orcamentos: return perfil.v_orcamentos;
            //                    case Conteudo.Pacotes: return perfil.v_pacotes;
            //                    case Conteudo.Visitas: return perfil.v_visitas; 
            //                    case Conteudo.Configuracoes: return perfil.v_configuracao;
            //                    case Conteudo.Estoque: return perfil.v_estoque;
            //                }
            //            } break;
            //    }
            //}
            //return false;
        }
        #endregion






        //COMPONENTES

        private static TagBuilder createSelect(string name, bool required)
        {
            TagBuilder select = new TagBuilder("select");
            select.MergeAttribute("name", name);
            select.MergeAttribute("id", name);
            select.MergeAttribute("class", "form-control");
            select.MergeAttribute("required", required.ToString());
            createOption(select, "", "Selecione");
            return select;
        }

        private static void createOption(TagBuilder select, string value, string innerHTML)
        {
            TagBuilder option = new TagBuilder("option");
            option.MergeAttribute("value", value);
            option.InnerHtml = innerHTML;
            select.InnerHtml += option.ToString();
        }

        public static string css { get; set; }
    }
}