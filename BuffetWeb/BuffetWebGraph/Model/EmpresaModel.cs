﻿using BuffetWebData.Models;
using BuffetWebGraph.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Utils;
using BuffetWebSerasa.Methods;

namespace BuffetWebGraph.Model
{
    public class EmpresaModel
    {
        public static Tuple<bool, string, string, dynamic> AddEmpresa(string cnpj)
        {
            try
            {
                string mensagem = "Empresa cadastrada com sucesso!";
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA e = db.EMPRESAs.Include(f => f.COLABORADORES).Include(de => de.EMPRESA_CNAES).FirstOrDefault(f => f.cnpj.Equals(cnpj));
                    if (e == null)
                    {
                        SerasaEmpresa r = new SerasaEmpresa(cnpj);
                        dynamic pfEstendida = r.informacoesEmpresa();
                        if (pfEstendida != null && pfEstendida.Status == false)
                        {
                            return new Tuple<bool, string, string, dynamic>(false, "", pfEstendida.Mensagem, null);
                        }

                        EMPRESA empresa = new EMPRESA();

                        empresa.telefone = "";
                        empresa.celular = "";

                        empresa.codigo_empresa = UtilsManager.gerarCodigoOrcamento();

                        empresa.cnpj = cnpj;
                        empresa.codigo_atividade_economica = pfEstendida.CodigoAtividadeEconomica;
                        empresa.codigo_atividade_economica_descricao = pfEstendida.CodigoAtividadeEconomicaDescricao;
                        empresa.codigo_natureza_juridica = pfEstendida.CodigoNaturezaJuridica;
                        empresa.codigo_natureza_juridica_descricao = pfEstendida.CodigoNaturezaJuridicaDescricao;
                        //DateTime convertido;
                        //if (DateTime.TryParse(pfEstendida.DataConsultaRFB, out convertido))
                        //{
                        //    empresa.data_consultaRFB = convertido.Date;
                        //}
                        //if (DateTime.TryParse(pfEstendida.DataFundacao, out convertido))
                        //{
                        //    empresa.data_consultaRFB = convertido.Date;
                        //}
                        //if (DateTime.TryParse(pfEstendida.DataMotivoEspecialSituacaoRFB, out convertido))
                        //{
                        //    empresa.data_consultaRFB = convertido.Date;
                        //}
                        //if (DateTime.TryParse(pfEstendida.DataSituacaoRFB, out convertido))
                        //{
                        //    empresa.data_consultaRFB = convertido.Date;
                        //}   
                        empresa.documento = pfEstendida.Documento;
                        empresa.motivo_especial_situacaoRFB = pfEstendida.MotivoEspecialSituacaoRFB;
                        empresa.motivo_situacaoRFB = pfEstendida.MotivoSituacaoRFB;
                        empresa.nome = pfEstendida.NomeFantasia;
                        empresa.razao_social = pfEstendida.RazaoSocial;
                        empresa.situacao_RFB = pfEstendida.SituacaoRFB;
                        empresa.status = pfEstendida.Status.ToString();
                        empresa.bloqueio_cnae = false;

                        if (pfEstendida.Enderecos != null)
                        {
                            foreach (var endereco in pfEstendida.Enderecos)
                            {
                                ENDERECO end = new ENDERECO();
                                end.cep = endereco.CEP;
                                end.endereco1 = endereco.Logradouro;
                                end.numero = endereco.Numero;
                                end.bairro = endereco.Bairro;
                                end.cidade = endereco.Cidade;
                                end.estado = endereco.Estado;
                                end.complemento = endereco.Complemento;
                                end.id_empresa = empresa.id;
                                empresa.id_endereco = end.id;
                                db.ENDERECOes.Add(end);
                            }
                        }


                        for (int i = 0; i < pfEstendida.CNAES.Count; i++)
                        {
                            empresa.EMPRESA_CNAES.Add(new EMPRESA_CNAES()
                            {
                                cnae = pfEstendida.CNAES[i].CNAE1,
                                cnae_descricao = pfEstendida.CNAES[i].CNAEDescricao,
                                id_empresa = empresa.id
                            });

                            if (empresa.EMPRESA_CNAES.FirstOrDefault(f => f.cnae.Equals("8230-0/02") || f.cnae.Equals("5620-1/02")) == null)
                            {
                                empresa.bloqueio_cnae = true;
                                mensagem = "Empresa cadastrada com sucesso, nossa equipe esta analisando e em breve entrara em contato";
                                new UtilsManager().sendEmail("Não disponível", string.Format("A empresa {0} se cadastrou, mas não é do seguimento de Buffet \nContato: {1}", empresa.razao_social, empresa.telefone), "contato@buffetweb.com");
                            }
                        }

                        empresa.EMPRESA_CONFIGURACAO.Add(new EMPRESA_CONFIGURACAO()
                        {
                            id_empresa = empresa.id,
                            permitirSMS = false,
                            pesquisa_dias_apos_evento = 1,
                            pesquisa_enviar_automaticamente = false,
                            quantidade_usuarios = 1,
                        });

                        string[] modulos = new string[] {
                            "ORCAMENTO", 
                            "CONVITE",
                            "FINANCEIRO",
                            "ESTOQUE",
                            "PORTAL"
                        };

                        for (int i = 0; i < modulos.Length; i++)
                        {
                            empresa.EMPRESA_MODULOS.Add(new EMPRESA_MODULOS()
                            {
                                adquirido_em = DateTime.Now.Date,
                                expira_em = new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.Day - 1)),
                                modulo = modulos[i]
                            });
                        }

                        db.EMPRESAs.Add(empresa);
                        db.SaveChanges();
                        //new FTPManager(empresa.id).CreateBaseEmpresa();
                        return new Tuple<bool, string, string, dynamic>(true, empresa.codigo_empresa, mensagem, pfEstendida);
                    }
                    else
                    {
                        if (e.COLABORADORES.Count == 0)
                        {
                            return new Tuple<bool, string, string, dynamic>(true, e.codigo_empresa, "Você precisa criar um usuário administrador!", null);
                        }
                        else
                        {
                            return new Tuple<bool, string, string, dynamic>(true, e.codigo_empresa, "Esta empresa já esta cadastrada", null);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new Tuple<bool, string, string, dynamic>(false, "", "Empresa não cadastrada", null);
            }
        }

        //public Tuple<bool, EmpresaException> ModuloConvite(string autenticacao, bool ativar)
        //{
        //    try
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            EMPRESA e = db.EMPRESAs.Include(m => m.EMPRESA_MODULOS).FirstOrDefault(f => f.codigo_empresa.Equals(autenticacao));
        //            e.EMPRESA_MODULOS.FirstOrDefault().convite = ativar;
        //            db.SaveChanges();
        //            return new Tuple<bool, EmpresaException>(true, null);
        //        }
        //    }
        //    catch (EmpresaException e)
        //    {
        //        return new Tuple<bool, EmpresaException>(false, null);
        //    }
        //}
    }
}