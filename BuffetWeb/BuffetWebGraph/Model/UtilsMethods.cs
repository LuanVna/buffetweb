﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebData.Utils;

namespace BuffetWebGraph.Model
{
    public class OperadoraCelular
    {
        public bool IsOk { get; set; }
        public string Mensagem { get; set; }
        public string Numero { get; set; }
        public string Operadora { get; set; }
        public string IconeOperadora { get; set; }
    }

    public class UtilsMethods
    {
        public OperadoraCelular GetCelular(string numero)
        {
            Tuple<bool, string> response = UtilsManager.GetOperadora(numero);
            return new OperadoraCelular()
            {
                IconeOperadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + response.Item2 + ".png",
                IsOk = response.Item1,
                Mensagem = !response.Item1 ? "Operadora Não Encontrada! \nNão esqueça de digitar o DDD" : "",
                Operadora = response.Item2,
                Numero = numero
            };
        }
    }
}