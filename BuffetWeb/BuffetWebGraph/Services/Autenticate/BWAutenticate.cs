﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Collections.Specialized;
using System.Data.Entity;

namespace BuffetWebGraph.Services.Autenticate
{
    public abstract class BWAutenticate
    {
        public static int Id_colaborador
        {
            get
            {
                return Colaborador.id;
            }
        }

        public static int Id_empresa
        {
            get
            {
                return Colaborador.id_empresa;
            }
        }

        public static EMPRESA Empresa
        {
            get
            {
                return Colaborador.EMPRESA;
            }
        }

        public static COLABORADORE Colaborador
        {
            get
            {
                NameValueCollection collection = HttpContext.Current.Request.ServerVariables;

                string usuario = collection["HTTP_USER"]; ;
                string senha = collection["HTTP_USER"]; ;
                string device = collection["HTTP_DEVICE"];
                if (!string.IsNullOrEmpty(usuario) && !string.IsNullOrEmpty(senha) && !string.IsNullOrEmpty(device))
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        COLABORADORE cola = db.COLABORADORES.Include(d => d.COLABORADOR_DEVICES).Include(s => s.COLABORADOR_DEVICES).FirstOrDefault(e => e.email.Equals(usuario) && e.senha.Equals(senha));
                        if (cola != null)
                        {
                            if (cola.PERFIL_COLABORADOR.device)
                            {
                                var devices = cola.COLABORADOR_DEVICES.FirstOrDefault(e => e.device.Equals(device));
                                if (devices != null)
                                {
                                    if (devices.status.Equals("habilitado"))
                                    {
                                        return cola;
                                    }
                                    else
                                    {
                                        throw new BWAutenticationException("Aparelho não habilitado!");
                                    }
                                }
                                else
                                {
                                    throw new BWAutenticationException("Aparelho não cadastrado!");
                                }
                            }
                            else
                            {
                                throw new BWAutenticationException("Usuario sem permissão");
                            }
                        }
                        else
                        {
                            throw new BWAutenticationException("Usuario não encontrado");
                        }

                    }
                }
                else
                {
                    throw new BWAutenticationException("Algum parametro obrigatorio não foi enviado");
                }
            }
        }
    }
}