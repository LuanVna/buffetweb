﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebGraph.Services.Autenticate
{
    public class BWAutenticationException : Exception
    {
        public BWAutenticationException(string message)
            : base(message)
        {

        }
    }
}