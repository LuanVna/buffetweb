﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BuffetWebData.Models;
using BuffetWebGraph.Model;
using Newtonsoft.Json;
using System.Reflection;
using BuffetWebGraph.Model;

namespace BuffetWebGraph
{
    //[WebService(Namespace = "http://www.graph.buffetweb.com/documentacao")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class BuffetWebGraph : System.Web.Services.WebService
    {
        [WebMethod]
        public void AddEmpresa(string cnpj)
        {
            Tuple<bool, string, string, dynamic> empresa = EmpresaModel.AddEmpresa(cnpj);

            Context.Response.Write(JsonConvert.SerializeObject(
                new
                {
                    status = empresa.Item1,
                    token_empresa = empresa.Item2,
                    mensagem = empresa.Item3,
                    empresa = empresa.Item4
                }
                ));
        }

        [WebMethod]
        public void RequestGraph(string Class, string Method, string json)
        {
            try
            {
                Type type = Type.GetType(Class, true);
                object instance = Activator.CreateInstance(type);

                Type thisType = type.GetType();
                MethodInfo theMethod = thisType.GetMethod(Method);
                Context.Response.Write(JsonConvert.SerializeObject(new
                {
                    isOk = true,
                    Method = Method,
                    Class = Class,
                    Response = theMethod.Invoke(this, (dynamic)json)
                }));
            }
            catch (Exception e)
            {
                Context.Response.Write(JsonConvert.SerializeObject(new { isOk = false, error = e.Message }));
            }
        }
    }
}
