﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebGraph.Services.Autenticate;
using Newtonsoft.Json;

namespace BuffetWebGraph.Services
{
    /// <summary>
    /// Summary description for Clientes
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Cliente : System.Web.Services.WebService
    {
        [WebMethod]
        public void Clientes()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var clientes = db.CLIENTEs.Where(e => e.id_empresa == BWAutenticate.Id_empresa).Select(e => new { e.nome, e.onde_conheceu, e.operadora_celular, e.celular, e.email });
                Context.Response.Write(new { isOk = true, response = JsonConvert.SerializeObject(clientes) });
            }
        }

        [WebMethod]
        public void DetalhesCliente()
        {
            using (BuffetContext db = new BuffetContext())
            {

            }
        }

        [WebMethod]
        public void NovoCliente(CLIENTE cliente)
        {
            using (BuffetContext db = new BuffetContext())
            {

            }
        }
    }
}
