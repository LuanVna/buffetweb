﻿using BuffetWebGraph.Services.Autenticate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace BuffetWebGraph
{
    /// <summary>
    /// Summary description for Seguranca
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class Authentication : SoapHeader
    {
        public string User;
        public string Password;
    }

    public class Seguranca : System.Web.Services.WebService
    {
        public Authentication authentication;

        [WebMethod]
        [SoapHeader("authentication")]

        public string HelloWorld()
        {
            string d = HttpContext.Current.Request.ServerVariables["HTTP_USER"];


            if (User.IsInRole("Customer"))
                return "User is in role customer";

            if (User.Identity.IsAuthenticated)
                return "User is a valid user";
            return "not authenticated";
        }
    }
    public class LogModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += this.OnBegin;
        }

        private void OnBegin(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            HttpContext context = app.Context;

            byte[] buffer = new byte[context.Request.InputStream.Length];
            context.Request.InputStream.Read(buffer, 0, buffer.Length);
            context.Request.InputStream.Position = 0;

            string soapMessage = Encoding.ASCII.GetString(buffer);

            // Do something with soapMessage
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

}



//namespace BuffetWebGraph
//{
//    [WebService(Namespace = "http://tempuri.org/")]
//    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//    [System.ComponentModel.ToolboxItem(false)]

//    public class Authentication : SoapHeader
//    {
//        public string User;
//        public string Password;
//    }

//    public class SecureWebService : WebService
//    {
//        public Authentication authentication;

//        [WebMethod]
//        [SoapHeader("authentication")]
//        public string ValidUser()
//        {
//            if (User.IsInRole("Customer"))
//                return "User is in role customer";

//            if (User.Identity.IsAuthenticated)
//                return "User is a valid user";
//            return "not authenticated";
//        }
//    }
//}