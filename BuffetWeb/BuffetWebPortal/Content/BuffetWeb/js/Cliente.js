﻿function BuscarOperadora(evt) {
    o('operadora').value = "";
    if (evt.value.length == 0) {
        o('valido').value = "";
        o('mensagem').style.display = "none";
    } else if (evt.value.length == 11) {
        $.getJSON('/Cliente/BuscarOperadora', { telefone: evt.value }, function (operadora) {
            if (operadora.Item1 == true) {
                if (operadora.Item2 != "invalido") {
                    o('mensagem').style.display = "none";
                    o('operadora').value = operadora.Item2;
                    var imagem_operadora = "http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + operadora.Item2 + ".png";
                    o("celular").setAttribute("style", "height: 50px;background-position: right; background-image:url('" + imagem_operadora + "');background-repeat: no-repeat;");
                    o('imagem_operadora').src = imagem_operadora;
                    o('valido').value = true;
                } else {
                    o('mensagem').style.display = "block";
                    o('mensagem').innerHTML = "Por favor informe um celular válido ( com DDD ) ";
                    o('valido').value = false;
                }
            }
        }, 'json');
    }
}

function EsqueciMinhaSenha(evt) {
    if (o('email').value == '') {
        o('mensagem').style.display = "block";
        o('mensagem').innerHTML = "Informe seu e-mail";
        return;
    }
    o('mensagem').style.display = "none";
    evt.innerHTML = "Enviando senha...";
    $.getJSON('/Cliente/SolicitarNovaSenha', { email: o('email').value }, function (data) {
        o('mensagem').style.display = "block";
        if (data.isOk) {
            o('mensagem').innerHTML = "renovação de senha enviado com sucesso!";
            evt.innerHTML = "";
        } else {
            o('mensagem').innerHTML = "Usuário não encontrado";
            evt.innerHTML = "Esqueci minha senha";
        }
    }, 'json');
}

function VerificarSenha(evt) {
    if (document.getElementById('senha').value.length < 7) {
        document.getElementById('mensagem').innerHTML = "A senha deve ter no minímo 7 letras!";
        return false;
    }
    if (document.getElementById('senha').value != document.getElementById('cSenha').value) {
        document.getElementById('mensagem').innerHTML = "As senhas não conferem";
        return false;
    }
    return true;
}

function o(id) {
    return document.getElementById(id);
}