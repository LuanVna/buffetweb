﻿$(document).ready(function () {
    $(document).ready(function () {
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '527135120709685',
                xfbml: true,
                version: 'v2.4'
            });

            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        });
    });
});

function StatusLogin() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    if (response.status === 'connected') {
        $.getJSON('/Cliente/StatusFacebook',
        {
            id_facebook: response.authResponse.userID,
            signedRequest: response.authResponse.signedRequest
        }, function (data) {
            if (data.isOk) {
                document.getElementById('associar').innerHTML = '<img src="/Content/imagens/conectado_facebook.png" style=" width: 184px;" />';
            }
        });
    }
}

function LoginComFacebook() {
    FB.login(function (response) {
        if (response.status == "connected") {
            $.getJSON('/Cliente/LoginFacebook',
                {
                    id_facebook: response.authResponse.userID,
                    signedRequest: response.authResponse.signedRequest
                }, function (data) {
                    if (data.isOk) {
                        window.location.href = data.redirect;
                    } else {
                        document.getElementById('mensagem').innerHTML = "Cliente não cadastrado";
                        document.getElementById('mensagem').style.display = "block";
                    }
                });
        }
    });
}

function AssociarAoFacebook(evt) {
    FB.login(function (response) {
        if (response.status == "connected") {
            $.getJSON('/Cliente/AssociarAoFacebook',
                {
                    id_facebook: response.authResponse.userID,
                    signedRequest: response.authResponse.signedRequest
                }, function (data) {
                    if (data.isOk) {
                        evt.innerHTML = '<img src="/Content/imagens/conectado_facebook.png" style=" width: 184px;"/>';
                    }
                });
        }
    });
}

function CadastrarComFacebook() {
    FB.api('/me', function (response) {
        $.getJSON('/Cliente/CadastrarComFacebook', { id_facebook: response.id }, function (data) {
            if (data.isOk) {
                document.getElementById('loginStatus').innerHTML = "OLÁ " + response.name;
            }
        });
    });
}

function BuscarInformacoes() {
    FB.login(function (response) {

    });
}