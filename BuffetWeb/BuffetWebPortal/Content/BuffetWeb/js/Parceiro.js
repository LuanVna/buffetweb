﻿$(function () {
    $('a.fullsizable').fullsizable();

    $(document).on('fullsizable:opened', function () {
        $("#jquery-fullsizable").swipe({
            swipeLeft: function () {
                $(document).trigger('fullsizable:next')
            },
            swipeRight: function () {
                $(document).trigger('fullsizable:prev')
            },
            swipeUp: function () {
                $(document).trigger('fullsizable:close')
            }
        });
    });
});

function Comentar(evt) {
    if (v('mensagem') == "" && v('avaliacao') == "") {

        return;
    }
    evt.innerHTML = "Comentando...";
    document.getElementById('mensagem').disabled = true;
    $.getJSON('/Parceiros/AvaliarParceiro',
        {
            comentario: v('mensagem'),
            avaliacao: v('avaliacao'),
        }, function (data) {
            if (data.isOk) {
                location.reload();
            } else {
                document.getElementById('mensagemComentario').style.display = "block";
                document.getElementById('mensagem').disabled = false;
                document.getElementById('mensagemComentario').innerHTML = "Ouve algum <strong>erro</strong> tente novamente!";
            }
        }, 'json');
}

function Favoritar(evt) {
    $.getJSON('/Parceiros/Favoritar', { favoritar: evt.dataset.favoritado }, function (data) {
        if (data.isOk) {
            if (data.favoritado) {
                evt.innerHTML = '<img src="/Content/Templates/Portal/image/icone_adicionar_favoritos.png" style="width: 190px;" />';
            } else {
                evt.innerHTML = '<img src="/Content/Templates/Portal/image/icone_remover_favoritos.png" style="width: 190px;"/>';
            }
            evt.dataset.favoritado = data.favoritado;
        }
    }, 'json');
}

function EnviarEmailParceiro(evt) {
    $.getJSON('/Parceiros/EnviarEmailParaDestinatario', {
        nome: v('nome'),
        email: v('email'),
        destinatario: v('destinatario'),
        comentario: v('comentario')
    }, function (data, statusText) {
        if (statusText == "success") {
            window.close();
        }
    }, 'json')
}



function v(id) {
    return document.getElementById(id).value;
}

function SelecionarEstrela(evt, index) {

    for (var x = 4 ; x > 0 ; x--) {
        document.getElementById('estrela_' + x).setAttribute('class', 'djrv_star');
    }
    for (var x = 0; x < (index + 1) ; x++) {
        document.getElementById('estrela_' + x).setAttribute('class', 'djrv_star active');
    }
    evt.setAttribute('class', 'djrv_star active');
    document.getElementById('avaliacao').value = (index + 1);
}

function MostrarMaisImagens(evt, total_imagens) {
    for (var i = 0; i != total_imagens; i++) {
        if (document.getElementById('imagem_' + i) != null) {
            document.getElementById('imagem_' + i).style.display = "block";
        }
    }
    evt.style.display = "none";
}