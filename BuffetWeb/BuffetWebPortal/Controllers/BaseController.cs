﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebData.Utils;

namespace BuffetWebPortal.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnAuthorization(AuthorizationContext context)
        {
            base.ControllerContext.RouteData.Values["controller"].ToString();
            string str = "";
            if ((str.Equals("Cliente") && ((str != "Login") || (str != "CriarConta"))) && !context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = base.RedirectToAction("Login", "Cliente");
            }
        }
        public int id_cliente
        {
            get
            {
                if (base.HttpContext.User.Identity.IsAuthenticated)
                {
                    return Convert.ToInt32(base.HttpContext.User.Identity.Name);
                }
                return -1;
            }
        }
        public PORTAL_CLIENTE_PERFIL GetCliente()
        {
            if (base.HttpContext.User.Identity.IsAuthenticated)
            {
                using (BuffetContext context = new BuffetContext())
                {
                    int id = 0;
                    if (int.TryParse(base.HttpContext.User.Identity.Name, out id))
                    {
                        //return context.PORTAL_CLIENTE_PERFIL.Include<PORTAL_CLIENTE_PERFIL, IEnumerable<LOCAL_EVENTO>>(c => (from e in c.LOCAL_EVENTO_COMENTARIOS select e.LOCAL_EVENTO)).Include<PORTAL_CLIENTE_PERFIL, IEnumerable<LOCAL_EVENTO>>(p => (from e in p.PORTAL_CLIENTE_FAVORITOS select e.LOCAL_EVENTO)).Include<PORTAL_CLIENTE_PERFIL, ICollection<PORTAL_CLIENTE_PESQUISAS>>(c => c.PORTAL_CLIENTE_PESQUISAS).FirstOrDefault(e => (e.id == id));
                        return null;
                    }
                }
            }
            return null;
        }
        public LOCAL_EVENTO GetParceiro()
        {
            using (BuffetContext context = new BuffetContext())
            {
                string parceiro = UtilsManager.GetCookie(base.HttpContext, "PARCEIRO");
                if (parceiro != null)
                {
                    return null;
                    //return context.LOCAL_EVENTO.Include(f => f.EMPRESA).FirstOrDefault(e => e.autenticacao.Equals(parceiro));
                }
                return null;
            }
        }
    }
}