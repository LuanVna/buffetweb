﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using System.Web.Security;
using RLSecurity;
using BuffetWebPortal.Models.Utils;
using BuffetWebData.Utils;
using BuffetWebPortal.Models.Cliente;


namespace BuffetWebPortal.Controllers
{
    public class ClienteController : BaseController
    {
        private static string mensagem;
        private static string _acao;
        private static string _controller;
        private static string _parametros;

        public ActionResult Acessar(string email, string senha)
        {
            using (BuffetContext context = new BuffetContext())
            {
                senha = Security.CriptographyOn(senha);
                PORTAL_CLIENTE_PERFIL client = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.email.Equals(email) && e.senha.Equals(senha));
                if (client != null)
                {
                    FormsAuthentication.SetAuthCookie(client.id.ToString(), false);
                    return BuffetAnalytics.AddActionOfClient(_parametros, client, _acao, _controller);
                }
                mensagem = "Email ou senha de usuário invalído";
                return RedirectToAction("Login");
            }
        }

        public ActionResult ApagarComentario(int id_comentario)
        {
            PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
            if (cliente != null)
            {
                using (BuffetContext context = new BuffetContext())
                {
                    context.LOCAL_EVENTO_COMENTARIOS.Remove(context.LOCAL_EVENTO_COMENTARIOS.FirstOrDefault<LOCAL_EVENTO_COMENTARIOS>(e => (e.id_cliente_perfil == cliente.id) && (e.id == id_comentario)));
                    context.SaveChanges();
                    return base.RedirectToAction("Perfil");
                }
            }
            return base.RedirectToAction("");
        }

        public ActionResult AssociarAoFacebook(string id_facebook, string signedRequest)
        {
            PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
            if (cliente != null)
            {
                using (BuffetContext context = new BuffetContext())
                {
                    PORTAL_CLIENTE_PERFIL portal_cliente_perfil = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.id == cliente.id);
                    portal_cliente_perfil.id_facebook = id_facebook;
                    portal_cliente_perfil.signedRequest = signedRequest;
                    context.SaveChanges();
                    return base.Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarOperadora(string telefone)
        {
            Tuple<bool, string> data = UtilsManager.GetOperadora(telefone);
            return base.Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Cadastrar(string nome, string celular, string operadora, string email, string senha)
        {
            using (BuffetContext context = new BuffetContext())
            {
                if (context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => (e.email.Equals(email) || e.celular.Equals(celular))) == null)
                {
                    PORTAL_CLIENTE_PERFIL entity = new PORTAL_CLIENTE_PERFIL
                    {
                        celular = celular,
                        email = email,
                        nome = nome,
                        operadora_celular = operadora,
                        senha = Security.CriptographyOn(senha),
                        perfil = UtilsManager.CriarAutenticacao(),
                        renovarSenha = ""
                    };
                    context.PORTAL_CLIENTE_PERFIL.Add(entity);
                    context.SaveChanges();
                    FormsAuthentication.SetAuthCookie(entity.id.ToString(), false);
                    return BuffetAnalytics.AddActionOfClient(_parametros, entity);
                }
                mensagem = "Este e-mail ou celular j\x00e1 est\x00e3o sendo utilizados!";
                return base.RedirectToAction("CriarConta");
            }
        }

        public static string renovarSenha;
        [AllowAnonymous]
        public ActionResult ConfirmarRenovacao(string senha)
        {
            if (!string.IsNullOrEmpty(renovarSenha))
            {
                using (BuffetContext context = new BuffetContext())
                {
                    PORTAL_CLIENTE_PERFIL portal_cliente_perfil = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.renovarSenha.Equals(renovarSenha));
                    if (portal_cliente_perfil != null)
                    {
                        portal_cliente_perfil.senha = Security.CriptographyOn(senha);
                        portal_cliente_perfil.renovarSenha = "";
                        FormsAuthentication.SetAuthCookie(portal_cliente_perfil.id.ToString(), false);
                        return base.RedirectToAction("Perfil");
                    }
                }
            }
            return base.RedirectToAction("Portal", "Home");
        }

        [AllowAnonymous]
        public ActionResult CriarConta(string acao, string controlle, string parametros)
        {
            if (base.GetCliente() != null)
            {
                return base.RedirectToAction("Perfil");
            }
            acao = _acao;
            _controller = controlle;
            _parametros = parametros;
            ((dynamic)base.ViewBag).mensagem = mensagem;
            mensagem = null;
            return base.View();
        }

        [AllowAnonymous]
        public ActionResult Login(string acao, string controlle, string parametros)
        {
            if (base.GetCliente() != null)
            {
                return base.RedirectToAction("Perfil");
            }
            ((dynamic)base.ViewBag).mensagem = mensagem;
            mensagem = null;
            _acao = acao;
            _controller = controlle;
            _parametros = parametros;

            return base.View();
        }

        [AllowAnonymous]
        public ActionResult LoginFacebook(string id_facebook, string signedRequest)
        {
            using (BuffetContext context = new BuffetContext())
            {
                PORTAL_CLIENTE_PERFIL client = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.id_facebook.Equals(id_facebook));
                if (client != null)
                {
                    FormsAuthentication.SetAuthCookie(client.id.ToString(), false);
                    return BuffetAnalytics.AddActionOfClient(_parametros, client, _acao, _controller);
                }
                return base.Json(new { isOk = false, redirect = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Perfil()
        {
            PerfilModel model = new PerfilModel
            {
                id_cliente = base.id_cliente
            };
            ((dynamic)base.ViewBag).mensagem = mensagem;
            mensagem = null;
            return base.View(model);
        }

        [AllowAnonymous]
        public ActionResult RenovarSenha(string autenticacao)
        {
            using (BuffetContext context = new BuffetContext())
            {
                PORTAL_CLIENTE_PERFIL model = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault(e => e.renovarSenha.Equals(autenticacao));
                if (model != null)
                {
                    autenticacao = UtilsManager.CriarAutenticacao();
                    renovarSenha = autenticacao;
                    model.renovarSenha = autenticacao;
                    context.SaveChanges();
                }
                return base.View(model);
            }
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return base.RedirectToAction("Index", "Portal");
        }

        public ActionResult SalvarAlteracoes(PORTAL_CLIENTE_PERFIL cliente, HttpPostedFileBase imagens)
        {
            PORTAL_CLIENTE_PERFIL portal_cliente_perfil = base.GetCliente();
            if (portal_cliente_perfil != null)
            {
                using (BuffetContext context = new BuffetContext())
                {
                    try
                    {
                        cliente.id = portal_cliente_perfil.id;
                        cliente.renovarSenha = "";
                        cliente.id_facebook = portal_cliente_perfil.id_facebook;
                        cliente.email = portal_cliente_perfil.email;
                        cliente.celular = portal_cliente_perfil.celular;
                        cliente.operadora_celular = portal_cliente_perfil.operadora_celular;
                        cliente.perfil = portal_cliente_perfil.perfil;
                        cliente.senha = portal_cliente_perfil.senha;
                        context.PORTAL_CLIENTE_PERFIL.Attach(cliente);
                        context.Entry(cliente).State = EntityState.Modified;
                        context.SaveChanges();
                        if (imagens != null)
                        {
                            var t = FTPManager.UploadImageSistema("PortalPerfilClientes", cliente.id.ToString(), imagens.InputStream);
                            if (t.Item1)
                            {
                                cliente.url = t.Item2;
                                context.SaveChanges();
                            }
                        }
                        mensagem = "Informa\x00e7\x00f5es alteradas com sucesso!";
                    }
                    catch (Exception e)
                    {
                    }
                    return base.RedirectToAction("Perfil");
                }
            }
            return base.RedirectToAction("Login");
        }
        [AllowAnonymous]
        public JsonResult SolicitarNovaSenha(string email)
        {
            using (BuffetContext context = new BuffetContext())
            {
                PORTAL_CLIENTE_PERFIL portal_cliente_perfil = context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.email.Equals(email));
                if (portal_cliente_perfil != null)
                {
                    string str = UtilsManager.CriarAutenticacao();
                    portal_cliente_perfil.renovarSenha = str;
                    context.SaveChanges();
                    ContactManager manager = new ContactManager();
                    EmailContent emailContent = new EmailContent
                    {
                        emailContentButton = EmailContentButton.Acessar,
                        emailContentHeader = EmailContentHeader.RenovarSenha,
                        emailTemplate = TemplateEmail.TemplatePortal,
                        nomeOla = portal_cliente_perfil.nome,
                        url = string.Format("http://www.portal2.buffetweb.com/Cliente/RenovarSenha?autenticacao={0}", str)
                    };
                    EmailStatusSend send = manager.SendEmailPortal(email, "BuffetWeb - Renovar Senha", emailContent.url, emailContent);
                    return base.Json(new { isOk = (send == EmailStatusSend.Send) ? true : false }, JsonRequestBehavior.AllowGet);
                }
                return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StatusFacebook(string id_facebook, string signedRequest)
        {
            using (BuffetContext context = new BuffetContext())
            {
                if (context.PORTAL_CLIENTE_PERFIL.FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => e.id_facebook.Equals(id_facebook)) != null)
                {
                    return base.Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}