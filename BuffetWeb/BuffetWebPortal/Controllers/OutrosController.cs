﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuffetWebPortal.Controllers
{
    public class OutrosController : BaseController
    {
        public ActionResult Termo()
        {
            return View();
        }

        public ActionResult Politica()
        {
            return View();
        }

        public ActionResult Mensagem()
        {
            return View();
        }
    }
}
