﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebData.Utils;
using BuffetWebPortal.Models.Parceiros;
using System.Web.Routing;
using System.Net;

namespace BuffetWebPortal.Controllers
{
    public class ParceirosController : BaseController
    {
        public ActionResult Parceiro(string Parceiro, string m, string notif_t, string autenticacao)
        {
            if (!string.IsNullOrEmpty(Parceiro))
            {
                using (BuffetContext context = new BuffetContext())
                {
                    UtilsManager.SaveCookie(base.HttpContext, "PARCEIRO", Parceiro);
                    new ContactManager().ReadContact(Request, autenticacao, TipoContato.PortalPromocao);
                    //.Select(d => d.B_PORTAL_PROMOCAO)
                    LOCAL_EVENTO model = context.LOCAL_EVENTO.Include(em => em.LOCAL_EVENTO_SOCIAL)
                                                             .Include(d => d.PORTAL_ANUNCIANTE_PROMOCAO)
                                                             .Include<LOCAL_EVENTO, ICollection<EMPRESA_MODULOS>>(em => em.EMPRESA.EMPRESA_MODULOS)
                                                             .Include(en => en.ENDERECO)
                                                             .Include(f => f.LOCAL_EVENTO_IMAGENS)
                                                             .Include(c => (from pc in c.LOCAL_EVENTO_COMENTARIOS select pc.PORTAL_CLIENTE_PERFIL))
                                                             .Include(v => v.PORTAL_VISUALIZACOES).FirstOrDefault<LOCAL_EVENTO>(e => e.autenticacao.Equals(Parceiro));

                    ParceirosManager.AddPageView(base.GetCliente(), model.id, notif_t);
                    PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
                    if (cliente != null)
                    {
                        ((dynamic)base.ViewBag).nome = cliente.nome;
                        ((dynamic)base.ViewBag).celular = cliente.celular;
                        ((dynamic)base.ViewBag).email = cliente.email;
                    }
                    if (model != null)
                    {
                        return base.View(model);
                    }
                }
            }
            return base.RedirectToAction("Index", "Portal");
        }

        public ActionResult PromocaoDoMes()
        {
            using (BuffetContext db = new BuffetContext())
            {
                B_PORTAL_PROMOCAO p = db.B_PORTAL_PROMOCAO.Include(b => b.PORTAL_ANUNCIANTE_PROMOCAO)
                                                           .FirstOrDefault(e => e.disponivel_de >= DateTime.Today && e.disponivel_ate <= DateTime.Today);
                return View(p);
            }
        }

        public JsonResult AvaliarParceiro(string comentario, int avaliacao)
        {
            using (BuffetContext context = new BuffetContext())
            {
                LOCAL_EVENTO parceiro = base.GetParceiro();
                PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
                if ((parceiro != null) && (cliente != null))
                {
                    LOCAL_EVENTO_COMENTARIOS local_evento_comentarios = context.LOCAL_EVENTO_COMENTARIOS.FirstOrDefault<LOCAL_EVENTO_COMENTARIOS>(e => (e.id_local_evento == parceiro.id) && (e.id_cliente_perfil == cliente.id));
                    if (local_evento_comentarios == null)
                    {
                        LOCAL_EVENTO_COMENTARIOS entity = new LOCAL_EVENTO_COMENTARIOS
                        {
                            desde = DateTime.Now,
                            comentario = comentario,
                            id_local_evento = parceiro.id,
                            avaliacao = avaliacao,
                            id_empresa = parceiro.id_empresa,
                            id_cliente_perfil = cliente.id,
                            analisado = true
                        };
                        context.LOCAL_EVENTO_COMENTARIOS.Add(entity);
                    }
                    else
                    {
                        local_evento_comentarios.avaliacao = avaliacao;
                        local_evento_comentarios.comentario = comentario;
                    }
                    context.SaveChanges();
                    return base.Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Denunciar(string comentario)
        {
            using (BuffetContext context = new BuffetContext())
            {
                LOCAL_EVENTO parceiro = base.GetParceiro();
                if (parceiro != null)
                {
                    B_PORTAL_DENUNCIAS entity = new B_PORTAL_DENUNCIAS
                    {
                        comentario = comentario,
                        desde = DateTime.Today,
                        id_local_evento = parceiro.id,
                        resolvido = false
                    };
                    context.B_PORTAL_DENUNCIAS.Add(entity);
                    context.SaveChanges();
                    return base.Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EnviarCotacao(ContactManager.SolicitarCotacao Cotacao)
        {
            using (new BuffetContext())
            {
                PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
                if (cliente != null)
                {
                    Cotacao.cliente = cliente;
                }
                return base.Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Favoritar(bool favoritar)
        {
            LOCAL_EVENTO parceiro = base.GetParceiro();
            PORTAL_CLIENTE_PERFIL cliente = base.GetCliente();
            if ((parceiro != null) && (cliente != null))
            {
                using (BuffetContext context = new BuffetContext())
                {
                    if (!favoritar)
                    {
                        PORTAL_CLIENTE_FAVORITOS entity = new PORTAL_CLIENTE_FAVORITOS
                        {
                            desde = DateTime.Today,
                            id_cliente_perfil = cliente.id,
                            id_local_evento = parceiro.id
                        };
                        context.PORTAL_CLIENTE_FAVORITOS.Add(entity);
                        context.SaveChanges();
                        return base.Json(new { isOk = true, favoritado = true }, JsonRequestBehavior.AllowGet);
                    }
                    context.PORTAL_CLIENTE_FAVORITOS.Remove(context.PORTAL_CLIENTE_FAVORITOS.FirstOrDefault<PORTAL_CLIENTE_FAVORITOS>(e => (e.id_cliente_perfil == cliente.id) && (e.id_local_evento == parceiro.id)));
                    context.SaveChanges();
                    return base.Json(new { isOk = true, favoritado = false }, JsonRequestBehavior.AllowGet);
                }
            }
            return base.Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EnviarEmail(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.LOCAL_EVENTO.Include(en => en.ENDERECO).FirstOrDefault(e => e.autenticacao.Equals(autenticacao)));
            }
        }

        [HttpGet]
        public ActionResult EnviarEmailParaDestinatario(string nome, string email, string destinatario, string comentario, string autenticacao)
        {




            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}