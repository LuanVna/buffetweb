﻿using System;
using System.Linq;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebPortal.Models.Portal;
using BuffetWebPortal.Models.Utils;
using System.Data.Entity;

namespace BuffetWebPortal.Controllers
{
    public class PortalController : BaseController
    {
        public ActionResult Index()
        {
            return base.View();
        }

        public ActionResult Regiao(string Regiao)
        {
            if (!string.IsNullOrEmpty(Regiao))
            {
                RegiaoModel model = new RegiaoModel
                {
                    regiao = Regiao
                };
                BuffetAnalytics.AddNewSearch(Regiao, string.Format("Foi encontrado {0} buffets na regiao de {1}", model.anunciantes.Count + model.parceiros.Count, Regiao), base.GetCliente());
                return base.View(model);
            }
            return base.RedirectToAction("Index");
        }
         
        public ActionResult Buscar(string BuscarPor)
        {
            if (!string.IsNullOrEmpty(BuscarPor))
            {
                BuscarModel model = new BuscarModel
                {
                    termo = BuscarPor
                };
                BuffetAnalytics.AddNewSearch(BuscarPor, string.Format("Foi encontrado {0} buffets com a pesquisa {1}", model.parceiros.Count + model.anunciantes.Count, BuscarPor), base.GetCliente());
                return base.View(model);
            }
            return base.RedirectToAction("Index");
        }

        public ActionResult PromocaoDoMes()
        { 
            using (BuffetContext db = new BuffetContext())
            {
                B_PORTAL_PROMOCAO p = db.B_PORTAL_PROMOCAO.Include(b => b.PORTAL_ANUNCIANTE_PROMOCAO.Select(f => f.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS))
                                                           .FirstOrDefault(e => DateTime.Today >= e.disponivel_de && DateTime.Today <= e.disponivel_ate);
                return View(p);
            }
        }
	}
}