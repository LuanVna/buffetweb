﻿using BuffetWebPortal.Models.Proposta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebData.Utils;
using System.Web.Routing;
using BuffetWebPortal.Models.Portal;
using BuffetWebPortal.Utils;

namespace BuffetWebPortal.Controllers
{
    public class PropostaController : BaseController
    {
        public ActionResult Informacoes()
        {
            return View();
        }

        public ActionResult VisualizarProposta(string autenticacao)
        {

            PropostaModel proposta = new PropostaModel()
            {
                autenticacao = autenticacao,
                httpContext = HttpContext
            };

            if (proposta.Proposta != null)
            {
                new ContactManager(proposta.Proposta.id_empresa, (int)proposta.Proposta.id_cliente).ReadContact(Request, autenticacao, TipoContato.Proposta);

                if (DateTime.Today > proposta.Proposta.disponivel_ate)
                    return Redirect(string.Format("{0}/Parceiros/Parceiro?autenticacao={1}&m=404", UtilsManager.URL_PORTAL, autenticacao));
                else if (!proposta.Proposta.disponivel)
                    return Redirect(string.Format("{0}/Parceiros/Parceiro?autenticacao={1}&m=403", UtilsManager.URL_PORTAL, autenticacao));
                return View(proposta);
            }
            else
                return Redirect(string.Format("{0}/Parceiros/Parceiro?autenticacao={1}&m=400", UtilsManager.URL_PORTAL, autenticacao));
        }

        public ActionResult AtualizarProposta(string autenticacao, int? convidados, DateTime? data_evento, string periodo)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA proposta = db.CLIENTE_PROPOSTA.FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (proposta != null)
                {
                    UtilsManager.SaveCookie(HttpContext, "KEY_EMPRESA", proposta.EMPRESA.codigo_empresa);

                    if (convidados != null)
                        proposta.convidados = convidados;
                    if (data_evento != null)
                        proposta.data_evento = data_evento;
                    if (periodo != null)
                        proposta.periodo = periodo;

                    db.SaveChanges();
                    return RedirectToAction("VisualizarProposta", new RouteValueDictionary(new { autenticacao = autenticacao }));
                }
                return Redirect("http://www.buffetweb.com");
            }
        }

        [AllowAnonymous]
        public JsonResult AgendarVisitaSimplificada(string autenticacao, DateTime data, string horario, string email, string celular, string mensagem)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA p = db.CLIENTE_PROPOSTA.Include(em => em.EMPRESA).Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (p != null)
                {
                    CLIENTE c = db.CLIENTEs.FirstOrDefault(e => e.id == p.CLIENTE.id);
                    if (c.email == null)
                        c.email = email;
                    if (c.celular == null)
                    {
                        c.celular = celular;
                        c.operadora_celular = UtilsManager.GetOperadora(celular).Item2;
                    }
                    db.SaveChanges();


                    string m = string.Format("O cliente {0} deseja agendar uma visita!", c.nome);
                    string autenticacao_visita = UtilsManager.CriarAutenticacao();
                    bool resposta = new ContactManager(p.EMPRESA.id).SendNotificationFromSystem("Agendamento de visita", m, new ContentNotification()
                    {
                        action = "VisitasAgendadas",
                        controller = "Agenda",
                        parameters = string.Format("autenticacao={0}&autenticacao_visita={1}", autenticacao, autenticacao_visita),
                    }, NotificationRule.orcamento_agendar_visita);
                    if (resposta)
                        return Json(new { isOk = true, mensagem = "Em breve entraremos em contato para verificar a disponibilidade da visita!" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { isOk = false, mensagem = string.Format(" Não foi possivel agendar uma visita, entre em contato por telefone: {0}", c.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);


                    //string mensagem = string.Format("O cliente {0} deseja agendar uma visita dia {1} as {2} <br />Email: {3} <br />Celular:{4}", c.nome, data.ToShortDateString(), horario, c.email, c.celular);

                    //EmailContent d = new EmailContent();
                    //d.emailContentButton = EmailContentButton.Visualizar;
                    //d.emailContentHeader = EmailContentHeader.Proposta;
                    //d.emailTemplate = TemplateEmail.TemplateEmpresa;
                    //d.nomeOla = c.nome;

                    //EmailStatusSend status = new ContactManager(p.id_empresa, c.id).SendEmail("contato@torceretorce.com", "Agendamento de Visita", autenticacao, TipoContato.AgendamentoDeVisita, mensagem, d);
                    //if (status == EmailStatusSend.Send)
                    //{
                    //    //if (c.email != null)
                    //    //{
                    //    //    new ContactManager(c.id_empresa, cliente.id).SendEmail(c.email, "Agendamento de Visita", "Você acaba de agendar uma visita, aguarde que entraremos em contato o mais breve possível!", TipoContato.AgendamentoDeVisita, autenticacao, d);
                    //    //}
                    //    //new ContactManager(c.id_empresa).SendNotificationFromSystem("Agendamento de Visita", mensagem, NotificationRule.AgendarVisita);
                    //    return Json(new { isOk = true, mensagem = "Em breve entraremos em contato para verificar a disponibilidade da visita!" }, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    return Json(new { isOk = false, mensagem = string.Format(" Não foi possivel agendar uma visita, entre em contato por telefone: {0}", c.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);
                    //}
                }
                return Json(new { isOk = false, mensagem = "Não foi possivel agendar uma visita!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public JsonResult EnviarEmailSimplificada(string autenticacao, string assunto, string mensagem, string email, string celular)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA p = db.CLIENTE_PROPOSTA.Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (p != null)
                {
                    CLIENTE c = db.CLIENTEs.FirstOrDefault(e => e.id == p.CLIENTE.id);
                    if (c.email == null)
                        c.email = email;
                    if (c.celular == null)
                    {
                        c.celular = celular;
                        c.operadora_celular = UtilsManager.GetOperadora(celular).Item2;
                    }
                    db.SaveChanges();

                    string mensagem2 = string.Format("O cliente {0} acaba de responder sua proposta do evento {1} <br/>{2} <br />Email: {3} <br />Celular:{4}", p.CLIENTE.nome, p.ORCAMENTO_PROPOSTA.nome_proposta, mensagem, p.CLIENTE.email, p.CLIENTE.celular);


                    EmailContent d = new EmailContent();
                    d.emailContentButton = EmailContentButton.Visualizar;
                    d.emailContentHeader = EmailContentHeader.Proposta;
                    d.emailTemplate = TemplateEmail.TemplateEmpresa;
                    d.nomeOla = c.nome;

                    EmailStatusSend status = new ContactManager(p.id_empresa, c.id).SendEmail("contato@torceretorce.com", "Resposta de proposta " + p.ORCAMENTO_PROPOSTA.nome_proposta, mensagem2, TipoContato.RespostaProposta, autenticacao, d);
                    if (status == EmailStatusSend.Send)
                    {
                        //new ContactManager(p.id_empresa).SendNotificationFromSystem("Resposta de proposta", mensagem, NotificationRule.EntrarContato);

                        return Json(new { isOk = true, mensagem = "Mensagem enviada com sucesso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { isOk = false, mensagem = string.Format("Não foi possivel enviar um contato!, entre em contato por telefone: {0}", p.EMPRESA.telefone) }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { isOk = false, mensagem = "Não foi possivel enviar um contato!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult ImprimirProposta(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO orcamen = db.ORCAMENTOes.Include(em => em.EMPRESA.ENDERECO).Include(c => c.CLIENTE).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));

                if (orcamen.EMPRESA != null)
                {
                    UtilsManager.SaveCookie(HttpContext, "KEY_EMPRESA", orcamen.EMPRESA.codigo_empresa);
                }

                if (orcamen != null)
                {
                    ViewBag.proposta = autenticacao;

                    return View(orcamen);
                }
                return RedirectToAction("");
            }
        }

        [AllowAnonymous]
        public ActionResult EnviarPropostaCompartilhada(PropostaCompartilhada proposta, string acao)
        {
            List<PropostaCompartilhadaModel> propostaModel = new List<PropostaCompartilhadaModel>();

            using (BuffetContext db = new BuffetContext())
            {
                int id_cliente = 0;
                RegiaoModel locais = new RegiaoModel { regiao = proposta.regiao };

                List<LOCAL_EVENTO> anunciantes = locais.anunciantes;

                foreach (var local in anunciantes)
                {
                    string au = UtilsManager.CriarAutenticacao();

                    CLIENTE cliente = db.CLIENTEs.FirstOrDefault(e => e.email.Equals(proposta.email) && e.id_empresa == local.id_empresa);
                    if (cliente == null)
                    {
                        CLIENTE novoCliente = new CLIENTE()
                        {
                            autenticacao = UtilsManager.CriarAutenticacao(),
                            celular = proposta.celular,
                            telefone = proposta.telefone,
                            nome = proposta.nome,
                            operadora_celular = UtilsManager.GetOperadora(proposta.celular).Item2,
                            regiao = local.regiao,
                            email = proposta.email,
                            desde = DateTime.Today,
                            cliente_de = "Portal",
                            id_empresa = local.id_empresa
                        };
                        db.CLIENTEs.Add(novoCliente);
                        db.SaveChanges();

                        new ContactManager(local.id_empresa).AddTimeLine(proposta.email, "Novo Cliente", string.Format("Cliente criado em {0} quando solicitou uma proposta no portal", DateTime.Today), TimeLineIcon.Info);

                        id_cliente = cliente.id;
                    }
                    id_cliente = cliente.id;

                    ORCAMENTO_PROPOSTA propostaOr = db.ORCAMENTO_PROPOSTA.FirstOrDefault(e => e.id_local_evento == local.id && e.categoria_proposta == proposta.categoria);
                    if (propostaOr != null)
                    {

                        db.CLIENTE_PROPOSTA.Add(new CLIENTE_PROPOSTA()
                        {
                            id_cliente = id_cliente,
                            autenticacao = au,
                            convidados = proposta.convidados,
                            data_envio = DateTime.Today,
                            data_evento = proposta.data_evento,
                            disponivel = true,
                            id_empresa = local.id_empresa,
                            id_proposta = propostaOr.id,
                            disponivel_ate = DateTime.Now.AddDays(propostaOr.validade_dias),
                            proposta_de = "Portal",
                            periodo = proposta.periodo
                        });

                        if (!string.IsNullOrEmpty(proposta.email))
                        {
                            propostaModel.Add(new PropostaCompartilhadaModel()
                            {
                                LocalDoEvento = local,
                                autenticacao = au
                            });
                        }
                        else if (!string.IsNullOrEmpty(proposta.celular))
                        {
                            string url = UtilsManager.EncurtarURL(string.Format("{0}/Proposta/VisualizarProposta?autenticacao={1}", UtilsManager.URL_PORTAL, au));
                            string mensagem = string.Format("Ola {0}, clique {1} p/ ver nossa proposta e conhecer melhor nossos serviços de Buffet!", proposta.nome.Split(' ')[0], url);

                            SMSStatusSend statusSMS = new ContactManager(local.id_empresa).SendSMS(proposta.celular, "Proposta " + propostaOr.nome_proposta, mensagem, TipoContato.Proposta, au);
                            if (statusSMS == SMSStatusSend.Send)
                                db.SaveChanges();
                        }
                    }
                }

                bool resposta = PropostaEmail.EnviarPropostaCompartilhada(proposta.email, propostaModel);

                return RedirectToAction("Agradecimento", new RouteValueDictionary(new { Tipo = "Compartilhada" }));
            }
        }

        [AllowAnonymous]
        public ActionResult EnviarPropostaUnica(PropostaUnica proposta, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                string au = UtilsManager.CriarAutenticacao();

                ORCAMENTO_PROPOSTA p = db.ORCAMENTO_PROPOSTA.Include(l => l.LOCAL_EVENTO).FirstOrDefault(e => e.autenticacao.Equals(proposta.proposta_autenticacao));
                if (p != null)
                {
                    int id_cliente = 0;
                    CLIENTE cliente = db.CLIENTEs.FirstOrDefault(e => e.email.Equals(proposta.email));
                    if (cliente == null)
                    {
                        CLIENTE novoCliente = new CLIENTE()
                        {
                            autenticacao = UtilsManager.CriarAutenticacao(),
                            celular = proposta.celular,
                            telefone = proposta.telefone,
                            nome = proposta.nome,
                            operadora_celular = UtilsManager.GetOperadora(proposta.celular).Item2,
                            regiao = p.LOCAL_EVENTO.regiao,
                            email = proposta.email,
                            desde = DateTime.Today,
                            cliente_de = "Portal",
                            id_empresa = p.id_empresa
                        };
                        db.CLIENTEs.Add(novoCliente);
                        db.SaveChanges();

                        new ContactManager(p.id_empresa).AddTimeLine(proposta.email, "Novo Cliente", string.Format("Cliente criado em {0} quando solicitou uma proposta no portal", DateTime.Today), TimeLineIcon.Info);

                        id_cliente = cliente.id;
                    }
                    id_cliente = cliente.id;

                    db.CLIENTE_PROPOSTA.Add(new CLIENTE_PROPOSTA()
                    {
                        id_cliente = id_cliente,
                        autenticacao = UtilsManager.CriarAutenticacao(),
                        convidados = proposta.convidados,
                        data_envio = DateTime.Today,
                        data_evento = proposta.data_evento,
                        disponivel = true,
                        id_empresa = p.id_empresa,
                        id_proposta = p.id,
                        disponivel_ate = DateTime.Now.AddDays(p.validade_dias),
                        proposta_de = "Portal",
                        periodo = proposta.periodo
                    });
                    db.SaveChanges();
                }

                string url = UtilsManager.EncurtarURL(string.Format("{0}/Proposta/VisualizarProposta?autenticacao={1}", UtilsManager.URL_PORTAL, au));

                if (!string.IsNullOrEmpty(proposta.email))
                {
                    string mensagem = string.Format("Estamos ansiosos para fazer a sua festa! <br />  <br /> Clique no botão para visualizar nossa proposta <br /> e conhecer melhor nossos serviços de Buffet!");

                    EmailContent content = new EmailContent()
                    {
                        url = url,
                        emailContentButton = EmailContentButton.Visualizar,
                        emailContentHeader = EmailContentHeader.Proposta,
                        nomeOla = proposta.nome,
                        emailTemplate = TemplateEmail.TemplateEmpresa
                    };

                    EmailStatusSend statusEmail = new ContactManager(p.id_empresa).SendEmail(proposta.email, "Proposta " + p.nome_proposta, mensagem, TipoContato.Proposta, au, content);
                    if (statusEmail == EmailStatusSend.Send)
                        db.SaveChanges();
                }
                else if (!string.IsNullOrEmpty(proposta.celular))
                {
                    string mensagem = string.Format("Ola {0}, clique {1} p/ ver nossa proposta e conhecer melhor nossos serviços de Buffet!", proposta.nome.Split(' ')[0], url);

                    SMSStatusSend statusSMS = new ContactManager(p.id_empresa).SendSMS(proposta.celular, "Proposta " + p.nome_proposta, mensagem, TipoContato.Proposta, au);
                    if (statusSMS == SMSStatusSend.Send)
                        db.SaveChanges();
                }
            }
            return RedirectToAction("Agradecimento", new RouteValueDictionary(new { Tipo = "Unica"}));
        }

        public ActionResult Agradecimento(string Tipo)
        {
            return View();
        }
    }
}
