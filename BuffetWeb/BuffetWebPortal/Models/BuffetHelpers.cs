﻿using BuffetWebPortal.Models;

namespace BuffetWebPortal.Helpers
{
    using BuffetWebData.Models;
    using BuffetWebData.Utils;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public enum TipoImagem
    {
        Normal,
        Anunciante
    }

    public static class BuffetHelpers
    {
        public static PORTAL_CLIENTE_PERFIL Cliente(this HtmlHelper helper, HttpContextBase contextBase)
        {
            if (contextBase.User.Identity.IsAuthenticated)
            {
                using (BuffetContext context = new BuffetContext())
                {
                    int id_cliente;
                    if (int.TryParse(contextBase.User.Identity.Name, out id_cliente))
                    {
                        return context.PORTAL_CLIENTE_PERFIL.Include<PORTAL_CLIENTE_PERFIL, ICollection<PORTAL_CLIENTE_FAVORITOS>>(d => d.PORTAL_CLIENTE_FAVORITOS).FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => (e.id == id_cliente));
                    }
                }
            }
            return null;
        }

        public static LOCAL_EVENTO_COMENTARIOS ComentarioCliente(this HtmlHelper helper, string autenticacao, HttpContextBase contextBase)
        {
            using (BuffetContext context = new BuffetContext())
            {
                LOCAL_EVENTO local_evento = context.LOCAL_EVENTO.Include(c => c.LOCAL_EVENTO_COMENTARIOS.Select(d => d.PORTAL_CLIENTE_PERFIL))
                                                                .Include<LOCAL_EVENTO, ICollection<LOCAL_EVENTO_COMENTARIOS>>(e => e.LOCAL_EVENTO_COMENTARIOS)
                                                                .FirstOrDefault<LOCAL_EVENTO>(e => e.autenticacao.Equals(autenticacao));
                if (local_evento != null)
                {
                    if (contextBase.User.Identity.IsAuthenticated)
                    {
                        return local_evento.LOCAL_EVENTO_COMENTARIOS.FirstOrDefault<LOCAL_EVENTO_COMENTARIOS>(e => (e.id_cliente_perfil == Convert.ToInt32(contextBase.User.Identity.Name)));
                    }
                }
                return null;
            }
        }

        public static List<PORTAL_ANUNCIANTE_DESTAQUE> GetAnuncianteDestaque(this HtmlHelper helper)
        {
            using (BuffetContext context = new BuffetContext())
            {
                DateTime today = DateTime.Today;
                return (from e in context.PORTAL_ANUNCIANTE_DESTAQUE.Include<PORTAL_ANUNCIANTE_DESTAQUE, ICollection<LOCAL_EVENTO_COMENTARIOS>>(d => d.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_DESTAQUE, ENDERECO>(en => en.LOCAL_EVENTO.ENDERECO).Include<PORTAL_ANUNCIANTE_DESTAQUE, ICollection<LOCAL_EVENTO_IMAGENS>>(i => i.LOCAL_EVENTO.LOCAL_EVENTO_IMAGENS).Include<PORTAL_ANUNCIANTE_DESTAQUE, ICollection<EMPRESA_MODULOS>>(em => em.LOCAL_EVENTO.EMPRESA.EMPRESA_MODULOS)
                        where e.ate > today
                        select e).ToList<PORTAL_ANUNCIANTE_DESTAQUE>();
            }
        }

        public static List<PORTAL_ANUNCIANTE_HEADER> GetAnuncianteHeader(this HtmlHelper helper)
        {
            using (BuffetContext context = new BuffetContext())
            {
                DateTime today = DateTime.Today;
                return (from e in context.PORTAL_ANUNCIANTE_HEADER.Include<PORTAL_ANUNCIANTE_HEADER, ICollection<LOCAL_EVENTO_COMENTARIOS>>(d => d.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_HEADER, ENDERECO>(en => en.LOCAL_EVENTO.ENDERECO).Include<PORTAL_ANUNCIANTE_HEADER, ICollection<LOCAL_EVENTO_IMAGENS>>(i => i.LOCAL_EVENTO.LOCAL_EVENTO_IMAGENS)
                        where e.ate > today
                        select e).ToList<PORTAL_ANUNCIANTE_HEADER>();
            }
        }

        public static List<PORTAL_ANUNCIANTE_HOME> GetAnuncianteHOME(this HtmlHelper helper)
        {
            using (BuffetContext context = new BuffetContext())
            {
                DateTime today = DateTime.Today;
                return (from e in context.PORTAL_ANUNCIANTE_HOME.Include<PORTAL_ANUNCIANTE_HOME, ICollection<LOCAL_EVENTO_COMENTARIOS>>(d => d.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_HOME, ENDERECO>(en => en.LOCAL_EVENTO.ENDERECO).Include<PORTAL_ANUNCIANTE_HOME, ICollection<LOCAL_EVENTO_IMAGENS>>(i => i.LOCAL_EVENTO.LOCAL_EVENTO_IMAGENS)
                        where e.ate > today
                        select e).ToList<PORTAL_ANUNCIANTE_HOME>();
            }
        }

        public static List<PORTAL_ANUNCIANTE_TOPMENU> GetAnuncianteTopMenu(this HtmlHelper helper)
        {
            using (BuffetContext context = new BuffetContext())
            {
                DateTime today = DateTime.Today;
                return (from e in context.PORTAL_ANUNCIANTE_TOPMENU.Include<PORTAL_ANUNCIANTE_TOPMENU, ICollection<LOCAL_EVENTO_COMENTARIOS>>(d => d.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_TOPMENU, ENDERECO>(en => en.LOCAL_EVENTO.ENDERECO).Include<PORTAL_ANUNCIANTE_TOPMENU, ICollection<LOCAL_EVENTO_IMAGENS>>(i => i.LOCAL_EVENTO.LOCAL_EVENTO_IMAGENS)
                        where e.ate > today
                        select e).ToList<PORTAL_ANUNCIANTE_TOPMENU>();
            }
        }

        public static B_PORTAL_PROMOCAO GetPromocao(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                B_PORTAL_PROMOCAO p = db.B_PORTAL_PROMOCAO.Include(b => b.PORTAL_ANUNCIANTE_PROMOCAO)
                                                           .FirstOrDefault(e => e.disponivel_de >= DateTime.Today && e.disponivel_ate <= DateTime.Today);
                return p;
            }
        }

        public static Tuple<bool, B_PORTAL_PROMOCAO> IsPromocao(this HtmlHelper helper, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                LOCAL_EVENTO d = db.LOCAL_EVENTO.Include(f => f.PORTAL_ANUNCIANTE_PROMOCAO.Select(g => g.B_PORTAL_PROMOCAO))
                                                .FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (d != null)
                {
                    PORTAL_ANUNCIANTE_PROMOCAO promocao = d.PORTAL_ANUNCIANTE_PROMOCAO.FirstOrDefault(e => e.disponivel == true);
                    //&& e.B_PORTAL_PROMOCAO.disponivel_de >= DateTime.Today && e.B_PORTAL_PROMOCAO.disponivel_ate <= DateTime.Today
                    return new Tuple<bool, B_PORTAL_PROMOCAO>(promocao != null, promocao != null ? promocao.B_PORTAL_PROMOCAO : null);
                }
                return new Tuple<bool, B_PORTAL_PROMOCAO>(false, null);
            }
        }

        public static string GetImagemParceiro(this HtmlHelper helper, LOCAL_EVENTO local, TipoImagem tipoImagem = TipoImagem.Anunciante, int sequencia = 0)
        {
            string path = string.Format("Empresas/{0}/LocalEvento/{1}_{2}.png", new UtilsEmpresa(local.id_empresa).NOME_EMPRESA_FTP, local.id, sequencia);
            if (new FTPManager().ExistFile(path))
            {
                return string.Format("http://www.buffetweb.com/Sites/Arquivos/{0}", path);
            }
            return string.Format("/Content/Templates/Portal/image/imagem_padrao_{0}.png", tipoImagem.ToString().ToLower());
        }

        public static string GetImagemParceiro(this HtmlHelper helper, string autenticacao, TipoImagem tipoImagem = TipoImagem.Anunciante, int sequencia = 0)
        {
            using (BuffetContext context = new BuffetContext())
            {
                LOCAL_EVENTO local = context.LOCAL_EVENTO.Include(d => d.EMPRESA).FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                if (local != null)
                {
                    string path = string.Format("Empresas/{0}/LocalEvento/{1}_{2}.png", new UtilsEmpresa(local.id_empresa).NOME_EMPRESA_FTP, local.id, sequencia);
                    if (new FTPManager().ExistFile(path))
                    {
                        return string.Format("http://www.buffetweb.com/Sites/Arquivos/{0}", path);
                    }
                }
                return string.Format("/Content/Templates/Portal/image/imagem_padrao_{0}.png", tipoImagem.ToString().ToLower());
            }
        }

        public static string EncurtarURL(this HtmlHelper helper, string url)
        {
            return UtilsManager.EncurtarURL(url);
        }

        public static List<ORCAMENTO_PROPOSTA> GetPropostas(this HtmlHelper helper, HttpRequestBase request)
        {
            using (BuffetContext db = new BuffetContext())
            {
                string parceiroID = request.Cookies.Get("PARCEIRO").Value;
                if (!string.IsNullOrEmpty(parceiroID))
                {
                    var local = db.LOCAL_EVENTO.FirstOrDefault(e => e.autenticacao.Equals(parceiroID));
                    var propostas = db.ORCAMENTO_PROPOSTA.Where(e => e.id_local_evento == 11).ToList();

                    return propostas;
                }
                return new List<ORCAMENTO_PROPOSTA>();
            }
        }

        public static List<ImagensParceiro> GetImagensParceiro(this HtmlHelper helper, HttpRequestBase request)
        {
            using (BuffetContext context = new BuffetContext())
            {
                string parceiroID = request.Cookies.Get("PARCEIRO").Value;
                List<LOCAL_EVENTO_IMAGENS> list = (from e in context.LOCAL_EVENTO_IMAGENS.Include(em => em.EMPRESA).Include(l => l.LOCAL_EVENTO)
                                                   where e.LOCAL_EVENTO.autenticacao.Equals(parceiroID)
                                                   select e).ToList();

                List<ImagensParceiro> list2 = new List<ImagensParceiro>();
                foreach (LOCAL_EVENTO_IMAGENS local_evento_imagens in list)
                {
                    string url = "";
                    string path = string.Format("Empresas/{0}/LocalEvento/{1}_{2}.png", new UtilsEmpresa(local_evento_imagens.id_empresa).NOME_EMPRESA_FTP, local_evento_imagens.LOCAL_EVENTO.id, 0);
                    if (new FTPManager().ExistFile(path))
                        url = string.Format("http://www.buffetweb.com/Sites/Arquivos/{0}", path);
                    else
                        url = string.Format("/Content/Templates/Portal/image/imagem_padrao_anunciante.png");

                    ImagensParceiro item = new ImagensParceiro
                    {
                        sequencia = local_evento_imagens.sequencia,
                        url = url
                    };
                    list2.Add(item);
                }
                return list2;
            }
        }

        public static bool isLogin(this HtmlHelper helper, HttpContextBase contextBase)
        {
            return contextBase.User.Identity.IsAuthenticated;
        }

        public static LOCAL_EVENTO LocalEvento(this HtmlHelper helper, HttpContextBase contextBase)
        {
            using (BuffetContext context = new BuffetContext())
            {
                string parceiro = UtilsManager.GetCookie(contextBase, "PARCEIRO");
                if (parceiro != null)
                {
                    return context.LOCAL_EVENTO.Include<LOCAL_EVENTO, EMPRESA>(f => f.EMPRESA).FirstOrDefault<LOCAL_EVENTO>(e => e.autenticacao.Equals(parceiro));
                }
                return null;
            }
        }

        public static int GetAvaliacao(this HtmlHelper helper, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var comentarios = db.LOCAL_EVENTO_COMENTARIOS.Include(f => f.LOCAL_EVENTO).Where(e => e.LOCAL_EVENTO.autenticacao.Equals(autenticacao)).ToList();
                var total_avaliacao = comentarios.Sum(d => d.id);
                var avaliacao = total_avaliacao != 0 ? (total_avaliacao / comentarios.Count) : 0;
                return avaliacao;
            } 
        }

        public static bool GetPlanoProposta(this HtmlHelper helper, string autenticacao)
        {
            return true;
        }

        private static HttpCookie keyEmpresa
        {
            get { return System.Web.HttpContext.Current.Request.Cookies["KEY_EMPRESA"]; }
        }

        public static bool isMobile(this HtmlHelper helper)
        {
            return System.Web.HttpContext.Current.Request.UserAgent.Contains("Mobile");
        }

        private static EMPRESA empresa()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
            }
        }

        //public static List<EMPRESA_SOCIAL> redeSocial(this HtmlHelper helper)
        //{
        //    using (BuffetContext db = new BuffetContext())
        //    {
        //        return db.EMPRESA_SOCIAL.Include(em => em.EMPRESA).Where(e => e.EMPRESA.codigo_empresa == keyEmpresa.Value).ToList();
        //    }
        //}

        public static string LogoEmpresa(this HtmlHelper helper)
        {
            return new FTPManager(empresa().id).LogoEmpresa();
        }

        public static string DominioEmpresa(this HtmlHelper helper)
        {
            return new UtilsManager(empresa().id).DominioEmpresa();
        }

        public static MvcHtmlString FontesCSS(this HtmlHelper helper)
        {
            return new MvcHtmlString(new UtilsManager().CSSFontes());
        }

        public static string ArquivoCSS(this HtmlHelper help)
        {
            string pathCSS = ArquivoEmpresa("CSS");
            if (pathCSS != null)
            {
                return pathCSS;
            }
            return "/Content/Gerenciador/css/style.css";
        }

        public static string ArquivoBOOTSTRAP(this HtmlHelper help)
        {
            string pathBootStrap = ArquivoEmpresa("BOOTSTRAP");
            if (pathBootStrap != null)
            {
                return pathBootStrap;
            }
            return "/Content/Gerenciador/css/bootstrap.min.css";
        }

        public static bool Send_SMS(this HtmlHelper help)
        {
            using (BuffetContext db = new BuffetContext())
            {
                int id_empresa = empresa().id;
                var send = db.EMPRESA_PACOTE_SMS.FirstOrDefault(e => e.quantidade_disponivel > 0 && e.id_empresa == id_empresa); /*&& permitirSMS*/
                if (send != null)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool AllowSomesSMS(this HtmlHelper help)
        {
            return true;
        }

        public static string ArquivoLOGO(this HtmlHelper help)
        {
            return ArquivoEmpresa("LOGO");
        }

        private static string ArquivoEmpresa(string key)
        {
            if (keyEmpresa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                    if (empresa != null)
                    {
                        List<EMPRESA_ARQUIVOS> arquivos = db.EMPRESA_ARQUIVOS.Where(i => i.id_empresa == empresa.id).ToList();
                        return arquivos.FirstOrDefault(e => e.chave.Equals(key)).valor;
                        //ViewBag.logo = arquivos.FirstOrDefault(e => e.chave.Equals("LOGO")).valor;
                        //ViewBag.bootstrap = arquivos.FirstOrDefault(e => e.chave.Equals("BOOTSTRAP")).valor;
                        //ViewBag.nome_empresa = empresa.nome;
                    }
                }
            }
            return null;
        }

        //public static ORCAMENTO orcamento(this HtmlHelper help)
        //{
        //    return BWCliente.getOrcamento();
        //}

        public static EMPRESA empresa(this HtmlHelper help)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (keyEmpresa != null)
                {
                    EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(keyEmpresa.Value));
                    return empresa;
                }
                return null;
            }
        }
    }
}
