﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebPortal.Models.Cliente
{
    public class PerfilModel
    {
        public int id_cliente { get; set; }

        public PORTAL_CLIENTE_PERFIL ClientePortal
        {
            get
            {
                using (BuffetContext context = new BuffetContext())
                {
                    return context.PORTAL_CLIENTE_PERFIL.Include<PORTAL_CLIENTE_PERFIL, IEnumerable<LOCAL_EVENTO>>(c => (from e in c.LOCAL_EVENTO_COMENTARIOS select e.LOCAL_EVENTO)).Include<PORTAL_CLIENTE_PERFIL, IEnumerable<LOCAL_EVENTO>>(p => (from e in p.PORTAL_CLIENTE_FAVORITOS select e.LOCAL_EVENTO)).Include<PORTAL_CLIENTE_PERFIL, ICollection<PORTAL_CLIENTE_PESQUISAS>>(c => c.PORTAL_CLIENTE_PESQUISAS).FirstOrDefault<PORTAL_CLIENTE_PERFIL>(e => (e.id == this.id_cliente));
                }
            }
        }
        public List<CLIENTE_PROPOSTA> ClientePropostas
        {
            get
            {
                using (BuffetContext context = new BuffetContext())
                {
                    PORTAL_CLIENTE_PERFIL perfil = this.ClientePortal;
                    if (perfil != null)
                    {
                        return (from e in context.CLIENTE_PROPOSTA.Include<CLIENTE_PROPOSTA, LOCAL_EVENTO>(b => b.ORCAMENTO_PROPOSTA.LOCAL_EVENTO).Include<CLIENTE_PROPOSTA, CLIENTE>(c => c.CLIENTE)
                                where e.CLIENTE.email.Equals(perfil.email)
                                select e).ToList<CLIENTE_PROPOSTA>();
                    }
                    return null;
                }
            }
        }
 


    }
}