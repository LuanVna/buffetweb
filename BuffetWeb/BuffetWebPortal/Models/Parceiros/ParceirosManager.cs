﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebPortal.Models.Parceiros
{
    public class ParceirosManager
    {
        public static void AddPageView(PORTAL_CLIENTE_PERFIL cliente, List<LOCAL_EVENTO> locais)
        {
            using (BuffetContext context = new BuffetContext())
            {
                int? nullable = null;
                if (cliente != null)
                {
                    nullable = new int?(cliente.id);
                }
                foreach (LOCAL_EVENTO local_evento in locais)
                {
                    PORTAL_VISUALIZACOES entity = new PORTAL_VISUALIZACOES
                    {
                        data = DateTime.Now,
                        id_local_evento = local_evento.id,
                        tipo = "LOCAL",
                        id_cliente_portal = nullable,
                    };
                    context.PORTAL_VISUALIZACOES.Add(entity);
                    context.SaveChanges();
                }
            }
        }
        public static void AddPageView(PORTAL_CLIENTE_PERFIL cliente, int id_local_evento, string notif_t)
        {
            using (BuffetContext context = new BuffetContext())
            {
                int? nullable = null;
                if (cliente != null)
                {
                    nullable = new int?(cliente.id);
                }
                PORTAL_VISUALIZACOES entity = new PORTAL_VISUALIZACOES
                {
                    data = DateTime.Now,
                    id_local_evento = id_local_evento,
                    tipo = "LOCAL",
                    id_cliente_portal = nullable,
                    facebook = notif_t,
                };
                context.PORTAL_VISUALIZACOES.Add(entity);
                context.SaveChanges();
            }
        }
    }
}