﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
 
namespace BuffetWebPortal.Models.Portal
{
    public class BuscarModel
    {
        public string termo { get; set; }
 

 

        public List<LOCAL_EVENTO> anunciantes
        {
            get
            {
                List<LOCAL_EVENTO> list = new List<LOCAL_EVENTO>();
                using (BuffetContext context = new BuffetContext())
                {
                    foreach (PORTAL_ANUNCIANTE_DESTAQUE portal_anunciante_destaque in (from e in context.PORTAL_ANUNCIANTE_DESTAQUE.Include<PORTAL_ANUNCIANTE_DESTAQUE, LOCAL_EVENTO>(l => l.LOCAL_EVENTO).Include<PORTAL_ANUNCIANTE_DESTAQUE, ICollection<LOCAL_EVENTO_COMENTARIOS>>(c => c.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_DESTAQUE, ICollection<EMPRESA_MODULOS>>(em => em.LOCAL_EVENTO.EMPRESA.EMPRESA_MODULOS)
                                                                                       where (e.ate < DateTime.Today) && ((((((e.LOCAL_EVENTO.palavras_chaves.Contains(this.termo) || e.LOCAL_EVENTO.nome.Contains(this.termo)) || e.LOCAL_EVENTO.regiao.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.bairro.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.cidade.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.complemento.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.endereco1.Contains(this.termo))
                                                                                       select e).OrderBy(e => e.destaque_pesquisa_zona).ToList<PORTAL_ANUNCIANTE_DESTAQUE>())
                    {
                        list.Add(portal_anunciante_destaque.LOCAL_EVENTO);
                    }
                    foreach (PORTAL_ANUNCIANTE_HOME portal_anunciante_home in (from e in context.PORTAL_ANUNCIANTE_HOME.Include<PORTAL_ANUNCIANTE_HOME, LOCAL_EVENTO>(l => l.LOCAL_EVENTO).Include<PORTAL_ANUNCIANTE_HOME, ICollection<LOCAL_EVENTO_COMENTARIOS>>(c => c.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_HOME, ICollection<EMPRESA_MODULOS>>(em => em.LOCAL_EVENTO.EMPRESA.EMPRESA_MODULOS)
                                                                               where (e.ate < DateTime.Today) && ((((((e.LOCAL_EVENTO.palavras_chaves.Contains(this.termo) || e.LOCAL_EVENTO.nome.Contains(this.termo)) || e.LOCAL_EVENTO.regiao.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.bairro.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.cidade.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.complemento.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.endereco1.Contains(this.termo))
                                                                               select e).OrderBy(e => e.destaque_pesquisa_zona).ToList<PORTAL_ANUNCIANTE_HOME>())
                    {
                        list.Add(portal_anunciante_home.LOCAL_EVENTO);
                    }
                    foreach (PORTAL_ANUNCIANTE_HEADER portal_anunciante_header in (from e in context.PORTAL_ANUNCIANTE_HEADER.Include<PORTAL_ANUNCIANTE_HEADER, LOCAL_EVENTO>(l => l.LOCAL_EVENTO).Include<PORTAL_ANUNCIANTE_HEADER, ICollection<LOCAL_EVENTO_COMENTARIOS>>(c => c.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_HEADER, ICollection<EMPRESA_MODULOS>>(em => em.LOCAL_EVENTO.EMPRESA.EMPRESA_MODULOS)
                                                                                   where (e.ate < DateTime.Today) && ((((((e.LOCAL_EVENTO.palavras_chaves.Contains(this.termo) || e.LOCAL_EVENTO.nome.Contains(this.termo)) || e.LOCAL_EVENTO.regiao.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.bairro.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.cidade.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.complemento.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.endereco1.Contains(this.termo))
                                                                                   select e).OrderBy(e => e.destaque_pesquisa_zona).ToList<PORTAL_ANUNCIANTE_HEADER>())
                    {
                        list.Add(portal_anunciante_header.LOCAL_EVENTO);
                    }
                    foreach (PORTAL_ANUNCIANTE_TOPMENU portal_anunciante_topmenu in (from e in context.PORTAL_ANUNCIANTE_TOPMENU.Include<PORTAL_ANUNCIANTE_TOPMENU, LOCAL_EVENTO>(l => l.LOCAL_EVENTO).Include<PORTAL_ANUNCIANTE_TOPMENU, ICollection<LOCAL_EVENTO_COMENTARIOS>>(c => c.LOCAL_EVENTO.LOCAL_EVENTO_COMENTARIOS).Include<PORTAL_ANUNCIANTE_TOPMENU, ICollection<EMPRESA_MODULOS>>(em => em.LOCAL_EVENTO.EMPRESA.EMPRESA_MODULOS)
                                                                                     where (e.ate < DateTime.Today) && ((((((e.LOCAL_EVENTO.palavras_chaves.Contains(this.termo) || e.LOCAL_EVENTO.nome.Contains(this.termo)) || e.LOCAL_EVENTO.regiao.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.bairro.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.cidade.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.complemento.Contains(this.termo)) || e.LOCAL_EVENTO.ENDERECO.endereco1.Contains(this.termo))
                                                                                     select e).OrderBy(e => e.destaque_pesquisa_zona).ToList<PORTAL_ANUNCIANTE_TOPMENU>())
                    {
                        list.Add(portal_anunciante_topmenu.LOCAL_EVENTO);
                    }
                    return list;
                }
            }
        }

        public List<LOCAL_EVENTO> parceiros
        {
            get
            {
                using (BuffetContext context = new BuffetContext())
                {
                    return (from e in context.LOCAL_EVENTO.Include<LOCAL_EVENTO, EMPRESA>(em => em.EMPRESA).Include<LOCAL_EVENTO, ICollection<LOCAL_EVENTO_COMENTARIOS>>(c => c.LOCAL_EVENTO_COMENTARIOS).Include<LOCAL_EVENTO, ICollection<EMPRESA_MODULOS>>(em => em.EMPRESA.EMPRESA_MODULOS)
                            where e.regiao.Contains(this.termo)
                            select e).ToList<LOCAL_EVENTO>();
                }
            }
        }
 

 

 

    }
}