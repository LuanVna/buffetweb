﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebPortal.Models.Proposta
{
    public class PropostaContent
    {

        public string celular { get; set; }
        public int convidados { get; set; }
        public DateTime data { get; set; }
        public string email { get; set; }
        public string periodo { get; set; }
        public string regiao { get; set; }
        public string telefone { get; set; }
        public string tipoFesta { get; set; } 
    }
}