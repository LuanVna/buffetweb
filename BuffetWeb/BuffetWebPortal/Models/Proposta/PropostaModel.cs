﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebData.Utils;

namespace BuffetWebPortal.Models.Proposta
{
    public enum PropostaResposta
    {
        Disponivel,
        Indisponivel,
        Expirada,
        NaoEncontrada
    }

    public class PropostaModel
    {
        public string autenticacao { get; set; }
        public HttpContextBase httpContext { get; set; }
        public CLIENTE_PROPOSTA Proposta
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CLIENTE_PROPOSTA proposta = db.CLIENTE_PROPOSTA.Include(e => e.ORCAMENTO_PROPOSTA.LOCAL_EVENTO)
                                                                   .Include(d => d.ORCAMENTO_PROPOSTA.ORCAMENTO_PROPOSTA_ABAS.Select(de => de.ORCAMENTO_PROPOSTA_DESCRICAO))
                                                                   .Include(e => e.ORCAMENTO_PROPOSTA.ORCAMENTO_PROPOSTA_PACOTE.Select(d => d.ORCAMENTO_PROPOSTA_PACOTE_VALORES))
                                                                   .Include(c => c.CLIENTE)
                                                                   .FirstOrDefault(e => e.autenticacao.Equals(this.autenticacao));

                    if (proposta != null)
                    {
                        UtilsManager.SaveCookie(httpContext, "KEY_EMPRESA", proposta.EMPRESA.codigo_empresa);
                        return proposta;
                    }
                    return proposta;
                }
            }
        }

        public LOCAL_EVENTO LocalEvento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.FirstOrDefault(e => e.autenticacao.Equals(this.autenticacao));
                }
            }
        }


        public ORCAMENTO_PROPOSTA_PACOTE_VALORES PacoteValores
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CLIENTE_PROPOSTA p = this.Proposta;
                    int? convidados = p.convidados;
                    if (convidados != null && convidados < 50)
                        convidados = 50;

                    ORCAMENTO_PROPOSTA_PACOTE valores = db.ORCAMENTO_PROPOSTA_PACOTE.Where(e => e.id_proposta == this.Proposta.id_proposta && e.ativo == true && e.quantidade_pessoas <= convidados).OrderByDescending(e => e.quantidade_pessoas).FirstOrDefault();
                    if (valores != null)
                    {
                        if (p.data_evento != null && valores != null)
                        {
                            ORCAMENTO_PROPOSTA_PACOTE_VALORES v = new ORCAMENTO_PROPOSTA_PACOTE_VALORES();
                            switch (Convert.ToDateTime(p.data_evento).DayOfWeek)
                            {
                                case DayOfWeek.Sunday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.sabado == true); break;
                                case DayOfWeek.Monday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.segunda == true); break;
                                case DayOfWeek.Tuesday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.terca == true); break;
                                case DayOfWeek.Wednesday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.quarta == true); break;
                                case DayOfWeek.Thursday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.quinta == true); break;
                                case DayOfWeek.Friday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.sexta == true); break;
                                case DayOfWeek.Saturday: v = valores.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.domingo == true); break;
                            }
                            return v;
                        }
                    }
                    return null;
                }
            }
        }
    }
}