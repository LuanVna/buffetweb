﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuffetWebPortal.Models.Proposta
{
    public class PropostaUnica
    {
        public string nome { get; set; }
        public string celular { get; set; }
        public string categoria { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public string proposta_autenticacao { get; set; }
        public string periodo { get; set; }
        public int convidados { get; set; }
        public DateTime data_evento { get; set; }
    }
}
