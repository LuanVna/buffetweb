﻿using BuffetWebData.Models;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuffetWebPortal.Models.Utils
{
    public class BuffetAnalytics
    {
        public static RedirectResult AddActionOfClient(string parametros, PORTAL_CLIENTE_PERFIL client, string action = "Parceiro", string controller = "Parceiros")
        {
            using (BuffetContext context = new BuffetContext())
            {
                if (action != null)
                {
                    switch (action)
                    {
                        case "Favoritar":
                            {
                                LOCAL_EVENTO local = context.LOCAL_EVENTO.FirstOrDefault<LOCAL_EVENTO>(e => e.autenticacao.Equals(parametros.Replace("parceiro=", "")));
                                if (local != null)
                                {
                                    if (context.PORTAL_CLIENTE_FAVORITOS.FirstOrDefault<PORTAL_CLIENTE_FAVORITOS>(e => (e.id_local_evento == local.id)) == null)
                                    {
                                        PORTAL_CLIENTE_FAVORITOS entity = new PORTAL_CLIENTE_FAVORITOS
                                        {
                                            id_cliente_perfil = client.id,
                                            id_local_evento = local.id,
                                            desde = DateTime.Now
                                        };
                                        context.PORTAL_CLIENTE_FAVORITOS.Add(entity);
                                        context.SaveChanges();
                                    }
                                    return new RedirectResult(string.Format("/{0}/{1}?{2}", controller, action, parametros));
                                }
                            } break;
                        default:
                            {
                                return new RedirectResult(string.Format("/{0}/{1}?{2}", controller, action, parametros));
                            };
                    }
                }
                return new RedirectResult("/Cliente/Perfil");
            }
        }

        public static void AddNewSearch(string term, string mensagem, PORTAL_CLIENTE_PERFIL cliente)
        {
            using (BuffetContext context = new BuffetContext())
            {
                int? id_cliente = null;
                if (cliente != null)
                {
                    id_cliente = new int?(cliente.id);
                }
                if (!id_cliente.HasValue)
                {
                    PORTAL_CLIENTE_PESQUISAS entity = new PORTAL_CLIENTE_PESQUISAS
                    {
                        data = DateTime.Now,
                        id_perfil_cliente = id_cliente,
                        termo = term,
                        mensagem = mensagem,
                        url = "/Portal/Buscar?BuscarPor=" + term
                    };
                    context.PORTAL_CLIENTE_PESQUISAS.Add(entity);
                }
                else if (context.PORTAL_CLIENTE_PESQUISAS.FirstOrDefault<PORTAL_CLIENTE_PESQUISAS>(e => (e.termo.Equals(term) && (e.id_perfil_cliente == id_cliente))) == null)
                {
                    PORTAL_CLIENTE_PESQUISAS portal_cliente_pesquisas3 = new PORTAL_CLIENTE_PESQUISAS
                    {
                        data = DateTime.Now,
                        id_perfil_cliente = id_cliente,
                        termo = term,
                        mensagem = mensagem,
                        url = "/Portal/Buscar?BuscarPor=" + term
                    };
                    context.PORTAL_CLIENTE_PESQUISAS.Add(portal_cliente_pesquisas3);
                }
                context.SaveChanges();
            }
        }
    }
}