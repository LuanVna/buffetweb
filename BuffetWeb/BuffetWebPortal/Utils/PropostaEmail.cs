﻿using BuffetWebPortal.Models.Proposta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Net;
using BuffetWebData.Utils;

namespace BuffetWebPortal.Utils
{
    public static class PropostaEmail
    {
        public static bool EnviarPropostaCompartilhada(string email, List<PropostaCompartilhadaModel> model)
        {
            String corpo = new WebClient().DownloadString("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Templates/PropostaCompartilhadoCorpo.html");
            String linha = new WebClient().DownloadString("http://www.buffetweb.com/Sites/Arquivos/Sistema/Email/Templates/PropostaCompartilhadaLinha.html");
            
            linha = linha.Replace("__DOMINIO__", UtilsManager.URL_PORTAL);

            String Linhas = "";
            foreach (var item in model)
            {
                string editarLinha = linha;
                editarLinha = editarLinha.Replace("__TITULO__", item.LocalDoEvento.nome);
                editarLinha = editarLinha.Replace("__DESCRICAO__", item.LocalDoEvento.descricao);
                editarLinha = editarLinha.Replace("__AUTENTICACAO_PROPOSTA", item.autenticacao);
                editarLinha = editarLinha.Replace("__NOME_AUTENTICACAO_LOCAL__", string.Format("{0}_{1}", item.LocalDoEvento.nome.Replace(" ", ""), item.LocalDoEvento.autenticacao));
                editarLinha = editarLinha.Replace("__ID_LOCALEVENTO__", item.LocalDoEvento.id.ToString());

                Linhas += editarLinha;
            }

            corpo = corpo.Replace("__LINHAS__", Linhas);
            corpo = corpo.Replace("__EMAIL_REMETENTE__", email);

            return new ContactManager().SendEmail(email, "BuffetWeb - Propostas disponíveis", corpo) == EmailStatusSend.Send;
        }
    }
}