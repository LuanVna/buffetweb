﻿using BuffetWebSerasa.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuffetWebSerasa.Request;

namespace BuffetWebSerasa.Methods
{
    public class SerasaEmpresa
    {
        private string cnpj { get; set; }
        private CreateRequest request { get; set; }
        public SerasaEmpresa(string cnpj)
        {
            this.cnpj = cnpj;
            this.request = new CreateRequest();
        }

        public dynamic informacoesEmpresa()
        {
            //this.cnpj = "99.999.999/9999-62";
            this.request.json = @"{" +
                                  "'Credenciais': {" +
                                      "'Email': 'contato@buffetweb.com'," +
                                      "'Senha': 'NKa2Yy5ut'}," +
                                  "'Documento': '" + this.cnpj + "'}";
            this.request.method = "/restservices/test-drive/cdc/pessoajuridicanfe.ashx";

            Request.Request r = new Request.Request();
            return r.loadPost(this.request);
        }
    }
}
