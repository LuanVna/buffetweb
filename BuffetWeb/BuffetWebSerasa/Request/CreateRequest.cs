﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuffetWebSerasa.Request
{
    public class CreateRequest
    {
       public string method { get; set; }
       public string json { get; set; }
    }
}


//Host: www.soawebservices.com.br
//POST /restservices/producao/cdc/pessoajuridicanfe.ashx
//Content-Type: application/json

//{
//  "Credenciais": {
//    "Email": "Seu endereco de E-Mail",
//    "Senha": "Sua Senha"
//  },
//  "Documento": "99.999.999/9999-62" 