﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BuffetWebSerasa.Request
{
    public class Request
    {

        public CreateRequest request { get; set; }
        private string url_server
        {
            get
            {
                return "http://www.soawebservices.com.br" + request.method;
            }
        } 

        public object loadPost(CreateRequest request){
            try
            { 
                this.request = request; 
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    return JsonConvert.DeserializeObject(client.UploadString(this.url_server, "POST", request.json));
                } 
            }
            catch (Exception e)
            {
                return null;
            }
        } 
    }
}
