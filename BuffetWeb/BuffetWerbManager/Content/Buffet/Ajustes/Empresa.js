﻿function AlterarDias(evt) {
    if (evt.value != "") {
        AlterarDias
        $.getJSON('/Ajustes/AlterarDiasApos', { dias: evt.value }, function (data) {

        }, 'json');
    }
}

function AlterarEnvioPesquisaAutomatica(evt) {
    $.getJSON('/Ajustes/AlterarEnvioPesquisaAutomatica', { alterar: o('permitir_envio_automatico').value }, function (data) {
        if (evt.dataset.check == "true") {
            UIShow('envio_automatico');
        } else {
            UIHide('envio_automatico');
        }
    }, 'json');
}

//SOCIAL
function RedeSocialSelecionada(evt) {
    var descricao = "Informe a URL da pagina na rede social";
    switch (evt.value) {
        case "Facebook": descricao = "http://www.facebook.com/Facebook"; break;
        case "Twitter": descricao = "http://www.twitter.com/Twitter"; break;
        case "Instagram": descricao = "https://instagram.com/Instagram"; break;
        case "Skype": descricao = "Nome do usuario"; break;
        case "E-mail": descricao = "email@email.com.br"; break;
        case "Google+": descricao = "https://plus.google.com/GooglePlus"; break;
        case "YouTube": descricao = "http://youtube.com/YouTube"; break;
    } 
    o('url_social').placeholder = descricao;
}