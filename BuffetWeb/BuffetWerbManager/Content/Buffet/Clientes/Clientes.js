﻿function EditarCliente(id, id_endereco, cliente_de, nome, telefone, celular, email, cpf, rg, id_onde_conheceu, cep, cidade, complemento, endereco, estado, logradouro, numero, bairro) {

    $.getJSON('/Clientes/Familiares', { id_cliente: id }, function (data) {
        if (data.isOk && data.familiares.length != 0) {
            o('Familiares').innerHTML = "";
            for (var i = 0; i < data.familiares.length; i++) {
                o('Familiares').innerHTML += templateAniversariantes(i, data.familiares[i].id, data.familiares[i].nome, data.familiares[i].nascimento, data.familiares[i].parentesco);
            }
            o('Familiares').dataset.quantidade = data.familiares.length;
        } else {
            o('Familiares').innerHTML = '<h3>Adicione seus familiares</h3>';
        }
    });

    o('id').value = id
    o('nome').value = nome;
    o('telefone').value = telefone;
    o('celular').value = celular;
    o('email').value = email;
    o('cpf').value = cpf;
    o('rg').value = rg;
    o('cliente_de').value = cliente_de;

    o('onde_conheceu').value = id_onde_conheceu;

    o('ENDERECO.id').value = id_endereco;
    o('cep').value = cep;
    o('ENDERECO.cidade').value = cidade;
    o('ENDERECO.estado').value = estado;
    o('complemento').value = complemento;
    o('endereco').value = endereco;
    o('numero').value = numero;
    o('bairro').value = bairro;
    o('SalvarOuAtualizar').innerHTML = "Salvar Alteração";
}

function NovoFamiliar(evt) {
    o('Familiares').innerHTML += templateAniversariantes(evt.dataset.quantidade, '', '', '', '');
    evt.dataset.quantidade++;
}

function VerificaEmail(evt) {
    if (isEmail(evt.value)) {
        UIHide('timeline');
        UIHide('informacaoTimeLine');
        $('#timeline').empty();

        $.ajax({
            url: '/Clientes/VerificaEmail',
            dataType: 'json',
            async: false,
            data: { email: evt.value },
            success: function (data) {
                if (data.ClienteDo == "Buffet") {
                    o('texto_mensagem').innerHTML = "O cliente " + data.nome + " já esta utilizando este email para contato!";
                    UIShow('mensagem');
                    o('SalvarOuAtualizar').disabled = "Disabled";
                } else if (data.ClienteDo == "Portal") {

                    VerificaTimeLine(data.timeline);

                    data = data.perfil;
                    o('nome').value = data.nome;
                    o('telefone').value = data.telefone;
                    o('celular').value = data.celular;

                    o('cep').value = data.cep;
                    o('ENDERECO.cidade').value = "São Paulo"; //data.cidade;
                    o('ENDERECO.estado').value = data.estado;
                    o('endereco').value = data.logradouro;
                    o('numero').value = data.numero;
                    o('bairro').value = data.bairro;

                    o('imagem').src = data.url;

                    o('texto_mensagem').innerHTML = "Este cliente tem um perfil no portal, importamos para você! <a href=\"/Planos/Portal\">Saiba mais</a>";

                    UIShow('mensagem');
                    o('SalvarOuAtualizar').disabled = "";
                } else {
                    UIHide('mensagem');
                    o('SalvarOuAtualizar').disabled = "";
                }
            }
        });
    } else {
        UIHide('mensagem');
        o('SalvarOuAtualizar').disabled = false;
    }
}

function VerificaTimeLine(timeline) {
    if (timeline.length > 0) {
        for (var i = 0; i < timeline.length; i++) {
            var obj = timeline[i];

            var icone = '/Content/Buffet/Orcamento/Imagens/icone_' + obj.icone + '.png';

            var linha = '<tr style="height:60px">' +
                             '<td width="80">' +
                             '<img style="vertical-align:middle" src="' + icone + '" />' +
                             '</td>' +
                             '<td  width="400">' +
                                 '<h5>' + obj.titulo + '</h5>' +
                                 '<div><small>' + obj.descricao + '</small></div>' +
                                 '<div><small>' + ConvertDate(obj.data) + '</small></div>' +
                             '</td>' +
                         '</tr>';
            $('#timeline').append(linha);
        }
        UIShow('timeline');
    } else {
        UIHide('timeline');
        UIShow('informacaoTimeLine');
    }
}

function ApagarFamiliar(evt) {
    o('familiar_' + evt.dataset.quantidade).style.display = "none";
    evt.dataset.quantidade--;
    if (evt.dataset.quantidade == 0) {
        o('Familiares').innerHTML = '<h3>Adicione seus familiares</h3>';
    }
}

function BuscarOperadora(evt) {
    if (evt.value.length == 13) {
        $.ajax({
            url: '/Clientes/BuscarOperadora',
            dataType: 'json',
            async: false,
            data: { telefone: evt.value },
            success: function (data) {
                if (data.Item1) {
                    o("operadora_celular").value = data.Item2;
                    o('label_celular').innerHTML = "Celular - " + data.Item2;
                }
            }
        });
    }
}


function ValidarCampos() {
    if (o('nome').value == "") {
        UIShow('mensagem');
        o('texto_mensagem').innerHTML = "O nome do cliente é obrigatório";
        return false;
    }
    if (o('email').value == "" && o('celular').value == "") {
        UIShow('mensagem');
        o('texto_mensagem').innerHTML = "Você precisa informar o email ou o celular do Cliente";
        return false;
    }
    if (o('email').value != "" && !isEmail(o('email').value)) {
        UIShow('mensagem');
        o('texto_mensagem').innerHTML = "Você precisa um email válido";
        return false;
    }
    return true;
}

function templateAniversariantes(quantidade, id, nome, nascimento, parentesco) {
    return '<div id="familiar_' + quantidade + '">' +
            '<input type="hidden" id="[' + quantidade + '].id" value="" />' +
                '<div class="col-md-4" id="' + quantidade + '">' +
                    '<label>Nome</label>' +
                    '<input type="text" class="form-control" name="[' + quantidade + '].nome" value=' + nome + ' />' +
                '</div>' +
                '<div class="col-md-4">' +
                    '<label>Nascido em</label>' +
                    '<input type="date" class="form-control" name="[' + quantidade + '].nascimento" value="' + ConvertDate(nascimento) + '"  />' +
                '</div>' +
                '<div class="col-md-3">' +
                    '<label>Graú de Parêntesco</label>' +
                    '<select class="form-control" name="[' + quantidade + '].parentesco">' +
                        '<option value="' + parentesco + '">' + parentesco + '</option>' +
                        '<option value="Filho">Filho</option>' +
                        '<option value="Cônjuges">Cônjuges</option>' +
                        '<option value="Pais">Pais</option>' +
                        '<option value="Irmão">Irmão</option>' +
                    '</select>' +
                '</div>' +
                '<div class="col-md-1 row">' +
                    '<input type="button" class="btn btn-danger" style=" margin-top: 20px; " value="x" onclick="ApagarFamiliar(this)" data-quantidade="' + quantidade + '" data-id="' + id + '" />' +
                '</div></div>';
}