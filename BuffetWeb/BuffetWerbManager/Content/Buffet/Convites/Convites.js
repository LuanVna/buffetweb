﻿var temaConvites = "";
var configuracao = o('configuracao').dataset;

$(document).ready(function () {
    $("#titulo_t").draggable();
    $("#descricao_t").draggable();
    $("#aniversariantes_t").draggable();
    $("#datahora_t").draggable();

    $("#owl-demo").owlCarousel({
        items: 3,
        lazyLoad: true,
        navigation: true,
        navigationText: [
          "<i class='fa fa-chevron-left icon-white'></i>",
          "<i class='fa fa-chevron-right icon-white'></i>"
        ],
        jsonPath: '/Convite/BuscarConviteTemas',
        jsonSuccess: SelecionarTema
    });

    BuscarFrases();

    if (configuracao.editando) {
        $.getJSON('/Convite/BuscarClienteSelecionado', { id_cliente: o('id_cliente').value }, function (cliente) {
            if (cliente.isOk) {
                TabelaFamiliares(cliente.data.familiares);
            }
        });
    }
});


function FiltrarPorTema(evt) {
    for (var i = 0; i < temaConvites.convite.length; i++) {
        $("#owl-demo").data('owlCarousel').removeItem();
    }

    //$("#owl-demo").owlCarousel();
    for (var i = 0; i < temaConvites.convite.length; i++) {
        if (evt.value == "semFiltro") {
            $("#owl-demo").data('owlCarousel').addItem(thumbnail(temaConvites.convite[i], i));
        } else {
            if (temaConvites.convite[i].tema == evt.value) {
                $("#owl-demo").data('owlCarousel').addItem(thumbnail(temaConvites.convite[i], i));
            }
        }
    }
}

function SelecionarTema(resposta) {
    temaConvites = resposta;
    $("#owl-demo").owlCarousel();
    var html = "";
    for (var i = 0; i < resposta.convite.length; i++) {
        html += thumbnail(temaConvites.convite[i], i);
    }
    $("#owl-demo").html(html);

    var temas = [];
    var contador = 0;
    for (var i = 0; i < resposta.convite.length; i++) {
        var naoExiste = true;
        for (var x = 0; x < temas.length; x++) {
            if (temas[x].tema == resposta.convite[i].tema) {
                naoExiste = false;
            }
        }
        if (naoExiste) {
            temas[contador] = { tema: resposta.convite[i].tema };
            contador++;
        }
    }

    o('filtrarPorTema').innerHTML += "<option value=semFiltro>Sem Filtro</option>";
    for (var i = 0; i < temas.length; i++) {
        o('filtrarPorTema').innerHTML += ("<option value='" + temas[i].tema + "'>" + temas[i].tema + "</option>");
    }
}

function temaSelecionado(evt) {

    var editar = temaConvites.convite[evt.dataset.index];
    if (configuracao.editando) {

        o('id_template_tema').value = editar.id;
        o('url_image').src = editar.url_image;

        //TITULO 
        o('titulo_t').innerHTML = editar.titulo
        o('titulo').value = editar.titulo;
        o('titulo_t').style.top = editar.titulo_top;
        o('titulo_t').style.left = editar.titulo_left;
        o('titulo_t').style.fontFamily = editar.titulo_fonte;
        o('titulo_t').style.fontSize = editar.titulo_tamanho + "px";
        o('titulo_tamanho').value = editar.titulo_tamanho;
        o('titulo_t').style.color = "#" + editar.titulo_cor;

        //DESCRICAO  
        o('descricao_t').innerHTML = editar.descricao
        o('descricao').value = editar.descricao;
        o('descricao_t').style.top = editar.descricao_top;
        o('descricao_t').style.left = editar.descricao_left;
        o('descricao_t').style.fontFamily = editar.descricao_fonte;
        o('descricao_t').style.fontSize = editar.descricao_tamanho + "px";
        o('descricao_tamanho').value = editar.descricao_tamanho;
        o('descricao_t').style.color = "#" + editar.descricao_cor;

        //ANIVERSARIANTE 
        o('aniversariantes_t').style.top = editar.aniversariantes_top;
        o('aniversariantes_t').style.left = editar.aniversariantes_left;
        o('aniversariantes_t').style.fontFamily = editar.aniversariantes_fonte;
        o('aniversariantes_t').style.fontSize = editar.aniversariantes_tamanho + "px";
        o('aniversariante_tamanho').value = editar.aniversariantes_tamanho;
        o('aniversariantes_t').style.color = "#" + editar.aniversariantes_cor;

        //DATAHORA 
        o('datahora_t').style.top = editar.datahora_top;
        o('datahora_t').style.left = editar.datahora_left;
        o('datahora_t').style.fontFamily = editar.datahora_fonte;
        o('datahora_t').style.fontSize = editar.datahora_tamanho + "px";
        o('datahora_tamanho').value = editar.datahora_tamanho;
        o('datahora_t').style.color = "#" + editar.datahora_cor;
    } else {
        o('id_template_tema').value = editar.id;
        o('url_image').src = editar.url_image;
    }

    //-------------------------------------------------------PREVIEW -------------------------------------------------------
    //o('img_convite_preview').src = editar.url_image;

    ////TITULO 
    //o('preview_titulo').innerHTML = editar.titulo
    //o('preview_titulo').style.top = editar.titulo_top;
    //o('preview_titulo').style.left = editar.titulo_left;
    //o('preview_titulo').style.fontFamily = editar.titulo_fonte;
    //o('preview_titulo').style.fontSize = editar.fontSize + "px";
    //o('preview_titulo').style.color = "#" + editar.titulo_cor;

    ////DESCRICAO  
    //o('preview_descricao').innerHTML = editar.descricao
    //o('preview_descricao').style.top = editar.descricao_top;
    //o('preview_descricao').style.left = editar.descricao_left;
    //o('preview_descricao').style.fontFamily = editar.descricao_fonte;
    //o('preview_descricao').style.fontSize = editar.descricao_tamanho + "px";
    //o('preview_descricao').style.color = "#" + editar.descricao_cor;

    ////ANIVERSARIANTE 
    //o('preview_aniversariantes').style.top = editar.aniversariantes_top;
    //o('preview_aniversariantes').style.left = editar.aniversariantes_left;
    //o('preview_aniversariantes').style.fontFamily = editar.aniversariantes_fonte;
    //o('preview_aniversariantes').style.fontSize = editar.aniversariantes_tamanho + "px";
    //o('preview_aniversariantes').style.color = "#" + editar.aniversariantes_cor;


    ////DATAHORA 
    //o('preview_datahora').style.top = editar.datahora_top;
    //o('preview_datahora').style.left = editar.datahora_left;
    //o('preview_datahora').style.fontFamily = editar.datahora_fonte;
    //o('preview_datahora').style.fontSize = editar.datahora_tamanho + "px";
    //o('preview_datahora').style.color = "#" + editar.datahora_cor;

    //$('#passoApasso a[href="#step4"]').tab('show');

    o('step4').setAttribute("class", "ls-steps-content ls-active");
    o('step4Tab').setAttribute("class", "ls-active");

    o('step3').setAttribute("class", "ls-steps-content");
    o('step3Tab').setAttribute("class", "");
}

function proximoEditando(evt) {
    o('step4').setAttribute("class", "ls-steps-content ls-active");
    o('step4Tab').setAttribute("class", "ls-active");

    o('step2').setAttribute("class", "ls-steps-content");
    o('step2Tab').setAttribute("class", "");
}

function proximoEditandoConvite(evt) {
    o('step4').setAttribute("class", "ls-steps-content ls-active");
    o('step4Tab').setAttribute("class", "ls-active");

    o('step3').setAttribute("class", "ls-steps-content");
    o('step3Tab').setAttribute("class", "");
}

function VoltarEditando(evt) {

    o('step3').setAttribute("class", "ls-steps-content ls-active");
    o('step3Tab').setAttribute("class", "ls-active");

    o('step4').setAttribute("class", "ls-steps-content");
    o('step4Tab').setAttribute("class", "");
}


function AtualizarTitulo(evt) {
    o('titulo_t').style.fontSize = evt.value + "px";
    o('titulo_t').innerHTML = o('titulo').value;
}

function LocalidadeTitulo(evt) {
    o('titulo_top').value = evt.style.top;
    o('titulo_left').value = evt.style.left;
    ////PREVIEW 
    //o('preview_titulo').style.top = evt.style.top;
    //o('preview_titulo').style.left = evt.style.left;
}

function AtualizarDescricao(evt) {
    o('descricao_t').style.fontSize = evt.value + "px";
    o('descricao_t').innerHTML = o('descricao').value;
}

function LocalidadeDescricao(evt) {
    o('descricao_top').value = evt.style.top;
    o('descricao_left').value = evt.style.left;
    ////PREVIEW 
    //o('preview_descricao').style.top = evt.style.top;
    //o('preview_descricao').style.left = evt.style.left;
}

function AtualizarAniversariantes(evt) {
    o('aniversariantes_t').style.fontSize = evt.value + "px";
}

function LocalidadeAniversariantes(evt) {
    o('aniversariantes_top').value = evt.style.top;
    o('aniversariantes_left').value = evt.style.left;
    ////PREVIEW 
    //o('preview_aniversariantes').style.top = evt.style.top;
    //o('preview_aniversariantes').style.left = evt.style.left;
}


function AtualizarDataHora(evt) {
    o('datahora_t').style.fontSize = o('datahora_tamanho').value + "px";

    if (o('dataDaFesta').value != "") {

        var dataS = o('dataDaFesta').value.split('-');

        var data = dataS[2] + "/" + dataS[1] + "/" + dataS[0];

        if (o('horario_fim_div').style.display == "none" && o('horario_inicio').value != "") {
            o('datahora_t').innerHTML = "Dia " + data + " As " + o('horario_inicio').value;
        }

        if (o('horario_fim_div').style.display == "block" && o('horario_inicio').value != "" && o('horario_fim').value != "") {
            o('datahora_t').innerHTML = "Dia " + data + " De " + o('horario_inicio').value + " As " + o('horario_fim').value;
        }
        o('titulo_datahora').value = o('datahora_t').innerHTML;
    }

    //if (o('dataDaFesta').value != "" && o('horarioDaFesta').value != "") {
    //    var horario = o('horarioDaFesta')[o('horarioDaFesta').selectedIndex].innerHTML.replace(' - ', ' Até ');
    //    o('datahora_t').innerHTML = "Dia " + o('dataDaFesta').value + " De " + horario;
    //}
}

function LocalidadeDataHora(evt) {
    o('datahora_top').value = evt.style.top;
    o('datahora_left').value = evt.style.left;
    ////PREVIEW 
    //o('preview_datahora').style.top = evt.style.top;
    //o('preview_datahora').style.left = evt.style.left;
}


function thumbnail(r, index) {
    return '<div class="item lazyOwl" style=" cursor: pointer; ">' +
             '<a data-index="' + index + '" data-action="next" onclick="temaSelecionado(this)" data-titulo="' + r.titulo + '">' +
                '<div class="thumbnail col-md-11">' +
                    '<img style=" height: 200px; " src="' + r.url_image + '" >' +
                    ' <div class="caption"> <h4 class="text-center">' + r.tema + '</h4></div>' +
                '</div></div>' +
            '</a>';
}


function adicionarOuRemoverTermino(evt) {
    if (o('horario_fim_div').style.display == "block") {
        evt.value = "Adicionar Hora de Término";
        o('horario_fim_div').style.display = "none"
    } else {
        evt.value = "Remover Hora de Término";
        o('horario_fim_div').style.display = "block"
    }
    AtualizarDataHora(evt);
}

//function HorariosDisponiveis() {
//    $.getJSON('/Convite/HorariosDisponiveis', function (horarios) {
//        var selects = "";
//        if (horarios.isOk) {
//            for (var i = 0; i < horarios.horarios.length; i++) {
//                var de = horarios.horarios[i].horario_de.Hours + ":" + horarios.horarios[i].horario_de.Minutes;
//                var ate = horarios.horarios[i].horario_ate.Hours + ":" + horarios.horarios[i].horario_ate.Minutes;
//                selects += ('<option value="' + horarios.horarios[i].id + '">' + de + ' - ' + ate + '</option>');
//            }
//            o('horarioDaFesta').innerHTML = selects;
//        }
//    }, 'json');
//}

function BuscarClienteSelecionado(evt) {
    if (evt.value == "") {
        o('cliente_selecionado').style.display = "none";
        o('next_cliente').disabled = true;
        return;
    }

    o('cliente_selecionado').style.display = "none";
    o('next_cliente').value = "Aguarde...";
    o('next_cliente').disabled = true;
    $.getJSON('/Convite/BuscarClienteSelecionado', { id_cliente: evt.value }, function (cliente) {
        if (cliente.isOk) {
            o('nome_cliente').innerHTML = cliente.data.nome;
            o('email_cliente').innerHTML = cliente.data.email;
            o('telefone_cliente').innerHTML = cliente.data.telefone;
            o('celular_cliente').innerHTML = cliente.data.celular;

            TabelaFamiliares(cliente.data.familiares);

            //o('onde_cliente').innerHTML = cliente.cliente.onde_cliente;
            o('cliente_selecionado').style.display = "block";
        }
        o('next_cliente').value = "Próximo";
        o('next_cliente').disabled = false;
    });
}

function TabelaFamiliares(familiar) {
    var familiares = "<thead> <tr> <th width='50'></th> <th>Nome</th> <th>Nascimento</th> <th>Parentesco</th> </tr> </thead>";
    if (familiar.length == 0) {
        o('tabelaAniversariantes').innerHTML = "";
    } else {
        for (var i = 0; i < familiar.length; i++) {
            familiares += blocoFamiliar(i, familiar[i].id, familiar[i].nome, familiar[i].parentesco, familiar[i].nascimento, familiar[i].idade);
        }
        o('tabelaAniversariantes').innerHTML = familiares;
        o('tabelaAniversariantes').dataset.quantidade = familiar.length;
    }
}


function blocoFamiliarNovo(index, id, nome, parentesco, nascimento, idade) {
    var data = nascimento.split('-');
    //1992-04-03"
    return '<tr>' +
                '<td width="20">' +
                    '<label class="label-checkbox" > ' +
                        '<input type="hidden" name="[' + index + '].id_aniversariante" value="' + id + '" />' +
                        '<input type="hidden" id="' + index + 'familiar" name="[' + index + '].aniversarianteSelecionado" data-nome="' + nome + '" data-ano_nascimento="' + idade + '" value="false" />' +
                        '<input type="checkbox" class="chk-row" onclick="o(\'' + index + 'familiar\').value = this.checked;">' +
                        '<span class="custom-checkbox"></span>' +
                    '</label>' +
                '</td>' +
                '<td>' + nome + '</td>' +
                '<td> ' + data[2] + '/' + data[1] + '/' + data[0] + ' </td>' +
                '<td>' + parentesco + ' </td>' +
            '</tr>';
}


function blocoFamiliar(index, id, nome, parentesco, nascimento) {
    var d = o('configuracao').dataset;
    var selecionado = false;
    if (d.editando) {
        selecionado = Contains(id, d.aniversariantes.split(','));
    }

    return '<tr>' +
                '<td width="20">' +
                    '<label class="label-checkbox" > ' +
                        '<input type="hidden" name="[' + index + '].id_aniversariante" value="' + id + '" />' +
                        '<input type="hidden" id="' + index + 'familiar" name="[' + index + '].aniversarianteSelecionado" data-nome="' + nome + '" data-ano_nascimento="' + (new Date().getYear() - new Date(ConvertDate(nascimento)).getYear()) + '" value="' + selecionado + '" />' +
                        '<input type="checkbox" class="chk-row" onclick="o(\'' + index + 'familiar\').value = this.checked;">' +
                         '<div style="padding:5px">' +
                         '<input type="hidden" name="' + index + '" id="' + index + '" value="' + d.editando + ' />' +
                            '<a id="check_"' + index + '" style="margin-right: 5px;" href="javascript:void(o)" onclick="CheckOrUncheck(this, "' + index + '");"' + "" + '" data-check="' + d.editando + '" >' +
                                '<img src="/Content/Buffet/Imagens/Base/icone_' + d.editando + '.png" width="30" />' +
                                '</a>' +
                                '<label style="cursor:pointer" onclick="CheckOrUncheck(o("check_"' + index + '"), "' + index + '");"' + "" + '" ></label>"' +
                            '</div>' +
                            '</label>' +
                        '</td>' +
                        '<td>' + nome + '</td>' +
                        '<td> ' + ConvertDate(nascimento) + ' </td>' +
                        '<td>' + parentesco + ' </td>' +
                        '</tr>';
}

function Checkeds(nameAndID, callBack, isCkeck, Titulo, Index) {
    return "<div style='padding:5px'>" +
            "<input type='hidden' name='" + nameAndID + "' id='" + nameAndID + "' value='" + (isCkeck ? "true" : "false") + "' />" +
            "<a id='check_" + nameAndID + "' style='margin-right: 5px;' href='javascript:void(o)' onclick='CheckOrUncheck(this, \"" + nameAndID + "\");" + callBack + "' data-check='" + (isCkeck ? "true" : "false") + "'>" +
                    "<img src='/Content/Buffet/Imagens/Base/icone_" + (isCkeck ? "check" : "uncheck") + ".png' width='30' />" +
            "</a>" +
            "<label style='cursor:pointer' onclick='CheckOrUncheck(o('check_" + nameAndID + "'), \"" + nameAndID + "\");" + callBack + "' >" + Titulo + " </label>" +
            "</div>"
}



function ConfirmarAniversariantes(evt) {
    var aniversariantes = "";
    var anivers_cont = 0;
    for (var i = 0; i < o('tabelaAniversariantes').dataset.quantidade; i++) {
        var familiar = o(i + 'familiar');
        if (familiar.value == "true") {
            data = familiar.dataset;
            aniversariantes += " " + data.nome + " - " + data.ano_nascimento + " " + (data.ano_nascimento.length == 1 ? "Ano" : "Anos") + " ";
            anivers_cont++;
        }
    }
    //o('preview_aniversariantes').innerHTML = aniversariantes;
    o('aniversariantes_t').innerHTML = aniversariantes;
}

function NovoFamiliarSalvar(evt) {
    if (o('nome_familiar').value == "" || o('parentesco_familiar').value == "" || o('nascimento_familiar').value == "") {
        o('mensagem_familiar').style.display = "block";
        return;
    } else {
        o('mensagem_familiar').style.display = "none";
    }

    evt.value = "Aguarde...";
    $.getJSON('/Convite/SalvarFamiliar', { id_cliente: v('id_cliente'), nome: v('nome_familiar'), parentesco: v('parentesco_familiar'), nascimento: v('nascimento_familiar'), }, function (data) {
        if (data.isOk) {
            var quantidade = o('tabelaAniversariantes').dataset.quantidade;
            o('tabelaAniversariantes').innerHTML += blocoFamiliarNovo(quantidade, data.id, v('nome_familiar'), v('parentesco_familiar'), v('nascimento_familiar'), data.idade);
            o('tabelaAniversariantes').dataset.quantidade++;
            evt.value = "Adicionar Aniversariante";
        } else {
            evt.value = "Adicionar Aniversariante";
        }
    }, 'json');
}

function BuscarListaDeConvidados(evt) {
    if (evt.value != "") {
        ShowMensagem('Aguarde...', 'Buscando Convidados!');
        $('#table').empty();
        $.getJSON('/Convite/BuscarListaDeConvidados', { id_orcamento: evt.value }, function (data) {
            if (data.length > 0) {
                $('#table').append(cabecalhoTabela());
                for (var i = 0; i < data.length; i++) {
                    $('#table').append('<tr><td>' + data[i].nome_completo +
                                       '</td><td>' + (data[i].email == null ? "" : data[i].email) +
                                       '</td><td>' + (data[i].telefone == null ? "" : data[i].telefone) +
                                       '</td><td>' + (data[i].email == null ? "Telefone" : data[i].ira) + '</td></tr>');
                }
                ShowMensagemError("Sucesso!", "Convidados Localizados.");
            } else {
                ShowMensagemError("Informação!", "Nenhum Convidado Localizados.");
            }
        }, 'json');
    } else {
        $('#table').empty();
    }
}


function cabecalhoTabela() {
    return '<thead><tr><th>Nome</th><th>E-mail</th><th>Telefone</th><th>Presença</th></tr></thead>';
}

function BuscarEnderecoLocal(evt) {
    if (evt.value != "") {
        $.getJSON('/Convite/BuscarEnderecoLocal', { id_local: evt.value }, function (resposta) {
            if (resposta.isOk) {
                o('endereco_informacao').innerHTML = "<h2>" + resposta.endereco + "</h2>";
                o('endereco_informacao').style.display = "block";
            }
        }, 'json');
    }
}

function SalvarConvidado(evt) {
    if (o('nome_completo').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe o nome do convidado!";
        return false;
    }
    if (o('email').value == "" && o('telefone').value == "") {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Você precisa informar no mínimo o Telefone ou Email do convidado!";
        return false;
    }
    if (o('telefone').value.length < 9) {
        o('mensagem').style.display = "block";
        o('textMensagem').innerHTML = "Informe um telefone valido com DDD!";
        return false;
    }

    evt.value = "Aguarde...";
    $.getJSON('/Convite/SalvarConvidado?' + $("#NOVOCONVIDADO").serialize(), function (resposta) {
        if (resposta == true) {
            location.reload();
        }
    }, 'json');
}

//FONTES DISPONIVEIS
function BuscarFrases() {
    $.getJSON('/Convite/BuscarFrases', function (resposta) {
        var cabecalho = "<thead> <tr> <td colspan='3'>Frase</td> <td></td> </tr> </thead>";
        for (var i = 0; i < resposta.frases.length; i++) {
            cabecalho += LinhaFrase(resposta.frases[i]);
        }
        o('tabela_Frases').innerHTML = cabecalho;
    }, 'json');
}

function LinhaFrase(frase) {
    return '<tr id="tr_tabela_frases' + frase.id + '">' +
                '<td>' + (frase.titulo == null ? "" : frase.titulo) + '</td>' +
                '<td>' + frase.descricao + '</td>' +
                '<td>' +
                    '<input type="button" data-dismiss="modal" class="btn btn-xs btn-success" data-titulo="' + frase.titulo + '" data-descricao="' + frase.descricao + '" onclick="SelecionarFrase(this)" value="Selecionar" />' +
                '</td>' +
            '</tr>';
}

function SelecionarFrase(evt) {
    if (evt.dataset.titulo != "null") {
        o('titulo').value = evt.dataset.titulo;
        o('titulo_t').innerHTML = evt.dataset.titulo;
    } else {
        o('titulo').value = "";
        o('titulo_t').innerHTML = "";
    }

    o('descric').value = evt.dataset.descricao;
    o('descricao_t').innerHTML = evt.dataset.descricao;
}

//SOLICITAR TEMA
function SolicitarTema(evt) {
    if (o('nome_tema').value == "") {
        o('alerta_tema').style.display = "block";
        return;
    }
    evt.value = "Aguarde...";
    $.getJSON('/Convite/SolicitarTema', { nome_tema: o('nome_tema').value }, function (resposta) {
        if (resposta.isOk) {
            o('alerta_tema').innerHTML = '<strong>Informação!</strong> Tema solicitado com sucesso!';
            o('alerta_tema').style.display = "block";
            evt.value = "Solicitar Tema";
        } else {
            o('alerta_tema').innerHTML = '<strong>Atenção!</strong> Verifique as informações e tente novamente!';
        }
    }, 'json');
}



//RESPOSTA

function ConfirmaPresencao(evt, presenca) {
    $.getJSON('/Convite/ConfirmarPresenca', { presenca: presenca }, function (resposta) {

    }, 'json');
}


