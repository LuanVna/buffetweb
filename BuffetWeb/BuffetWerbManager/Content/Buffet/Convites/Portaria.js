﻿function VerificarInformacoes(evt, id, id_orcamento) {
    for (var i = 0; i < evt.dataset.convidados; i++) {
        if (o(id + '_email_familiar_' + i).value.length > 0 && !isEmail(o(id + '_email_familiar_' + i).value)) {
            o('alert_' + id).style.display = "block";
            o('alert_' + id).innerHTML = "Algum email não esta em um formato valido!";
            return;
        }
        if (o(id + '_telefone_familiar_' + i).value.length > 0) {
            if (o(id + '_telefone_familiar_' + i).value.length < 12 || o(id + '_telefone_familiar_' + i).value.length < 13) {
                o('alert_' + id).style.display = "block";
                o('alert_' + id).innerHTML = "Algum telefone não esta em um formato valido!";
                return;
            }
        }
        if (o(id + '_nome_familiar_' + i).value.length == 0 && (o(id + '_email_familiar_' + i).value.length > 0 || o(id + '_telefone_familiar_' + i).value.length > 0)) {
            o('alert_' + id).style.display = "block";
            o('alert_' + id).innerHTML = "Informe o nome do familiar!";
            return;
        }
    }

    for (var i = 0; i < evt.dataset.convidados; i++) {
        if (o(id + '_nome_familiar_' + i).value != "") {
            $.getJSON('/Convite/ConfirmarFamiliar',
                    {
                        i: i,
                        id_convidado: id,
                        id_orcamento: id_orcamento,
                        id: o(id + '_id_familiar_' + i).value,
                        nome: o(id + '_nome_familiar_' + i).value,
                        email: o(id + '_email_familiar_' + i).value,
                        telefone: o(id + '_telefone_familiar_' + i).value,
                        tipo_convidado: o(id + '_tipo_convidado_familiar_' + i).value,
                        convidados: evt.dataset.convidados
                    },
                    function (data) {
                        if (data.isOk) {
                            o(id + '_id_familiar_' + data.i).value = data.id;
                        }
                    }, 'json');
        }
    }


    o('alert_' + id).style.display = "none";
    $('#familiares_' + id).modal('hide');
}

function NovoFamiliar(evt, id) {
    var confirmar = o('confirmar_familiares_' + id);
    var i = confirmar.dataset.convidados;
    var novo = '<input type="hidden" value="0" id="' + id + '_id_familiar_' + i + '" />' +
                    '<div class="col-md-12">' +
                        '<div class="col-md-4">' +
                            '<label>Nome</label>' +
                        '<input type="text" class="form-control" id="' + id + '_nome_familiar_' + i + '" />' +
                    '</div>' +
                            '<div class="col-md-2">' +
                            '<label>Convidado</label>' +
                        '<select class="form-control" id="' + id + '_tipo_convidado_familiar_' + i + '">' +
                            '<option value="Adulto">Adulto</option>' +
                            '<option value="Criança">Criança</option>' +
                        '</select>' +
                                '</div>' +
                                '<div class="col-md-3">' +
                            '<label>Email</label>' +
                        '<input type="text" class="form-control" id="' + id + '_email_familiar_' + i + '" />' +
                        '</div>' +
                            '<div class="col-md-3">' +
                            '<label>Telefone</label>' +
                        '<input type="text" class="form-control" id="' + id + '_telefone_familiar_' + i + '" pattern="^.{13,13}$|^.{12,12}$" onkeypress="DIMask(this, \'##-#####-####\'); return DIOnlyNumber(this);" maxlength="13" />' +
                        '</div>' +
                '</div>';
    o('novo_convidado_' + id).innerHTML += novo;
    confirmar.dataset.convidados++;
}

function ValidaNovoConvidado() {
    if (o('novo_email').value.length > 0 && !isEmail(o('novo_email').value)) {
        o('novo_alert').style.display = "block";
        o('novo_alert').innerHTML = "Algum email não esta em um formato valido!";
        return false;
    }
    if (o('novo_telefone').value.length > 0) {
        if (o('novo_telefone').value.length < 12 || o('novo_telefone').value.length < 13) {
            o('novo_alert').style.display = "block";
            o('novo_alert').innerHTML = "Algum telefone não esta em um formato valido!";
            return false;
        }
    }
    if (o('novo_nome').value.length == 0) {
        o('novo_alert').style.display = "block";
        o('novo_alert').innerHTML = "Informe o nome do familiar!";
        return false;
    }
    if (o('novo_acompanhante').value.length == 0) {
        o('novo_alert').style.display = "block";
        o('novo_alert').innerHTML = "Informe a quantidade de familiares!";
        return false;
    }
    return true;
}



function ConfirmarPresenca(evt, id_orcamento, id) {
    var auxValue = evt.value;
    evt.value = "Aguarde...";
    $.getJSON('/Convite/ConfirmarPresenca', { id: id, id_orcamento_convite: id_orcamento, email: o('email_' + id).value, telefone: o('telefone_' + id).value, status: auxValue, convidados: evt.dataset.convidados }, function (resposta) {
        if (resposta.isOk) {
            if (auxValue != "Confirmar") {
                evt.value = "Confirmar";
                o("presenca_" + id).setAttribute("class", "btn btn-success btn-block");
            } else {
                evt.value = "Confirmado";
                o("presenca_" + id).setAttribute("class", "btn btn-info btn-block");
            }
        }
    }, 'json');
}

function SalvarEmailETelefone(evt, id_orcamento, id) {
    if ((o('telefone_' + id).value.length == 13 || o('telefone_' + id).value.length == 12) && isEmail(o('email_' + id).value)) {
        $.getJSON('/Convite/SalvarEmailETelefone', { id: id, id_orcamento: id_orcamento, email: o('email_' + id).value, telefone: o('telefone_' + id).value }, function (resposta) {
            if (resposta.isOk) {

            }
        }, 'json');
    }
}