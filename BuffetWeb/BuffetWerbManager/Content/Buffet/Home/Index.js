﻿function validaFormularioLogin(evt) {
    if (o('email').value == "" && o('senha').value == "") {
        return false;
    }
    return true;
}

function VerificaEmail(evt) {
    if (isEmail(evt.value)) {
        $.getJSON('/Home/VerificaEmail', { email: evt.value }, function (data) {
            if (data.isOk) {

                o('CriarAcesso').disabled = data.existe;

                if (data.existe) {
                    o('mensagem').style.display = "block";
                    o('mensagem').innerHTML = "Este e-mail já esta sendo utilizado!";
                    return;
                }
                o('mensagem').style.display = "none";
                return;
            }
        }, 'json');
    }
}

function validaFormularioResetarSenha(evt) {
    if (o('cSenha').value == "" || o('senha').value == "") {
        o('mensagem').innerHTML = "Preencha os dois campos!";
        return false;
    }
    if (o('cSenha').value == o('senha').value) {
        o('mensagem').innerHTML = "As senhas não conferem!";
        return false;
    }
    return true;
}

function EsqueciMinhaSenha(evt) {
    o('mensagem').style.display = "block";
    if (o('email').value == "") {
        o('mensagem').innerHTML = "Você precisa informar seu e-mail de login";
        return;
    }
    if (!isEmail(o('email').value)) {
        o('mensagem').innerHTML = "Informe um e-mail válido";
        return;
    }
    o('mensagem').innerHTML = "Aguarde...";
    $.getJSON('/Home/EsqueciMinhaSenha', { email: o('email').value }, function (data) {
        o('mensagem').innerHTML = data.mensagem;
    }, 'json');
}