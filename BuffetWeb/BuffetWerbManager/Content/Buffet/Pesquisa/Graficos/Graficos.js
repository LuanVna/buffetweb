﻿var satisfeito = 0;
var insatisfeito = 0;
var mediaTotal = 0;

$(document).ready(function () {

    if (o('configuracao_convidados') != null) { 
        var all_convidados = o('configuracao_convidados').dataset.all_convidados.split(',');
        for (var i = 0; i < all_convidados.length; i++) {
            if (all_convidados[i] != "") {
                var id = "#owl-convidados_" + all_convidados[i];
                $(id).owlCarousel({
                    navigation: false,
                    items: 3,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [980, 2],
                    itemsTablet: [768, 2],
                });
            }
        }
    } else {
        var owls = [{ carrosel: "owl-sorrisos", items: 3 }, { carrosel: "owl-gostei", items: 4 }, { carrosel: "owl-estrela", items: 3 }];
        for (var i = 0; i < owls.length; i++) {
            $("#" + owls[i].carrosel).owlCarousel({
                navigation: false,
                items: owls[i].items,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [980, 2],
                itemsTablet: [768, 2],
                //navigationText: [
                //  "<i class='fa fa-chevron-left icon-white'></i>",
                //  "<i class='fa fa-chevron-right icon-white'></i>"
                //]
            });
        }


        var all_satisfacao = o('all_satisfacao').value.split(',');
        for (var i = 0; i < all_satisfacao.length; i++) {
            if (all_satisfacao[i] != "") {
                $("#owl-satisfacao_" + all_satisfacao[i]).owlCarousel({
                    navigation: false,
                    items: 3,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [980, 2],
                    itemsTablet: [768, 2],
                });
            }
        }

        $.getJSON('/Pesquisa/PerguntasPesquisa', { id_pesquisa: o('configuracao').dataset.id_pesquisa }, function (data) {
            if (data.isOk) {
                for (var i = 0; i < data.perguntas.length; i++) {
                    if (data.perguntas[i].tipo_pergunta == "SORRISO") {
                        GraficoSorriso(data.perguntas[i].id, data.perguntas[i].quantidade);
                    }
                    if (data.perguntas[i].tipo_pergunta == "GOSTEI") {
                        GraficoGostei(data.perguntas[i].id, data.perguntas[i].quantidade);
                    }
                    if (data.perguntas[i].tipo_pergunta == "ESTRELA") {
                        GraficoEstrela(data.perguntas[i].id, data.perguntas[i].quantidade);
                    }
                    if (data.perguntas[i].tipo_pergunta == "SATISFACAO") {
                        GraficoSatisfacao(data.perguntas[i].id, data.perguntas[i].quantidade);
                    }
                }
            }
        }, 'json');
    }
});

function GraficoSatisfacao(id_pergunta, quantidade) {
    $.getJSON('/Pesquisa/RespostaSatisfacao', { id_pergunta: id_pergunta }, function (data) {
        if (data.isOk) {
            for (var i = 0; i < data.satisfacao.length; i++) {
                var opcao = data.satisfacao[i];

                if (opcao.isResposta) {

                    var options = {
                        responsive: true,
                    };

                    var grafico = [
                        {
                            value: opcao.opcoes.pessimo,
                            color: "#FE5E27",
                            highlight: "#FE5E27",
                            label: "Péssimo"
                        },
                        {
                            value: opcao.opcoes.ruim,
                            color: "#EBBB5E",
                            highlight: "#EBBB5E",
                            label: "Ruim"
                        },
                        {
                            value: opcao.opcoes.normal,
                            color: "#CCCCCC",
                            highlight: "#CCCCCC",
                            label: "Normal"
                        },
                        {
                            value: opcao.opcoes.bom,
                            color: "#33CC33",
                            highlight: "#33CC33",
                            label: "Bom"
                        },
                        {
                            value: opcao.opcoes.exelente,
                            color: "#FFD702",
                            highlight: "#FFD702",
                            label: "Exelente"
                        }];


                    var legenda = "";
                    if (opcao.opcoes.pessimo != 0) {
                        legenda += Legenda("Péssimo", "#FE5E27", (opcao.opcoes.pessimo / opcao.total) * 100, opcao.pessimo);
                        insatisfeito++;
                    }
                    if (opcao.opcoes.ruim != 0) {
                        legenda += Legenda("Ruim", "#EBBB5E", (opcao.opcoes.ruim / opcao.total) * 100, opcao.ruim);
                    }
                    if (opcao.opcoes.normal != 0) {
                        legenda += Legenda("Normal", "#CCCCCC", (opcao.opcoes.normal / opcao.total) * 100, opcao.normal);
                        satisfeito++;
                    }
                    if (opcao.opcoes.bom != 0) {
                        legenda += Legenda("Bom", "#33CC33", (opcao.opcoes.bom / opcao.total) * 100, opcao.bom);
                        satisfeito++;
                    }
                    if (opcao.opcoes.exelente != 0) {
                        legenda += Legenda("Exelente", "#FFD702", (opcao.opcoes.exelente / opcao.total) * 100, opcao.exelente);
                        satisfeito++;
                    }

                    o('titulo_satisfacao_' + opcao.id).innerHTML = opcao.titulo;
                    $('#loading_satisfacao_' + opcao.id).remove();
                    o('legenda_satisfacao_' + opcao.id).innerHTML = legenda;
                    var ctx = document.getElementById('cavas_satisfacao_' + opcao.id).getContext("2d");
                    var PizzaChart = new Chart(ctx).Doughnut(grafico, options);

                } else {
                    o('titulo_satisfacao_' + opcao.id).innerHTML = opcao.titulo + "<br /><br /> Nenhuma resposta!";
                    $('#loading_satisfacao_' + opcao.id).remove();
                }
            }
        }
    }, 'json');
}


function GraficoGostei(id_pergunta, quantidade) {
    if (quantidade != 0) {
        $.getJSON('/Pesquisa/RespostaGostei', { id_pergunta: id_pergunta }, function (data) {
            if (data.isOk) {
                o('legenda_left_' + id_pergunta).innerHTML = ((data.gostei.resposta.gostei / data.gostei.total) * 100).toFixed(0) + '%';
                o('legenda_right_' + id_pergunta).innerHTML = ((data.gostei.resposta.naogostei / data.gostei.total) * 100).toFixed(0) + '%';

                o('titulo_' + data.gostei.id_pergunta).innerHTML = data.gostei.titulo;
                $('#loading_' + data.gostei.id_pergunta).remove();

                satisfeito += data.gostei.resposta.gostei;
                satisfeito += data.gostei.resposta.naogostei;
                mediaTotal += data.gostei.total;

                var options = {
                    responsive: true,
                };

                var grafico = [
                    {
                        value: data.gostei.resposta.gostei,
                        color: "#0480FF",
                        highlight: "#0480FF",
                        label: "Gostei"
                    },
                    {
                        value: data.gostei.resposta.naogostei,
                        color: "#FF0000",
                        highlight: "#FF0000",
                        label: "Não Gostei"
                    }];
                var ctx = document.getElementById('cavas_' + data.gostei.id_pergunta).getContext("2d");
                var PizzaChart = new Chart(ctx).Doughnut(grafico, options);

                MediaTotal();
            } else {
                o('legenda_' + data.gostei.id_pergunta).innerHTML = "Nenhuma resposta!";
            }
            $('#loading_' + data.gostei.id_pergunta).remove();
        }, 'json');
    } else {
        o('titulo_' + id_pergunta).innerHTML = "Nenhuma resposta!";
        $('#loading_' + id_pergunta).remove();
    }
}

function GraficoEstrela(id_pergunta, quantidade) {
    if (quantidade != 0) {
        $.getJSON('/Pesquisa/RespostaEstrela', { id_pergunta: id_pergunta }, function (data) {
            if (data.isOk) {

                var options = {
                    responsive: true,
                };

                var grafico = [
                    {
                        value: data.estrela.resposta.UmaEstrela,
                        color: "#FFD700",
                        highlight: "#FFD700",
                        label: "1 Estrela"
                    },
                    {
                        value: data.estrela.resposta.DuasEstrelas,
                        color: "#FFD701",
                        highlight: "#FFD701",
                        label: "2 Estrela"
                    }, {
                        value: data.estrela.resposta.TresEstrelas,
                        color: "#FFD702",
                        highlight: "#FFD702",
                        label: "3 Estrela"
                    },
                    {
                        value: data.estrela.resposta.QuatroEstrelas,
                        color: "#FFD704",
                        highlight: "#FFD704",
                        label: "4 Estrela"
                    },
                    {
                        value: data.estrela.resposta.CincoEstrelas,
                        color: "#FFD705",
                        highlight: "#FFD705",
                        label: "5 Estrela"
                    }, ];

                var legenda = "";
                if (data.estrela.resposta.CincoEstrelas != 0) {
                    legenda += Legenda("5 Estrelas", "#FFD701", (data.estrela.resposta.CincoEstrelas / data.estrela.total) * 100, data.estrela.resposta.CincoEstrelas);
                    insatisfeito++;
                }
                if (data.estrela.resposta.QuatroEstrelas != 0) {
                    legenda += Legenda("4 Estrelas", "#FFD702", (data.estrela.resposta.QuatroEstrelas / data.estrela.total) * 100, data.estrela.resposta.QuatroEstrelas);
                    insatisfeito++;
                }
                if (data.estrela.resposta.TresEstrelas != 0) {
                    legenda += Legenda("3 Estrelas", "#FFD703", (data.estrela.resposta.TresEstrelas / data.estrela.total) * 100, data.estrela.resposta.TresEstrelas);
                }
                if (data.estrela.resposta.DuasEstrelas != 0) {
                    legenda += Legenda("2 Estrelas", "#FFD704", (data.estrela.resposta.DuasEstrelas / data.estrela.total) * 100, data.estrela.resposta.DuasEstrelas);
                    satisfeito++;
                }
                if (data.estrela.resposta.UmaEstrela != 0) {
                    legenda += Legenda("1 Estrela", "#FFD705", (data.estrela.resposta.UmaEstrela / data.estrela.total) * 100, data.estrela.resposta.UmaEstrela);
                    satisfeito++;
                }

                mediaTotal += data.estrela.total;

                if (o('legenda_' + data.estrela.id_pergunta) != null) {
                    o('legenda_' + data.estrela.id_pergunta).innerHTML = legenda;
                    o('titulo_' + data.estrela.id_pergunta).innerHTML = data.estrela.titulo;
                    var ctx = document.getElementById('cavas_' + data.estrela.id_pergunta).getContext("2d");
                    var PizzaChart = new Chart(ctx).Doughnut(grafico, options);
                    MediaTotal();
                }
            } else {
                o('legenda_' + data.estrela.id_pergunta).innerHTML = "Nenhuma resposta!";
            }
            $('#loading_' + data.estrela.id_pergunta).remove();
        }, 'json');
    } else {
        o('titulo_' + id_pergunta).innerHTML = "Nenhuma resposta!";
        $('#loading_' + id_pergunta).remove();
    }
}


function GraficoSorriso(id_pergunta, quantidade) {
    if (quantidade != 0) {
        $.getJSON('/Pesquisa/RespostaSorriso', { id_pergunta: id_pergunta }, function (data) {
            if (data.isOk) {

                var options = {
                    responsive: true,
                };

                var grafico = [
                    {
                        value: data.sorriso.resposta.muito_insatisfeito,
                        color: "#FE5E27",
                        highlight: "#FE5E27",
                        label: "Muito Insatisfeito"
                    },
                    {
                        value: data.sorriso.resposta.insatisfeito,
                        color: "#EBBB5E",
                        highlight: "#EBBB5E",
                        label: "Insatisfeito"
                    }, {
                        value: data.sorriso.resposta.indiferente,
                        color: "#FFC744",
                        highlight: "#FFC744",
                        label: "Indiferente"
                    },
                    {
                        value: data.sorriso.resposta.satisfeito,
                        color: "#FFA93B",
                        highlight: "#FFA93B",
                        label: "Satisfeito"
                    },
                    {
                        value: data.sorriso.resposta.muito_satisfeito,
                        color: "#FEE94F",
                        highlight: "#FEE94F",
                        label: "Muito Satisfeito"
                    }, ];

                var legenda = "";
                if (data.sorriso.resposta.muito_satisfeito != 0) {
                    legenda += Legenda("Muito Satisfeito", "#FEE94F", (data.sorriso.resposta.muito_satisfeito / data.sorriso.total) * 100, data.sorriso.resposta.muito_satisfeito);
                    satisfeito++;
                }
                if (data.sorriso.resposta.satisfeito != 0) {
                    legenda += Legenda("Satisfeito", "#FFA93B", (data.sorriso.resposta.satisfeito / data.sorriso.total) * 100, data.sorriso.resposta.satisfeito);
                    satisfeito++;
                }
                if (data.sorriso.resposta.indiferente != 0) {
                    legenda += Legenda("Indiferente", "#FFC744", (data.sorriso.resposta.indiferente / data.sorriso.total) * 100, data.sorriso.resposta.indiferente);
                }
                if (data.sorriso.resposta.insatisfeito != 0) {
                    legenda += Legenda("Insatisfeito", "#EBBB5E", (data.sorriso.resposta.insatisfeito / data.sorriso.total) * 100, data.sorriso.resposta.insatisfeito);
                    insatisfeito++;
                }
                if (data.sorriso.resposta.muito_insatisfeito != 0) {
                    legenda += Legenda("Muito Insatisfeito", "#FE5E27", (data.sorriso.resposta.muito_insatisfeito / data.sorriso.total) * 100, data.sorriso.resposta.muito_insatisfeito);
                    insatisfeito++;
                }

                mediaTotal += data.sorriso.total;

                if (o('legenda_' + data.sorriso.id_pergunta) != null) {
                    o('legenda_' + data.sorriso.id_pergunta).innerHTML = legenda;
                    o('titulo_' + data.sorriso.id_pergunta).innerHTML = data.sorriso.titulo;
                    o('loading_' + data.sorriso.id_pergunta).innerHTML = "";
                    var ctx = document.getElementById('cavas_' + data.sorriso.id_pergunta).getContext("2d");
                    var PizzaChart = new Chart(ctx).Doughnut(grafico, options);
                    $('#loading_' + data.sorriso.id_pergunta).remove();
                    MediaTotal();
                }
            }
        }, 'json');

    } else {
        o('titulo_' + id_pergunta).innerHTML = "Nenhuma resposta!";
        $('#loading_' + id_pergunta).remove();
    }
}

function MediaTotal() {
    //var options = {
    //    responsive: true,
    //};

    //var grafico = [
    //    {
    //        value: ((insatisfeito / mediaTotal) * 100).toFixed(0) + '%',
    //        color: "#FE5E27",
    //        highlight: "#FE5E27",
    //        label: "Insatisfeito"
    //    },
    //    {
    //        value: ((satisfeito / mediaTotal) * 100).toFixed(0) + '%',
    //        color: "#46BFBD",
    //        highlight: "#5AD3D1",
    //        label: "Satisfeito"
    //    }];
    //var ctx = document.getElementById('mediaGeral').getContext("2d");
    //var PizzaChart = new Chart(ctx).Doughnut(grafico, options);
}

function Legenda(titulo, cor, porcentagem, respostas) {
    return '<div style="margin-top:5px">' +
              '<span class="label" style="background-color:' + cor + '; color: white;margin-right:5px">' + porcentagem.toFixed(0) + '%</span>' +
              '<small>' + titulo + '</small>';
    '</div>';
}

