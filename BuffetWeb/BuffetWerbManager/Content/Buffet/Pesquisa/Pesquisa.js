﻿
//SATISFACAO
var index_satisfacao = 0;
var all_satisfacao = [];
var all_opcoes_apagadas = [];

$(document).ready(function () {
    if (o('owl-demo') != null) {
        $("#owl-demo").owlCarousel({
            items: 1,
            //navigation: false,
            //pagination: false,
            paginationNumbers: true,
            mouseDrag: false,
            touchDrag: false,
            singleItem: true,
        });

        if (o('configuracao').dataset.item_carrossel != 0) {
            UIShow('remove_item');
        }
        if (o('configuracao').dataset.satisfacao_titulos.length == 1) {
            all_satisfacao[0] = o('configuracao').dataset.satisfacao_titulos;
        } else {
            all_satisfacao = o('configuracao').dataset.satisfacao_titulos.split(',');
        }
    }
});

function AbrirSatisfacao(evt) {
    UIHide('satisfacao_opcoes_tabela');
    UIHide('titulo_satisfacao_repetido');

    UIShow('descricao_opcoes_tabela');
    o('titulo_pergunta_opcoes').disabled = false;
    o('titulo_pergunta_opcoes').value = "";
    o('opcao').value = ""; 

    o('tabelaSatisfacao').innerHTML = '<thead> <tr> <th width="200" align="center">Avaliação</th> <th width="50" align="center">Péssimo</th> <th width="50" align="center">Ruim</th> <th width="50" align="center">Normal</th> <th width="50" align="center">Bom</th> <th width="50" align="center">Exelente</th> <th></th> </tr> </thead>';
    o('botao_satisfacao').value = "Adicionar";
    $('#satisfacao').modal('show');
}









function VerificaParaCriar() {
    if (o('copiar_pergunta').value == "true" && o('id_pesquisa').value == 0) {
        o('alert_pesquisa').innerHTML = "<strong>Atenção</strong> Você precisa selecionar uma pesquisa!";
        UIShow('alert_pesquisa');
        return false;
    }

    if (o('id_orcamento').value == 0) {
        o('alert_pesquisa').innerHTML = "<strong>Atenção</strong> Você precisa selecionar um evento!";
        UIShow('alert_pesquisa');
        return false;
    }
    UIHide('alert_pesquisa');
    return true;
}

function MostrarComboPesquisas(evt) {
    if (evt.dataset.check == "true") {
        UIShow('combo_associar_pergunta');
    } else {
        UIHide('combo_associar_pergunta');
    }
}

function VerificaPerguntaRepetida(evt) {
    if (o('botao_satisfacao').value == "Adicionar" && evt.value != "") {
        for (var i = 0; i < all_satisfacao.length; i++) {
            if (all_satisfacao[i] == evt.value) {
                UIShow('titulo_satisfacao_repetido');
                o('botao_satisfacao').disabled = true;
                return;
            }
        }
    }
    UIHide('titulo_satisfacao_repetido');
    o('botao_satisfacao').disabled = false;
}

//SATISFACAO 
function AdicionarOpcao(evt, opcao) {
    if (opcao.value != "" && o('titulo_pergunta_opcoes').value != "") {
        if (o('botao_satisfacao').value == "Adicionar") {
            for (var i = 0; i < all_satisfacao.length; i++) {
                if (all_satisfacao[i] == o('titulo_pergunta_opcoes').value) {
                    UIShow('titulo_satisfacao_repetido');
                    return;
                }
            }
        }
        var titulo_pergunta = o('titulo_pergunta_opcoes').value.replace(' ', '');
        for (var i = 0; i < 10; i++) {
            titulo_pergunta = titulo_pergunta.replace(' ', '');
        }

        o('tabelaSatisfacao').innerHTML += ('<tr id="' + titulo_pergunta + '_avaliacao_' + o('configuracao').dataset.opcoes + '" data-grupo="' + titulo_pergunta + '" data-titulo_opcao_id="" data-titulo_opcao="' + opcao.value + '"> <td align="left">' + opcao.value + '</td> <td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td> <td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td> <td> <input type="button" class="btn btn-danger btn-sm" onclick="RemoverOpcao(this, \'' + titulo_pergunta + '_avaliacao_' + o('configuracao').dataset.opcoes + '\')" value="-">  </td> </tr>');

        o('configuracao').dataset.opcoes++;
        opcao.value = "";
        UIShow('satisfacao_opcoes_tabela');
        UIHide('descricao_opcoes_tabela');
        UIHide('titulo_satisfacao_repetido');
        o('titulo_pergunta_opcoes').disabled = true;
    }
}

function RemoverOpcao(evt, id) {

    if (o(id).dataset.titulo_opcao_id != "") {
        all_opcoes_apagadas[all_opcoes_apagadas.length++] = o(id).dataset.titulo_opcao_id;
    }

    //var ultimo = [];
    //for (var i = 0; i < 10; i++) {
    //    if (o(o(id).dataset.grupo + '_editar_avaliacao_' + i) == null) {
    //        ultimo[i] = true;
    //    }
    //}

    //if (ultimo.length == 50) {
    //    UIShow('descricao_opcoes_tabela');
    //    UIHide('satisfacao_opcoes_tabela');
    //    o('titulo_pergunta_opcoes').disabled = false;

    //    for (var i = 0; i < all_satisfacao.length; i++) {
    //        if (all_satisfacao[i] == o('titulo_pergunta_opcoes').value) {
    //            all_satisfacao[i] = '';
    //            index_satisfacao--;
    //            break;
    //        }
    //    }
    //}

    o(id).remove();
    o('configuracao').dataset.opcoes--;
}

function AdicionarSatisfacao(evt) {
    if (evt.value == "Adicionar") {
        if (o('configuracao').dataset.opcoes != 0) {
            var satisfacao = '<div class="item col-md-12"  ><div style=" margin-left: 20%; " class="col-md-6"><div class="col-md-12" style=" margin-left: 8%; " ><h3 data-id="" data-tipo_pesquisa="SATISFACAO" class="text-center" id="satisfacao_' + o('configuracao').dataset.satisfacao + '">' + o('titulo_opcoes').innerHTML + '</h3></div><div> <div class="col-md-12"> <div class="col-md-12"> <h3 class="text-center" id="texto_gostei"></h3> </div> <div style=" margin-left: 15%; ">' + o('opcoes_preview').innerHTML + '</div> </div></div><div class="col-md-12" style="margin-top: 30px; "><div></div></div></div></div>';
            addInCarousel(satisfacao);
            o('tabelaSatisfacao').innerHTML = '<thead> <tr> <th width="200" align="center">Avaliação</th> <th width="50" align="center">Péssimo</th> <th width="50" align="center">Ruim</th> <th width="50" align="center">Normal</th> <th width="50" align="center">Bom</th> <th width="50" align="center">Exelente</th> <th></th> </tr> </thead>';
            o('configuracao').dataset.satisfacao++;
        }
    } else {
        var tabela = o('opcoes_preview').innerHTML;
        for (var i = 0; i < 100; i++) {
            tabela = tabela.replace('_editar_', '_');
        }
        var tpo = o('titulo_pergunta_opcoes').value;
        for (var uu = 0; uu < 10; uu++) {
            tpo = tpo.replace(' ', '');
        }
        o(tpo + '_tabela_satisfacao').innerHTML = tabela;
    }

    $("#satisfacao").modal('hide');

    evt.value = "Adicionar";

    all_satisfacao[all_satisfacao.length] = o('titulo_pergunta_opcoes').value;
    o('titulo_pergunta_opcoes').disabled = false;
    o('titulo_pergunta_opcoes').value = "";

    UIHide('satisfacao_opcoes_tabela');
    UIShow('descricao_opcoes_tabela');
}

function EditarSatisfacao(evt) {
    var id = evt.dataset.id;
    var quantidade_satisfacao = evt.dataset.quantidade_satisfacao;
    var titulo = evt.dataset.titulo_satisfacao;

    o('titulo_opcoes').innerHTML = titulo;
    o('titulo_pergunta_opcoes').value = titulo;
    o('titulo_pergunta_opcoes').disabled = true;
    o('botao_satisfacao').value = "Atualizar";

    o('tabelaSatisfacao').innerHTML = '<thead> <tr> <th width="200" align="center">Avaliação</th> <th width="50" align="center">Péssimo</th> <th width="50" align="center">Ruim</th> <th width="50" align="center">Normal</th> <th width="50" align="center">Bom</th> <th width="50" align="center">Exelente</th> <th></th> </tr> </thead>';
    for (var i = 0; i < quantidade_satisfacao + 1; i++) {
        var tr_opcao = o(titulo.replace(' ', '') + '_avaliacao_' + i);
        if (tr_opcao != null) {
            o('tabelaSatisfacao').innerHTML += ('<tr id="' + titulo.replace(' ', '') + '_editar_avaliacao_' + i + '" data-grupo="' + titulo.replace(' ', '') + '" data-titulo_opcao_id="' + tr_opcao.dataset.titulo_opcao_id + '" data-titulo_opcao="' + tr_opcao.dataset.titulo_opcao + '"> <td align="left">' + tr_opcao.dataset.titulo_opcao + '</td> <td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td> <td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td><td align="center"><img src="/Content/Buffet/Pesquisa/Imagens/icone_multipla_escolha_cinza.png" width="30" /></td> <td> <input type="button" class="btn btn-danger btn-sm" onclick="RemoverOpcao(this, \'' + titulo.replace(' ', '') + '_editar_avaliacao_' + i + '\')" value="-">  </td> </tr>');
        }
    }

    UIShow('satisfacao_opcoes_tabela');
    UIHide('descricao_opcoes_tabela');

    $("#satisfacao").modal("show");
}


//SORRISO 
var id_sorriso_edita = "";
function AdicionarSorriso(evt) {
    if (evt.value == "Adicionar") {
        var sorriso = '<div class="item col-md-12" ><div style=" margin-left: 30%; " class="col-md-5"><div class="col-md-12" style=" margin-bottom: 16px; "><h3 data-tipo_pesquisa="SORRISO" class="text-center" style="cursor:pointer" onclick="EditarSorriso(this)" id="sorriso_' + o('configuracao').dataset.sorriso + '">' + o('texto_sorriso').innerHTML + '</h3></div><div style=" margin-left: 12%;"><table><tbody><tr><td width="70"><img src="/Content/Buffet/Pesquisa/Imagens/muito_insatisfeito.png" width="60" ></td><td width="70"><img src="/Content/Buffet/Pesquisa/Imagens/insatisfeito.png" width="60" ></td><td width="70"><img src="/Content/Buffet/Pesquisa/Imagens/indiferente.png" width="60" ></td><td width="70"><img src="/Content/Buffet/Pesquisa/Imagens/satisfeito.png" width="60" ></td><td width="70"><img src="/Content/Buffet/Pesquisa/Imagens/muito_satisfeito.png" width="60" ></td></tr></tbody></table></div><div class="col-md-12" style="margin-top: 30px; "><div></div></div></div></div>';
        addInCarousel(sorriso);
        o('configuracao').dataset.sorriso++;
    } else {
        o(id_sorriso_edita).innerHTML = o('texto_sorriso').innerHTML;
    }
    $("#sorrisos").modal('hide');
    o('botao_sorriso').value = "Adicionar";
}

function EditarSorriso(evt) {
    o('botao_sorriso').value = "Atualizar";
    o('texto_sorriso').innerHTML = evt.innerHTML;
    o('texto_sorriso_text').value = evt.innerHTML;
    $('#sorrisos').modal('show');
    id_sorriso_edita = evt.id;
}

//ESTRELA 
var id_estrela_edita = "";
function AdicionarEstrela(evt) {
    if (evt.value == "Adicionar") {
        var estrela = '<div class="item col-md-12" ><div style=" margin-left: 30%; " class="col-md-5"><div class="col-md-12" style=" margin-bottom: 16px; "><h3 data-id="" data-tipo_pesquisa="ESTRELA" class="text-center" style="cursor:pointer" onclick="EditarEstrela(this)" id="estrela_' + o('configuracao').dataset.estrela + '">' + o('texto_estrela').innerHTML + '</h3></div><div style=" margin-left: 12%;"><table> <tbody> <tr> <td width="70"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png" width="60"> </td> <td width="70"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png" width="60"> </td> <td width="70"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png" width="60"> </td> <td width="70"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png" width="60"> </td> <td width="70"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_estrela_selecionada.png" width="60"> </td> </tr> </tbody> </table></div><div class="col-md-12" style="margin-top: 30px; "><div></div></div></div></div>';
        addInCarousel(estrela);
        o('configuracao').dataset.estrela++;
    } else {
        o(id_estrela_edita).innerHTML = o('texto_estrela').innerHTML;
    }
    $("#estrelas").modal('hide');
    o('botao_estrela').value = "Adicionar";
}

function EditarEstrela(evt) {
    o('botao_estrela').value = "Atualizar";

    o('texto_estrela').innerHTML = evt.innerHTML;
    o('texto_estrela_text').value = evt.innerHTML;
    $('#estrelas').modal('show');
    id_estrela_edita = evt.id;
}

//COMENTARIO 
var id_comentario_edita = "";
function AdicionarComentario(evt) {
    if (evt.value == "Adicionar") {
        var comentario = '<div class="item col-md-12"  ><div style=" margin-left: 30%; " class="col-md-5"><div class="col-md-12" style=" margin-bottom: 16px; "><h3 data-id="" data-tipo_pesquisa="COMENTARIO" class="text-center" style="cursor:pointer" onclick="EditarComentario(this)"  id="comentarios_' + o('configuracao').dataset.comentario + '">' + o('texto_comentario').innerHTML + '</h3></div><div> <textarea class="form-control" rows="5" ></textarea></div><div class="col-md-12" style="margin-top: 30px; "><div></div></div></div></div>';
        addInCarousel(comentario);
        o('configuracao').dataset.comentario++;
    } else {
        o(id_comentario_edita).innerHTML = o('texto_comentario').innerHTML;
    }
    $("#comentarios").modal('hide');
    o('botao_comentario').value = "Adicionar";
}

function EditarComentario(evt) {
    o('botao_comentario').value = "Atualizar";
    o('texto_comentario').innerHTML = evt.innerHTML;
    o('texto_comentario_text').value = evt.innerHTML;
    $('#comentarios').modal('show');
    id_comentario_edita = evt.id;
}


//GOSTEI 
var id_comentario_gostei = "";
function AdicionarGostei(evt) {
    if (evt.value == "Adicionar") {
        var gostei = '<div class="item col-md-12"><div style=" margin-left: 30%; " class="col-md-5"><div class="col-md-12" style=" margin-bottom: 16px; "><h3 data-id="" data-tipo_pesquisa="GOSTEI" class="text-center"  style="cursor:pointer" onclick="EditarGostei(this)"  id="gostei_' + o('configuracao').dataset.gostei + '">' + o('texto_gostei').innerHTML + '</h3></div><div style=" margin-left: 12%;"> <div class="col-md-12"> <div class="col-md-12"> <h3 class="text-center" id="texto_gostei"></h3> </div> <div style=" margin-left: 15%; "> <table> <tr> <td width="100"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_like_cinza.png" width="90" /> </td> <td width="100"> <img src="/Content/Buffet/Pesquisa/Imagens/icone_dislike_cinza.png" width="90" /> </td> </tr> </table> </div> </div></div><div class="col-md-12" style="margin-top: 30px; "><div></div></div></div></div>';
        addInCarousel(gostei);
        o('configuracao').dataset.gostei++;
    } else {
        o(id_comentario_gostei).innerHTML = o('texto_gostei').innerHTML;
    }
    $("#gostei").modal('hide');
    o('botao_gostei').value = "Adicionar";
}

function EditarGostei(evt) {
    o('botao_gostei').value = "Atualizar";
    o('texto_gostei').innerHTML = evt.innerHTML;
    o('texto_gostei_text').value = evt.innerHTML;
    $('#gostei').modal('show');
    id_comentario_gostei = evt.id;
}

function addInCarousel(item) {
    $("#owl-demo").data('owlCarousel').addItem('<div class="item" data-merge="3">' + item + '</div>');
    UIShow('remove_item');
    o('configuracao').dataset.item_carrossel++;
}

function RemoveItem(evt) {
    $("#owl-demo").data('owlCarousel').removeItem();
    o('configuracao').dataset.item_carrossel != 0 ? o('configuracao').dataset.item_carrossel-- : null;
    if (o('configuracao').dataset.item_carrossel == 0) {
        UIHide('remove_item');
    }
}

function proximo(evt) {
    $("#owl-demo").trigger('owl.next');
}

function anterior(evt) {
    $("#owl-demo").trigger('owl.prev');
}


function SalvarNovaPesquisa() {
    ShowMensagem("Aguarde", "Salvando Pesquisa");
    SalvarSorriso();
    SalvarGostei();
    SalvarEstrela();
    SalvarComentarios();
    ApagarOpcoes();
    SalvarSatisfacao();
    window.location.href = '/Pesquisa/Pesquisa';
}

function SalvarGostei() {
    for (var i = 0; i < o('configuracao').dataset.gostei; i++) {
        if (o('gostei_' + i) != null) {
            SalvarPergunta(o('gostei_' + i));
        }
    }
}

function SalvarEstrela() {
    for (var i = 0; i < o('configuracao').dataset.estrela; i++) {
        if (o('estrela_' + i) != null) {
            SalvarPergunta(o('estrela_' + i));
        }
    }
}

function SalvarComentarios() {
    for (var i = 0; i < o('configuracao').dataset.comentario; i++) {
        if (o('comentarios_' + i) != null) {
            SalvarPergunta(o('comentarios_' + i));
        }
    }
}

function SalvarSorriso() {
    for (var i = 0; i < o('configuracao').dataset.sorriso; i++) {
        if (o('sorriso_' + i) != null) {
            SalvarPergunta(o('sorriso_' + i));
        }
    }
}

function ApagarOpcoes() {
    for (var i = 0; i < all_opcoes_apagadas.length; i++) {
        $.getJSON('/Pesquisa/ApagarOpcao', { id_opcao: all_opcoes_apagadas[i] }, function (data) {
            if (data.isOk) {

            }
        }, 'json');
    }
}

function SalvarSatisfacao() {
    for (var i = 0; i < all_satisfacao.length; i++) {
        var titulo_satisfacao = all_satisfacao[i];

        $.ajax({
            url: '/Pesquisa/SalvarPergunta?',
            dataType: 'json',
            async: false,
            data: {
                id_pesquisa: o('configuracao').dataset.id_pesquisa,
                titulo: all_satisfacao[i],
                tipo_pergunta: 'SATISFACAO',
                id: o(titulo_satisfacao + '_editando_satisfacao') != null ? o(titulo_satisfacao + '_editando_satisfacao').value : ""
            },
            success: function (data) {
                if (data.isOk) {
                    for (var i = 0; i < 20; i++) {
                        titulo_satisfacao = titulo_satisfacao.replace(' ', '');
                    }
                    for (var y = 0; y < 10 /*o('configuracao').dataset.opcoes*/; y++) {
                        if (o(titulo_satisfacao + '_avaliacao_' + y) != null) {
                            $.ajax({
                                url: '/Pesquisa/SalvarOpcao',
                                dataType: 'json',
                                async: false,
                                data: {
                                    id_pergunta: data.id,
                                    titulo: o(titulo_satisfacao + '_avaliacao_' + y).dataset.titulo_opcao,
                                    id: o(titulo_satisfacao + '_avaliacao_' + y).dataset.titulo_opcao_id
                                }, success: function (resposta) {

                                }
                            });
                        }
                    }
                }
            }
        });
    }
}

function SalvarPergunta(evt) {
    $.ajax({
        url: '/Pesquisa/SalvarPergunta',
        dataType: 'json',
        async: false,
        data: {
            id_pesquisa: o('configuracao').dataset.id_pesquisa,
            titulo: evt.innerHTML,
            tipo_pergunta: evt.dataset.tipo_pesquisa,
            id: evt.dataset.id
        },
        success: function (data) {
            if (data.isOk) {

            }
        }
    });
}
