﻿function VerificaBandeira(evt) {
    if (evt.value.length >= 6) {
        DigitoVerificador(evt);
    }
}

function BackSpaceCartao(evt) { 
    var key = event.keyCode || event.charCode;
    if ((key == 8 || key == 46) && evt.value.length < 6) {
         LimpaDados();
    }
}

function LimpaDados() {
    o('NumeroCartao').setAttribute("class", "form-control input-lg");
    UIHide('MensagemCartaoCredito');
    o('Bandeira').value = "";
}

function SelecionarBandeira(evt, bandeira) {
    evt.setAttribute("class", 'text-cartao-' + bandeira + ' form-control input-lg ');
    o('Bandeira').value = bandeira;
}

function Start(evt, Values, icon) {
    var evtV = evt.value.replace("-", "");
    for (var i = 0; i < Values.length; i++) {
        if (DIstartWith(evt.value.replace("-", ""), Values[i])) {
            SelecionarBandeira(evt, icon);
            return true;
        }
    }
    return false;
}

function DigitoVerificador(evt) {
    UIHide('MensagemCartaoCredito');
    if (Start(evt, ["636368", "438935", "504175", "451416", "636297", "5067", "4011", "4576"], "elo"))
        return
    var Discover = ["300", "3059", "3095", "3528", "3589", "3600", "3699",
                    "3800", "3999", "6011", "6221", "6229", "6240", "6269",
                    "6282", "6288", "6440", "6599"];
    if (Start(evt, Discover, "discover"))
        return

    var Diners = ["301", "305", "36", "38"];
    if (Start(evt, Diners, "diners"))
        return;

    var American = ["34", "37"];
    if (Start(evt, American, "american"))
        return;

    if (Start(evt, ["50"], "aura"))
        return;

    if (Start(evt, ["4"], "visa"))
        return;

    if (Start(evt, ["5"], "master"))
        return;

    LimpaDados();

    o('MensagemCartaoCredito').innerHTML = "Este número de cartão não é valído!";
    UIShow('MensagemCartaoCredito');
}



























