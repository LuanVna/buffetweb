﻿function AlteraValorPlano(evt, id_plano) {
    var plano = evt.options[evt.selectedIndex];
    var valor_before = plano.dataset.valorBefore;
    var valor_after = plano.dataset.valorAfter;
    if (plano.value == "MENSAL")
        o('valor_mensal_' + id_plano).innerHTML = '<span class="plan-price-old"> - </span>';
    else
        o('valor_mensal_' + id_plano).innerHTML = 'de R$ <strong>' + o('valor_mensal_' + id_plano).dataset.valor + '</strong> por';

    $({ numberValue: 0 }).animate({ numberValue: o('valor_atual_before_' + id_plano) }, {
        duration: 2500,
        easing: 'linear',
        step: function () {
            o('valor_atual_before_' + id_plano).innerHTML = valor_before;
        }
    });
    $({ numberValue: 0 }).animate({ numberValue: o('valor_atual_after_' + id_plano) }, {
        duration: 2500,
        easing: 'linear',
        step: function () {
            o('valor_atual_after_' + id_plano).innerHTML = "," + valor_after + " *";
        }
    });
}


function VerificaDisponibilidade(evt) {
    var local = evt.options[evt.selectedIndex].dataset;
    if (local.participante == "True") {
        o('disponibilidade').value = "Remover da Promoção";
        UIShow('mensagemPromocao');
    } else {
        o('disponibilidade').value = "Participar";
        UIHide('mensagemPromocao');
    }
}

function SelecionarFormaDePagamento(evt, uncheck) {
    o('check_' + uncheck).innerHTML = '<img src="/Content/Buffet/Imagens/Base/icone_uncheck.png" width="30">';
    o('check_' + uncheck).dataset.check = false;

    if (o('check_boleto').dataset.check == "false" && o('check_credito').dataset.check == "false") {
        o('EfetuarPagamento').style.display = "none";
    } else {
        o('EfetuarPagamento').style.display = "block";
        if (o('check_boleto').dataset.check == "true") {
            o('EfetuarPagamento').innerHTML = "Gerar Boleto";
        } else {
            o('EfetuarPagamento').innerHTML = "Efetuar Pagamento";
        }
    }
}

function ValidarDados() {
    if (v('NomeTitular') == "" ||
        v('CPJouCNPJ') == "" ||
        v('NumeroCartao') == "" ||
        v('MesValidade') == "" ||
        v('AnoValidade') == "" ||
        v('CodigoSeguranca') == "") {
        o('MensagemCartaoCredito').innerHTML = "Informe todos os dados";
        UIShow('MensagemCartaoCredito');
        return false;
    }
    if (v('CPJouCNPJ').length == 14 && !isCPF(v('CPJouCNPJ'))) {
        o('MensagemCartaoCredito').innerHTML = "CPF Inválido";
        UIShow('MensagemCartaoCredito');
        return false;
    }
    if (v('CPJouCNPJ').length > 14 && v('CPJouCNPJ').length < 18) {
        o('MensagemCartaoCredito').innerHTML = "CNPJ Inválido";
        UIShow('MensagemCartaoCredito');
        return false;
    }
    if (v('CodigoSeguranca').length < 3) {
        o('MensagemCartaoCredito').innerHTML = "Código de segurança Inválido";
        UIShow('MensagemCartaoCredito');
        return false;
    }
    UIHide('MensagemCartaoCredito');
    return true;
}

function EfetuarPagamentoCartao(evt) {
    if (ValidarDados()) {
        var data = new FormData();
        data.append('NomeTitular', v('NomeTitular'));
        data.append('CPJouCNPJ', v('CPJouCNPJ'));
        data.append('NumeroCartao', v('NumeroCartao'));
        data.append('MesValidade', v('MesValidade'));
        data.append('AnoValidade', v('AnoValidade'));
        data.append('Bandeira', v('Bandeira'));
        data.append('CodigoSeguranca', v('CodigoSeguranca'));
        data.append('renovar_automaticamente', v('renovar_automaticamente'));

        evt.value = "Aguarde, validando informações";
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/Planos/EfetuarPagamento', true);
        xhr.onload = function () {
            var data = JSON.parse(this.response);
            if (data.isOk)
                location.href = JSON.parse(this.response).redirect;
            else {
                o('MensagemCartaoCredito').innerHTML = data.mensagem;
                UISHow('MensagemCartaoCredito');
            }
        };
        xhr.send(data);
    }
}

function removeItemCompleted(results) {

}

function removeItemFailed(request, status, error) {

}

$(document).ready(function () {
    VefificaCupomDesconto();
});

function VefificaCupomDesconto() {
    setTimeout(function () {
        $.getJSON('/Planos/VefificaCupomDesconto', { quantidade: o('valores_carrinho').dataset.quantidadecupons }, function (data) {
            if (data.isOk && data.cupons.length > 0) {
                var desconto = 0;
                $("#tabela_cupons").find("tr:gt(0)").remove();
                for (var i = 0; i < data.cupons.length; i++) {
                    $('#tabela_cupons').append(AdicionarCupom(data.cupons[i]));
                    desconto += parseFloat(data.cupons[i].valor_desconto);
                }

                o('ValorTotalDesconto').innerHTML = "R$ " + (parseFloat(o('valores_carrinho').dataset.valortotal) - parseFloat(desconto)).toFixed(2);
                UIShow('Cupons_disponiveis');
                UIShow('DescricaoDesconto');

                o('valores_carrinho').dataset.quantidadecupons = data.cupons.length;
            }
            VefificaCupomDesconto();
        }, '');
    }, 10000);
}

function AdicionarCupom(cupom) {
    return '<tr style=" border: 0px; "><td width="35">' +
                '<img src="/Content/Buffet/Imagens/Base/' + (cupom.disponivel ? "icone_check" : "icone_ajuda") + '.png">' +
                 '</td>' +
                 '<td>' + cupom.para_plano + '</td>' +
                 '<td>' + cupom.periodo + '</td>' +
                 '<td>' + cupom.filial + '</td>' +
                 '<td>' + cupom.descricao + '</td>' +
                 '<td align="right"> R$ ' + parseFloat(cupom.valor_desconto).toFixed(2) + '</td>' +
            '</tr>';
}