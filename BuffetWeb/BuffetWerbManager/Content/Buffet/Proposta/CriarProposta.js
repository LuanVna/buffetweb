﻿//Adicionar Menu
function AdicionarCard(evt, tab, coluna) {
    for (var item = 0; item < 5; item++) {
        var compornent = o('tab_' + tab + '_coluna_' + coluna + '_item_' + item);
        if (compornent.style.display == "none") {
            var beforeComponent = o('tab_' + tab + '_coluna_' + coluna + '_item_' + (item - 1));
            if (beforeComponent != null) {
                var titulo = o('tab_' + tab + '_coluna_' + coluna + '_titulo_' + (item - 1)).value;
                var descricao = o('tab_' + tab + '_coluna_' + coluna + '_descricao_' + (item - 1)).value;

                if (titulo != "" && descricao != "") {
                    compornent.style.display = "block"
                    AtualizarStatusCard(this, compornent.dataset.id_aba, compornent.dataset.id_descricao, compornent.dataset.coluna, true);
                }
            } else {
                compornent.style.display = "block"
                AtualizarStatusCard(this, compornent.dataset.id_aba, compornent.dataset.id_descricao, compornent.dataset.coluna, true);
            }
            break;
        }
    }
}

function RemoverCard(evt, tab, coluna) {
    for (var item = 4; item >= 0; item--) {
        var compornent = o('tab_' + tab + '_coluna_' + coluna + '_item_' + item);
        if (compornent.style.display == "block") {
            compornent.style.display = "none"
            AtualizarStatusCard(this, compornent.dataset.id_aba, compornent.dataset.id_descricao, compornent.dataset.coluna, false);
            break;
        }
    }
}

function ShowNewTab(evt, tipo, id_aba) {
    if (evt.id == "main_tab_new" || evt.className == "active") {
        if (tipo == "Atualizar") {

            o('nome_aba').value = o('aba_hidden_' + id_aba).dataset.titulo;
            o('NovaAba').dataset.id_tab = o('aba_hidden_' + id_aba).dataset.id_aba;
            o('NovaAba').dataset.index = id_aba; 
        } else {

            for (var i = 0; i < 5; i++) {
                if (o('aba_' + i).style.display == "none") {
                    o('NovaAba').dataset.id_tab = o('aba_hidden_' + i).dataset.id_aba;
                    o('NovaAba').dataset.index = o('aba_hidden_' + i).dataset.index;
                    break;
                }
            }
        }
        //else {
        //    o('aba_hidden_' + id_aba).dataset.titulo = o('nome_aba').value;
        //    o('nome_aba').value = "";
        //}

        o('AddNewTabButton').value = tipo;
        if (!remove)
            $('#NovaAba').modal('show');

        remove = false;
    }
}

function AddNewTab(e) {
    if (v('nome_aba') != "") {
        if (o('AddNewTabButton').value == "Adicionar") {
            var allblock = 0;
            for (var i = 0; i < 5; i++) {
                if (o('aba_' + i).style.display == "none") {
                    o('aba_' + i).style.display = "block"

                    o('aba_descricao_' + o('NovaAba').dataset.index).innerHTML = v('nome_aba');
                    o('aba_hidden_' + o('NovaAba').dataset.index).dataset.titulo = v('nome_aba');
                    break;
                }
                allblock++;
            }

        } else {
            o('aba_descricao_' + o('NovaAba').dataset.index).innerHTML = v('nome_aba');
        }

        $.getJSON('/Proposta/AtualizarAba', {
            id_aba: o('NovaAba').dataset.id_tab,
            titulo: v('nome_aba'),
            disponivel: true
        }, function (data) {
            if (allblock == 4)
                o('main_tab').style.display = "none";
        });

        $('#NovaAba').modal('hide');
    }
}

var remove = false;
function ConfirmRemove(evt, id_aba) {
    remove = true;
    o('remove_description').innerHTML = "Deseja remover a aba " + o('aba_hidden_' + id_aba).dataset.titulo + "?";
    o('remover_tab').dataset.id_tab = id_aba;
    $('#RemoverAba').modal('show');
}

function RemoveTab(e) {

    $.getJSON('/Proposta/AtualizarAba', {
        id_aba: o('aba_hidden_' + e.dataset.id_tab).dataset.id_aba,
        disponivel: false
    }, function (data) {
        if (data.isOk) {
            var allblock = 4;
            for (var i = 4; i > 0; i--) {
                if (o('aba_' + i).style.display == "none") {
                    break;
                }
                allblock--;
            }

            if (allblock == 0)
                o('main_tab').style.display = "block";

            o('aba_' + e.dataset.id_tab).style.display = "none";
            $('#RemoverAba').modal('hide');
        }
    });
}

function AtualizarCard(evt, id_aba, id_descricao, tipo) {
    $.getJSON('/Proposta/AtualizarCard', {
        id_aba: id_aba,
        id_descricao: id_descricao,
        titulo: (tipo == "titulo" ? evt.value : null),
        descricao: (tipo == "descricao" ? evt.value : null)
    }, function (data) {
    });
}


function AtualizarStatusCard(evt, id_aba, id_descricao, coluna, disponivel) {
    $.getJSON('/Proposta/AtualizarStatusCard', {
        id_aba: id_aba,
        id_descricao: id_descricao,
        coluna: coluna,
        disponivel: disponivel,
    }, function (data) {
    });
}

function TransferirValor(evt, id_recebe) {
    o(id_recebe).value = evt.value;
}

function SelecionarDias(evt, linha_valor, dia_semana, linha, id_pacote, id_valores) {
    $.getJSON('/Proposta/AlteraDiaSemana', {
        id_valores: id_valores,
        dia_semana: dia_semana,
        valor: o(linha_valor + '_valor_' + dia_semana + '_semana_' + linha + '_linha_' + id_pacote + '_id_pacote').value,
        id_pacote: id_pacote
    }, function (data) {
    }, 'json');
    for (var i = 0; i < 2; i++) {
        o(linha_valor + '_valor_' + dia_semana + '_semana_' + i + '_linha_' + id_pacote + '_id_pacote').value = false;
        o('check_' + linha_valor + '_valor_' + dia_semana + '_semana_' + i + '_linha_' + id_pacote + '_id_pacote').innerHTML = '<img src="/Content/Buffet/Imagens/Base/icone_uncheck.png" width="30">';
    }
    o(linha_valor + '_valor_' + dia_semana + '_semana_' + linha + '_linha_' + id_pacote + '_id_pacote').value = true;
    o('check_' + linha_valor + '_valor_' + dia_semana + '_semana_' + linha + '_linha_' + id_pacote + '_id_pacote').innerHTML = '<img src="/Content/Buffet/Imagens/Base/icone_check.png" width="30">';
}

function SalvaValorPacote(evt, id_valores) {
    $.getJSON('/Proposta/SalvaValorValores', {
        id_valores: id_valores,
        valor_almoco: o(id_valores + '_valor_almoco').value,
        valor_jantar: o(id_valores + '_valor_jantar').value,
        valor_adicional: o(id_valores + '_valor_adicional').value,
    }, function (data) {

    }, 'json');
}


function AtivatOuInativarPacote(evt, id_pacote, pacote) {
    $.getJSON('/Proposta/AtivarPacote', {
        id_pacote: id_pacote,
        status: evt.value
    }, function (data) {
        if (data.isOk) {
            if (evt.value == "true") {
                o('pacote_' + pacote).innerHTML = '<img src="/Content/Buffet/Proposta/Imagens/' + pacote + '_ativo.png">';
            } else {
                o('pacote_' + pacote).innerHTML = '<img src="/Content/Buffet/Proposta/Imagens/' + pacote + '.png">';
            }
        }
    }, 'json');
}