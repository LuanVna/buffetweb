﻿var nome_proposta = "";
function OnAlteraProposta(evt) {
    nome_proposta = evt.options[evt.selectedIndex].dataset.nome_proposta;
}

function EnviarOrcamentoSimplificado(evt, id_cliente, tipo_envio, proposta_id, data, convidados, tipo, periodo) {
    if (o('autenticacao_proposta').value == "" && tipo === undefined) {
        o('alert_proposta_selecionada').innerHTML = "Selecione uma proposta!";
        UIShow('alert_proposta_selecionada');
        return;
    }
    UIHide('alert_proposta_selecionada');

    evt.innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_' + tipo_envio + '_load.gif" style="width:40px" />';
    $.getJSON('/Proposta/EnviarDadosSimplificados',
        {
            id_cliente: id_cliente,
            tipo_envio: tipo_envio,
            periodo: tipo === undefined ? v('periodo_' + id_cliente) : periodo,
            autenticacao_proposta: tipo === undefined ? v('autenticacao_proposta') : proposta_id,
            data: tipo === undefined ? v('data_' + id_cliente) : data,
            n_convidados: tipo === undefined ? v('n_convidados_' + id_cliente) : convidados
        },
        function (data) {
            if (data.isOk) {
                evt.innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_' + tipo_envio + '_send.png" style="width:40px" />';
                if (tipo != null || data.existe) {
                    o('disponivel_' + data.proposta).innerHTML = '<label class="label label-success">Por ' + data.dias + ' dias</label>';
                } else {
                    $('#tabela_propostas_cliente_' + id_cliente).append(linhaProposta(v('autenticacao_proposta'),
                                                                                      v('data_' + id_cliente),
                                                                                      v('n_convidados_' + id_cliente),
                                                                                      v('periodo_' + id_cliente), id_cliente, nome_proposta, data.dias));
                    UIShow('tabela_propostas_cliente_' + id_cliente);
                    UIHide('h2_propostas_cliente_' + id_cliente);
                }
            } else {
                evt.innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_' + tipo_envio + '_error.png" style="width:40px" />';
            }
        }, 'json');
}

function linhaProposta(autenticacao_proposta, data, n_convidados, periodo, id_cliente, proposta, dias) {
    return '<tr>' +
             '<td>' + proposta + '</td>' +
             '<td>' + (periodo == "" ? "Não informado" : periodo) + '</td>' +
             '<td>Administrador</td>' +
             '<td>' + (data == "" ? "Não informado" : data) + '</td>' +
             '<td>' + (n_convidados == "" ? "Não informado" : n_convidados) + '</td>' +
             '<td id="' + autenticacao_proposta + '">' +
                '<label class="label label-success">Por ' + dias + ' </label>' +
             '</td>' +
             '<td>' +
             '<a id="email_' + id_cliente + '" href="javascript:void(o)" onclick="EnviarOrcamentoSimplificado(this, 343,  \'email\', \'' + autenticacao_proposta + '\',\'' + (data == "" ? 'Não definido' : data) + '\', \'' + (n_convidados == "" ? 'Não definido' : n_convidados) + '\',\'reenviar\', \'periodo\')">' +
                 '<img width="40" src="/Content/Buffet/Orcamento/Imagens/icone_email_send.png ">' +
                 '</a>' +
                 '</td>' +
             '<td>' +
             '<a id="sms_' + id_cliente + '" href="javascript:void(o)" onclick="EnviarOrcamentoSimplificado(this, 343,  \'sms\', \'' + autenticacao_proposta + '\',\'' + (data == "" ? 'Não definido' : data) + '\', \'' + (n_convidados == "" ? 'Não definido' : n_convidados) + '\',\'reenviar\', \'periodo\')">' +
                 '<img width="40" src="/Content/Buffet/Orcamento/Imagens/icone_sms.png">' +
             '</a>' +
             '</td>' +
        '</tr>';
}

function AtualizarSimplificados(evt, id_cliente) {
    //if (o('status_' + id_cliente).value == "Proposta") {
    //    if (isEmail(v('cliente_email_' + id_cliente))) {
    //        if (o('email_' + id_cliente).innerHTML.trim() == "") {
    //            o('email_' + id_cliente).innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_email.png" style="width:40px" />';
    //        }
    //        o('email_' + id_cliente).style.display = "block";
    //    } else {
    //        o('email_' + id_cliente).style.display = "none";
    //    }
    //    if (o('cliente_celular_' + id_cliente).value.length >= 12) {
    //        if (o('sms_' + id_cliente).innerHTML.trim() == "") {
    //            o('sms_' + id_cliente).innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_sms.png" style="width:40px" />';
    //        }
    //        o('sms_' + id_cliente).style.display = "block";
    //    } else {
    //        o('sms_' + id_cliente).style.display = "none";
    //    }
    //} else {
    //    o('sms_' + id_cliente).style.display = "none";
    //    o('email_' + id_cliente).style.display = "none";
    //}

    $.getJSON('/Proposta/AtualizarSimplificados',
        {
            data: v('data_' + id_cliente),
            n_convidados: v('n_convidados_' + id_cliente),
            periodo: v('perido_' + id_cliente),
            autenticacao_proposta: o('autenticacao_proposta').value,
            id_cliente: id_cliente
        },
        function (data) {

        }, 'json');
}

function AtualizaClienteSimplificados(evt, id_cliente) {
    UIHide('alert_proposta');
    var Email = isEmail(v('cliente_email_' + id_cliente));
    var Celular = o('cliente_celular_' + id_cliente).value.length >= 12;
    if (Email || Celular) {
        $.getJSON('/Proposta/AtualizaClienteSimplificados',
        {
            email: v('cliente_email_' + id_cliente),
            isEmail: Email,
            celular: v('cliente_celular_' + id_cliente),
            id_cliente: id_cliente
        },
        function (data) {
            if (data.isOk) {
                if (Email) {
                    if (o('email_' + id_cliente).innerHTML.trim() == "") {
                        o('email_' + id_cliente).innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_email.png" style="width:40px" />';
                    }
                    o('email_' + id_cliente).style.display = "block";
                } else {
                    o('email_' + id_cliente).style.display = "none";
                }

                if (Celular) {
                    if (o('sms_' + id_cliente).innerHTML.trim() == "") {
                        o('sms_' + id_cliente).innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_sms.png" style="width:40px" />';
                    }
                    o('sms_' + id_cliente).style.display = "block";
                } else {
                    o('sms_' + id_cliente).style.display = "none";
                }
            } else {
                UIShowWithText('alert_proposta', data.mensagem);
            }
        }, 'json');
    }
}

function CalculaProposta() {
    if (o('convidados_inicial').value != "") {
        if (v('valor_total') != "") {
            o('valor_pessoa').value = (v('valor_total') / v('convidados_inicial'));//.toFixed(2);
        }
        if (v('valor_pessoa') != "") {
            o('valor_total').value = (v('valor_pessoa') * v('convidados_inicial'));//.toFixed(2);
        }
    }
}