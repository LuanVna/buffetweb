﻿function PagamentoCielo(evt, ativo) {
    o('cielo').value = ativo
    if (ativo) {
        o('pagamentoCielo').style.display = 'block'; 
    } else {
        o('pagamentoCielo').style.display = 'none';
    }
}

function PagamentoBrasil(evt, ativo) {
    o('brasil').value = ativo
    if (ativo) {
        o('pagamentoBrasil').style.display = 'block';
    } else {
        o('pagamentoBrasil').style.display = 'none';
    }
}

function PagamentoBoleto(evt, ativo) {
    o('boleto').value = ativo
    if (ativo) {
        o('pagamentoBoleto').style.display = 'block';
    } else {
        o('pagamentoBoleto').style.display = 'none';
    }
}

