﻿function MostrarValor(evt, checked) {
    o('mostrar_valor').value = checked;
    o('mostrar_valor_desc').innerHTML = "Selecione (" + (checked == true ? " Sim " : " Não ") + ")"
}

function PacoteDeEpoca(evt, checked) {
    o('de_epoca').value = checked;
    o('de_epoca_desc').innerHTML = "Selecione (" + (checked == true ? " Sim " : " Não ") + ")"
    o('de_epoca_div').style.display = o('de_epoca_div').style.display == "none" ? "block" : "none"
    if (checked) {
        o('evento_de').required = true;
        o('evento_ate').required = true;
    } else {
        o('evento_de').required = false;
        o('evento_ate').required = false;
    }
}

function Editavel(evt, checked, id, editavel, max_itens) { 
    if (checked) {
        o(id).value = max_itens;
        //o(id).style.display = "block";
        //o(id).required = "required";
    } else {
        o(id).value = 0;
        //o(id).style.display = "none";
        //o(id).required = "";
    }
    o(editavel).value = checked;
    o((id + "_desc")).innerHTML = "Editavel? (" + (checked == true ? " Sim - " + max_itens + " " : " Não ") + ")"
}

function CalcularValor(evt, id_select, valor, valor_p) {

    o('id_select').value = evt.checked;
    var valor_total = o('valor_total').innerHTML;
    //var valor_pessoa = o('valor_pessoa').innerHTML;
    if (evt.checked) {
        o('valor_total').innerHTML = (parseFloat(valor_total) + parseFloat(valor)).toFixed(2);
        //o('valor_pessoa').innerHTML = (parseFloat(valor_pessoa) + parseFloat(valor_p)).toFixed(2);
    } else {
        o('valor_total').innerHTML = (parseFloat(valor_total) - parseFloat(valor)).toFixed(2);
        //o('valor_pessoa').innerHTML = (parseFloat(valor_pessoa) - parseFloat(valor_p)).toFixed(2);
    }
}

function Selecionar(evt, checked) {
    o(checked).value = evt.checked;
}

function Status(evt, checked) {
    o('status').value = checked;
    o('status_desc').innerHTML = checked ? "Ativo" : "Inativo";
}


function DetalheItem(evt, nome, rendimento, valor_compra) {
    o('imagem_item').src = "";
    o('nome_item').innerHTML = nome;
    o('valorvenda_item').innerHTML = valor_compra;
    o('rendimento_item').innerHTML = rendimento; 
    $('#itens_modal').modal('show');
}

function DetalheServico(evt, nome, descricao, valor_venda, valor_compra) {
    o('imagem_servico').src = "";
    o('nome_servico').innerHTML = nome;
    o('valorvenda_servico').innerHTML = valor_venda; 
    o('descricao_servico').innerHTML = descricao;
    if (o('valorcompra_servico') != null) {
        o('valorcompra_servico').innerHTML = valor_compra;
    }
    $('#servico_modal').modal('show');
}

function DetalheAluguel(evt, nome, descricao, valor_venda, valor_compra) {
    o('imagem_aluguel').src = "";
    o('nome_aluguel').innerHTML = nome;
    o('valorvenda_aluguel').innerHTML = valor_venda;
    o('descricao_aluguel').innerHTML = descricao;
    if (o('valorcompra_aluguel') != null) {
        o('valorcompra_aluguel').innerHTML = valor_compra;
    }
    $('#aluguel_modal').modal('show');
}

function DetalheDiversos(evt, nome, descricao, valor_venda, valor_compra) {
    o('imagem_diversos').src = "";
    o('nome_diversos').innerHTML = nome;
    o('valorvenda_diversos').innerHTML = valor_venda;
    o('descricao_diversos').innerHTML = descricao;
    if (o('valorcompra_diversos') != null) {
        o('valorcompra_diversos').innerHTML = valor_compra;
    }
    $('#diversos_modal').modal('show');
} 

function DetalheServico(evt, nome, descricao, valor_venda, valor_compra) {
    o('imagem_servico').src = "";
    o('nome_servico').innerHTML = nome;
    o('valorvenda_servico').innerHTML = valor_venda;
    o('descricao_servico').innerHTML = descricao;
    if (o('valorcompra_servico') != null) {
        o('valorcompra_servico').innerHTML = valor_compra;
    }
    $('#servico_modal').modal('show');
}

