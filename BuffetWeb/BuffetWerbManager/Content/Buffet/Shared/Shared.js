﻿function EnviarEmailPlataform(evt) {
    evt.value = "Aguarde...";
    if (v('plataform_titulo') != "" && v('plataform_mensagem') != "") { 
        $.getJSON('/Ajustes/EnviarEmailPlataforma', {
            email: v('plataform_email'),
            nome: v('plataform_nome'),
            mensagem: v('plataform_mensagem'),
            titulo: v('plataform_titulo')
        }, function (data) {
            if (data.isOk) {
                o('plataform_alert').innerHTML = data.mensagem;
                UIShow('plataform_alert');
            }
            evt.value = "Enviar E-mail";
        }, 'json');
    } else {
        o('plataform_alert').innerHTML = "Todos os campos são obrigatórios";
        UIShow('plataform_alert');
    }
}

function EnviarSMSPlataform(evt) {
    evt.value = "Aguarde...";
    if (v('plataform_titulo_sms') != "" && v('plataform_mensagem_sms') != "") {
        $.getJSON('/Ajustes/EnviarSMSPlataforma', {
            telefone: v('plataform_telefone_sms'),
            nome: v('plataform_nome_sms'),
            mensagem: v('plataform_mensagem_sms'),
            titulo: v('plataform_titulo_sms')
        }, function (data) {
            if (data.isOk) {
                o('plataform_alert_sms').innerHTML = data.mensagem;
                UIShow('plataform_alert_sms');
            }
            evt.value = "Enviar SMS";
        }, 'json');
    } else {
        o('plataform_alert_sms').innerHTML = "Todos os campos são obrigatórios";
        UIShow('plataform_alert_sms');
    }
}
