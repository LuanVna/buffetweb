﻿var index = 0;
function NovaOrdem(evt) {
    if (o('ordem_' + index).selectedIndex != 0) {
        index++;
        if (o('ordem_' + index) != null) {
            o('ordem_' + index).style.display = "block";
        }
    } else {
        alert("Preencha a ultima ordem antes de adicionar uma nova");
    }
}

function ApagaOrdem(evt) {
    if (index == 0) {
        return;
    }
    if (o('ordem_' + index) != null) {
        o('ordem_' + index).style.display = "none";
        index--;
    }
}

function buscarCEP(evt) {
    if (evt.value.length == 8) {
        $.getJSON("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: evt.value },
        function (result) {
            if (result.status != 1) {
                alert(result.message || "Houve um erro desconhecido");
                return;
            }
            $("input#cep").val(result.code);
            $("input#bairro").val(result.district);
            $("input#endereco").val(result.address);
            o('numero').focus();
        });
    }
}
