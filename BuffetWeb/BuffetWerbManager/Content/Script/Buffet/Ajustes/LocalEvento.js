﻿function LimparEditar(evt) {
    for (var i = 0; i < 12; i++) {
        if (o('imagem_' + i).src != "http://placehold.it/150x120/EFEFEF/EBBB5E") {
            o('imagem_' + i).src = "http://placehold.it/150x120/EFEFEF/EBBB5E";
            o('excluir_' + i).style.display = "none";
        }
    }
    o('id').value = "";
    o('ENDERECOid').value = "";
    o('autenticacao').value = "";

    o('form_local').reset();
    o('salvar').value = "Salvar";
    evt.style.display = "none";
    o('comentarios').style.display = "none";
}

function SelecionarLocal(evt, index, total) {
    for (var i = 0; i < total; i++) {
        UIHide('localEvento_' + i);
    }
    UIShow('localEvento_' + index);
}

function RemoverComentario(evt, id_comentario, autenticacao) {
    $.getJSON('/Portal/RemoverComentario', {
        id_comentario: id_comentario,
        autenticacao: autenticacao
    }, function (data) {
        if (data.isOk) {
            o('comentario_' + id_comentario).remove();
        }
    }, 'json');
}

function EditarLocalEvento(evt, id_local) {
    for (var i = 0; i < 12; i++) {
        if (o('imagem_' + i).src != "http://placehold.it/150x120/EFEFEF/EBBB5E") {
            o('imagem_' + i).src = "http://placehold.it/150x120/EFEFEF/EBBB5E";
            o('excluir_' + i).style.display = "none";
        }
    }

    $.getJSON('/Ajustes/EditarLocalEvento', { id_local: id_local }, function (resposta) {
        if (resposta.isOk) {
            o('cancelarEdicao').style.display = "block";
            o('salvar').value = "Atualizar";

            o('comentarios').style.display = "block";
            ListarComentarios(resposta.comentarios);

            var data = resposta.local;

            o('id').value = data.id;
            o('ENDERECOid').value = data.enderecoid;
            o('autenticacao').value = data.autenticacao;
            o('idade_minima_pagante').value = data.idade_minima_pagante;

            var i_almoco = data.horario_inicio_almoco;
            var f_almoco = data.horario_fim_almoco;
            var i_jantar = data.horario_inicio_jantar;
            var f_jantar = data.horario_fim_jantar;

            if (i_almoco != null) {
                o('horario_inicio_almoco').value = (i_almoco.Hours + ':' + i_almoco.Minutes);
                o('horario_fim_almoco').value = (f_almoco.Hours + ':' + f_almoco.Minutes);
                o('horario_inicio_jantar').value = (i_jantar.Hours + ':' + i_jantar.Minutes);
                o('horario_fim_jantar').value = (f_jantar.Hours + ':' + f_jantar.Minutes);
            } else {
                o('horario_inicio_almoco').value = "";
                o('horario_fim_almoco').value = "";
                o('horario_inicio_jantar').value = "";
                o('horario_fim_jantar').value = "";
            }

            o('nome').value = data.nome;
            //o('capacidade').value = data.capacidade;
            o('regiao').value = data.regiao;
            o('descricao').value = data.descricao;

            if (o('palavras_chaves') != null) {
                o('palavras_chaves').value = data.palavras_chaves;
            }

            //o('espaco_Desc').innerHTML = data.porEspaco ? "Espaço" : "Pessoas";
            //o('porEspaco').value = data.porEspaco;

            //o('valor_minimo_text').value = data.valorMinimo;
            //o('valor').value = data.valor;

            o('telefone').value = data.telefone;
            o('cep').value = data.cep;
            o('endereco').value = data.endereco1;
            o('bairro').value = data.bairro;
            o('ENDERECO.cidade').value = data.cidade;
            o('ENDERECO.estado').value = data.estado;
            o('ENDERECO.complemento').value = data.complemento;
            o('numero').value = data.numero;
            o('video').value = data.video;

            for (var i = 0; i < 12; i++) {
                if (resposta.imagens[i] != null) {
                    o('imagem_' + resposta.imagens[i].url).src = 'http://www.buffetweb.com/Sites/Arquivos/Empresas/' + resposta.empresa + '/LocalEvento/' + data.id + '_' + resposta.imagens[i].url + '.png';
                    o('excluir_' + resposta.imagens[i].url).style.display = "block";
                }
            }
        }
    }, 'json');
}

function ListarComentarios(comentarios) {
    //c.avaliacao,
    //                                                                c.comentario,
    //                                                                c.desde,
    //                                                                c.CLIENTE_PERFIL_PORTAL.url,
    //                                                                c.CLIENTE_PERFIL_PORTAL.nome,
    //                                                                c.CLIENTE_PERFIL_PORTAL.perfil
}


function ImagemSelecionada(evt, imagem) {
    o('excluir_' + imagem.replace('imagem_', '')).style.display = "block";
}

function RemoverImagem(evt) {
    $.getJSON('/Ajustes/ApagarImagem', { sequencia: evt.dataset.sequencia }, function (data) {
        o('imagem_' + evt.dataset.sequencia).src = "http://placehold.it/150x120/EFEFEF/EBBB5E";
        o('excluir_' + evt.dataset.sequencia).style.display = "none";
    }, "json");
}

function PerguntaRemover(evt, sequencia) {
    o('idImagem').dataset.sequencia = sequencia;
}

function BuscarCEP(evt) {
    if (evt.value.length == 8) {
        $.getJSON("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: evt.value },
        function (result) {
            if (result.status != 1) {
                alert(result.message || "Houve um erro desconhecido");
                return;
            }
            $("input#cep").val(result.code);
            $("input#bairro").val(result.district);
            $("input#endereco").val(result.address);
            o('numero').focus();
        });
    }
}



function CobrarPor(evt, isEspaco) {
    if (isEspaco) {
        o('desc_valor').innerHTML = "Valor Espaço";
        o('valor_minimo').style.display = "none";
        o('valor_espaco').style.display = "block";
        o('espaco_Desc').innerHTML = " Espaço "
    } else {
        o('desc_valor').innerHTML = "Valor Por Pessoa";
        o('valor_minimo').style.display = "block";
        o('valor_espaco').style.display = "block";
        o('espaco_Desc').innerHTML = " Pessoa "
    }
    o('porEspaco').value = isEspaco;
}

function SelecionaImagem(evt, draw_image) {
    var fReader = new FileReader();
    fReader.readAsDataURL(evt.files[0]);
    fReader.onloadend = function (evento) {
        o(draw_image).src = evento.target.result;
        o('img-lg').src = evento.target.result;
    }
}