﻿function SendContact(evt, path, icon, callBack) {
    evt.innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_' + icon + '_load.gif" style="width:40px" />';
    $.getJSON(path, function (data) {
        evt.innerHTML = '<img src="/Content/Buffet/Orcamento/Imagens/icone_' + icon + '_' + data.status + '.png" style="width:40px" />';
        if (callBack !== undefined)
            callBack(data.isOk);
    }, 'json');
}