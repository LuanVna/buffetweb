﻿function GetParametro(parametro) {
    parametro = parametro.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + parametro + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function v(obj) {
    console.log(obj);
    return document.getElementById(obj).value;
}

function vn(obj) {
    console.log(obj);
    document.getElementsByName(obj)[0].value;
}

function o(obj) {
    console.log(obj);
    return document.getElementById(obj);
}

function on(obj) {
    console.log(obj);
    return document.getElementsByName(obj)[0];
}

function DIBaseValue(obj) {
    console.log(obj);
    return v(obj);
}

function DIBaseObj(obj) {
    console.log(obj);
    return o(obj);
}

function DISubmit() {
    document.getElementById('form').submit();
}

function DISubmit(id) {
    document.getElementById(id).submit();
}

function DIBaseNextFocus(obj, to, length) {
    if (obj.value.length == length) {
        document.getElementById(to).focus();
    }
}

function DIBaseNextSubmit(obj, length, submit_id) {
    if (obj.value.length == length) {
        document.getElementById(submit_id).submit();
    }
}

function DIBaseNextFunction(obj, length, functionCallBack) {
    if (obj.value.length == length) {
        functionCallBack();
    }
}

function DIBaseIsEmptyAnd() {
    for (var i = 0; i < arguments.length; i++) {
        if (v(arguments[i]) == '') {
            return true;
        }
    }
    return false;
}

function DIMask(src, mask) {
    var i = src.value.length;
    var saida = mask.substring(0, 1);
    var texto = mask.substring(i)
    if (texto.substring(0, 1) != saida) {
        src.value += texto.substring(0, 1);
    }
}

function DIBaseIsEmptyOr() {
    var args = 0;
    for (var i = 0; i < arguments.length; i++) {
        if (v(arguments[i]) == '') {
            args++
        }
    }
    return args == arguments.length;
}

function DIBaseArguments() {
    var args = '/?&';
    for (var i = 0; i < arguments.length; i++) {
        args += arguments[i] + "=" + v(arguments[i]) + "&";
    }
    return args;
}

function DIBaseEmpty(obj) {
    return v(obj) == '';
}

function DIOnlyNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function DIstartWith(value, startWith) {
    var start = false;
    for (var i = 0; i < startWith.length; i++) {
        if (i < value.length) {
            if (value[i] == startWith[i]) {
                start = true;
            } else {
                start = false;
            }
            if (value[i] != startWith[i]) {
                return false;
            }
        } else {
            return false;
        }
    }
    return start;
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function setCookie(key, value) {
    document.cookie = key + "=" + value;
}


function arquivoSelecionado(evt, draw_image, callBack) {
    var fReader = new FileReader();
    fReader.readAsDataURL(evt.files[0]);
    fReader.onloadend = function (evento) {
        var imagem = document.getElementById(draw_image);
        imagem.src = evento.target.result;
        if (callBack != "") {
            callBack(evt, draw_image);
        }
    }
}

function isEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isCPF(strCPF) {
    strCPF = strCPF.replace('-', '').replace('.', '').replace('.', '').replace('.', '');

    var Soma; var Resto; Soma = 0; if (strCPF == "00000000000") return false;

    for (i = 1; i <= 9; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i); Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) Resto = 0; if (Resto != parseInt(strCPF.substring(9, 10)))
        return false; Soma = 0;
    for (i = 1; i <= 10; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i); Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) Resto = 0; if (Resto != parseInt(strCPF.substring(10, 11)))
        return false;
    return true;
}

function DIMaxValue(evt, maxValue) {
    if (maxValue != "") {
        if (evt.value > maxValue || evt.length > maxValue) {
            evt.value = maxValue;
        }
    }
}

function ConvertDate(data) {
    if (data != "" && data !== 'undefined') {
        var re = /-?\d+/;
        var m = re.exec(data);
        var d = new Date(parseInt(m[0]));
        var dia = d.getUTCDate().toString().length == 1 ? ("0" + d.getUTCDate()) : d.getUTCDate();
        var mes = (d.getMonth() + 1).toString().length == 1 ? ("0" + (d.getMonth() + 1)) : d.getMonth();

        return dia + "/" + mes + "/" + d.getFullYear();
    }
    return data;
}

function Contains(contains, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == contains) {
            return true;
        }
    }
}


function FiltrarInformacoes(evt, form_id) {
    if (evt.value != "") {
        o(form_id).submit();
    }
}

function ConvertTimeformat(str) {
    var time = str;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;

    //Creating the todays date in the right format
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;//January is 0!`
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    var todaysdate = dd + '/' + mm + '/' + yyyy + " "; //<--I added an extra space!
    var hoursNminutes = sHours + ":" + sMinutes
    //CREATE THE RIGHT FORMAT FOR DATE.PARSEXACT "dd/MM/yyyy HH:mm"
    var dateToParse = todaysdate + hoursNminutes
    return dateToParse;
}