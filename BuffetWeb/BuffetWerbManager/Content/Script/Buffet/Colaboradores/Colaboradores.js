﻿function atualizarColaborador(id, nome, telefone, id_perfil, valor_custo, valor_venda) {
    o('nome').value = nome;
    o('id').value = id;
    o('telefone').value = telefone;
    o('id_perfil').value = id_perfil;
    o('valor_compra').value = valor_custo;
    o('valor_venda').value = valor_venda;
}

function atualizarPerfil(id, nome, descricao) {
    o('id_perfil_').value = id;
    o('nome_perfil').value = nome;
    o('descricao').value = descricao;
}

function PerfilSelecionado(evt) {
    o('modal_button').dataset.target = "#" + evt.value;
}


function o(text) {
    return document.getElementById(text);
}