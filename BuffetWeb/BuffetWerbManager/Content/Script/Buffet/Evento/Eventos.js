﻿function EventoDeEpoca(evt, de_epoca) {
    o('de_epoca').value = de_epoca;
    if (de_epoca) {
        o('evento_de').style.display = 'block';
        o('evento_de').required = 'required';
        o('evento_ate').required = 'required';
        o('evento_ate').style.display = 'block';
        o('quantidade_disponivel').style.display = 'block';

        o('evento_de_label').style.display = 'block';
        o('evento_ate_label').style.display = 'block';
        o('quantidade_disponivel_label').style.display = 'block';
    } else {
        o('evento_de').style.display = 'none';
        o('evento_de').required = '';
        o('evento_ate').required = '';
        o('evento_ate').style.display = 'none';
        o('quantidade_disponivel').style.display = 'none';

        o('evento_de_label').style.display = 'none';
        o('evento_ate_label').style.display = 'none';
        o('quantidade_disponivel_label').style.display = 'none';
    }
    o('de_epoca_desc').innerHTML = de_epoca ? "De Época? ( Sim )" : "De Época? ( Não )";
}


function SalvarTipoEvento(evt) {
    ShowMensagem('Aguarde...', 'Salvando Novo Evento');
    $.ajax({
        url: '/Eventos/SalvarTipoEventoJSON?',
        dataType: 'json',
        async: true,
        data: $("#TIPOEVENTO").serialize(),
        success: function (data) {
            if (data.Item1) {
                UPLoadImage(data.item2);
                SalvarCamposDisponiveis(data.Item2);
                SalvarPacotesEvento(data.Item2);
                SalvarAluguel(evt.dataset.alugueis, data.Item2);
                SalvarDiversos(evt.dataset.diversos, data.Item2);
                SalvarServicos(evt.dataset.servicos, data.Item2);
                SalvarPrestadores(evt.dataset.prestador, data.Item2);
                ShowMensagemError('Sucesso', 'Evento Salvo com sucesso!');
                location.reload();
            } else {
                ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
            }
        }
    });
}

function UPLoadImage(id_item) {
    var data = new FormData();
    var files = $("#Image").get(0).files;
    if (files.length > 0) {
        data.append("Image", files[0]);
    }
    $.ajax({
        url: '/Estoque/SalvarImagemEventos?id_item=' + id_item,
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            console.log("Upload" + response)
        },
        error: function (er) {
            console.log("Upload Error")
        }
    });
}
 

function SalvarCamposDisponiveis(id_evento) {
    $("#dadosSelect option").each(function (i) {
        var id_campos_descricao = $(this).val();
        $.ajax({
            url: '/Eventos/SalvarCamposDisponiveisJSON',
            dataType: 'json',
            async: false,
            data: { id_tipo_evento: id_evento, id_campos_descricao: id_campos_descricao },
            success: function (data) {

            }
        });
    });
}

function SalvarPacotesEvento(id_evento) {
    $("#cardapiosSelect option").each(function (i) {
        var id_cardapio = $(this).val();
        $.ajax({
            url: '/Eventos/SalvarCardapioJSON',
            dataType: 'json',
            async: false,
            data: { id_evento: id_evento, id_pacote: id_cardapio },
            success: function (data) {

            }
        });
    });
}

function SalvarAluguel(alugueis, id_evento) {
    for (var i = 0; i < alugueis.split(',').length; i++) {
        if (alugueis.split(',')[i] != "") {
            var id_aluguel = alugueis.split(',')[i];
            var aluguel_brinde = v(alugueis.split(',')[i] + 'aluguel_brinde');
            var aluguel_custo_brinde = v(alugueis.split(',')[i] + 'aluguel_custo_brinde');
            var aluguel_selecionado = v(alugueis.split(',')[i] + 'aluguel_selecionado');
            if (aluguel_selecionado == "true") {
                $.ajax({
                    url: '/Eventos/SalvarAluguelJSON',
                    dataType: 'json',
                    async: false,
                    data: { id_itens_aluguel: id_aluguel, id_tipo_evento: id_evento, cobrar_brinde: aluguel_custo_brinde, brinde: aluguel_brinde },
                    success: function (data) {

                    }
                });
            }
        }
    }
}

function SalvarDiversos(diversos, id_evento) {
    for (var i = 0; i < diversos.split(',').length; i++) {
        if (diversos.split(',')[i] != "") {
            var id_diversos = diversos.split(',')[i];
            var diversos_brinde = v(diversos.split(',')[i] + 'diversos_brinde');
            var diversos_custo_brinde = v(diversos.split(',')[i] + 'diversos_custo_brinde');
            var diversos_selecionado = v(diversos.split(',')[i] + 'diversos_selecionado');
            if (diversos_selecionado == "true") {
                $.ajax({
                    url: '/Eventos/SalvarDiversosJSON',
                    dataType: 'json',
                    async: false,
                    data: { id_itens_diversos: id_diversos, id_tipo_evento: id_evento, cobrar_brinde: diversos_custo_brinde, brinde: diversos_brinde },
                    success: function (data) {

                    }
                });
            }
        }
    }
}

function SalvarServicos(servicos, id_evento) {
    for (var i = 0; i < servicos.split(',').length; i++) {
        if (servicos.split(',')[i] != "") {
            var id_servicos = servicos.split(',')[i];
            var servicos_brinde = v(servicos.split(',')[i] + 'servicos_brinde');
            var servicos_custo_brinde = v(servicos.split(',')[i] + 'servicos_custo_brinde');
            var servicos_selecionado = v(servicos.split(',')[i] + 'servicos_selecionado');
            if (servicos_selecionado == "true") {
                $.ajax({
                    url: '/Eventos/SalvarServicosJSON',
                    dataType: 'json',
                    async: false,
                    data: { id_itens_servicos: id_servicos, id_tipo_evento: id_evento, cobrar_brinde: servicos_custo_brinde, brinde: servicos_brinde },
                    success: function (data) {

                    }
                });
            }
        }
    }
}


function SalvarPrestadores(prestadores, id_evento) {
    for (var i = 0; i < prestadores.split(',').length; i++) {
        if (prestadores.split(',')[i] != "") {
            var id_servicos = prestadores.split(',')[i];
            var servicos_selecionado = v(prestadores.split(',')[i] + 'prestador_selecionado');
            var prestador_quantidade = v(prestadores.split(',')[i] + 'prestador_quantidade');
            var prestador_tipo = v(prestadores.split(',')[i] + 'prestador_tipo');
            if (servicos_selecionado == "true") {
                $.ajax({
                    url: '/Eventos/SalvarPrestadoresJSON',
                    dataType: 'json',
                    async: false,
                    data: { quantidade: prestador_quantidade, id_evento: id_evento, tipo: prestador_tipo },
                    success: function (data) {

                    }
                });
            }
        }
    }
}

function uploadImagem(id_evento) {
    var data = new FormData();
    var files = $("#uploadEditorImage").get(0).files;
    if (files.length > 0) {
        data.append("HelpSectionImages", files[0]);
    }
    $.ajax({
        url: resolveUrl("~/Admin/HelpSection/AddTextEditorImage/"),
        type: "POST",
        processData: false,
        contentType: false,
        data: data,
        success: function (response) {
            //code after success

        },
        error: function (er) {
            alert(er);
        }

    });
}