﻿function atualizarItensAluguel(id, nome, valor_custo, valor_venda, brinde, id_categoria, descricao) {
    o('id').value = id;
    o('nome').value = nome;
    o('valor_custo').value = valor_custo;
    o('valor_venda').value = valor_venda;
    o('brinde').value = brinde;
    o('descricao').value = descricao;
    o('id_categoria_aluguel').value = id_categoria;
    o('imagem_produto').value = imagem_produto;
}

function SalvarItemAlguel(evt) {
    ShowMensagem('Aguarde...', 'Salvando Item de Aluguel');
    $.getJSON('/Itens/SalvarItemAlguelJSON?' + $("#ITEMALUGUEL").serialize(), function (data) {
        if (data.Item1) {
            $("#fornecedoresSelect option").each(function (i) {
                var id_fornecedor = $(this).val();
                var jsonFornecedores = { id_fornecedor: id_fornecedor, id_aluguel: data.Item2 }
                $.getJSON('/Itens/SalvarFornecedoresItemAlguelJSON', jsonFornecedores, function (data) {

                }, 'json');
            });
            ShowMensagemError("Sucesso!", "Item de Aluguel Salvo com sucess!");
            o("ITEMALUGUEL").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

function SalvarCategoriaAluguel(evt) {
    ShowMensagem('Aguarde...', 'Salvando Categoria de Aluguel');
    $.getJSON('/Itens/SalvarCategoriaAluguel?' + $("#CATEGORIAALUGUEL").serialize(), function (data) {
        if (data.Item1) { 
            PreencheSelect('id_categoria', v('nome_categoria'), data.Item2);
            ShowMensagemError("Sucesso!", "Categoria de Aluguel Salva com sucess!");
            o("CATEGORIAALUGUEL").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}

