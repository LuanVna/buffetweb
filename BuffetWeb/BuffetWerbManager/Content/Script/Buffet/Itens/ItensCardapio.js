﻿function atualizarCardapio(id, nome, id_categoria, consumo_convidado, consumo_convidado_red, valor_compra, valor_venda, status, id_unidade_medida, descricao, disponivel_em, indexMax) {
    o('id').value = id;
    o('nome').value = nome;
    o('id_categoria').value = id_categoria;
    o('consumo_convidado').value = consumo_convidado;
    o('consumo_convidado_red').value = consumo_convidado_red;
    o('valor_compra').value = valor_compra;
    o('valor_venda').value = valor_venda;
    o('status').value = status;
    o('id_unidade_medida').value = id_unidade_medida;
    o('descricao').value = descricao;

    index = 0;
    var disponiveis = disponivel_em.split(" ");
    for (var disponivel in disponiveis) {
        if (disponivel != "0" && index != indexMax) {
            o('disponivel_em' + index).value = disponivel;
            o('disponivel_em' + index).style.display = "block";
            index++;
        }
    }
}

//function DisponivelEm(evt) {
//    var valor = v('disponivel_em');
//    if (o('disponivel_em').value.indexOf(valor) != -1) {
//        o('disponivel_em').value = valor + " " + evt.selectedIndex; 
//    } 
//}

var index = 0;
function NovaOrdem(evt) {
    var select = 'disponivel_em' + index;
    if (o(select).selectedIndex != 0) {
        index++;
        if (o('disponivel_em' + index) != null) {
            o('disponivel_em' + index).style.display = "block";
        }
    } else {
        alert("Preencha a ultima ordem antes de adicionar uma nova");
    }
}

function ApagaOrdem(evt) {
    if (index == 0) {
        return;
    }
    var select = 'disponivel_em' + index;
    if (o('disponivel_em' + index) != null) {
        o('disponivel_em' + index).style.display = "none";
        index--;
    }
}