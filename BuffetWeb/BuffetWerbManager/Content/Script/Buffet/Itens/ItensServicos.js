﻿function atualizarItensServico(id, nome, valor_custo, valor_venda, id_categoria,  descricao) {
    o('id').value = id;
    o('nome').value = nome;
    o('valor_custo').value = valor_custo;
    o('valor_venda').value = valor_venda;
    o('descricao').value = descricao;
    o('id_categoria').value = id_categoria;       
}

function SalvarItemServico(evt) {
    ShowMensagem('Aguarde...', 'Salvando Item de Serviço');
    $.getJSON('/Itens/SalvarItemServicoJSON?' + $("#ITEMSERVICO").serialize(), function (data) {
        if (data.Item1) {
            $("#prestadoresSelect option").each(function (i) {
                var id_prestador = $(this).val();
                var jsonFornecedores = { id_prestador_funcao: id_prestador, id_servico: data.Item2 }
                $.getJSON('/Itens/SalvarPrestadorItemServicoJSON', jsonFornecedores, function (data) {

                }, 'json');
            });
            ShowMensagemError("Sucesso!", "Item de Serviço Salvo com sucess!");
            o("ITEMSERVICO").reset();
        } else {
            ShowMensagemError("Atenção", "Verifique as Informações preenchidas!");
        }
    }, 'json');
}