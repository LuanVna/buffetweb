﻿var index = 0;
function NovaOrdem(evt) {
    if (o('ordem_' + index).selectedIndex != 0) {
        index++;
        if (o('ordem_' + index) != null) {
            o('ordem_' + index).style.display = "block";
        }
    } else {
        alert("Preencha a ultima ordem antes de adicionar uma nova");
    } 
}

function ApagaOrdem(evt) {
    if (index == 0) {
        return;
    }
    if (o('ordem_' + index) != null) {
        o('ordem_' + index).style.display = "none";
        index--;
    }
}

var index2 = 0;
function NovaDados(evt) {
    if (o('dados_' + index2).selectedIndex != 0) {
        index2++;
        if (o('dados_' + index2) != null) {
            o('dados_' + index2).style.display = "block";
        }
    } else {
        alert("Preencha o ultimo dado antes de adicionar uma nova");
    }
}

function ApagaDados(evt) {
    if (index2 == 0) {
        return;
    }
    if (o('dados_' + index2) != null) {
        o('dados_' + index2).style.display = "none";
        index2--;
    }
}