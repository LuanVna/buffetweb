﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;

using BuffetWebManager.Models.Visitas;
using System.Web.Routing;
using System.Data.Entity;
using BuffetWebManager.Models.Agenda;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class AgendaController : BaseController
    {
        private static List<ORCAMENTO_VISITAS> visitas;

        public ActionResult Visitas(string codigo_unico)
        {
            base.getMensagem();
            ViewBag.c_visitas = perfil.c_visitas;
            using (BuffetContext db = new BuffetContext())
            {
                Visitas visi = new Visitas() { id_empresa = base.ID_EMPRESA };
                ViewBag.codigo_unico = codigo_unico;
                if (codigo_unico != null)
                {
                    visi.orcamentos = db.ORCAMENTOes.Include(c => c.CLIENTE).Where(i => i.codigo_unico.Equals(codigo_unico) && i.id_empresa == ID_EMPRESA).ToList();
                    return View(visi);
                }
                else
                {
                    //DateTime now = DateTime.Now.Date;
                    //visi.orcamentos = db.ORCAMENTOes.Include(c => c.CLIENTE).Where(o => o.data_evento.Month == now.Month && o.data_evento.Year == now.Year && o.id_empresa == ID_EMPRESA).ToList();
                    //if (visitas == null)
                    //{
                    //    visi.visitas = db.ORCAMENTO_VISITAS.Include(o => o.ORCAMENTO).Include(l => l.LOCAL_EVENTO).Where(e => e.data.Month == now.Month && e.id_empresa == ID_EMPRESA).ToList();
                    //}
                    //else
                    //{
                    //    visi.visitas = visitas;
                    //    visitas = null;
                    //    return View(visi);
                    //}
                }
                return View(visi);
            }
        }

        public ActionResult SalvarNovaVisita(ORCAMENTO_VISITAS visita, string codigo_unico, string TIPO)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (TIPO.Equals("C"))
                {
                    visita.id_empresa = base.ID_EMPRESA;
                    db.ORCAMENTO_VISITAS.Add(visita);
                    db.SaveChanges();
                    if (codigo_unico != null)
                    {
                        return RedirectToAction("DetalhesOrcamento", "Orcamento", new RouteValueDictionary(new { codigo_unico = codigo_unico, tab = "visita" }));
                    }
                    else
                    {
                        base.setMensagem("Visita salva com sucesso!", tipoMensagem.tipoSucesso);
                    }
                }
                else if (TIPO.Equals("B"))
                {
                    visitas = db.ORCAMENTO_VISITAS.Include(o => o.ORCAMENTO).Include(l => l.LOCAL_EVENTO).Where(s => s.id_orcamento == visita.id_orcamento || s.status_visita == visita.status_visita || s.id_local_evento == visita.id_local_evento || s.tipo_visita == visita.tipo_visita).ToList();
                    base.setMensagem("Visita encontrada!", tipoMensagem.tipoInfo);
                }
            }
            return RedirectToAction("Visitas");
        }

        public ActionResult ProximosEventos(int? periodo)
        {
            return View();
            //return View(ModelAgenda.agendaOrcamento(periodo)); 
        } 

        public ActionResult Ajustes()
        {
            return View(new AgendaModel() { id_empresa = base.ID_EMPRESA });
        }
    }
}
