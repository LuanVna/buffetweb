﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebManager.Models;
using BuffetWebManager.Models.Ajustes;
using System.Web.Routing;
using BuffetWebData.Utils;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class AjustesController : BaseController
    {
        public ActionResult HistoricoPagamento(string autenticacao)
        {
            return View(new HistoricoPagamento() { id_empresa = ID_EMPRESA, autenticacao = autenticacao });
        }

        public ActionResult LocalEvento()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.LOCAL_EVENTO.Include(en => en.ENDERECO)
                    .Include(em => em.EMPRESA)
                    .Include(i => i.LOCAL_EVENTO_IMAGENS)
                    .Where(e => e.id_empresa == ID_EMPRESA).ToList());
            }
        }

        [ValidateInput(false)]
        public ActionResult SalvarLocalEvento(LOCAL_EVENTO localEvento)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    localEvento.id_empresa = ID_EMPRESA;
                    localEvento.ENDERECO.id_empresa = ID_EMPRESA;
                    localEvento.id_endereco = localEvento.ENDERECO.id;

                    if (localEvento.id != 0)
                    {
                        db.LOCAL_EVENTO.Attach(localEvento);
                        db.Entry(localEvento).State = EntityState.Modified;
                        db.ENDERECOes.Attach(localEvento.ENDERECO);
                        db.Entry(localEvento.ENDERECO).State = EntityState.Modified;
                        base.setMensagem("Local do Evento Atualizado!", tipoMensagem.tipoSucesso);
                    }
                    else
                    {
                        localEvento.autenticacao = UtilsManager.CriarAutenticacaoEmpresa();
                        db.LOCAL_EVENTO.Add(localEvento);
                        base.setMensagem("Local de Evento adicionado!", tipoMensagem.tipoSucesso);
                    }

                    db.SaveChanges();

                    List<LOCAL_EVENTO_IMAGENS> imagens_local = db.LOCAL_EVENTO_IMAGENS.Where(e => e.id_local_evento == localEvento.id && e.id_empresa == ID_EMPRESA).ToList();

                    for (int i = 0; i != Request.Files.Count; i++)
                    {
                        if (Request.Files[i].ContentLength > 0)
                        {
                            HttpPostedFileBase imagem = Request.Files[i] as HttpPostedFileBase;
                            if (imagens_local.FirstOrDefault(e => e.sequencia == i) == null)
                            {
                                LOCAL_EVENTO_IMAGENS imag = new LOCAL_EVENTO_IMAGENS()
                                {
                                    sequencia = i,
                                    id_local_evento = localEvento.id,
                                    id_empresa = ID_EMPRESA
                                };
                                db.LOCAL_EVENTO_IMAGENS.Add(imag);
                                db.SaveChanges();
                                new FTPManager(ID_EMPRESA).uploadImage(ImageFolder.LocalEvento, string.Format("{0}_{1}", localEvento.id, i.ToString()), imagem.InputStream);
                            }
                            else
                            {
                                new FTPManager(ID_EMPRESA).uploadImage(ImageFolder.LocalEvento, string.Format("{0}_{1}", localEvento.id, i.ToString()), imagem.InputStream);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Erro ao tentar adicionar Local de Evento!", tipoMensagem.tipoErro);
            }
            return RedirectToAction("LocalEvento");
        }

        public JsonResult ApagarImagem(int sequencia)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var l = db.LOCAL_EVENTO_IMAGENS.FirstOrDefault(e => e.sequencia == sequencia);
                if (l != null)
                {
                    db.LOCAL_EVENTO_IMAGENS.Remove(l);
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Empresa(string tab)
        {
            ViewBag.tab = tab;
            return View(EMPRESA);
        }

        public ActionResult AtualizarEmpresa(string contato, string telefone, string celular, HttpPostedFileBase novo_logo)
        {
            if (string.IsNullOrEmpty(contato) || string.IsNullOrEmpty(telefone) || string.IsNullOrEmpty(celular))
            {
                base.setMensagem("Verifique os Datos e Tente Novamente", tipoMensagem.tipoInfo);
                return RedirectToAction("Empresa");
            }
            else
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var em = db.EMPRESAs.FirstOrDefault(f => f.id == ID_EMPRESA);
                    if (em != null)
                    {
                        em.contato = contato;
                        em.telefone = telefone;
                        em.celular = celular;
                        db.SaveChanges();

                        if (novo_logo != null)
                        {
                            new FTPManager(em.id).updateNewLogo(novo_logo.InputStream);
                        }

                        base.setMensagem("Datos Atualizados", tipoMensagem.tipoSucesso);
                        return RedirectToAction("Empresa");
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        public ActionResult SalvarRedeSocial(LOCAL_EVENTO_SOCIAL social)
        {
            using (BuffetContext db = new BuffetContext())
            {
                social.id_empresa = ID_EMPRESA;
                db.LOCAL_EVENTO_SOCIAL.Add(social);
                db.SaveChanges();
                base.setMensagem("Rede social adicionada", tipoMensagem.tipoSucesso);
                return RedirectToAction("Empresa", new RouteValueDictionary(new { tab = "social" }));
            }
        }

        public ActionResult ApagarRedeSocial(int id_social)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.LOCAL_EVENTO_SOCIAL.Remove(db.LOCAL_EVENTO_SOCIAL.FirstOrDefault(f => f.id == id_social && f.id_empresa == ID_EMPRESA));
                    db.SaveChanges();
                    base.setMensagem("Rede social apagada", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception)
            {
                base.setMensagem("Tente novamente", tipoMensagem.tipoErro);
            }
            return RedirectToAction("Empresa", new RouteValueDictionary(new { tab = "social" }));
        }

        //EMAIL
        public ActionResult SalvarEmail(string email, string senha, string smtp, string porta)
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA_CONFIGURACAO configuracao = db.EMPRESA_CONFIGURACAO.FirstOrDefault(f => f.id_empresa == ID_EMPRESA);
                if (configuracao != null)
                {
                    configuracao.email_host = smtp;
                    configuracao.email_login = email;
                    configuracao.email_porta = porta;
                    configuracao.email_senha = senha;
                    db.SaveChanges();
                    base.setMensagem("Email configurado", tipoMensagem.tipoSucesso);
                }
            }
            return RedirectToAction("Empresa", new RouteValueDictionary(new { tab = "email" }));
        }



        //--------------------------------------------- PESQUISA ---------------------------------------------
        public JsonResult AlterarDiasApos(int dias)
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA_CONFIGURACAO config = db.EMPRESA_CONFIGURACAO.FirstOrDefault(e => e.id_empresa == ID_EMPRESA);
                config.pesquisa_dias_apos_evento = dias;
                db.SaveChanges();
                return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AlterarEnvioPesquisaAutomatica(bool alterar)
        {
            using (BuffetContext db = new BuffetContext())
            {
                EMPRESA_CONFIGURACAO config = db.EMPRESA_CONFIGURACAO.FirstOrDefault(e => e.id_empresa == ID_EMPRESA);
                config.pesquisa_enviar_automaticamente = alterar;
                db.SaveChanges();
                return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
            }
        }


        //--------------------------------------------- EMAIL CONTATO ---------------------------------------------
        public JsonResult EnviarEmailPlataforma(string email, string nome, string titulo, string mensagem)
        {
            bool enviado = new UtilsManager(base.ID_EMPRESA).SendEmailBuffetweb(base.colaborador, titulo, mensagem, email);
            if (enviado)
            {
                return Json(new { isOk = true, mensagem = "E-mail Enviado com sucesso!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isOk = true, mensagem = "Ouve algum erro, atualiza a página e tente novamente!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EnviarSMSPlataforma(string telefone, string nome, string titulo, string mensagem)
        {
            bool enviado = new UtilsManager(base.ID_EMPRESA).SendSMSBuffetweb(base.colaborador, titulo, mensagem, telefone);
            if (enviado)
            {
                return Json(new { isOk = true, mensagem = "SMS Enviado com sucesso!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isOk = true, mensagem = "Ouve algum erro, atualiza a página e tente novamente!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditarLocalEvento(int id_local)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var nome_empresa = PerfilEmpresa.NOME_EMPRESA_FTP;
                try
                {
                    var local = db.LOCAL_EVENTO.Include(d => d.LOCAL_EVENTO_IMAGENS)
                                                        .Include(e => e.ENDERECO)
                                                        .Where(e => e.id_empresa == ID_EMPRESA && e.id == id_local)
                                                        .Select(f => new
                                                        {
                                                            f.id,
                                                            f.autenticacao,
                                                            f.nome,
                                                            f.capacidade,
                                                            f.porEspaco,
                                                            f.valorMinimo,
                                                            f.telefone,
                                                            f.valor,
                                                            f.regiao,
                                                            f.palavras_chaves,
                                                            f.descricao,
                                                            f.horario_fim_almoco,
                                                            f.horario_fim_jantar,
                                                            f.horario_inicio_almoco,
                                                            f.horario_inicio_jantar,
                                                            f.idade_minima_pagante,
                                                            f.video,
                                                            enderecoid = f.ENDERECO.id,
                                                            cep = f.ENDERECO.cep,
                                                            endereco1 = f.ENDERECO.endereco1,
                                                            bairro = f.ENDERECO.bairro,
                                                            cidade = f.ENDERECO.cidade,
                                                            estado = f.ENDERECO.estado,
                                                            numero = f.ENDERECO.numero,
                                                            complemento = f.ENDERECO.complemento
                                                        }).FirstOrDefault();


                    var imagens = db.LOCAL_EVENTO_IMAGENS.Where(e => e.id_local_evento == local.id && e.id_empresa == ID_EMPRESA).OrderBy(s => s.sequencia).Select(i => new
                                                     {
                                                         url = i.sequencia.ToString()
                                                     }).ToList();

                    var comentarios = db.LOCAL_EVENTO_COMENTARIOS.Include(c => c.PORTAL_CLIENTE_PERFIL)
                                                                 .Where(e => e.id == id_local && e.id_empresa == ID_EMPRESA).Select(c => new
                                                                 {
                                                                     c.avaliacao,
                                                                     c.comentario,
                                                                     c.desde,
                                                                     c.PORTAL_CLIENTE_PERFIL.url,
                                                                     c.PORTAL_CLIENTE_PERFIL.nome,
                                                                     c.PORTAL_CLIENTE_PERFIL.perfil
                                                                 }).ToList();

                    return Json(new { isOk = true, empresa = nome_empresa, local = local, imagens = imagens, comentarios = comentarios }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}