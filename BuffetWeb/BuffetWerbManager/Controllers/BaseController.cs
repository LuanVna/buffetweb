﻿using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebManager.Models;


namespace BuffetWebManager.Controllers
{
    public abstract class BaseController : Controller
    {
        public BaseController()
        {
            this.getMensagem();
            this.setBackButton(false);

            ViewBag.mensagemModulo = mensagemModulo;
            ViewBag.moduloRenovar = moduloRenovar;
        }

        public string currentAction { get; set; }
        public string currentController { get; set; }
        public static string mensagemModulo { get; set; }
        public static string moduloRenovar { get; set; }

        protected override void OnAuthorization(AuthorizationContext context)
        {
            if (Request.UserAgent.Contains("Mobile"))
            {
                context.Result = RedirectToAction("Mobile", "Modulos");
                return;
            }

            currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            currentController = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (currentAction.Equals("Login") || currentAction.Equals("Home"))
            {
                currentAction = "";
                currentController = "";
            }

            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                int id;
                if (int.TryParse(context.RequestContext.HttpContext.User.Identity.Name, out id))
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        COLABORADORE colabor = db.COLABORADORES.Include(p => p.PERFIL_COLABORADOR).FirstOrDefault(e => e.id == id);
                        if (colabor != null)
                        {
                            List<EMPRESA_MODULOS> modulos = db.EMPRESA_MODULOS.Where(e => e.id_empresa == colabor.id_empresa).ToList();

                            PERFIL_COLABORADOR perfil = colabor.PERFIL_COLABORADOR;

                            switch (currentController)
                            {
                                case "Orcamento":
                                    {
                                        EMPRESA_MODULOS orcamento = modulos.FirstOrDefault(e => e.modulo.Equals("ORCAMENTO"));
                                        if (orcamento.expira_em.Date <= DateTime.Now.Date)
                                        {
                                            if (currentAction != "Orcamentos" &&
                                                currentAction != "DetalhesOrcamento" &&
                                                currentAction != "BuscarOrcamento" &&
                                                currentAction != "EnviarDados" &&
                                                currentAction != "SalvarAnotacao")
                                            {
                                                mensagemModulo = String.Format("Este módulo esteve disponivel até {0}", orcamento.expira_em.ToShortDateString());
                                                moduloRenovar = "orcamento";
                                                context.Result = NotModule;
                                            }
                                        } break;
                                    }

                                case "Financeiro":
                                    {
                                        EMPRESA_MODULOS financeiro = modulos.FirstOrDefault(e => e.modulo.Equals("FINANCEIRO"));
                                        if (financeiro.expira_em.Date <= DateTime.Now.Date)
                                        {
                                            mensagemModulo = String.Format("Este módulo esteve disponivel até {0}", financeiro.expira_em.ToShortDateString());
                                            moduloRenovar = "financeiro";
                                            context.Result = NotModule;
                                        }
                                    }; break;
                                case "Estoque":
                                    {
                                        EMPRESA_MODULOS estoque = modulos.FirstOrDefault(e => e.modulo.Equals("ESTOQUE"));
                                        if (estoque.expira_em.Date <= DateTime.Now.Date)
                                        {
                                            mensagemModulo = String.Format("Este módulo esteve disponivel até {0}", estoque.expira_em.ToShortDateString());
                                            moduloRenovar = "financeiro";
                                            context.Result = NotModule;
                                        }
                                    }; break;
                                case "Convite":
                                    {
                                        EMPRESA_MODULOS convite = modulos.FirstOrDefault(e => e.modulo.Equals("CONVITE"));
                                        if (convite.expira_em.Date <= DateTime.Now.Date)
                                        {
                                            mensagemModulo = String.Format("Este módulo esteve disponivel até {0}", convite.expira_em.ToShortDateString());
                                            moduloRenovar = "convite";
                                            context.Result = NotModule;
                                        }

                                    }; break;
                                case "Portal":
                                    {
                                        EMPRESA_MODULOS portal = modulos.FirstOrDefault(e => e.modulo.Equals("PORTAL"));
                                        if (portal.expira_em.Date <= DateTime.Now.Date)
                                        {
                                            mensagemModulo = String.Format("Este módulo esteve disponivel até {0}", portal.expira_em.ToShortDateString());
                                            moduloRenovar = "portal";
                                            context.Result = NotModule;
                                        }

                                    }; break;

                                case "Ajustes":
                                    {
                                        if (colaborador.administrador == false)
                                        {
                                            context.Result = NotModule;
                                        }
                                    }; break;
                            }

                            switch (currentController)
                            {
                                case "Orcamento": if (!perfil.v_orcamentos) { context.Result = NotAcess; }; break;
                                case "Ajustes": if (!perfil.v_ajustes) { context.Result = NotAcess; }; break;
                                case "Clientes": if (!perfil.v_clientes) { context.Result = NotAcess; }; break;
                                case "Colaborador": if (!perfil.v_colaborador) { context.Result = NotAcess; }; break;
                                case "Estoque": if (!perfil.v_estoque) { context.Result = NotAcess; }; break;
                                case "Eventos": if (!perfil.v_eventos) { context.Result = NotAcess; }; break;
                                case "Financeiro": if (!perfil.v_financeiro) { context.Result = NotAcess; }; break;
                                case "Convite": if (!perfil.v_convite) { context.Result = NotAcess; }; break;
                            }
                        }
                    }
                }
            } 
        }

        public enum tipoMensagem
        {
            tipoSucesso,
            tipoErro,
            tipoAtencao,
            tipoInfo
        }

        private static string mensagem;
        private static string tipo;

        public int ID_EMPRESA { get { return PerfilEmpresa.ID_EMPRESA; } }

        public EMPRESA EMPRESA
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.Include(a => a.EMPRESA_ARQUIVOS)
                                      .Include(m => m.EMPRESA_MODULOS)
                                      .Include(c => c.EMPRESA_CONFIGURACAO) 
                                      .Include(e => e.ENDERECO)
                                      .FirstOrDefault(e => e.id == this.ID_EMPRESA);
                }
            }
        }

        public RedirectToRouteResult LoginView
        {
            get
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public RedirectToRouteResult IndexView
        {
            get
            {
                return RedirectToAction("", "");
            }
        }
        public RedirectToRouteResult NotModule
        {
            get
            {
                return RedirectToAction("ModuloNaoPermitido", "Modulos");
            }
        }

        public RedirectToRouteResult NotAcess
        {
            get
            {
                return RedirectToAction("AcessoNegado", "Modulos");
            }
        }

        public RedirectToRouteResult redirect(string action, string controller = "")
        {
            this.setMensagem("Você  não tem permissão para fazer isto!");
            return RedirectToAction(action, controller);
        }

        public COLABORADORE colaborador
        {
            get { return this.colaboradorLogado(); }
        }

        public bool isLogin()
        {
            return this.colaborador != null;
        }

        public PERFIL_COLABORADOR perfil
        {
            get
            {
                return this.colaboradorLogado().PERFIL_COLABORADOR;
            }
        }

        private COLABORADORE colaboradorLogado()
        {
            return BWColaborador.getColaborador();
        }

        public bool Entrar(string email, string senha)
        {
            return BWColaborador.AutenticaColaborador(email, senha);
        }

        public void Sair()
        {
            BWColaborador.Deslogar();
        }

        public void getMensagem()
        {
            if (mensagem != null)
            {
                ViewBag.mensagem = mensagem;
                ViewBag.tipo = tipo;
                mensagem = null;
            }
        }

        private static bool BackButton { get; set; }

        public void setBackButton(bool status)
        {
            ViewBag.backButton = status;
        }

        public void setMensagem(string texto, tipoMensagem tipoMensagem = tipoMensagem.tipoInfo)
        {
            switch (tipoMensagem)
            {
                case tipoMensagem.tipoSucesso: tipo = "alert-sucess"; break;
                case tipoMensagem.tipoErro: tipo = "alert-danger"; break;
                case tipoMensagem.tipoAtencao: tipo = "alert-warning"; break;
                case tipoMensagem.tipoInfo: tipo = "alert-info"; break;
            }
            mensagem = string.Format("{0}|{1}", texto, tipo);
        }
    }
}
