﻿using BuffetWebManager.Controllers;
using BuffetWerb.Models.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;

using System.Web.Routing;
using BuffetWebManager.Models;
using System.Data.Entity;
using BuffetWebData.Utils;
using System.IO;

namespace BuffetWerb.Controllers
{
    [Authorize]
    public class ClientesController : BaseController
    {
        public ActionResult NovoCliente(string acao, string controlador)
        {
            base.setBackButton(!string.IsNullOrEmpty(acao));

            ViewBag.action = acao;
            ViewBag.controller = controlador;

            return View();
        }

        public ActionResult MeusClientes()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.CLIENTEs.Include(o => o.ORCAMENTOes).Include(em => em.ENDERECO).Where(e => e.id_empresa == ID_EMPRESA).ToList());
            }
        }

        public ActionResult ImportarClientes()
        {
            return View();
        }

        public ActionResult DetalhesCliente(int? id_cliente)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE cliente = db.CLIENTEs.Include(em => em.ENDERECO)
                                            .Include(p => p.CLIENTE_PROPOSTA.Select(e => e.ORCAMENTO_PROPOSTA))
                                            .FirstOrDefault(e => e.id == (int)id_cliente && e.id_empresa == ID_EMPRESA);
                if (cliente != null)
                {
                    base.setBackButton(true);
                    return View(cliente);
                }

                base.setMensagem("Cliente não encontrado");
                return RedirectToAction("Clientes");
            }
        }


        public ActionResult SalvarClientes(CLIENTE cliente, List<CLIENTE_ANIVERSARIANTES> familiares, string acao, string controlador)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    if (cliente.email == null && cliente.celular == null)
                    {
                        base.setMensagem("Você precisa informar o email ou o celular do Cliente", tipoMensagem.tipoAtencao);
                        return RedirectToAction("Clientes");
                    }

                    int id_empresa = PerfilEmpresa.ID_EMPRESA;
                    cliente.id_empresa = id_empresa;
                    if (familiares != null)
                    {
                        familiares = familiares.Where(e => e.nome != null).ToList();
                        familiares.ForEach(f => f.id_empresa = ID_EMPRESA);
                        familiares.ForEach(f => f.id_cliente = cliente.id);
                        familiares.ForEach(f => f.desde = DateTime.Now.Date);
                        cliente.CLIENTE_ANIVERSARIANTES = familiares;
                    }

                    if (cliente.ENDERECO.cep == null)
                        cliente.ENDERECO = null;
                    else
                        cliente.ENDERECO.id_empresa = id_empresa;

                    if (cliente.id == 0)
                    {
                        cliente.autenticacao = UtilsManager.CriarAutenticacao();
                        cliente.desde = DateTime.Today;
                        cliente.cliente_de = "ADMINISTRATIVO";
                        base.setMensagem("Cliente adicionado", tipoMensagem.tipoSucesso);
                        db.CLIENTEs.Add(cliente);
                    }
                    else
                    {
                        base.setMensagem("Cliente editado", tipoMensagem.tipoSucesso);
                        db.CLIENTEs.Attach(cliente);
                        db.Entry(cliente).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                catch (Exception e) { }
                if (acao != null && !acao.Equals(""))
                {
                    return RedirectToAction(acao, controlador, new RouteValueDictionary(new { id_cliente = cliente.id }));
                }
            }
            return RedirectToAction("NovoCliente");
        }

        public ActionResult ImportarContatos(bool confirmar, HttpPostedFileBase contatos)
        {
            if (contatos != null)
            {
                string path = Path.Combine(Server.MapPath("~/App_Data/Temp"), Path.GetFileName(contatos.FileName));

                LogImportClientes d = new Clientes(ID_EMPRESA).ImportarClientes(contatos, confirmar, path);
                return View(d);
            }
            return RedirectToAction("Clientes");
        }

        public JsonResult VerificaEmail(string email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE cliente = db.CLIENTEs.FirstOrDefault(e => e.email.Equals(email) && e.id_empresa == ID_EMPRESA);
                if (cliente != null)
                    return Json(new { ClienteDo = "Buffet", nome = cliente.nome }, JsonRequestBehavior.AllowGet);

                var perfil = db.PORTAL_CLIENTE_PERFIL.Where(e => e.email.Equals(email))
                                                                       .Select(e => new
                                                                       {
                                                                           e.id,
                                                                           e.nome,
                                                                           e.perfil,
                                                                           e.telefone,
                                                                           e.celular,
                                                                           e.url,
                                                                           e.id_facebook,
                                                                           e.cep,
                                                                           e.logradouro,
                                                                           e.bairro,
                                                                           e.cidade,
                                                                           e.estado,
                                                                           e.numero,
                                                                       }).FirstOrDefault();

                var timeline = db.CLIENTE_TIMELINE.Where(e => e.email.Equals(email) && e.id_empresa == ID_EMPRESA)
                                                  .Select(f => new
                                                        {
                                                            f.email,
                                                            f.descricao,
                                                            data = f.data,
                                                            f.titulo,
                                                            f.icone
                                                        }).ToList();

                if (perfil != null)
                {
                    return Json(new { ClienteDo = "Portal", perfil = perfil, timeline = timeline }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { ClienteDo = "Nenhum" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarOperadora(string telefone)
        {
            Tuple<bool, string> resposta = UtilsManager.GetOperadora(telefone);
            return Json(resposta, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Familiares(int id_cliente)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var familiares = db.CLIENTE_ANIVERSARIANTES.Where(d => d.id_cliente == id_cliente && d.id_empresa == ID_EMPRESA)
                    .Select(f => new
                    {
                        f.id,
                        f.nome,
                        f.nascimento,
                        f.parentesco
                    }).ToList();
                return Json(new { isOk = true, familiares = familiares }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
