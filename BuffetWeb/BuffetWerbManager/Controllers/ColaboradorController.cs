﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebManager.Models;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebManager.Models.Colaboradores;
using BuffetWebManager.Models.Colaborador;
using BuffetWebData.Utils;
using System.Web.Routing;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class ColaboradorController : BaseController
    {
        public ActionResult Perfil(string tab)
        {
            COLABORADORE colab = base.colaborador;
            ViewBag.tab = tab;
            using (BuffetContext db = new BuffetContext())
            {
                List<NOTIFICACO> notificacoes = db.NOTIFICACOES.Where(e => e.para_id_colaborador == colab.id).ToList();
                if (notificacoes != null)
                {
                    notificacoes.ForEach(e => e.lido = true);
                    db.SaveChanges();
                }
                base.getMensagem();
            }
            return View(colab);
        }

        public ActionResult AtualizarImagem(HttpPostedFileBase image)
        {
            new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Perfil, base.colaborador.id.ToString(), image.InputStream);
            return RedirectToAction("Perfil");
        }

        public ActionResult EnviarNotificacoes(string mensagem, List<ColaboradorSelecionado> colaboradores, string tab)
        {
            UtilsManager utils = new UtilsManager(base.ID_EMPRESA);
            NOTIFICACO notificacao = new NOTIFICACO();
            notificacao.data = DateTime.Now;
            notificacao.mensagem = mensagem;
            notificacao.de_id_colaborador = base.colaborador.id;
            foreach (var colaborador in colaboradores.Where(s => s.colaborador_selecionado == true).ToList())
            {
                notificacao.para_id_colaborador = colaborador.id_colaborador;
                utils.sendNotificacao(notificacao);
            }

            base.setMensagem(string.Format("{0}", colaboradores.Where(s => s.colaborador_selecionado == true).ToList().Count == 1 ? "Mensagem enviada com sucesso!" : "Mensagens enviadas com sucesso!"));
            return RedirectToAction("Perfil", new RouteValueDictionary(new { tab = tab }));
        }

        public ActionResult AlterarStatus(int id_device, bool status)
        {
            int id_colaborador = base.colaborador.id;
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADOR_DEVICES device = db.COLABORADOR_DEVICES.FirstOrDefault(e => e.id == id_device && e.id_colaborador == id_colaborador && e.id_empresa == ID_EMPRESA);
                if (device != null)
                {
                    device.status = status;
                    db.SaveChanges();
                    base.setMensagem(string.Format("Aparelho {0}", status ? "habilitado" : "desabilitado"), tipoMensagem.tipoSucesso);
                    return RedirectToAction("Perfil", new { tab = "devices" });
                }
                return RedirectToAction("Perfil");
            }
        }

        public ActionResult ApagarDevice(int id_device)
        {
            int id_colaborador = base.colaborador.id;
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADOR_DEVICES device = db.COLABORADOR_DEVICES.FirstOrDefault(e => e.id == id_device && e.id_colaborador == id_colaborador && e.id_empresa == ID_EMPRESA);
                if (device != null)
                {
                    db.COLABORADOR_DEVICES.Remove(device);
                    base.setMensagem("Aparelho removido", tipoMensagem.tipoSucesso);
                    return RedirectToAction("Perfil", new { tab = "devices" });
                }
                return RedirectToAction("Perfil");
            }
        }


        public ActionResult Colaboradores()
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (db.COLABORADORES.Where(c => c.id_empresa == ID_EMPRESA).ToList().Count != PerfilEmpresa.MAX_COLABORADOR)
                {
                    base.setMensagem("Você atingiu a quantidade máxima de colaboradores cadastrados entre em contato com o comercial do BuffetWeb para mais informações!", tipoMensagem.tipoInfo);
                }
                return View(new Colaboradores());
            }
        }

        public ActionResult SalvarColaborador(COLABORADORE colaborador, HttpPostedFileBase image, int? id)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (id == null)
                {
                    if (db.COLABORADORES.Where(c => c.id_empresa == ID_EMPRESA).ToList().Count != PerfilEmpresa.MAX_COLABORADOR)
                    {
                        try
                        {
                            colaborador.id_empresa = ID_EMPRESA;
                            colaborador.senha = RLSecurity.Security.CriptographyOn(PerfilEmpresa.NOME_EMPRESA.Replace(" ", "").ToLower());
                            colaborador.sessao = "";
                            colaborador.mostrar_dica = true;
                            db.COLABORADORES.Add(colaborador);
                            db.SaveChanges();
                            if (image != null)
                            {
                                Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Perfil, colaborador.id.ToString(), image.InputStream);
                                if (resposta.Item1)
                                {
                                    colaborador.url = resposta.Item2;
                                    db.SaveChanges();
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            return RedirectToAction("Colaboradores");
                        }
                    }
                    else
                    {
                        base.setMensagem("Você atingiu a quantidade máxima de colaboradores cadastrados entre em contato com o comercial do BuffetWeb para mais informações!", tipoMensagem.tipoInfo);
                        return RedirectToAction("Colaboradores");
                    }
                }
                else
                {
                    COLABORADORE c = db.COLABORADORES.Where(co => co.id == id).FirstOrDefault();
                    c.id_perfil = colaborador.id_perfil;
                    c.nome = colaborador.nome;
                    c.senha = RLSecurity.Security.CriptographyOn(PerfilEmpresa.NOME_EMPRESA.Replace(" ", "").ToLower());
                    c.email = colaborador.email;
                    c.telefone = colaborador.telefone;
                    db.SaveChanges();
                    if (image != null)
                    {
                        Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Perfil, colaborador.id.ToString(), image.InputStream);
                        if (resposta.Item1)
                        {
                            c.url = resposta.Item2;
                            db.SaveChanges();
                        }
                    }
                }

                base.setMensagem("Colaborador cadastrado", tipoMensagem.tipoSucesso);
            }
            return RedirectToAction("Colaboradores");
        }

        public ActionResult ApagarColaboradores(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    COLABORADORE co = db.COLABORADORES.Where(i => i.id == id && i.id_empresa == PerfilEmpresa.ID_EMPRESA).FirstOrDefault();
                    if (co != null)
                    {
                        db.COLABORADORES.Remove(co);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

            }
            return RedirectToAction("Colaboradores");
        }

        public ActionResult ResetarSenha(int id)
        {
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADORE colaborador = db.COLABORADORES.Where(c => c.id == id && c.id_empresa == PerfilEmpresa.ID_EMPRESA).FirstOrDefault();
                if (colaborador != null)
                {
                    colaborador.senha = RLSecurity.Security.CriptographyOn(PerfilEmpresa.NOME_EMPRESA.Replace(" ", "").ToLower());
                    db.SaveChanges();
                    base.setMensagem("Senha resetada com sucesso");
                    if (base.colaborador.id == id)
                    {
                        base.Sair();
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return RedirectToAction("Colaboradores");
        }


        public ActionResult Perfis()
        {
            ViewBag.c_colaborador = base.perfil.c_colaborador;
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.PERFIL_COLABORADOR.Where(p => p.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList());
            }
        }


        public ActionResult SalvarPerfil(PERFIL_COLABORADOR perfil, int? id)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (id == null)
                {
                    perfil.id_empresa = PerfilEmpresa.ID_EMPRESA;
                    db.PERFIL_COLABORADOR.Add(perfil);
                    base.setMensagem("Perfil atualizado com sucesso!");
                }
                else
                {
                    PERFIL_COLABORADOR p = db.PERFIL_COLABORADOR.Where(pe => pe.id == id && pe.id_empresa == PerfilEmpresa.ID_EMPRESA).FirstOrDefault();
                    if (p != null)
                    {
                        p.nome = perfil.nome;
                        p.descricao = perfil.descricao;
                        base.setMensagem("Perfil editado com sucesso!");
                    }
                }
                db.SaveChanges();
            }
            return RedirectToAction("Colaboradores");
        }

        public ActionResult ApagarPerfil(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    PERFIL_COLABORADOR co = db.PERFIL_COLABORADOR.Where(i => i.id == id && i.id_empresa == PerfilEmpresa.ID_EMPRESA).FirstOrDefault();
                    if (co != null)
                    {
                        db.PERFIL_COLABORADOR.Remove(co);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
            }
            return RedirectToAction("Colaboradores");
        }

        //-------------------------- JSON -----------------------------
        public JsonResult Ajustes(bool check, string key)
        {
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADORE cola = db.COLABORADORES.Include(n => n.COLABORADOR_NOTIFICACAO).Where(c => c.id == colaborador.id && c.id_empresa == PerfilEmpresa.ID_EMPRESA).FirstOrDefault();
                if (cola != null)
                {
                    if (key.Equals("MostrarDica"))
                    {
                        cola.mostrar_dica = check;
                    }
                    else
                    {
                        switch (key)
                        {
                            case "FechaPedido": cola.COLABORADOR_NOTIFICACAO.orcamento_fecha_pedido = check; break;
                            case "AgendarVisita": cola.COLABORADOR_NOTIFICACAO.orcamento_gendar_visita = check; break;
                            case "NovoPedido": cola.COLABORADOR_NOTIFICACAO.orcamento_novo_pedido = check; break;
                            case "NotificacaoEmail": cola.COLABORADOR_NOTIFICACAO.notificacao_email = check; break;
                            case "PesquisaConcluida": cola.COLABORADOR_NOTIFICACAO.pesquisa_concluida = check; break;
                        }
                    }
                    db.SaveChanges();
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
