﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebManager.Models;
using BuffetWebManager.Models.Convite;
using System.Web.Routing;
using BuffetWebData.Utils;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class ConviteController : BaseController
    {
        public ActionResult CriarConvites(string filtro)
        {
            ViewBag.filtro = filtro;
            return View(new CriarConvitesModel() { id_empresa = ID_EMPRESA, filtro = filtro });
        }

        public ActionResult Convites(string codigo_unico, int? id_cliente)
        {
            if (string.IsNullOrEmpty(codigo_unico) && string.IsNullOrEmpty(id_cliente.ToString()))
            {
                return RedirectToAction("CriarConvites");
            }

            ViewBag.isDesktop = !Request.UserAgent.Contains("Mobile");
            ViewBag.codigo_unico = codigo_unico;
            ConviteModel convite = new ConviteModel() { id_empresa = base.ID_EMPRESA, codigo_unico = codigo_unico, id_cliente = id_cliente }; 
            return View(convite);
        }

        public ActionResult DetalhesConvite(string codigo_unico)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ViewBag.NOME_EMPRESA = base.EMPRESA.nome.Replace(" ", "");
                return View(db.ORCAMENTOes.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.codigo_unico.Equals(codigo_unico)));
            }
        }

        public ActionResult ImprimirListaDeConvidados(string codigo_unico, string tipoLista)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (tipoLista.Equals("COMPLETA"))
                {
                    return View(db.ORCAMENTOes.Include(f => f.CONVITE_CONVIDADOS)
                    .FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico) && e.id_empresa == ID_EMPRESA)
                    .CONVITE_CONVIDADOS.OrderBy(od => od.nome_completo).ToList());
                }
                else
                {
                    return View(db.ORCAMENTOes.Include(f => f.CONVITE_CONVIDADOS)
                    .FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico) && e.id_empresa == ID_EMPRESA)
                    .CONVITE_CONVIDADOS.Where(f => f.ira.Equals("EU VOU")).OrderBy(od => od.nome_completo).ToList());
                }
            }
        }

        public ActionResult SalvarConvite(string codigo_unico, ORCAMENTO orcamento, int id_template_tema, ICollection<AniversariantesModel> aniversariantes, CONVITE_TEMPLATE template, int? id_convite_editando)
        {
            orcamento.codigo_unico = new UtilsManager().GerarCodigoUnico();
           
            //orcamento.senha = UtilsManager.gerarSenha().Item2;
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id_convite_editando == null)
                    {
                        CLIENTE cliente = db.CLIENTEs.FirstOrDefault(c => c.id == orcamento.id_cliente);
                        if (cliente != null)
                        {
                            orcamento.data_criacao = DateTime.Now.Date;
                            orcamento.email = cliente.email;
                            orcamento.id_empresa = base.ID_EMPRESA;
                            orcamento.apenas_convite = true;
                            orcamento.status = "CONVITE";
                            orcamento.max_acompanhantes = 0;
                            orcamento.autenticacao = UtilsManager.CriarAutenticacao();

                            if (aniversariantes != null)
                            {
                                foreach (var aniversariante in aniversariantes.Where(e => e.aniversarianteSelecionado == true).ToList())
                                {
                                    orcamento.CONVITE_ANIVERSARIANTES.Add(new CONVITE_ANIVERSARIANTES()
                                    {
                                        id_aniversariante = aniversariante.id_aniversariante,
                                        id_empresa = base.ID_EMPRESA,
                                        id_orcamento = orcamento.id
                                    });
                                }
                            }

                            template.id_orcamento = orcamento.id;
                            template.id_empresa = base.ID_EMPRESA;
                            template.id_convite = id_template_tema;
                            template.url_image = "";
                            template.titulo_convite = template.titulo_convite == null ? "" : template.titulo_convite;
                            db.ORCAMENTOes.Add(orcamento);
                            db.CONVITE_TEMPLATE.Add(template);
                            db.SaveChanges();

                            Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadTemplateConvite(template.id, id_template_tema);
                            if (resposta.Item1)
                            {
                                template.url_image = resposta.Item2;
                                db.SaveChanges();
                            }
                            base.setMensagem("Novo convite adicionado!", tipoMensagem.tipoSucesso);
                            return RedirectToAction("DetalhesOrcamento", "Orcamento", new RouteValueDictionary(new { codigo_unico = orcamento.codigo_unico, tab = "convidados" }));

                        }
                        else
                        {
                            base.setMensagem("Esse cliente não existe!", tipoMensagem.tipoErro);
                            return RedirectToAction("Orcamentos", "Orcamento");
                        }
                    }
                    else
                    {
                        CONVITE_TEMPLATE convite = db.CONVITE_TEMPLATE.FirstOrDefault(e => e.id == (int)id_convite_editando);
                        if (convite != null)
                        {
                            ORCAMENTO o = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico));
                            if (o != null)
                            {
                                template.id = (int)id_convite_editando;
                                template.url_image = "";
                                template.id_empresa = base.ID_EMPRESA;
                                template.id_orcamento = o.id;
                                template.id_convite = id_template_tema;
                                template.titulo_convite = template.titulo_convite == null ? "" : template.titulo_convite;

                                db.Entry(convite).CurrentValues.SetValues(template);

                                db.SaveChanges();

                                Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadTemplateConvite(template.id, id_template_tema);
                                if (resposta.Item1)
                                {
                                    convite.url_image = resposta.Item2;
                                    db.SaveChanges();
                                }
                            }
                            base.setMensagem("Convite atualizado!", tipoMensagem.tipoSucesso);
                            return RedirectToAction("DetalhesOrcamento", "Orcamento", new RouteValueDictionary(new { codigo_unico = codigo_unico, tab = "convidados" }));
                        }
                    }

                }
            }
            catch (Exception e)
            {
                base.setMensagem("Verifique as informações e tente novamente!", tipoMensagem.tipoErro);
            }
            return RedirectToAction("Orcamento", "Orcamento");
        }

        //------------------------------- CONVITE --------------------------/
        public ActionResult SalvarNovoConvite(ORCAMENTO convite, HttpPostedFileBase image)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    bool repeat = true;
                    do
                    {
                        string codigo_unico = new Random().Next(0, 100000000).ToString();
                        if (db.ORCAMENTOes.Where(i => i.codigo_unico.Equals(codigo_unico)).FirstOrDefault() == null)
                        {
                            convite.apenas_convite = true;
                            convite.codigo_unico = codigo_unico;
                            convite.id_empresa = base.ID_EMPRESA;
                            convite.status = "CONVITE";
                            convite.data_criacao = DateTime.Now.Date;
                            convite.email = db.CLIENTEs.FirstOrDefault(e => e.id == convite.id_cliente).email;
                            string senha = new Random().Next(0, 100000).ToString();
                            convite.senha = RLSecurity.Security.CriptographyOn(senha);

                            db.ORCAMENTOes.Add(convite);
                            db.SaveChanges();

                            if (image != null)
                            {
                                FTPManager img = new FTPManager(base.ID_EMPRESA);
                                Tuple<bool, string> resposta = img.uploadImage(ImageFolder.Convites, convite.codigo_unico, image.InputStream);
                            }
                            repeat = false;
                        }
                    } while (repeat);
                    base.setMensagem("Novo Convite Salvo");
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Erro");
            }
            return RedirectToAction("DetalhesConvite", new RouteValueDictionary(new { codigo_unico = convite.codigo_unico }));

        }

        public JsonResult SalvarConvidado(CONVITE_CONVIDADOS convidado, int id_orcamento_convite)
        {
            using (BuffetContext db = new BuffetContext())
            {
                convidado.id_empresa = base.ID_EMPRESA;
                convidado.id_orcamento = id_orcamento_convite;
                convidado.ira = "Confirmado";
                convidado.autenticacao = RLSecurity.Security.CriptographyOn(new Random().Next(1, 200000).ToString());
                db.CONVITE_CONVIDADOS.Add(convidado);
                db.SaveChanges();
                //id_orcamento = id_orcamento_convite;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }


        //------------------------------- PORTARIA --------------------------/

        public ActionResult Portaria(int? id_orcamento)
        {
            PortariaModel portaria = new PortariaModel() { id_empresa = ID_EMPRESA, id_orcamento = id_orcamento };
            id_orcamento = null;
            return View(portaria);
        }

        public ActionResult NovoConvidado(CONVITE_CONVIDADOS convidado)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    convidado.id_empresa = ID_EMPRESA;
                    convidado.ira = "EU VOU";
                    convidado.autenticacao = UtilsManager.CriarAutenticacao();
                    convidado.sms_enviado = false;
                    convidado.email_enviado = false;
                    convidado.mensagem = "";
                    db.CONVITE_CONVIDADOS.Add(convidado);
                    db.SaveChanges();
                    base.setMensagem("Novo convidado adicionado com sucess!");
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Ouve algum erro ao tentar adicionar um novo convidado!", tipoMensagem.tipoErro);
            }
            return RedirectToAction("Portaria", new RouteValueDictionary(new { id_orcamento = convidado.id_orcamento }));
        }

        public JsonResult ConfirmarPresenca(int id, int id_orcamento_convite, string email, string telefone, string status, int? convidados)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.id_orcamento == id_orcamento_convite && e.id_empresa == ID_EMPRESA && e.id == id);
                    if (convidado != null)
                    {
                        convidado.email = email;
                        convidado.telefone = telefone;
                        convidado.compareceu = status.Equals("Confirmar");
                        convidado.convidados = convidados;
                        db.SaveChanges();
                    }
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarEmailETelefone(int id, int id_orcamento, string email, string telefone)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.id_orcamento == id_orcamento && e.id_empresa == ID_EMPRESA && e.id == id);
                if (convidado != null)
                {
                    convidado.email = email;
                    convidado.telefone = telefone;
                    db.SaveChanges();
                }
                return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ConfirmarFamiliar(CONVITE_CONVIDADOS_FAMILIARES familiar, int convidados, int i)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CONVITE_CONVIDADOS convidado = db.CONVITE_CONVIDADOS.FirstOrDefault(e => e.id_orcamento == familiar.id_orcamento && e.id_empresa == ID_EMPRESA && e.id == familiar.id_convidado);
                    if (convidado != null)
                    {
                        convidado.convidados = convidados;
                        familiar.id_empresa = ID_EMPRESA;
                        if (familiar.id == 0)
                        {
                            db.CONVITE_CONVIDADOS_FAMILIARES.Add(familiar);
                        }
                        else
                        {
                            db.CONVITE_CONVIDADOS_FAMILIARES.Attach(familiar);
                            db.Entry(familiar).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                        return Json(new { isOk = true, id = familiar.id, i = i }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }













        //------------------------------- JSON -------------------------------
        public JsonResult BuscarListaDeConvidados(int id_orcamento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var convidados = db.CONVITE_CONVIDADOS.Where(e => e.id_orcamento == id_orcamento && e.id_empresa == ID_EMPRESA)
                                                      .Select(e => new { isOk = true, e.id, e.nome_completo, e.telefone, e.email, e.ira, e.compareceu }).ToList();
                return Json(convidados, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarConviteTemas()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var convite = db.B_CONVITES_DISPONIVEIS.Where(e => e.disponivel == true).OrderBy(e => e.tema).ToList();
                return Json(new { isOk = true, convite = convite }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarClienteSelecionado(int id_cliente)
        {
            using (BuffetContext db = new BuffetContext())
            {
                int ano = DateTime.Now.Date.Year;

                var cliente = db.CLIENTEs.Include(f => f.CLIENTE_ANIVERSARIANTES)
                                          .Where(c => c.id == id_cliente)
                                          .Select(f => new
                                          {
                                              nome = f.nome,
                                              telefone = f.telefone,
                                              celular = f.celular,
                                              email = f.email,
                                              //TODO: mexer depois
                                              //onde_cliente = f.ONDE_CONHECEU.nome,
                                              familiares = f.CLIENTE_ANIVERSARIANTES.Select(fa => new
                                              {
                                                  id = fa.id,
                                                  nome = fa.nome,
                                                  parentesco = fa.parentesco,
                                                  nascimento = fa.nascimento,
                                                  idade = (ano - fa.nascimento.Year)
                                              })
                                          }).FirstOrDefault();

                cliente.familiares.ToList().ForEach(e => e.nascimento.ToShortDateString().Replace("00:00:00", ""));
                if (cliente != null)
                {
                    return Json(new { isOk = true, data = cliente }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult HorariosDisponiveis()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var horarios = db.HORARIOS.Select(e => new { e.id, e.horario_de, e.horario_ate }).ToList();
                return Json(new { isOk = true, horarios = horarios }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarEnderecoLocal(int id_local)
        {
            using (BuffetContext db = new BuffetContext())
            {
                LOCAL_EVENTO local = db.LOCAL_EVENTO.Include(en => en.ENDERECO).FirstOrDefault(e => e.id == id_local && e.id_empresa == ID_EMPRESA);
                if (local != null)
                {
                    string local_evento = string.Format("{0}, {1}, {2} \n{3}. \nContato {4}", local.ENDERECO.endereco1, local.ENDERECO.numero, local.ENDERECO.bairro, local.ENDERECO.complemento, local.telefone);
                    return Json(new { isOk = true, endereco = local_evento }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("load", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarFamiliar(CLIENTE_ANIVERSARIANTES familiar)
        {
            using (BuffetContext db = new BuffetContext())
            {
                familiar.id_empresa = base.ID_EMPRESA;
                familiar.desde = DateTime.Now.Date;
                db.CLIENTE_ANIVERSARIANTES.Add(familiar);
                db.SaveChanges();
                return Json(new { isOk = true, id = familiar.id, idade = (DateTime.Now.Year - familiar.nascimento.Year) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarFrases()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return Json(new { frases = db.B_CONVITES_FRASES.ToList() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SolicitarTema(string nome_tema)
        {
            using (BuffetContext db = new BuffetContext())
            {
                UtilsManager notificar = new UtilsManager(base.ID_EMPRESA);
                NOTIFICACO notificacao = new NOTIFICACO();
                notificacao.de_id_colaborador = base.colaborador.id;
                notificacao.data = DateTime.Now.Date;
                notificacao.mensagem = string.Format("O tema {0} foi solicitador por {1} da empresa {2}", nome_tema, base.colaborador.nome, base.EMPRESA.nome);
                notificacao.paraBuffetWeb = true;
                return Json(new { isOk = notificar.sendNotificacao(notificacao) }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
