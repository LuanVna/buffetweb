﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebManager.Models.Estoque;
using BuffetWebManager.Models;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class EstoqueController : BaseController
    {
        public ActionResult Fornecedores(bool? deCardapio)
        {
            base.getMensagem();
            ViewBag.deCardapio = deCardapio;
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.FORNECEDORs.Where(f => f.id_empresa == ID_EMPRESA).ToList());
            }
        }

        public ActionResult SalvarFornecedor(FORNECEDOR fornecedor, bool? deCardapio)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    fornecedor.id_empresa = base.ID_EMPRESA;
                    db.FORNECEDORs.Add(fornecedor);
                    db.SaveChanges();
                    base.setMensagem("Fornecedor salvo com sucesso", tipoMensagem.tipoSucesso);
                    if (deCardapio == true)
                    {
                        return RedirectToAction("ItensCardapio", "Itens");
                    }
                }
            }
            catch (Exception)
            {
                base.setMensagem("Fornecedor salvo com sucesso", tipoMensagem.tipoSucesso);
                if (deCardapio == true)
                {
                    base.setMensagem("Fornecedor não foi salvo", tipoMensagem.tipoErro);
                }
            }
            return RedirectToAction("Fornecedores");
        }

        public JsonResult SalvarFornecedorJSON(FORNECEDOR fornecedor, ENDERECO endereco)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    fornecedor.id_empresa = base.ID_EMPRESA;
                    db.FORNECEDORs.Add(fornecedor);
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult SalvarFornecedorIngredienteJSON(int id_ingrediente, int id_fornecedor)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.FORNECEDOR_INGREDIENTE.Add(new FORNECEDOR_INGREDIENTE()
                    {
                        id_empresa = base.ID_EMPRESA,
                        id_fornecedor = id_fornecedor,
                        id_ingrediente = id_ingrediente
                    });
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarIngredienteJSON(INGREDIENTE ingrediente, int? id_ingrediente)
        {
            if (id_ingrediente == null)
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        ingrediente.id_empresa = base.ID_EMPRESA;
                        db.INGREDIENTES.Add(ingrediente);
                        db.SaveChanges();
                        Tuple<bool, int> resposta = new Tuple<bool, int>(true, ingrediente.id);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                using (BuffetContext db = new BuffetContext())
                {
                    INGREDIENTE ing = db.INGREDIENTES.FirstOrDefault(e => e.id == id_ingrediente && e.id_empresa == ID_EMPRESA);
                    if (ing != null)
                    {
                        db.FORNECEDOR_INGREDIENTE.RemoveRange(db.FORNECEDOR_INGREDIENTE.Where(e => e.id_ingrediente == id_ingrediente).ToList());
                        ing.id_unidade_medida = ingrediente.id_unidade_medida;
                        ing.marca = ingrediente.marca;
                        ing.quantidade_embalagem = ingrediente.quantidade_embalagem;
                        ing.valor_custo = ingrediente.valor_custo;
                        db.SaveChanges();
                        Tuple<bool, int> resposta = new Tuple<bool, int>(true, ing.id);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarIngrediente(int id_ingrediente)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    INGREDIENTE ingrediente = db.INGREDIENTES.Include(f => f.FORNECEDOR_INGREDIENTE.Select(e => e.FORNECEDOR)).FirstOrDefault(e => e.id == id_ingrediente);
                    if (ingrediente != null)
                    {

                        List<object> forne = new List<object>();
                        foreach (var fornecedor in ingrediente.FORNECEDOR_INGREDIENTE)
                        {
                            forne.Add(new
                            {
                                id_fornecedor = fornecedor.id_fornecedor,
                                razao_social = fornecedor.FORNECEDOR.razao_social
                            });
                        }

                        object ing = new
                        {
                            isOk = true,
                            id_unidade_medida = ingrediente.id_unidade_medida,
                            marca = ingrediente.marca,
                            quantidade_embalagem = ingrediente.quantidade_embalagem,
                            valor_custo = ingrediente.valor_custo,
                            fornecedores = forne
                        };
                        return Json(ing, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                object ing = new { isOk = false };
                return Json(ing, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarItemJSON(ITENS_CARDAPIO item, int? id_item)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id_item == null)
                    {
                        item.id_empresa = base.ID_EMPRESA;
                        item.status = true;
                        db.ITENS_CARDAPIO.Add(item);
                    }
                    else
                    {
                        ITENS_CARDAPIO item_e = db.ITENS_CARDAPIO.Include(it => it.ITENS_INGREDIENTES).FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA);
                        if (item_e != null)
                        {
                            item_e.custo_pessoa = item.custo_pessoa;
                            item_e.descricao = item.descricao;
                            item_e.id_categoria = item.id_categoria;
                            item_e.id_sub_categoria = item.id_sub_categoria;
                            item_e.id_unidade_medida = item.id_unidade_medida;
                            item_e.margem_lucro = item.margem_lucro;
                            item_e.nome = item.nome;
                            item_e.quantidade_embalagem = item.quantidade_embalagem;
                            item_e.receita = item.receita;
                            item_e.rendimento = item.rendimento;
                            item_e.valor_compra = item.valor_compra;
                            item_e.venda_pessoa = item.venda_pessoa;

                            //MONTAR ITEM
                            List<ITENS_INGREDIENTES> ingredientes = db.ITENS_INGREDIENTES.Where(e => e.id_cardapio == item_e.id && e.id_empresa == ID_EMPRESA).ToList();
                            if (ingredientes.Count != 0)
                            {
                                db.ITENS_INGREDIENTES.RemoveRange(ingredientes);
                            }

                            //ITEM PRONTO
                            else
                            {
                                db.FORNECEDOR_CARDAPIO.RemoveRange(db.FORNECEDOR_CARDAPIO.Where(e => e.id_cardapio == (int)id_item && e.id_empresa == ID_EMPRESA).ToList());
                            }
                        }
                    }
                    db.SaveChanges();
                    if (id_item != null)
                    {
                        Tuple<bool, int> resposta = new Tuple<bool, int>(true, (int)id_item);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        Tuple<bool, int> resposta = new Tuple<bool, int>(true, item.id);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarFornecedorItemProntoJSON(int id_cardapio, int id_fornecedor)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.FORNECEDOR_CARDAPIO.Add(new FORNECEDOR_CARDAPIO()
                    {
                        id_empresa = base.ID_EMPRESA,
                        id_fornecedor = id_fornecedor,
                        id_cardapio = id_cardapio
                    });
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarCategoriaJSON(CATEGORIA_ITENS categoria, int? id_categoria)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id_categoria == null)
                    {
                        categoria.id_empresa = base.ID_EMPRESA;
                        db.CATEGORIA_ITENS.Add(categoria);
                    }
                    else
                    {
                        CATEGORIA_ITENS c = db.CATEGORIA_ITENS.FirstOrDefault(e => e.id == id_categoria && e.id_empresa == ID_EMPRESA);
                        if (c != null)
                        {
                            c.nome = categoria.nome;
                            c.descricao = categoria.descricao;
                        }
                    }
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, categoria.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarSubCategoriaJSON(SUB_CATEGORIA_ITENS subCategoria, int? id_subcategoria)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id_subcategoria == null)
                    {
                        subCategoria.id_empresa = base.ID_EMPRESA;
                        db.SUB_CATEGORIA_ITENS.Add(subCategoria);
                    }
                    else
                    {
                        SUB_CATEGORIA_ITENS sub = db.SUB_CATEGORIA_ITENS.FirstOrDefault(e => e.id == id_subcategoria && e.id_empresa == ID_EMPRESA);
                        if (sub != null)
                        {
                            sub.id_categoria_itens = subCategoria.id_categoria_itens;
                            sub.id_unidade_medida = subCategoria.id_unidade_medida;
                            sub.nome = subCategoria.nome;
                            sub.rendimento_pessoa = subCategoria.rendimento_pessoa;
                        }
                    }
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, subCategoria.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarSubCategoria(int id_categoria)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<SubCategoriaModel> sub = new List<SubCategoriaModel>();
                    List<SUB_CATEGORIA_ITENS> sub_categorias = db.SUB_CATEGORIA_ITENS.Where(s => s.id_categoria_itens == id_categoria && s.id_empresa == ID_EMPRESA).ToList();
                    foreach (var sub_categoria in sub_categorias)
                    {
                        sub.Add(new SubCategoriaModel()
                        {
                            id_sub_categoria = sub_categoria.id,
                            nome_subcategoria = sub_categoria.nome,
                            rendimento_pessoa = sub_categoria.rendimento_pessoa,
                            id_categoria_itens = sub_categoria.id_categoria_itens,
                            id_unidade_medida = sub_categoria.id_unidade_medida
                        });
                    }
                    return Json(sub, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarSubCategoriaEditar(int id_subcategoria)
        {
            using (BuffetContext db = new BuffetContext())
            {
                SUB_CATEGORIA_ITENS sub = db.SUB_CATEGORIA_ITENS.FirstOrDefault(e => e.id == id_subcategoria);
                object cat = new
                {
                    isOk = true,
                    id = id_subcategoria,
                    nome = sub.nome,
                    rendimento_pessoa = sub.rendimento_pessoa,
                    id_unidade_medida = sub.id_unidade_medida,
                    id_categoria = sub.id_categoria_itens
                };
                return Json(cat, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarCategoriaEditar(int id_categoria)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CATEGORIA_ITENS sub = db.CATEGORIA_ITENS.FirstOrDefault(e => e.id == id_categoria);
                object cat = new
                {
                    isOk = true,
                    id = id_categoria,
                    nome = sub.nome,
                    opcao = sub.descricao,
                    url_image = sub.url_image
                };
                return Json(cat, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarItemEditar(int id_item)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ITENS_CARDAPIO item = db.ITENS_CARDAPIO.Include(it => it.ITENS_INGREDIENTES.Select(e => e.INGREDIENTE)).Include(f => f.FORNECEDOR_CARDAPIO).FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA);

                List<FORNECEDOR> forn = db.FORNECEDORs.Where(e => e.id_empresa == ID_EMPRESA).ToList();
                if (item != null)
                {
                    bool isItemPronto = item.ITENS_INGREDIENTES.Count == 0;

                    List<object> forne = new List<object>();
                    foreach (var fornecedor in item.FORNECEDOR_CARDAPIO)
                    {
                        forne.Add(new
                        {
                            id_fornecedor = fornecedor.id_fornecedor,
                            razao_social = forn.FirstOrDefault(f => f.id == fornecedor.id_fornecedor).razao_social
                        });
                    }

                    List<object> ingredientes = new List<object>();
                    foreach (var ingerdiente in item.ITENS_INGREDIENTES)
                    {
                        ingredientes.Add(new
                        {
                            id_ingrediente = ingerdiente.id_ingrediente,
                            quantidade = ingerdiente.quantidade,
                            valor_atual = ingerdiente.valor_atual,
                            marca = ingerdiente.INGREDIENTE.marca,
                        });
                    }


                    List<SUB_CATEGORIA_ITENS> sub_categorias = db.SUB_CATEGORIA_ITENS.Where(e => e.id_categoria_itens == item.id_categoria).ToList();
                    List<object> sub_categoria = new List<object>();
                    foreach (var subg in sub_categorias)
                    {
                        sub_categoria.Add(new
                        {
                            id_sub_categoria = subg.id,
                            nome = subg.nome
                        });
                    }

                    object it = new
                    {
                        isOk = true,
                        isItemPronto = isItemPronto,
                        nome = item.nome,
                        id_categoria = item.id_categoria,
                        status = item.status,
                        descricao = item.descricao,
                        valor_compra = item.valor_compra,
                        id_unidade_medida = item.id_unidade_medida,
                        rendimento = item.rendimento,
                        receita = item.receita,
                        url_image = item.url_image,
                        id_sub_categoria = item.id_sub_categoria,
                        quantidade_embalagem = item.quantidade_embalagem,
                        custo_pessoa = item.custo_pessoa,
                        margem_lucro = item.margem_lucro,
                        venda_pessoa = item.venda_pessoa,
                        fornecedores = forne,
                        ingredientes = ingredientes,
                        sub_categorias = sub_categoria
                    };
                    return Json(it, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SalvarIngredienteMontarItemJSON(int id_item, int id_ingrediente, int quantidade)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.ITENS_INGREDIENTES.Add(new ITENS_INGREDIENTES()
                    {
                        id_empresa = base.ID_EMPRESA,
                        id_cardapio = id_item,
                        quantidade = quantidade,
                        valor_atual = (Decimal)db.ITENS_CARDAPIO.FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA).valor_compra,
                        id_ingrediente = id_ingrediente
                    });
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizarStatusItemEditar(int id_item, string status)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_CARDAPIO item = db.ITENS_CARDAPIO.FirstOrDefault(e => e.id == id_item);
                    if (item != null)
                    {
                        item.status = status.Equals("Ativo") ? true : false;
                        db.SaveChanges();
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }





        public JsonResult SalvarCardapioJSON(PACOTE pacote)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    pacote.id_empresa = base.ID_EMPRESA;
                    pacote.status = true;
                    pacote.data_criacao = DateTime.Now.Date;
                    db.PACOTES.Add(pacote);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, pacote.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPacoteCategoriaJSON(PACOTE_CATEGORIA_ITENS categoria)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    categoria.id_empresa = base.ID_EMPRESA;
                    db.PACOTE_CATEGORIA_ITENS.Add(categoria);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, categoria.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPacoteItemJSON(PACOTES_ITENS itens)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    itens.id_empresa = base.ID_EMPRESA;
                    db.PACOTES_ITENS.Add(itens);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, itens.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DetalhesFornecedor(int id_fornecedor)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.FORNECEDORs.FirstOrDefault(f => f.id == id_fornecedor && f.id_empresa == ID_EMPRESA));
            }
        }

        public ActionResult Ingredientes(bool? deCardapio)
        {
            base.getMensagem();
            ViewBag.deCardapio = deCardapio;
            return View(new ListarIngredientes() { id_empresa = base.ID_EMPRESA });
        }

        //public ActionResult SalvarIngrediente(INGREDIENTE ingrediente, ICollection<FornecedorSelecionado> fornecedor, bool? deCardapio)
        //{
        //    if (base.isLogin())
        //    {
        //        if (base.perfil.c_estoque)
        //        {
        //            if (ActionsEstoque.SalvarIngrediente(ingrediente, fornecedor, base.ID_EMPRESA).Item1)
        //            {
        //                base.setMensagem("Ingrediente salvo com sucesso", tipoMensagem.tipoSucesso);
        //                if (deCardapio == true)
        //                {
        //                    return RedirectToAction("ItensCardapio", "Itens");
        //                }
        //            }
        //            else
        //                base.setMensagem("Ingrediente não foi salvo", tipoMensagem.tipoErro);

        //            return RedirectToAction("Ingredientes");
        //        }
        //        return base.NotAcess;
        //    }
        //    return base.LoginView;
        //}

        public JsonResult UpLoadImagemItemCardapio(int id_item, string pasta)
        {
            var pic = System.Web.HttpContext.Current.Request.Files["Image"];
            Tuple<bool, string> resposta = new FTPManager(ID_EMPRESA).uploadImage(ImageFolder.ItemCardapio, id_item.ToString(), pic.InputStream);
            if (resposta.Item1)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_CARDAPIO cardapio = db.ITENS_CARDAPIO.FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA);
                    if (cardapio != null)
                    {
                        cardapio.url_image = resposta.Item2;
                        db.SaveChanges();
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpLoadImagemCardapio(int id_item, string pasta)
        {
            var pic = System.Web.HttpContext.Current.Request.Files["Image"];
            Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Cardapio, id_item.ToString(), pic.InputStream);
            if (resposta.Item1)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    PACOTE cardapio = db.PACOTES.FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA);
                    if (cardapio != null)
                    {
                        cardapio.url_image = resposta.Item2;
                        db.SaveChanges();
                    }
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}
