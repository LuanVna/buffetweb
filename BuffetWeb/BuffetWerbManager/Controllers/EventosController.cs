﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebManager.Models.Eventos;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class EventosController : BaseController
    {
        public ActionResult TipoDeEventos()
        {
            ViewBag.c_tipo_evento = base.perfil.c_tipo_eventos;
            base.getMensagem();
            return View(new NovoTipoEvento() { id_empresa = base.ID_EMPRESA });
        }

        public JsonResult SalvarTipoEventoJSON(EVENTO evento)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    evento.id_empresa = base.ID_EMPRESA;
                    evento.status = true;
                    db.EVENTOes.Add(evento);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, evento.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarAluguelJSON(EVENTO_ALUGUEL aluguel)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    aluguel.id_empresa = base.ID_EMPRESA;
                    db.EVENTO_ALUGUEL.Add(aluguel);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, aluguel.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarDiversosJSON(EVENTO_DIVERSOS diversos)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    diversos.id_empresa = base.ID_EMPRESA;
                    db.EVENTO_DIVERSOS.Add(diversos);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, diversos.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarServicosJSON(EVENTO_SERVICOS servicos)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    servicos.id_empresa = base.ID_EMPRESA;
                    db.EVENTO_SERVICOS.Add(servicos);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, servicos.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarCamposDisponiveisJSON(CAMPOS_DISPONIVEIS campos)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    campos.id_empresa = base.ID_EMPRESA;
                    db.CAMPOS_DISPONIVEIS.Add(campos);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, -1);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarCardapioJSON(EVENTOS_PACOTES cardapios)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    cardapios.id_empresa = base.ID_EMPRESA;
                    db.EVENTOS_PACOTES.Add(cardapios);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, -1);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarPrestadoresJSON(EVENTO_PRESTADORES prestador)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    prestador.id_empresa = base.ID_EMPRESA;
                    db.EVENTO_PRESTADORES.Add(prestador);
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SalvarImagemEventos(int id_item)
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["Image"];
                Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.TipoEvento, id_item.ToString(), pic.InputStream);
                if (resposta.Item1)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        EVENTO evento = db.EVENTOes.FirstOrDefault(e => e.id == id_item && e.id_empresa == ID_EMPRESA);
                        if (evento != null)
                        {
                            evento.url = resposta.Item2;
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult SalvarTipoEvento(TIPO_EVENTO evento, /*List<OpcoesSelecionadas> sequencia,*/ List<DadosSelecionados> dados, List<PacotesSelecionados> pacotes, HttpPostedFileBase image, int? id)
        //{
        //    if (base.isLogin())
        //    {
        //        if (base.perfil.c_eventos)
        //        {
        //            if (ActionsEventos.SalvarTipoEvento(evento, /*sequencia,*/ dados, pacotes, image.InputStream, id, base.ID_EMPRESA))
        //                base.setMensagem("", tipoMensagem.tipoSucesso);
        //            else
        //                base.setMensagem("", tipoMensagem.tipoErro);

        //            return RedirectToAction("TipoDeEventos");
        //        }
        //        return base.NotAcess;
        //    }
        //    return base.LoginView;
        //}

        //public ActionResult DetalhesEvento(int? id_evento)
        //{
        //    if (base.isLogin())
        //    {
        //        if (base.perfil.c_eventos)
        //        {
        //            if (id_evento != null)
        //            {
        //                return View(new ModelTipoEventoDetalhes() { id_event = (int)id_evento, id_empresa = base.ID_EMPRESA });
        //            }
        //            return RedirectToAction("TipoDeEventos");
        //        }
        //        return base.NotAcess;
        //    }
        //    return base.LoginView;
        //}
    }
}
