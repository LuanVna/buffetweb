﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebManager.Controllers;
using System.Data.Entity;
using BuffetWebManager.Models.Financeiro;
using System.Web.Routing;
using BuffetWebManager.Models;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class FinanceiroController : BaseController
    {
        //
        // GET: /Financeiro/ 

        public ActionResult Financeiro()
        {
            return View();
        }

        public ActionResult AdicionarPagamento(string codigo_unico)
        {
            if (codigo_unico != null)
            {
                ViewBag.codigo_unico = codigo_unico;
                using (BuffetContext db = new BuffetContext())
                {
                    return View(db.ORCAMENTOes.Include(cl => cl.CLIENTE).Include(f => f.FINANCEIRO_P_CREDITO).Include(f => f.FINANCEIRO_P_DEBITO).FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico) && e.id_empresa == ID_EMPRESA));
                }
            }
            else
            {
                return RedirectToAction("Orcamentos", "Orcamento", new RouteValueDictionary(new { deFinanceiro = true }));
            }
        }

        public ActionResult AdicionarEspecie(FINANCEIRO_P_ESPECIE especie, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (cola != null)
            {
                if (codigo_unico != null && base.perfil.c_financeiro)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        especie.id_colaborador = cola.id;
                        especie.id_empresa = base.ID_EMPRESA;
                        especie.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                        db.FINANCEIRO_P_ESPECIE.Add(especie);
                        db.SaveChanges();
                    }
                    return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                }
            }
            return base.LoginView;
        }

        public ActionResult AdicionarCredito(FINANCEIRO_P_CREDITO cartao, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (codigo_unico != null && cola != null)
            {
                if (base.perfil.c_financeiro)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        cartao.id_colaborador = cola.id;
                        cartao.id_empresa = base.ID_EMPRESA;
                        cartao.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                        db.FINANCEIRO_P_CREDITO.Add(cartao);
                        db.SaveChanges();
                    }
                    return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                }
            }
            return base.LoginView;
        }

        public ActionResult AdicionarDebito(FINANCEIRO_P_DEBITO debito, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (codigo_unico != null && cola != null)
            {
                if (base.perfil.c_financeiro)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        debito.id_colaborador = cola.id;
                        debito.id_empresa = base.ID_EMPRESA;
                        debito.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                        db.FINANCEIRO_P_DEBITO.Add(debito);
                        db.SaveChanges();
                    }
                    return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                }
            }
            return base.LoginView;
        }

        public ActionResult AdicionarCheque(FINANCEIRO_P_CHEQUE cheque, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (codigo_unico != null && cola != null)
            {
                if (base.perfil.c_financeiro)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        cheque.id_colaborador = cola.id;
                        cheque.id_empresa = base.ID_EMPRESA;
                        cheque.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                        db.FINANCEIRO_P_CHEQUE.Add(cheque);
                        db.SaveChanges();
                    }
                    return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                }
            }
            return base.LoginView;
        }

        public ActionResult AdicionarBoleto(FINANCEIRO_P_BOLETO boleto, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (cola != null)
            {
                if (base.perfil.c_financeiro)
                {
                    if (codigo_unico != null)
                    {
                        using (BuffetContext db = new BuffetContext())
                        {
                            boleto.id_colaborador = cola.id;
                            boleto.id_empresa = base.ID_EMPRESA;
                            boleto.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                            db.FINANCEIRO_P_BOLETO.Add(boleto);
                            db.SaveChanges();
                        }
                        return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                    }
                }
            }
            return base.LoginView;
        }

        public ActionResult AdicionarDeposito(FINANCEIRO_P_DEPOSITO deposito, string codigo_unico)
        {
            COLABORADORE cola = base.colaborador;
            if (cola != null)
            {
                if (base.perfil.c_financeiro)
                {
                    if (codigo_unico != null)
                    {
                        using (BuffetContext db = new BuffetContext())
                        {
                            deposito.id_colaborador = cola.id;
                            deposito.id_empresa = base.ID_EMPRESA;
                            deposito.id_orcamento = db.ORCAMENTOes.FirstOrDefault(e => e.codigo_unico.Equals(codigo_unico)).id;
                            db.FINANCEIRO_P_DEPOSITO.Add(deposito);
                            db.SaveChanges();
                        }
                        return RedirectToAction("AdicionarPagamento", new RouteValueDictionary(new { codigo_unico = codigo_unico }));
                    }
                }
            }
            return base.LoginView;
        }


        //public ActionResult FormasDePagamento()
        //{
        //    formasDePagamento pagamento = new formasDePagamento();
        //    //CIELO
        //    pagamento.cielo = PerfilEmpresa.PAGAMENTO_CREDENCIAL_CIELO_ATIVO;
        //    pagamento.credencial_cielo = PerfilEmpresa.PAGAMENTO_CREDENCIAL_CIELO;

        //    //BANCO DO BRASIL 
        //    pagamento.brasil = PerfilEmpresa.PAGAMENTO_CREDENCIAL_BRASIL_ATIVO;
        //    pagamento.codigo_banco_brasil = PerfilEmpresa.PAGAMENTO_CREDENCIAL_BRASIL;

        //    //BOLETO 
        //    pagamento.boleto = PerfilEmpresa.PAGAMENTO_BOLETO_CODIGO_ATIVO;
        //    pagamento.boleto_codigo = PerfilEmpresa.PAGAMENTO_BOLETO_CODIGO;
        //    pagamento.boleto_agencia = PerfilEmpresa.PAGAMENTO_BOLETO_AGENCIA;
        //    pagamento.boleto_conta = PerfilEmpresa.PAGAMENTO_BOLETO_CONTA;

        //    return View(pagamento);
        //} 
    }
}
