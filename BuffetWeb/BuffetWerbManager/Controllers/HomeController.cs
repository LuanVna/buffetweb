﻿using BuffetWebManager.Controllers;
using BuffetWebManager.Models.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebManager.Models;
using BuffetWebData.Models;
using BuffetWebManager.Models.Shared;
using System.Web.Routing;
using System.Data.Entity;
using BuffetWebData.Utils;

namespace BuffetWebManager.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            return View(new Index() { id_empresa = base.ID_EMPRESA });
        }

        [AllowAnonymous]
        public ActionResult EntrarAdministrativo(string email, string senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                senha = RLSecurity.Security.CriptographyOn(senha);
                COLABORADORE cola = db.COLABORADORES.Include(em => em.EMPRESA).FirstOrDefault(c => c.email.Equals(email) && c.senha.Equals(senha));
                if (cola != null)
                {
                    string rSenha = RLSecurity.Security.CriptographyOn(cola.EMPRESA.nome.ToLower().Replace(" ", "").ToLower());
                    if (rSenha == senha)
                    {
                        return RedirectToAction("ResetarSenha", new RouteValueDictionary(new { email = cola.email, token = cola.EMPRESA.codigo_empresa }));
                    }
                    else
                    {
                        base.Entrar(email, senha);
                        //var url = HttpUtility.ParseQueryString(Request.UrlReferrer.Query).GetValues("ReturnUrl")[0];
                        //if (url != null)
                        //    return Redirect(url);
                        return RedirectToAction("Index");
                    }
                }
            }
            base.setMensagem("Email ou Senha invalido!");
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult ResetarSenha(string email, string token)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var em = db.EMPRESAs.FirstOrDefault(e => e.codigo_empresa.Equals(token));
                if (em != null)
                {
                    ViewBag.email = email;
                    ViewBag.token = token;
                    return View();
                }
                base.setMensagem("Informações não encontradas, tente novamente!");
                return RedirectToAction("Index");
            }
        }

        [AllowAnonymous]
        public ActionResult ConfirmarResetar(string senha, string email, string token)
        {
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADORE cola = db.COLABORADORES.Include(e => e.EMPRESA).FirstOrDefault(c => c.email.Equals(email) && c.EMPRESA.codigo_empresa.Equals(token));
                if (cola != null)
                {
                    string rSenha = RLSecurity.Security.CriptographyOn(cola.EMPRESA.nome.ToLower().Replace(" ", "").ToLower());
                    if (rSenha == cola.senha)
                    {
                        cola.senha = RLSecurity.Security.CriptographyOn(senha);
                        db.SaveChanges();

                        base.Entrar(email, senha);
                        return RedirectToAction("Index");
                    }
                    return RedirectToAction("ResetarSenha", new RouteValueDictionary(new { email = cola.email }));
                }
                return RedirectToAction("Index");
            }
        }

        [AllowAnonymous]
        public JsonResult EsqueciMinhaSenha(string email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                COLABORADORE cola = db.COLABORADORES.Include(f => f.EMPRESA).FirstOrDefault(e => e.email.Equals(email));
                if (cola != null)
                {
                    string senha = new Random().Next(0, 99999).ToString();
                    cola.senha = RLSecurity.Security.CriptographyOn(senha);
                    COLABORADORE co = db.COLABORADORES.FirstOrDefault(f => f.id_empresa == cola.id_empresa && f.administrador == true);
                    bool send = new UtilsManager(cola.EMPRESA.id).sendEmail("Renovar senha", string.Format("Você solicitou uma renovação de senha! \nAqui esta sua nova senha: {0}{1}", senha, cola.id != co.id ? ".\n\n\n\n Caso queira uma senha própria você deve pedir ao seu superior imediato que resete sua senha a partir do painel administrativo." : ""), cola.email);
                    if (send)
                    {
                        if (co != null && co.id != cola.id)
                        {
                            new UtilsManager(co.id_empresa).sendEmail("Renovação de senha", string.Format("{0} acaba de renovar sua senha", cola.nome), co.email);
                        }
                        return Json(new { isOk = true, mensagem = "Uma nova senha foi enviada para seu e-mail!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { isOk = true, mensagem = "Ouve algum erro, atualize a página e tente novamente!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { isOk = true, mensagem = "Usuario não encontrado!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string email, string m, string De)
        {
            if (BWColaborador.sessao_alterada)
            {
                base.setMensagem("Sua conta foi acessada em outro navegador!");
            }
            ViewBag.email = email;
            ViewBag.De = De;
            if (m != null)
            {
                switch (m)
                {
                    case "UE": base.setMensagem("O usuario administrador já foi criado, clique em esqueci minha senha caso queira redefir!"); break;
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Cadastrar()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmarCadastro(string email, string celular, string nome, string senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                senha = RLSecurity.Security.CriptographyOn(senha);
                B_POSSIVEL_PARCEIRO p = db.B_POSSIVEL_PARCEIRO.FirstOrDefault(e => e.email.Equals(email) && e.celular.Equals(celular) && e.nome.Equals(nome) && e.senha.Equals(senha));
                if (p == null)
                {
                    B_POSSIVEL_PARCEIRO pp = new B_POSSIVEL_PARCEIRO()
                     {
                         nome = nome,
                         celular = celular,
                         desde = DateTime.Today,
                         email = email,
                         senha = senha,
                         status = "Cadastro realizado"
                     };
                    db.SaveChanges();
                    return View(pp);
                }
                return View(p);
            }
        }

        [AllowAnonymous]
        public ActionResult CriarAcesso(string autenticacao)
        {
            if (!string.IsNullOrEmpty(autenticacao))
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var em = db.EMPRESAs.Include(co => co.COLABORADORES).Include(p => p.CLIENTE_PROPOSTA).FirstOrDefault(e => e.codigo_empresa == autenticacao);
                    if (em != null)
                    {
                        if (em.COLABORADORES.Count == 0)
                        {
                            ViewBag.autenticacao = autenticacao;
                            ViewBag.propostas = em.CLIENTE_PROPOSTA.Count;
                            return View(em);
                        }
                        return RedirectToAction("Login", new RouteValueDictionary(new { m = "UE" }));
                    }
                    return RedirectToAction("Login");
                }
            }
            return Redirect("Login");
        }

        public ActionResult ConfirmarAcesso(string autenticacao, string nome, string celular, string email, string senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var em = db.EMPRESAs.Include(co => co.COLABORADORES).Include(p => p.PERFIL_COLABORADOR).FirstOrDefault(e => e.codigo_empresa == autenticacao);
                if (em.COLABORADORES.Count == 0)
                {
                    try
                    {
                        var colaborador = db.COLABORADORES.FirstOrDefault(e => e.email.Equals(email));
                        if (colaborador == null)
                        {
                            db.COLABORADORES.Add(new COLABORADORE()
                            {
                                nome = nome,
                                email = email,
                                senha = RLSecurity.Security.CriptographyOn(senha),
                                administrador = true,
                                sessao = HttpContext.Session.SessionID,
                                celular = celular,
                                id_perfil = em.PERFIL_COLABORADOR.FirstOrDefault().id,
                                id_empresa = em.id,
                            });
                            db.SaveChanges();
                            return RedirectToAction("EntrarAdministrativo", new RouteValueDictionary(new { email = email, senha = senha }));
                        }
                    }
                    catch (Exception e)
                    {

                    }
                    base.setMensagem("Este e-mail já esta sendo utilizado!");
                    return RedirectToAction("CriarAcesso", new RouteValueDictionary(new { autenticacao = em.codigo_empresa }));
                }
                return RedirectToAction("Login", new RouteValueDictionary(new { m = "UE" }));
            }
        }

        public JsonResult VerificaEmail(string email)
        {
            using (BuffetContext db = new BuffetContext())
            {
                bool existe = db.COLABORADORES.FirstOrDefault(e => e.email.Equals(email)) != null;
                return Json(new { isOk = true, existe = existe }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CriarBuffet()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult CadastrarBuffet(string cnpj, string nome, string email, string senha)
        {
            var novaEmpresa = new UtilsEmpresa().CriarEmpresa(cnpj);

            using (BuffetContext db = new BuffetContext())
            {

            }

            return View();
        }

        public ActionResult Deslogar()
        {
            base.Sair();
            return RedirectToAction("Login");
        }
    }
}
