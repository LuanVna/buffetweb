﻿using BuffetWebManager.Controllers;
using BuffetWebManager.Models.Galeria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebManager.Models;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class ImagensController : BaseController
    {
        //
        // GET: /Galeria/ 
        public ActionResult ImagensEventos()
        {
            base.getMensagem();
            return View();
        }

        public ActionResult SalvarGaleriaEvento(IMAGENS_GALERIA_EVENTOS galeriaEvento)
        {
            //if (ActionsImagens.SalvarGaleriaEvento(galeriaEvento, base.ID_EMPRESA))
            //    base.setMensagem("Galeria criada com sucesso!");

            return RedirectToAction("ImagensEventos");
        }

        public ActionResult GaleriaInicial()
        {
            return View(new PaginaInicial() { id_empresa = base.ID_EMPRESA });
        }

        public ActionResult SalvarGaleriaInicial(IEnumerable<HttpPostedFileBase> imagens)
        {
            using (BuffetContext db = new BuffetContext())
            {
                FTPManager imageUpload = new FTPManager(base.ID_EMPRESA);
                foreach (HttpPostedFileBase imagem in imagens)
                {
                    if (imagem != null)
                    {
                        //if (imageUpload.uploadImage("IMAGEM_GALERIA", imagem.FileName, imagem.InputStream))
                        //{
                        //    //db.IMAGENS_INICIAL.Add(new IMAGENS_INICIAL()
                        //    //{
                        //    //    nome = imagem.FileName
                        //    //});
                        //}
                    }
                }
                db.SaveChanges();
            }
            return RedirectToAction("GaleriaInicial");
        }
    }
}
