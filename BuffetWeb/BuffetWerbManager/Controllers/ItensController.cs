﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebManager.Models;
using BuffetWebManager.Models.Utils;
using BuffetWebManager.Models.Itens;
using BuffetWebManager.Models.Itens.Lista;

using BuffetWebManager.Models.Orcamento;
using BuffetWebManager.Models.Estoque;
using System.Web.Routing;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class ItensController : BaseController
    {
        public int id_empresa { get; set; }

        public ActionResult Itens()
        {
            base.getMensagem();
            return View(new NovoItem() { id_empresa = base.ID_EMPRESA });
        }

        public ActionResult SalvarCardapio(ITENS_CARDAPIO cardapio, ICollection<FornecedorSelecionado> fornecedores, List<IngredientesCardapio> ingredientes, HttpPostedFileBase image, int? id, string tipo_item)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    cardapio.id_empresa = ID_EMPRESA;
                    if (fornecedores.Where(f => f.id_fornecedor != 0).FirstOrDefault() == null)
                    {
                        List<INGREDIENTE> ingred = db.INGREDIENTES.ToList();
                        //ITEM MONTADO
                        List<ITENS_INGREDIENTES> ingre = new List<ITENS_INGREDIENTES>();
                        foreach (var ingrediente in ingredientes.Where(f => f.id_ingrediente != 0).ToList())
                        {
                            cardapio.ITENS_INGREDIENTES.Add(new ITENS_INGREDIENTES()
                            {
                                id_ingrediente = ingrediente.id_ingrediente,
                                id_cardapio = cardapio.id,
                                id_empresa = ID_EMPRESA,
                                valor_atual = ingred.FirstOrDefault(i => i.id == ingrediente.id_ingrediente).id
                            });
                        }
                    }
                    else
                    {
                        //ITEM PRONTO
                        foreach (var fornecedor in fornecedores)
                        {
                            cardapio.FORNECEDOR_CARDAPIO.Add(new FORNECEDOR_CARDAPIO()
                            {
                                id_cardapio = cardapio.id,
                                id_empresa = ID_EMPRESA,
                                id_fornecedor = fornecedor.id_fornecedor
                            });
                        }
                    }

                    db.ITENS_CARDAPIO.Add(cardapio);
                    db.SaveChanges();
                    if (image != null)
                    {
                        Tuple<bool, string> resposta = new FTPManager(ID_EMPRESA).uploadImage(ImageFolder.Cardapio, cardapio.id.ToString(), image.InputStream);
                        if (resposta.Item1)
                        {
                            db.IMAGENS_ITEM.Add(new IMAGENS_ITEM()
                            {
                                pasta = "Cardapio",
                                url = resposta.Item2,
                                nome = cardapio.id.ToString(),
                                id_empresa = ID_EMPRESA
                            });
                            db.SaveChanges();
                        }
                    }
                }
                base.setMensagem("Item salvo", tipoMensagem.tipoSucesso);
            }
            catch (Exception e)
            {
                base.setMensagem("Erro ao tentar cadastrar item", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensCardapio", new RouteValueDictionary(new { tipo_item = tipo_item }));
        }

        public ActionResult ApagarItemCardapio(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_CARDAPIO al = db.ITENS_CARDAPIO.Where(i => i.id == id).FirstOrDefault();
                    db.ITENS_CARDAPIO.Remove(al);
                    db.SaveChanges();
                    base.setMensagem("Cardapio Apagado", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Cardapio", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensCardapio");
        }


        public ActionResult ItensDiversos()
        {
            base.getMensagem();
            return View(new ItensDiversos() { id_empresa = base.ID_EMPRESA });
        }
        public ActionResult SalvarItemDiverso(ITENS_DIVERSOS itens_diverso, HttpPostedFileBase image, int? id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id == null)
                    {
                        itens_diverso.id_empresa = base.ID_EMPRESA;
                        db.ITENS_DIVERSOS.Add(itens_diverso);
                    }
                    else
                    {
                        ITENS_DIVERSOS itd = new ITENS_DIVERSOS();
                        itd.descricao = itens_diverso.descricao;
                        itd.status = true;
                        itd.id_categoria = itens_diverso.id_categoria;
                        itd.nome = itens_diverso.nome;
                        itd.valor_venda = itens_diverso.valor_venda;
                        itd.valor_custo = itens_diverso.valor_custo;
                    }
                    db.SaveChanges();
                    Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Diversos, itens_diverso.id.ToString(), image.InputStream);
                    db.IMAGENS_ITEM.Add(new IMAGENS_ITEM()
                    {
                        id_empresa = base.ID_EMPRESA,
                        nome = itens_diverso.id.ToString(),
                        pasta = "Diversos",
                        url = resposta.Item2
                    });
                    db.SaveChanges();
                    base.setMensagem("Item diverso salvo com sucesso!", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Erro diverso", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensDiversos");
        }

        public ActionResult ApagarItemDiverso(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_DIVERSOS al = db.ITENS_DIVERSOS.Where(i => i.id == id).FirstOrDefault();
                    if (al != null)
                    {
                        db.ITENS_DIVERSOS.Remove(al);
                        db.SaveChanges();
                        base.setMensagem("Servico apagado", tipoMensagem.tipoSucesso);
                    }
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Servico", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensDiversos");
        }

        public ActionResult ItensServico()
        {
            base.getMensagem();
            return View(new ItensServico() { id_empresa = base.ID_EMPRESA });
        }

        public ActionResult SalvarItemServico(ITENS_SERVICOS itens_servico, List<PrestadoresSelecionados> prestadores, HttpPostedFileBase image, int? id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (id == null)
                    {
                        itens_servico.id_empresa = base.ID_EMPRESA;
                        db.ITENS_SERVICOS.Add(itens_servico);

                        foreach (var prestador in prestadores.Where(p => p.id_prestador != 0).ToList())
                        {
                            db.PRESTADOR_SERVICO.Add(new PRESTADOR_SERVICO()
                            {
                                id_empresa = base.ID_EMPRESA,
                                id_prestador_funcao = prestador.id_prestador,
                                id_servico = itens_servico.id
                            });
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        ITENS_SERVICOS s = db.ITENS_SERVICOS.Where(i => i.id == id).FirstOrDefault();
                        if (s != null)
                        {
                            s.id = itens_servico.id;
                            s.nome = itens_servico.nome;
                            s.valor_custo = itens_servico.valor_custo;
                            s.valor_venda = itens_servico.valor_venda;
                            s.descricao = itens_servico.descricao;
                            s.id_categoria = itens_servico.id_categoria;
                        }
                    }
                    db.SaveChanges();
                    if (image != null)
                    {
                        Tuple<bool, string> resposta = new FTPManager(ID_EMPRESA).uploadImage(ImageFolder.Aluguel, itens_servico.id.ToString(), image.InputStream);
                        if (resposta.Item1)
                        {
                            db.IMAGENS_ITEM.Add(new IMAGENS_ITEM()
                            {
                                id_empresa = base.ID_EMPRESA,
                                nome = itens_servico.id.ToString(),
                                pasta = "Servico",
                                url = resposta.Item2
                            });
                            db.SaveChanges();
                        }
                    }
                    base.setMensagem("Servico salvo com sucesso!", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Servico", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensServico");
        }

        public ActionResult ApagarItemServico(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_SERVICOS al = db.ITENS_SERVICOS.Where(i => i.id == id).FirstOrDefault();
                    if (al != null)
                    {
                        db.ITENS_SERVICOS.Remove(al);
                        db.SaveChanges();
                        base.setMensagem("Servico apagado", tipoMensagem.tipoSucesso);
                    }
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Servico", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensServico");
        }

        public ActionResult ItensAluguel()
        {
            return View(new ItensAluguel() { id_empresa = base.ID_EMPRESA });
        }

        public ActionResult SalvarItemAluguel(ITENS_ALUGUEL item_aluguel, HttpPostedFileBase image, List<FornecedorSelecionado> fornecedores, int? id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    item_aluguel.id_empresa = base.ID_EMPRESA;
                    if (id == null)
                    {
                        db.ITENS_ALUGUEL.Add(item_aluguel);

                        foreach (var fornecedor in fornecedores.Where(e => e.id_fornecedor != 0).ToList())
                        {
                            item_aluguel.FORNECEDOR_ALUGUEL.Add(new FORNECEDOR_ALUGUEL()
                            {
                                id_aluguel = item_aluguel.id,
                                id_empresa = base.ID_EMPRESA,
                                id_fornecedor = fornecedor.id_fornecedor
                            });
                        }
                    }
                    else
                    {
                        ITENS_ALUGUEL ia = db.ITENS_ALUGUEL.Where(i => i.id == id).FirstOrDefault();
                        if (ia != null)
                        {
                            ia.nome = item_aluguel.nome;
                            ia.valor_custo = item_aluguel.valor_custo;
                            ia.valor_venda = item_aluguel.valor_venda;
                        }
                    }
                    db.SaveChanges();
                    if (image != null)
                    {
                        Tuple<bool, string> resposta = new FTPManager(base.ID_EMPRESA).uploadImage(ImageFolder.Aluguel, item_aluguel.id.ToString(), image.InputStream);
                        if (resposta.Item1)
                        {
                            db.IMAGENS_ITEM.Add(new IMAGENS_ITEM()
                            {
                                id_empresa = base.ID_EMPRESA,
                                nome = item_aluguel.id.ToString(),
                                pasta = "Aluguel",
                                url = resposta.Item2
                            });
                            db.SaveChanges();
                        }
                    }
                    base.setMensagem("Item de Aluguel salvo com sucesso!", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Erro Aluguel", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensAluguel");
        }

        public ActionResult ApagarItemAluguel(int id)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ITENS_ALUGUEL al = db.ITENS_ALUGUEL.Where(i => i.id == id).FirstOrDefault();
                    db.ITENS_ALUGUEL.Remove(al);
                    db.SaveChanges();
                    base.setMensagem("Aluguel apagado", tipoMensagem.tipoSucesso);
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Aluguel", tipoMensagem.tipoErro);
            }
            return RedirectToAction("ItensAluguel");
        }

        public JsonResult SalvarItemAlguelJSON(ITENS_ALUGUEL itemAluguel)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    itemAluguel.id_empresa = base.ID_EMPRESA;
                    itemAluguel.status = true;
                    db.ITENS_ALUGUEL.Add(itemAluguel);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, itemAluguel.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarFornecedoresItemAlguelJSON(FORNECEDOR_ALUGUEL fornecedores)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    fornecedores.id_empresa = base.ID_EMPRESA;
                    db.FORNECEDOR_ALUGUEL.Add(fornecedores);
                    db.SaveChanges();
                    Tuple<bool> resposta = new Tuple<bool>(true);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarCategoriaAluguel(CATEGORIA_ALUGUEL categoriaAluguel)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    categoriaAluguel.id_empresa = base.ID_EMPRESA;
                    categoriaAluguel.descricao = "";
                    db.CATEGORIA_ALUGUEL.Add(categoriaAluguel);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, categoriaAluguel.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SalvarItemServicoJSON(ITENS_SERVICOS itemServico)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    itemServico.id_empresa = base.ID_EMPRESA;
                    itemServico.status = true;
                    db.ITENS_SERVICOS.Add(itemServico);
                    db.SaveChanges();
                    Tuple<bool, int> resposta = new Tuple<bool, int>(true, itemServico.id);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarPrestadorItemServicoJSON(PRESTADOR_SERVICO prestadores)
        {
            if (base.isLogin())
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        prestadores.id_empresa = base.ID_EMPRESA;
                        db.PRESTADOR_SERVICO.Add(prestadores);
                        db.SaveChanges();
                        Tuple<bool> resposta = new Tuple<bool>(true);
                        return Json(resposta, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception e)
                {
                    Tuple<bool, int> resposta = new Tuple<bool, int>(false, -1);
                }
            }
            return Json("reload", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarItemDiversosJSON(ITENS_DIVERSOS itensDiversos)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    itensDiversos.id_empresa = base.ID_EMPRESA;
                    db.ITENS_DIVERSOS.Add(itensDiversos);
                    db.SaveChanges();
                    Tuple<bool> resposta = new Tuple<bool>(true);
                    return Json(resposta, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Tuple<bool> resposta = new Tuple<bool>(false);
                return Json(resposta, JsonRequestBehavior.AllowGet);
            }
        }
    }
}