﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Controllers
{
    public class MarketingController : BaseController
    {
        public ActionResult CriarCampanha()
        {
            return View();
        }

        public ActionResult EstatisticasDeEnvio(string filtro)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.CAMPANHA_CONTATOS_ENVIADOS.Where(e => e.id_empresa == ID_EMPRESA).ToList());
            }
        }

        public ActionResult Visualizacoes()
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<LOCAL_EVENTO> locais = db.LOCAL_EVENTO.Include(d => d.PORTAL_VISUALIZACOES)
                                                           .Include(em => em.EMPRESA.EMPRESA_MODULOS)
                                                           .Where(e => e.id_empresa == ID_EMPRESA).ToList();
                return View(locais);
            }
        }
    }
}
