﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;

namespace BuffetWebManager.Controllers
{
    public class ModulosController : BaseController
    {
        //
        // GET: /Modulos/

        public ActionResult ModuloNaoPermitido()
        {
            ViewBag.administrador = (bool)base.colaborador.administrador;
            return View();
        }

        public ActionResult AcessoNegado()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Mobile()
        {
            return View();
        }
    }
}
