﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Web.Routing;
using System.Data.Entity;
using BuffetWebManager.Models;

namespace BuffetWebManager.Controllers
{
    public class MontarEventoController : BaseController
    {
        //
        // GET: /MontarEvento/

        public ActionResult InformacoesDoEvento(int? id_cliente, int? id_tipo_evento)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_orcamentos)
                {
                    if (id_tipo_evento != null)
                    {
                        ViewBag.id_tipo_evento = id_tipo_evento;
                        //return View(new ModelMontarEvento() { id_cliente = id_cliente, id_tipo_evento = (int)id_tipo_evento, id_empresa = base.ID_EMPRESA });
                        return View();
                    }
                    return RedirectToAction("Orcamento", "NovoOrcamento");
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        public ActionResult SalvarInformacoesDoEvento(ORCAMENTO orcamento, CLIENTE cliente, int id_evento)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_orcamentos)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        try
                        {
                            int id_cliente = 0;
                            string email = "";
                            CLIENTE clien = db.CLIENTEs.Where(c => c.email.Equals(cliente.email)).FirstOrDefault();
                            if (clien == null)
                            {
                                cliente.desde = DateTime.Now.Date;
                                cliente.id_empresa = base.ID_EMPRESA;
                                db.CLIENTEs.Add(cliente);
                                db.SaveChanges();
                                id_cliente = cliente.id;
                                email = cliente.email;
                            }
                            else
                            {
                                id_cliente = clien.id;
                                email = clien.email;
                            }

                            List<ORCAMENTO> o = db.ORCAMENTOes.ToList();
                            bool repeat = true;
                            do
                            {
                                string codigo_unico = new Random().Next(0, 100000000).ToString();
                                if (o.Where(i => i.codigo_unico.Equals(codigo_unico)).FirstOrDefault() == null)
                                {
                                    orcamento.id_cliente = id_cliente;
                                    orcamento.id_tipo_evento = id_evento;
                                    orcamento.codigo_unico = codigo_unico;
                                    orcamento.status = "INICIADO";
                                    orcamento.data_criacao = DateTime.Now.Date;

                                    orcamento.id_empresa = base.ID_EMPRESA;

                                    string senha = new Random().Next(0, 100000).ToString();
                                    orcamento.senha = RLSecurity.Security.CriptographyOn(senha);

                                    db.ORCAMENTOes.Add(orcamento);
                                    db.SaveChanges();
                                    return RedirectToAction("Cardapios", new RouteValueDictionary(new { codigo_unico = orcamento.codigo_unico }));
                                }
                            } while (repeat);
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception e)
                        {
                            base.setMensagem("Ouve algum erro ao tentar gerar um orcamento, tente novamente", tipoMensagem.tipoErro);
                            return RedirectToAction("NovoOrcamento", "Orcamento");
                        }
                    }
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        public ActionResult Cardapios(string codigo_unico)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_orcamentos)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        ORCAMENTO orcamento = db.ORCAMENTOes.Include(t => t.EVENTO).FirstOrDefault(o => o.codigo_unico.Equals(codigo_unico));
                        ViewBag.CODIGO_UNICO = orcamento.codigo_unico;
                        ViewBag.NOME_CLIENTE = orcamento.nome; 
                         
                        ViewBag.evento = orcamento.EVENTO.nome;
                        ViewBag.imagem = orcamento.EVENTO.url;

                        DateTime data = DateTime.Now.Date;
                        EVENTO evento = db.EVENTOes.Include(c => c.EVENTOS_PACOTES.Select(p => p.PACOTE)).FirstOrDefault(e => e.id == orcamento.id_tipo_evento && e.status == true && e.id_empresa == PerfilEmpresa.ID_EMPRESA /* && (e.evento_de > data && e.evento_ate < data)*/);

                        //EVENTOS_PACOTES pacote_evento = db.EVENTOS_PACOTES.Include(p => p.PACOTE.IMAGENS_PACOTES).Include(e => e.EVENTO).Include(c => c..FirstOrDefault(e => e.id_evento == orcamento.id_tipo_evento && e.id_empresa == PerfilEmpresa.ID_EMPRESA && (e.PACOTE.status == true));
                        return View(evento);
                    }
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        public ActionResult CardapioSelecionado(int id_cardapio, string codigo_unico)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_orcamentos)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        ORCAMENTO orcamento = db.ORCAMENTOes.FirstOrDefault(o => o.codigo_unico.Equals(codigo_unico)); 
                        int convidados = orcamento.n_convidados != null ? (int)orcamento.n_convidados : 0;
                        convidados += orcamento.n_convidados != null ? (int)orcamento.n_convidados_c : 0;
                        ViewBag.convidados = convidados;
                    } 

                    ViewBag.id_cardapio = id_cardapio;
                    ViewBag.codigo_unico = codigo_unico;

                    return View();
                    //return View(new PacoteEscolhido() { id_empresa = base.ID_EMPRESA, id_cardapio = id_cardapio });
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }
    }
}
