﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using BuffetWebManager.Controllers;
using System.Data.Entity;
using BuffetWebManager.Models.Orcamento;

using System.Web.Routing;
using BuffetWebData.Utils;
using BuffetWebManager.Models;

namespace BuffetWebManager.Controllers
{
    [Authorize]
    public class OrcamentoController : BaseController
    {
        public ActionResult Orcamentos(int? filtro, string tipo, string statusOrcamento = null, bool deFinanceiro = false)
        {
            ViewBag.isFinanceiro = deFinanceiro;
            ViewBag.tipo = tipo;
            BuscarOrcamentos buscar = new BuscarOrcamentos()
            {
                id_empresa = ID_EMPRESA,
                tipo = tipo,
                filtro = filtro
            };
            return View(buscar);
        }

        public ActionResult NovoOrcamento(int? id_cliente)
        {
            ViewBag.id_cliente = id_cliente;
            return View(new NovoOrcamento());
        }




        //public ActionResult BuscarOrcamento(ParametrosOrcamento p)
        //{
        //    using (BuffetContext db = new BuffetContext())
        //    {
        //        if (PerfilEmpresa.MODULO_ORCAMENTO && PerfilEmpresa.MODULO_CONVITES)
        //        {
        //            orcamentos = db.ORCAMENTOes.Include(cli => cli.CLIENTE).Include(c => c.EVENTO).Include(l => l.LOCAL_EVENTO).Where(o => o.apenas_convite == true && (o.codigo_unico == p.codigo_unico ||
        //                                                                      o.id_local_evento == p.id_local_evento ||
        //                                                                      o.id_tipo_evento == p.id_tipo_evento ||
        //                                                                      o.status == p.status ||
        //                                                                      o.id_cliente == p.id_cliente) && o.id_empresa == ID_EMPRESA).ToList();
        //        }
        //        else
        //        {
        //            orcamentos = db.ORCAMENTOes.Include(cli => cli.CLIENTE).Include(c => c.EVENTO).Include(l => l.LOCAL_EVENTO).Where(o => (o.codigo_unico == p.codigo_unico ||
        //                                                                      o.id_local_evento == p.id_local_evento ||
        //                                                                      o.id_tipo_evento == p.id_tipo_evento ||
        //                                                                      o.status == p.status ||
        //                                                                      o.id_cliente == p.id_cliente) && o.id_empresa == ID_EMPRESA).ToList();
        //        }
        //        return RedirectToAction("Orcamentos");
        //    }
        //}

        public ActionResult DetalhesOrcamento(string codigo_unico, string tab)
        {
            ViewBag.tab = tab;
            base.setBackButton(true);
            return View(new DetalhesOrcamento() { codigo_unico = codigo_unico, id_empresa = base.ID_EMPRESA });
        }

        //ANOTACOES
        public ActionResult SalvarAnotacao(string titulo, string anotacao, string codigo_unico)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_ANOTACOES anota = new ORCAMENTO_ANOTACOES();
                DateTime now = DateTime.Now;
                anota.data_horario = now;
                anota.id_empresa = base.ID_EMPRESA;
                anota.id_orcamento = db.ORCAMENTOes.Where(o => o.codigo_unico.Equals(codigo_unico)).FirstOrDefault().id;
                anota.titulo = titulo;
                anota.anotacao = anotacao;
                anota.id_colaborador = base.colaborador.id;
                db.ORCAMENTO_ANOTACOES.Add(anota);
                db.SaveChanges();

                return RedirectToAction("DetalhesOrcamento", new RouteValueDictionary(new { codigo_unico = codigo_unico, tab = "anotacoes" }));
            }
        }

        public JsonResult EnviarDados(string codigo_unico, string enviar_por)
        {
            if (!string.IsNullOrEmpty(codigo_unico))
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO orcamento = db.ORCAMENTOes.Include(c => c.CLIENTE).Include(f => f.CONVITE_ANIVERSARIANTES).FirstOrDefault(o => o.codigo_unico.Equals(codigo_unico) && o.id_empresa == ID_EMPRESA);
                    if (orcamento != null)
                    {
                        Tuple<string, string> s = new Tuple<string, string>("", "");// UtilsManager.gerarSenha();
                        orcamento.senha = s.Item2;
                        db.SaveChanges();

                        ContactManager contact = new ContactManager(ID_EMPRESA, orcamento.id_cliente);

                        string aniversariantes = "";
                        foreach (var aniversariante in orcamento.CONVITE_ANIVERSARIANTES)
                        {
                            aniversariantes = string.Format("{0} {1}", aniversariantes, aniversariante.CLIENTE_ANIVERSARIANTES.nome);
                        }

                        string url = string.Format("http://www.convite.buffetweb.com?codigo_empresa={0}&autenticacao={1}&codigo_unico={2}", EMPRESA.codigo_empresa, orcamento.autenticacao, orcamento.codigo_unico);

                        if (enviar_por.Equals("email"))
                        {
                            EmailContent content = new EmailContent()
                            {
                                emailContentButton = EmailContentButton.Acessar,
                                emailContentHeader = EmailContentHeader.Convite,
                                nomeOla = orcamento.CLIENTE.nome,
                                url = url
                            };

                            string mensagem = string.Format("Você esta recebendo seu login e senha para acesso ao Convite Online da festa de {0} <br /><br />Login: {1}<br />Senha: {2}", aniversariantes, orcamento.codigo_unico, s.Item1);

                            EmailStatusSend statusEmail = contact.SendEmail(orcamento.CLIENTE.email, "Informações de Login", mensagem, TipoContato.InformacoesDeLogin, orcamento.autenticacao, content);
                            if (statusEmail == EmailStatusSend.Send)
                                return Json(new { status = "send", icon = enviar_por, mensagem = "Login de acesso e senha enviado para o cliente!" }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { status = "error", icon = enviar_por, mensagem = "Login não enviado para o cliente!" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string m = string.Format("{0} Login: {1} e senha: {2} p/ acesso ao {3} Online", UtilsManager.EncurtarURL(url), orcamento.codigo_unico, s.Item1, orcamento.apenas_convite ? "Convite" : "Orçamento");

                            SMSStatusSend d = contact.SendSMS(orcamento.CLIENTE.celular, "Informações de Login", m, TipoContato.InformacoesDeLogin);
                            if (d == SMSStatusSend.Send)
                                return Json(new { status = "send", icon = enviar_por, mensagem = "Login de acesso e senha enviado para o cliente!" }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { status = "error", icon = enviar_por, mensagem = "Login não enviado para o cliente!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(new { status = "error", icon = enviar_por, mensagem = "Orçamento não encontrado!" }, JsonRequestBehavior.AllowGet);
        }

        //------------------- JSON ---------------------
        [HttpGet]
        public JsonResult CarregarPacotes(int id_evento)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<EVENTOS_PACOTES> pacote_evento = db.EVENTOS_PACOTES.Include(p => p.PACOTE).Include(e => e.EVENTO).Where(e => e.id_evento == id_evento && e.id_empresa == ID_EMPRESA && (e.PACOTE.status == true)).ToList();
                List<NovoOrcamentoPacotes> pacotes = new List<NovoOrcamentoPacotes>();
                foreach (var pacote in pacote_evento)
                {
                    pacotes.Add(new NovoOrcamentoPacotes()
                    {
                        id_pacote = pacote.PACOTE.id,
                        nome_pacote = pacote.PACOTE.nome,
                    });
                }
                return Json(pacotes, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
