﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebManager.Models.OutrosCadastros;

namespace BuffetWebManager.Controllers
{
    public class OutrosCadastrosController : BaseController
    {
        public ActionResult OutrosCadastros()
        {
            if (base.isLogin())
            {
                base.getMensagem();
                using (BuffetContext db = new BuffetContext())
                {
                    OutrosCadastros o = new OutrosCadastros();
                    o.categoria_itens = db.CATEGORIA_ITENS.ToList();
                    //o.tipo_evento = db.TIPO_EVENTO.Include(cp => cp.POTENCIAL_CLIENTE).ToList();
                    //o.unidade_itens = db.UNIDADE_ITENS.Include(ic => ic.).ToList();
                    return View(o);
                }
            }
            return RedirectToAction("Login");
        }
        //public ActionResult NovaTipoFesta(EVENTO tipoFesta)
        //{
        //    if (base.isLogin())
        //    {
        //        try
        //        {
        //            using (BuffetContext db = new BuffetContext())
        //            {
        //                db.TIPO_EVENTO.Add(tipoFesta);
        //                db.SaveChanges();
        //            }
        //            base.setMensagem("Tipo de festa adicionado", tipoMensagem.tipoSucesso);
        //        }
        //        catch (Exception)
        //        {
        //        }
        //        return RedirectToAction("OutrosCadastros", "OutrosCadastros");
        //    }
        //    return RedirectToAction("Login");
        //}

        public ActionResult ApagarNovaTipoFesta(int id)
        {
            if (base.isLogin())
            {
                using (BuffetContext db = new BuffetContext())
                {
                    try
                    {
                        EVENTO tipo = db.EVENTOes.Where(t => t.id == id).FirstOrDefault();
                        if (tipo != null)
                        {
                            db.EVENTOes.Remove(tipo);
                            db.SaveChanges();
                        }
                        base.setMensagem("Tipo de festa apagado", tipoMensagem.tipoSucesso);
                    }
                    catch (Exception)
                    {
                        base.setMensagem("", tipoMensagem.tipoSucesso);
                    }
                }
                return RedirectToAction("OutrosCadastros", "OutrosCadastros");
            }
            return RedirectToAction("Login");
        }

        //public ActionResult NovoUnidadeMedida(UNIDADE_MEDIDA unidade)
        //{
        //    if (base.isLogin())
        //    {
        //        try
        //        {
        //            using (BuffetContext db = new BuffetContext())
        //            {
        //                db.UNIDADE_MEDIDA.Add(unidade);
        //                db.SaveChanges();
        //            }
        //            base.setMensagem("Unidade de medida adicionado ", tipoMensagem.tipoSucesso);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //        return RedirectToAction("OutrosCadastros", "OutrosCadastros");
        //    }
        //    return RedirectToAction("Login");
        //}
        //public ActionResult ApagarNovoUnidadeMedida(int id)
        //{
        //    if (base.isLogin())
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            try
        //            {
        //                UNIDADE_MEDIDA tipo = db.UNIDADE_MEDIDA.FirstOrDefault(t => t.id == id);
        //                if (tipo != null)
        //                {
        //                    db.UNIDADE_ITENS.Remove(tipo);
        //                    db.SaveChanges();
        //                }
        //                base.setMensagem("Unidade de medida apagado", tipoMensagem.tipoSucesso);
        //            }
        //            catch (Exception)
        //            {
        //                base.setMensagem("", tipoMensagem.tipoSucesso);
        //            }
        //        }
        //        return RedirectToAction("OutrosCadastros", "OutrosCadastros");
        //    }
        //    return RedirectToAction("Login");
        //}
        //public ActionResult NovoAmbiente(EVENTO ambiente)
        //{
        //    if (base.isLogin())
        //    {
        //        try
        //        {
        //            using (BuffetContext db = new BuffetContext())
        //            {
        //                db.EVENTOS.Add(ambiente);
        //                db.SaveChanges();
        //            }
        //            base.setMensagem("Ambiente adicionado ", tipoMensagem.tipoSucesso);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //        return RedirectToAction("OutrosCadastros", "OutrosCadastros");
        //    }
        //    return RedirectToAction("Login");
        //}

        public ActionResult NovoCategoriaItens(CATEGORIA_ITENS categoria_itens)
        {
            if (base.isLogin())
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        db.CATEGORIA_ITENS.Add(categoria_itens);
                        db.SaveChanges();
                    }
                    base.setMensagem("Categoria de itens adicionado ", tipoMensagem.tipoSucesso);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction("OutrosCadastros", "OutrosCadastros");
            }
            return RedirectToAction("Login");
        }

        //Cadastrar Horarios
    }
}
