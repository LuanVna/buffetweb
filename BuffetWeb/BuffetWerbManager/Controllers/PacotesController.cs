﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models; 
using System.Data.Entity;
using BuffetWebManager.Models.Pacotes; 

namespace BuffetWebManager.Controllers
{
    public class PacotesController : BaseController
    {
        //
        // GET: /Pacotes/

        public ActionResult Pacotes()
        {
            if (base.isLogin())
            {
                if (base.perfil.v_pacotes)
                {
                    base.getMensagem();
                    return View(new Pacotes() { id_empresa = base.ID_EMPRESA });
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        //public ActionResult SalvarPacote(FormCollection selecionados, HttpPostedFileBase image, List<PAluguelSelecionado> aluguel, List<PServicosSelecionado> servicos, List<PDiversosSelecionado> diversos)
        //{
        //    if (base.isLogin())
        //    {
        //        if (base.perfil.c_pacotes)
        //        {
        //            if (ActionsPacotes.SalvarPacotes(selecionados, aluguel, servicos, diversos, image != null ? image.InputStream : null, base.ID_EMPRESA))
        //                base.setMensagem("Pacote salvo com sucesso", tipoMensagem.tipoSucesso);
        //            else
        //                base.setMensagem("Pacote não foi salvo", tipoMensagem.tipoSucesso);

        //            return RedirectToAction("Pacotes");
        //        }
        //        return base.NotAcess;
        //    }
        //    return base.LoginView;
        //}

        public ActionResult AlterarStatus(int id, string status)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_pacotes)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        PACOTE pacote = db.PACOTES.FirstOrDefault(e => e.id == id);
                        if (pacote != null)
                        {
                            pacote.status = status.Equals("ATIVO");
                            db.SaveChanges();
                        }
                    } 
                    return RedirectToAction("Pacotes");
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }
    }
}
