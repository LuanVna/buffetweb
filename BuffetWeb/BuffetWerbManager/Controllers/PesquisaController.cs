﻿using BuffetWebManager.Models.Pesquisa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebData.Utils;

namespace BuffetWebManager.Controllers
{
    public class PesquisaController : BaseController
    {
        public ActionResult Pesquisa(string filtro)
        {
            ViewBag.filtro = filtro;
            return View(new PesquisaModel() { id_empresa = ID_EMPRESA, filtro = filtro });
        }

        public ActionResult GraficosConvidados(int? id_pesquisa)
        {
            if (id_pesquisa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    PESQUISA_PESQUISAS pesquisa = db.PESQUISA_PESQUISAS.FirstOrDefault(f => f.id == id_pesquisa);
                    List<CONVITE_CONVIDADOS> convidados = db.CONVITE_CONVIDADOS.Include(d => d.PESQUISA_RESPOSTA.Select(df => df.PESQUISA_PERGUNTA)).Include(d => d.PESQUISA_RESPOSTA_OPCOES.Select(t => t.PESQUISA_PERGUNTA_OPCOES)).Where(f => f.id_orcamento == pesquisa.id_orcamento && f.status_pequisa != null).ToList();
                    return View(convidados);
                }
            }
            return RedirectToAction("Pesquisa");
        }

        public ActionResult Graficos(int? id_pesquisa)
        {
            if (id_pesquisa != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    PESQUISA_PESQUISAS pesquisa = db.PESQUISA_PESQUISAS.Include(o => o.ORCAMENTO).Include(p => p.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_RESPOSTA.Select(c => c.CONVITE_CONVIDADOS))).Include(o => o.PESQUISA_PERGUNTA.Select(d => d.PESQUISA_PERGUNTA_OPCOES.Select(es => es.PESQUISA_RESPOSTA_OPCOES))).FirstOrDefault(p => p.id == id_pesquisa && p.id_empresa == ID_EMPRESA);
                    if (pesquisa != null)
                    {
                        if (pesquisa.enviada == true)
                        {
                            ViewBag.id_pesquisa = id_pesquisa;
                            return View(pesquisa);
                        }
                        else
                        {
                            base.setMensagem("Essa pesquisa ainda não foi enviada aos convidados!");
                            return RedirectToAction("Pesquisa");
                        }
                    }
                    base.setMensagem("Pesquisa não encontrada, verifique se ela já foi enviada aos convidados!");
                }
            }
            return RedirectToAction("Pesquisa");
        }

        public ActionResult CriarPesquisa(int? id_pesquisa, int? id_orcamento, string tipo_pesquisa, bool? copiar_pergunta, int? id_pesquisa_clonar)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ViewBag.tipo_pesquisa = tipo_pesquisa;
                ViewBag.associando = false;
                ViewBag.id_pesquisa = id_pesquisa;
                if (tipo_pesquisa.Equals("EDITAR"))
                {
                    PESQUISA_PESQUISAS pesquisa = db.PESQUISA_PESQUISAS.Include(e => e.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_PERGUNTA_OPCOES)).FirstOrDefault(p => p.id == id_pesquisa && p.id_empresa == ID_EMPRESA && p.enviada == false);
                    ViewBag.codigo_unico = db.ORCAMENTOes.FirstOrDefault(e => e.id == pesquisa.id_orcamento).codigo_unico;
                    return View(pesquisa);
                }
                else
                {
                    ORCAMENTO orcam = db.ORCAMENTOes.Include(p => p.PESQUISA_PESQUISAS).FirstOrDefault(e => e.id == id_orcamento && e.id_empresa == ID_EMPRESA && e.PESQUISA_PESQUISAS.Count == 0);
                    if (orcam != null)
                    {
                        ViewBag.codigo_unico = orcam.codigo_unico;
                        PESQUISA_PESQUISAS pesquisa = new PESQUISA_PESQUISAS()
                        {
                            id_empresa = ID_EMPRESA,
                            id_orcamento = orcam.id,
                            enviada = false,

                            //data_envio = new DateTime(orcam.data_evento.ToString()).AddDays((int)new UtilsManager(ID_EMPRESA).configuracao().pesquisa_dias_apos_evento)
                        };
                        db.PESQUISA_PESQUISAS.Add(pesquisa);
                        db.SaveChanges();

                        ViewBag.id_pesquisa = pesquisa.id;

                        if (copiar_pergunta == true)
                        {
                            ViewBag.associando = true;
                            return View(db.PESQUISA_PESQUISAS.Include(e => e.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_PERGUNTA_OPCOES)).FirstOrDefault(p => p.id == id_pesquisa_clonar && p.id_empresa == ID_EMPRESA));
                        }
                        else
                        {
                            return View(db.PESQUISA_PESQUISAS.Include(e => e.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_PERGUNTA_OPCOES)).FirstOrDefault(p => p.id == pesquisa.id && p.id_empresa == ID_EMPRESA));
                        }
                    }
                    else
                    {
                        base.setMensagem("Já existe uma pesquisa criada para este pedido!");
                    }
                }
            }
            return RedirectToAction("Pesquisa");
        }

        public ActionResult ApagarPesquisa(int id_pesquisa)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.PESQUISA_PESQUISAS.Remove(db.PESQUISA_PESQUISAS.FirstOrDefault(e => e.id == id_pesquisa && e.id_empresa == ID_EMPRESA));
                    db.SaveChanges();
                    base.setMensagem("Pesquisa apagada!");
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Pesquisa não apagada!", tipoMensagem.tipoErro);
            }
            return RedirectToAction("Pesquisa");
        }


        public ActionResult EnviarPesquisaManual(int id_pesquisa)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (EMPRESA.EMPRESA_CONFIGURACAO.FirstOrDefault().pesquisa_enviar_automaticamente == false)
                {
                    PESQUISA_PESQUISAS pesquisa = db.PESQUISA_PESQUISAS.FirstOrDefault(p => p.id == id_pesquisa);
                    List<CONVITE_CONVIDADOS> convidados = db.CONVITE_CONVIDADOS.Where(f => f.id_orcamento == pesquisa.id_orcamento && f.compareceu == true).ToList();

                    UtilsManager manager = new UtilsManager(base.ID_EMPRESA);
                    String convite = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/App_Data/Pesquisa/PESQUISA_EMAIL.txt"));
                    foreach (var convidado in convidados)
                    {
                        if (manager.SendPesquisa(convidado, convite))
                        {
                            convidado.status_pequisa = "ENVIADA";
                        }
                    }

                    pesquisa.data_envio = DateTime.Now.Date;
                    pesquisa.enviada = true;
                    db.SaveChanges();
                    base.setMensagem(string.Format(" {0} Convidados receberam essa pesquisa!", convidados.Count), tipoMensagem.tipoSucesso);
                }
                else
                {
                    base.setMensagem("Você não pode enviar essa pesquisa manualmente!", tipoMensagem.tipoAtencao);
                }
            }
            return RedirectToAction("Pesquisa");
        }




        /*------------------------------------ JSON ------------------------------------*/
        public JsonResult SalvarPergunta(PESQUISA_PERGUNTA pergunta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    PESQUISA_PERGUNTA per = db.PESQUISA_PERGUNTA.FirstOrDefault(e => e.titulo == pergunta.titulo && e.id_pesquisa == pergunta.id_pesquisa && e.id_empresa == ID_EMPRESA);
                    if (per == null)
                    {
                        pergunta.id_empresa = base.ID_EMPRESA;
                        if (pergunta.id == 0)
                        {
                            db.PESQUISA_PERGUNTA.Add(pergunta);
                            db.SaveChanges();
                            return Json(new { isOk = true, id = pergunta.id }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            PESQUISA_PERGUNTA p = db.PESQUISA_PERGUNTA.FirstOrDefault(e => e.id == pergunta.id);
                            p.titulo = pergunta.titulo;
                            db.SaveChanges();
                            return Json(new { isOk = true, id = p.id }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { isOk = true, id = per.id }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarOpcao(PESQUISA_PERGUNTA_OPCOES opcao)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (db.PESQUISA_PERGUNTA_OPCOES.FirstOrDefault(e => e.titulo.Equals(opcao.titulo) && e.id_pergunta == opcao.id_pergunta) == null)
                    {
                        opcao.id_empresa = ID_EMPRESA;
                        if (opcao.id == 0)
                        {
                            db.PESQUISA_PERGUNTA_OPCOES.Add(opcao);
                        }
                        else
                        {
                            db.PESQUISA_PERGUNTA_OPCOES.Attach(opcao);
                            db.Entry(opcao).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {

            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApagarOpcao(int id_opcao)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    db.PESQUISA_PERGUNTA_OPCOES.Remove(db.PESQUISA_PERGUNTA_OPCOES.FirstOrDefault(e => e.id == id_opcao && e.id_empresa == ID_EMPRESA));
                    db.SaveChanges();
                }
            }
            catch (Exception)
            { }
            return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PerguntasPesquisa(int id_pesquisa)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var perguntas = db.PESQUISA_PERGUNTA.Include(r => r.PESQUISA_RESPOSTA).Where(e => e.id_pesquisa == id_pesquisa).Select(f => new { f.id, f.tipo_pergunta, quantidade = f.PESQUISA_RESPOSTA.Count }).ToList();
                    return Json(new { isOk = true, perguntas = perguntas }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RespostaSorriso(int id_pergunta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<PESQUISA_RESPOSTA> respostas = db.PESQUISA_RESPOSTA.Include(f => f.PESQUISA_PERGUNTA).Where(e => e.id_pergunta == id_pergunta).ToList();

                    dynamic resposta = new
                    {
                        satisfeito = respostas.Where(e => e.resposta.Equals("satisfeito")).ToList().Count,
                        muito_satisfeito = respostas.Where(e => e.resposta.Equals("muito_satisfeito")).ToList().Count,
                        indiferente = respostas.Where(e => e.resposta.Equals("indiferente")).ToList().Count,
                        muito_insatisfeito = respostas.Where(e => e.resposta.Equals("muito_insatisfeito")).ToList().Count,
                        insatisfeito = respostas.Where(e => e.resposta.Equals("insatisfeito")).ToList().Count
                    };

                    PESQUISA_PERGUNTA pergunta = respostas.FirstOrDefault().PESQUISA_PERGUNTA;
                    dynamic sorriso = new
                    {
                        titulo = pergunta.titulo,
                        id_pergunta = pergunta.id,
                        resposta = resposta,
                        total = respostas.Count
                    };
                    return Json(new { isOk = true, sorriso = sorriso }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RespostaEstrela(int id_pergunta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<PESQUISA_RESPOSTA> respostas = db.PESQUISA_RESPOSTA.Include(f => f.PESQUISA_PERGUNTA).Where(e => e.id_pergunta == id_pergunta).ToList();

                    dynamic resposta = new
                    {
                        UmaEstrela = respostas.Where(e => e.resposta.Equals("0")).ToList().Count,
                        DuasEstrelas = respostas.Where(e => e.resposta.Equals("1")).ToList().Count,
                        TresEstrelas = respostas.Where(e => e.resposta.Equals("2")).ToList().Count,
                        QuatroEstrelas = respostas.Where(e => e.resposta.Equals("3")).ToList().Count,
                        CincoEstrelas = respostas.Where(e => e.resposta.Equals("4")).ToList().Count
                    };

                    PESQUISA_PERGUNTA pergunta = respostas.FirstOrDefault().PESQUISA_PERGUNTA;
                    dynamic estrela = new
                    {
                        titulo = pergunta.titulo,
                        id_pergunta = pergunta.id,
                        resposta = resposta,
                        total = respostas.Count
                    };
                    return Json(new { isOk = true, estrela = estrela }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RespostaGostei(int id_pergunta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<PESQUISA_RESPOSTA> respostas = db.PESQUISA_RESPOSTA.Include(f => f.PESQUISA_PERGUNTA).Where(e => e.id_pergunta == id_pergunta).ToList();

                    dynamic resposta = new
                    {
                        gostei = respostas.Where(e => e.resposta.Equals("GOSTEI")).ToList().Count,
                        naogostei = respostas.Where(e => e.resposta.Equals("NAO GOSTEI")).ToList().Count,
                    };

                    PESQUISA_PERGUNTA pergunta = respostas.FirstOrDefault().PESQUISA_PERGUNTA;
                    dynamic gostei = new
                    {
                        titulo = pergunta.titulo,
                        id_pergunta = pergunta.id,
                        resposta = resposta,
                        total = respostas.Count
                    };
                    return Json(new { isOk = true, gostei = gostei }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RespostaSatisfacao(int id_pergunta)
        {
            try
            {
                List<Object> all_respostas = new List<Object>();
                using (BuffetContext db = new BuffetContext())
                {
                    List<PESQUISA_PERGUNTA> respostas = db.PESQUISA_PERGUNTA.Include(op => op.PESQUISA_PERGUNTA_OPCOES.Select(f => f.PESQUISA_RESPOSTA_OPCOES)).Where(f => f.id == id_pergunta).ToList();
                    foreach (var resposta in respostas)
                    {
                        foreach (var opcao in resposta.PESQUISA_PERGUNTA_OPCOES)
                        {
                            dynamic opcoes = new
                            {
                                pessimo = opcao.PESQUISA_RESPOSTA_OPCOES.Where(f => f.resposta == "PESSIMO").ToList().Count,
                                ruim = opcao.PESQUISA_RESPOSTA_OPCOES.Where(f => f.resposta == "RUIM").ToList().Count,
                                normal = opcao.PESQUISA_RESPOSTA_OPCOES.Where(f => f.resposta == "NORMAL").ToList().Count,
                                bom = opcao.PESQUISA_RESPOSTA_OPCOES.Where(f => f.resposta == "BOM").ToList().Count,
                                exelente = opcao.PESQUISA_RESPOSTA_OPCOES.Where(f => f.resposta == "EXELENTE").ToList().Count
                            };

                            bool isResposta = true;

                            if (opcoes.pessimo == 0 &&
                                opcoes.ruim == 0 &&
                                opcoes.normal == 0 &&
                                opcoes.bom == 0 &&
                                opcoes.exelente == 0)
                            {
                                isResposta = false;
                            }

                            dynamic satisfacao = new
                            {
                                id = opcao.id,
                                titulo = opcao.titulo,
                                total = opcao.PESQUISA_RESPOSTA_OPCOES.Count,
                                opcoes = opcoes,
                                isResposta = isResposta
                            };
                            all_respostas.Add(satisfacao);
                        }
                    }
                }
                return Json(new { isOk = true, satisfacao = all_respostas }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            { }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

    }
}
