﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebManager.Models.Planos;
using BuffetWebData.Utils;
using BuffetWebCielo.Pedido;
using BuffetWebCielo.Transacao;

namespace BuffetWebManager.Controllers
{
    public class PlanosController : BaseController
    {
        public ActionResult ComprarPlano()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.B_PLANOS_DISPONIVEIS.ToList());
            }
        }

        public ActionResult MeusPlanos()
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<LOCAL_EVENTO> locais = db.LOCAL_EVENTO.Include(d => d.PORTAL_ANUNCIANTE_DESTAQUE)
                                                           .Include(f => f.PORTAL_ANUNCIANTE_HEADER)
                                                           .Include(t => t.PORTAL_ANUNCIANTE_TOPMENU)
                                                           .Include(h => h.PORTAL_ANUNCIANTE_HOME)
                                                           .Include(p => p.PORTAL_ANUNCIANTE_PROMOCAO.Select(e => e.B_PORTAL_PROMOCAO))
                                                           .Where(e => e.id_empresa == ID_EMPRESA).ToList();
                return View(locais);
            }
        }

        public ActionResult AdicionarPlano(int id_local_evento, int id_plano, string periodo)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (new string[] { "MENSAL", "TRIMESTRAL", "SEMESTRAL", "ANUAL", "GRATIS" }.Contains(periodo))
                {
                    var item = db.EMPRESA_CARRINHO.FirstOrDefault(e => e.id_local_evento == id_local_evento && e.id_plano_disponivel == id_plano && e.comprado == false);
                    if (item != null) db.EMPRESA_CARRINHO.Remove(item);

                    db.EMPRESA_CARRINHO.Add(new EMPRESA_CARRINHO()
                    {
                        id_empresa = base.ID_EMPRESA,
                        id_local_evento = id_local_evento,
                        id_plano_disponivel = id_plano,
                        periodo = periodo,
                        adicionado_em = DateTime.Now,
                        comprado = false
                    });
                    db.SaveChanges();
                    return RedirectToAction("MeuCarrinho");
                }
                base.setMensagem("Periodo Invalído", tipoMensagem.tipoErro);
                return RedirectToAction("ComprarPlano");
            }
        }

        public ActionResult ApagarItem(int id_item)
        {
            using (BuffetContext db = new BuffetContext())
            {
                db.EMPRESA_CARRINHO.Remove(db.EMPRESA_CARRINHO.FirstOrDefault(e => e.id == id_item));
                db.SaveChanges();
                return RedirectToAction("MeuCarrinho");
            }
        }

        public JsonResult VefificaCupomDesconto(int quantidade)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var cupons = db.EMPRESA_CARRINHO_CUPOM.Include(f => f.B_PLANOS_DISPONIVEIS)
                                                     .Where(e => e.id_empresa == ID_EMPRESA && e.usado == false)
                                                     .Select(d => new
                                                     {
                                                         disponivel = (db.EMPRESA_CARRINHO.FirstOrDefault(e => e.periodo.Equals(d.periodo) && e.id_local_evento == d.id_local_evento && e.id_plano_disponivel == d.id_planos_disponiveis) != null),
                                                         para_plano = d.B_PLANOS_DISPONIVEIS.nome,
                                                         d.periodo,
                                                         filial = d.LOCAL_EVENTO.nome,
                                                         id_filial = d.LOCAL_EVENTO.id,
                                                         d.descricao,
                                                         d.valor_desconto
                                                     }).ToList();
                if (cupons.Count != quantidade)
                {
                    return Json(new { isOk = true, cupons = cupons }, JsonRequestBehavior.AllowGet);
                } return Json(new { isOk = false}, JsonRequestBehavior.AllowGet);
            }
        }

        private static string Autenticacao;
        private static bool TransacaoEfetuada;
        public ActionResult MeuCarrinho()
        {
            MeuCarrinhoModel carrinho = new MeuCarrinhoModel() { id_empresa = base.ID_EMPRESA };
            if (TransacaoEfetuada)
            {
                ViewBag.Concluido = true;
                ViewBag.Status = new TransacoesEfetuadas(ID_EMPRESA).Consultar(Autenticacao).status;
                TransacaoEfetuada = false;
                return View(carrinho);
            }
            else
            {
                if (carrinho.Carrinho.Count != 0)
                {
                    base.setBackButton(true);
                    return View(carrinho);
                }
            }
            base.setMensagem("Você não tem itens no carrinho!");
            return RedirectToAction("ComprarPlanos");
        }

        public ActionResult PortalPromocao()
        {
            return View(new PortalPromocoesModel());
        }

        public ActionResult VerificarPromocao(List<LocalSelecionado> locais, int id_promocao)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    foreach (var local in locais)
                    {
                        var l = db.PORTAL_ANUNCIANTE_PROMOCAO.FirstOrDefault(e => e.id_local_evento == local.id_local_evento && e.id_portal_promocao == id_promocao);

                        if (l == null)
                        {
                            db.PORTAL_ANUNCIANTE_PROMOCAO.Add(new PORTAL_ANUNCIANTE_PROMOCAO()
                            {
                                id_local_evento = local.id_local_evento,
                                id_portal_promocao = id_promocao,
                                disponivel = local.selecionado,
                            });

                            if (local.selecionado)
                                new FacebookManager(l.LOCAL_EVENTO).NotificarPromocao(l.B_PORTAL_PROMOCAO);
                        }
                        else
                            l.disponivel = local.selecionado;
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("PortalPromocao");
            }
            catch (Exception e)
            {
                base.setMensagem("Ouve algum erro", tipoMensagem.tipoErro);
                return RedirectToAction("MeusPlanos");
            }
        }

        [HttpPost]
        public JsonResult EfetuarPagamento(DadosPedido dadosPedido, string renovar_automaticamente)
        {
            MeuCarrinhoTransacao transacao = new MeuCarrinhoTransacao() { id_empresa = ID_EMPRESA };
            RespostaTransacaoCarrinho resposta = transacao.EfetuarPagamento(dadosPedido);
            TransacaoEfetuada = resposta.isOk;
            Autenticacao = resposta.autenticacao;

            return Json(resposta, JsonRequestBehavior.DenyGet);
        }
    }
}
