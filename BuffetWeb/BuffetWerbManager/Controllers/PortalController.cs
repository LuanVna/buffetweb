﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Controllers
{
    public class PortalController : BaseController
    {
        public ActionResult LocalDoEvento(string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ViewBag.autenticacao = autenticacao;
                return View(db.LOCAL_EVENTO.Include(e => e.ENDERECO)
                                           .Include(v => v.PORTAL_VISUALIZACOES)
                                           .Include(c => c.LOCAL_EVENTO_COMENTARIOS.Select(d => d.PORTAL_CLIENTE_PERFIL))
                                           .Where(e => e.id_empresa == ID_EMPRESA).ToList());
            }
        }

        public ActionResult Estatisticas(string autenticacao)
        {
            return View();
        }





        public JsonResult RemoverComentario(int id_comentario, string autenticacao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                LOCAL_EVENTO l = db.LOCAL_EVENTO.FirstOrDefault(e => e.autenticacao == autenticacao && e.id_empresa == ID_EMPRESA);
                if (l != null)
                {
                    LOCAL_EVENTO_COMENTARIOS comentario = db.LOCAL_EVENTO_COMENTARIOS.FirstOrDefault(e => e.id == id_comentario);
                    if (comentario != null)
                    {
                        comentario.analisado = false;
                        db.SaveChanges();
                        return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
