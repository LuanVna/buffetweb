﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity; 
using BuffetWebData.Models; 

namespace BuffetWebManager.Controllers
{
    public class PrestadorController : BaseController
    {
        public ActionResult Prestadores()
        {
            if (base.isLogin())
            {
                if (base.perfil.v_configuracao)
                {
                    base.getMensagem();
                    using (BuffetContext db = new BuffetContext())
                    {
                        return View(db.PRESTADORs.Where(e => e.id_empresa == ID_EMPRESA).ToList());
                    }
                }
            }
            return base.LoginView;
        }

        public ActionResult SalvarPrestador(PRESTADOR prestador)
        {
            if (base.isLogin())
            {
                if (base.perfil.c_configuracao)
                {
                    try
                    {
                        using (BuffetContext db = new BuffetContext())
                        {
                            prestador.id_empresa = base.ID_EMPRESA;
                            db.PRESTADORs.Add(prestador);
                            db.SaveChanges();
                            base.setMensagem("Prestador salvo com sucesso!", tipoMensagem.tipoSucesso);
                        }
                    }
                    catch (Exception)
                    {
                        base.setMensagem("Erro Prestador!", tipoMensagem.tipoErro); 
                    } 
                    return RedirectToAction("Prestadores");
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        public ActionResult PrestadorFuncoes()
        {
            if (base.isLogin())
            {
                if (base.perfil.v_configuracao)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        return View(db.PRESTADOR_FUNCAO.Where(e => e.id_empresa == ID_EMPRESA).ToList());
                    }
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }

        public ActionResult SalvarFuncaoPrestador(PRESTADOR_FUNCAO funcao)
        {
            if (base.isLogin())
            {
                if (base.perfil.v_configuracao)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        funcao.id_empresa = base.ID_EMPRESA;
                        db.PRESTADOR_FUNCAO.Add(funcao);
                        db.SaveChanges();
                        return RedirectToAction("PrestadorFuncoes");
                    }
                }
                return base.NotAcess;
            }
            return base.LoginView;
        }
    }
}
