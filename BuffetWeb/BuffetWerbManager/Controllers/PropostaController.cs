﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebData.Utils;
using System.Reflection;
using BuffetWebManager.Models;
using BuffetWebManager.Models.Proposta;

namespace BuffetWebManager.Controllers
{
    public class PropostaController : BaseController
    {
        public ActionResult CriarProposta()
        {
            return View();
        }

        public ActionResult MinhasPropostas()
        {
            using (BuffetContext db = new BuffetContext())
            {
                return View(db.ORCAMENTO_PROPOSTA.Include(d => d.ORCAMENTO_PROPOSTA_PACOTE).Where(e => e.id_empresa == ID_EMPRESA).ToList());
            }
        }

        public ActionResult NovaProposta(string autenticacao, ORCAMENTO_PROPOSTA proposta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (string.IsNullOrEmpty(autenticacao))
                    {
                        int id_empresa = ID_EMPRESA;
                        proposta.id_empresa = id_empresa;
                        proposta.autenticacao = UtilsManager.CriarAutenticacaoEmpresa();

                        db.ORCAMENTO_PROPOSTA.Add(proposta);
                        db.SaveChanges();

                        Int32[] quantidade = new Int32[] { 50, 60, 70, 80, 90, 100, 120, 150, 180, 200 };
                        for (int i = 0; i < quantidade.Length; i++)
                        {
                            ORCAMENTO_PROPOSTA_PACOTE pacote = new ORCAMENTO_PROPOSTA_PACOTE()
                               {
                                   id_empresa = id_empresa,
                                   id_proposta = proposta.id,
                                   quantidade_pessoas = quantidade[i],
                                   ativo = false
                               };

                            db.ORCAMENTO_PROPOSTA_PACOTE.Add(pacote);
                            db.SaveChanges();

                            for (int x = 0; x < 2; x++)
                            {
                                ORCAMENTO_PROPOSTA_PACOTE_VALORES valores = new ORCAMENTO_PROPOSTA_PACOTE_VALORES()
                                {
                                    id_empresa = id_empresa,
                                    id_proposta_pacote = pacote.id,
                                    segunda = (x == 0),
                                    terca = (x == 0),
                                    quarta = (x == 0),
                                    quinta = (x == 0),
                                    sexta = (x == 1),
                                    sabado = (x == 1),
                                    domingo = (x == 1),
                                    valor_almoco = 0,
                                    valor_jantar = 0,
                                    valor_adicional_pessoa = 0,
                                };
                                db.ORCAMENTO_PROPOSTA_PACOTE_VALORES.Add(valores);
                                db.SaveChanges();
                            }
                        }

                        for (int i = 0; i < 5; i++)
                        {
                            ORCAMENTO_PROPOSTA_ABAS orcamentoAbas = new ORCAMENTO_PROPOSTA_ABAS()
                            {
                                id_empresa = id_empresa,
                                id_orcamento_proposta = proposta.id,
                                nome_aba = "",
                                disponivel = false
                            };
                            db.ORCAMENTO_PROPOSTA_ABAS.Add(orcamentoAbas);
                            db.SaveChanges();

                            for (int f = 0; f < 15; f++)
                            {
                                db.ORCAMENTO_PROPOSTA_DESCRICAO.Add(new ORCAMENTO_PROPOSTA_DESCRICAO()
                                {
                                    nome = "",
                                    descricao = "",
                                    disponivel = false,
                                    id_empresa = id_empresa,
                                    id_orcamento_proposta_aba = orcamentoAbas.id
                                });
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();

                        base.setMensagem("Proposta adicionada", tipoMensagem.tipoSucesso);
                        return RedirectToAction("NovaProposta", new { autenticacao = proposta.autenticacao });
                    }
                    else
                    {
                        base.setMensagem("Proposta selecionada", tipoMensagem.tipoInfo);
                        ORCAMENTO_PROPOSTA p = db.ORCAMENTO_PROPOSTA.Include(l => l.LOCAL_EVENTO)
                                                                    .Include(f => f.ORCAMENTO_PROPOSTA_ABAS.Select(d => d.ORCAMENTO_PROPOSTA_DESCRICAO))
                                                                    .Include(d => d.ORCAMENTO_PROPOSTA_PACOTE.Select(v => v.ORCAMENTO_PROPOSTA_PACOTE_VALORES))
                                                                    .FirstOrDefault(e => e.autenticacao.Equals(autenticacao));
                        if (p != null)
                        {
                            return View(p);
                        }
                        else
                        {
                            base.setMensagem("Proposta não encontrada", tipoMensagem.tipoErro);
                            return RedirectToAction("CriarProposta");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                base.setMensagem("Ouve algum erro, tente novamente", tipoMensagem.tipoErro);
                return RedirectToAction("CriarProposta");
            }
        }

        public ActionResult EditarProposta(ORCAMENTO_PROPOSTA proposta, int id_proposta)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA p = db.ORCAMENTO_PROPOSTA.FirstOrDefault(e => e.id == id_proposta);
                p.nome_proposta = proposta.nome_proposta;
                p.permitir_palavra_chave = proposta.permitir_palavra_chave;
                p.validade_dias = proposta.validade_dias;
                p.quantidade_parcelas = proposta.quantidade_parcelas;
                p.boleto = proposta.boleto;
                p.cartao = proposta.cartao;

                db.SaveChanges();

                base.setMensagem("Proposta editada com sucesso!");
                return RedirectToAction("NovaProposta", new { autenticacao = p.autenticacao });
            }
        }

        [ValidateInput(false)]
        public JsonResult AlteraDiaSemana(int id_valores, string dia_semana, bool valor, int id_pacote)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_PACOTE pacote = db.ORCAMENTO_PROPOSTA_PACOTE.Include(d => d.ORCAMENTO_PROPOSTA_PACOTE_VALORES).FirstOrDefault(f => f.id == id_pacote && f.id_empresa == ID_EMPRESA);

                foreach (var v in pacote.ORCAMENTO_PROPOSTA_PACOTE_VALORES)
                {
                    try
                    {
                        PropertyInfo propriedade = v.GetType().GetProperties().FirstOrDefault(d => d.Name.Equals(dia_semana));
                        propriedade.SetValue(v, false, null);
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                    }
                }

                ORCAMENTO_PROPOSTA_PACOTE_VALORES valores = db.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.id == id_valores && e.id_empresa == ID_EMPRESA);
                if (valores != null)
                {
                    PropertyInfo propriedade = valores.GetType().GetProperties().FirstOrDefault(d => d.Name.Equals(dia_semana));
                    propriedade.SetValue(valores, valor, null);
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult SalvaValorValores(int id_valores, string valor_almoco, string valor_jantar, string valor_adicional)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_PACOTE_VALORES valores = db.ORCAMENTO_PROPOSTA_PACOTE_VALORES.FirstOrDefault(e => e.id == id_valores && e.id_empresa == ID_EMPRESA);
                if (valores != null)
                {
                    valores.valor_almoco = Decimal.Parse(valor_almoco);
                    valores.valor_jantar = Decimal.Parse(valor_jantar);
                    valores.valor_adicional_pessoa = Decimal.Parse(valor_adicional);
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateInput(false)]
        public JsonResult AtivarPacote(int id_pacote, bool status)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_PACOTE pacote = db.ORCAMENTO_PROPOSTA_PACOTE.FirstOrDefault(e => e.id == id_pacote && e.id_empresa == ID_EMPRESA);
                if (pacote != null)
                {
                    pacote.ativo = status;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateInput(false)]
        public JsonResult AtualizarAba(int id_aba, string titulo, bool disponivel)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_ABAS aba = db.ORCAMENTO_PROPOSTA_ABAS.FirstOrDefault(e => e.id == id_aba && e.id_empresa == ID_EMPRESA);
                if (aba != null)
                {
                    if (!string.IsNullOrEmpty(titulo))
                        aba.nome_aba = titulo;

                    aba.disponivel = disponivel;

                    if (!disponivel)
                        db.ORCAMENTO_PROPOSTA_DESCRICAO.Where(f => f.id_orcamento_proposta_aba == id_aba && f.id_empresa == ID_EMPRESA).ToList()
                                                       .ForEach(d => d.disponivel = false);

                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateInput(false)]
        public JsonResult AtualizarCard(int id_aba, int id_descricao, string titulo, string descricao)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_DESCRICAO desc = db.ORCAMENTO_PROPOSTA_DESCRICAO.FirstOrDefault(e => e.id_orcamento_proposta_aba == id_aba && e.id_empresa == ID_EMPRESA && e.id == id_descricao);
                if (desc != null)
                {
                    if (!string.IsNullOrEmpty(titulo))
                        desc.nome = titulo;
                    if (!string.IsNullOrEmpty(descricao))
                        desc.descricao = descricao;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateInput(false)]
        public JsonResult AtualizarStatusCard(int id_aba, int id_descricao, int coluna, bool disponivel)
        {
            using (BuffetContext db = new BuffetContext())
            {
                ORCAMENTO_PROPOSTA_DESCRICAO desc = db.ORCAMENTO_PROPOSTA_DESCRICAO.FirstOrDefault(e => e.id_orcamento_proposta_aba == id_aba && e.id_empresa == ID_EMPRESA && e.id == id_descricao);
                if (desc != null)
                {
                    desc.disponivel = disponivel;
                    desc.coluna = coluna;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EnviarProposta()
        {
            var propostas = new EnvioDePropostas()
                            {
                                id_empresa = ID_EMPRESA
                            };
            if (propostas.propostas.Count == 0)
            {
                base.setMensagem("Você precisa ter ao menos uma proposta cadastrada!");
                return RedirectToAction("CriarProposta");
            }
            return View(propostas);
        }

        public ActionResult EnviarPropostasCompartilhadas(string autenticacao_proposta, bool enviar_email, bool enviar_sms, bool data_vingente)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var proposta = db.ORCAMENTO_PROPOSTA.FirstOrDefault(e => e.autenticacao.Equals(autenticacao_proposta));

                var clientes = db.CLIENTEs.Where(e => e.email_marketing == true && e.id_empresa == ID_EMPRESA)
                                 .Select(e => new
                                 {
                                     e.id,
                                     e.nome,
                                     e.email,
                                     e.celular
                                 }).ToList();

                foreach (var cliente in clientes)
                {
                    string autenticacao = "";
                    string nome_proposta = "";
                    int dias = 0;

                    if (proposta != null)
                    {
                        nome_proposta = proposta.nome_proposta;
                        CLIENTE_PROPOSTA p = new CLIENTE_PROPOSTA()
                        {
                            autenticacao = UtilsManager.CriarAutenticacao(),
                            data_envio = DateTime.Today,
                            disponivel = true,
                            id_cliente = cliente.id,
                            id_empresa = ID_EMPRESA,
                            id_proposta = proposta.id,
                            disponivel_ate = DateTime.Now.AddDays(proposta.validade_dias),
                            proposta_de = "Administrativo"
                        };
                        autenticacao = p.autenticacao;
                        dias = (p.disponivel_ate - DateTime.Today).Days;
                        db.CLIENTE_PROPOSTA.Add(p);
                        db.SaveChanges();
                    }

                    string url = UtilsManager.EncurtarURL(string.Format("http://www.portal.buffetweb.com/Proposta/VisualizarProposta?autenticacao={0}", autenticacao));

                    if (enviar_email)
                    {
                        string mensagem = string.Format("Estamos ansiosos para fazer a sua festa! <br />  <br /> Clique no botão para visualizar nossa proposta <br /> e conhecer melhor nossos serviços de Buffet!");

                        EmailContent content = new EmailContent()
                        {
                            url = url,
                            emailContentButton = EmailContentButton.Visualizar,
                            emailContentHeader = EmailContentHeader.Proposta,
                            nomeOla = cliente.nome,
                            emailTemplate = TemplateEmail.TemplateEmpresa
                        };

                        EmailStatusSend statusEmail = new ContactManager(ID_EMPRESA, cliente.id).SendEmail(cliente.email, proposta.nome_proposta, mensagem, TipoContato.Proposta, autenticacao, content);
                        if (statusEmail == EmailStatusSend.Send)
                        {
                            db.SaveChanges();
                            new ContactManager(ID_EMPRESA).AddTimeLine(cliente.email, "Envio de proposta", string.Format("Proposta {0} enviada por email", proposta.nome_proposta), TimeLineIcon.Info);
                        }
                    }

                    if (enviar_sms)
                    {
                        string mensagem = string.Format("Ola {0}! Clique no link {1} p/ ver nossa proposta e conhecer melhor nossos servicos de Buffet!", cliente.nome.Split(' ')[0], url);

                        SMSStatusSend statusSMS = new ContactManager(ID_EMPRESA, cliente.id).SendSMS(cliente.celular, "Proposta Festa Infantil", mensagem, TipoContato.Proposta, autenticacao);
                        if (statusSMS == SMSStatusSend.Send)
                        {
                            db.SaveChanges();
                            new ContactManager(ID_EMPRESA).AddTimeLine(cliente.email, "Envio de proposta", string.Format("Proposta {0} enviada por sms", proposta.nome_proposta), TimeLineIcon.Info);
                        }
                    }
                }
            }

            base.setMensagem("Propostas enviadas com sucesso!");
            return RedirectToAction("EnviarProposta");
        }


        public JsonResult EnviarDadosSimplificados(int id_cliente, string tipo_envio, string periodo, DateTime? data, int? n_convidados, string status, string autenticacao_proposta)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    CLIENTE cliente = db.CLIENTEs.Include(p => p.CLIENTE_PROPOSTA).FirstOrDefault(e => e.id == id_cliente);
                    if (cliente != null)
                    {
                        string autenticacao = "";
                        bool existe = false;
                        string nome_proposta = "";
                        int dias = 0;
                        ORCAMENTO_PROPOSTA proposta = db.ORCAMENTO_PROPOSTA.FirstOrDefault(e => e.autenticacao.Equals(autenticacao_proposta));
                        if (proposta != null)
                        {
                            nome_proposta = proposta.nome_proposta;
                            CLIENTE_PROPOSTA r = db.CLIENTE_PROPOSTA.FirstOrDefault(e => e.id_proposta == proposta.id && e.id_cliente == cliente.id && e.convidados == n_convidados && e.data_evento == data && e.periodo == periodo);
                            if (r == null)
                            {
                                CLIENTE_PROPOSTA p = new CLIENTE_PROPOSTA()
                                 {
                                     autenticacao = UtilsManager.CriarAutenticacao(),
                                     convidados = n_convidados,
                                     data_envio = DateTime.Today,
                                     data_evento = data,
                                     disponivel = true,
                                     id_cliente = cliente.id,
                                     id_empresa = ID_EMPRESA,
                                     id_proposta = proposta.id,
                                     disponivel_ate = DateTime.Now.AddDays(proposta.validade_dias),
                                     proposta_de = "Administrativo",
                                     periodo = periodo
                                 };
                                autenticacao = p.autenticacao;
                                dias = (p.disponivel_ate - DateTime.Today).Days;
                                db.CLIENTE_PROPOSTA.Add(p);
                                db.SaveChanges();
                            }
                            else
                            {
                                r.disponivel_ate = DateTime.Now.AddDays(proposta.validade_dias);
                                r.data_envio = DateTime.Today;
                                autenticacao = r.autenticacao;
                                dias = (r.disponivel_ate - DateTime.Today).Days;
                                existe = true;
                                db.SaveChanges();
                            }
                        }

                        string url = UtilsManager.EncurtarURL(string.Format("http://www.portal.buffetweb.com/Proposta/VisualizarProposta?autenticacao={0}", autenticacao));

                        if (tipo_envio.Equals("email"))
                        {
                            string mensagem = string.Format("Estamos ansiosos para fazer a sua festa! <br />  <br /> Clique no botão para visualizar nossa proposta <br /> e conhecer melhor nossos serviços de Buffet!");

                            EmailContent content = new EmailContent()
                            {
                                url = url,
                                emailContentButton = EmailContentButton.Visualizar,
                                emailContentHeader = EmailContentHeader.Proposta,
                                nomeOla = cliente.nome,
                                emailTemplate = TemplateEmail.TemplateEmpresa
                            };

                            EmailStatusSend statusEmail = new ContactManager(ID_EMPRESA, cliente.id).SendEmail(cliente.email, proposta.nome_proposta, mensagem, TipoContato.Proposta, autenticacao, content);
                            if (statusEmail == EmailStatusSend.Send)
                            {
                                db.SaveChanges();
                                new ContactManager(ID_EMPRESA).AddTimeLine(cliente.email, "Envio de proposta", string.Format("Proposta {0} enviada por email", proposta.nome_proposta), TimeLineIcon.Info);
                                return Json(new { isOk = true, existe = existe, proposta = autenticacao, nome_proposta = nome_proposta, dias = dias }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string mensagem = string.Format("Ola {0}! Clique no link {1} p/ ver nossa proposta e conhecer melhor nossos servicos de Buffet!", cliente.nome.Split(' ')[0], url);

                            SMSStatusSend statusSMS = new ContactManager(ID_EMPRESA, cliente.id).SendSMS(cliente.celular, "Proposta Festa Infantil", mensagem, TipoContato.Proposta, autenticacao);
                            if (statusSMS == SMSStatusSend.Send)
                            {
                                db.SaveChanges();
                                new ContactManager(ID_EMPRESA).AddTimeLine(cliente.email, "Envio de proposta", string.Format("Proposta {0} enviada por sms", proposta.nome_proposta), TimeLineIcon.Info);
                                return Json(new { isOk = true, existe = existe, proposta = autenticacao, nome_proposta = nome_proposta, dias = dias }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AtualizaClienteSimplificados(int id_cliente, string celular, string email, bool isEmail)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE cliente = db.CLIENTEs.FirstOrDefault(e => e.id == id_cliente && e.id_empresa == ID_EMPRESA);
                if (cliente != null)
                {
                    var c = db.CLIENTEs.FirstOrDefault(e => e.id != id_cliente && e.email.Equals(email));
                    if (c != null)
                    {
                        return Json(new { isOk = false, mensagem = string.Format("Este email já esta sendo utilizado pelo cliente {0}!", c.nome) }, JsonRequestBehavior.AllowGet);
                    }
                    if (isEmail)
                        cliente.email = email;

                    cliente.celular = celular;
                    db.SaveChanges();
                    return Json(new { isOk = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { isOk = false, mensagem = "Este cliente não existe!" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AtualizarSimplificados(int id_cliente, string data, int? n_convidados, string status, string periodo, string autenticacao_proposta)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CLIENTE_PROPOSTA proposta = db.CLIENTE_PROPOSTA.FirstOrDefault(e => e.id_cliente == id_cliente && e.autenticacao.Equals(autenticacao_proposta));
                if (proposta != null)
                {
                    proposta.convidados = n_convidados;
                    proposta.autenticacao = autenticacao_proposta;
                    proposta.data_evento = Convert.ToDateTime(data);
                    proposta.disponivel = true;
                }
                return Json(new { isOk = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
