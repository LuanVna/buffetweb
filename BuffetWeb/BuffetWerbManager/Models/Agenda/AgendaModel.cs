﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Agenda
{
    public class AgendaModel
    {
        public int id_empresa { get; set; }
        public List<LOCAL_EVENTO> local_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(l => l.id_empresa == this.id_empresa).ToList();
                }
            }
        }

    }
}