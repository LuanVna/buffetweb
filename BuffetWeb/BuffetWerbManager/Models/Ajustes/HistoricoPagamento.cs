﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebCielo.Pedido;
using BuffetWebCielo.Transacao;

namespace BuffetWebManager.Models.Ajustes
{
    public class HistoricoPagamentoModel
    {
        public string StatusDescricao { get; set; }
        public TransacaoStatus Status { get; set; }
        public List<EMPRESA_HISTORICO_PAGAMENTO> Historico { get; set; }
    }

    public class HistoricoPagamento
    {
        public int id_empresa { get; set; }
        public string autenticacao { get; set; }

        public HistoricoPagamentoModel pagamentoAtual
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var pagamento = db.EMPRESA_HISTORICO_PAGAMENTO.Where(e => e.id_empresa == this.id_empresa && e.autenticacao_interna.Equals(autenticacao)).ToList();
                    if (pagamento.Count != 0)
                    {
                        Tuple<string, TransacaoStatus> status = VerificaStatusTransacao(pagamento.FirstOrDefault());
                        return new HistoricoPagamentoModel()
                        {
                            Historico = pagamento,
                            StatusDescricao = status.Item1,
                            Status = status.Item2
                        };
                    }
                }
                return null;
            }
        }

        public List<HistoricoPagamentoModel> historicoPagamento
        {
            get
            {
                List<HistoricoPagamentoModel> model = new List<HistoricoPagamentoModel>();
                using (BuffetContext db = new BuffetContext())
                {
                    var pagamentos = db.EMPRESA_HISTORICO_PAGAMENTO.Where(e => e.id_empresa == this.id_empresa).ToList();
                    foreach (var pagamento in pagamentos.Distinct())
                    {
                        if (pagamento.status != "Autorizado")
                        {
                            Tuple<string, TransacaoStatus> status = VerificaStatusTransacao(pagamento);
                            if (status.Item2 == TransacaoStatus.Autorizada)
	                        {
                                pagamentos.Where(e => e.autenticacao_externa.Equals(pagamento.autenticacao_externa)).ToList().ForEach(e => e.status = "Autorizado");
                                db.SaveChanges();
	                        }

                            model.Add(new HistoricoPagamentoModel()
                            {
                                Historico = pagamentos.Where(e => e.autenticacao_externa.Equals(pagamento.autenticacao_externa)).ToList(),
                                StatusDescricao = status.Item1,
                                Status = status.Item2
                            });
                        }
                        else
                        { 
                            model.Add(new HistoricoPagamentoModel()
                            {
                                Historico = pagamentos.Where(e => e.autenticacao_externa.Equals(pagamento.autenticacao_externa)).ToList(),
                                StatusDescricao = "Autorizado",
                                Status = TransacaoStatus.Autorizada
                            });
                        }
                    }
                }
                return model;
            }
        }

        private Tuple<string, TransacaoStatus> VerificaStatusTransacao(EMPRESA_HISTORICO_PAGAMENTO historico)
        {
            if (historico.cartao != null)
            {
                TransacaoStatus status = new TransacoesEfetuadas(this.id_empresa).Consultar(historico.autenticacao_externa).status;
                switch (status)
                {
                    case TransacaoStatus.Andamento: return new Tuple<string, TransacaoStatus>("Aguardando", TransacaoStatus.SemStatus);
                    case TransacaoStatus.Autorizada: return new Tuple<string, TransacaoStatus>("Aprovada", TransacaoStatus.SemStatus);
                    case TransacaoStatus.NaoAutorizada: return new Tuple<string, TransacaoStatus>("Não Autorizada", TransacaoStatus.SemStatus);
                }
                return new Tuple<string, TransacaoStatus>("Sem Status", TransacaoStatus.SemStatus);
            }
            else
            {
                //TODO: VERIFICAR BOLETO BANCARIO
                return new Tuple<string, TransacaoStatus>("Sem Status", TransacaoStatus.SemStatus);
            }
        }
    }
}