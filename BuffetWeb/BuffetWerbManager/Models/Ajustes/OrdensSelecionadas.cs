﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models
{
    public class OrdensSelecionadas
    {
        public int id_ordem { get; set; }
        public bool multipla_selecao { get; set; }
    }
}