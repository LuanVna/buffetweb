﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity; 
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Ajustes
{
    public class TipoEvento
    {
        public List<EVENTO> tipo_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EVENTOes.Include(t => t.CAMPOS_DISPONIVEIS).ToList();
                }
            }
        }

        public List<CAMPOS_DESCRICAO> campos_descricao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CAMPOS_DESCRICAO.Include(c => c.CAMPOS_COMPONENTES).ToList();
                }
            }
        }
    }
}