﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
using BuffetWebManager.Models;
using System.IO;
using LinqToExcel;
using System.Net;
using Newtonsoft.Json;
using BuffetWebData.Utils;
using System.Data.OleDb;
using System.Data;


namespace BuffetWerb.Models.Clientes
{
    public enum StatusLog
    {
        Sucesso,
        Informacao,
        Erro
    }

    public class ImportDone
    {
        public bool existe { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
        public string nome_cliente { get; set; }
        public Tuple<StatusLog, string> mensagem { get; set; }
    }

    public class LogImportClientes
    {
        public LogImportClientes()
        {
            this.importDone = new List<ImportDone>();
        }
        public int total { get; set; }
        public int importados { get; set; }
        public int duplicados { get; set; }
        public string tempoImportacao { get; set; }
        public bool confirmar { get; set; }
        public string mensagemErro { get; set; }
        public List<ImportDone> importDone { get; set; }
    }

    public class Clientes
    {
        public int id_empresa { get; set; }
        public Clientes(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public LogImportClientes ImportarClientes(HttpPostedFileBase file, bool confirmar, string path)
        {
            file.SaveAs(path);

            //string ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";

            //using (OleDbConnection conn = new System.Data.OleDb.OleDbConnection(ConnectionString))
            //{
            //    conn.Open();
            //    using (DataTable dtExcelSchema = conn.GetSchema("Tables"))
            //    { 
            //        string query = "SELECT * FROM Plan1$";
            //        OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
            //        DataSet ds = new DataSet();
            //        adapter.Fill(ds, "Items");
            //        if (ds.Tables.Count > 0)
            //        {
            //            if (ds.Tables[0].Rows.Count > 0)
            //            {
            //                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //                {
            //                }
            //            }
            //        }
            //    }
            //}

            LogImportClientes log = new LogImportClientes();
            DateTime myDate1 = DateTime.Now;

            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var clientesFile = new ExcelQueryFactory(file.FileName);

                    var clientes = (from x in clientesFile.Worksheet("Plan1")
                                    select new
                                    {
                                        nome = x["nome"],
                                        email = x["email"],
                                        telefone = x["telefone"],
                                        celular = x["celular"],
                                        cep = x["cep"],
                                        numero = x["numero"]
                                    }).ToList();

                    log.total = clientes.Count;
                    log.confirmar = confirmar;

                    List<CLIENTE> dbClientes = db.CLIENTEs.Where(e => e.id_empresa == this.id_empresa).ToList();

                    foreach (var cliente in clientes)
                    {
                        if (string.IsNullOrEmpty(cliente.nome) || (string.IsNullOrEmpty(cliente.email) && string.IsNullOrEmpty(cliente.celular)))
                        {
                            log.importDone.Add(new ImportDone()
                            {
                                email = cliente.email,
                                nome_cliente = cliente.nome,
                                mensagem = new Tuple<StatusLog, string>(StatusLog.Erro, string.Format("Você precisa informar email e/ou telefone no minimo do cliente {0}", cliente.nome))
                            });
                            continue;
                        }

                        var existeCliente = dbClientes.FirstOrDefault(e => e.email == cliente.email);
                        if (existeCliente == null)
                        {
                            log.importados++;

                            CLIENTE c = new CLIENTE();
                            c.nome = cliente.nome;
                            c.telefone = cliente.telefone;
                            c.celular = cliente.celular;
                            c.email = cliente.email;
                            c.desde = DateTime.Today;
                            c.cliente_de = "ADMINISTRATIVO";
                            c.email_marketing = false;
                            c.autenticacao = UtilsManager.CriarAutenticacao();
                            if (cliente.cep.ToString().Trim() != "" && cliente.numero.ToString().Trim() != "")
                            {
                                Tuple<bool, string, EnderecoCliente> endereco = this.enderecoPorCEP(cliente.cep);
                                if (endereco.Item1)
                                {
                                    //{"status":1,"code":"05878-040","state":"SP","city":"S\u00e3o Paulo","district":"Parque Independ\u00eancia","address":"Rua Ermano de Stradelli"}
                                    EnderecoCliente e = endereco.Item3;
                                    c.ENDERECO = new ENDERECO()
                                    {
                                        cep = cliente.cep,
                                        cidade = e.city,
                                        endereco1 = e.address,
                                        estado = e.state,
                                        numero = cliente.numero,
                                        bairro = e.district,
                                        id_empresa = this.id_empresa
                                    };
                                }
                                else
                                {
                                    log.importDone.Add(new ImportDone()
                                    {
                                        email = cliente.email,
                                        celular = cliente.celular,
                                        nome_cliente = cliente.nome,
                                        mensagem = new Tuple<StatusLog, string>(StatusLog.Informacao, string.Format("Cliente adicionado, mas não foi encontrado nenhum endereço com este CEP {0}", cliente.cep))
                                    });
                                }
                            }
                            db.CLIENTEs.Add(c);
                            string msOkOrError = "";
                            bool eOrD = true;
                            if (confirmar)
                            {
                                //EmailStatusSend status = new ContactManager(this.id_empresa, c.id).SendEmail(c.email, "", "", TipoContato.ConfirmacaoCadastro, c.autenticacao, new EmailContent()
                                //  {
                                //      emailContentButton = EmailContentButton.Visualizar,
                                //      emailContentHeader = EmailContentHeader.Confirmar,
                                //      emailTemplate = TemplateEmail.TemplateEmpresa,
                                //      nomeOla = c.nome,
                                //      url = "http://www.email.buffetweb.com.br/EmailMarketing/ConfirmarCadastro?autenticacao=" + c.autenticacao
                                //  });

                                EmailStatusSend status = EmailStatusSend.Send;
                                if (status == EmailStatusSend.Send)
                                {
                                    msOkOrError = "<br /><small>Email de confirmação enviado com sucesso!</small>";
                                    c.email_marketing = true;
                                }
                                else
                                {
                                    msOkOrError = "<br /><small>Ouve um erro ao tentar enviar o email de confirmação!</small>";
                                    eOrD = false;
                                }
                            }

                            log.importDone.Add(new ImportDone()
                            {
                                email = cliente.email,
                                nome_cliente = cliente.nome,
                                celular = cliente.celular,
                                mensagem = new Tuple<StatusLog, string>(eOrD ? StatusLog.Sucesso : StatusLog.Informacao, string.Format("Cliente {0} Adicionado com sucesso! {1}", cliente.nome, "\n" + msOkOrError))
                            });
                        }
                        else
                        {
                            log.duplicados++;
                            log.importDone.Add(new ImportDone()
                            {
                                email = cliente.email,
                                nome_cliente = cliente.nome,
                                celular = cliente.celular,
                                mensagem = new Tuple<StatusLog, string>(StatusLog.Informacao, string.Format("Este email Já esta associado ao cliente {0} desde {1}", existeCliente.nome, existeCliente.desde))
                            });
                        }
                    }
                    db.SaveChanges();

                    DateTime myDate2 = DateTime.Now;
                    TimeSpan myDateResult;
                    myDateResult = myDate2 - myDate1;
                    log.tempoImportacao = myDateResult.TotalSeconds.ToString();
                }
            }
            catch (Exception e)
            {
                log.mensagemErro = e.Message;
            }
            return log;
        }


        private Tuple<bool, string, EnderecoCliente> enderecoPorCEP(string cep)
        {
            dynamic endereco = new WebClient().DownloadString("http://apps.widenet.com.br/busca-cep/api/cep.json?code=" + cep);
            try
            {
                if (endereco.message == null)
                {
                    return new Tuple<bool, string, EnderecoCliente>(true, "Ok", JsonConvert.DeserializeObject<EnderecoCliente>(endereco));
                }
            }
            catch (Exception)
            {
            }
            return new Tuple<bool, string, EnderecoCliente>(false, string.Format("Endereço para o CEP: {0} não encontrado", cep), null);
        }
    }

    public class EnderecoCliente
    {
        //{"status":1,"code":"05878-040","state":"SP","city":"S\u00e3o Paulo","district":"Parque Independ\u00eancia","address":"Rua Ermano de Stradelli"}
        public string status { get; set; }
        public string code { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string address { get; set; }
    }
}