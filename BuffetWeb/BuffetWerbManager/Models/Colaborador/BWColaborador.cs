﻿
using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;

namespace BuffetWebManager.Models
{
    public class BWColaborador
    {
        private static int? idColaborador()
        {
            try
            {
                return Convert.ToInt32(HttpContext.Current.User.Identity.Name);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool isLogin()
        {
            if (getColaborador() != null)
                return true;
            return false;
        }

        public static int isLoginID()
        {
            return getColaborador().id;
        }

        public static COLABORADORE getColaborador()
        {
            int? _idAdministrador = idColaborador();
            if (_idAdministrador == null)
            {
                return null;
            }
            else
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        COLABORADORE administrador = db.COLABORADORES.Include(p => p.COLABORADOR_NOTIFICACAO)
                                                                     .Include(n => n.NOTIFICACOES)
                                                                     .Include(p => p.PERFIL_COLABORADOR)
                                                                     .Include(d => d.COLABORADOR_DEVICES).FirstOrDefault(a => a.id == _idAdministrador);
                        if (administrador != null)
                        {
                            //if (sessao != null)
                            //{
                            //    if (administrador.sessao.Equals(sessao))
                            //    {
                            //        sessao_alterada = false;
                            return administrador;
                            //}
                            //else
                            //{
                            //    sessao_alterada = true;
                            //    return null;
                            //}
                            //}
                            //sessao_atual = null;
                            //return null;
                        }
                        return administrador;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public static bool sessao_alterada { get; set; }

        private static string sessao
        {
            get
            {
                if (sessao_atual == null)
                {
                    sessao_atual = HttpContext.Current.Session.SessionID;
                }
                return sessao_atual;
            }
        }

        private static string sessao_atual { get; set; }
        public static bool AutenticaColaborador(string usuario, string Senha)
        {
            using (BuffetContext db = new BuffetContext())
            {
                try
                {
                    COLABORADORE colaborador = db.COLABORADORES.FirstOrDefault(u => u.email.ToLower() == usuario.ToLower() && u.senha == Senha);
                    if (colaborador == null)
                        return false;
                    colaborador.sessao = sessao;
                    db.SaveChanges();

                    FormsAuthentication.SetAuthCookie(colaborador.id.ToString(), false);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public static void Deslogar()
        {
            FormsAuthentication.SignOut();
        }
    }
}