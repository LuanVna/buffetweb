﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Colaborador
{
    public class ColaboradorSelecionado
    {
        public int id_colaborador { get; set; }
        public bool colaborador_selecionado { get; set; }
    }
}