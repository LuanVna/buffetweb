﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
 

namespace BuffetWebManager.Models.Colaboradores
{
    public class Colaboradores
    { 
        public List<PERFIL_COLABORADOR> perfis
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PERFIL_COLABORADOR.Where(i => i.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                } 
            }
        }

        public List<COLABORADORE> colaboradores
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.COLABORADORES.Include(c => c.PERFIL_COLABORADOR).Where(i => i.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                }
            }
        } 
    }
}