﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Convite
{
    public class ConviteModel
    {
        public int id_empresa { get; set; }
        public int? id_orcamento { get; set; }
        public int? id_cliente { get; set; }
        public string codigo_unico { get; set; }
        public bool editando
        {
            get
            {
                return this.codigo_unico != null;
            }
        }

        public ORCAMENTO orcamento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTOes.Include(a => a.CONVITE_ANIVERSARIANTES).Include(c => c.CLIENTE).Include(l => l.LOCAL_EVENTO.ENDERECO).Include(t => t.CONVITE_TEMPLATE).FirstOrDefault(d => d.codigo_unico.Equals(codigo_unico) && d.id_empresa == this.id_empresa);
                }
            }
        }

        public CONVITE_TEMPLATE convite
        {
            get
            {
                if (this.editando)
                {
                    return this.orcamento.CONVITE_TEMPLATE.FirstOrDefault();
                }
                return null;
            }
        }

        public B_CONVITES_DISPONIVEIS disponivel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_CONVITES_DISPONIVEIS.FirstOrDefault(e => e.id == this.convite.id_convite);
                }
            }
        }

        public CLIENTE cliente
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (this.orcamento != null)
                    {

                        return db.CLIENTEs.FirstOrDefault(e => e.id == this.orcamento.CLIENTE.id); 
                    }
                    else
                    {
                        return db.CLIENTEs.FirstOrDefault(e => e.id == this.id_cliente);
                    }
                } 
            }
        }

        public List<LOCAL_EVENTO> localEvento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public LOCAL_EVENTO localSelecionado
        {
            get
            {
                return orcamento.LOCAL_EVENTO;
            }
        } 
    }
}