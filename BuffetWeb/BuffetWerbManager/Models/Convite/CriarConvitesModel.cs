﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Convite
{
    public class CriarConvitesModel
    {
        public int id_empresa { get; set; }
        public string filtro { get; set; }

        public List<CLIENTE> clientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CLIENTEs.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ORCAMENTO> convites
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTOes.Include(c => c.CLIENTE).Include(f => f.LOCAL_EVENTO).Where(e => e.apenas_convite == true && e.id_empresa == this.id_empresa).ToList();
                }
            }
        } 
    }
}