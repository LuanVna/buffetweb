﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Convite
{
    public class PortariaModel
    {
        public int? id_orcamento { get; set; }
        public int id_empresa { get; set; }
        public ORCAMENTO orcamento
        {
            get
            {
                if (id_orcamento != null)
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        return db.ORCAMENTOes.Include(e => e.EVENTO).Include(c => c.CLIENTE).Include(e => e.CONVITE_CONVIDADOS.Select(f => f.CONVITE_CONVIDADOS_FAMILIARES)).FirstOrDefault(e => e.id == this.id_orcamento && e.id_empresa == this.id_empresa);
                    }
                }
                return null;
            }
        }

        public List<ORCAMENTO> orcamentos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTOes.Include(c => c.CLIENTE).Where(e => e.id_empresa == this.id_empresa && e.CONVITE_CONVIDADOS.Count > 0).OrderBy(d => d.data_evento).ToList();
                }
            }
        }

    }
}