﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
 

namespace BuffetWebManager.Models.Estoque
{
    public class ListarIngredientes
    {
        public List<INGREDIENTE> ingredientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.INGREDIENTES.Include(c => c.UNIDADE_MEDIDA).Include(d => d.FORNECEDOR_INGREDIENTE).Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }
        
        public List<UNIDADE_MEDIDA> unidade_medida
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.UNIDADE_MEDIDA.ToList();
                }
            }
        }

        public List<FORNECEDOR> fornecedores
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.FORNECEDORs.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public bool MODULO_ESTOQUE
        {
            get
            {
                return Convert.ToBoolean(PerfilEmpresa.MODULO_ESTOQUE);
            }
        }

        public int id_empresa { get; set; }
    } 
}