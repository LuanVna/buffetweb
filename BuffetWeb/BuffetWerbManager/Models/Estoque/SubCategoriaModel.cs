﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Estoque
{
    public class SubCategoriaModel
    {
        public int id_sub_categoria { get; set; }
        public string nome_subcategoria { get; set; } 
        public int id_categoria_itens { get; set; } 
        public Decimal rendimento_pessoa { get; set; } 
        public int id_unidade_medida { get; set; } 
    }
}