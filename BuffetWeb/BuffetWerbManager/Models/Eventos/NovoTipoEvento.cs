﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity; 

namespace BuffetWebManager.Models.Eventos
{
    public class NovoTipoEvento
    {
        public int id_empresa { get; set; }
        public NovoTipoEvento()
        {
            this.id_empresa = this.id_empresa;
        }

        public List<CATEGORIA_ITENS> categoria
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ITENS.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CATEGORIA_DIVERSOS> categorias_diversos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_DIVERSOS.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CATEGORIA_SERVICOS> categorias_servicos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_SERVICOS.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }
        public List<CATEGORIA_ALUGUEL> categorias_alugueis
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ALUGUEL.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }  

        public List<PRESTADOR_FUNCAO> prestador_funcao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PRESTADOR_FUNCAO.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        //public List<TIPO_EVENTO> tipo_evento
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.TIPO_EVENTO.Include(c => c.CAMPOS_DISPONIVEIS).Include(e => e.PACOTES_EVENTOS).Where(f => f.id_empresa == this.id_empresa).ToList();
        //        }
        //    }
        //}
        public List<CAMPOS_DESCRICAO> campo_descricao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CAMPOS_DESCRICAO.ToList();
                }
            }
        }

        public List<PACOTE> pacotes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PACOTES.Where(f => f.id_empresa == this.id_empresa && f.status == true).ToList();
                }
            }
        }

        public List<ITENS_SERVICOS> servicos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_SERVICOS.Where(f => f.id_empresa == this.id_empresa && f.status == true).ToList();
                }
            }
        }

        public List<ITENS_ALUGUEL> aluguel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_ALUGUEL.Where(f => f.id_empresa == this.id_empresa && f.status == true).ToList();
                }
            }
        }

        public List<ITENS_DIVERSOS> diversos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_DIVERSOS.Where(f => f.id_empresa == this.id_empresa && f.status == true).ToList();
                }
            }
        }
    }
}