﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Financeiro
{
    public class formasDePagamento
    {
        public string cielo { get; set; }
        public string credencial_cielo { get; set; }
        public string brasil { get; set; }
        public string codigo_banco_brasil { get; set; }

        public string boleto { get; set; }
        public string boleto_agencia { get; set; }
        public string boleto_conta { get; set; }
        public string boleto_codigo { get; set; }
    }
}