﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Index
{
    public class Index
    {
        public int id_empresa { get; set; }
        public List<ORCAMENTO> orcamento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (PerfilEmpresa.MODULO_ORCAMENTO)
                    {
                        DateTime now = DateTime.Now;
                        List<ORCAMENTO> o = db.ORCAMENTOes.Where(d => d.data_criacao.Month == now.Month && d.data_criacao.Year == now.Year && d.id_empresa == this.id_empresa).ToList();
                        return o;
                    }
                    else
                    {
                        return db.ORCAMENTOes.Where(e => e.apenas_convite == true && e.id_empresa == this.id_empresa).ToList();
                    }
                }
            }
        }

        public List<CLIENTE_PROPOSTA> propostas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CLIENTE_PROPOSTA.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CONVITE_CONVIDADOS> convidados
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CONVITE_CONVIDADOS.Include(e => e.CONVITE_CONVIDADOS_FAMILIARES).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        //public List<LOCAL_EVENTO> local_evento
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.LOCAL_EVENTO.ToList();
        //        }
        //    }
        //} 
    }
}