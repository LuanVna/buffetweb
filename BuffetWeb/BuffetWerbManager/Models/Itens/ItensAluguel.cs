﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Itens
{
    public class ItensAluguel
    { 
        public int id_empresa { get; set; }
        public List<ITENS_ALUGUEL> itens_aluguel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                   return db.ITENS_ALUGUEL.Include(c => c.CATEGORIA_ALUGUEL).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CATEGORIA_ALUGUEL> categoria_aluguel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ALUGUEL.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<FORNECEDOR> fornecedores
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.FORNECEDORs.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }  
    }
}