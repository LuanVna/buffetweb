﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Itens
{
    public class ItensDiversos
    {
        public int id_empresa { get; set; }
        public List<ITENS_DIVERSOS> itens_diversos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_DIVERSOS.Include(e => e.CATEGORIA_SERVICOS).Include(d => d.UNIDADE_MEDIDA).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CATEGORIA_DIVERSOS> categorias
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_DIVERSOS.Where(e => e.id_empresa == this.id_empresa).ToList();
                } 
            }
        }

        public List<UNIDADE_MEDIDA> unidades
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.UNIDADE_MEDIDA.ToList();
                }  
            }
        }
    }
}