﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Itens
{
    public class ItensServico
    {
        public int id_empresa { get; set; }
        public List<ITENS_SERVICOS> itens_servico
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_SERVICOS.Include(c => c.CATEGORIA_SERVICOS).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CATEGORIA_SERVICOS> categorias_servicos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_SERVICOS.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<PRESTADOR_FUNCAO> prestador_funcao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PRESTADOR_FUNCAO.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }
    }
}