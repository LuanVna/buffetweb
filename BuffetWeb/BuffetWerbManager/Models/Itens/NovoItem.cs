﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models; 
using System.Data.Entity; 

namespace BuffetWebManager.Models.Itens
{
    public class NovoItem
    {
        public int id_empresa { get; set; }
        public List<CATEGORIA_ITENS> categoria
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ITENS.Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<SUB_CATEGORIA_ITENS> sub_categoria
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.SUB_CATEGORIA_ITENS.Include(e => e.UNIDADE_MEDIDA).Include(f => f.CATEGORIA_ITENS).Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<UNIDADE_MEDIDA> unidades
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.UNIDADE_MEDIDA.ToList();
                }
            }
        }
        public List<ITENS_CARDAPIO> cardapios
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_CARDAPIO.Include(c => c.CATEGORIA_ITENS).Include(e => e.SUB_CATEGORIA_ITENS).Where(i => i.id_empresa == this.id_empresa).OrderByDescending(i => i.id).Take(20).ToList();
                }
            }
        }

        public List<INGREDIENTE> ingredientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.INGREDIENTES.Include(c => c.UNIDADE_MEDIDA).Where(i => i.id_empresa == this.id_empresa).OrderBy(n => n.marca).ToList();
                }
            }
        }

        public List<FORNECEDOR> fornecedores
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.FORNECEDORs.Where(f => f.id_empresa == this.id_empresa).ToList();
                }
            }
        }
         

        public List<ITENS_CARDAPIO> itens_cardapio
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_CARDAPIO.Include(c => c.CATEGORIA_ITENS).Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ITENS_SERVICOS> itens_servico
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_SERVICOS.Include(c => c.CATEGORIA_SERVICOS).Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ITENS_ALUGUEL> itens_aluguel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_ALUGUEL.Include(c => c.CATEGORIA_ALUGUEL).Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ITENS_DIVERSOS> itens_diversos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_DIVERSOS.Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }
          
        private List<ITENS_CARDAPIO> cardapio(string categoria)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.ITENS_CARDAPIO.Where(c => c.CATEGORIA_ITENS.nome.Equals(categoria)).ToList();
            }
        }

    }
}