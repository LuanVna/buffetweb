﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace BuffetWebManager.Models.Orcamento
{
    public class BuscarOrcamentos
    {
        public int id_empresa { get; set; }
        public string tipo { get; set; }
        public int? filtro { get; set; }

        public List<ORCAMENTO> orcamentos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (filtro != 0)
                    {
                        if (filtro == null) filtro = 30;
                        DateTime afterDays = DateTime.Now.Date;
                        if (PerfilEmpresa.MODULO_ORCAMENTO)
                        {
                            return db.ORCAMENTOes.Include(c => c.CLIENTE)
                                                 .Include(c => c.EVENTO).Include(l => l.LOCAL_EVENTO)
                                                 .Where(d => d.id_empresa == this.id_empresa && SqlFunctions.DateDiff("day", d.data_evento, afterDays) <= filtro).ToList();
                        }
                        else
                        {
                            return db.ORCAMENTOes.Include(c => c.CLIENTE)
                                            .Include(c => c.EVENTO).Include(l => l.LOCAL_EVENTO)
                                            .Where(d => d.id_empresa == this.id_empresa && d.apenas_convite == true && SqlFunctions.DateDiff("day", d.data_evento, afterDays) <= filtro).ToList();

                        }
                    }
                    else
                    {
                        if (PerfilEmpresa.MODULO_ORCAMENTO)
                        {
                            return db.ORCAMENTOes.Include(c => c.CLIENTE)
                                                .Include(c => c.EVENTO)
                                                .Include(l => l.LOCAL_EVENTO)
                                                .Where(d => d.id_empresa == this.id_empresa).ToList();
                        }
                        else
                        {
                            return db.ORCAMENTOes.Include(c => c.CLIENTE).Include(c => c.EVENTO)
                                                .Include(l => l.LOCAL_EVENTO)
                                                .Where(d => d.id_empresa == this.id_empresa && d.apenas_convite == true).ToList();
                        }
                    }
                }
            }
        }

        public List<LOCAL_EVENTO> locais_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(l => l.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<CLIENTE> clientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CLIENTEs.Where(c => c.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<EVENTO> tipo_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EVENTOes.Where(c => c.id_empresa == this.id_empresa).ToList();
                }
            }
        }
    }
}