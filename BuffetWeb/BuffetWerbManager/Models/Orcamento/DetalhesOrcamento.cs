﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;
 

namespace BuffetWebManager.Models.Orcamento
{
    public class DetalhesOrcamento
    {
        public string codigo_unico { get; set; }
        public int id_empresa { get; set; }
        public ORCAMENTO orcamento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    ORCAMENTO orcament = db.ORCAMENTOes.Include(it => it.ORCAMENTO_ITENS.Select(ii => ii.ITENS_CARDAPIO.CATEGORIA_ITENS))
                                          .Include(l => l.EVENTO)
                                          .Include(i => i.ORCAMENTO_ITENS)
                                          .Include(v => v.ORCAMENTO_VISITAS)
                                          .Include(c => c.CLIENTE)
                                          .Include(co => co.CONVITE_CONVIDADOS)
                                          .Include(t => t.CONVITE_TEMPLATE)
                                          .Include(it => it.ORCAMENTO_ITENS)
                                          .Include(a => a.ORCAMENTO_ANOTACOES.Select(c => c.COLABORADORE))
                                          .Include(lo => lo.LOCAL_EVENTO)
                                          .Include(f => f.FINANCEIRO_P_DEBITO)
                                          .Include(f => f.FINANCEIRO_P_CHEQUE)
                                          .Include(f => f.FINANCEIRO_P_DEPOSITO)
                                          .Include(f => f.FINANCEIRO_P_BOLETO)
                                          .Include(d => d.FINANCEIRO_P_ESPECIE) 
                                          .Include(c => c.FINANCEIRO_P_CREDITO.Select(b => b.FINANCEIRO_BANDEIRAS)) .FirstOrDefault(i => i.codigo_unico.Equals(this.codigo_unico) && i.id_empresa == this.id_empresa);
                    return orcament;
                }
            }
        }

        public List<CATEGORIA_ITENS> categorias_itens
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ITENS.Where(c => c.id_empresa == this.id_empresa).ToList();
                }
            } 
        }

        //public List<HORARIO_DISPONIVEIS> horarios_visitas
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.HORARIO_DISPONIVEIS.Where(o => o.id_empresa == this.id_empresa).ToList();
        //        }
        //    }
        //}
        public List<LOCAL_EVENTO> local_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(o => o.id_empresa == this.id_empresa).ToList();
                }
            }
        }


        public List<CAMPOS_DESCRICAO> campos_detalhes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    List<CAMPOS_DISPONIVEIS> c = db.CAMPOS_DISPONIVEIS.Where(d => d.id_tipo_evento == this.orcamento.id_tipo_evento).ToList();
                    List<CAMPOS_DESCRICAO> campos = new List<CAMPOS_DESCRICAO>();
                    foreach (var disponivel in c)
                    {
                        campos.Add(db.CAMPOS_DESCRICAO.Include(i => i.CAMPOS_COMPONENTES).FirstOrDefault(cc => cc.id == disponivel.id_campos_descricao));
                    }
                    return campos;
                }
            }
        }

       
    }
}