﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Orcamento
{
    public class IngredientesCardapio
    {
        public int id_ingrediente { get; set; }
        public decimal valor_total { get; set; }
        public decimal quantidade { get; set; }
    }
}