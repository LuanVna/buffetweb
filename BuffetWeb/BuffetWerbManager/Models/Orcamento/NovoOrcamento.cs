﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Orcamento
{
    public class NovoOrcamento
    {
        public List<CLIENTE> clientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CLIENTEs.Where(i => i.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                }
            }
        }

        public List<EVENTO> tipo_eventos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EVENTOes.Where(i => i.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                }
            }
        }
    }
}