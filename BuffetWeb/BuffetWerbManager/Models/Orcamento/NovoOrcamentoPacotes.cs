﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Orcamento
{
    public class NovoOrcamentoPacotes
    {
        public int id_pacote { get; set; }
        public string nome_pacote { get; set; }
        public bool permitir_personalizar { get; set; }
    }
}