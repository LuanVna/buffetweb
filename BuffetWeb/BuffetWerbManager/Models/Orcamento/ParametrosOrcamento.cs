﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Orcamento
{
    public class ParametrosOrcamento
    {
        public string codigo_unico { get; set; }
        public int? id_cliente { get; set; }
        public int? id_local_evento { get; set; }
        public int? id_tipo_evento { get; set; }
        public string status { get; set; } 
    }
}