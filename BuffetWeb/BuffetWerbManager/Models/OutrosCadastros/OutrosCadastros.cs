﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.OutrosCadastros
{
    public class OutrosCadastros
    {
        public OutrosCadastros()
        {
            this.tipo_evento = new List<EVENTO>();
            this.unidade_itens = new List<UNIDADE_MEDIDA>();
            this.categoria_itens = new List<CATEGORIA_ITENS>();
        }
        public List<EVENTO> tipo_evento { get; set; }
        public List<UNIDADE_MEDIDA> unidade_itens { get; set; }
        public List<CATEGORIA_ITENS> categoria_itens { get; set; }
    }
}