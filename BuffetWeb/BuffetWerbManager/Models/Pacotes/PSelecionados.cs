﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Pacotes
{

    public class PSelecionados
    {
        public int? max_itens { get; set; }
        public bool? editavel { get; set; } 
        public List<Itens> itens { get; set; }
    }

    public class Itens
    {
        public int? id_cardapio { get; set; }
        public bool isSelect { get; set; }
    } 
}