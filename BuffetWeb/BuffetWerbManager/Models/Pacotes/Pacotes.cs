﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;


namespace BuffetWebManager.Models.Pacotes
{
    public class Pacotes
    { 
        public int id_empresa { get; set; }
        public List<PACOTE> pacotes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PACOTES.Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<PACOTES_ITENS> itens_pacotes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PACOTES_ITENS.Where(i => i.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ITENS_CARDAPIO> itens_cardapio
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_CARDAPIO.Include(c => c.CATEGORIA_ITENS).Where(i => i.id_empresa == this.id_empresa && i.status == true).ToList();
                }
            }
        }

        public List<ITENS_SERVICOS> itens_servico
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_SERVICOS.Include(c => c.CATEGORIA_SERVICOS).Where(i => i.id_empresa == this.id_empresa && i.status == true).ToList();
                }
            }
        }

        public List<ITENS_ALUGUEL> itens_aluguel
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_ALUGUEL.Include(c => c.CATEGORIA_ALUGUEL).Where(i => i.id_empresa == this.id_empresa && i.status == true).ToList();
                }
            }
        }

        public List<ITENS_DIVERSOS> itens_diversos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ITENS_DIVERSOS.Where(i => i.id_empresa == this.id_empresa && i.status == true).ToList();
                }
            }
        }


        public List<CATEGORIA_ITENS> categoria
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.CATEGORIA_ITENS.Where(e => e.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                }
            }
        }

        //public List<ITENS_DIVERSOS> itens_diversos
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.ITENS_DIVERSOS.ToList();
        //        }
        //    }
        //}

        //public List<ITENS_ALUGUEL> itens_aluguel
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.ITENS_ALUGUEL.ToList();
        //        }
        //    }
        //}

        //public List<ITENS_SERVICO> itens_servico
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.ITENS_SERVICO.ToList();
        //        }
        //    }
        //}

        private List<ITENS_CARDAPIO> cardapio(string categoria)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.ITENS_CARDAPIO.Where(c => c.CATEGORIA_ITENS.nome.Equals(categoria)).ToList();
            }
        }

    }
}