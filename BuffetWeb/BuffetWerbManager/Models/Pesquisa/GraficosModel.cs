﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Pesquisa
{
    public class GraficosModel
    {
        public int id_pesquisa { get; set; }
        public int id_empresa { get; set; }
        public PESQUISA_PESQUISAS pesquisa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.PESQUISA_PESQUISAS.Include(o => o.ORCAMENTO).Include(p => p.PESQUISA_PERGUNTA.Select(f => f.PESQUISA_RESPOSTA.Select(c => c.CONVITE_CONVIDADOS))).FirstOrDefault(p => p.id == this.id_pesquisa && p.id_empresa == this.id_empresa && p.enviada == true);
                }
            }
        }

    }
}