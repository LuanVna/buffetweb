﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuffetWebData.Models;
using System.Data.Entity;

namespace BuffetWebManager.Models.Pesquisa
{
    public class PesquisaModel
    {
        public int id_empresa { get; set; }
        public string filtro { get; set; }

        public List<PESQUISA_PESQUISAS> pesquisas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    if (filtro != "0")
                    {
                        //Filtra as pesquisas
                        if (filtro == null) filtro = "30";
                        DateTime afterDays = DateTime.Now.AddDays(double.Parse(filtro));
                        return db.PESQUISA_PESQUISAS.Include(g => g.PESQUISA_PERGUNTA).Include(o => o.ORCAMENTO.CLIENTE).Where(e => e.id_empresa == this.id_empresa && afterDays >= e.ORCAMENTO.data_evento).ToList();
                    }
                    else
                    {
                        //Desde o inicio
                        return db.PESQUISA_PESQUISAS.Include(g => g.PESQUISA_PERGUNTA).Include(o => o.ORCAMENTO.CLIENTE).Where(e => e.id_empresa == this.id_empresa).ToList();
                    }
                }
            }
        }

        public List<ORCAMENTO> orcamentos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.ORCAMENTOes.Include(c => c.CLIENTE).Include(p => p.PESQUISA_PESQUISAS).Where(o => o.id_empresa == this.id_empresa && o.PESQUISA_PESQUISAS.Count == 0).ToList();
                }
            }
        }
    }
}