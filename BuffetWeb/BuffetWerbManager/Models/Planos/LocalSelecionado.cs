﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuffetWebManager.Models.Planos
{
    public class LocalSelecionado
    {
        public int id_local_evento { get; set; }
        public bool selecionado { get; set; }
    }
}