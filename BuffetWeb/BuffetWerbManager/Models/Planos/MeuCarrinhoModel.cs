﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Planos
{
    public class MeuCarrinhoModel
    {
        public int id_empresa { get; set; }
        public List<EMPRESA_CARRINHO> Carrinho
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESA_CARRINHO.Include(l => l.LOCAL_EVENTO)
                                                   .Include(p => p.B_PLANOS_DISPONIVEIS)
                                                   .Where(e => e.id_empresa == this.id_empresa && e.comprado == false).ToList();
                }
            }
        }

        public List<EMPRESA_CARRINHO_CUPOM> Codigos
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESA_CARRINHO_CUPOM.Include(p => p.B_PLANOS_DISPONIVEIS)
                                                    .Include(l => l.LOCAL_EVENTO)
                                                    .Where(e => e.id_empresa == this.id_empresa && e.usado == false && DateTime.Today <= e.disponivel_ate).ToList();
                }
            }
        }
    }
}