﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using BuffetWebCielo.Pedido;

namespace BuffetWebManager.Models.Planos
{
    public class RespostaTransacaoCarrinho
    {
        public bool isOk { get; set; }
        public string mensagem { get; set; }
        public string redirect { get; set; }
        public string autenticacao { get; set; }
    }

    public class MeuCarrinhoTransacao
    {
        public int id_empresa { get; set; }
        public RespostaTransacaoCarrinho EfetuarPagamento(DadosPedido dadosPedido)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<EMPRESA_CARRINHO> itens = db.EMPRESA_CARRINHO.Include(l => l.LOCAL_EVENTO).Include(p => p.B_PLANOS_DISPONIVEIS).Where(e => e.id_empresa == this.id_empresa && e.comprado == false).ToList();
                List<EMPRESA_CARRINHO_CUPOM> cupons = db.EMPRESA_CARRINHO_CUPOM.Where(e => e.id_empresa == this.id_empresa && e.usado == false).ToList();

                Decimal subTotal = 0;
                Decimal desconto = 0;
                foreach (var item in itens)
                {
                    switch (item.periodo)
                    {
                        case "MENSAL": subTotal += item.B_PLANOS_DISPONIVEIS.valor_mensal; break;
                        case "TRIMESTRAL": subTotal += item.B_PLANOS_DISPONIVEIS.valor_trimestral; break;
                        case "SEMESTRAL": subTotal += item.B_PLANOS_DISPONIVEIS.valor_semestral; break;
                        case "ANUAL": subTotal += item.B_PLANOS_DISPONIVEIS.valor_anual; break;
                    }

                    var cupom = cupons.FirstOrDefault(e => e.periodo.Equals(item.periodo) && e.id_local_evento == item.id_local_evento && e.id_planos_disponiveis == item.id_plano_disponivel);
                    if (cupom != null)
                    {
                        desconto += cupom.valor_desconto;
                        cupom.usado = true;
                    }
                }

                dadosPedido.CallToken = true;
                dadosPedido.Parcelas = 1;
                dadosPedido.ValorTotal = (subTotal - desconto);
                PedidoTransacao resposta = new NovoPedido(-1).EfetuarTransacao(dadosPedido);
                if (resposta.transacaoStatus == TransacaoStatus.Capturada)
                {
                    if (this.VerificaInformacoesProdutos(resposta, itens))
                    {
                        foreach (var item in itens)
                        {
                            var cupom = cupons.FirstOrDefault(e => e.periodo.Equals(item.periodo) && e.id_local_evento == item.id_local_evento && e.id_planos_disponiveis == item.id_plano_disponivel);
                            int? id_cupom = null;
                            if (cupom != null)
                                id_cupom = (int)cupom.id;

                            item.comprado = true;

                            Decimal valor = 0;
                            switch (item.periodo)
                            {
                                case "MENSAL": valor = item.B_PLANOS_DISPONIVEIS.valor_mensal; break;
                                case "TRIMESTRAL": valor = item.B_PLANOS_DISPONIVEIS.valor_trimestral; break;
                                case "SEMESTRAL": valor = item.B_PLANOS_DISPONIVEIS.valor_semestral; break;
                                case "ANUAL": valor = item.B_PLANOS_DISPONIVEIS.valor_anual; break;
                            }

                            db.EMPRESA_HISTORICO_PAGAMENTO.Add(new EMPRESA_HISTORICO_PAGAMENTO()
                            {
                                autenticacao_externa = resposta.tid,
                                autenticacao_interna = resposta.codigoInternoPedido,
                                cartao = dadosPedido.NumeroCartao.Substring(dadosPedido.NumeroCartao.Length - 4, 4),
                                comprado_em = DateTime.Today,
                                produto = string.Format("{0} - {1}", item.B_PLANOS_DISPONIVEIS.nome, item.periodo),
                                id_empresa = this.id_empresa,
                                id_cupom_promocional = id_cupom,
                                valor = valor,
                                forma_pagamento = "Cartão de crédito"
                            });
                        }
                        db.SaveChanges();
                    }
                    return new RespostaTransacaoCarrinho()
                    {
                        isOk = true,
                        mensagem = "Compra efetuada com sucesso!",
                        redirect = resposta.redirect
                    };
                }
                else
                {
                    return new RespostaTransacaoCarrinho()
                    {
                        isOk = true,
                        mensagem = "Compra não efetuada!",
                        redirect = resposta.redirect
                    };
                }
            }
        }

        private bool VerificaInformacoesProdutos(PedidoTransacao resposta, List<EMPRESA_CARRINHO> itens)
        {
            try
            {
                using (BuffetContext db = new BuffetContext())
                {
                    foreach (var item in itens)
                    {
                        DateTime disponivel_ate = DateTime.Today;
                        switch (item.periodo)
                        {
                            case "MENSAL": disponivel_ate = disponivel_ate.AddMonths(1); break;
                            case "TRIMESTRAL": disponivel_ate = disponivel_ate.AddMonths(3); break;
                            case "SEMESTRAL": disponivel_ate = disponivel_ate.AddMonths(6); break;
                            case "ANUAL": disponivel_ate = disponivel_ate.AddYears(1); break;
                        }

                        var d = item.B_PLANOS_DISPONIVEIS.B_PLANOS_PORTAL;
                        switch (d.anuncio)
                        {
                            case "HEADE":
                                {
                                    db.PORTAL_ANUNCIANTE_HEADER.Add(new PORTAL_ANUNCIANTE_HEADER()
                                    {
                                        de = DateTime.Today,
                                        ate = disponivel_ate,
                                        id_local_evento = item.id_local_evento,
                                        destaque_pesquisa_zona = d.destaque_pesquisa_zona,
                                        destaque_mapa = d.destaque_mapa,
                                        envio_proposta = d.envio_proposta
                                    });
                                }; break;
                            case "TOPMENU":
                                {
                                    var topMenu = db.PORTAL_ANUNCIANTE_TOPMENU.Where(e => e.regiao.Equals(item.LOCAL_EVENTO.regiao)).OrderByDescending(e => e.ate).Take(3).ToList();
                                    if (topMenu.Count < 3)
                                    {
                                        db.PORTAL_ANUNCIANTE_TOPMENU.Add(new PORTAL_ANUNCIANTE_TOPMENU()
                                        {
                                            de = DateTime.Today,
                                            ate = disponivel_ate,
                                            id_local_evento = item.id_local_evento,
                                            destaque_pesquisa_zona = d.destaque_pesquisa_zona,
                                            destaque_mapa = d.destaque_mapa,
                                            envio_proposta = d.envio_proposta,
                                            regiao = item.LOCAL_EVENTO.regiao
                                        });
                                    }
                                    else
                                    {
                                        DateTime ultimaData = topMenu.FirstOrDefault().ate.Date;

                                        switch (item.periodo)
                                        {
                                            case "MENSAL": disponivel_ate = ultimaData.AddMonths(1); break;
                                            case "TRIMESTRAL": disponivel_ate = ultimaData.AddMonths(3); break;
                                            case "SEMESTRAL": disponivel_ate = ultimaData.AddMonths(6); break;
                                            case "ANUAL": disponivel_ate = ultimaData.AddYears(1); break;
                                        }

                                        db.PORTAL_ANUNCIANTE_TOPMENU.Add(new PORTAL_ANUNCIANTE_TOPMENU()
                                        {
                                            de = ultimaData,
                                            ate = disponivel_ate,
                                            id_local_evento = item.id_local_evento,
                                            destaque_pesquisa_zona = d.destaque_pesquisa_zona,
                                            destaque_mapa = d.destaque_mapa,
                                            envio_proposta = d.envio_proposta,
                                            regiao = item.LOCAL_EVENTO.regiao
                                        });
                                    }
                                }; break;
                            case "HOME":
                                {
                                    db.PORTAL_ANUNCIANTE_HOME.Add(new PORTAL_ANUNCIANTE_HOME()
                                    {
                                        de = DateTime.Today,
                                        ate = disponivel_ate,
                                        id_local_evento = item.id_local_evento,
                                        destaque_pesquisa_zona = d.destaque_pesquisa_zona,
                                        destaque_mapa = d.destaque_mapa,
                                        envio_proposta = d.envio_proposta
                                    });
                                }; break;
                            case "DESTAQUE":
                                {
                                    db.PORTAL_ANUNCIANTE_DESTAQUE.Add(new PORTAL_ANUNCIANTE_DESTAQUE()
                                    {
                                        de = DateTime.Today,
                                        ate = disponivel_ate,
                                        id_local_evento = item.id_local_evento,
                                        destaque_pesquisa_zona = d.destaque_pesquisa_zona,
                                        destaque_mapa = d.destaque_mapa,
                                        envio_proposta = d.envio_proposta
                                    });
                                }; break;
                        }
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}