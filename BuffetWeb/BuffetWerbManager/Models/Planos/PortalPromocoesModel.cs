﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Planos
{
    public class PortalPromocoesModel
    {
        public B_PORTAL_PROMOCAO PortalPromocao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.B_PORTAL_PROMOCAO.Include(e => e.PORTAL_ANUNCIANTE_PROMOCAO.Select(f => f.LOCAL_EVENTO)).FirstOrDefault(e => (DateTime.Today >= e.disponivel_de && DateTime.Today <= e.disponivel_ate));
                }
            }
        }

        public int Regiao
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    int regiao = 0;
                    var locais = db.LOCAL_EVENTO.Where(e => e.id_empresa == PerfilEmpresa.ID_EMPRESA);
                    foreach (var local in locais)
                    {
                        regiao += db.PORTAL_CLIENTE_PERFIL.Where(e => e.regiao.Equals(local.regiao)).ToList().Count;
                    }
                    return regiao;
                }
            }
        }
    }
}