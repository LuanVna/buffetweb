﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;

namespace BuffetWebManager.Models.Proposta
{
    public class EnvioDePropostas
    {
        public int id_empresa { get; set; }

        public List<CLIENTE> clientes
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return new List<CLIENTE>();
                     //&& c.email.Equals("luan.vna@gmail.com")
                    //return db.CLIENTEs.Include(o => o.ORCAMENTOes).Include(p => p.CLIENTE_PROPOSTA.Select(e => e.ORCAMENTO_PROPOSTA)).Where(c => c.id_empresa == this.id_empresa).ToList();
                }
            }
        }

        public List<ORCAMENTO_PROPOSTA> propostas
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    //return new List<ORCAMENTO_PROPOSTA>();
                    return db.ORCAMENTO_PROPOSTA.Include(d => d.LOCAL_EVENTO).Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }
    }
}