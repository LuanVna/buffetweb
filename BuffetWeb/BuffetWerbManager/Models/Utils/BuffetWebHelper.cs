﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BuffetWebData.Models;
using System.Web.Mvc;
using System.Web.Routing;
using BuffetWebManager.Models;
using BuffetWebData.Utils;
using System.Net.Mail;

namespace BuffetWeb.Helpers
{
    public enum Modulo
    {
        Financeiro,
        Estoque,
        Orcamento,
        Galeria,
        Convite,
        Portal,
        Marketing
    }

    public enum Permitir
    {
        Criar,
        Visualizar
    }

    public enum Conteudo
    {
        Ajustes,
        Clientes,
        Convites,
        Colaborador,
        Configuracoes,
        Eventos,
        Estoque,
        Fornecedor,
        Financeiro,
        Galeria,
        Ingredientes,
        Itens,
        Orcamentos,
        TipoEvento,
        Pacotes,
        Visitas,
        Pesquisa,
        Portal,
        Marketing
    }

    public enum Perfil
    {
        Nome,
        Imagem
    }


    public enum MensagemDe
    {
        Sucesso,
        Erro,
        Atencao
    }

    public enum ContactIcon
    {
        Email,
        SMS
    }

    public static class BuffetWebHelper
    {
        public static int ID_EMPRESA(this HtmlHelper helper)
        {
            return PerfilEmpresa.ID_EMPRESA;
        }
        public static string NOME_EMPRESA_FTP(this HtmlHelper helper)
        {
            return PerfilEmpresa.NOME_EMPRESA_FTP;
        }


        public static EMPRESA_CONFIGURACAO Configuracao(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.EMPRESA_CONFIGURACAO.FirstOrDefault(e => e.id_empresa == PerfilEmpresa.ID_EMPRESA);
            }
        }

        public static List<LOCAL_EVENTO> LocalEvento(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.LOCAL_EVENTO.Where(e => e.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
            }
        }

        public static MvcHtmlString LocalEventos(this HtmlHelper helper, string name, bool required = false)
        {
            using (BuffetContext db = new BuffetContext())
            {
                List<LOCAL_EVENTO> locais = db.LOCAL_EVENTO.Where(l => l.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                TagBuilder select = createSelect(name, required);
                createOption(select, "", "Selecione");
                foreach (var local in locais)
                {
                    createOption(select, local.id.ToString(), local.nome);
                }
                return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
            }
        }

        public static MvcHtmlString NumeroParcelas(this HtmlHelper helper, string name, int maxLength, bool required = false)
        {
            using (BuffetContext db = new BuffetContext())
            {
                TagBuilder select = createSelect(name, required);
                createOption(select, "", "Selecione");
                for (int i = 1; i < maxLength; i++)
                {
                    createOption(select, i.ToString(), i.ToString());
                }
                return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
            }
        }

        public static bool isMobile(this HtmlHelper helper, HttpRequestBase request)
        {
            return request.UserAgent.Contains("Mobile");
        }

        public static MvcHtmlString FontesCSS(this HtmlHelper helper)
        {
            return new MvcHtmlString(new UtilsManager().CSSFontes());
        }

        public static string StatusContactIcon(this HtmlHelper helper, string autenticacao, TipoContato tipoContato, ContactIcon icon)
        {
            using (BuffetContext db = new BuffetContext())
            {
                CAMPANHA_CONTATOS_ENVIADOS contato = db.CAMPANHA_CONTATOS_ENVIADOS.FirstOrDefault(e => e.autenticacao.Equals(autenticacao) && e.tipo_contato.Equals(tipoContato.ToString()) && e.id_empresa == PerfilEmpresa.ID_EMPRESA);

                if (icon == ContactIcon.Email)
                {
                    if (contato != null && contato.enviado_email == true)
                    {
                        if (contato.lido_email)
                        {
                            return "/Content/Buffet/Orcamento/Imagens/icone_email_read.png";
                        }
                        return "/Content/Buffet/Orcamento/Imagens/icone_email_send.png";
                    }
                    return "/Content/Buffet/Orcamento/Imagens/icone_email.png";
                }
                else
                {
                    if (contato != null && contato.enviado_sms == true)
                    {
                        if (contato.lido_sms)
                        {
                            return "/Content/Buffet/Orcamento/Imagens/icone_sms_read.png";
                        }
                        return "/Content/Buffet/Orcamento/Imagens/icone_sms_send.png";
                    }
                    return "/Content/Buffet/Orcamento/Imagens/icone_sms.png";
                }
            }
        }

        public static MvcHtmlString BackButton(this HtmlHelper helper, bool backButton)
        {
            if (backButton)
            {
                string url_image = @"\Content\Buffet\Imagens\Base\Icon_Back.png";
                return new MvcHtmlString("<a href=\"javascript:window.history.back();\"><img style=\" width: 30px; \" src=\"" + url_image + "\" /></a>");
            }
            return new MvcHtmlString("");
        }

        public static MvcHtmlString AdicionarFiltro(this HtmlHelper helper, string controller, string action, string filtro = null, string name = "filtro", string formId = "form_filter")
        {
            string select = "";
            if (filtro == null || filtro == "30")
            {
                select = "<option value=\"30\">Últimos 30 dias</option>" +
                        "<option value=\"60\">Filtrar por 60 dias</option>" +
                        "<option value=\"120\">Filtrar por 120 dias</option>" +
                        "<option value=\"0\">Desde Inicio</option>";
            }
            else if (filtro == "60")
            {
                select = "<option value=\"60\">Filtrar por 60 dias</option>" +
                         "<option value=\"30\">Últimos 30 dias</option>" +
                         "<option value=\"120\">Filtrar por 120 dias</option>" +
                         "<option value=\"0\">Desde Inicio</option>";

            }
            else if (filtro == "120")
            {
                select = "<option value=\"120\">Filtrar por 120 dias</option>" +
                         "<option value=\"60\">Filtrar por 60 dias</option>" +
                         "<option value=\"30\">Últimos 30 dias</option>" +
                         "<option value=\"0\">Desde Inicio</option>";

            }
            else if (filtro == "0")
            {
                select = "<option value=\"0\">Desde Inicio</option>" +
                         "<option value=\"120\">Filtrar por 120 dias</option>" +
                         "<option value=\"60\">Filtrar por 60 dias</option>" +
                         "<option value=\"30\">Últimos 30 dias</option>";

            }

            string component = "<form action=\"" + string.Format("/{0}/{1}", controller, action) + "\" method=\"post\" id=\"" + formId + "\">" +
                                    "<select class=\"form-control col-md-2 col-sm-2 col-lg-2 pull-left\" name=\"" + name + "\" onchange=\"FiltrarInformacoes(this, '" + formId + "')\" style=\" margin-bottom: -40px; z-index: 1000;\">" +
                                        select +
                                    "</select>" +
                                "</form>";
            return new MvcHtmlString(component);
        }


        public static MvcHtmlString BandeirasDisponiveis(this HtmlHelper helper, string name, bool required = false)
        {
            using (BuffetContext db = new BuffetContext())
            {
                TagBuilder select = createSelect(name, required);
                List<FINANCEIRO_BANDEIRAS> bandeiras = db.FINANCEIRO_BANDEIRAS.Where(l => l.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                foreach (var bandeira in bandeiras)
                {
                    createOption(select, bandeira.id.ToString(), bandeira.bandeira);
                }
                return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
            }
        }

        public static bool ValidateEmail(this HtmlHelper helper, string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static MvcHtmlString isEmail(this HtmlHelper helper, string email, string nome)
        {
            return new MvcHtmlString("<a href=\"#\" data-target=\"#EmailPlataform\" onclick=\"o('plataform_nome').value = '" + nome + "'; o('plataform_email').value = '" + email + "'; UIHide('plataform_alert')\" data-toggle=\"modal\" >" + email + "</a>");
        }

        public static MvcHtmlString isCelular(this HtmlHelper helper, string telefone, string nome)
        {
            return new MvcHtmlString("<a href=\"#\" data-target=\"#SMSPlataform\" onclick=\"o('plataform_nome_sms').value = '" + nome + "'; o('plataform_telefone_sms').value = '" + telefone + "'; UIHide('plataform_alert_sms')\" data-toggle=\"modal\" >" + telefone + "</a>");
        }

        public static MvcHtmlString OndeConheceu(this HtmlHelper helper, string name, bool required = false)
        {
            TagBuilder select = createSelect(name, required);
            createOption(select, "", "Selecione");
            using (BuffetContext db = new BuffetContext())
            {
                List<ONDE_CONHECEU> onde = db.ONDE_CONHECEU.Where(o => o.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                foreach (var ond in onde)
                {
                    createOption(select, ond.nome, ond.nome);
                }
                return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
            }
        }

        public static MvcHtmlString BancosDisponiveis(this HtmlHelper helper, string name, bool required = false)
        {
            TagBuilder select = createSelect(name, required);
            createOption(select, "", "Selecione");
            createOption(select, "itau", "Itau");
            createOption(select, "Bradesco", "Bradesco");
            createOption(select, "Santander", "Santander");
            createOption(select, "HSBC", "HSBC");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Estados(this HtmlHelper helper, string name, bool required = false)
        {

            TagBuilder select = createSelect(name, required);
            createOption(select, "", "Selecione");
            createOption(select, "SP", "SP");
            createOption(select, "RJ", "RJ");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Cidade(this HtmlHelper helper, string name, bool required = false)
        {
            TagBuilder select = createSelect(name, required);
            createOption(select, "", "Selecione");
            createOption(select, "São Paulo", "São Paulo");
            createOption(select, "Rio de Janeiro", "Rio de Janeiro");
            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Dica(this HtmlHelper helper, string orientacao, string mensagem)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            string dica = "";
            if (colaborador.mostrar_dica == true)
            {
                dica = string.Format("data-toggle='tooltip' data-placement='{0}' data-original-title='{1}'", orientacao, mensagem);
            }
            return new MvcHtmlString(dica);
        }

        public static MvcHtmlString BuffetCheck(this HtmlHelper helper, string titulo, string nameAndID = "", bool isCkeck = true, string callBack = "")
        {
            return new MvcHtmlString("<div style='padding:5px'>" +
                                      "<input type='hidden' name='" + nameAndID + "' id='" + nameAndID + "' value='" + (isCkeck ? "true" : "false") + "' />" +
                                      "<a id='check_" + nameAndID + "' style='margin-right: 5px;' href='javascript:void(o)' onclick='CheckOrUncheck(this, \"" + nameAndID + "\");" + callBack + "' data-check='" + (isCkeck ? "true" : "false") + "'>" +
                                            "<img src='/Content/Buffet/Imagens/Base/icone_" + (isCkeck ? "check" : "uncheck") + ".png' width='30' />" +
                                       "</a>" +
                                      "<label style='cursor:pointer' onclick='CheckOrUncheck(o('check_" + nameAndID + "'), \"" + nameAndID + "\"); " + callBack + "'>" + titulo + " </label>" +
                                     "</div>");
        }




        public static MvcHtmlString URLImagem(this HtmlHelper helper, string url)
        {
            return new MvcHtmlString(url != null || url != "" ? url : "");
        }

        public static MvcHtmlString AddMensagem(this HtmlHelper helper, string mensagem, MensagemDe mensagemDe = MensagemDe.Sucesso)
        {
            if (mensagem != null)
            {
                string[] divider = mensagem.Split('|');

                string alert = @"<div class='alert " + divider[1] + " role='alert'>" +
                                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>" +
                                "<strong>" + alerta(divider[1]) + "!</strong> " + divider[0] + " </div>";
                return new MvcHtmlString(alert);
            }
            return new MvcHtmlString("");
        }

        public static MvcHtmlString AnaliseDeEmpresa(this HtmlHelper helper)
        {
            NotificacoesModel s = new NotificacoesSistema(PerfilEmpresa.ID_EMPRESA).VerificarDisponiveis();

            if (s != null && s.isOk)
            {
                bool notificar = BWColaborador.getColaborador().administrador == true;
                if (!notificar)
                {
                    if (s.modulo.Contains("Orcamento"))
                        notificar = Permissoes(helper, Permitir.Criar, Conteudo.Orcamentos);
                    else if (s.modulo.Contains("Convite"))
                        notificar = Permissoes(helper, Permitir.Criar, Conteudo.Convites);
                    if (s.onlyAdm && BWColaborador.getColaborador().administrador == false)
                        return new MvcHtmlString("");

                    notificar = true;
                }

                //CASO A URL SEJA A MESMA QUE O USUARIO ESTA ATUALMENTE
                string currentAction = helper.ViewContext.RouteData.Values["action"].ToString();
                string currentController = helper.ViewContext.RouteData.Values["controller"].ToString();
                if (string.Format("/{0}/{1}", currentAction, currentController).Contains(s.url))
                {
                    return new MvcHtmlString("");
                }

                if (notificar)
                {
                    string notificacao = "<div class='grey-container shortcut-wrapper' style='background-color:" + s.cor + "; min-height:70px; margin-top:-1px'>" +
                                        "<div class='row'>" +
                                            "<div class='col-md-1 pull-left'>" +
                                                "<img src='http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/" + s.icone + ".png' width='50' height='50'>" +
                                            "</div>" +
                                            "<div class='col-md-10'>" +
                                                "<h4 style='color:white;text-align: left;'>" +
                                                s.titulo +
                                                "</h4>" +
                                                "<h5 style='color:white;text-align: left;'>" +
                                                s.mensagem +
                                                "</h5>" +
                                            "</div>" +
                                            "<div class='col-md-1' style=' margin-top: 9px; '>" +
                                                "<a style=' background-color: " + s.cor + "; border-color: white; ' href='" + s.url + "' class='btn btn-danger pull-right' target='_self'>Veja mais</a>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>";
                    return new MvcHtmlString(notificacao);
                }
            }
            return new MvcHtmlString("");
        }

        private static string alerta(string mensagemde)
        {
            switch (mensagemde)
            {
                case "alert-sucess": return "Sucesso";
                case "alert-danger": return "Erro";
                case "alert-warning": return "Atenção";
                case "alert-info": return "Informação";
            }
            return "";
        }

        private static string textoAlerta(MensagemDe mensagemde)
        {
            switch (mensagemde)
            {
                case MensagemDe.Sucesso: return "Sucesso";
                case MensagemDe.Erro: return "Erro";
                case MensagemDe.Atencao: return "Atenção";
            }
            return "";
        }

        #region PERFIL
        public static string PerfilLogado(this HtmlHelper helper, Perfil modulo)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null)
            {
                switch (modulo)
                {
                    case Perfil.Nome: return colaborador.nome.Split(' ')[0] ?? colaborador.nome;
                    case Perfil.Imagem: return colaborador.url ?? @"http://www.buffetweb.com/Sites/Arquivos/Sistema/Imagens/Selecionar_Imagem.png";
                }
            }
            return "";
        }

        public static List<NOTIFICACO> notificacoes(this HtmlHelper helper)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.NOTIFICACOES.Include(e => e.COLABORADORE).Where(e => e.para_id_colaborador == colaborador.id && e.id_empresa == PerfilEmpresa.ID_EMPRESA).OrderByDescending(e => e.data).Take(5).ToList();
                }
            }
            return new List<NOTIFICACO>();
        }

        public static List<EMPRESA_CARRINHO> ItensCarrinho(this HtmlHelper helper)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null && colaborador.administrador == true)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESA_CARRINHO.Include(e => e.B_PLANOS_DISPONIVEIS).Where(e => e.id_empresa == colaborador.id_empresa && e.comprado == false).ToList();
                }
            }
            return new List<EMPRESA_CARRINHO>();
        }

        public static List<COLABORADORE> colaboradores(this HtmlHelper helper)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.COLABORADORES.Include(p => p.PERFIL_COLABORADOR).Where(e => e.id != colaborador.id && e.id_empresa == PerfilEmpresa.ID_EMPRESA).ToList();
                }
            }
            return new List<COLABORADORE>();
        }

        public static string ArquivoLOGO(this HtmlHelper help)
        {
            return getConfig("LOGO");
        }

        public static string CaminhoFontes(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals("FONTES")).valor;
            }
        }

        public static bool Modulos(this HtmlHelper helper, Modulo modulo)
        {
            switch (modulo)
            {
                case Modulo.Financeiro: return PerfilEmpresa.MODULO_FINANCEIRO;
                case Modulo.Estoque: return PerfilEmpresa.MODULO_ESTOQUE;
                case Modulo.Orcamento: return PerfilEmpresa.MODULO_ORCAMENTO;
                case Modulo.Convite: return PerfilEmpresa.MODULO_CONVITES;
                case Modulo.Portal: return PerfilEmpresa.MODULO_PORTAL;
            }
            return false;
        }

        public static bool Administrador(this HtmlHelper helper)
        {
            using (BuffetContext db = new BuffetContext())
            {
                if (BWColaborador.getColaborador().administrador == true)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool Permissoes(this HtmlHelper helper, Permitir permitir, Conteudo conteudo)
        {
            PERFIL_COLABORADOR perfil = BWColaborador.getColaborador().PERFIL_COLABORADOR;
            if (perfil != null)
            {
                switch (permitir)
                {
                    case Permitir.Criar:
                        {
                            switch (conteudo)
                            {
                                case Conteudo.Itens: return perfil.c_itens;
                                case Conteudo.Ajustes: return true;
                                case Conteudo.Clientes: return perfil.c_clientes;
                                case Conteudo.Convites: return perfil.c_convite;
                                case Conteudo.Colaborador: return perfil.c_colaborador;
                                case Conteudo.Fornecedor: return perfil.c_fornecedores;
                                case Conteudo.Ingredientes: return perfil.c_itens;
                                case Conteudo.Eventos: return perfil.c_eventos;
                                case Conteudo.TipoEvento: return perfil.c_tipo_eventos;
                                case Conteudo.Financeiro: return perfil.c_financeiro;
                                case Conteudo.Galeria: return perfil.c_galeria;
                                case Conteudo.Orcamentos: return perfil.c_orcamentos;
                                case Conteudo.Pacotes: return perfil.c_pacotes;
                                case Conteudo.Visitas: return perfil.c_visitas;
                                case Conteudo.Configuracoes: return perfil.c_configuracao;
                                case Conteudo.Estoque: return perfil.c_estoque;
                                case Conteudo.Portal: return true; //perfil.v_pesquisa;
                            }
                        }; break;
                    case Permitir.Visualizar:
                        {
                            switch (conteudo)
                            {
                                case Conteudo.Itens: return perfil.v_itens;
                                case Conteudo.Ajustes: return true;
                                case Conteudo.Clientes: return perfil.v_clientes;
                                case Conteudo.Convites: return perfil.v_convite;
                                case Conteudo.Colaborador: return perfil.v_colaborador;
                                case Conteudo.Fornecedor: return perfil.v_fornecedores;
                                case Conteudo.Ingredientes: return perfil.v_itens;
                                case Conteudo.Eventos: return perfil.v_eventos;
                                case Conteudo.TipoEvento: return perfil.v_tipo_eventos;
                                case Conteudo.Financeiro: return perfil.v_financeiro;
                                case Conteudo.Galeria: return perfil.v_galeria;
                                case Conteudo.Orcamentos: return perfil.v_orcamentos;
                                case Conteudo.Pacotes: return perfil.v_pacotes;
                                case Conteudo.Visitas: return perfil.v_visitas;
                                case Conteudo.Configuracoes: return perfil.v_configuracao;
                                case Conteudo.Estoque: return perfil.v_estoque;
                                case Conteudo.Pesquisa: return true; //perfil.v_pesquisa;
                                case Conteudo.Portal: return true; //perfil.v_pesquisa;
                            }
                        } break;
                }
            }
            return false;
        }





        #endregion


        //COMPONENTES

        private static TagBuilder createSelect(string name, bool required)
        {
            TagBuilder select = new TagBuilder("select");
            select.MergeAttribute("name", name);
            select.MergeAttribute("id", name);
            select.MergeAttribute("class", "form-control");
            if (required)
            {
                select.MergeAttribute("required", "required");
            }
            return select;
        }

        private static void createOption(TagBuilder select, string value, string innerHTML)
        {
            TagBuilder option = new TagBuilder("option");
            option.MergeAttribute("value", value);
            option.InnerHtml = innerHTML;
            select.InnerHtml += option.ToString();
        }

        private static string getConfig(string key)
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null)
            {
                try
                {
                    using (BuffetContext db = new BuffetContext())
                    {
                        EMPRESA empresa = db.EMPRESAs.FirstOrDefault(e => e.id == colaborador.id_empresa);
                        if (empresa != null)
                        {
                            return db.EMPRESA_ARQUIVOS.FirstOrDefault(i => i.id_empresa == empresa.id && i.chave.Equals(key)).valor;
                        }
                    }
                }
                catch (Exception)
                {
                    return "";
                }
            }
            return "";
        }

        public static MvcHtmlString SelecionarImagem(this HtmlHelper helper, string nomeImagem, string nomeFile, string CallBack = "", int heigth = 100, int width = 100)
        {
            string imageFile = @"<input type='file' name='" + nomeFile + "' style='display:none' id='" + nomeFile + "'  onchange=\"arquivoSelecionado(this, '" + nomeImagem + "', " + CallBack + ")\" />" +
                                "<a href='#' onclick=\"o('" + nomeFile + "').click()\">" +
                               "<img style=\"width: " + width + "px; height: " + heigth + "px;\" id=\"" + nomeImagem + "\" class=\"img-thumbnail\" alt=" + string.Format("{0}x{1}", heigth, width) + " src=\"http://placehold.it/" + width + "x" + heigth + "/EFEFEF/EBBB5E\"></a>";
            return new MvcHtmlString(imageFile);
        }
    }
}