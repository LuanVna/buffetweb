﻿using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BuffetWebManager.Models
{
    public class PerfilEmpresa
    {
        public static int ID_EMPRESA { get { return idEmpresa(); } }
        private static EMPRESA empresa
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.EMPRESAs.Include(c => c.EMPRESA_CONFIGURACAO).FirstOrDefault(e => e.id == ID_EMPRESA);
                }
            }
        }

        public static string NOME_EMPRESA { get { return empresa.nome; } }
        public static string NOME_EMPRESA_FTP { get { return string.Format("{0}_{1}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa); } }

        //MODULO 
        public static bool MODULO_CONVITES
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.modulo.Equals("CONVITE"));
                    return (DateTime.Now.Date <= orcamento.expira_em.Date);
                }
            }
        }

        public static bool MODULO_ORCAMENTO
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.modulo.Equals("ORCAMENTO"));
                    return (DateTime.Now.Date <= orcamento.expira_em.Date);
                }
            }
        }

        public static bool MODULO_FINANCEIRO
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS financeiro = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.modulo.Equals("FINANCEIRO"));
                    return (DateTime.Now.Date <= financeiro.expira_em.Date);
                }
            }
        }

        public static bool MODULO_ESTOQUE
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.modulo.Equals("ESTOQUE"));
                    return (DateTime.Now.Date <= orcamento.expira_em.Date);
                }
            }
        }

        public static bool MODULO_PORTAL
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    EMPRESA_MODULOS orcamento = db.EMPRESA_MODULOS.FirstOrDefault(e => e.id_empresa == ID_EMPRESA && e.modulo.Equals("PORTAL"));
                    return (DateTime.Now.Date <= orcamento.expira_em.Date);
                }
            }
        }

        public static int MAX_COLABORADOR { get { return 1; } }

        private static int idEmpresa()
        {
            COLABORADORE colaborador = BWColaborador.getColaborador();
            if (colaborador != null) return colaborador.id_empresa;
            else throw new Exception("ERROREMPRESA");
        }
    }
}