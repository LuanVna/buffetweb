﻿ 
using BuffetWebData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BuffetWebManager.Models.Visitas
{
    public class Visitas
    {
        public int id_empresa { get; set; }
        public List<ORCAMENTO_VISITAS> visitas { get; set; }

        public List<ORCAMENTO> orcamentos { get; set; }

        //public List<HORARIO_DISPONIVEIS> horarios_disponiveis
        //{
        //    get
        //    {
        //        using (BuffetContext db = new BuffetContext())
        //        {
        //            return db.HORARIO_DISPONIVEIS.Where(e => e.disponivel == true && e.id_empresa == this.id_empresa).ToList();
        //        }
        //    }
        //}

        public List<LOCAL_EVENTO> local_evento
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return db.LOCAL_EVENTO.Where(e => e.id_empresa == this.id_empresa).ToList();
                }
            }
        }
    }

    public class Visita
    {
        public int id { get; set; }
        public string title { get; set; }
        public string data { get; set; }
    }
}