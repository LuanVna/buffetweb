using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ADMINISTRADOR
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string senha { get; set; }
        public int id_empresa { get; set; }
    }
}
