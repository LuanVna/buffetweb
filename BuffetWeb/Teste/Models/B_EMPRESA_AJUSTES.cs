using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_EMPRESA_AJUSTES
    {
        public int id { get; set; }
        public string chave { get; set; }
        public string valor { get; set; }
    }
}
