using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_EMPRESA_USUARIOS
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string senha { get; set; }
        public string autenticacao { get; set; }
        public string celular { get; set; }
    }
}
