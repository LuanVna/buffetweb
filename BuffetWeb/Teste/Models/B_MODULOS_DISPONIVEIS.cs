using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_MODULOS_DISPONIVEIS
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int meses_plano { get; set; }
        public decimal valor { get; set; }
    }
}
