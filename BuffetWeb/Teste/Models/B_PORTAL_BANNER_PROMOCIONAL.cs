using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_PORTAL_BANNER_PROMOCIONAL
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string tipo_banner { get; set; }
        public string url { get; set; }
        public string url_imagem { get; set; }
        public int duracao { get; set; }
        public bool indeterminado { get; set; }
        public Nullable<System.DateTime> disponivel_de { get; set; }
        public Nullable<System.DateTime> disponivel_ate { get; set; }
    }
}
