using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_PORTAL_DENUNCIAS
    {
        public int id { get; set; }
        public string comentario { get; set; }
        public System.DateTime desde { get; set; }
        public int id_local_evento { get; set; }
        public bool resolvido { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
