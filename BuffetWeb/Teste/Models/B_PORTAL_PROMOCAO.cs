using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_PORTAL_PROMOCAO
    {
        public B_PORTAL_PROMOCAO()
        {
            this.PORTAL_ANUNCIANTE_PROMOCAO = new List<PORTAL_ANUNCIANTE_PROMOCAO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public System.DateTime disponivel_de { get; set; }
        public System.DateTime disponivel_ate { get; set; }
        public decimal desconto_de { get; set; }
        public string url_imagem { get; set; }
        public virtual ICollection<PORTAL_ANUNCIANTE_PROMOCAO> PORTAL_ANUNCIANTE_PROMOCAO { get; set; }
    }
}
