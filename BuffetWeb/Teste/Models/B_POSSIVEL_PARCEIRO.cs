using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_POSSIVEL_PARCEIRO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
        public string operadora { get; set; }
        public string senha { get; set; }
        public System.DateTime desde { get; set; }
        public string status { get; set; }
    }
}
