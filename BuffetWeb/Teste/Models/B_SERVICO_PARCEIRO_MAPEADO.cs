using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_SERVICO_PARCEIRO_MAPEADO
    {
        public B_SERVICO_PARCEIRO_MAPEADO()
        {
            this.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS = new List<B_SERVICO_PARCEIRO_MAPEADO_IMAGENS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public string telefone { get; set; }
        public string rua { get; set; }
        public string localidade { get; set; }
        public string status { get; set; }
        public string plano_atual { get; set; }
        public Nullable<System.DateTime> mapeado_em { get; set; }
        public Nullable<System.DateTime> atualizado_em { get; set; }
        public string de_concorrente { get; set; }
        public string site_portal { get; set; }
        public string site { get; set; }
        public string video { get; set; }
        public string email { get; set; }
        public string logo { get; set; }
        public string autenticacao { get; set; }
        public virtual ICollection<B_SERVICO_PARCEIRO_MAPEADO_IMAGENS> B_SERVICO_PARCEIRO_MAPEADO_IMAGENS { get; set; }
    }
}
