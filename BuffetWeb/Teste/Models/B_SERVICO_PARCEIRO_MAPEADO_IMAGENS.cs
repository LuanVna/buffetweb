using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class B_SERVICO_PARCEIRO_MAPEADO_IMAGENS
    {
        public int id { get; set; }
        public string url { get; set; }
        public int id_parceiro_mapeado { get; set; }
        public virtual B_SERVICO_PARCEIRO_MAPEADO B_SERVICO_PARCEIRO_MAPEADO { get; set; }
    }
}
