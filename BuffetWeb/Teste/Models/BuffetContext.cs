using System.Data.Entity;
using BuffetDataManager.Models.Mapping;
using MySql.Data.Entity;

namespace BuffetDataManager.Models
{
    public partial class BuffetContext : DbContext
    {
        static BuffetContext()
        {
            Database.SetInitializer<BuffetContext>(null);
        }

        public BuffetContext()
            : base("Name=BuffetWebContext")
        {
        }

        public DbSet<ADMINISTRADOR> ADMINISTRADORs { get; set; }
        public DbSet<B_CONVITES_DISPONIVEIS> B_CONVITES_DISPONIVEIS { get; set; }
        public DbSet<B_CONVITES_FONTES> B_CONVITES_FONTES { get; set; }
        public DbSet<B_CONVITES_FRASES> B_CONVITES_FRASES { get; set; }
        public DbSet<B_EMPRESA_AJUSTES> B_EMPRESA_AJUSTES { get; set; }
        public DbSet<B_EMPRESA_USUARIOS> B_EMPRESA_USUARIOS { get; set; }
        public DbSet<B_MODULOS_DISPONIVEIS> B_MODULOS_DISPONIVEIS { get; set; }
        public DbSet<B_PLANOS_DISPONIVEIS> B_PLANOS_DISPONIVEIS { get; set; }
        public DbSet<B_PLANOS_PORTAL> B_PLANOS_PORTAL { get; set; }
        public DbSet<B_PORTAL_BANNER_PROMOCIONAL> B_PORTAL_BANNER_PROMOCIONAL { get; set; }
        public DbSet<B_PORTAL_DENUNCIAS> B_PORTAL_DENUNCIAS { get; set; }
        public DbSet<B_PORTAL_PROMOCAO> B_PORTAL_PROMOCAO { get; set; }
        public DbSet<B_POSSIVEL_PARCEIRO> B_POSSIVEL_PARCEIRO { get; set; }
        public DbSet<B_SERVICO_MAPEAR_REGIAO> B_SERVICO_MAPEAR_REGIAO { get; set; }
        public DbSet<B_SERVICO_PARCEIRO_MAPEADO> B_SERVICO_PARCEIRO_MAPEADO { get; set; }
        public DbSet<B_SERVICO_PARCEIRO_MAPEADO_IMAGENS> B_SERVICO_PARCEIRO_MAPEADO_IMAGENS { get; set; }
        public DbSet<C_CONVITE_OPCOES> C_CONVITE_OPCOES { get; set; }
        public DbSet<C_FORMA_PAGAMENTO> C_FORMA_PAGAMENTO { get; set; }
        public DbSet<CAMPANHA> CAMPANHAs { get; set; }
        public DbSet<CAMPANHA_CONTATOS_ENVIADOS> CAMPANHA_CONTATOS_ENVIADOS { get; set; }
        public DbSet<CAMPANHA_LINHA_TEMPO> CAMPANHA_LINHA_TEMPO { get; set; }
        public DbSet<CAMPANHA_LISTA> CAMPANHA_LISTA { get; set; }
        public DbSet<CAMPOS_COMPONENTES> CAMPOS_COMPONENTES { get; set; }
        public DbSet<CAMPOS_DESCRICAO> CAMPOS_DESCRICAO { get; set; }
        public DbSet<CAMPOS_DISPONIVEIS> CAMPOS_DISPONIVEIS { get; set; }
        public DbSet<CATEGORIA_ALUGUEL> CATEGORIA_ALUGUEL { get; set; }
        public DbSet<CATEGORIA_DIVERSOS> CATEGORIA_DIVERSOS { get; set; }
        public DbSet<CATEGORIA_ITENS> CATEGORIA_ITENS { get; set; }
        public DbSet<CATEGORIA_SERVICOS> CATEGORIA_SERVICOS { get; set; }
        public DbSet<CLIENTE> CLIENTEs { get; set; }
        public DbSet<CLIENTE_ANIVERSARIANTES> CLIENTE_ANIVERSARIANTES { get; set; }
        public DbSet<CLIENTE_PROPOSTA> CLIENTE_PROPOSTA { get; set; }
        public DbSet<CLIENTE_TIMELINE> CLIENTE_TIMELINE { get; set; }
        public DbSet<COLABORADOR_DEVICES> COLABORADOR_DEVICES { get; set; }
        public DbSet<COLABORADOR_NOTIFICACAO> COLABORADOR_NOTIFICACAO { get; set; }
        public DbSet<COLABORADORE> COLABORADORES { get; set; }
        public DbSet<CONTATO_PLATAFORMA> CONTATO_PLATAFORMA { get; set; }
        public DbSet<CONVITE_ANIVERSARIANTES> CONVITE_ANIVERSARIANTES { get; set; }
        public DbSet<CONVITE_CONVIDADOS> CONVITE_CONVIDADOS { get; set; }
        public DbSet<CONVITE_CONVIDADOS_FACEBOOK> CONVITE_CONVIDADOS_FACEBOOK { get; set; }
        public DbSet<CONVITE_CONVIDADOS_FAMILIARES> CONVITE_CONVIDADOS_FAMILIARES { get; set; }
        public DbSet<CONVITE_PERGUNTA_OPCOES> CONVITE_PERGUNTA_OPCOES { get; set; }
        public DbSet<CONVITE_RESPOSTAS_OPCOES> CONVITE_RESPOSTAS_OPCOES { get; set; }
        public DbSet<CONVITE_TEMPLATE> CONVITE_TEMPLATE { get; set; }
        public DbSet<EMPRESA> EMPRESAs { get; set; }
        public DbSet<EMPRESA_ARQUIVOS> EMPRESA_ARQUIVOS { get; set; }
        public DbSet<EMPRESA_CARRINHO> EMPRESA_CARRINHO { get; set; }
        public DbSet<EMPRESA_CARRINHO_CUPOM> EMPRESA_CARRINHO_CUPOM { get; set; }
        public DbSet<EMPRESA_CNAES> EMPRESA_CNAES { get; set; }
        public DbSet<EMPRESA_CONFIGURACAO> EMPRESA_CONFIGURACAO { get; set; }
        public DbSet<EMPRESA_HISTORICO_PAGAMENTO> EMPRESA_HISTORICO_PAGAMENTO { get; set; }
        public DbSet<EMPRESA_MODULOS> EMPRESA_MODULOS { get; set; }
        public DbSet<EMPRESA_PACOTE_SMS> EMPRESA_PACOTE_SMS { get; set; }
        public DbSet<EMPRESA_PAGAMENTO_CARTAO> EMPRESA_PAGAMENTO_CARTAO { get; set; }
        public DbSet<ENDERECO> ENDERECOes { get; set; }
        public DbSet<EVENTO> EVENTOes { get; set; }
        public DbSet<EVENTO_ALUGUEL> EVENTO_ALUGUEL { get; set; }
        public DbSet<EVENTO_DIVERSOS> EVENTO_DIVERSOS { get; set; }
        public DbSet<EVENTO_PRESTADORES> EVENTO_PRESTADORES { get; set; }
        public DbSet<EVENTO_SERVICOS> EVENTO_SERVICOS { get; set; }
        public DbSet<EVENTOS_PACOTES> EVENTOS_PACOTES { get; set; }
        public DbSet<FINANCEIRO_BANDEIRAS> FINANCEIRO_BANDEIRAS { get; set; }
        public DbSet<FINANCEIRO_P_BOLETO> FINANCEIRO_P_BOLETO { get; set; }
        public DbSet<FINANCEIRO_P_CHEQUE> FINANCEIRO_P_CHEQUE { get; set; }
        public DbSet<FINANCEIRO_P_CREDITO> FINANCEIRO_P_CREDITO { get; set; }
        public DbSet<FINANCEIRO_P_DEBITO> FINANCEIRO_P_DEBITO { get; set; }
        public DbSet<FINANCEIRO_P_DEPOSITO> FINANCEIRO_P_DEPOSITO { get; set; }
        public DbSet<FINANCEIRO_P_ESPECIE> FINANCEIRO_P_ESPECIE { get; set; }
        public DbSet<FORNECEDOR> FORNECEDORs { get; set; }
        public DbSet<FORNECEDOR_ALUGUEL> FORNECEDOR_ALUGUEL { get; set; }
        public DbSet<FORNECEDOR_CARDAPIO> FORNECEDOR_CARDAPIO { get; set; }
        public DbSet<FORNECEDOR_INGREDIENTE> FORNECEDOR_INGREDIENTE { get; set; }
        public DbSet<HORARIO> HORARIOS { get; set; }
        public DbSet<IMAGENS_DOS_EVENTOS> IMAGENS_DOS_EVENTOS { get; set; }
        public DbSet<IMAGENS_ESPACO_LOCAL_EVENTOS> IMAGENS_ESPACO_LOCAL_EVENTOS { get; set; }
        public DbSet<IMAGENS_GALERIA_EVENTOS> IMAGENS_GALERIA_EVENTOS { get; set; }
        public DbSet<IMAGENS_ITEM> IMAGENS_ITEM { get; set; }
        public DbSet<IMAGENS_PACOTES> IMAGENS_PACOTES { get; set; }
        public DbSet<INGREDIENTE> INGREDIENTES { get; set; }
        public DbSet<ITENS_ALUGUEL> ITENS_ALUGUEL { get; set; }
        public DbSet<ITENS_CARDAPIO> ITENS_CARDAPIO { get; set; }
        public DbSet<ITENS_DIVERSOS> ITENS_DIVERSOS { get; set; }
        public DbSet<ITENS_INGREDIENTES> ITENS_INGREDIENTES { get; set; }
        public DbSet<ITENS_SERVICOS> ITENS_SERVICOS { get; set; }
        public DbSet<LOCAL_EVENTO> LOCAL_EVENTO { get; set; }
        public DbSet<LOCAL_EVENTO_COMENTARIOS> LOCAL_EVENTO_COMENTARIOS { get; set; }
        public DbSet<LOCAL_EVENTO_IMAGENS> LOCAL_EVENTO_IMAGENS { get; set; }
        public DbSet<LOCAL_EVENTO_SOCIAL> LOCAL_EVENTO_SOCIAL { get; set; }
        public DbSet<NOTIFICACO> NOTIFICACOES { get; set; }
        public DbSet<ONDE_CONHECEU> ONDE_CONHECEU { get; set; }
        public DbSet<ORCAMENTO> ORCAMENTOes { get; set; }
        public DbSet<ORCAMENTO_ANOTACOES> ORCAMENTO_ANOTACOES { get; set; }
        public DbSet<ORCAMENTO_ITENS> ORCAMENTO_ITENS { get; set; }
        public DbSet<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
        public DbSet<ORCAMENTO_PACOTES> ORCAMENTO_PACOTES { get; set; }
        public DbSet<ORCAMENTO_PROPOSTA> ORCAMENTO_PROPOSTA { get; set; }
        public DbSet<ORCAMENTO_PROPOSTA_ABAS> ORCAMENTO_PROPOSTA_ABAS { get; set; }
        public DbSet<ORCAMENTO_PROPOSTA_DESCRICAO> ORCAMENTO_PROPOSTA_DESCRICAO { get; set; }
        public DbSet<ORCAMENTO_PROPOSTA_PACOTE> ORCAMENTO_PROPOSTA_PACOTE { get; set; }
        public DbSet<ORCAMENTO_PROPOSTA_PACOTE_VALORES> ORCAMENTO_PROPOSTA_PACOTE_VALORES { get; set; }
        public DbSet<ORCAMENTO_VISITAS> ORCAMENTO_VISITAS { get; set; }
        public DbSet<PACOTE_CATEGORIA_ITENS> PACOTE_CATEGORIA_ITENS { get; set; }
        public DbSet<PACOTE> PACOTES { get; set; }
        public DbSet<PACOTES_ITENS> PACOTES_ITENS { get; set; }
        public DbSet<PERFIL_COLABORADOR> PERFIL_COLABORADOR { get; set; }
        public DbSet<PESQUISA_PERGUNTA> PESQUISA_PERGUNTA { get; set; }
        public DbSet<PESQUISA_PERGUNTA_OPCOES> PESQUISA_PERGUNTA_OPCOES { get; set; }
        public DbSet<PESQUISA_PESQUISAS> PESQUISA_PESQUISAS { get; set; }
        public DbSet<PESQUISA_RESPOSTA> PESQUISA_RESPOSTA { get; set; }
        public DbSet<PESQUISA_RESPOSTA_OPCOES> PESQUISA_RESPOSTA_OPCOES { get; set; }
        public DbSet<PORTAL_ANUNCIANTE_DESTAQUE> PORTAL_ANUNCIANTE_DESTAQUE { get; set; }
        public DbSet<PORTAL_ANUNCIANTE_HEADER> PORTAL_ANUNCIANTE_HEADER { get; set; }
        public DbSet<PORTAL_ANUNCIANTE_HOME> PORTAL_ANUNCIANTE_HOME { get; set; }
        public DbSet<PORTAL_ANUNCIANTE_PROMOCAO> PORTAL_ANUNCIANTE_PROMOCAO { get; set; }
        public DbSet<PORTAL_ANUNCIANTE_TOPMENU> PORTAL_ANUNCIANTE_TOPMENU { get; set; }
        public DbSet<PORTAL_CLIENTE_FAVORITOS> PORTAL_CLIENTE_FAVORITOS { get; set; }
        public DbSet<PORTAL_CLIENTE_PERFIL> PORTAL_CLIENTE_PERFIL { get; set; }
        public DbSet<PORTAL_CLIENTE_PESQUISAS> PORTAL_CLIENTE_PESQUISAS { get; set; }
        public DbSet<PORTAL_VISUALIZACOES> PORTAL_VISUALIZACOES { get; set; }
        public DbSet<POTENCIAL_CLIENTE> POTENCIAL_CLIENTE { get; set; }
        public DbSet<PRESTADOR> PRESTADORs { get; set; }
        public DbSet<PRESTADOR_FUNCAO> PRESTADOR_FUNCAO { get; set; }
        public DbSet<PRESTADOR_SERVICO> PRESTADOR_SERVICO { get; set; }
        public DbSet<SUB_CATEGORIA_ITENS> SUB_CATEGORIA_ITENS { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<TIPO_SERVICO> TIPO_SERVICO { get; set; }
        public DbSet<UNIDADE_MEDIDA> UNIDADE_MEDIDA { get; set; }
        public DbSet<VISITAS_AGENDADAS> VISITAS_AGENDADAS { get; set; }
        public DbSet<VISITAS_DISPONIVEIS> VISITAS_DISPONIVEIS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ADMINISTRADORMap());
            modelBuilder.Configurations.Add(new B_CONVITES_DISPONIVEISMap());
            modelBuilder.Configurations.Add(new B_CONVITES_FONTESMap());
            modelBuilder.Configurations.Add(new B_CONVITES_FRASESMap());
            modelBuilder.Configurations.Add(new B_EMPRESA_AJUSTESMap());
            modelBuilder.Configurations.Add(new B_EMPRESA_USUARIOSMap());
            modelBuilder.Configurations.Add(new B_MODULOS_DISPONIVEISMap());
            modelBuilder.Configurations.Add(new B_PLANOS_DISPONIVEISMap());
            modelBuilder.Configurations.Add(new B_PLANOS_PORTALMap());
            modelBuilder.Configurations.Add(new B_PORTAL_BANNER_PROMOCIONALMap());
            modelBuilder.Configurations.Add(new B_PORTAL_DENUNCIASMap());
            modelBuilder.Configurations.Add(new B_PORTAL_PROMOCAOMap());
            modelBuilder.Configurations.Add(new B_POSSIVEL_PARCEIROMap());
            modelBuilder.Configurations.Add(new B_SERVICO_MAPEAR_REGIAOMap());
            modelBuilder.Configurations.Add(new B_SERVICO_PARCEIRO_MAPEADOMap());
            modelBuilder.Configurations.Add(new B_SERVICO_PARCEIRO_MAPEADO_IMAGENSMap());
            modelBuilder.Configurations.Add(new C_CONVITE_OPCOESMap());
            modelBuilder.Configurations.Add(new C_FORMA_PAGAMENTOMap());
            modelBuilder.Configurations.Add(new CAMPANHAMap());
            modelBuilder.Configurations.Add(new CAMPANHA_CONTATOS_ENVIADOSMap());
            modelBuilder.Configurations.Add(new CAMPANHA_LINHA_TEMPOMap());
            modelBuilder.Configurations.Add(new CAMPANHA_LISTAMap());
            modelBuilder.Configurations.Add(new CAMPOS_COMPONENTESMap());
            modelBuilder.Configurations.Add(new CAMPOS_DESCRICAOMap());
            modelBuilder.Configurations.Add(new CAMPOS_DISPONIVEISMap());
            modelBuilder.Configurations.Add(new CATEGORIA_ALUGUELMap());
            modelBuilder.Configurations.Add(new CATEGORIA_DIVERSOSMap());
            modelBuilder.Configurations.Add(new CATEGORIA_ITENSMap());
            modelBuilder.Configurations.Add(new CATEGORIA_SERVICOSMap());
            modelBuilder.Configurations.Add(new CLIENTEMap());
            modelBuilder.Configurations.Add(new CLIENTE_ANIVERSARIANTESMap());
            modelBuilder.Configurations.Add(new CLIENTE_PROPOSTAMap());
            modelBuilder.Configurations.Add(new CLIENTE_TIMELINEMap());
            modelBuilder.Configurations.Add(new COLABORADOR_DEVICESMap());
            modelBuilder.Configurations.Add(new COLABORADOR_NOTIFICACAOMap());
            modelBuilder.Configurations.Add(new COLABORADOREMap());
            modelBuilder.Configurations.Add(new CONTATO_PLATAFORMAMap());
            modelBuilder.Configurations.Add(new CONVITE_ANIVERSARIANTESMap());
            modelBuilder.Configurations.Add(new CONVITE_CONVIDADOSMap());
            modelBuilder.Configurations.Add(new CONVITE_CONVIDADOS_FACEBOOKMap());
            modelBuilder.Configurations.Add(new CONVITE_CONVIDADOS_FAMILIARESMap());
            modelBuilder.Configurations.Add(new CONVITE_PERGUNTA_OPCOESMap());
            modelBuilder.Configurations.Add(new CONVITE_RESPOSTAS_OPCOESMap());
            modelBuilder.Configurations.Add(new CONVITE_TEMPLATEMap());
            modelBuilder.Configurations.Add(new EMPRESAMap());
            modelBuilder.Configurations.Add(new EMPRESA_ARQUIVOSMap());
            modelBuilder.Configurations.Add(new EMPRESA_CARRINHOMap());
            modelBuilder.Configurations.Add(new EMPRESA_CARRINHO_CUPOMMap());
            modelBuilder.Configurations.Add(new EMPRESA_CNAESMap());
            modelBuilder.Configurations.Add(new EMPRESA_CONFIGURACAOMap());
            modelBuilder.Configurations.Add(new EMPRESA_HISTORICO_PAGAMENTOMap());
            modelBuilder.Configurations.Add(new EMPRESA_MODULOSMap());
            modelBuilder.Configurations.Add(new EMPRESA_PACOTE_SMSMap());
            modelBuilder.Configurations.Add(new EMPRESA_PAGAMENTO_CARTAOMap());
            modelBuilder.Configurations.Add(new ENDERECOMap());
            modelBuilder.Configurations.Add(new EVENTOMap());
            modelBuilder.Configurations.Add(new EVENTO_ALUGUELMap());
            modelBuilder.Configurations.Add(new EVENTO_DIVERSOSMap());
            modelBuilder.Configurations.Add(new EVENTO_PRESTADORESMap());
            modelBuilder.Configurations.Add(new EVENTO_SERVICOSMap());
            modelBuilder.Configurations.Add(new EVENTOS_PACOTESMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_BANDEIRASMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_BOLETOMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_CHEQUEMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_CREDITOMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_DEBITOMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_DEPOSITOMap());
            modelBuilder.Configurations.Add(new FINANCEIRO_P_ESPECIEMap());
            modelBuilder.Configurations.Add(new FORNECEDORMap());
            modelBuilder.Configurations.Add(new FORNECEDOR_ALUGUELMap());
            modelBuilder.Configurations.Add(new FORNECEDOR_CARDAPIOMap());
            modelBuilder.Configurations.Add(new FORNECEDOR_INGREDIENTEMap());
            modelBuilder.Configurations.Add(new HORARIOMap());
            modelBuilder.Configurations.Add(new IMAGENS_DOS_EVENTOSMap());
            modelBuilder.Configurations.Add(new IMAGENS_ESPACO_LOCAL_EVENTOSMap());
            modelBuilder.Configurations.Add(new IMAGENS_GALERIA_EVENTOSMap());
            modelBuilder.Configurations.Add(new IMAGENS_ITEMMap());
            modelBuilder.Configurations.Add(new IMAGENS_PACOTESMap());
            modelBuilder.Configurations.Add(new INGREDIENTEMap());
            modelBuilder.Configurations.Add(new ITENS_ALUGUELMap());
            modelBuilder.Configurations.Add(new ITENS_CARDAPIOMap());
            modelBuilder.Configurations.Add(new ITENS_DIVERSOSMap());
            modelBuilder.Configurations.Add(new ITENS_INGREDIENTESMap());
            modelBuilder.Configurations.Add(new ITENS_SERVICOSMap());
            modelBuilder.Configurations.Add(new LOCAL_EVENTOMap());
            modelBuilder.Configurations.Add(new LOCAL_EVENTO_COMENTARIOSMap());
            modelBuilder.Configurations.Add(new LOCAL_EVENTO_IMAGENSMap());
            modelBuilder.Configurations.Add(new LOCAL_EVENTO_SOCIALMap());
            modelBuilder.Configurations.Add(new NOTIFICACOMap());
            modelBuilder.Configurations.Add(new ONDE_CONHECEUMap());
            modelBuilder.Configurations.Add(new ORCAMENTOMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_ANOTACOESMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_ITENSMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PACOTEMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PACOTESMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PROPOSTAMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PROPOSTA_ABASMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PROPOSTA_DESCRICAOMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PROPOSTA_PACOTEMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_PROPOSTA_PACOTE_VALORESMap());
            modelBuilder.Configurations.Add(new ORCAMENTO_VISITASMap());
            modelBuilder.Configurations.Add(new PACOTE_CATEGORIA_ITENSMap());
            modelBuilder.Configurations.Add(new PACOTEMap());
            modelBuilder.Configurations.Add(new PACOTES_ITENSMap());
            modelBuilder.Configurations.Add(new PERFIL_COLABORADORMap());
            modelBuilder.Configurations.Add(new PESQUISA_PERGUNTAMap());
            modelBuilder.Configurations.Add(new PESQUISA_PERGUNTA_OPCOESMap());
            modelBuilder.Configurations.Add(new PESQUISA_PESQUISASMap());
            modelBuilder.Configurations.Add(new PESQUISA_RESPOSTAMap());
            modelBuilder.Configurations.Add(new PESQUISA_RESPOSTA_OPCOESMap());
            modelBuilder.Configurations.Add(new PORTAL_ANUNCIANTE_DESTAQUEMap());
            modelBuilder.Configurations.Add(new PORTAL_ANUNCIANTE_HEADERMap());
            modelBuilder.Configurations.Add(new PORTAL_ANUNCIANTE_HOMEMap());
            modelBuilder.Configurations.Add(new PORTAL_ANUNCIANTE_PROMOCAOMap());
            modelBuilder.Configurations.Add(new PORTAL_ANUNCIANTE_TOPMENUMap());
            modelBuilder.Configurations.Add(new PORTAL_CLIENTE_FAVORITOSMap());
            modelBuilder.Configurations.Add(new PORTAL_CLIENTE_PERFILMap());
            modelBuilder.Configurations.Add(new PORTAL_CLIENTE_PESQUISASMap());
            modelBuilder.Configurations.Add(new PORTAL_VISUALIZACOESMap());
            modelBuilder.Configurations.Add(new POTENCIAL_CLIENTEMap());
            modelBuilder.Configurations.Add(new PRESTADORMap());
            modelBuilder.Configurations.Add(new PRESTADOR_FUNCAOMap());
            modelBuilder.Configurations.Add(new PRESTADOR_SERVICOMap());
            modelBuilder.Configurations.Add(new SUB_CATEGORIA_ITENSMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new TIPO_SERVICOMap());
            modelBuilder.Configurations.Add(new UNIDADE_MEDIDAMap());
            modelBuilder.Configurations.Add(new VISITAS_AGENDADASMap());
            modelBuilder.Configurations.Add(new VISITAS_DISPONIVEISMap());
        }
    }
}
