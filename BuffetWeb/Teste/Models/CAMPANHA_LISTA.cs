using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CAMPANHA_LISTA
    {
        public CAMPANHA_LISTA()
        {
            this.CAMPANHAs = new List<CAMPANHA>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public System.DateTime data_inclusao { get; set; }
        public Nullable<int> regras_idade_minima { get; set; }
        public Nullable<int> regras_idade_maxima { get; set; }
        public string regras_reigao { get; set; }
        public Nullable<bool> com_filhos { get; set; }
        public string sexo { get; set; }
        public Nullable<bool> com_orcamentos { get; set; }
        public int id_empresa { get; set; }
        public Nullable<bool> confirmados { get; set; }
        public virtual ICollection<CAMPANHA> CAMPANHAs { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
