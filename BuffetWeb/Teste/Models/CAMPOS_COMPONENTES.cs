using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CAMPOS_COMPONENTES
    {
        public int id { get; set; }
        public int id_campo_descricao { get; set; }
        public string label { get; set; }
        public string name { get; set; }
        public string id_component { get; set; }
        public string type { get; set; }
        public Nullable<int> id_values_select { get; set; }
        public bool required { get; set; }
        public string regex { get; set; }
        public int max_length { get; set; }
        public string css { get; set; }
        public string function { get; set; }
        public int ordem { get; set; }
        public virtual CAMPOS_DESCRICAO CAMPOS_DESCRICAO { get; set; }
    }
}
