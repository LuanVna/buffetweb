using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CAMPOS_DESCRICAO
    {
        public CAMPOS_DESCRICAO()
        {
            this.CAMPOS_COMPONENTES = new List<CAMPOS_COMPONENTES>();
            this.CAMPOS_DISPONIVEIS = new List<CAMPOS_DISPONIVEIS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public virtual ICollection<CAMPOS_COMPONENTES> CAMPOS_COMPONENTES { get; set; }
        public virtual ICollection<CAMPOS_DISPONIVEIS> CAMPOS_DISPONIVEIS { get; set; }
    }
}
