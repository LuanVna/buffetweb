using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CATEGORIA_ITENS
    {
        public CATEGORIA_ITENS()
        {
            this.ITENS_CARDAPIO = new List<ITENS_CARDAPIO>();
            this.PACOTES_ITENS = new List<PACOTES_ITENS>();
            this.PACOTE_CATEGORIA_ITENS = new List<PACOTE_CATEGORIA_ITENS>();
            this.SUB_CATEGORIA_ITENS = new List<SUB_CATEGORIA_ITENS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int id_empresa { get; set; }
        public string url_image { get; set; }
        public virtual ICollection<ITENS_CARDAPIO> ITENS_CARDAPIO { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<PACOTES_ITENS> PACOTES_ITENS { get; set; }
        public virtual ICollection<PACOTE_CATEGORIA_ITENS> PACOTE_CATEGORIA_ITENS { get; set; }
        public virtual ICollection<SUB_CATEGORIA_ITENS> SUB_CATEGORIA_ITENS { get; set; }
    }
}
