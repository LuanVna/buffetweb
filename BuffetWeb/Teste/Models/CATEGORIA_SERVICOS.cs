using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CATEGORIA_SERVICOS
    {
        public CATEGORIA_SERVICOS()
        {
            this.ITENS_DIVERSOS = new List<ITENS_DIVERSOS>();
            this.ITENS_SERVICOS = new List<ITENS_SERVICOS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int id_empresa { get; set; }
        public string url_image { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<ITENS_DIVERSOS> ITENS_DIVERSOS { get; set; }
        public virtual ICollection<ITENS_SERVICOS> ITENS_SERVICOS { get; set; }
    }
}
