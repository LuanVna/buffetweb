using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CLIENTE_ANIVERSARIANTES
    {
        public CLIENTE_ANIVERSARIANTES()
        {
            this.CONVITE_ANIVERSARIANTES = new List<CONVITE_ANIVERSARIANTES>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public System.DateTime nascimento { get; set; }
        public string parentesco { get; set; }
        public System.DateTime desde { get; set; }
        public int id_cliente { get; set; }
        public int id_empresa { get; set; }
        public virtual CLIENTE CLIENTE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<CONVITE_ANIVERSARIANTES> CONVITE_ANIVERSARIANTES { get; set; }
    }
}
