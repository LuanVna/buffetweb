using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class COLABORADORE
    {
        public COLABORADORE()
        {
            this.COLABORADOR_DEVICES = new List<COLABORADOR_DEVICES>();
            this.FINANCEIRO_P_ESPECIE = new List<FINANCEIRO_P_ESPECIE>();
            this.FINANCEIRO_P_BOLETO = new List<FINANCEIRO_P_BOLETO>();
            this.FINANCEIRO_P_CHEQUE = new List<FINANCEIRO_P_CHEQUE>();
            this.FINANCEIRO_P_CREDITO = new List<FINANCEIRO_P_CREDITO>();
            this.FINANCEIRO_P_DEBITO = new List<FINANCEIRO_P_DEBITO>();
            this.FINANCEIRO_P_DEPOSITO = new List<FINANCEIRO_P_DEPOSITO>();
            this.ORCAMENTO_ANOTACOES = new List<ORCAMENTO_ANOTACOES>();
            this.CONTATO_PLATAFORMA = new List<CONTATO_PLATAFORMA>();
            this.NOTIFICACOES = new List<NOTIFICACO>();
            this.NOTIFICACOES1 = new List<NOTIFICACO>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public string senha { get; set; }
        public string sessao { get; set; }
        public int id_perfil { get; set; }
        public Nullable<bool> mostrar_dica { get; set; }
        public int id_empresa { get; set; }
        public string url { get; set; }
        public Nullable<int> id_notificacao { get; set; }
        public Nullable<bool> administrador { get; set; }
        public string celular { get; set; }
        public virtual ICollection<COLABORADOR_DEVICES> COLABORADOR_DEVICES { get; set; }
        public virtual COLABORADOR_NOTIFICACAO COLABORADOR_NOTIFICACAO { get; set; }
        public virtual ICollection<FINANCEIRO_P_ESPECIE> FINANCEIRO_P_ESPECIE { get; set; }
        public virtual ICollection<FINANCEIRO_P_BOLETO> FINANCEIRO_P_BOLETO { get; set; }
        public virtual ICollection<FINANCEIRO_P_CHEQUE> FINANCEIRO_P_CHEQUE { get; set; }
        public virtual ICollection<FINANCEIRO_P_CREDITO> FINANCEIRO_P_CREDITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEBITO> FINANCEIRO_P_DEBITO { get; set; }
        public virtual ICollection<FINANCEIRO_P_DEPOSITO> FINANCEIRO_P_DEPOSITO { get; set; }
        public virtual ICollection<ORCAMENTO_ANOTACOES> ORCAMENTO_ANOTACOES { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PERFIL_COLABORADOR PERFIL_COLABORADOR { get; set; }
        public virtual ICollection<CONTATO_PLATAFORMA> CONTATO_PLATAFORMA { get; set; }
        public virtual ICollection<NOTIFICACO> NOTIFICACOES { get; set; }
        public virtual ICollection<NOTIFICACO> NOTIFICACOES1 { get; set; }
    }
}
