using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class COLABORADOR_DEVICES
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string autenticacao { get; set; }
        public bool status { get; set; }
        public System.DateTime desde { get; set; }
        public int id_colaborador { get; set; }
        public int id_empresa { get; set; }
        public string device { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
