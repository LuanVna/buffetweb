using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class COLABORADOR_NOTIFICACAO
    {
        public COLABORADOR_NOTIFICACAO()
        {
            this.COLABORADORES = new List<COLABORADORE>();
        }

        public int id { get; set; }
        public bool orcamento_novo_pedido { get; set; }
        public bool orcamento_gendar_visita { get; set; }
        public bool orcamento_fecha_pedido { get; set; }
        public bool convite { get; set; }
        public bool financeiro_pagamento_efetuado { get; set; }
        public bool estoque { get; set; }
        public bool notificacao_email { get; set; }
        public bool pesquisa_concluida { get; set; }
        public virtual ICollection<COLABORADORE> COLABORADORES { get; set; }
    }
}
