using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CONTATO_PLATAFORMA
    {
        public int id { get; set; }
        public string email { get; set; }
        public string titulo { get; set; }
        public string mensagem_envio { get; set; }
        public System.DateTime data_envio { get; set; }
        public string mensagem_resposta { get; set; }
        public Nullable<System.DateTime> data_resposta { get; set; }
        public int id_colaborador { get; set; }
        public int id_empresa { get; set; }
        public string autenticacao { get; set; }
        public string telefone { get; set; }
        public string tipo_contato { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
