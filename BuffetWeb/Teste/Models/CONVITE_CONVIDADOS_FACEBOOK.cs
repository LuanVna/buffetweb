using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CONVITE_CONVIDADOS_FACEBOOK
    {
        public int id { get; set; }
        public int id_orcamento { get; set; }
        public string id_facebook { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string link { get; set; }
        public string locale { get; set; }
        public string amigo_de_nome { get; set; }
        public string amigo_de_id_facebook { get; set; }
        public bool ativo { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
