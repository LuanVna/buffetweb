using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CONVITE_CONVIDADOS_FAMILIARES
    {
        public int id { get; set; }
        public int id_convidado { get; set; }
        public string nome { get; set; }
        public string tipo_convidado { get; set; }
        public int id_empresa { get; set; }
        public int id_orcamento { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public virtual CONVITE_CONVIDADOS CONVITE_CONVIDADOS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
