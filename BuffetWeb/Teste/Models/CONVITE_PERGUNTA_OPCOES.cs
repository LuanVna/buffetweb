using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CONVITE_PERGUNTA_OPCOES
    {
        public CONVITE_PERGUNTA_OPCOES()
        {
            this.CONVITE_RESPOSTAS_OPCOES = new List<CONVITE_RESPOSTAS_OPCOES>();
        }

        public int id { get; set; }
        public string pergunta { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<CONVITE_RESPOSTAS_OPCOES> CONVITE_RESPOSTAS_OPCOES { get; set; }
    }
}
