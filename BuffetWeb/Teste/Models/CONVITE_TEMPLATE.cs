using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class CONVITE_TEMPLATE
    {
        public int id { get; set; }
        public string titulo_convite { get; set; }
        public string titulo_tamanho { get; set; }
        public string titulo_top { get; set; }
        public string titulo_left { get; set; }
        public string titulo_descricao { get; set; }
        public string descricao_top { get; set; }
        public string descricao_left { get; set; }
        public string tamanho_descricao { get; set; }
        public string aniversariante_tamanho { get; set; }
        public string aniversariantes_top { get; set; }
        public string aniversariantes_left { get; set; }
        public string datahora_top { get; set; }
        public string datahora_left { get; set; }
        public string datahora_tamanho { get; set; }
        public int id_empresa { get; set; }
        public int id_orcamento { get; set; }
        public string url_image { get; set; }
        public int id_convite { get; set; }
        public string titulo_datahora { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
