using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class C_CONVITE_OPCOES
    {
        public C_CONVITE_OPCOES()
        {
            this.CONVITE_RESPOSTAS_OPCOES = new List<CONVITE_RESPOSTAS_OPCOES>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public virtual ICollection<CONVITE_RESPOSTAS_OPCOES> CONVITE_RESPOSTAS_OPCOES { get; set; }
    }
}
