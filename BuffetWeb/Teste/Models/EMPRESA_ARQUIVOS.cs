using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class EMPRESA_ARQUIVOS
    {
        public int id { get; set; }
        public string chave { get; set; }
        public string valor { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
