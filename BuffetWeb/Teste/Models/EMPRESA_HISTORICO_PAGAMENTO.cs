using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class EMPRESA_HISTORICO_PAGAMENTO
    {
        public int id { get; set; }
        public Nullable<int> id_empresa { get; set; }
        public string produto { get; set; }
        public System.DateTime comprado_em { get; set; }
        public string cartao { get; set; }
        public string autenticacao_externa { get; set; }
        public string autenticacao_interna { get; set; }
        public Nullable<int> id_cupom_promocional { get; set; }
        public decimal valor { get; set; }
        public string forma_pagamento { get; set; }
        public string status { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual EMPRESA_CARRINHO_CUPOM EMPRESA_CARRINHO_CUPOM { get; set; }
    }
}
