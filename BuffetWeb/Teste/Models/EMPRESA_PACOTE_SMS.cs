using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class EMPRESA_PACOTE_SMS
    {
        public int id { get; set; }
        public int quantidade_pacote { get; set; }
        public Nullable<int> quantidade_disponivel { get; set; }
        public decimal valor { get; set; }
        public string autenticacao { get; set; }
        public System.DateTime data_compra { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
