using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ENDERECO
    {
        public ENDERECO()
        {
            this.CLIENTEs = new List<CLIENTE>();
            this.EMPRESAs = new List<EMPRESA>();
            this.FORNECEDORs = new List<FORNECEDOR>();
            this.LOCAL_EVENTO = new List<LOCAL_EVENTO>();
        }

        public int id { get; set; }
        public string cep { get; set; }
        public string endereco1 { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string complemento { get; set; }
        public string numero { get; set; }
        public int id_empresa { get; set; }
        public Nullable<decimal> longitude { get; set; }
        public Nullable<decimal> latitude { get; set; }
        public virtual ICollection<CLIENTE> CLIENTEs { get; set; }
        public virtual ICollection<EMPRESA> EMPRESAs { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<FORNECEDOR> FORNECEDORs { get; set; }
        public virtual ICollection<LOCAL_EVENTO> LOCAL_EVENTO { get; set; }
    }
}
