using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class EVENTO
    {
        public EVENTO()
        {
            this.CAMPOS_DISPONIVEIS = new List<CAMPOS_DISPONIVEIS>();
            this.EVENTO_PRESTADORES = new List<EVENTO_PRESTADORES>();
            this.ORCAMENTOes = new List<ORCAMENTO>();
            this.EVENTO_ALUGUEL = new List<EVENTO_ALUGUEL>();
            this.EVENTO_DIVERSOS = new List<EVENTO_DIVERSOS>();
            this.EVENTOS_PACOTES = new List<EVENTOS_PACOTES>();
            this.EVENTO_SERVICOS = new List<EVENTO_SERVICOS>();
            this.POTENCIAL_CLIENTE = new List<POTENCIAL_CLIENTE>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public Nullable<System.DateTime> evento_de { get; set; }
        public Nullable<System.DateTime> evento_ate { get; set; }
        public string descricao { get; set; }
        public Nullable<bool> de_epoca { get; set; }
        public Nullable<int> quantidade_disponivel { get; set; }
        public int id_empresa { get; set; }
        public bool dividir_valor_pessoa { get; set; }
        public bool status { get; set; }
        public string url { get; set; }
        public virtual ICollection<CAMPOS_DISPONIVEIS> CAMPOS_DISPONIVEIS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<EVENTO_PRESTADORES> EVENTO_PRESTADORES { get; set; }
        public virtual ICollection<ORCAMENTO> ORCAMENTOes { get; set; }
        public virtual ICollection<EVENTO_ALUGUEL> EVENTO_ALUGUEL { get; set; }
        public virtual ICollection<EVENTO_DIVERSOS> EVENTO_DIVERSOS { get; set; }
        public virtual ICollection<EVENTOS_PACOTES> EVENTOS_PACOTES { get; set; }
        public virtual ICollection<EVENTO_SERVICOS> EVENTO_SERVICOS { get; set; }
        public virtual ICollection<POTENCIAL_CLIENTE> POTENCIAL_CLIENTE { get; set; }
    }
}
