using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class FINANCEIRO_BANDEIRAS
    {
        public FINANCEIRO_BANDEIRAS()
        {
            this.FINANCEIRO_P_CREDITO = new List<FINANCEIRO_P_CREDITO>();
        }

        public int id { get; set; }
        public string bandeira { get; set; }
        public bool ativo { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<FINANCEIRO_P_CREDITO> FINANCEIRO_P_CREDITO { get; set; }
    }
}
