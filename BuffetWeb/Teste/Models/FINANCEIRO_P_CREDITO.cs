using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class FINANCEIRO_P_CREDITO
    {
        public int id { get; set; }
        public System.DateTime data { get; set; }
        public decimal valor { get; set; }
        public int n_parcelas { get; set; }
        public int n_autorizacao { get; set; }
        public int id_bandeira { get; set; }
        public int id_orcamento { get; set; }
        public int id_colaborador { get; set; }
        public int id_empresa { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual FINANCEIRO_BANDEIRAS FINANCEIRO_BANDEIRAS { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
