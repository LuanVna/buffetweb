using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class FORNECEDOR_CARDAPIO
    {
        public int id { get; set; }
        public int id_fornecedor { get; set; }
        public int id_cardapio { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_CARDAPIO ITENS_CARDAPIO { get; set; }
    }
}
