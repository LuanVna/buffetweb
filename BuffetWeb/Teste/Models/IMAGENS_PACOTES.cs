using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class IMAGENS_PACOTES
    {
        public int id { get; set; }
        public Nullable<int> id_pacote { get; set; }
        public string nome { get; set; }
        public string url { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PACOTE PACOTE { get; set; }
    }
}
