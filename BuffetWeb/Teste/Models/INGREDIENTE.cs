using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class INGREDIENTE
    {
        public INGREDIENTE()
        {
            this.FORNECEDOR_INGREDIENTE = new List<FORNECEDOR_INGREDIENTE>();
            this.ITENS_INGREDIENTES = new List<ITENS_INGREDIENTES>();
        }

        public int id { get; set; }
        public string marca { get; set; }
        public string quantidade_embalagem { get; set; }
        public int id_unidade_medida { get; set; }
        public decimal valor_custo { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<FORNECEDOR_INGREDIENTE> FORNECEDOR_INGREDIENTE { get; set; }
        public virtual ICollection<ITENS_INGREDIENTES> ITENS_INGREDIENTES { get; set; }
        public virtual UNIDADE_MEDIDA UNIDADE_MEDIDA { get; set; }
    }
}
