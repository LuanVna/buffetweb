using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ITENS_CARDAPIO
    {
        public ITENS_CARDAPIO()
        {
            this.FORNECEDOR_CARDAPIO = new List<FORNECEDOR_CARDAPIO>();
            this.ITENS_INGREDIENTES = new List<ITENS_INGREDIENTES>();
            this.ORCAMENTO_ITENS = new List<ORCAMENTO_ITENS>();
            this.PACOTES_ITENS = new List<PACOTES_ITENS>();
            this.ORCAMENTO_PACOTE = new List<ORCAMENTO_PACOTE>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public int id_categoria { get; set; }
        public Nullable<bool> status { get; set; }
        public string descricao { get; set; }
        public Nullable<decimal> valor_compra { get; set; }
        public Nullable<int> id_unidade_medida { get; set; }
        public decimal rendimento { get; set; }
        public int id_empresa { get; set; }
        public string receita { get; set; }
        public Nullable<int> id_sub_categoria { get; set; }
        public Nullable<decimal> quantidade_embalagem { get; set; }
        public Nullable<decimal> custo_pessoa { get; set; }
        public Nullable<decimal> margem_lucro { get; set; }
        public Nullable<decimal> venda_pessoa { get; set; }
        public string url_image { get; set; }
        public virtual CATEGORIA_ITENS CATEGORIA_ITENS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<FORNECEDOR_CARDAPIO> FORNECEDOR_CARDAPIO { get; set; }
        public virtual ICollection<ITENS_INGREDIENTES> ITENS_INGREDIENTES { get; set; }
        public virtual ICollection<ORCAMENTO_ITENS> ORCAMENTO_ITENS { get; set; }
        public virtual SUB_CATEGORIA_ITENS SUB_CATEGORIA_ITENS { get; set; }
        public virtual UNIDADE_MEDIDA UNIDADE_MEDIDA { get; set; }
        public virtual ICollection<PACOTES_ITENS> PACOTES_ITENS { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE { get; set; }
    }
}
