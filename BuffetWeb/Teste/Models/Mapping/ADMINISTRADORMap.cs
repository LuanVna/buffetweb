using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ADMINISTRADORMap : EntityTypeConfiguration<ADMINISTRADOR>
    {
        public ADMINISTRADORMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.senha)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ADMINISTRADOR");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
        }
    }
}
