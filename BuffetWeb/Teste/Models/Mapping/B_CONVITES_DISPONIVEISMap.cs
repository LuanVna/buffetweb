using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_CONVITES_DISPONIVEISMap : EntityTypeConfiguration<B_CONVITES_DISPONIVEIS>
    {
        public B_CONVITES_DISPONIVEISMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.tema)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.url_image)
                .HasMaxLength(500);

            this.Property(t => t.titulo)
                .HasMaxLength(200);

            this.Property(t => t.titulo_cor)
                .HasMaxLength(200);

            this.Property(t => t.titulo_fonte)
                .HasMaxLength(200);

            this.Property(t => t.titulo_tamanho)
                .HasMaxLength(200);

            this.Property(t => t.titulo_top)
                .HasMaxLength(200);

            this.Property(t => t.titulo_left)
                .HasMaxLength(200);

            this.Property(t => t.datahora_cor)
                .HasMaxLength(200);

            this.Property(t => t.datahora_fonte)
                .HasMaxLength(200);

            this.Property(t => t.datahora_tamanho)
                .HasMaxLength(200);

            this.Property(t => t.datahora_top)
                .HasMaxLength(200);

            this.Property(t => t.datahora_left)
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            this.Property(t => t.descricao_cor)
                .HasMaxLength(200);

            this.Property(t => t.descricao_fonte)
                .HasMaxLength(200);

            this.Property(t => t.descricao_tamanho)
                .HasMaxLength(200);

            this.Property(t => t.descricao_top)
                .HasMaxLength(200);

            this.Property(t => t.descricao_left)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_cor)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_fonte)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_tamanho)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_top)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_left)
                .HasMaxLength(200);

            this.Property(t => t.tipo_evento)
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_titulo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.datahora_titulo)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("B_CONVITES_DISPONIVEIS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.tema).HasColumnName("tema");
            this.Property(t => t.url_image).HasColumnName("url_image");
            this.Property(t => t.disponivel).HasColumnName("disponivel");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.titulo_cor).HasColumnName("titulo_cor");
            this.Property(t => t.titulo_fonte).HasColumnName("titulo_fonte");
            this.Property(t => t.titulo_tamanho).HasColumnName("titulo_tamanho");
            this.Property(t => t.titulo_top).HasColumnName("titulo_top");
            this.Property(t => t.titulo_left).HasColumnName("titulo_left");
            this.Property(t => t.datahora_cor).HasColumnName("datahora_cor");
            this.Property(t => t.datahora_fonte).HasColumnName("datahora_fonte");
            this.Property(t => t.datahora_tamanho).HasColumnName("datahora_tamanho");
            this.Property(t => t.datahora_top).HasColumnName("datahora_top");
            this.Property(t => t.datahora_left).HasColumnName("datahora_left");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.descricao_cor).HasColumnName("descricao_cor");
            this.Property(t => t.descricao_fonte).HasColumnName("descricao_fonte");
            this.Property(t => t.descricao_tamanho).HasColumnName("descricao_tamanho");
            this.Property(t => t.descricao_top).HasColumnName("descricao_top");
            this.Property(t => t.descricao_left).HasColumnName("descricao_left");
            this.Property(t => t.aniversariantes_cor).HasColumnName("aniversariantes_cor");
            this.Property(t => t.aniversariantes_fonte).HasColumnName("aniversariantes_fonte");
            this.Property(t => t.aniversariantes_tamanho).HasColumnName("aniversariantes_tamanho");
            this.Property(t => t.aniversariantes_top).HasColumnName("aniversariantes_top");
            this.Property(t => t.aniversariantes_left).HasColumnName("aniversariantes_left");
            this.Property(t => t.tipo_evento).HasColumnName("tipo_evento");
            this.Property(t => t.aniversariantes_titulo).HasColumnName("aniversariantes_titulo");
            this.Property(t => t.datahora_titulo).HasColumnName("datahora_titulo");
        }
    }
}
