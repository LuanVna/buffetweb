using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_CONVITES_FONTESMap : EntityTypeConfiguration<B_CONVITES_FONTES>
    {
        public B_CONVITES_FONTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome_fonte)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.nome_visivel)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.caminho_fonte)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_CONVITES_FONTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome_fonte).HasColumnName("nome_fonte");
            this.Property(t => t.nome_visivel).HasColumnName("nome_visivel");
            this.Property(t => t.caminho_fonte).HasColumnName("caminho_fonte");
            this.Property(t => t.css).HasColumnName("css");
        }
    }
}
