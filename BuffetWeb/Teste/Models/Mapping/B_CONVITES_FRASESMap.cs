using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_CONVITES_FRASESMap : EntityTypeConfiguration<B_CONVITES_FRASES>
    {
        public B_CONVITES_FRASESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_CONVITES_FRASES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.descricao).HasColumnName("descricao");
        }
    }
}
