using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_EMPRESA_AJUSTESMap : EntityTypeConfiguration<B_EMPRESA_AJUSTES>
    {
        public B_EMPRESA_AJUSTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.chave)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.valor)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_EMPRESA_AJUSTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.chave).HasColumnName("chave");
            this.Property(t => t.valor).HasColumnName("valor");
        }
    }
}
