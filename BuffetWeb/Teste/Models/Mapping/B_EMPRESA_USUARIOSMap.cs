using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_EMPRESA_USUARIOSMap : EntityTypeConfiguration<B_EMPRESA_USUARIOS>
    {
        public B_EMPRESA_USUARIOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.senha)
                .IsRequired();

            this.Property(t => t.autenticacao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.celular)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("B_EMPRESA_USUARIOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.celular).HasColumnName("celular");
        }
    }
}
