using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_MODULOS_DISPONIVEISMap : EntityTypeConfiguration<B_MODULOS_DISPONIVEIS>
    {
        public B_MODULOS_DISPONIVEISMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_MODULOS_DISPONIVEIS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.meses_plano).HasColumnName("meses_plano");
            this.Property(t => t.valor).HasColumnName("valor");
        }
    }
}
