using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_PLANOS_DISPONIVEISMap : EntityTypeConfiguration<B_PLANOS_DISPONIVEIS>
    {
        public B_PLANOS_DISPONIVEISMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            this.Property(t => t.img_miniatura)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.img_normal)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("B_PLANOS_DISPONIVEIS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.img_miniatura).HasColumnName("img_miniatura");
            this.Property(t => t.img_normal).HasColumnName("img_normal");
            this.Property(t => t.valor_mensal).HasColumnName("valor_mensal");
            this.Property(t => t.valor_trimestral).HasColumnName("valor_trimestral");
            this.Property(t => t.valor_semestral).HasColumnName("valor_semestral");
            this.Property(t => t.valor_anual).HasColumnName("valor_anual");
            this.Property(t => t.disponivel_de).HasColumnName("disponivel_de");
            this.Property(t => t.disponivel_ate).HasColumnName("disponivel_ate");
            this.Property(t => t.id_plano_portal).HasColumnName("id_plano_portal");

            // Relationships
            this.HasOptional(t => t.B_PLANOS_PORTAL)
                .WithMany(t => t.B_PLANOS_DISPONIVEIS)
                .HasForeignKey(d => d.id_plano_portal);

        }
    }
}
