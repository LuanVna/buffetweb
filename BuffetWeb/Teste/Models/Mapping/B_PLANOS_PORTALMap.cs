using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_PLANOS_PORTALMap : EntityTypeConfiguration<B_PLANOS_PORTAL>
    {
        public B_PLANOS_PORTALMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.anuncio)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("B_PLANOS_PORTAL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.destaque_pesquisa_zona).HasColumnName("destaque_pesquisa_zona");
            this.Property(t => t.destaque_mapa).HasColumnName("destaque_mapa");
            this.Property(t => t.envio_proposta).HasColumnName("envio_proposta");
            this.Property(t => t.envio_proposta_quantidade_mes).HasColumnName("envio_proposta_quantidade_mes");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.anuncio).HasColumnName("anuncio");
        }
    }
}
