using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_PORTAL_BANNER_PROMOCIONALMap : EntityTypeConfiguration<B_PORTAL_BANNER_PROMOCIONAL>
    {
        public B_PORTAL_BANNER_PROMOCIONALMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.descricao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.tipo_banner)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.url)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.url_imagem)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("B_PORTAL_BANNER_PROMOCIONAL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.tipo_banner).HasColumnName("tipo_banner");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.url_imagem).HasColumnName("url_imagem");
            this.Property(t => t.duracao).HasColumnName("duracao");
            this.Property(t => t.indeterminado).HasColumnName("indeterminado");
            this.Property(t => t.disponivel_de).HasColumnName("disponivel_de");
            this.Property(t => t.disponivel_ate).HasColumnName("disponivel_ate");
        }
    }
}
