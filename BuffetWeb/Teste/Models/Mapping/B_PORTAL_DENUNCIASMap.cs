using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_PORTAL_DENUNCIASMap : EntityTypeConfiguration<B_PORTAL_DENUNCIAS>
    {
        public B_PORTAL_DENUNCIASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.comentario)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_PORTAL_DENUNCIAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.comentario).HasColumnName("comentario");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.resolvido).HasColumnName("resolvido");

            // Relationships
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.B_PORTAL_DENUNCIAS)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
