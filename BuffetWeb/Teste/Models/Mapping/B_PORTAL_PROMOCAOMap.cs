using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_PORTAL_PROMOCAOMap : EntityTypeConfiguration<B_PORTAL_PROMOCAO>
    {
        public B_PORTAL_PROMOCAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.descricao)
                .IsRequired();

            this.Property(t => t.url_imagem)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("B_PORTAL_PROMOCAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.disponivel_de).HasColumnName("disponivel_de");
            this.Property(t => t.disponivel_ate).HasColumnName("disponivel_ate");
            this.Property(t => t.desconto_de).HasColumnName("desconto_de");
            this.Property(t => t.url_imagem).HasColumnName("url_imagem");
        }
    }
}
