using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_POSSIVEL_PARCEIROMap : EntityTypeConfiguration<B_POSSIVEL_PARCEIRO>
    {
        public B_POSSIVEL_PARCEIROMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.celular)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.operadora)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.senha)
                .IsRequired();

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("B_POSSIVEL_PARCEIRO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.celular).HasColumnName("celular");
            this.Property(t => t.operadora).HasColumnName("operadora");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.status).HasColumnName("status");
        }
    }
}
