using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_SERVICO_MAPEAR_REGIAOMap : EntityTypeConfiguration<B_SERVICO_MAPEAR_REGIAO>
    {
        public B_SERVICO_MAPEAR_REGIAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.url)
                .IsRequired();

            this.Property(t => t.concorrente)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("B_SERVICO_MAPEAR_REGIAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.quantidade_mapeada).HasColumnName("quantidade_mapeada");
            this.Property(t => t.ultima_atualizacao).HasColumnName("ultima_atualizacao");
            this.Property(t => t.concorrente).HasColumnName("concorrente");
        }
    }
}
