using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_SERVICO_PARCEIRO_MAPEADOMap : EntityTypeConfiguration<B_SERVICO_PARCEIRO_MAPEADO>
    {
        public B_SERVICO_PARCEIRO_MAPEADOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.telefone)
                .HasMaxLength(20);

            this.Property(t => t.rua)
                .HasMaxLength(200);

            this.Property(t => t.localidade)
                .HasMaxLength(300);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.plano_atual)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.de_concorrente)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.site_portal)
                .IsRequired();

            this.Property(t => t.site)
                .IsRequired();

            this.Property(t => t.video)
                .HasMaxLength(200);

            this.Property(t => t.email)
                .HasMaxLength(200);

            this.Property(t => t.logo)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("B_SERVICO_PARCEIRO_MAPEADO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.rua).HasColumnName("rua");
            this.Property(t => t.localidade).HasColumnName("localidade");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.plano_atual).HasColumnName("plano_atual");
            this.Property(t => t.mapeado_em).HasColumnName("mapeado_em");
            this.Property(t => t.atualizado_em).HasColumnName("atualizado_em");
            this.Property(t => t.de_concorrente).HasColumnName("de_concorrente");
            this.Property(t => t.site_portal).HasColumnName("site_portal");
            this.Property(t => t.site).HasColumnName("site");
            this.Property(t => t.video).HasColumnName("video");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.logo).HasColumnName("logo");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
        }
    }
}
