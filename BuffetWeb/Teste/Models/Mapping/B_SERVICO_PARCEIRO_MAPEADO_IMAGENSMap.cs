using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class B_SERVICO_PARCEIRO_MAPEADO_IMAGENSMap : EntityTypeConfiguration<B_SERVICO_PARCEIRO_MAPEADO_IMAGENS>
    {
        public B_SERVICO_PARCEIRO_MAPEADO_IMAGENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("B_SERVICO_PARCEIRO_MAPEADO_IMAGENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_parceiro_mapeado).HasColumnName("id_parceiro_mapeado");

            // Relationships
            this.HasRequired(t => t.B_SERVICO_PARCEIRO_MAPEADO)
                .WithMany(t => t.B_SERVICO_PARCEIRO_MAPEADO_IMAGENS)
                .HasForeignKey(d => d.id_parceiro_mapeado);

        }
    }
}
