using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPANHAMap : EntityTypeConfiguration<CAMPANHA>
    {
        public CAMPANHAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.assunto)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CAMPANHA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.assunto).HasColumnName("assunto");
            this.Property(t => t.data_inclusao).HasColumnName("data_inclusao");
            this.Property(t => t.id_lista).HasColumnName("id_lista");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.CAMPANHA_LISTA)
                .WithMany(t => t.CAMPANHAs)
                .HasForeignKey(d => d.id_lista);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CAMPANHAs)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
