using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPANHA_CONTATOS_ENVIADOSMap : EntityTypeConfiguration<CAMPANHA_CONTATOS_ENVIADOS>
    {
        public CAMPANHA_CONTATOS_ENVIADOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.mensagem)
                .IsRequired();

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.endereco_ip)
                .HasMaxLength(100);

            this.Property(t => t.hostname)
                .HasMaxLength(100);

            this.Property(t => t.estado)
                .HasMaxLength(100);

            this.Property(t => t.longitude)
                .HasMaxLength(100);

            this.Property(t => t.latitude)
                .HasMaxLength(100);

            this.Property(t => t.provedor)
                .HasMaxLength(100);

            this.Property(t => t.cidade)
                .HasMaxLength(100);

            this.Property(t => t.navegador_nome)
                .HasMaxLength(100);

            this.Property(t => t.navegador_versao)
                .HasMaxLength(100);

            this.Property(t => t.sistema_operacional)
                .HasMaxLength(100);

            this.Property(t => t.tipo_contato)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CAMPANHA_CONTATOS_ENVIADOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.mensagem).HasColumnName("mensagem");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.id_cliente).HasColumnName("id_cliente");
            this.Property(t => t.data_envio).HasColumnName("data_envio");
            this.Property(t => t.data_lido).HasColumnName("data_lido");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.lido_email).HasColumnName("lido_email");
            this.Property(t => t.lido_sms).HasColumnName("lido_sms");
            this.Property(t => t.endereco_ip).HasColumnName("endereco_ip");
            this.Property(t => t.hostname).HasColumnName("hostname");
            this.Property(t => t.estado).HasColumnName("estado");
            this.Property(t => t.longitude).HasColumnName("longitude");
            this.Property(t => t.latitude).HasColumnName("latitude");
            this.Property(t => t.provedor).HasColumnName("provedor");
            this.Property(t => t.cidade).HasColumnName("cidade");
            this.Property(t => t.navegador_nome).HasColumnName("navegador_nome");
            this.Property(t => t.navegador_versao).HasColumnName("navegador_versao");
            this.Property(t => t.sistema_operacional).HasColumnName("sistema_operacional");
            this.Property(t => t.lido_com_device).HasColumnName("lido_com_device");
            this.Property(t => t.enviado_email).HasColumnName("enviado_email");
            this.Property(t => t.enviado_sms).HasColumnName("enviado_sms");
            this.Property(t => t.tipo_contato).HasColumnName("tipo_contato");
            this.Property(t => t.quantidade_cliques).HasColumnName("quantidade_cliques");
            this.Property(t => t.enviado_facebook).HasColumnName("enviado_facebook");
            this.Property(t => t.lido_facebook).HasColumnName("lido_facebook");

            // Relationships
            this.HasOptional(t => t.CLIENTE)
                .WithMany(t => t.CAMPANHA_CONTATOS_ENVIADOS)
                .HasForeignKey(d => d.id_cliente);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CAMPANHA_CONTATOS_ENVIADOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
