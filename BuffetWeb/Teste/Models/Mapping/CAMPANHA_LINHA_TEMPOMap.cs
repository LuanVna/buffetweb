using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPANHA_LINHA_TEMPOMap : EntityTypeConfiguration<CAMPANHA_LINHA_TEMPO>
    {
        public CAMPANHA_LINHA_TEMPOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.acontecimento)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CAMPANHA_LINHA_TEMPO");
            this.Property(t => t.acontecimento).HasColumnName("acontecimento");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.id_campanha_contatos_enviados).HasColumnName("id_campanha_contatos_enviados");
            this.Property(t => t.id).HasColumnName("id");

            // Relationships
            this.HasRequired(t => t.CAMPANHA_CONTATOS_ENVIADOS)
                .WithMany(t => t.CAMPANHA_LINHA_TEMPO)
                .HasForeignKey(d => d.id_campanha_contatos_enviados);

        }
    }
}
