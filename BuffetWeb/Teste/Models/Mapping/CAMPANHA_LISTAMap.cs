using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPANHA_LISTAMap : EntityTypeConfiguration<CAMPANHA_LISTA>
    {
        public CAMPANHA_LISTAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.sexo)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CAMPANHA_LISTA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.data_inclusao).HasColumnName("data_inclusao");
            this.Property(t => t.regras_idade_minima).HasColumnName("regras_idade_minima");
            this.Property(t => t.regras_idade_maxima).HasColumnName("regras_idade_maxima");
            this.Property(t => t.regras_reigao).HasColumnName("regras_reigao");
            this.Property(t => t.com_filhos).HasColumnName("com_filhos");
            this.Property(t => t.sexo).HasColumnName("sexo");
            this.Property(t => t.com_orcamentos).HasColumnName("com_orcamentos");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.confirmados).HasColumnName("confirmados");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CAMPANHA_LISTA)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
