using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPOS_COMPONENTESMap : EntityTypeConfiguration<CAMPOS_COMPONENTES>
    {
        public CAMPOS_COMPONENTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.label)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.id_component)
                .HasMaxLength(200);

            this.Property(t => t.type)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.regex)
                .HasMaxLength(200);

            this.Property(t => t.css)
                .HasMaxLength(200);

            this.Property(t => t.function)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CAMPOS_COMPONENTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_campo_descricao).HasColumnName("id_campo_descricao");
            this.Property(t => t.label).HasColumnName("label");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.id_component).HasColumnName("id_component");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.id_values_select).HasColumnName("id_values_select");
            this.Property(t => t.required).HasColumnName("required");
            this.Property(t => t.regex).HasColumnName("regex");
            this.Property(t => t.max_length).HasColumnName("max-length");
            this.Property(t => t.css).HasColumnName("css");
            this.Property(t => t.function).HasColumnName("function");
            this.Property(t => t.ordem).HasColumnName("ordem");

            // Relationships
            this.HasRequired(t => t.CAMPOS_DESCRICAO)
                .WithMany(t => t.CAMPOS_COMPONENTES)
                .HasForeignKey(d => d.id_campo_descricao);

        }
    }
}
