using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CAMPOS_DISPONIVEISMap : EntityTypeConfiguration<CAMPOS_DISPONIVEIS>
    {
        public CAMPOS_DISPONIVEISMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CAMPOS_DISPONIVEIS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_tipo_evento).HasColumnName("id_tipo_evento");
            this.Property(t => t.id_campos_descricao).HasColumnName("id_campos_descricao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.CAMPOS_DESCRICAO)
                .WithMany(t => t.CAMPOS_DISPONIVEIS)
                .HasForeignKey(d => d.id_campos_descricao);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CAMPOS_DISPONIVEIS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.CAMPOS_DISPONIVEIS)
                .HasForeignKey(d => d.id_tipo_evento);

        }
    }
}
