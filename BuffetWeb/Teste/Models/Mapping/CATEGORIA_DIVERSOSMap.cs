using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CATEGORIA_DIVERSOSMap : EntityTypeConfiguration<CATEGORIA_DIVERSOS>
    {
        public CATEGORIA_DIVERSOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            this.Property(t => t.url_image)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("CATEGORIA_DIVERSOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.url_image).HasColumnName("url_image");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CATEGORIA_DIVERSOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
