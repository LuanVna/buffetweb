using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CLIENTEMap : EntityTypeConfiguration<CLIENTE>
    {
        public CLIENTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .HasMaxLength(200);

            this.Property(t => t.celular)
                .HasMaxLength(200);

            this.Property(t => t.email)
                .HasMaxLength(200);

            this.Property(t => t.cpf)
                .HasMaxLength(20);

            this.Property(t => t.rg)
                .HasMaxLength(20);

            this.Property(t => t.cliente_de)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.operadora_celular)
                .HasMaxLength(10);

            this.Property(t => t.onde_conheceu)
                .HasMaxLength(100);

            this.Property(t => t.regiao)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("CLIENTE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.celular).HasColumnName("celular");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.cpf).HasColumnName("cpf");
            this.Property(t => t.rg).HasColumnName("rg");
            this.Property(t => t.id_endereco).HasColumnName("id_endereco");
            this.Property(t => t.cliente_de).HasColumnName("cliente_de");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.operadora_celular).HasColumnName("operadora_celular");
            this.Property(t => t.onde_conheceu).HasColumnName("onde_conheceu");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.email_marketing).HasColumnName("email_marketing");
            this.Property(t => t.regiao).HasColumnName("regiao");

            // Relationships
            this.HasOptional(t => t.ENDERECO)
                .WithMany(t => t.CLIENTEs)
                .HasForeignKey(d => d.id_endereco);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CLIENTEs)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
