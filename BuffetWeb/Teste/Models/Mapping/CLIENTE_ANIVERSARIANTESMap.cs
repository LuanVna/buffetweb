using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CLIENTE_ANIVERSARIANTESMap : EntityTypeConfiguration<CLIENTE_ANIVERSARIANTES>
    {
        public CLIENTE_ANIVERSARIANTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.parentesco)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CLIENTE_ANIVERSARIANTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.nascimento).HasColumnName("nascimento");
            this.Property(t => t.parentesco).HasColumnName("parentesco");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.id_cliente).HasColumnName("id_cliente");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.CLIENTE)
                .WithMany(t => t.CLIENTE_ANIVERSARIANTES)
                .HasForeignKey(d => d.id_cliente);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CLIENTE_ANIVERSARIANTES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
