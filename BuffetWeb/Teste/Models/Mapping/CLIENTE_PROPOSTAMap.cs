using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CLIENTE_PROPOSTAMap : EntityTypeConfiguration<CLIENTE_PROPOSTA>
    {
        public CLIENTE_PROPOSTAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.proposta_de)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.periodo)
                .HasMaxLength(40);

            this.Property(t => t.autenticacao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("CLIENTE_PROPOSTA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_cliente).HasColumnName("id_cliente");
            this.Property(t => t.data_envio).HasColumnName("data_envio");
            this.Property(t => t.proposta_de).HasColumnName("proposta_de");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.convidados).HasColumnName("convidados");
            this.Property(t => t.data_evento).HasColumnName("data_evento");
            this.Property(t => t.periodo).HasColumnName("periodo");
            this.Property(t => t.id_proposta).HasColumnName("id_proposta");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.disponivel).HasColumnName("disponivel");
            this.Property(t => t.disponivel_ate).HasColumnName("disponivel_ate");

            // Relationships
            this.HasOptional(t => t.CLIENTE)
                .WithMany(t => t.CLIENTE_PROPOSTA)
                .HasForeignKey(d => d.id_cliente);
            this.HasOptional(t => t.CLIENTE1)
                .WithMany(t => t.CLIENTE_PROPOSTA1)
                .HasForeignKey(d => d.id_cliente);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CLIENTE_PROPOSTA)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PROPOSTA)
                .WithMany(t => t.CLIENTE_PROPOSTA)
                .HasForeignKey(d => d.id_proposta);

        }
    }
}
