using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CLIENTE_TIMELINEMap : EntityTypeConfiguration<CLIENTE_TIMELINE>
    {
        public CLIENTE_TIMELINEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(400);

            this.Property(t => t.titulo)
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            this.Property(t => t.icone)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CLIENTE_TIMELINE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.icone).HasColumnName("icone");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CLIENTE_TIMELINE)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
