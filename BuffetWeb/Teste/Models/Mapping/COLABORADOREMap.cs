using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class COLABORADOREMap : EntityTypeConfiguration<COLABORADORE>
    {
        public COLABORADOREMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .HasMaxLength(15);

            this.Property(t => t.email)
                .IsRequired();

            this.Property(t => t.senha)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.sessao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.celular)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("COLABORADORES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.sessao).HasColumnName("sessao");
            this.Property(t => t.id_perfil).HasColumnName("id_perfil");
            this.Property(t => t.mostrar_dica).HasColumnName("mostrar_dica");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_notificacao).HasColumnName("id_notificacao");
            this.Property(t => t.administrador).HasColumnName("administrador");
            this.Property(t => t.celular).HasColumnName("celular");

            // Relationships
            this.HasOptional(t => t.COLABORADOR_NOTIFICACAO)
                .WithMany(t => t.COLABORADORES)
                .HasForeignKey(d => d.id_notificacao);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.COLABORADORES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.PERFIL_COLABORADOR)
                .WithMany(t => t.COLABORADORES)
                .HasForeignKey(d => d.id_perfil);

        }
    }
}
