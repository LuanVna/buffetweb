using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class COLABORADOR_DEVICESMap : EntityTypeConfiguration<COLABORADOR_DEVICES>
    {
        public COLABORADOR_DEVICESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.device)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("COLABORADOR_DEVICES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.id_colaborador).HasColumnName("id_colaborador");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.device).HasColumnName("device");

            // Relationships
            this.HasRequired(t => t.COLABORADORE)
                .WithMany(t => t.COLABORADOR_DEVICES)
                .HasForeignKey(d => d.id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.COLABORADOR_DEVICES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
