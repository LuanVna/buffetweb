using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class COLABORADOR_NOTIFICACAOMap : EntityTypeConfiguration<COLABORADOR_NOTIFICACAO>
    {
        public COLABORADOR_NOTIFICACAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("COLABORADOR_NOTIFICACAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.orcamento_novo_pedido).HasColumnName("orcamento_novo_pedido");
            this.Property(t => t.orcamento_gendar_visita).HasColumnName("orcamento_gendar_visita");
            this.Property(t => t.orcamento_fecha_pedido).HasColumnName("orcamento_fecha_pedido");
            this.Property(t => t.convite).HasColumnName("convite");
            this.Property(t => t.financeiro_pagamento_efetuado).HasColumnName("financeiro_pagamento_efetuado");
            this.Property(t => t.estoque).HasColumnName("estoque");
            this.Property(t => t.notificacao_email).HasColumnName("notificacao_email");
            this.Property(t => t.pesquisa_concluida).HasColumnName("pesquisa_concluida");
        }
    }
}
