using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONTATO_PLATAFORMAMap : EntityTypeConfiguration<CONTATO_PLATAFORMA>
    {
        public CONTATO_PLATAFORMAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired();

            this.Property(t => t.mensagem_envio)
                .IsRequired();

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.tipo_contato)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("CONTATO_PLATAFORMA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.mensagem_envio).HasColumnName("mensagem_envio");
            this.Property(t => t.data_envio).HasColumnName("data_envio");
            this.Property(t => t.mensagem_resposta).HasColumnName("mensagem_resposta");
            this.Property(t => t.data_resposta).HasColumnName("data_resposta");
            this.Property(t => t.id_colaborador).HasColumnName("id_colaborador");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.tipo_contato).HasColumnName("tipo_contato");

            // Relationships
            this.HasRequired(t => t.COLABORADORE)
                .WithMany(t => t.CONTATO_PLATAFORMA)
                .HasForeignKey(d => d.id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONTATO_PLATAFORMA)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
