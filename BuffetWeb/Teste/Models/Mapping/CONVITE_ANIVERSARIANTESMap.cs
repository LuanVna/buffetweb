using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_ANIVERSARIANTESMap : EntityTypeConfiguration<CONVITE_ANIVERSARIANTES>
    {
        public CONVITE_ANIVERSARIANTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CONVITE_ANIVERSARIANTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_aniversariante).HasColumnName("id_aniversariante");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.CLIENTE_ANIVERSARIANTES)
                .WithMany(t => t.CONVITE_ANIVERSARIANTES)
                .HasForeignKey(d => d.id_aniversariante);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_ANIVERSARIANTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.CONVITE_ANIVERSARIANTES)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
