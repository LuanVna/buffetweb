using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_CONVIDADOSMap : EntityTypeConfiguration<CONVITE_CONVIDADOS>
    {
        public CONVITE_CONVIDADOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome_completo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .HasMaxLength(200);

            this.Property(t => t.ira)
                .HasMaxLength(200);

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.telefone)
                .HasMaxLength(20);

            this.Property(t => t.operadora)
                .HasMaxLength(50);

            this.Property(t => t.resposta)
                .HasMaxLength(200);

            this.Property(t => t.nome_facebook)
                .HasMaxLength(200);

            this.Property(t => t.status_pequisa)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CONVITE_CONVIDADOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.nome_completo).HasColumnName("nome_completo");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.ira).HasColumnName("ira");
            this.Property(t => t.mensagem).HasColumnName("mensagem");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.compareceu).HasColumnName("compareceu");
            this.Property(t => t.operadora).HasColumnName("operadora");
            this.Property(t => t.sms_enviado).HasColumnName("sms_enviado");
            this.Property(t => t.email_enviado).HasColumnName("email_enviado");
            this.Property(t => t.convidados).HasColumnName("convidados");
            this.Property(t => t.facebook).HasColumnName("facebook");
            this.Property(t => t.resposta).HasColumnName("resposta");
            this.Property(t => t.id_facebook).HasColumnName("id_facebook");
            this.Property(t => t.facebook_enviado).HasColumnName("facebook_enviado");
            this.Property(t => t.enviado).HasColumnName("enviado");
            this.Property(t => t.nome_facebook).HasColumnName("nome_facebook");
            this.Property(t => t.status_pequisa).HasColumnName("status_pequisa");

            // Relationships
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.CONVITE_CONVIDADOS)
                .HasForeignKey(d => d.id_orcamento);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_CONVIDADOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
