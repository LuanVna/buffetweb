using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_CONVIDADOS_FACEBOOKMap : EntityTypeConfiguration<CONVITE_CONVIDADOS_FACEBOOK>
    {
        public CONVITE_CONVIDADOS_FACEBOOKMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id_facebook)
                .IsRequired();

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.first_name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.last_name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.gender)
                .HasMaxLength(10);

            this.Property(t => t.locale)
                .HasMaxLength(200);

            this.Property(t => t.amigo_de_nome)
                .HasMaxLength(200);

            this.Property(t => t.amigo_de_id_facebook)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CONVITE_CONVIDADOS_FACEBOOK");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.id_facebook).HasColumnName("id_facebook");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.first_name).HasColumnName("first_name");
            this.Property(t => t.last_name).HasColumnName("last_name");
            this.Property(t => t.gender).HasColumnName("gender");
            this.Property(t => t.link).HasColumnName("link");
            this.Property(t => t.locale).HasColumnName("locale");
            this.Property(t => t.amigo_de_nome).HasColumnName("amigo_de_nome");
            this.Property(t => t.amigo_de_id_facebook).HasColumnName("amigo_de_id_facebook");
            this.Property(t => t.ativo).HasColumnName("ativo");

            // Relationships
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.CONVITE_CONVIDADOS_FACEBOOK)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
