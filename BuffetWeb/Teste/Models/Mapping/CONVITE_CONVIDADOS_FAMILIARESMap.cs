using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_CONVIDADOS_FAMILIARESMap : EntityTypeConfiguration<CONVITE_CONVIDADOS_FAMILIARES>
    {
        public CONVITE_CONVIDADOS_FAMILIARESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.tipo_convidado)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .HasMaxLength(300);

            this.Property(t => t.telefone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("CONVITE_CONVIDADOS_FAMILIARES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_convidado).HasColumnName("id_convidado");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.tipo_convidado).HasColumnName("tipo_convidado");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.telefone).HasColumnName("telefone");

            // Relationships
            this.HasRequired(t => t.CONVITE_CONVIDADOS)
                .WithMany(t => t.CONVITE_CONVIDADOS_FAMILIARES)
                .HasForeignKey(d => d.id_convidado);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_CONVIDADOS_FAMILIARES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.CONVITE_CONVIDADOS_FAMILIARES)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
