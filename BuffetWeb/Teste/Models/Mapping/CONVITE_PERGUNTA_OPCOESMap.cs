using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_PERGUNTA_OPCOESMap : EntityTypeConfiguration<CONVITE_PERGUNTA_OPCOES>
    {
        public CONVITE_PERGUNTA_OPCOESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.pergunta)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CONVITE_PERGUNTA_OPCOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.pergunta).HasColumnName("pergunta");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_PERGUNTA_OPCOES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
