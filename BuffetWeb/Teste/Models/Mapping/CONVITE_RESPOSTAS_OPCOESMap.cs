using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_RESPOSTAS_OPCOESMap : EntityTypeConfiguration<CONVITE_RESPOSTAS_OPCOES>
    {
        public CONVITE_RESPOSTAS_OPCOESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("CONVITE_RESPOSTAS_OPCOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_opcao).HasColumnName("id_opcao");
            this.Property(t => t.id_pergunta_opcao).HasColumnName("id_pergunta_opcao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.C_CONVITE_OPCOES)
                .WithMany(t => t.CONVITE_RESPOSTAS_OPCOES)
                .HasForeignKey(d => d.id_opcao);
            this.HasRequired(t => t.CONVITE_PERGUNTA_OPCOES)
                .WithMany(t => t.CONVITE_RESPOSTAS_OPCOES)
                .HasForeignKey(d => d.id_pergunta_opcao);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_RESPOSTAS_OPCOES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
