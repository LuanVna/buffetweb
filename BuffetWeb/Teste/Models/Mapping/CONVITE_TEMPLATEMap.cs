using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class CONVITE_TEMPLATEMap : EntityTypeConfiguration<CONVITE_TEMPLATE>
    {
        public CONVITE_TEMPLATEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo_convite)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.titulo_tamanho)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.titulo_top)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.titulo_left)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.titulo_descricao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao_top)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao_left)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.tamanho_descricao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.aniversariante_tamanho)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_top)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.aniversariantes_left)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.datahora_top)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.datahora_left)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.datahora_tamanho)
                .HasMaxLength(200);

            this.Property(t => t.url_image)
                .IsRequired();

            this.Property(t => t.titulo_datahora)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CONVITE_TEMPLATE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.titulo_convite).HasColumnName("titulo_convite");
            this.Property(t => t.titulo_tamanho).HasColumnName("titulo_tamanho");
            this.Property(t => t.titulo_top).HasColumnName("titulo_top");
            this.Property(t => t.titulo_left).HasColumnName("titulo_left");
            this.Property(t => t.titulo_descricao).HasColumnName("titulo_descricao");
            this.Property(t => t.descricao_top).HasColumnName("descricao_top");
            this.Property(t => t.descricao_left).HasColumnName("descricao_left");
            this.Property(t => t.tamanho_descricao).HasColumnName("tamanho_descricao");
            this.Property(t => t.aniversariante_tamanho).HasColumnName("aniversariante_tamanho");
            this.Property(t => t.aniversariantes_top).HasColumnName("aniversariantes_top");
            this.Property(t => t.aniversariantes_left).HasColumnName("aniversariantes_left");
            this.Property(t => t.datahora_top).HasColumnName("datahora_top");
            this.Property(t => t.datahora_left).HasColumnName("datahora_left");
            this.Property(t => t.datahora_tamanho).HasColumnName("datahora_tamanho");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.url_image).HasColumnName("url_image");
            this.Property(t => t.id_convite).HasColumnName("id_convite");
            this.Property(t => t.titulo_datahora).HasColumnName("titulo_datahora");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.CONVITE_TEMPLATE)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.CONVITE_TEMPLATE)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
