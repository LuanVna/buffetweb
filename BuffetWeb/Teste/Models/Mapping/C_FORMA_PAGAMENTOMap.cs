using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class C_FORMA_PAGAMENTOMap : EntityTypeConfiguration<C_FORMA_PAGAMENTO>
    {
        public C_FORMA_PAGAMENTOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.identitifcador)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("C_FORMA_PAGAMENTO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.identitifcador).HasColumnName("identitifcador");
        }
    }
}
