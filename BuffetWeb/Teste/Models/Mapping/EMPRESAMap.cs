using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESAMap : EntityTypeConfiguration<EMPRESA>
    {
        public EMPRESAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.codigo_empresa)
                .IsRequired();

            this.Property(t => t.telefone)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.celular)
                .HasMaxLength(20);

            this.Property(t => t.cnpj)
                .HasMaxLength(100);

            this.Property(t => t.razao_social)
                .HasMaxLength(200);

            this.Property(t => t.codigo_atividade_economica)
                .HasMaxLength(200);

            this.Property(t => t.codigo_atividade_economica_descricao)
                .HasMaxLength(200);

            this.Property(t => t.codigo_natureza_juridica)
                .HasMaxLength(200);

            this.Property(t => t.codigo_natureza_juridica_descricao)
                .HasMaxLength(200);

            this.Property(t => t.documento)
                .HasMaxLength(200);

            this.Property(t => t.motivo_especial_situacaoRFB)
                .HasMaxLength(200);

            this.Property(t => t.situacao_RFB)
                .HasMaxLength(200);

            this.Property(t => t.status)
                .HasMaxLength(200);

            this.Property(t => t.motivo_situacaoRFB)
                .HasMaxLength(200);

            this.Property(t => t.contato)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EMPRESA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.codigo_empresa).HasColumnName("codigo_empresa");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.celular).HasColumnName("celular");
            this.Property(t => t.cnpj).HasColumnName("cnpj");
            this.Property(t => t.razao_social).HasColumnName("razao_social");
            this.Property(t => t.data_da_situacao).HasColumnName("data_da_situacao");
            this.Property(t => t.data_da_situacao_especial).HasColumnName("data_da_situacao_especial");
            this.Property(t => t.id_endereco).HasColumnName("id_endereco");
            this.Property(t => t.codigo_atividade_economica).HasColumnName("codigo_atividade_economica");
            this.Property(t => t.codigo_atividade_economica_descricao).HasColumnName("codigo_atividade_economica_descricao");
            this.Property(t => t.codigo_natureza_juridica).HasColumnName("codigo_natureza_juridica");
            this.Property(t => t.codigo_natureza_juridica_descricao).HasColumnName("codigo_natureza_juridica_descricao");
            this.Property(t => t.data_consultaRFB).HasColumnName("data_consultaRFB");
            this.Property(t => t.data_fundacao).HasColumnName("data_fundacao");
            this.Property(t => t.data_motivo_especial_situacaoRFB).HasColumnName("data_motivo_especial_situacaoRFB");
            this.Property(t => t.data_situacao_RFB).HasColumnName("data_situacao_RFB");
            this.Property(t => t.documento).HasColumnName("documento");
            this.Property(t => t.motivo_especial_situacaoRFB).HasColumnName("motivo_especial_situacaoRFB");
            this.Property(t => t.situacao_RFB).HasColumnName("situacao_RFB");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.bloqueio_cnae).HasColumnName("bloqueio_cnae");
            this.Property(t => t.motivo_situacaoRFB).HasColumnName("motivo_situacaoRFB");
            this.Property(t => t.contato).HasColumnName("contato");
            this.Property(t => t.aguardando).HasColumnName("aguardando");

            // Relationships
            this.HasOptional(t => t.ENDERECO)
                .WithMany(t => t.EMPRESAs)
                .HasForeignKey(d => d.id_endereco);

        }
    }
}
