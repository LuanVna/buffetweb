using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_ARQUIVOSMap : EntityTypeConfiguration<EMPRESA_ARQUIVOS>
    {
        public EMPRESA_ARQUIVOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.chave)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.valor)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("EMPRESA_ARQUIVOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.chave).HasColumnName("chave");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_ARQUIVOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
