using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_CARRINHOMap : EntityTypeConfiguration<EMPRESA_CARRINHO>
    {
        public EMPRESA_CARRINHOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.periodo)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EMPRESA_CARRINHO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.adicionado_em).HasColumnName("adicionado_em");
            this.Property(t => t.periodo).HasColumnName("periodo");
            this.Property(t => t.id_plano_disponivel).HasColumnName("id_plano_disponivel");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.comprado).HasColumnName("comprado");

            // Relationships
            this.HasRequired(t => t.B_PLANOS_DISPONIVEIS)
                .WithMany(t => t.EMPRESA_CARRINHO)
                .HasForeignKey(d => d.id_plano_disponivel);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_CARRINHO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.EMPRESA_CARRINHO)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
