using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_CARRINHO_CUPOMMap : EntityTypeConfiguration<EMPRESA_CARRINHO_CUPOM>
    {
        public EMPRESA_CARRINHO_CUPOMMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.descricao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.periodo)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EMPRESA_CARRINHO_CUPOM");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.valor_desconto).HasColumnName("valor_desconto");
            this.Property(t => t.disponivel_ate).HasColumnName("disponivel_ate");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.usado).HasColumnName("usado");
            this.Property(t => t.id_planos_disponiveis).HasColumnName("id_planos_disponiveis");
            this.Property(t => t.periodo).HasColumnName("periodo");

            // Relationships
            this.HasRequired(t => t.B_PLANOS_DISPONIVEIS)
                .WithMany(t => t.EMPRESA_CARRINHO_CUPOM)
                .HasForeignKey(d => d.id_planos_disponiveis);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_CARRINHO_CUPOM)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.EMPRESA_CARRINHO_CUPOM)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
