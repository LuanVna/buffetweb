using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_CNAESMap : EntityTypeConfiguration<EMPRESA_CNAES>
    {
        public EMPRESA_CNAESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.cnae)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.cnae_descricao)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EMPRESA_CNAES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.cnae).HasColumnName("cnae");
            this.Property(t => t.cnae_descricao).HasColumnName("cnae_descricao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_CNAES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
