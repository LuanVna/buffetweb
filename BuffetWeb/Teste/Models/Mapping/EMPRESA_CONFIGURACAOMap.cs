using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_CONFIGURACAOMap : EntityTypeConfiguration<EMPRESA_CONFIGURACAO>
    {
        public EMPRESA_CONFIGURACAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.email_porta)
                .HasMaxLength(500);

            this.Property(t => t.email_login)
                .HasMaxLength(500);

            this.Property(t => t.email_host)
                .HasMaxLength(500);

            this.Property(t => t.email_senha)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("EMPRESA_CONFIGURACAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.email_porta).HasColumnName("email_porta");
            this.Property(t => t.email_login).HasColumnName("email_login");
            this.Property(t => t.email_host).HasColumnName("email_host");
            this.Property(t => t.email_senha).HasColumnName("email_senha");
            this.Property(t => t.permitirSMS).HasColumnName("permitirSMS");
            this.Property(t => t.pesquisa_enviar_automaticamente).HasColumnName("pesquisa_enviar_automaticamente");
            this.Property(t => t.pesquisa_dias_apos_evento).HasColumnName("pesquisa_dias_apos_evento");
            this.Property(t => t.quantidade_usuarios).HasColumnName("quantidade_usuarios");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_CONFIGURACAO)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
