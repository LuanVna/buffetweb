using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_HISTORICO_PAGAMENTOMap : EntityTypeConfiguration<EMPRESA_HISTORICO_PAGAMENTO>
    {
        public EMPRESA_HISTORICO_PAGAMENTOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.produto)
                .HasMaxLength(200);

            this.Property(t => t.cartao)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.autenticacao_externa)
                .IsRequired();

            this.Property(t => t.autenticacao_interna)
                .IsRequired();

            this.Property(t => t.forma_pagamento)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.status)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("EMPRESA_HISTORICO_PAGAMENTO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.produto).HasColumnName("produto");
            this.Property(t => t.comprado_em).HasColumnName("comprado_em");
            this.Property(t => t.cartao).HasColumnName("cartao");
            this.Property(t => t.autenticacao_externa).HasColumnName("autenticacao_externa");
            this.Property(t => t.autenticacao_interna).HasColumnName("autenticacao_interna");
            this.Property(t => t.id_cupom_promocional).HasColumnName("id_cupom_promocional");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.forma_pagamento).HasColumnName("forma_pagamento");
            this.Property(t => t.status).HasColumnName("status");

            // Relationships
            this.HasOptional(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_HISTORICO_PAGAMENTO)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.EMPRESA_CARRINHO_CUPOM)
                .WithMany(t => t.EMPRESA_HISTORICO_PAGAMENTO)
                .HasForeignKey(d => d.id_cupom_promocional);

        }
    }
}
