using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_MODULOSMap : EntityTypeConfiguration<EMPRESA_MODULOS>
    {
        public EMPRESA_MODULOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.modulo)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("EMPRESA_MODULOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.modulo).HasColumnName("modulo");
            this.Property(t => t.adquirido_em).HasColumnName("adquirido_em");
            this.Property(t => t.expira_em).HasColumnName("expira_em");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_MODULOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
