using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_PACOTE_SMSMap : EntityTypeConfiguration<EMPRESA_PACOTE_SMS>
    {
        public EMPRESA_PACOTE_SMSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.autenticacao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("EMPRESA_PACOTE_SMS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.quantidade_pacote).HasColumnName("quantidade_pacote");
            this.Property(t => t.quantidade_disponivel).HasColumnName("quantidade_disponivel");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.data_compra).HasColumnName("data_compra");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_PACOTE_SMS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
