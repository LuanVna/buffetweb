using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EMPRESA_PAGAMENTO_CARTAOMap : EntityTypeConfiguration<EMPRESA_PAGAMENTO_CARTAO>
    {
        public EMPRESA_PAGAMENTO_CARTAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.bandeira)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.nome_titular)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.ultimos_digitos)
                .IsRequired()
                .HasMaxLength(4);

            this.Property(t => t.token_autorizacao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("EMPRESA_PAGAMENTO_CARTAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.bandeira).HasColumnName("bandeira");
            this.Property(t => t.nome_titular).HasColumnName("nome_titular");
            this.Property(t => t.ultimos_digitos).HasColumnName("ultimos_digitos");
            this.Property(t => t.token_autorizacao).HasColumnName("token_autorizacao");

            // Relationships
            this.HasOptional(t => t.EMPRESA)
                .WithMany(t => t.EMPRESA_PAGAMENTO_CARTAO)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
