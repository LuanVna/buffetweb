using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ENDERECOMap : EntityTypeConfiguration<ENDERECO>
    {
        public ENDERECOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.cep)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.endereco1)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.bairro)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.cidade)
                .HasMaxLength(200);

            this.Property(t => t.estado)
                .IsRequired()
                .HasMaxLength(2);

            this.Property(t => t.complemento)
                .HasMaxLength(200);

            this.Property(t => t.numero)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ENDERECO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.cep).HasColumnName("cep");
            this.Property(t => t.endereco1).HasColumnName("endereco");
            this.Property(t => t.bairro).HasColumnName("bairro");
            this.Property(t => t.cidade).HasColumnName("cidade");
            this.Property(t => t.estado).HasColumnName("estado");
            this.Property(t => t.complemento).HasColumnName("complemento");
            this.Property(t => t.numero).HasColumnName("numero");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.longitude).HasColumnName("longitude");
            this.Property(t => t.latitude).HasColumnName("latitude");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ENDERECOes)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
