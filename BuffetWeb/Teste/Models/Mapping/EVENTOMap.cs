using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTOMap : EntityTypeConfiguration<EVENTO>
    {
        public EVENTOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EVENTO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.evento_de).HasColumnName("evento_de");
            this.Property(t => t.evento_ate).HasColumnName("evento_ate");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.de_epoca).HasColumnName("de_epoca");
            this.Property(t => t.quantidade_disponivel).HasColumnName("quantidade_disponivel");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.dividir_valor_pessoa).HasColumnName("dividir_valor_pessoa");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.url).HasColumnName("url");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTOes)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
