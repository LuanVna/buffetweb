using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTOS_PACOTESMap : EntityTypeConfiguration<EVENTOS_PACOTES>
    {
        public EVENTOS_PACOTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("EVENTOS_PACOTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pacote).HasColumnName("id_pacote");
            this.Property(t => t.id_evento).HasColumnName("id_evento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTOS_PACOTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.EVENTOS_PACOTES)
                .HasForeignKey(d => d.id_evento);
            this.HasRequired(t => t.PACOTE)
                .WithMany(t => t.EVENTOS_PACOTES)
                .HasForeignKey(d => d.id_pacote);

        }
    }
}
