using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTO_ALUGUELMap : EntityTypeConfiguration<EVENTO_ALUGUEL>
    {
        public EVENTO_ALUGUELMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.id_itens_aluguel, t.id_empresa, t.id_tipo_evento, t.cobrar_brinde, t.brinde, t.quantidade });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.id_itens_aluguel)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_empresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_tipo_evento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.quantidade)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("EVENTO_ALUGUEL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_itens_aluguel).HasColumnName("id_itens_aluguel");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_tipo_evento).HasColumnName("id_tipo_evento");
            this.Property(t => t.cobrar_brinde).HasColumnName("cobrar_brinde");
            this.Property(t => t.brinde).HasColumnName("brinde");
            this.Property(t => t.quantidade).HasColumnName("quantidade");
            this.Property(t => t.mostrar_aluguel).HasColumnName("mostrar_aluguel");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTO_ALUGUEL)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.EVENTO_ALUGUEL)
                .HasForeignKey(d => d.id_tipo_evento);
            this.HasRequired(t => t.ITENS_ALUGUEL)
                .WithMany(t => t.EVENTO_ALUGUEL)
                .HasForeignKey(d => d.id_itens_aluguel);

        }
    }
}
