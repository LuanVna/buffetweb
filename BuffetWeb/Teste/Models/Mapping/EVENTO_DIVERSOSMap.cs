using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTO_DIVERSOSMap : EntityTypeConfiguration<EVENTO_DIVERSOS>
    {
        public EVENTO_DIVERSOSMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.id_itens_diversos, t.id_empresa, t.id_tipo_evento, t.cobrar_brinde, t.brinde, t.quantidade });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.id_itens_diversos)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_empresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_tipo_evento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.quantidade)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("EVENTO_DIVERSOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_itens_diversos).HasColumnName("id_itens_diversos");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_tipo_evento).HasColumnName("id_tipo_evento");
            this.Property(t => t.cobrar_brinde).HasColumnName("cobrar_brinde");
            this.Property(t => t.brinde).HasColumnName("brinde");
            this.Property(t => t.quantidade).HasColumnName("quantidade");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTO_DIVERSOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.EVENTO_DIVERSOS)
                .HasForeignKey(d => d.id_tipo_evento);
            this.HasRequired(t => t.ITENS_DIVERSOS)
                .WithMany(t => t.EVENTO_DIVERSOS)
                .HasForeignKey(d => d.id_itens_diversos);

        }
    }
}
