using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTO_PRESTADORESMap : EntityTypeConfiguration<EVENTO_PRESTADORES>
    {
        public EVENTO_PRESTADORESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.tipo)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("EVENTO_PRESTADORES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.quantidade).HasColumnName("quantidade");
            this.Property(t => t.id_evento).HasColumnName("id_evento");
            this.Property(t => t.tipo).HasColumnName("tipo");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTO_PRESTADORES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.EVENTO_PRESTADORES)
                .HasForeignKey(d => d.id_evento);

        }
    }
}
