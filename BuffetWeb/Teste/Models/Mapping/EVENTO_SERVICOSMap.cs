using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class EVENTO_SERVICOSMap : EntityTypeConfiguration<EVENTO_SERVICOS>
    {
        public EVENTO_SERVICOSMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.id_itens_servicos, t.id_empresa, t.id_tipo_evento, t.cobrar_brinde, t.brinde, t.quantidade, t.mostrar_servico });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.id_itens_servicos)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_empresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_tipo_evento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.quantidade)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("EVENTO_SERVICOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_itens_servicos).HasColumnName("id_itens_servicos");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_tipo_evento).HasColumnName("id_tipo_evento");
            this.Property(t => t.cobrar_brinde).HasColumnName("cobrar_brinde");
            this.Property(t => t.brinde).HasColumnName("brinde");
            this.Property(t => t.quantidade).HasColumnName("quantidade");
            this.Property(t => t.mostrar_servico).HasColumnName("mostrar_servico");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.EVENTO_SERVICOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.EVENTO_SERVICOS)
                .HasForeignKey(d => d.id_tipo_evento);
            this.HasRequired(t => t.ITENS_SERVICOS)
                .WithMany(t => t.EVENTO_SERVICOS)
                .HasForeignKey(d => d.id_itens_servicos);

        }
    }
}
