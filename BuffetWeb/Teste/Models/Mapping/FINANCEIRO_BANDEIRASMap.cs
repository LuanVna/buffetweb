using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FINANCEIRO_BANDEIRASMap : EntityTypeConfiguration<FINANCEIRO_BANDEIRAS>
    {
        public FINANCEIRO_BANDEIRASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.bandeira)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("FINANCEIRO_BANDEIRAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.bandeira).HasColumnName("bandeira");
            this.Property(t => t.ativo).HasColumnName("ativo");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FINANCEIRO_BANDEIRAS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
