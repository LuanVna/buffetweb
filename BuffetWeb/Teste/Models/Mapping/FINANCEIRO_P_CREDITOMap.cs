using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FINANCEIRO_P_CREDITOMap : EntityTypeConfiguration<FINANCEIRO_P_CREDITO>
    {
        public FINANCEIRO_P_CREDITOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FINANCEIRO_P_CREDITO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.n_parcelas).HasColumnName("n_parcelas");
            this.Property(t => t.n_autorizacao).HasColumnName("n_autorizacao");
            this.Property(t => t.id_bandeira).HasColumnName("id_bandeira");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.id_colaborador).HasColumnName("id_colaborador");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.COLABORADORE)
                .WithMany(t => t.FINANCEIRO_P_CREDITO)
                .HasForeignKey(d => d.id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FINANCEIRO_P_CREDITO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.FINANCEIRO_BANDEIRAS)
                .WithMany(t => t.FINANCEIRO_P_CREDITO)
                .HasForeignKey(d => d.id_bandeira);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.FINANCEIRO_P_CREDITO)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
