using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FINANCEIRO_P_DEPOSITOMap : EntityTypeConfiguration<FINANCEIRO_P_DEPOSITO>
    {
        public FINANCEIRO_P_DEPOSITOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.banco)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("FINANCEIRO_P_DEPOSITO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.banco).HasColumnName("banco");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.id_colaborador).HasColumnName("id_colaborador");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.COLABORADORE)
                .WithMany(t => t.FINANCEIRO_P_DEPOSITO)
                .HasForeignKey(d => d.id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FINANCEIRO_P_DEPOSITO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.FINANCEIRO_P_DEPOSITO)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
