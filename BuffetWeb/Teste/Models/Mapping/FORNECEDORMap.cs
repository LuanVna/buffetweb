using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FORNECEDORMap : EntityTypeConfiguration<FORNECEDOR>
    {
        public FORNECEDORMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.razao_social)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.cnpj)
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .HasMaxLength(15);

            this.Property(t => t.contato)
                .HasMaxLength(200);

            this.Property(t => t.telefone_contato)
                .HasMaxLength(15);

            this.Property(t => t.celular_contato)
                .HasMaxLength(15);

            this.Property(t => t.email)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("FORNECEDOR");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.razao_social).HasColumnName("razao_social");
            this.Property(t => t.cnpj).HasColumnName("cnpj");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.contato).HasColumnName("contato");
            this.Property(t => t.telefone_contato).HasColumnName("telefone_contato");
            this.Property(t => t.celular_contato).HasColumnName("celular_contato");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.id_endereco).HasColumnName("id_endereco");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FORNECEDORs)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.ENDERECO)
                .WithMany(t => t.FORNECEDORs)
                .HasForeignKey(d => d.id_endereco);

        }
    }
}
