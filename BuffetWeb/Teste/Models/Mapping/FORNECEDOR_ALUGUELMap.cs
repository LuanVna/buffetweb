using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FORNECEDOR_ALUGUELMap : EntityTypeConfiguration<FORNECEDOR_ALUGUEL>
    {
        public FORNECEDOR_ALUGUELMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FORNECEDOR_ALUGUEL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_fornecedor).HasColumnName("id_fornecedor");
            this.Property(t => t.id_aluguel).HasColumnName("id_aluguel");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FORNECEDOR_ALUGUEL)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ITENS_ALUGUEL)
                .WithMany(t => t.FORNECEDOR_ALUGUEL)
                .HasForeignKey(d => d.id_aluguel);

        }
    }
}
