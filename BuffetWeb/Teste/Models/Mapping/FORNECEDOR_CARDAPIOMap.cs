using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FORNECEDOR_CARDAPIOMap : EntityTypeConfiguration<FORNECEDOR_CARDAPIO>
    {
        public FORNECEDOR_CARDAPIOMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.id_fornecedor, t.id_cardapio, t.id_empresa });

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.id_fornecedor)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_cardapio)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.id_empresa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("FORNECEDOR_CARDAPIO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_fornecedor).HasColumnName("id_fornecedor");
            this.Property(t => t.id_cardapio).HasColumnName("id_cardapio");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FORNECEDOR_CARDAPIO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ITENS_CARDAPIO)
                .WithMany(t => t.FORNECEDOR_CARDAPIO)
                .HasForeignKey(d => d.id_cardapio);

        }
    }
}
