using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class FORNECEDOR_INGREDIENTEMap : EntityTypeConfiguration<FORNECEDOR_INGREDIENTE>
    {
        public FORNECEDOR_INGREDIENTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FORNECEDOR_INGREDIENTE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_ingrediente).HasColumnName("id_ingrediente");
            this.Property(t => t.id_fornecedor).HasColumnName("id_fornecedor");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.FORNECEDOR_INGREDIENTE)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.FORNECEDOR)
                .WithMany(t => t.FORNECEDOR_INGREDIENTE)
                .HasForeignKey(d => d.id_fornecedor);
            this.HasRequired(t => t.INGREDIENTE)
                .WithMany(t => t.FORNECEDOR_INGREDIENTE)
                .HasForeignKey(d => d.id_ingrediente);

        }
    }
}
