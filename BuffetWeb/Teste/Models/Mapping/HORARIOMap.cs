using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class HORARIOMap : EntityTypeConfiguration<HORARIO>
    {
        public HORARIOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HORARIOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.horario_de).HasColumnName("horario_de");
            this.Property(t => t.horario_ate).HasColumnName("horario_ate");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
        }
    }
}
