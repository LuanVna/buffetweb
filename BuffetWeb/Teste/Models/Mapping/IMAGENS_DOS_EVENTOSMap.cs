using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class IMAGENS_DOS_EVENTOSMap : EntityTypeConfiguration<IMAGENS_DOS_EVENTOS>
    {
        public IMAGENS_DOS_EVENTOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IMAGENS_DOS_EVENTOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_imagens_evento).HasColumnName("id_imagens_evento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.IMAGENS_DOS_EVENTOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.IMAGENS_GALERIA_EVENTOS)
                .WithMany(t => t.IMAGENS_DOS_EVENTOS)
                .HasForeignKey(d => d.id_imagens_evento);

        }
    }
}
