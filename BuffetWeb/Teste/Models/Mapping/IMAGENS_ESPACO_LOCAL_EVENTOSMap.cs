using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class IMAGENS_ESPACO_LOCAL_EVENTOSMap : EntityTypeConfiguration<IMAGENS_ESPACO_LOCAL_EVENTOS>
    {
        public IMAGENS_ESPACO_LOCAL_EVENTOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .HasMaxLength(200);

            this.Property(t => t.url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IMAGENS_ESPACO_LOCAL_EVENTOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.IMAGENS_ESPACO_LOCAL_EVENTOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.IMAGENS_ESPACO_LOCAL_EVENTOS)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
