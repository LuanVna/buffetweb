using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class IMAGENS_GALERIA_EVENTOSMap : EntityTypeConfiguration<IMAGENS_GALERIA_EVENTOS>
    {
        public IMAGENS_GALERIA_EVENTOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("IMAGENS_GALERIA_EVENTOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.descricao).HasColumnName("descricao");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.IMAGENS_GALERIA_EVENTOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.IMAGENS_GALERIA_EVENTOS)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
