using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class IMAGENS_ITEMMap : EntityTypeConfiguration<IMAGENS_ITEM>
    {
        public IMAGENS_ITEMMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .HasMaxLength(200);

            this.Property(t => t.url)
                .IsRequired();

            this.Property(t => t.pasta)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("IMAGENS_ITEM");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.pasta).HasColumnName("pasta");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.IMAGENS_ITEM)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
