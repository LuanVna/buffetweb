using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class IMAGENS_PACOTESMap : EntityTypeConfiguration<IMAGENS_PACOTES>
    {
        public IMAGENS_PACOTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .HasMaxLength(200);

            this.Property(t => t.url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IMAGENS_PACOTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pacote).HasColumnName("id_pacote");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.IMAGENS_PACOTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.PACOTE)
                .WithMany(t => t.IMAGENS_PACOTES)
                .HasForeignKey(d => d.id_pacote);

        }
    }
}
