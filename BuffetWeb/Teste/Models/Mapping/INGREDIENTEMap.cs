using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class INGREDIENTEMap : EntityTypeConfiguration<INGREDIENTE>
    {
        public INGREDIENTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.marca)
                .HasMaxLength(200);

            this.Property(t => t.quantidade_embalagem)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("INGREDIENTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.marca).HasColumnName("marca");
            this.Property(t => t.quantidade_embalagem).HasColumnName("quantidade_embalagem");
            this.Property(t => t.id_unidade_medida).HasColumnName("id_unidade_medida");
            this.Property(t => t.valor_custo).HasColumnName("valor_custo");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.INGREDIENTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.UNIDADE_MEDIDA)
                .WithMany(t => t.INGREDIENTES)
                .HasForeignKey(d => d.id_unidade_medida);

        }
    }
}
