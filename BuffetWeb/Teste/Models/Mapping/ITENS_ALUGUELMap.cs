using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ITENS_ALUGUELMap : EntityTypeConfiguration<ITENS_ALUGUEL>
    {
        public ITENS_ALUGUELMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.url_image)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ITENS_ALUGUEL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.valor_custo).HasColumnName("valor_custo");
            this.Property(t => t.valor_venda).HasColumnName("valor_venda");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_categoria).HasColumnName("id_categoria");
            this.Property(t => t.valor_reembolso).HasColumnName("valor_reembolso");
            this.Property(t => t.valor_atraso).HasColumnName("valor_atraso");
            this.Property(t => t.quantidade_disponivel).HasColumnName("quantidade_disponivel");
            this.Property(t => t.url_image).HasColumnName("url_image");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_ALUGUEL)
                .WithMany(t => t.ITENS_ALUGUEL)
                .HasForeignKey(d => d.id_categoria);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ITENS_ALUGUEL)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
