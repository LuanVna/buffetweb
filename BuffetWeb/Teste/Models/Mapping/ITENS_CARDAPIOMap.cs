using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ITENS_CARDAPIOMap : EntityTypeConfiguration<ITENS_CARDAPIO>
    {
        public ITENS_CARDAPIOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.url_image)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ITENS_CARDAPIO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.id_categoria).HasColumnName("id_categoria");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.valor_compra).HasColumnName("valor_compra");
            this.Property(t => t.id_unidade_medida).HasColumnName("id_unidade_medida");
            this.Property(t => t.rendimento).HasColumnName("rendimento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.receita).HasColumnName("receita");
            this.Property(t => t.id_sub_categoria).HasColumnName("id_sub_categoria");
            this.Property(t => t.quantidade_embalagem).HasColumnName("quantidade_embalagem");
            this.Property(t => t.custo_pessoa).HasColumnName("custo_pessoa");
            this.Property(t => t.margem_lucro).HasColumnName("margem_lucro");
            this.Property(t => t.venda_pessoa).HasColumnName("venda_pessoa");
            this.Property(t => t.url_image).HasColumnName("url_image");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_ITENS)
                .WithMany(t => t.ITENS_CARDAPIO)
                .HasForeignKey(d => d.id_categoria);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ITENS_CARDAPIO)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.SUB_CATEGORIA_ITENS)
                .WithMany(t => t.ITENS_CARDAPIO)
                .HasForeignKey(d => d.id_sub_categoria);
            this.HasOptional(t => t.UNIDADE_MEDIDA)
                .WithMany(t => t.ITENS_CARDAPIO)
                .HasForeignKey(d => d.id_unidade_medida);

        }
    }
}
