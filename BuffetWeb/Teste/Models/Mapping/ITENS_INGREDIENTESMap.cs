using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ITENS_INGREDIENTESMap : EntityTypeConfiguration<ITENS_INGREDIENTES>
    {
        public ITENS_INGREDIENTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ITENS_INGREDIENTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_cardapio).HasColumnName("id_cardapio");
            this.Property(t => t.id_ingrediente).HasColumnName("id_ingrediente");
            this.Property(t => t.valor_atual).HasColumnName("valor_atual");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.quantidade).HasColumnName("quantidade");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ITENS_INGREDIENTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.INGREDIENTE)
                .WithMany(t => t.ITENS_INGREDIENTES)
                .HasForeignKey(d => d.id_ingrediente);
            this.HasRequired(t => t.ITENS_CARDAPIO)
                .WithMany(t => t.ITENS_INGREDIENTES)
                .HasForeignKey(d => d.id_cardapio);

        }
    }
}
