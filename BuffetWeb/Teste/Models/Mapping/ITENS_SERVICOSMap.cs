using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ITENS_SERVICOSMap : EntityTypeConfiguration<ITENS_SERVICOS>
    {
        public ITENS_SERVICOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .HasMaxLength(200);

            this.Property(t => t.url_image)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ITENS_SERVICOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.valor_custo).HasColumnName("valor_custo");
            this.Property(t => t.valor_venda).HasColumnName("valor_venda");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_categoria).HasColumnName("id_categoria");
            this.Property(t => t.url_image).HasColumnName("url_image");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_SERVICOS)
                .WithMany(t => t.ITENS_SERVICOS)
                .HasForeignKey(d => d.id_categoria);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ITENS_SERVICOS)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
