using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class LOCAL_EVENTOMap : EntityTypeConfiguration<LOCAL_EVENTO>
    {
        public LOCAL_EVENTOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .HasMaxLength(15);

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.regiao)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.video)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("LOCAL_EVENTO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.id_endereco).HasColumnName("id_endereco");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.capacidade).HasColumnName("capacidade");
            this.Property(t => t.porEspaco).HasColumnName("porEspaco");
            this.Property(t => t.valorMinimo).HasColumnName("valorMinimo");
            this.Property(t => t.valor).HasColumnName("valor");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.regiao).HasColumnName("regiao");
            this.Property(t => t.idade_minima_pagante).HasColumnName("idade_minima_pagante");
            this.Property(t => t.horario_inicio_almoco).HasColumnName("horario_inicio_almoco");
            this.Property(t => t.horario_fim_almoco).HasColumnName("horario_fim_almoco");
            this.Property(t => t.horario_inicio_jantar).HasColumnName("horario_inicio_jantar");
            this.Property(t => t.horario_fim_jantar).HasColumnName("horario_fim_jantar");
            this.Property(t => t.palavras_chaves).HasColumnName("palavras_chaves");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.video).HasColumnName("video");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.LOCAL_EVENTO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ENDERECO)
                .WithMany(t => t.LOCAL_EVENTO)
                .HasForeignKey(d => d.id_endereco);

        }
    }
}
