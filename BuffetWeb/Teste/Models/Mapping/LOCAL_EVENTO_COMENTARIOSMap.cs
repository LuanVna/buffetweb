using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class LOCAL_EVENTO_COMENTARIOSMap : EntityTypeConfiguration<LOCAL_EVENTO_COMENTARIOS>
    {
        public LOCAL_EVENTO_COMENTARIOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.comentario)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("LOCAL_EVENTO_COMENTARIOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.comentario).HasColumnName("comentario");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.avaliacao).HasColumnName("avaliacao");
            this.Property(t => t.id_cliente_perfil).HasColumnName("id_cliente_perfil");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.analisado).HasColumnName("analisado");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.LOCAL_EVENTO_COMENTARIOS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.LOCAL_EVENTO_COMENTARIOS)
                .HasForeignKey(d => d.id_local_evento);
            this.HasRequired(t => t.PORTAL_CLIENTE_PERFIL)
                .WithMany(t => t.LOCAL_EVENTO_COMENTARIOS)
                .HasForeignKey(d => d.id_cliente_perfil);

        }
    }
}
