using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class LOCAL_EVENTO_IMAGENSMap : EntityTypeConfiguration<LOCAL_EVENTO_IMAGENS>
    {
        public LOCAL_EVENTO_IMAGENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("LOCAL_EVENTO_IMAGENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.sequencia).HasColumnName("sequencia");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.LOCAL_EVENTO_IMAGENS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.LOCAL_EVENTO_IMAGENS)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
