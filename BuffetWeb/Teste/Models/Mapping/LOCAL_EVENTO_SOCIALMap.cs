using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class LOCAL_EVENTO_SOCIALMap : EntityTypeConfiguration<LOCAL_EVENTO_SOCIAL>
    {
        public LOCAL_EVENTO_SOCIALMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.plataforma)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("LOCAL_EVENTO_SOCIAL");
            this.Property(t => t.plataforma).HasColumnName("plataforma");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.LOCAL_EVENTO_SOCIAL)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.LOCAL_EVENTO_SOCIAL)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
