using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class NOTIFICACOMap : EntityTypeConfiguration<NOTIFICACO>
    {
        public NOTIFICACOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.mensagem)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("NOTIFICACOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.mensagem).HasColumnName("mensagem");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.lido).HasColumnName("lido");
            this.Property(t => t.deBuffetWeb).HasColumnName("deBuffetWeb");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.para_id_colaborador).HasColumnName("para_id_colaborador");
            this.Property(t => t.de_id_colaborador).HasColumnName("de_id_colaborador");
            this.Property(t => t.paraBuffetWeb).HasColumnName("paraBuffetWeb");

            // Relationships
            this.HasOptional(t => t.COLABORADORE)
                .WithMany(t => t.NOTIFICACOES)
                .HasForeignKey(d => d.de_id_colaborador);
            this.HasOptional(t => t.COLABORADORE1)
                .WithMany(t => t.NOTIFICACOES1)
                .HasForeignKey(d => d.para_id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.NOTIFICACOES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
