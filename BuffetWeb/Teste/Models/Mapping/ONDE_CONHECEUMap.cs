using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ONDE_CONHECEUMap : EntityTypeConfiguration<ONDE_CONHECEU>
    {
        public ONDE_CONHECEUMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ONDE_CONHECEU");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ONDE_CONHECEU)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
