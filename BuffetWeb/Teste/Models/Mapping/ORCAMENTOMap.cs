using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTOMap : EntityTypeConfiguration<ORCAMENTO>
    {
        public ORCAMENTOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.codigo_unico)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .HasMaxLength(15);

            this.Property(t => t.email)
                .HasMaxLength(200);

            this.Property(t => t.status)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.senha)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email2)
                .HasMaxLength(200);

            this.Property(t => t.instituicao)
                .HasMaxLength(200);

            this.Property(t => t.nome2)
                .HasMaxLength(200);

            this.Property(t => t.razao)
                .HasMaxLength(200);

            this.Property(t => t.nome)
                .HasMaxLength(200);

            this.Property(t => t.bodas_de_bodas)
                .HasMaxLength(200);

            this.Property(t => t.telefone2)
                .HasMaxLength(200);

            this.Property(t => t.nome_noivo)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.nome_noiva)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.telefone_noivo)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.telefone_noiva)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.email_noivo)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.email_noiva)
                .IsFixedLength()
                .HasMaxLength(200);

            this.Property(t => t.mensagem_padrao)
                .HasMaxLength(200);

            this.Property(t => t.autenticacao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ORCAMENTO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.codigo_unico).HasColumnName("codigo_unico");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.data_evento).HasColumnName("data_evento");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.evento_para).HasColumnName("evento_para");
            this.Property(t => t.id_tipo_servico).HasColumnName("id_tipo_servico");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.data_nascimento).HasColumnName("data_nascimento");
            this.Property(t => t.email2).HasColumnName("email2");
            this.Property(t => t.instituicao).HasColumnName("instituicao");
            this.Property(t => t.n_convidados).HasColumnName("n_convidados");
            this.Property(t => t.n_convidados_c).HasColumnName("n_convidados_c");
            this.Property(t => t.nome2).HasColumnName("nome2");
            this.Property(t => t.razao).HasColumnName("razao");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.bodas_de_bodas).HasColumnName("bodas_de_bodas");
            this.Property(t => t.telefone2).HasColumnName("telefone2");
            this.Property(t => t.data_criacao).HasColumnName("data_criacao");
            this.Property(t => t.id_cliente).HasColumnName("id_cliente");
            this.Property(t => t.id_tipo_evento).HasColumnName("id_tipo_evento");
            this.Property(t => t.aceita_contrato).HasColumnName("aceita_contrato");
            this.Property(t => t.nome_noivo).HasColumnName("nome_noivo");
            this.Property(t => t.nome_noiva).HasColumnName("nome_noiva");
            this.Property(t => t.telefone_noivo).HasColumnName("telefone_noivo");
            this.Property(t => t.telefone_noiva).HasColumnName("telefone_noiva");
            this.Property(t => t.email_noivo).HasColumnName("email_noivo");
            this.Property(t => t.email_noiva).HasColumnName("email_noiva");
            this.Property(t => t.apenas_convite).HasColumnName("apenas_convite");
            this.Property(t => t.horario_inicio).HasColumnName("horario_inicio");
            this.Property(t => t.horario_fim).HasColumnName("horario_fim");
            this.Property(t => t.max_acompanhantes).HasColumnName("max_acompanhantes");
            this.Property(t => t.mensagem_padrao).HasColumnName("mensagem_padrao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.id_evento).HasColumnName("id_evento");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");

            // Relationships
            this.HasRequired(t => t.CLIENTE)
                .WithMany(t => t.ORCAMENTOes)
                .HasForeignKey(d => d.id_cliente);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTOes)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.EVENTO)
                .WithMany(t => t.ORCAMENTOes)
                .HasForeignKey(d => d.id_evento);
            this.HasOptional(t => t.LOCAL_EVENTO)
                .WithMany(t => t.ORCAMENTOes)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
