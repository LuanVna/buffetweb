using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_ANOTACOESMap : EntityTypeConfiguration<ORCAMENTO_ANOTACOES>
    {
        public ORCAMENTO_ANOTACOESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.anotacao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_ANOTACOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.anotacao).HasColumnName("anotacao");
            this.Property(t => t.data_horario).HasColumnName("data_horario");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_colaborador).HasColumnName("id_colaborador");

            // Relationships
            this.HasRequired(t => t.COLABORADORE)
                .WithMany(t => t.ORCAMENTO_ANOTACOES)
                .HasForeignKey(d => d.id_colaborador);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_ANOTACOES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.ORCAMENTO_ANOTACOES)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
