using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_ITENSMap : EntityTypeConfiguration<ORCAMENTO_ITENS>
    {
        public ORCAMENTO_ITENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ORCAMENTO_ITENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.valor_atual).HasColumnName("valor_atual");
            this.Property(t => t.id_cardapio).HasColumnName("id_cardapio");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.data_objeto).HasColumnName("data_objeto");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_ITENS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ITENS_CARDAPIO)
                .WithMany(t => t.ORCAMENTO_ITENS)
                .HasForeignKey(d => d.id_cardapio);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.ORCAMENTO_ITENS)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
