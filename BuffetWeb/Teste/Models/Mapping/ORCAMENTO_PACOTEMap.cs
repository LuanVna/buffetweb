using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PACOTEMap : EntityTypeConfiguration<ORCAMENTO_PACOTE>
    {
        public ORCAMENTO_PACOTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PACOTE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pacotes).HasColumnName("id_pacotes");
            this.Property(t => t.id_cardapio).HasColumnName("id_cardapio");
            this.Property(t => t.id_aluguel).HasColumnName("id_aluguel");
            this.Property(t => t.id_diversos).HasColumnName("id_diversos");
            this.Property(t => t.id_servico).HasColumnName("id_servico");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PACOTE)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.ITENS_ALUGUEL)
                .WithMany(t => t.ORCAMENTO_PACOTE)
                .HasForeignKey(d => d.id_aluguel);
            this.HasOptional(t => t.ITENS_CARDAPIO)
                .WithMany(t => t.ORCAMENTO_PACOTE)
                .HasForeignKey(d => d.id_cardapio);
            this.HasOptional(t => t.ITENS_DIVERSOS)
                .WithMany(t => t.ORCAMENTO_PACOTE)
                .HasForeignKey(d => d.id_diversos);
            this.HasOptional(t => t.ITENS_SERVICOS)
                .WithMany(t => t.ORCAMENTO_PACOTE)
                .HasForeignKey(d => d.id_servico);
            this.HasRequired(t => t.ORCAMENTO_PACOTE2)
                .WithMany(t => t.ORCAMENTO_PACOTE1)
                .HasForeignKey(d => d.id_pacotes);

        }
    }
}
