using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PACOTESMap : EntityTypeConfiguration<ORCAMENTO_PACOTES>
    {
        public ORCAMENTO_PACOTESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome_pacote)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PACOTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pacote).HasColumnName("id_pacote");
            this.Property(t => t.nome_pacote).HasColumnName("nome_pacote");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.disponivel).HasColumnName("disponivel");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PACOTES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PACOTE)
                .WithMany(t => t.ORCAMENTO_PACOTES)
                .HasForeignKey(d => d.id_pacote);

        }
    }
}
