using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PROPOSTAMap : EntityTypeConfiguration<ORCAMENTO_PROPOSTA>
    {
        public ORCAMENTO_PROPOSTAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome_proposta)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.autenticacao)
                .IsRequired();

            this.Property(t => t.categoria_proposta)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PROPOSTA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome_proposta).HasColumnName("nome_proposta");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.autenticacao).HasColumnName("autenticacao");
            this.Property(t => t.porcento_desconto_avista).HasColumnName("porcento_desconto_avista");
            this.Property(t => t.quantidade_parcelas).HasColumnName("quantidade_parcelas");
            this.Property(t => t.validade_dias).HasColumnName("validade_dias");
            this.Property(t => t.permitir_palavra_chave).HasColumnName("permitir_palavra_chave");
            this.Property(t => t.categoria_proposta).HasColumnName("categoria_proposta");
            this.Property(t => t.boleto).HasColumnName("boleto");
            this.Property(t => t.cartao).HasColumnName("cartao");
            this.Property(t => t.idade_minima).HasColumnName("idade_minima");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.LOCAL_EVENTO)
                .WithMany(t => t.ORCAMENTO_PROPOSTA)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
