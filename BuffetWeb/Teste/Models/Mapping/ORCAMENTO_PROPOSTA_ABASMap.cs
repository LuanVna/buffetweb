using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PROPOSTA_ABASMap : EntityTypeConfiguration<ORCAMENTO_PROPOSTA_ABAS>
    {
        public ORCAMENTO_PROPOSTA_ABASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome_aba)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PROPOSTA_ABAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome_aba).HasColumnName("nome_aba");
            this.Property(t => t.id_orcamento_proposta).HasColumnName("id_orcamento_proposta");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.disponivel).HasColumnName("disponivel");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_ABAS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PROPOSTA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_ABAS)
                .HasForeignKey(d => d.id_orcamento_proposta);

        }
    }
}
