using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PROPOSTA_DESCRICAOMap : EntityTypeConfiguration<ORCAMENTO_PROPOSTA_DESCRICAO>
    {
        public ORCAMENTO_PROPOSTA_DESCRICAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PROPOSTA_DESCRICAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_orcamento_proposta_aba).HasColumnName("id_orcamento_proposta_aba");
            this.Property(t => t.disponivel).HasColumnName("disponivel");
            this.Property(t => t.coluna).HasColumnName("coluna");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_DESCRICAO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PROPOSTA_ABAS)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_DESCRICAO)
                .HasForeignKey(d => d.id_orcamento_proposta_aba);

        }
    }
}
