using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PROPOSTA_PACOTEMap : EntityTypeConfiguration<ORCAMENTO_PROPOSTA_PACOTE>
    {
        public ORCAMENTO_PROPOSTA_PACOTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PROPOSTA_PACOTE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.quantidade_pessoas).HasColumnName("quantidade_pessoas");
            this.Property(t => t.id_proposta).HasColumnName("id_proposta");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.ativo).HasColumnName("ativo");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_PACOTE)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PROPOSTA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_PACOTE)
                .HasForeignKey(d => d.id_proposta);

        }
    }
}
