using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_PROPOSTA_PACOTE_VALORESMap : EntityTypeConfiguration<ORCAMENTO_PROPOSTA_PACOTE_VALORES>
    {
        public ORCAMENTO_PROPOSTA_PACOTE_VALORESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ORCAMENTO_PROPOSTA_PACOTE_VALORES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.valor_almoco).HasColumnName("valor_almoco");
            this.Property(t => t.valor_jantar).HasColumnName("valor_jantar");
            this.Property(t => t.segunda).HasColumnName("segunda");
            this.Property(t => t.terca).HasColumnName("terca");
            this.Property(t => t.quarta).HasColumnName("quarta");
            this.Property(t => t.quinta).HasColumnName("quinta");
            this.Property(t => t.sexta).HasColumnName("sexta");
            this.Property(t => t.sabado).HasColumnName("sabado");
            this.Property(t => t.id_proposta_pacote).HasColumnName("id_proposta_pacote");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.domingo).HasColumnName("domingo");
            this.Property(t => t.valor_adicional_pessoa).HasColumnName("valor_adicional_pessoa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_PACOTE_VALORES)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO_PROPOSTA_PACOTE)
                .WithMany(t => t.ORCAMENTO_PROPOSTA_PACOTE_VALORES)
                .HasForeignKey(d => d.id_proposta_pacote);

        }
    }
}
