using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class ORCAMENTO_VISITASMap : EntityTypeConfiguration<ORCAMENTO_VISITAS>
    {
        public ORCAMENTO_VISITASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.status_visita)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.tipo_visita)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ORCAMENTO_VISITAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.mensagem).HasColumnName("mensagem");
            this.Property(t => t.status_visita).HasColumnName("status_visita");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.id_horario).HasColumnName("id_horario");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.tipo_visita).HasColumnName("tipo_visita");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.ORCAMENTO_VISITAS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.ORCAMENTO_VISITAS)
                .HasForeignKey(d => d.id_local_evento);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.ORCAMENTO_VISITAS)
                .HasForeignKey(d => d.id_orcamento);
            this.HasRequired(t => t.VISITAS_AGENDADAS)
                .WithMany(t => t.ORCAMENTO_VISITAS)
                .HasForeignKey(d => d.id_horario);

        }
    }
}
