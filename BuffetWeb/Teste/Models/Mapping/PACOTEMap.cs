using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PACOTEMap : EntityTypeConfiguration<PACOTE>
    {
        public PACOTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.descricao)
                .IsRequired();

            this.Property(t => t.url_image)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PACOTES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.data_criacao).HasColumnName("data_criacao");
            this.Property(t => t.mostrar_valor).HasColumnName("mostrar_valor");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.margem_lucro).HasColumnName("margem_lucro");
            this.Property(t => t.valor_custo).HasColumnName("valor_custo");
            this.Property(t => t.valor_venda).HasColumnName("valor_venda");
            this.Property(t => t.url_image).HasColumnName("url_image");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PACOTES)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
