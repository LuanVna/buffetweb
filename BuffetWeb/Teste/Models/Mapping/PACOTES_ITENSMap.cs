using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PACOTES_ITENSMap : EntityTypeConfiguration<PACOTES_ITENS>
    {
        public PACOTES_ITENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PACOTES_ITENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_categoria_pacote).HasColumnName("id_categoria_pacote");
            this.Property(t => t.id_categoria).HasColumnName("id_categoria");
            this.Property(t => t.id_itens_cardapio).HasColumnName("id_itens_cardapio");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_ITENS)
                .WithMany(t => t.PACOTES_ITENS)
                .HasForeignKey(d => d.id_categoria);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PACOTES_ITENS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ITENS_CARDAPIO)
                .WithMany(t => t.PACOTES_ITENS)
                .HasForeignKey(d => d.id_itens_cardapio);
            this.HasRequired(t => t.PACOTE_CATEGORIA_ITENS)
                .WithMany(t => t.PACOTES_ITENS)
                .HasForeignKey(d => d.id_categoria_pacote);

        }
    }
}
