using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PACOTE_CATEGORIA_ITENSMap : EntityTypeConfiguration<PACOTE_CATEGORIA_ITENS>
    {
        public PACOTE_CATEGORIA_ITENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PACOTE_CATEGORIA_ITENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.editavel).HasColumnName("editavel");
            this.Property(t => t.maximo_itens).HasColumnName("maximo_itens");
            this.Property(t => t.id_pacote_evento).HasColumnName("id_pacote_evento");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_categoria).HasColumnName("id_categoria");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_ITENS)
                .WithMany(t => t.PACOTE_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_categoria);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PACOTE_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.PACOTE)
                .WithMany(t => t.PACOTE_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_pacote_evento);

        }
    }
}
