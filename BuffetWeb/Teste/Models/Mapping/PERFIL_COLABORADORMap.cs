using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PERFIL_COLABORADORMap : EntityTypeConfiguration<PERFIL_COLABORADOR>
    {
        public PERFIL_COLABORADORMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PERFIL_COLABORADOR");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.c_itens).HasColumnName("c_itens");
            this.Property(t => t.v_itens).HasColumnName("v_itens");
            this.Property(t => t.e_ajustes).HasColumnName("e_ajustes");
            this.Property(t => t.c_clientes).HasColumnName("c_clientes");
            this.Property(t => t.v_clientes).HasColumnName("v_clientes");
            this.Property(t => t.c_colaborador).HasColumnName("c_colaborador");
            this.Property(t => t.v_colaborador).HasColumnName("v_colaborador");
            this.Property(t => t.c_fornecedores).HasColumnName("c_fornecedores");
            this.Property(t => t.v_fornecedores).HasColumnName("v_fornecedores");
            this.Property(t => t.c_ingredientes).HasColumnName("c_ingredientes");
            this.Property(t => t.v_ingredientes).HasColumnName("v_ingredientes");
            this.Property(t => t.c_eventos).HasColumnName("c_eventos");
            this.Property(t => t.v_eventos).HasColumnName("v_eventos");
            this.Property(t => t.c_tipo_eventos).HasColumnName("c_tipo_eventos");
            this.Property(t => t.v_tipo_eventos).HasColumnName("v_tipo_eventos");
            this.Property(t => t.c_forma_pagamento).HasColumnName("c_forma_pagamento");
            this.Property(t => t.v_forma_pagamento).HasColumnName("v_forma_pagamento");
            this.Property(t => t.c_galeria).HasColumnName("c_galeria");
            this.Property(t => t.v_galeria).HasColumnName("v_galeria");
            this.Property(t => t.c_orcamentos).HasColumnName("c_orcamentos");
            this.Property(t => t.v_orcamentos).HasColumnName("v_orcamentos");
            this.Property(t => t.c_pacotes).HasColumnName("c_pacotes");
            this.Property(t => t.v_pacotes).HasColumnName("v_pacotes");
            this.Property(t => t.c_visitas).HasColumnName("c_visitas");
            this.Property(t => t.v_visitas).HasColumnName("v_visitas");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.c_financeiro).HasColumnName("c_financeiro");
            this.Property(t => t.v_financeiro).HasColumnName("v_financeiro");
            this.Property(t => t.v_configuracao).HasColumnName("v_configuracao");
            this.Property(t => t.c_configuracao).HasColumnName("c_configuracao");
            this.Property(t => t.c_estoque).HasColumnName("c_estoque");
            this.Property(t => t.v_estoque).HasColumnName("v_estoque");
            this.Property(t => t.v_convite).HasColumnName("v_convite");
            this.Property(t => t.c_convite).HasColumnName("c_convite");
            this.Property(t => t.c_ajustes).HasColumnName("c_ajustes");
            this.Property(t => t.v_ajustes).HasColumnName("v_ajustes");
            this.Property(t => t.device).HasColumnName("device");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PERFIL_COLABORADOR)
                .HasForeignKey(d => d.id_empresa);

        }
    }
}
