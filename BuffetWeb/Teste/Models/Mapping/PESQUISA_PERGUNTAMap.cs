using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PESQUISA_PERGUNTAMap : EntityTypeConfiguration<PESQUISA_PERGUNTA>
    {
        public PESQUISA_PERGUNTAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.tipo_pergunta)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("PESQUISA_PERGUNTA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pesquisa).HasColumnName("id_pesquisa");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.tipo_pergunta).HasColumnName("tipo_pergunta");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PESQUISA_PERGUNTA)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.PESQUISA_PESQUISAS)
                .WithMany(t => t.PESQUISA_PERGUNTA)
                .HasForeignKey(d => d.id_pesquisa);

        }
    }
}
