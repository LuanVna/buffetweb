using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PESQUISA_PERGUNTA_OPCOESMap : EntityTypeConfiguration<PESQUISA_PERGUNTA_OPCOES>
    {
        public PESQUISA_PERGUNTA_OPCOESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.titulo)
                .IsRequired()
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("PESQUISA_PERGUNTA_OPCOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.titulo).HasColumnName("titulo");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_pergunta).HasColumnName("id_pergunta");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PESQUISA_PERGUNTA_OPCOES)
                .HasForeignKey(d => d.id_empresa);
            this.HasOptional(t => t.PESQUISA_PERGUNTA)
                .WithMany(t => t.PESQUISA_PERGUNTA_OPCOES)
                .HasForeignKey(d => d.id_pergunta);

        }
    }
}
