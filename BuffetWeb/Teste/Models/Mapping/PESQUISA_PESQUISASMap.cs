using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PESQUISA_PESQUISASMap : EntityTypeConfiguration<PESQUISA_PESQUISAS>
    {
        public PESQUISA_PESQUISASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PESQUISA_PESQUISAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");
            this.Property(t => t.total_respostas).HasColumnName("total_respostas");
            this.Property(t => t.total_enviadas).HasColumnName("total_enviadas");
            this.Property(t => t.data_envio).HasColumnName("data_envio");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.enviada).HasColumnName("enviada");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PESQUISA_PESQUISAS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.PESQUISA_PESQUISAS)
                .HasForeignKey(d => d.id_orcamento);

        }
    }
}
