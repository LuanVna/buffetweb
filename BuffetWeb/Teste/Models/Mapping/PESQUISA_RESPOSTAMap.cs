using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PESQUISA_RESPOSTAMap : EntityTypeConfiguration<PESQUISA_RESPOSTA>
    {
        public PESQUISA_RESPOSTAMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.resposta)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PESQUISA_RESPOSTA");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_pergunta).HasColumnName("id_pergunta");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.resposta).HasColumnName("resposta");
            this.Property(t => t.id_convidado).HasColumnName("id_convidado");

            // Relationships
            this.HasOptional(t => t.CONVITE_CONVIDADOS)
                .WithMany(t => t.PESQUISA_RESPOSTA)
                .HasForeignKey(d => d.id_convidado);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PESQUISA_RESPOSTA)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.PESQUISA_PERGUNTA)
                .WithMany(t => t.PESQUISA_RESPOSTA)
                .HasForeignKey(d => d.id_pergunta);

        }
    }
}
