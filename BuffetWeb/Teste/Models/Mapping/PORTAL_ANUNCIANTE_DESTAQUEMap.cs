using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_ANUNCIANTE_DESTAQUEMap : EntityTypeConfiguration<PORTAL_ANUNCIANTE_DESTAQUE>
    {
        public PORTAL_ANUNCIANTE_DESTAQUEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PORTAL_ANUNCIANTE_DESTAQUE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.de).HasColumnName("de");
            this.Property(t => t.ate).HasColumnName("ate");
            this.Property(t => t.destaque_pesquisa_zona).HasColumnName("destaque_pesquisa_zona");
            this.Property(t => t.destaque_mapa).HasColumnName("destaque_mapa");
            this.Property(t => t.envio_proposta).HasColumnName("envio_proposta");

            // Relationships
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.PORTAL_ANUNCIANTE_DESTAQUE)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
