using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_ANUNCIANTE_PROMOCAOMap : EntityTypeConfiguration<PORTAL_ANUNCIANTE_PROMOCAO>
    {
        public PORTAL_ANUNCIANTE_PROMOCAOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PORTAL_ANUNCIANTE_PROMOCAO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_portal_promocao).HasColumnName("id_portal_promocao");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.disponivel).HasColumnName("disponivel");
            this.Property(t => t.destaque_pesquisa_zona).HasColumnName("destaque_pesquisa_zona");
            this.Property(t => t.destaque_mapa).HasColumnName("destaque_mapa");
            this.Property(t => t.envio_proposta).HasColumnName("envio_proposta");

            // Relationships
            this.HasRequired(t => t.B_PORTAL_PROMOCAO)
                .WithMany(t => t.PORTAL_ANUNCIANTE_PROMOCAO)
                .HasForeignKey(d => d.id_portal_promocao);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.PORTAL_ANUNCIANTE_PROMOCAO)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
