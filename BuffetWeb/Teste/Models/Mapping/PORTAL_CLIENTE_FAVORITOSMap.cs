using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_CLIENTE_FAVORITOSMap : EntityTypeConfiguration<PORTAL_CLIENTE_FAVORITOS>
    {
        public PORTAL_CLIENTE_FAVORITOSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PORTAL_CLIENTE_FAVORITOS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.desde).HasColumnName("desde");
            this.Property(t => t.id_cliente_perfil).HasColumnName("id_cliente_perfil");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");

            // Relationships
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.PORTAL_CLIENTE_FAVORITOS)
                .HasForeignKey(d => d.id_local_evento);
            this.HasRequired(t => t.PORTAL_CLIENTE_PERFIL)
                .WithMany(t => t.PORTAL_CLIENTE_FAVORITOS)
                .HasForeignKey(d => d.id_cliente_perfil);

        }
    }
}
