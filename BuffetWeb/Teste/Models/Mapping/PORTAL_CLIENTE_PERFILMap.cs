using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_CLIENTE_PERFILMap : EntityTypeConfiguration<PORTAL_CLIENTE_PERFIL>
    {
        public PORTAL_CLIENTE_PERFILMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.telefone)
                .HasMaxLength(15);

            this.Property(t => t.celular)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.operadora_celular)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.perfil)
                .IsRequired();

            this.Property(t => t.regiao)
                .HasMaxLength(300);

            this.Property(t => t.senha)
                .IsRequired();

            this.Property(t => t.id_facebook)
                .HasMaxLength(200);

            this.Property(t => t.cep)
                .HasMaxLength(20);

            this.Property(t => t.logradouro)
                .HasMaxLength(20);

            this.Property(t => t.numero)
                .HasMaxLength(20);

            this.Property(t => t.bairro)
                .HasMaxLength(20);

            this.Property(t => t.cidade)
                .HasMaxLength(20);

            this.Property(t => t.estado)
                .HasMaxLength(20);

            this.Property(t => t.renovarSenha)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("PORTAL_CLIENTE_PERFIL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.celular).HasColumnName("celular");
            this.Property(t => t.operadora_celular).HasColumnName("operadora_celular");
            this.Property(t => t.perfil).HasColumnName("perfil");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.regiao).HasColumnName("regiao");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.id_facebook).HasColumnName("id_facebook");
            this.Property(t => t.cep).HasColumnName("cep");
            this.Property(t => t.logradouro).HasColumnName("logradouro");
            this.Property(t => t.numero).HasColumnName("numero");
            this.Property(t => t.bairro).HasColumnName("bairro");
            this.Property(t => t.cidade).HasColumnName("cidade");
            this.Property(t => t.estado).HasColumnName("estado");
            this.Property(t => t.renovarSenha).HasColumnName("renovarSenha");
            this.Property(t => t.signedRequest).HasColumnName("signedRequest");
        }
    }
}
