using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_CLIENTE_PESQUISASMap : EntityTypeConfiguration<PORTAL_CLIENTE_PESQUISAS>
    {
        public PORTAL_CLIENTE_PESQUISASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.termo)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.url)
                .IsRequired();

            this.Property(t => t.mensagem)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PORTAL_CLIENTE_PESQUISAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.termo).HasColumnName("termo");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.id_perfil_cliente).HasColumnName("id_perfil_cliente");
            this.Property(t => t.mensagem).HasColumnName("mensagem");

            // Relationships
            this.HasOptional(t => t.PORTAL_CLIENTE_PERFIL)
                .WithMany(t => t.PORTAL_CLIENTE_PESQUISAS)
                .HasForeignKey(d => d.id_perfil_cliente);

        }
    }
}
