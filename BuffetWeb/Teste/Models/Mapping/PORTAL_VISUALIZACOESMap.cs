using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PORTAL_VISUALIZACOESMap : EntityTypeConfiguration<PORTAL_VISUALIZACOES>
    {
        public PORTAL_VISUALIZACOESMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.tipo)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.facebook)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PORTAL_VISUALIZACOES");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.id_cliente_portal).HasColumnName("id_cliente_portal");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");
            this.Property(t => t.tipo).HasColumnName("tipo");
            this.Property(t => t.facebook).HasColumnName("facebook");

            // Relationships
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.PORTAL_VISUALIZACOES)
                .HasForeignKey(d => d.id_local_evento);
            this.HasOptional(t => t.PORTAL_CLIENTE_PERFIL)
                .WithMany(t => t.PORTAL_VISUALIZACOES)
                .HasForeignKey(d => d.id_cliente_portal);

        }
    }
}
