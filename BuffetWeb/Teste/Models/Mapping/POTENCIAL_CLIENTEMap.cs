using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class POTENCIAL_CLIENTEMap : EntityTypeConfiguration<POTENCIAL_CLIENTE>
    {
        public POTENCIAL_CLIENTEMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.telefone)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("POTENCIAL_CLIENTE");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.telefone).HasColumnName("telefone");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.id_festa).HasColumnName("id_festa");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.POTENCIAL_CLIENTE)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.EVENTO)
                .WithMany(t => t.POTENCIAL_CLIENTE)
                .HasForeignKey(d => d.id_festa);

        }
    }
}
