using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PRESTADORMap : EntityTypeConfiguration<PRESTADOR>
    {
        public PRESTADORMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.segunda)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.terca)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.quarta)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.quinta)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.sexta)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.sabado)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.domingo)
                .IsRequired()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("PRESTADOR");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.sexo).HasColumnName("sexo");
            this.Property(t => t.valor_custo).HasColumnName("valor_custo");
            this.Property(t => t.valor_venda).HasColumnName("valor_venda");
            this.Property(t => t.id_funcao).HasColumnName("id_funcao");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.segunda).HasColumnName("segunda");
            this.Property(t => t.terca).HasColumnName("terca");
            this.Property(t => t.quarta).HasColumnName("quarta");
            this.Property(t => t.quinta).HasColumnName("quinta");
            this.Property(t => t.sexta).HasColumnName("sexta");
            this.Property(t => t.sabado).HasColumnName("sabado");
            this.Property(t => t.domingo).HasColumnName("domingo");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PRESTADORs)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.PRESTADOR_FUNCAO)
                .WithMany(t => t.PRESTADORs)
                .HasForeignKey(d => d.id_funcao);

        }
    }
}
