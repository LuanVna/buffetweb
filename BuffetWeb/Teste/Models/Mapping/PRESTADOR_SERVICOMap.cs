using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class PRESTADOR_SERVICOMap : EntityTypeConfiguration<PRESTADOR_SERVICO>
    {
        public PRESTADOR_SERVICOMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("PRESTADOR_SERVICO");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_servico).HasColumnName("id_servico");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_prestador_funcao).HasColumnName("id_prestador_funcao");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.PRESTADOR_SERVICO)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ITENS_SERVICOS)
                .WithMany(t => t.PRESTADOR_SERVICO)
                .HasForeignKey(d => d.id_servico);
            this.HasRequired(t => t.PRESTADOR_FUNCAO)
                .WithMany(t => t.PRESTADOR_SERVICO)
                .HasForeignKey(d => d.id_prestador_funcao);

        }
    }
}
