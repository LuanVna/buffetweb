using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class SUB_CATEGORIA_ITENSMap : EntityTypeConfiguration<SUB_CATEGORIA_ITENS>
    {
        public SUB_CATEGORIA_ITENSMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("SUB_CATEGORIA_ITENS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.id_categoria_itens).HasColumnName("id_categoria_itens");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_unidade_medida).HasColumnName("id_unidade_medida");
            this.Property(t => t.rendimento_pessoa).HasColumnName("rendimento_pessoa");

            // Relationships
            this.HasRequired(t => t.CATEGORIA_ITENS)
                .WithMany(t => t.SUB_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_categoria_itens);
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.SUB_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.UNIDADE_MEDIDA)
                .WithMany(t => t.SUB_CATEGORIA_ITENS)
                .HasForeignKey(d => d.id_unidade_medida);

        }
    }
}
