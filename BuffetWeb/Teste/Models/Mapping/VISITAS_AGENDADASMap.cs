using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class VISITAS_AGENDADASMap : EntityTypeConfiguration<VISITAS_AGENDADAS>
    {
        public VISITAS_AGENDADASMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.dia)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("VISITAS_AGENDADAS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.dia).HasColumnName("dia");
            this.Property(t => t.data).HasColumnName("data");
            this.Property(t => t.id_visitas_disponiveis).HasColumnName("id_visitas_disponiveis");
            this.Property(t => t.id_orcamento).HasColumnName("id_orcamento");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.VISITAS_AGENDADAS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.ORCAMENTO)
                .WithMany(t => t.VISITAS_AGENDADAS)
                .HasForeignKey(d => d.id_orcamento);
            this.HasRequired(t => t.VISITAS_DISPONIVEIS)
                .WithMany(t => t.VISITAS_AGENDADAS)
                .HasForeignKey(d => d.id_visitas_disponiveis);

        }
    }
}
