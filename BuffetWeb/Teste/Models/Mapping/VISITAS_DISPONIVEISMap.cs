using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BuffetDataManager.Models.Mapping
{
    public class VISITAS_DISPONIVEISMap : EntityTypeConfiguration<VISITAS_DISPONIVEIS>
    {
        public VISITAS_DISPONIVEISMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.dia_visita)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.tipo_visita)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("VISITAS_DISPONIVEIS");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.dia_visita).HasColumnName("dia_visita");
            this.Property(t => t.horario).HasColumnName("horario");
            this.Property(t => t.tipo_visita).HasColumnName("tipo_visita");
            this.Property(t => t.id_empresa).HasColumnName("id_empresa");
            this.Property(t => t.id_local_evento).HasColumnName("id_local_evento");

            // Relationships
            this.HasRequired(t => t.EMPRESA)
                .WithMany(t => t.VISITAS_DISPONIVEIS)
                .HasForeignKey(d => d.id_empresa);
            this.HasRequired(t => t.LOCAL_EVENTO)
                .WithMany(t => t.VISITAS_DISPONIVEIS)
                .HasForeignKey(d => d.id_local_evento);

        }
    }
}
