using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class NOTIFICACO
    {
        public int id { get; set; }
        public string mensagem { get; set; }
        public System.DateTime data { get; set; }
        public bool lido { get; set; }
        public bool deBuffetWeb { get; set; }
        public int id_empresa { get; set; }
        public Nullable<int> para_id_colaborador { get; set; }
        public Nullable<int> de_id_colaborador { get; set; }
        public Nullable<bool> paraBuffetWeb { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual COLABORADORE COLABORADORE1 { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
