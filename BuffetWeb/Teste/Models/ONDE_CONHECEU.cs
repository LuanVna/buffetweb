using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ONDE_CONHECEU
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
