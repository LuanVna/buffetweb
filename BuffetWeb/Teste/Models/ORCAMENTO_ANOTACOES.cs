using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_ANOTACOES
    {
        public int id { get; set; }
        public int id_orcamento { get; set; }
        public string titulo { get; set; }
        public string anotacao { get; set; }
        public System.DateTime data_horario { get; set; }
        public int id_empresa { get; set; }
        public int id_colaborador { get; set; }
        public virtual COLABORADORE COLABORADORE { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
    }
}
