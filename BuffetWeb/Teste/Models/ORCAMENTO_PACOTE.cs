using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_PACOTE
    {
        public ORCAMENTO_PACOTE()
        {
            this.ORCAMENTO_PACOTE1 = new List<ORCAMENTO_PACOTE>();
            this.ORCAMENTO_PACOTES = new List<ORCAMENTO_PACOTES>();
        }

        public int id { get; set; }
        public int id_pacotes { get; set; }
        public Nullable<int> id_cardapio { get; set; }
        public Nullable<int> id_aluguel { get; set; }
        public Nullable<int> id_diversos { get; set; }
        public Nullable<int> id_servico { get; set; }
        public int id_empresa { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ITENS_ALUGUEL ITENS_ALUGUEL { get; set; }
        public virtual ITENS_CARDAPIO ITENS_CARDAPIO { get; set; }
        public virtual ITENS_DIVERSOS ITENS_DIVERSOS { get; set; }
        public virtual ITENS_SERVICOS ITENS_SERVICOS { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTE> ORCAMENTO_PACOTE1 { get; set; }
        public virtual ORCAMENTO_PACOTE ORCAMENTO_PACOTE2 { get; set; }
        public virtual ICollection<ORCAMENTO_PACOTES> ORCAMENTO_PACOTES { get; set; }
    }
}
