using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_PROPOSTA
    {
        public ORCAMENTO_PROPOSTA()
        {
            this.CLIENTE_PROPOSTA = new List<CLIENTE_PROPOSTA>();
            this.ORCAMENTO_PROPOSTA_PACOTE = new List<ORCAMENTO_PROPOSTA_PACOTE>();
            this.ORCAMENTO_PROPOSTA_ABAS = new List<ORCAMENTO_PROPOSTA_ABAS>();
        }

        public int id { get; set; }
        public string nome_proposta { get; set; }
        public int id_empresa { get; set; }
        public Nullable<int> id_local_evento { get; set; }
        public string autenticacao { get; set; }
        public int porcento_desconto_avista { get; set; }
        public int quantidade_parcelas { get; set; }
        public int validade_dias { get; set; }
        public bool permitir_palavra_chave { get; set; }
        public string categoria_proposta { get; set; }
        public Nullable<bool> boleto { get; set; }
        public Nullable<bool> cartao { get; set; }
        public Nullable<int> idade_minima { get; set; }
        public virtual ICollection<CLIENTE_PROPOSTA> CLIENTE_PROPOSTA { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_PACOTE> ORCAMENTO_PROPOSTA_PACOTE { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_ABAS> ORCAMENTO_PROPOSTA_ABAS { get; set; }
    }
}
