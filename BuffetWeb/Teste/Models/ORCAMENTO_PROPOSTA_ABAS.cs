using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_PROPOSTA_ABAS
    {
        public ORCAMENTO_PROPOSTA_ABAS()
        {
            this.ORCAMENTO_PROPOSTA_DESCRICAO = new List<ORCAMENTO_PROPOSTA_DESCRICAO>();
        }

        public int id { get; set; }
        public string nome_aba { get; set; }
        public int id_orcamento_proposta { get; set; }
        public int id_empresa { get; set; }
        public bool disponivel { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PROPOSTA ORCAMENTO_PROPOSTA { get; set; }
        public virtual ICollection<ORCAMENTO_PROPOSTA_DESCRICAO> ORCAMENTO_PROPOSTA_DESCRICAO { get; set; }
    }
}
