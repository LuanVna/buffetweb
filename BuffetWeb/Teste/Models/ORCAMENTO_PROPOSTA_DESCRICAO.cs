using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_PROPOSTA_DESCRICAO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int id_empresa { get; set; }
        public int id_orcamento_proposta_aba { get; set; }
        public bool disponivel { get; set; }
        public Nullable<int> coluna { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ORCAMENTO_PROPOSTA_ABAS ORCAMENTO_PROPOSTA_ABAS { get; set; }
    }
}
