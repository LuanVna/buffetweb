using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class ORCAMENTO_VISITAS
    {
        public int id { get; set; }
        public System.DateTime data { get; set; }
        public string mensagem { get; set; }
        public string status_visita { get; set; }
        public int id_local_evento { get; set; }
        public int id_horario { get; set; }
        public int id_orcamento { get; set; }
        public int id_empresa { get; set; }
        public string tipo_visita { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual ORCAMENTO ORCAMENTO { get; set; }
        public virtual VISITAS_AGENDADAS VISITAS_AGENDADAS { get; set; }
    }
}
