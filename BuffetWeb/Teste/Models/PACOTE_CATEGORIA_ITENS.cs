using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PACOTE_CATEGORIA_ITENS
    {
        public PACOTE_CATEGORIA_ITENS()
        {
            this.PACOTES_ITENS = new List<PACOTES_ITENS>();
        }

        public int id { get; set; }
        public Nullable<bool> editavel { get; set; }
        public int maximo_itens { get; set; }
        public int id_pacote_evento { get; set; }
        public int id_empresa { get; set; }
        public int id_categoria { get; set; }
        public virtual CATEGORIA_ITENS CATEGORIA_ITENS { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual ICollection<PACOTES_ITENS> PACOTES_ITENS { get; set; }
        public virtual PACOTE PACOTE { get; set; }
    }
}
