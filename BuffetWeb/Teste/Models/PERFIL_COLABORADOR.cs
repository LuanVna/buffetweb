using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PERFIL_COLABORADOR
    {
        public PERFIL_COLABORADOR()
        {
            this.COLABORADORES = new List<COLABORADORE>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public bool c_itens { get; set; }
        public bool v_itens { get; set; }
        public bool e_ajustes { get; set; }
        public bool c_clientes { get; set; }
        public bool v_clientes { get; set; }
        public bool c_colaborador { get; set; }
        public bool v_colaborador { get; set; }
        public bool c_fornecedores { get; set; }
        public bool v_fornecedores { get; set; }
        public bool c_ingredientes { get; set; }
        public bool v_ingredientes { get; set; }
        public bool c_eventos { get; set; }
        public bool v_eventos { get; set; }
        public bool c_tipo_eventos { get; set; }
        public bool v_tipo_eventos { get; set; }
        public bool c_forma_pagamento { get; set; }
        public bool v_forma_pagamento { get; set; }
        public bool c_galeria { get; set; }
        public bool v_galeria { get; set; }
        public bool c_orcamentos { get; set; }
        public bool v_orcamentos { get; set; }
        public bool c_pacotes { get; set; }
        public bool v_pacotes { get; set; }
        public bool c_visitas { get; set; }
        public bool v_visitas { get; set; }
        public int id_empresa { get; set; }
        public bool c_financeiro { get; set; }
        public bool v_financeiro { get; set; }
        public bool v_configuracao { get; set; }
        public bool c_configuracao { get; set; }
        public bool c_estoque { get; set; }
        public bool v_estoque { get; set; }
        public bool v_convite { get; set; }
        public bool c_convite { get; set; }
        public bool c_ajustes { get; set; }
        public bool v_ajustes { get; set; }
        public bool device { get; set; }
        public virtual ICollection<COLABORADORE> COLABORADORES { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
    }
}
