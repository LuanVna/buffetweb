using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PORTAL_ANUNCIANTE_TOPMENU
    {
        public int id { get; set; }
        public int id_local_evento { get; set; }
        public System.DateTime de { get; set; }
        public System.DateTime ate { get; set; }
        public string regiao { get; set; }
        public bool destaque_pesquisa_zona { get; set; }
        public bool destaque_mapa { get; set; }
        public bool envio_proposta { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
    }
}
