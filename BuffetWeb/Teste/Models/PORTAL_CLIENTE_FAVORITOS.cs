using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PORTAL_CLIENTE_FAVORITOS
    {
        public int id { get; set; }
        public System.DateTime desde { get; set; }
        public int id_cliente_perfil { get; set; }
        public int id_local_evento { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual PORTAL_CLIENTE_PERFIL PORTAL_CLIENTE_PERFIL { get; set; }
    }
}
