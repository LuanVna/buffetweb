using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PORTAL_CLIENTE_PERFIL
    {
        public PORTAL_CLIENTE_PERFIL()
        {
            this.LOCAL_EVENTO_COMENTARIOS = new List<LOCAL_EVENTO_COMENTARIOS>();
            this.PORTAL_CLIENTE_FAVORITOS = new List<PORTAL_CLIENTE_FAVORITOS>();
            this.PORTAL_VISUALIZACOES = new List<PORTAL_VISUALIZACOES>();
            this.PORTAL_CLIENTE_PESQUISAS = new List<PORTAL_CLIENTE_PESQUISAS>();
        }

        public int id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string operadora_celular { get; set; }
        public string perfil { get; set; }
        public string url { get; set; }
        public string regiao { get; set; }
        public string senha { get; set; }
        public string id_facebook { get; set; }
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string renovarSenha { get; set; }
        public string signedRequest { get; set; }
        public virtual ICollection<LOCAL_EVENTO_COMENTARIOS> LOCAL_EVENTO_COMENTARIOS { get; set; }
        public virtual ICollection<PORTAL_CLIENTE_FAVORITOS> PORTAL_CLIENTE_FAVORITOS { get; set; }
        public virtual ICollection<PORTAL_VISUALIZACOES> PORTAL_VISUALIZACOES { get; set; }
        public virtual ICollection<PORTAL_CLIENTE_PESQUISAS> PORTAL_CLIENTE_PESQUISAS { get; set; }
    }
}
