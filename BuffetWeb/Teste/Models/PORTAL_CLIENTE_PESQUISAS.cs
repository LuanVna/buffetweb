using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PORTAL_CLIENTE_PESQUISAS
    {
        public int id { get; set; }
        public string termo { get; set; }
        public string url { get; set; }
        public System.DateTime data { get; set; }
        public Nullable<int> id_perfil_cliente { get; set; }
        public string mensagem { get; set; }
        public virtual PORTAL_CLIENTE_PERFIL PORTAL_CLIENTE_PERFIL { get; set; }
    }
}
