using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class PRESTADOR
    {
        public int id { get; set; }
        public string nome { get; set; }
        public bool sexo { get; set; }
        public decimal valor_custo { get; set; }
        public decimal valor_venda { get; set; }
        public int id_funcao { get; set; }
        public int id_empresa { get; set; }
        public string segunda { get; set; }
        public string terca { get; set; }
        public string quarta { get; set; }
        public string quinta { get; set; }
        public string sexta { get; set; }
        public string sabado { get; set; }
        public string domingo { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual PRESTADOR_FUNCAO PRESTADOR_FUNCAO { get; set; }
    }
}
