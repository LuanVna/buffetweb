using System;
using System.Collections.Generic;

namespace BuffetDataManager.Models
{
    public partial class VISITAS_DISPONIVEIS
    {
        public VISITAS_DISPONIVEIS()
        {
            this.VISITAS_AGENDADAS = new List<VISITAS_AGENDADAS>();
        }

        public int id { get; set; }
        public string dia_visita { get; set; }
        public System.TimeSpan horario { get; set; }
        public string tipo_visita { get; set; }
        public int id_empresa { get; set; }
        public int id_local_evento { get; set; }
        public virtual EMPRESA EMPRESA { get; set; }
        public virtual LOCAL_EVENTO LOCAL_EVENTO { get; set; }
        public virtual ICollection<VISITAS_AGENDADAS> VISITAS_AGENDADAS { get; set; }
    }
}
