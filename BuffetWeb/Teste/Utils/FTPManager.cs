﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BuffetDataManager.Models;
using System.Configuration;

namespace BuffetDataManager.Models
{

    public enum ImageFolder
    {
        TipoEvento,
        Cardapio,
        Perfil,
        Pacotes,
        Aluguel,
        Diversos,
        Convites,
        FacebookProfile,
        ItemCardapio,
        LocalEvento
    }

    public class FTPManager
    {
        public FTPManager() { }

        public FTPManager(int id_empresa)
        {
            this.id_empresa = id_empresa;
        }

        public string PATH_FTP
        {
            get
            {
                using (BuffetContext db = new BuffetContext())
                {
                    var empresa = db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                    return string.Format("{0}_{1}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa);
                }
            }
        }

        public string PATH_FTP_URL(string folder)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var empresa = db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                return string.Format("ftp://ftp.buffetweb.com/www/Sites/Arquivos/Empresas/{0}_{1}/{2}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa, folder);
            }
        }

        public string PATH_URL(string folder)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var empresa = db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                return string.Format("http://www.buffetweb.com/Sites/Arquivos/Empresas/{0}_{1}/{2}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa, folder);
            }
        }

        private int id_empresa { get; set; }
        private string FTP_HOST { get { return getConfig("FTP_HOST"); } }
        private string FTP_USUARIO { get { return getConfig("FTP_USUARIO"); } }
        private string FTP_SENHA { get { return getConfig("FTP_SENHA"); } }

        private string getConfig(string chave)
        {
            using (BuffetContext db = new BuffetContext())
            {
                return db.B_EMPRESA_AJUSTES.FirstOrDefault(e => e.chave.Equals(chave)).valor;
            }
        }

        private void CreateFolder(string folder)
        {
            WebRequest request = WebRequest.Create(string.Format("ftp://ftp.buffetweb.com/www/Sites/Arquivos/Empresas/{0}", folder));
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = new NetworkCredential(this.FTP_USUARIO, this.FTP_SENHA);
            using (var resp = (FtpWebResponse)request.GetResponse())
            {
                Console.WriteLine(resp.StatusCode);
            }
        }

        public void CreateFolderBussines()
        {
            using (BuffetContext db = new BuffetContext())
            {
                var empresa = db.EMPRESAs.FirstOrDefault(e => e.id == this.id_empresa);
                string baseBussines = string.Format("{0}_{1}", empresa.nome.Replace(" ", ""), empresa.codigo_empresa);

                this.CreateFolder(baseBussines);

                var folders = new ImageFolder[]{
                    ImageFolder.Aluguel,
                    ImageFolder.Cardapio,
                    ImageFolder.Convites,
                    ImageFolder.Diversos,
                    ImageFolder.FacebookProfile,
                    ImageFolder.ItemCardapio,
                    ImageFolder.LocalEvento,
                    ImageFolder.Pacotes,
                    ImageFolder.Perfil,
                    ImageFolder.TipoEvento,
                };

                for (int i = 0; i < folders.Length; i++)
                {
                    this.CreateFolder(string.Format("{0}/{1}", baseBussines, folders[i].ToString()));
                }
            }
        }

        public Tuple<bool, string> updateNewLogo(Stream image)
        {
            using (BuffetContext db = new BuffetContext())
            {
                var empresa = db.EMPRESAs.FirstOrDefault(f => f.id == this.id_empresa);
                return upload(image, this.PATH_FTP_URL("Sistema/Logo.png"));
            }
        }

        public string LogoEmpresa()
        {
            return this.PATH_URL("Sistema/Logo.png");
        }

        public Tuple<bool, string> uploadImage(ImageFolder imageFolder, string name, Stream imagem)
        {
            if (imagem != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return upload(imagem, this.PATH_FTP_URL(string.Format("{0}/{1}", imageFolder.ToString(), name)));
                }
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }

        public static Tuple<bool, string> UploadImageSistema(string path, string name, Stream imagem)
        {
            if (imagem != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    return new FTPManager(0).upload(imagem, string.Format("ftp://buffetweb@buffetweb.com/www/Sites/Arquivos/Sistema/{0}/{1}.png", path, name.Replace(".png", "")));
                }
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }

        public Tuple<bool, string> uploadImage(ImageFolder imageFolder, string name, string url)
        {
            if (url != null)
            {
                using (BuffetContext db = new BuffetContext())
                {
                    byte[] imageData = null;
                    using (var wc = new WebClient())
                        imageData = wc.DownloadData(url);

                    Stream stream = new MemoryStream(imageData);
                    return this.uploadImage(imageFolder, name, stream);
                }
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }

        #region MASTER
        //TODO: Arrumar depois
        public Tuple<bool, string> uploadBusiness(string pasta, string nome, Stream imagem)
        {
            if (imagem != null)
            {
                this.id_empresa = id_empresa;
                pasta = string.Format(@"ftp://ftp.buffetweb.com/www/Sites/Arquivos/Empresas/{0}/{1}.png", pasta, nome);

                return upload(imagem, pasta);
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }

        public Tuple<bool, string> uploadFonte(string name, Stream fonte)
        {
            if (fonte != null)
            {
                this.id_empresa = id_empresa;
                name = string.Format(@"http://www.buffetweb.com/Sites/Arquivos/Empresas/Sistema/Fontes{0}.ttf", name);

                return upload(fonte, name);
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }

        public Tuple<bool, string> uploadTemplateConvite(int id_convite, int id_template_convite)
        {
            try
            {
                byte[] imageData = null;

                using (var wc = new System.Net.WebClient())
                    imageData = wc.DownloadData("http://buffetweb.com/Sites/Arquivos/Empresas/Sistema/Convite/" + id_template_convite + ".png");

                Stream stream = new MemoryStream(imageData);
                return this.uploadImage(ImageFolder.Convites, id_convite.ToString(), stream);

            }
            catch (Exception e)
            {
                return new Tuple<bool, string>(false, "Tente enviar a imagem novamente!");
            }
        }

        #endregion

        public Tuple<bool, string> uploadPhotoFacebook(string id_Facebook)
        {
            try
            {
                byte[] imageData = null;

                using (var wc = new System.Net.WebClient())
                    imageData = wc.DownloadData("http://graph.facebook.com/" + id_Facebook + "/picture");

                Stream stream = new MemoryStream(imageData);
                return this.uploadImage(ImageFolder.FacebookProfile, id_Facebook, stream);

            }
            catch (Exception e)
            {
                return new Tuple<bool, string>(false, "Tente enviar a imagem novamente!");
            }
        }

        public bool ExistFile(string path)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://ftp.buffetweb.com/www/Sites/Arquivos/{0}", path));
            request.Credentials = new NetworkCredential(this.FTP_USUARIO, this.FTP_SENHA);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                return !(response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable);
            } 
        }

        public Tuple<bool, string> upload(Stream imagem, string url)
        {
            if (imagem != null)
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(this.FTP_USUARIO, this.FTP_SENHA);

                request.KeepAlive = false;
                request.UseBinary = true;
                request.UsePassive = true;
                request.ContentLength = imagem.Length;
                request.EnableSsl = false;

                int buffLen = 2048;
                byte[] buff = new byte[buffLen];
                int contentLen;

                try
                {
                    Stream ftpStream = request.GetRequestStream();
                    contentLen = imagem.Read(buff, 0, buffLen);
                    while (contentLen != 0)
                    {
                        ftpStream.Write(buff, 0, contentLen);
                        contentLen = imagem.Read(buff, 0, buffLen);
                    }
                    ftpStream.Flush();
                    ftpStream.Close();
                    return new Tuple<bool, string>(true, url.Replace("ftp://buffetweb@", "http://www.").Replace("ftp://", "http://").Replace("ftp.", "www.").Replace("www/", ""));
                }
                catch (Exception exc)
                {
                    return new Tuple<bool, string>(false, exc.Message);
                }
            }
            return new Tuple<bool, string>(false, "Nenhuma imagem enviada");
        }
    }
}
