﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuffetWeb.Controllers
{
    public class GaleriaImagensController : Controller
    {
        //
        // GET: /GaleriaImagens/
        public ActionResult PortfolioCasamento1()
        {
            return View();
        }

        public ActionResult PortfolioCasamento2()
        {
            return View();
        }

        public ActionResult PortfolioEspaco1()
        {
            return View();
        }

        public ActionResult PortfolioEspaco2()
        {
            return View();
        }

        public ActionResult PortfolioEspaco3()
        {
            return View();
        }

        public ActionResult PortfolioEspaco4()
        {
            return View();
        }

        public ActionResult PortfolioFesta1()
        {
            return View();
        }

        public ActionResult PortfolioFesta2()
        {
            return View();
        }
	}
}