﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BuffetWeb.Controllers
{
    public class HomeController : Controller
    {
        public static bool? MensagemEnviada { get; set; }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Orcamento()
        {
            return View();
        }

        public ActionResult Convite()
        {
            return View();
        }

        public ActionResult ERP()
        {
            return View();
        }

        public ActionResult GaleriaImagens()
        {
            return View();
        }

        public ActionResult Implantacao()
        {
            ViewBag.mensagem = MensagemEnviada;
            MensagemEnviada = null;
            return View();
        }

        public ActionResult Treinamento()
        {
            return View();
        }

        public ActionResult Suporte()
        {
            return View();
        }

        public ActionResult Manutencao()
        {
            return View();
        }

        public ActionResult Contato(string assunto)
        {
            ViewBag.assunto = assunto;
            ViewBag.mensagem = MensagemEnviada;
            MensagemEnviada = null;
            return View();
        }

        public ActionResult Mensagem(string nome, string email, string telefone, string assunto, string mensagem)
        {
            try
            {
                SmtpClient cliente = new SmtpClient("smtpi.uni5.net", 587);
                NetworkCredential credenciais = new NetworkCredential("contato@buffetweb.com", "8i9j3u0p");
                MailMessage mensagem_email = new MailMessage();
                cliente.UseDefaultCredentials = false;
                cliente.Credentials = credenciais;
                cliente.EnableSsl = true;
                mensagem_email.Subject = assunto;
                mensagem_email.From = new MailAddress(email);
                mensagem_email.To.Add(new MailAddress("contato@buffetweb.com"));
                mensagem_email.Body = mensagem;
                cliente.Send(mensagem_email);
                MensagemEnviada = true;
            }
            catch (Exception e)
            {
                MensagemEnviada = false;
            }
            return RedirectToAction("Contato");
        }

        public ActionResult FormImplanta(string nome, string email, string telefone, string assunto, string mensagem)
        {
            try
            {
                SmtpClient cliente = new SmtpClient("smtpi.uni5.net", 587);
                NetworkCredential credenciais = new NetworkCredential("contato@buffetweb.com", "8i9j3u0p");
                MailMessage mensagem_email = new MailMessage();
                cliente.UseDefaultCredentials = false;
                cliente.Credentials = credenciais;
                cliente.EnableSsl = true;
                mensagem_email.Subject = assunto;
                mensagem_email.From = new MailAddress(email);
                mensagem_email.To.Add(new MailAddress("contato@buffetweb.com"));
                mensagem_email.Body = mensagem;
                cliente.Send(mensagem_email);
                MensagemEnviada = true;
            }
            catch (Exception e)
            {
                MensagemEnviada = false;
            }
            return RedirectToAction("Implantacao");
        }
    }
}