﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BuffetWeb.Startup))]
namespace BuffetWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
